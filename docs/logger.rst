=====================
 Ferda log structure
=====================

This document describes structure of audit logs sent to FRED logger service.

* All messages belong to service ``Ferda``
* Return code is either ``Success``, ``Fail`` or ``Error`` (default)

.. contents:: Table of Contents
   :depth: 2

The following abbreviations are used to describe references and properties:

* ``R`` – property/reference is required
* ``#`` – number of properties/references in log entry; ``*`` stands for arbitrary number
* ``key`` – property/reference key
* ``type`` – property/reference type
* ``value`` – property/reference default value
* ``note`` – anything else that's worth mentioning

--------------------
Contact-related logs
--------------------

``ContactDetail``
=================

A user has accessed basic contact information.

References
----------

.. csv-table::
   :header: R, #, key, type, value, note

   , 1, contact, UUID, , Contact identifier

Properties
----------

.. csv-table::
   :header: I/O, R, #, key, type, value, note

   I, ✔, 1, contact, UUID, , Contact identifier
   I,  , 1, history_id [#history_id_datetime]_, UUID, , Contact history identifier
   I,  , 1, history_datetime, datetime, , Contact history at given point
   O,  , 1, found_history_id, UUID, , Contact history id found from time point

``ContactState``
================

A user has accessed contact state or state history information.

References
----------

.. csv-table::
   :header: R, #, key, type, value, note

   , 1, contact, UUID, , Contact identifier

Properties
----------

.. csv-table::
   :header: I/O, R, #, key, type, value, note

   I, ✔, 1, contact, UUID, , Contact identifier
   I,  , 1, history, bool, \"true\", Whether user requested state history

``ContactHistory``
==================

A user has accessed contact history overview.

References
----------

.. csv-table::
   :header: R, #, key, type, value, note

   , 1, contact, UUID, , Contact identifier

Properties
----------

.. csv-table::
   :header: I/O, R, #, key, type, value, note

   I, ✔, 1, contact, UUID, , Contact identifier

``ContactSearch``
=================

A user has run a search for contacts.

References
----------

.. csv-table::
   :header: R, #, key, type, value, note

   , \*, contact, UUID, , Matching contacts identifiers

Properties
----------

.. csv-table::
   :header: I/O, R, #, key, type, value, note

   I, ✔, \*, query_values, str, , Query values
   I,  , \*, search_item, str, , Contact items that should be searched for match


``ContactRepresentativeDetail``
===============================

A user has viewed contact representative information.

References
----------

.. csv-table::
   :header: R, #, key, type, value, note

   , 1, representative, UUID, , Contact representative identifier
   , 1, contact, UUID, , Contact identifier

Properties
----------

.. csv-table::
   :header: I/O, R, #, key, type, value, note

   I, , 1, representative_id, UUID, , Contact representative identifier
   I, , 1, history_id, UUID, , Contact representative history identifier
   I, , 1, contact_id, UUID, , Contact identifier


``ContactRepresentativeCreate``
===============================

A user has viewed contact representative information.

References
----------

.. csv-table::
   :header: R, #, key, type, value, note

   , 1, representative, UUID, , Contact representative identifier
   , 1, contact, UUID, , Contact identifier

``ContactRepresentativeChange``
===============================

A user has changed contact representative information.

References
----------

.. csv-table::
   :header: R, #, key, type, value, note

   , 1, representative, UUID, , Contact representative identifier
   , 1, contact, UUID, , Contact identifier

Properties
----------

.. csv-table::
   :header: I/O, R, #, key, type, value, note

   I, ✔, 1, representative_id, UUID, , Contact representative identifier

``ContactRepresentativeDelete``
===============================

A user has deleted contact representative.

References
----------

.. csv-table::
   :header: R, #, key, type, value, note

   , 1, representative, UUID, , Contact representative identifier

Properties
----------

.. csv-table::
   :header: I/O, R, #, key, type, value, note

   I, ✔, 1, representative_id, UUID, , Contact representative identifier


-------------------
Domain-related logs
-------------------

``DomainDetail``
================

A user has accessed basic domain information.

References
----------

.. csv-table::
   :header: R, #, key, type, value, note

   , 1, domain, UUID, , Domain identifier

Properties
----------

.. csv-table::
   :header: I/O, R, #, key, type, value, note

   I, ✔, 1, domain, UUID, , Domain identifier
   I,  , 1, history_id, UUID, , Domain history identifier
   I,  , 1, history_datetime, datetime, , Domain history at given point
   O,  , 1, found_history_id, UUID, , Domain history id found from time point

``DomainState``
===============

A user has accessed domain state or state history information.

References
----------

.. csv-table::
   :header: R, #, key, type, value, note

   , 1, domain, UUID, , Domain identifier

Properties
----------

.. csv-table::
   :header: I/O, R, #, key, type, value, note

   I, ✔, 1, domain, UUID, , Domain identifier
   I,  , 1, history, bool, \"true\", Whether user requested state history

``DomainHistory``
=================

A user has accessed domain history overview.

References
----------

.. csv-table::
   :header: R, #, key, type, value, note

   , 1, domain, UUID, , Domain identifier

Properties
----------

.. csv-table::
   :header: I/O, R, #, key, type, value, note

   I, ✔, 1, domain, UUID, , Domain identifier

``DomainSearch``
================

A user has run a search for domains.

References
----------

.. csv-table::
   :header: R, #, key, type, value, note

   , \*, domain, UUID, , Matching domains identifiers

Properties
----------

.. csv-table::
   :header: I/O, R, #, key, type, value, note

   I, ✔, \*, query_values, str, , Query values
   I,  , \*, search_item, str, , Domain items that should be searched for match

``DomainList``
==============

User accessed list of domains.

References
----------

.. csv-table::
   :header: R, #, key, type, value, note

   , 1, contact, str, , Contact linked to list of domains

Properties
----------

.. csv-table::
   :header: I/O, R, #, key, type, value, note

   I,  , 1, include_deleted, bool, , Whether to include deleted domains
   I,  , 1, order_by, str, , Domain list ordering
   I,  , 1, fqdn_filter, str, , Domain list fqdn filter
   I,  , 1, page_token, str, , Domain list pagination token
   I,  , 1, page_size, int, , Domain list pagination page size
   I,  , \*, roles, str, , Roles of contact in relation to linked domain

``DomainContactInfo``
=====================

User accessed list of domains with notify contact information.

Properties
----------

.. csv-table::
   :header: I/O, R, #, key, type, value, note

   I, ✔, 1, event, str, , Event to which are the contact information tied to.
   I, ✔, 1, start, datetime, , Start of the interval in which the event should occur
   I, ✔, 1, end, datetime, , End of the interval in which the event should occur
   I,  , 1, page_token, str, , Pagination token
   I,  , 1, page_size, int, , Pagination page size

``DomainContactInfoUpdate``
===========================

User updated list of domains with notify contact information.

References
----------

.. csv-table::
   :header: R, #, key, type, value, note

   ✔, \*, domain, str, , Domain id included in the update list

Properties
----------

.. csv-table::
   :header: I/O, R, #, key, type, value, note

   I, ✔, 1, event, str, , Event to which are the contact information tied to.
   I, ✔, 1, start, datetime, , Start of the interval in which the event should occur
   I, ✔, 1, end, datetime, , End of the interval in which the event should occur
   I, ✔, \*, domain, str, , Domain being updated with contact informations as child properties

------------------
Nsset-related logs
------------------

``NssetDetail``
===============

A user has accessed basic nsset information.

References
----------

.. csv-table::
   :header: R, #, key, type, value, note

   , 1, nsset, UUID, , Nsset identifier

Properties
----------

.. csv-table::
   :header: I/O, R, #, key, type, value, note

   I, ✔, 1, nsset, UUID, , Nsset identifier
   I,  , 1, history_id, UUID [#history_id_datetime]_, , Nsset history identifier
   I,  , 1, history_datetime, datetime, , Nsset history at given point
   O,  , 1, found_history_id, UUID, , Nsset history id found from time point

``NssetState``
==============

A user has accessed nsset state or state history information.

References
----------

.. csv-table::
   :header: R, #, key, type, value, note

   , 1, nsset, UUID, , Nsset identifier

Properties
----------

.. csv-table::
   :header: I/O, R, #, key, type, value, note

   I, ✔, 1, nsset, UUID, , Nsset identifier
   I,  , 1, history, bool, \"true\", Whether user requested state history

``NssetHistory``
================

A user has accessed nsset history overview.

References
----------

.. csv-table::
   :header: R, #, key, type, value, note

   , 1, nsset, UUID, , Nsset identifier

Properties
----------

.. csv-table::
   :header: I/O, R, #, key, type, value, note

   I, ✔, 1, nsset, UUID, , Nsset identifier


``NssetSearch``
===============

A user has run a search for nssets.

References
----------

.. csv-table::
   :header: R, #, key, type, value, note

   , \*, nsset, UUID, , Matching nssets identifiers

Properties
----------

.. csv-table::
   :header: I/O, R, #, key, type, value, note

   I, ✔, \*, query_values, str, , Query values
   I,  , \*, search_item, str, , Nsset items that should be searched for match


-------------------
Keyset-related logs
-------------------

``KeysetDetail``
===============

A user has accessed basic keyset information.

References
----------

.. csv-table::
   :header: R, #, key, type, value, note

   , 1, keyset, UUID, , Keyset identifier

Properties
----------

.. csv-table::
   :header: I/O, R, #, key, type, value, note

   I, ✔, 1, keyset, UUID, , Keyset identifier
   I,  , 1, history_id, UUID [#history_id_datetime]_, , Keyset history identifier
   I,  , 1, history_datetime, datetime, , Keyset history at given point
   O,  , 1, found_history_id, UUID, , Keyset history id found from time point

``KeysetState``
==============

A user has accessed keyset state or state history information.

References
----------

.. csv-table::
   :header: R, #, key, type, value, note

   , 1, keyset, UUID, , Keyset identifier

Properties
----------

.. csv-table::
   :header: I/O, R, #, key, type, value, note

   I, ✔, 1, keyset, UUID, , Keyset identifier
   I,  , 1, history, bool, \"true\", Whether user requested state history

``KeysetHistory``
================

A user has accessed keyset history overview.

References
----------

.. csv-table::
   :header: R, #, key, type, value, note

   , 1, keyset, UUID, , Keyset identifier

Properties
----------

.. csv-table::
   :header: I/O, R, #, key, type, value, note

   I, ✔, 1, keyset, UUID, , Keyset identifier


``KeysetSearch``
===============

A user has run a search for keysets.

References
----------

.. csv-table::
   :header: R, #, key, type, value, note

   , \*, keyset, UUID, , Matching keysets identifiers

Properties
----------

.. csv-table::
   :header: I/O, R, #, key, type, value, note

   I, ✔, \*, query_values, str, , Query values
   I,  , \*, search_item, str, , Keyset items that should be searched for match


----------------------
Registrar-related logs
----------------------

``RegistrarDetail``
===================

A user has accessed registrar detail.

References
----------

.. csv-table::
   :header: R, #, key, type, value, note

   , 1, registrar, str, , Registrar handle

Properties
----------

.. csv-table::
   :header: I/O, R, #, key, type, value, note

   I, ✔, 1, registrar_handle, str, , Registrar handle


``RegistrarList``
=================

A user has accessed registrar list.

References
----------

.. csv-table::
   :header: R, #, key, type, value, note

   , \*, registrar, str, , Registrar handles


``RegistrarCreditList``
=======================

A user has accessed list of registrars credit.

References
----------

.. csv-table::
   :header: R, #, key, type, value, note

   , \*, registrar, str, , Registrar handles


``RegistrarUpdate``
===================

A user has updated registrar.

References
----------

.. csv-table::
   :header: R, #, key, type, value, note

   , 1, registrar, str, , Registrar handle

Properties
----------

.. csv-table::
   :header: I/O, R, #, key, type, value, note

   I,  , 1, registrar_handle, str, , Registrar handle
   I,  , 1, name, str, , Registrar name
   I,  , 1, organization, str, , Registrar organization
   I,  , 1, place, Address, , Registrar address
   I,  , 1, telephone, str, , Phone number
   I,  , 1, fax, str, , Fax number
   I,  , \*, emails, str, , Email address
   I,  , 1, url, str, , Registrar url
   I,  , 1, is_system_registrar, bool, , Whether registrar is system registrar
   I,  , 1, company_registration_number, str, , Company registration number
   I,  , 1, vat_identification_number, str, , VAT identification number
   I,  , 1, variable_symbol, str, , Variable symbol
   I,  , 1, payment_memo_regex, str, , Payment memo regex
   I,  , 1, is_vat_payer, bool, , Whether registrar is VAT payer
   I,  , 1, is_internal, bool, , Whether registrar is internal


``RegistrarEppCredentialsView``
===============================

A user has viewed registrar epp credentials.

References
----------

.. csv-table::
   :header: R, #, key, type, value, note

   , 1, registrar, str, , Registrar handle


``RegistrarEppCredentialsCreate``
=================================

A user has created new registrar epp credentials.

References
----------

.. csv-table::
   :header: R, #, key, type, value, note

   , 1, registrar, str, , Registrar handle

Properties
----------

.. csv-table::
   :header: I/O, R, #, key, type, value, note

   I,  , 1, fingerprint, str, , Certificate fingerprint
   I,  , 1, template_credentials_id, str, , Template credentials id to copy password from

Content
-------

.. csv-table::
   :header: I/O, R, #, type, value, note

   I,  , 1, str, , Certificate body


``RegistrarEppCredentialsUpdate``
=================================

A user has updated registrar epp credentials.

References
----------

.. csv-table::
   :header: R, #, key, type, value, note

   , 1, registrar, str, , Registrar handle

Properties
----------

.. csv-table::
   :header: I/O, R, #, key, type, value, note

   I,  , 1, credentials_id, str, , Credentials id


``RegistrarEppCredentialsDelete``
=================================

A user has deleted registrar epp credentials.

References
----------

.. csv-table::
   :header: R, #, key, type, value, note

   , 1, registrar, str, , Registrar handle

Properties
----------

.. csv-table::
   :header: I/O, R, #, key, type, value, note

   I,  , 1, credentials_id, str, , Credentials id


``RegistrarGroupView``
======================

A user has viewed groups registrar belongs to.

References
----------

.. csv-table::
   :header: R, #, key, type, value, note

   , 1, registrar, str, , Registrar id

Properties
----------

.. csv-table::
   :header: I/O, R, #, key, type, value, note

   I, ✔, 1, registrar_id, str,  , Registrar id
   O,  , 1, items_count, int,  , Count of returned groups


``RegistrarGroupCreate``
========================

A user has added registrar to group.

References
----------

.. csv-table::
   :header: R, #, key, type, value, note

   , 1, registrar, str, , Registrar id

Properties
----------

.. csv-table::
   :header: I/O, R, #, key, type, value, note

   I, ✔, 1, registrar_id, str,  , Registrar id
   I, ✔, 1, group, str,  , Group registrar is being added to
   O,  , 1, items_count, int,  , Count of returned groups


``RegistrarGroupDelete``
========================

A user has deleted registrar from group.

References
----------

.. csv-table::
   :header: R, #, key, type, value, note

   , 1, registrar, str, , Registrar id

Properties
----------

.. csv-table::
   :header: I/O, R, #, key, type, value, note

   I, ✔, 1, registrar_id, str,  , Registrar id
   I, ✔, 1, group, str,  , Group registrar is being deleted from
   O,  , 1, items_count, int,  , Count of returned groups


``RegistrarCertificationView``
==============================

A user has viewed registrar certifications.

References
----------

.. csv-table::
   :header: R, #, key, type, value, note

   , 1, registrar, str, , Registrar id

Properties
----------

.. csv-table::
   :header: I/O, R, #, key, type, value, note

   I, ✔, 1, registrar_id, str,  , Registrar id
   O,  , 1, items_count, int,  , Count of returned certifications


``RegistrarCertificationCreate``
================================

A user has created new registrar certification.

References
----------

.. csv-table::
   :header: R, #, key, type, value, note

   , 1, registrar, str, , Registrar id

Properties
----------

.. csv-table::
   :header: I/O, R, #, key, type, value, note

   I, ✔, 1, registrar_id, str,  , Registrar id
   I, ✔, 1, classification, int,  , Classification
   I,  , 1, valid_from, datetime,  , Start of validity
   I,  , 1, valid_to, datetime,  , End of validity
   O,  , 1, file_id, str,  , Certification file id


``RegistrarCertificationUpdate``
================================

A user has updated registrar certification.

References
----------

.. csv-table::
   :header: R, #, key, type, value, note

   , 1, registrar, str, , Registrar id

Properties
----------

.. csv-table::
   :header: I/O, R, #, key, type, value, note

   I, ✔, 1, registrar_id, str,  , Registrar id
   I, ✔, 1, certification_id, str,  , Certification id
   I,  , 1, set_classification, int,  , New classification
   I,  , 1, set_valid_to, datetime,  , New end of validity
   O,  , 1, set_file_id, str,  , New file id


``RegistrarCertificationDelete``
================================

A user has deleted registrar certification.

References
----------

.. csv-table::
   :header: R, #, key, type, value, note

   , 1, registrar, str, , Registrar id

Properties
----------

.. csv-table::
   :header: I/O, R, #, key, type, value, note

   I, ✔, 1, registrar_id, str,  , Registrar id
   I, ✔, 1, certification_id, str,  , Certification id


--------------------
Message-related logs
--------------------

``EmailMessageList``
====================

A user has accessed list of email messages.

References
----------

.. csv-table::
   :header: R, #, key, type, value, note

   , \*, message, str, , Message UID

Properties
----------

.. csv-table::
   :header: I/O, R, #, key, type, value, note

   I,  , \*, recipient, str, , Recipient email address
   I,  , \*, message_type, str, , Email message type
   I,  , 1, created_from, datetime, , Lower limit on message creation date
   I,  , 1, created_to, datetime, , Upper limit on message creation date


``LetterMessageList``
=====================

A user has accessed list of letter messages.

References
----------

.. csv-table::
   :header: R, #, key, type, value, note

   , \*, message, str, , Message UID

Properties
----------

.. csv-table::
   :header: I/O, R, #, key, type, value, note

   I,  , \*, recipient, str, , Recipient address query string
   I,  , \*, message_type, str, , Letter message type
   I,  , 1, created_from, datetime, , Lower limit on message creation date
   I,  , 1, created_to, datetime, , Upper limit on message creation date


``SmsMessageList``
==================

A user has accessed list of SMS messages.

References
----------

.. csv-table::
   :header: R, #, key, type, value, note

   , \*, message, str, , Message UID

Properties
----------

.. csv-table::
   :header: I/O, R, #, key, type, value, note

   I,  , \*, recipient, str, , Recipient phone number
   I,  , \*, message_type, str, , SMS message type
   I,  , 1, created_from, datetime, , Lower limit on message creation date
   I,  , 1, created_to, datetime, , Upper limit on message creation date


-----------------
File-related logs
-----------------

``FileRead``
============

A user has accessed a file.

Properties
----------

.. csv-table::
   :header: I/O, R, #, key, type, value, note

   I,  , 1, uuid, str, , File uuid


``RecordStatement``
===================

A user has requested verified record statement.

References
----------

.. csv-table::
   :header: R, #, key, type, value, note

   ✔, 1, contact|domain|keyset|nsset, str, , Object identifier

---------
Footnotes
---------

.. [#history_id_datetime] Up to version 3.7.0, ``history_id`` property could contain either UUID or datetime.
