import i18next from 'i18next'
import Vue from 'vue'
import Vuetify from 'vuetify/lib'

import { get_vuetify_options, i18n, store } from 'FerdaJS/app.config'
import router from 'FerdaJS/router/logger'
import LoggerBaseView from 'ferda/logger/LoggerBaseView.vue'

i18next.on('initialized', () => {
    const logger_el = document.querySelector('#logger')
    if (logger_el) {
        new Vue({
            el: '#logger',
            name: 'LoggerList',
            vuetify: new Vuetify(get_vuetify_options()),
            i18n,
            store,
            router,
            render: createElement => createElement(LoggerBaseView),
        })
    }
})
