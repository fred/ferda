import equal from 'deep-equal'

import {
    CONTACT_ADDRESS_ITEMS,
    EVENT_MAIN_ITEMS,
    OBJECT_TYPE,
} from 'ferda/RegistryMixin'

export const HistoryMixin = {
    data() {
        return {
            __history_object_cache: {},
        }
    },

    methods: {
        shallow_diff(old_info, new_info) {
            return {
                differs: !equal(old_info, new_info),
                old_value: old_info,
                new_value: new_info,
            }
        },

        deep_diff(old_info, new_info) {
            if (typeof old_info !== typeof new_info) {
                return {
                    differs: true,
                    old_value: old_info,
                    new_value: new_info,
                }
            }
            if (Array.isArray(old_info) && Array.isArray(new_info))
                return this._array_diff(old_info, new_info)
            if (old_info instanceof Object && new_info instanceof Object)
                return this._object_diff(old_info, new_info)
            return {
                differs: old_info !== new_info,
                old_value: old_info,
                new_value: new_info,
            }
        },

        _array_diff(old_info, new_info) {
            const added = []
            const deleted = []
            const stayed = []
            const stayed_new_indexes = []
            for (let i = 0; i < old_info.length; i++) {
                let found_equal = false
                for (let j = 0; j < new_info.length; j++) {
                    if (equal(old_info[i], new_info[j])) {
                        stayed.push(old_info[i])
                        found_equal = true
                        stayed_new_indexes.push(j)
                        break
                    }
                }
                if (!found_equal)
                    deleted.push(old_info[i])
            }
            for (let j = 0; j < new_info.length; j++) {
                if (!stayed_new_indexes.includes(j))
                    added.push(new_info[j])
            }

            return {
                differs: added.length > 0 || deleted.length > 0,
                added: added,
                deleted: deleted,
                stayed: stayed,
                old_value: old_info,
                new_value: new_info,
            }
        },

        _object_diff(old_info, new_info) {
            const object_diff = {}
            const all_keys = new Set([...Object.keys(old_info), ...Object.keys(new_info)])
            for (const key of all_keys) {
                object_diff[key] = this.deep_diff(old_info[key], new_info[key])
            }
            object_diff.differs = Object.keys(object_diff).reduce((acc, x) => acc || object_diff[x].differs, false)
            return object_diff
        },

        _managed_object_diff(old_info, new_info) {
            if (!old_info || !new_info)
                return null

            const diff = this.deep_diff(old_info, new_info)

            for (const event of EVENT_MAIN_ITEMS) {
                if (old_info.events[event] || new_info.events[event]) {
                    diff.events[event] = this.shallow_diff(old_info.events[event], new_info.events[event])
                    diff.events[event].differs = this.events_differs(
                        diff.events[event].old_value,
                        diff.events[event].new_value,
                    )
                }
            }

            return diff
        },

        /**
         * Creates a contact diff
         *
         * @param { Object } old_info - old contact state
         * @param { Object } new_info - new contact state
         * @returns { Object } - diff between 2 states
         * @returns { Object } - null in case of non-existent old or new identifier
         */
        contact_diff(old_info, new_info) {
            if (!old_info || !new_info)
                return null

            const diff = this._managed_object_diff(old_info, new_info)

            diff.additional_identifier = this.shallow_diff(
                this.format_additional_identifier(old_info.additional_identifier),
                this.format_additional_identifier(new_info.additional_identifier),
            )

            for (const address of CONTACT_ADDRESS_ITEMS) {
                if (old_info[address] || new_info[address])
                    diff[address] = this.shallow_diff(old_info[address], new_info[address])
            }

            if (diff.warning_letter) {
                diff.warning_letter.old_value =
                    this.$t(`contact:warning_letter_preference.${old_info.warning_letter}`)
                diff.warning_letter.new_value =
                    this.$t(`contact:warning_letter_preference.${new_info.warning_letter}`)
            }

            if (diff.emails) {
                diff.emails.old_value = diff.emails.old_value.join(', ')
                diff.emails.new_value = diff.emails.new_value.join(', ')
            }

            if (diff.notify_emails) {
                diff.notify_emails.old_value = diff.notify_emails.old_value.join(', ')
                diff.notify_emails.new_value = diff.notify_emails.new_value.join(', ')
            }

            return diff
        },

        async domain_diff(old_domain, new_domain) {
            return this._managed_object_diff(old_domain, new_domain)
        },

        nsset_diff(old_nsset, new_nsset) {
            return this._managed_object_diff(old_nsset, new_nsset)
        },

        keyset_diff(old_keyset, new_keyset) {
            return this._managed_object_diff(old_keyset, new_keyset)
        },

        events_differs(event1, event2) {
            // Events are considered to be different if exactly one of them is not defined
            // or if they have different registrar handles. Different timestamps are not taken into account.
            if (!event1 && !event2)
                return false
            if (!event1 || !event2)
                return true
            return event1.registrar_handle !== event2.registrar_handle
        },

        format_additional_identifier(identifier) {
            if (identifier)
                return `${identifier.value} (${this.$t(`contact:identifier_type.${identifier.type}`)})`
            else
                return null
        },

        /**
         * Gets cached info about particular object
         *
         * @param { String } type - type of object
         * @param { String } uuid - object identifier
         * @param { String } history_spec - history identifier
         * @param { Boolean } is_datetime - whether history identifier is datetime or not
         * @returns { Object } - null in case of unspecified uuid
         * @returns { Object } - cached object info if exists
         */
        async get_object_info_cached(type, uuid, history_spec, is_datetime = true) {
            if (uuid === null)
                return null

            if (!this.$data.__history_object_cache[type])
                this.$data.__history_object_cache[type] = {}
            if (!this.$data.__history_object_cache[type][uuid])
                this.$data.__history_object_cache[type][uuid] = {}
            if (!this.$data.__history_object_cache[type][uuid][history_spec])
                this.$data.__history_object_cache[type][uuid][history_spec] =
                    await this.get_object_info(type, uuid, history_spec, is_datetime)
            return this.$data.__history_object_cache[type][uuid][history_spec]
        },

        sponsoring_registrar(old_info, new_info) {
            const registrar = {}

            if (old_info) {
                const old_registrar_handle = old_info.events && old_info.events.transferred &&
                old_info.events.transferred.registrar_handle
                registrar.old = { registrar_handle: old_registrar_handle }
            }
            if (new_info) {
                const new_registrar_handle = new_info.events && new_info.events.transferred &&
                new_info.events.transferred.registrar_handle
                registrar.new = { registrar_handle: new_registrar_handle }
            }
            return registrar
        },

        async get_contacts_history(contacts, old_datetime, new_datetime) {
            const contacts_history = []
            for (const contact of contacts.deleted)
                contacts_history.push({
                    old_value: this.get_object_info_cached(OBJECT_TYPE.CONTACT, contact.id, old_datetime),
                    new_value: null,
                    differs: true,
                })
            for (const contact of contacts.added)
                contacts_history.push({
                    old_value: null,
                    new_value: this.get_object_info_cached(OBJECT_TYPE.CONTACT, contact.id, new_datetime),
                    differs: true,
                })
            for (const contact of contacts.stayed)
                contacts_history.push({
                    old_value: this.get_object_info_cached(OBJECT_TYPE.CONTACT, contact.id, old_datetime),
                    new_value: this.get_object_info_cached(OBJECT_TYPE.CONTACT, contact.id, new_datetime),
                    differs: false,
                })
            for (const contact of contacts_history) {
                contact.old_value = await Promise.resolve(contact.old_value)
                contact.new_value = await Promise.resolve(contact.new_value)
            }
            return contacts_history
        },
    },
}
