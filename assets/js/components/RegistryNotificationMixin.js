export const CONTACT_TYPE = Object.freeze({ EMAIL: 'email', TELEPHONE: 'telephone' })
export const NOTIFICATION_TABLE_COLUMNS = Object.freeze({
    FQDN: 'fqdn',
    EMAIL: 'email',
    REGISTRANT_HANDLE: 'registrant_handle',
    CONTACTS: 'contacts',
    TELEPHONE: 'telephone',
})
export const PAGE_NUMBERS = Object.freeze([50, 100, 200, 300, 400, 500])
export const NOTIFICATION_PARAM = Object.freeze({
    NOTIFICATION_TYPE: 'notification_type',
    START: 'start',
    END: 'end',
    PAGE_SIZE: 'page_size',
    PAGE_TOKEN: 'page_token',
})
export const NOTIFICATION_TYPE = Object.freeze({ DELETE: 'delete', OUTZONE: 'outzone' })

export const RegistryNotificationMixin = {}
