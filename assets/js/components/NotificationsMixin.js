const get_default_message_object = () => {
    return {
        title: 'Message',
        type: 'info',
        duration: 7000,
    }
}

export const NotificationsMixin = {
    methods: {
        notify_message(message_object) {
            // Amend default poerties and create copy of message_object ($notify below is changing it)
            message_object = Object.assign(get_default_message_object(), message_object)

            // eslint-disable-next-line no-console
            console.log(`${message_object.type}: ${message_object.text}`)
            this.$notify(message_object)
        },
        clear_notifications() {
            this.$notify({ clean: true })
        },
    },
    created() {
        this.ERROR_SOMETHING_WENT_WRONG = {
            title: this.$t('notifications:operation_failed'),
            text: this.$t('notifications:something_went_wrong'),
            type: 'error',
            duration: -1,
        }
        this.INFO_REQUEST_ABORTED = {
            title: this.$t('notifications:search_abort'),
            text: this.$t('notifications:search_aborted'),
            type: 'info',
            duration: 7000,
        },
        this.CLEAR_NOTIFICATIONS = {
            clean: true,
        }
        this.SUCCESS_CHANGES_SAVED = {
            title: this.$t('notifications:success'),
            text: this.$t('notifications:changes_saved'),
            type: 'success',
            duration: 7000,
        }
    },
}
