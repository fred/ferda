/**
 * Waits for a specified period of time
 * @param { Number } time - The period of time to sleep (in miliseconds)
 */
export const sleep = time => new Promise((resolve) => setTimeout(resolve, time))

/**
 * Takes chunk and yields it's data row by row, if the chunk is cut in the middle of a row, it joins it
 * Warning: This function assumes data separation in chunks by new line
 * @param { Object } reader - Reader bound to chosen ReadableStream
 * @yields { String } - Generated row of a chunk
 */
export async function* generate_row_from_chunk(reader) {
    const decoder = new TextDecoder()
    let { value: chunk, done: reader_done } = await reader.read()
    chunk = chunk ? decoder.decode(chunk, { stream: true }) : ''

    const new_line_regex = /\r\n|\n|\r/gm
    let start_index = 0

    while (true) {
        const result = new_line_regex.exec(chunk)
        if (!result) {
            if (reader_done) break
            const remainder = chunk.substring(start_index)
            const read_result = await reader.read()
            chunk = read_result.value
            reader_done = read_result.done
            chunk = remainder + (chunk ? decoder.decode(chunk, { stream: true }) : '')
            start_index = 0
            new_line_regex.lastIndex = 0
            continue
        }
        // Wait for a while so that the performance is not exhausted
        await sleep(20)
        yield chunk.substring(start_index, result.index)
        start_index = new_line_regex.lastIndex
    }
    if (start_index < chunk.length) yield chunk.substring(start_index)
}

export const capitalize_string = input => input.charAt(0).toUpperCase() + input.slice(1)

/**
 * Trims value's beginning and ending space and filters out single spaces and falsy values
 *
 * @param { Array<String> } values - original values
 * @returns { Array<String> } - trimmed values
*/
export const trim_array = values => values.map(item => item.trim()).filter(Boolean)
