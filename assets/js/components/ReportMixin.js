import moment from 'moment-timezone'
import { mapGetters } from 'vuex'

import { ConsoleMixin } from './ConsoleMixin'
import { DatetimeMixin } from './DatetimeMixin'
import { HttpMixin } from './HttpMixin'

export const WIDGET_TYPE = Object.freeze({
    DATE: 'date',
    TIME: 'time',
    TEXT: 'text',
    TEXTAREA: 'textarea',
    NUMBER: 'number',
    CHECKBOX: 'checkbox',
    SELECT: 'select',
    DATETIME: 'datetime',
})

export const WIDGET_POSTGRESQL_TYPES = Object.freeze({
    [WIDGET_TYPE.DATE]: ['date'],
    [WIDGET_TYPE.DATETIME]: ['timestamp', 'timestamp without time zone', 'timestamp with time zone'],
    [WIDGET_TYPE.TIME]: ['time', 'time without time zone', 'time with time zone'],
    [WIDGET_TYPE.TEXT]: [
        'character varying',
        'varchar',
        'character',
        'char',
        'bytea',
        'inet',
        'cidr',
        'macaddr',
        'macaddr8',
    ],
    [WIDGET_TYPE.TEXTAREA]: ['text'],
    [WIDGET_TYPE.NUMBER]: [
        'smallint',
        'integer',
        'bigint',
        'decimal',
        'numeric',
        'real',
        'double precision',
        'smallserial',
        'serial',
        'bigserial',
    ],
    [WIDGET_TYPE.CHECKBOX]: ['boolean'],
    [WIDGET_TYPE.SELECT]: ['enum'],
})

export const DECIMAL_TYPES = Object.freeze(['numeric', 'decimal', 'real', 'double precision'])

export const TIME_ZONE_TYPES = ['timestamp with time zone', 'time with time zone']

const NAIVE_DATETIME_TYPES = Object.freeze([
    'timestamp without time zone', 'timestamp', 'time without time zone', 'time',
])

export const ReportMixin = {
    mixins: [ConsoleMixin, DatetimeMixin, HttpMixin],
    created() {
        this.WIDGET_TYPE = WIDGET_TYPE
        this.TIME_ZONE_TYPES = TIME_ZONE_TYPES
    },
    methods: {
        get_report_headers(report) {
            return report.output_parameters.map(param => ({
                text: param.label || param.name,
                value: param.name,
                divider: true,
                formatter: value => this.format_output_parameter(value, param.type),
                sort: DECIMAL_TYPES.includes(param.type) ? this.sort_decimals : null,
            }))
        },
        /**
         * Sorting method used when comparing decimal numbers.
         *
         * @param { String } a - value for compare
         * @param { String } b - value for compare
         * @returns { Number } - the result of comparing the two values
         */
        sort_decimals(a, b) {
            const float_a = parseFloat(a.replace(',', '.'))
            const float_b = parseFloat(b.replace(',', '.'))
            if (float_a > float_b) {
                return  1
            } else if (float_a === float_b) {
                return 0
            }
            return -1
        },
        format_output_parameter(value, output_parameter_type) {
            // Date might be null
            if (value === null) {
                return ''
            }

            // Value with time zone
            if (TIME_ZONE_TYPES.includes(output_parameter_type)) {
                const dt = moment.tz(new Date(value), this.TIME_ZONE).locale(this.selected_language)

                // Timestamp with time zone
                if (dt.isValid()) {
                    return this.iso_to_locale(value, true)
                }

                // Time with time zone
                return this.time_to_locale(value, true)
            }

            // Date
            if (WIDGET_POSTGRESQL_TYPES[WIDGET_TYPE.DATE].includes(output_parameter_type)) {
                return this.iso_to_locale_date(value)
            }

            // Datetime
            if (WIDGET_POSTGRESQL_TYPES[WIDGET_TYPE.DATETIME].includes(output_parameter_type)) {
                return this.iso_to_locale(value)
            }

            // Time
            if (WIDGET_POSTGRESQL_TYPES[WIDGET_TYPE.TIME].includes(output_parameter_type)) {
                return this.time_to_locale(value)
            }

            // Anything else
            return value
        },
        export_table_csv(table_data, report) {
            const { output_parameters, name } = report

            const csv_content = []

            csv_content.push(output_parameters.map(param => param.label || param.name).join(';') + '\n')
            for (const row of table_data) {
                csv_content.push(output_parameters.map(param => row[param.name]).join(';') + '\n')
            }

            const blob = new Blob(csv_content, { type: 'text/csv;charset=utf-8' })
            const url = URL.createObjectURL(blob)

            const link = document.createElement('a')
            link.setAttribute('href', url)
            link.setAttribute('download', `${name}.csv`)
            link.click()

            URL.revokeObjectURL(url)
        },
        get_widget_type(postgresql_type) {
            return Object.keys(WIDGET_POSTGRESQL_TYPES).find(
                key => WIDGET_POSTGRESQL_TYPES[key].includes(postgresql_type),
            )
        },
        parse_datetime(datetime_string) {
            const dt = moment
                .tz(datetime_string, this.TIME_ZONE)
                .locale(this.selected_language)

            const date_time = dt.format('D. MMM YYYY HH:mm:ss')
            const date = dt.format('YYYY-MM-DD')
            const time = dt.format('HH:mm:ss')

            return { date_time, date, time }
        },
        get_run_params(properties) {
            const params = new URLSearchParams()

            for (const key in properties) {
                params.append(key, properties[key])
            }

            return params
        },
        get_params_for_zone_conversion(input_params) {
            return input_params
                .filter(input_param => NAIVE_DATETIME_TYPES.includes(input_param.type))
                .map(input_param => input_param.name)
        },
        convert_naive_properties_to_aware_locals(properties, parameters) {
            const data = new Map(Object.entries(properties))
            const params_to_convert = this.get_params_for_zone_conversion(parameters)
            for (const [key, value] of data.entries()) {
                if (params_to_convert.includes(key)) {
                    if (this.is_valid_time(value)) data.set(key, this.naive_utc_time_to_aware_local_time(value, true))
                    else data.set(key, this.naive_utc_to_aware_local(value))
                }
            }
            return Object.fromEntries(data)
        },
        add_time_zone(value) {
            const new_value = moment.tz(new Date(value), this.TIME_ZONE)

            // timestamp with time zone
            if (new_value.isValid()) {
                return new_value.tz(this.TIME_ZONE).locale(this.selected_language).format()
            }

            // time with time zone
            return `${value}${this.get_time_zone_offset()}`
        },
        strip_time_zone_from_time(value) {
            return moment.parseZone(value, ['HH:mm:ssZ', 'HH:mm:ss']).utcOffset(0, true).format('HH:mm:ss')
        },
        strip_time_zone_from_datetime(value) {
            return moment
                .parseZone(value, ['YYYY-MM-DDTHH:mm:ss', 'YYYY-MM-DDTHH:mm:ssZ'])
                .utcOffset(0, true)
                .locale(this.selected_language)
                .format('ll LTS')
        },
    },
    computed: {
        ...mapGetters('settings', ['TIME_ZONE']),
        ...mapGetters('preferences', ['selected_language']),
    },
}
