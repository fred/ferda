import moment from 'moment-timezone'

import { DatetimeMixin } from './DatetimeMixin'

export const RuleMixin = {
    mixins: [DatetimeMixin],
    methods: {
        /**
         * Validates if specified value is filled in
         *
         * @returns { Array<Boolean> } true - Rule passed
         * @returns { Array<String> } error - Text with particular error
         */
        required_rule() {
            return [ val => {
                if (Array.isArray(val)) {
                    return !!val.length || this.$t('rule:field_required')
                }
                return !!val || this.$t('rule:field_required')
            }]
        },
        /**
         * Validates if value doesn't have invalid range
         *
         * @param { String } time_begin_from
         * @param { String } time_begin_to
         * @returns { Array<Boolean> } true - Rule passed
         * @returns { Array<String> } error - Text with particular error
         */
        less_than_rule(time_begin_from, time_begin_to) {
            if (time_begin_from === null || time_begin_to === null) {
                return [true]
            }
            return [
                () => this.is_less_than(time_begin_from, time_begin_to)
                || this.$t('rule:invalid_range'),
            ]
        },
        /**
         * Validates if values are both filled or both blank
         *
         * @param { String } first_value
         * @param { String } second_value
         * @returns { Array<Boolean> } true - Rule passed
         * @returns { Array<String> } error - Text with particular error
         */
        both_values_required_rule(first_value, second_value) {
            if ((!first_value && !second_value) || first_value && second_value) {
                return [true]
            }
            // Check value to show error message only at the empty box of the 2
            return [ val => !! val || this.$t('rule:field_required')]
        },
        /**
         * Validates if date is in specified range
         *
         * @param { String } time_begin_from - date
         * @param { String } time_begin_to- date
         * @returns { Boolean } true - Rule passed
         * @returns { String } error - Text with particular error
         */
        has_proper_range_rule(time_begin_from, time_begin_to) {
            return [
                () => this.is_less_than(time_begin_from, time_begin_to)
                || this.$t('rule:invalid_range'),
            ]
        },
        /**
         * Checks email validity
         *
         * @param { String | Array<String> } value - given email or array of emails
         * @param { Boolean } show_message - whether message should be shown or not
         * @returns { Array<Boolean> } - array with true of false
         * @returns { String } error - Text with particular error
         */
        email_rule(value, show_message = true) {
            const email_regex = /^[\w+]+([.-]?[\w+]+)*@\w+([.-]?\w+)*\.\w+(-?\w+){1,}$/
            const result = Array.isArray(value) ?
                value.every(email => email_regex.test(email)) : email_regex.test(value)
            return show_message ? [ result || this.$t('rule:invalid_email') ] : [ result ]
        },
        /**
         * Checks phone validity
         *
         * @param { String | Array<String> } value - given phone or array of phones
         * @param { Boolean } show_message - whether message should be shown or not
         * @returns { Array<Boolean> } - array with true of false
         * @returns { Array<String> } - Text with particular error
         */
        phone_rule(value, show_message = true) {
            const phone_regex = /^\+\d+\.[0-9]+$/
            const result = Array.isArray(value) ?
                value.every(phone => phone_regex.test(phone)) : phone_regex.test(value)
            return show_message ? [ result || this.$t('rule:invalid_phone') ] : [ result ]
        },
        /**
         * Validates if value is in the future
         *
         * @param { String } value - date
         * @returns { Array<Boolean> } true - Rule passed
         * @returns { Array<String> } error - Text with particular error
         */
        future_date_rule(value) {
            return [
                () => this.is_valid_future_date(value) || this.$t('rule:future_date'),
            ]
        },
        /**
         * Validates if value of valid_to isn't less than value of valid_from
         *
         * @param { String } valid_from - date
         * @param { String } valid_to - date
         * @returns { Array<Boolean> } true - Rule passed
         * @returns { Array<String> } error - Text with particular error
         */
        range_rule(valid_from, valid_to) {
            return [
                () => this.is_valid_range(valid_from, valid_to)
                || this.$t('rule:range_inzone'),
            ]
        },
        /**
         * Checks if value is not duplicite
         *
         * @param { Array } existing_values - all exist values
         * @param { String } compared_value - value to be compared
         * @returns { Array<Boolean> } true - Rule passed
         * @returns { Array<String> } error - Text with particular error
         */
        duplicite_rule(existing_values, compared_value) {
            return [ !existing_values.includes(compared_value) || this.$t('rule:duplicite_value') ]
        },
        /**
         * Checks whether the length of value is equal or less than maximum specified length
         *
         * @param { String } value - value to be checked
         * @param { Number } max_length - maximum value length
         * @returns { Array<Boolean> } - Rule passed
         * @returns { Array<String> } - Text with particular error
         */
        max_length_rule(value, max_length) {
            return [ value.length <= max_length || this.$t('rule:too_long', { max_length }) ]
        },
        /**
         * Checks whether the length of value is equal or greater than minimum specified length
         *
         * @param { String } value - value to be checked
         * @param { Number } min_length - minimum value length
         * @returns { Array<Boolean> } - Rule passed
         * @returns { Array<String> } - Text with particular error
         */
        min_length_rule(value, min_length) {
            return [ value.length >= min_length || this.$t('rule:too_short', { min_length }) ]
        },
        /**
         * Checks whether value matches against specified regex pattern
         *
         * @param { String } value - value to be checked
         * @param { String } regex - pattern to match against
         * @returns { Array<Boolean> } - Rule passed
         * @returns { Array<String> } - Text with particular error
         */
        regex_pattern_rule(value, regex) {
            return [ new RegExp(regex).test(value) || this.$t('rule:invalid_pattern') ]
        },
        /**
         * Checks whether value is valid URL
         *
         * @param { String } value - value to be checked
         * @returns { Array<Boolean> } - Rule passed
         * @returns { Array<String> } - Text with particular error
         */
        url_rule(value) {
            try {
                return [Boolean(new URL(value))]
            } catch {
                return [this.$t('rule:invalid_url')]
            }
        },
        /**
         * Checks whether value is valid ISO
         *
         * @param { String } input - user input
         * @returns { Array<Boolean> } - Rule passed
         * @returns { Array<String> } - Text with particular error
         */
        valid_iso_rule(input) {
            return [ moment(input, moment.ISO_8601).isValid() || this.$t('common:iso_format_expected') ]
        },
        /**
         * Checks if the value meets the range.
         *
         * @param { Number } value - specified value
         * @param { Number } min_value - minimum value
         * @param { Number } mmax_value - maximum value
         * @returns { Array<Boolean> } - Rule passed
         * @returns { Array<String> } - Text with particular error
         */
        number_range_rule(value, min_value, max_value) {
            return [ Number(value) >= min_value && Number(value) <= max_value
                || this.$t('rule:scope_value', { min_value, max_value }) ]
        },
        /**
         * Checks whether the uploaded file size doesn't exceed specified maximum
         *
         * @param { Number } max_size_bytes - maximum file size in bytes
         * @returns { Array<Boolean> } - Rule passed or file was deleted from input
         * @returns { Array<String> } - text with particular error
         */
        max_file_size_rule(max_size_bytes) {
            return [ file => {
                if (!file) {
                    // File was deleted from input
                    return true
                }
                return file.size <= max_size_bytes || this.$t('rule:file_too_large')
            }]
        },
        /**
         * Checks length of search query values for contact
         * Each word needs to have at least 3 characters or user has to choose to search in handle only
         *
         * @param { Array<string> } query_values - given query
         * @param { Array<string> } search_fields - chosen search fields
         * @returns { Array<Boolean> } - rule passed
         * @returns { Array<String> } - text with particular error
         */
        min_contact_search_length_rule(query_values, search_fields) {
            if (query_values.length === 0 || (search_fields.length === 1 && search_fields[0] === 'contact_handle')) {
                return [true]
            }
            return [
                query_values.every(query_value => query_value.split(' ').every(word => word.length >= 3)) ||
                    this.$t('rule:min_search_length'),
            ]
        },
    },
}
