import { URLS } from 'ferda/Urls'

import { ConsoleMixin } from './ConsoleMixin'
import { DATE_RANGE } from './DatetimeMixin'
import { HttpMixin } from './HttpMixin'
import { NotificationsMixin } from './NotificationsMixin'

export const MESSAGE_TYPE = Object.freeze({ EMAIL: 'email', SMS: 'sms', LETTER: 'letter' })
export const MESSAGE_FORM_FIELDS = Object.freeze({
    TIME_BEGIN_FROM: 'time_begin_from',
    TIME_BEGIN_TO: 'time_begin_to',
    MESSAGE_TYPE: 'type',
    RECIPIENTS: 'recipients',
    SUBTYPES: 'types',
    PAGE_SIZE: 'page_size',
    REFERENCES: 'references',
    DATE_RANGE: 'date_range',
})
export const MESSAGE_STATES = Object.freeze({
    PENDING: 0,
    SENT: 1,
    FAILED: 2,
    ERROR: 3,
    DELIVERED: 4,
    UNDELIVERED: 5,
    CANCELED: 6,
})
export const MESSAGE_STATE_ICONS = Object.freeze({
    [MESSAGE_STATES.PENDING]: 'email-sync',
    [MESSAGE_STATES.SENT]: 'email-arrow-right',
    [MESSAGE_STATES.DELIVERED]: 'email-check',
    [MESSAGE_STATES.FAILED]: 'email-alert',
    [MESSAGE_STATES.ERROR]: 'email-alert',
    [MESSAGE_STATES.UNDELIVERED]: 'email-arrow-left',
    [MESSAGE_STATES.CANCELED]: 'email-remove',
})
export const MESSAGE_STATE_COLORS = Object.freeze({
    [MESSAGE_STATES.PENDING]: 'warning',
    [MESSAGE_STATES.SENT]: 'success',
    [MESSAGE_STATES.DELIVERED]: 'success',
    [MESSAGE_STATES.FAILED]: 'error',
    [MESSAGE_STATES.ERROR]: 'error',
    [MESSAGE_STATES.UNDELIVERED]: 'error',
    [MESSAGE_STATES.CANCELED]: 'grey',
})
export const PAGE_NUMBERS = Object.freeze([5, 10, 20, 50])

export const MessagesMixin = {
    mixins: [ConsoleMixin, HttpMixin, NotificationsMixin],
    created() {
        this.MESSAGE_TYPE = MESSAGE_TYPE
        this.DEFAULT_DATE_RANGE = DATE_RANGE.CURRENT_MONTH
    },
    computed: {
        /**
         * Displays current results page
         *
         * @param { Object } messages
         * @returns Array
         */
        search_results_page(messages) {
            const begin = (messages.current_page - 1) * messages.page_size
            const end = messages.current_page * messages.page_size
            return messages.messages.slice(begin, end)
        },
    },
    methods: {
        /**
         * Fetches message subtypes according to given message type
         *
         * @param { String } message_type - Email, sms or letter
         * @returns { null } - In case of server error
         * @returns { Object } - Message subtypes
         */
        async get_message_subtypes(message_type) {
            let result
            try {
                result = await this.http_get(URLS.message_subtypes(message_type))
            } catch (error) {
                const error_message = this.is_http_cancel(error) ?
                    this.INFO_REQUEST_ABORTED : this.ERROR_SOMETHING_WENT_WRONG
                this.notify_message(error_message)
                this.log_error(error)
                return null
            }
            return result.data
        },
        /**
        * Fetches messages from API
        *
        * @param { Object | null } message_params - Params for request
        * @param { Number | null } page_size - Number of new page
        * @param { Object | null } abort_controller
        * @param { String | null } url - String = pagination call, null = form submission
        * @returns { Object } null - In case of server error or abort request
        * @returns { Object } results, next - messages and URLs for pagination
        */
        async get_message_list(message_params, page_size, abort_controller, url = null) {
            let result
            try {
                result = await this.http_get(URLS.message_list(message_params.message_type), {
                    params: this.get_message_list_url_params(message_params, page_size, url),
                    signal: abort_controller.signal,
                })
            } catch (error) {
                const error_message = this.is_http_cancel(error) ?
                    this.INFO_REQUEST_ABORTED : this.ERROR_SOMETHING_WENT_WRONG
                this.notify_message(error_message)
                this.log_error(error)
                return null
            }
            const { results, next } = result.data
            return { results, next }
        },
        /**
        * Sets URL parameters for API call. If url is passed, it means pagination was called, and therefore the params
        * are kept from the pagination URL and only page_size is updated accordingly
        *
        * @param { Object | null } message_params - Params for request
        * @param { Number | null } page_size - Number of new page
        * @param { String | null } url - String = pagination call, null = form submission
        * @returns { Object } - URLSearchParams - Prepared params for API call
        */
        get_message_list_url_params(message_params, page_size, url = null) {
            const params = new URLSearchParams(url ? new URL(url).search : undefined)
            if (url === null) {
                if (message_params.time_begin_from) {
                    params.set(MESSAGE_FORM_FIELDS.TIME_BEGIN_FROM, message_params.time_begin_from)
                }
                if (message_params.time_begin_to) {
                    params.set(MESSAGE_FORM_FIELDS.TIME_BEGIN_TO, message_params.time_begin_to)
                }
                for (const subtype of message_params.selected_subtypes) {
                    params.append(MESSAGE_FORM_FIELDS.SUBTYPES, subtype)
                }
                if (message_params.recipients) {
                    for (const recipient of message_params.recipients) {
                        params.append(MESSAGE_FORM_FIELDS.RECIPIENTS, recipient)
                    }
                }
                if (message_params.references) {
                    params.set(MESSAGE_FORM_FIELDS.REFERENCES, message_params.references)
                }
            }
            params.set(MESSAGE_FORM_FIELDS.PAGE_SIZE, page_size)
            return params
        },
        /**
         * Generates special caption for letter, returns default data for email and sms
         *
         * @param { String } last_search_message_type - Last searched message type
         * @param { Object } recipient - Recipient to generate caption for
         * @returns { String } - generated recipient caption
         */
        get_recipient_caption(last_search_message_type, recipient) {
            if (last_search_message_type === MESSAGE_TYPE.LETTER) {
                return [recipient.organization, recipient.name].filter(Boolean).join(', ')
            }
            return recipient
        },
        /**
         * Compares message subtypes labels
         *
         * @param { Object } a - compared value
         * @param { Object } b - compared value
         * @returns { Number } - determine the order values
         */
        compare_subtypes_label(a, b) {
            return a.label.localeCompare(b.label)
        },
        /**
         * Filters name and label from message subtypes
         *
         * @param { Object } item
         * @param { String } query_text - search text
         * @returns { Boolean } - if a match exists
         */
        filter_subtypes(item, query_text) {
            const search_text = query_text.toLowerCase()
            return item.text.toLowerCase().includes(search_text)
            || item.value.toLowerCase().includes(search_text)
        },
        /**
         * Finds the desired label
         *
         * @param { String } subtype - name of subtype
         * @param { Array<String> } labels - list of subtypes
         * @returns { String } search label
         */
        get_matching_label(subtype, labels) {
            if (!subtype || !labels.length) {
                return null
            }
            const found_label = labels.find(({name}) => subtype === name)
            return found_label?.label || null
        },
        /**
         * Highlights subtypes query if provided by user
         *
         * @param { String } subtype_text - value to match against
         * @param { String } query - input from user
         * @returns { String } - whole subtype_text if no query is provided
         * @returns { String } - whole subtype_text if nothing matched
         * @returns { String } - string with higlighted, matched part
         */
        highlight_subtypes(subtype_text, query) {
            // User just clicks to the menu
            if (query === null) return subtype_text

            // Nothing matched
            const index = subtype_text.toLowerCase().indexOf(query)
            if (index < 0) return subtype_text

            // Chop it to 3 pieces - middle is the match
            const start = subtype_text.slice(0, index)
            const middle = subtype_text.slice(index, index + query.length)
            const end = subtype_text.slice(index + query.length)

            return `${start}<span class="v-list-item__mask">${middle}</span>${end}`
        },
    },
}
