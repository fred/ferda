<template>
    <VMenu
        v-model="show_menu"
        :close-on-content-click="false"
        transition="scale-transition"
        offset-y
    >
        <template v-slot:activator="{ on, attrs }">
            <VTextField
                :label="label"
                :outlined="outlined"
                :value="display_value"
                :class="inner_class"
                :rules="rules"
                :error-messages="error_messages"
                :hide-details="hide_details"
                :clearable="clearable"
                :hint="$t('common:iso_format_expected')"
                :dense="dense"
                clear-icon="close-circle"
                @focus="reset_date"
                @blur="change_date"
            >
                <template v-slot:prepend>
                    <VBtn
                        icon
                        :color="icon_color"
                        small
                        v-bind="attrs"
                        class="tref-date-icon"
                        @click="open"
                        v-on="on"
                    >
                        <FerdaIcon icon="calendar" size="24"/>
                    </VBtn>
                </template>
                <template v-slot:append>
                    <FerdaIcon :icon="append_icon" size="24"/>
                </template>
            </VTextField>
        </template>
        <VRow :class="$style.dateTimeWidget">
            <VCol cols="12" lg="6" md="6">
                <VDatePicker
                    elevation="3"
                    :class="[$style.picker, 'tref-date-picker']"
                    :value="date"
                    :reactive="true"
                    :picker-date="date"
                    :active-picker.sync="active_picker"
                    @update:picker-date="update_value($event, 'date')"
                    @change="update_value($event, 'date')"
                />
            </VCol>
            <VCol cols="12" lg="6" md="6">
                <VTimePicker
                    ref="time_picker"
                    elevation="3"
                    :class="[$style.picker, 'tref-time-picker']"
                    :use-seconds="true"
                    :value="time"
                    format="24hr"
                    @input="update_value($event, 'time')"
                />
            </VCol>
        </VRow>
        <VRow class="text-right white py-2 px-8" no-gutters>
            <VCol cols="12">
                <VBtn text color="secondary" class="tref-prefill-now" @click="prefill_now_datetime">
                    {{ $t('common:now') }}
                </VBtn>
                <VBtn text color="primary" class="tref-save-date" :disabled="!is_valid" @click="save">
                    {{ $t('common:save') }}
                </VBtn>
            </VCol>
        </VRow>
    </VMenu>
</template>

<script>
import { ConsoleMixin } from 'ferda/ConsoleMixin'
import { DatetimeMixin } from 'ferda/DatetimeMixin'
import { NotificationsMixin } from 'ferda/NotificationsMixin'
import { ReportMixin } from 'ferda/ReportMixin'
import FerdaIcon from 'ferda/common/FerdaIcon.vue'

export default {
    name: 'DateTimePickerMenu',
    components: { FerdaIcon },
    mixins: [ConsoleMixin, DatetimeMixin, NotificationsMixin, ReportMixin],
    props: {
        value: {
            type: String,
            default: null,
        },
        label: {
            type: String,
            default: null,
        },
        rules: {
            type: Array,
            default: () => [],
        },
        name: {
            type: String,
            default: null,
        },
        inner_class: {
            type: Array,
            default: () => [],
        },
        with_time_zone: {
            type: Boolean,
            default: () => false,
        },
        outlined: {
            type: Boolean,
            default: () => false,
        },
        append_icon: {
            type: String,
            default: () => null,
        },
        clearable: {
            type: Boolean,
            default: () => false,
        },
        mount_date: {
            type: String,
            default: () => null,
        },
        icon_color: {
            type: String,
            default: () => 'grey darken-1',
        },
        hide_details: {
            type: [String, Boolean],
            default: () => 'auto',
        },
        dense: {
            type: Boolean,
            default: () => false,
        },
    },
    data() {
        return {
            active_picker: null,
            show_menu: false,
            date: null,
            time: null,
            date_time: null,
            error_messages: [],
        }
    },
    computed: {
        /**
         * Checks whether input is valid
         *
         * @returns { Boolean } - Whether input is valid or not
         */
        is_valid() {
            return this.date !== null && this.time !== null
        },
        /**
         * Displays locale value
         *
         * @returns { null } - In case input is empty
         * @returns { String } - Locale value without timezone if component serves for such input
         * @returns { String } - Locale value with time zone appended at the end
         */
        display_value() {
            const { date_time } = this.parse_datetime(this.value)
            if (this.value === null) return ''
            if (!this.with_time_zone)
                return this.strip_time_zone_from_datetime(this.naive_utc_to_aware_local(this.value))

            return `${date_time} ${this.get_time_zone_abbr()}`
        },
    },
    watch: {
        /**
         * Watches for value changes and deletes it in case input is empty
         *
         * @param { String | null } new_value - Value to which the component should update
         * @event change - Deletes value in case input is empty
         */
        value(new_value) {
            if (new_value === null) {
                this.$emit('change', null)
                this.date = null
                this.time = null
                this.date_time = null
            } else {
                const dt = this.iso_to_moment(new_value)
                this.date = dt.format('YYYY-MM-DD')
                this.time = dt.format('HH:mm:ss')
            }
        },
    },
    mounted() {
        const params = new URLSearchParams(location.search)
        const { date_time, date, time } = this.parse_datetime(
            this.naive_utc_to_aware_local(params.get(this.name) || this.now().startOf('day').format()),
        )
        this.date_time = date_time
        this.date = date
        this.time = time

        this.$emit(
            'change',
            this.mount_date ? this.mount_date : this.get_datetime_string(this.date, this.time, this.with_time_zone),
        )
    },
    methods: {
        /**
         * Resets date when user focuses the box.
         *
         * @event update:value - updates value
         * @event change - emits additional event in case it is needed
         */
        reset_date() {
            this.$emit('update:value', null)
            this.$emit('change', null)
        },
        /**
         * Updates date-time
         *
         * @param { String } new_value - Value to which the component should update
         * @param { String } type - date / time
         */
        update_value(new_value, type) {
            this[type] = new_value
        },
        /**
         * Changes date
         *
         * @param { String } input_field - Value to be updated to
         * @returns { void } - In case date is empty
         * @returns { void } - In case value is not changed
         * @returns { void } - In case of errors
         * @event update:value - Directly updates value with 2 way binding
         * @event change - Additional event emitted in case it is needed
         */
        change_date(input_field) {
            this.error_messages = []
            const result = this.validate_datetime_input(input_field.target.value, this.display_value)

            // Date can be empty
            if (result === null) {
                this.$emit('update:value', null)
                this.$emit('change', null)
                return
            }

            // Input was just blured, not changed or changed to the same string
            if (result === false) {
                return
            }

            // Input can be invalid ISO
            if (Array.isArray(result)) {
                this.error_messages = result
                return
            }

            const dt = this.iso_to_moment(result).utc()
            this.date = dt.format('YYYY-MM-DD')
            this.time = dt.format('HH:mm:ss')
            this.save()
        },
        /**
         * Saves selected date and time
         *
         * @event update:value - Directly updates value with 2 way binding
         * @event change - Additional event emitted in case it is needed
         */
        save() {
            // If widget handles naive datetime, convert it to UTC, but don't show the conversion to the user
            const dt_string = this.get_datetime_string(this.date, this.time, this.with_time_zone, false)
            const { date_time } = this.parse_datetime(dt_string)

            this.date_time = date_time
            this.$emit('update:value', dt_string)

            // If widget handles naive datetime, convert it to UTC, otherwise use particular zone
            this.$emit('change', this.with_time_zone ? dt_string : this.naive_local_to_naive_utc(dt_string))
            this.show_menu = false
            this.error_messages = []
        },
        /**
         * Prefills field with current date-time
         */
        prefill_now_datetime() {
            const dt = this.now()
            this.date = dt.format('YYYY-MM-DD')
            this.time = dt.format('HH:mm:ss')
        },
        /**
         * Sets default values to the menu when opened
         */
        async open() {
            if (this.time === null) this.time = '00:00:00'
            if (this.date === null) this.date = this.now().format('YYYY-MM-DD')

            await this.$nextTick()
            this.$refs.time_picker.selecting = 1
            this.active_picker = 'DATE'
        },
        /**
         * Returns stringified ISO date
         *
         * @param { String } date - Given date value
         * @param { String } time - Given time value
         * @param { Boolean } with_time_zone - Whether time zone is included
         * @param { Boolean } make_naive_aware_conversion - Whether to convert into utc or not
         * @returns { String } - Stringified ISO value
         */
        get_datetime_string(date, time, with_time_zone, make_naive_aware_conversion = true) {
            const dt_string = [this.get_iso_date(date), time].filter(Boolean).join('T')
            if (with_time_zone) {
                return this.add_time_zone(dt_string)
            }
            if (make_naive_aware_conversion) {
                return this.naive_local_to_naive_utc(dt_string)
            }
            return dt_string
        },
    },
}
</script>

<style module lang="scss">
.dateTimeWidget {
    background-color: #fff;
    text-align: center;
    padding: 1em;

    .picker {
        height: 100%;
    }
}
</style>
