import axios from 'axios'
export const HTTP_ERRORS = Object.freeze({ ABORT_ERROR: 'AbortError' })

export const HttpMixin = {
    methods: {
        /**
         * Makes HTTP GET request
         *
         * @param { String } url - GET request url
         * @param { Object } config - additional config
         * @param { Array<Number> } additional_status_codes - additional accepted status codes
         * @returns { Object } - axios promise
         */
        async http_get(url, config = {}, additional_status_codes = []) {
            config.validateStatus = status => this.validate_status(status, additional_status_codes)
            return axios.get(url, config)
        },
        /**
         * Makes HTTP POST request
         *
         * @param { String } url - POST request url
         * @param { Object } data - data sent with request
         * @param { Object } config - additional config
         * @param { Array<Number> } additional_status_codes - additional accepted status codes
         * @returns { Object } - axios promise
         */
        async http_post(url, data = {}, config = {}, additional_status_codes = []) {
            config.validateStatus = status => this.validate_status(status, additional_status_codes)
            return axios.post(url, data, config)
        },
        /**
         * Makes HTTP PUT request
         *
         * @param { String } url - PUT request url
         * @param { Object } data - data sent with request
         * @param { Object } config - additional config
         * @param { Array<Number> } additional_status_codes - additional accepted status codes
         * @returns { Object } - axios promise
         */
        async http_put(url, data = {}, config = {}, additional_status_codes = []) {
            config.validateStatus = status => this.validate_status(status, additional_status_codes)
            return axios.put(url, data, config)
        },
        /**
         * Makes HTTP DELETE request
         *
         * @param { String } url - DELETE request url
         * @param { Object } config - additional config
         * @param { Array<Number> } additional_status_codes - additional accepted status codes
         * @returns { Object } - axios promise
         */
        async http_delete(url, config, additional_status_codes = []) {
            config.validateStatus = status => this.validate_status(status, additional_status_codes)
            return axios.delete(url, config)
        },
        /**
         * Gets abort controller
         *
         * @returns { Object } - new abort controller
         */
        get_abort_controller() {
            return new AbortController()
        },
        /**
         * Aborts ongoing request
         *
         * @param { Object } abort_controller - abort controller instance
         */
        async abort_request(abort_controller) {
            await abort_controller.abort()
        },
        /**
         * Takes catched error and decides whether it is abort error or not
         * It is able to detect both axios and fetch abort errors
         *
         * @param { Object } error - Catched error
         * @returns { Boolean } true / false - Whether it is abort error or not
         */
        is_http_cancel(error) {
            return axios.isCancel(error) || error.name === HTTP_ERRORS.ABORT_ERROR
        },
        /**
         * Validates status whether it is success or fail
         *
         * @param { Number } status - result of request
         * @param { Array<Number> } additional_status_codes - additional accepted status codes
         */
        validate_status(status, additional_status_codes) {
            return (status >= 200 && status < 300) || additional_status_codes.includes(status)
        },
    },
}
