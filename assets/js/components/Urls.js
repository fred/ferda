export const URLS = Object.freeze({
    additional_delete_contacts_ajax: () => '/api/registry/domain/-/get_additional_delete_contacts/',
    additional_delete_contacts_result_ajax: () => '/api/registry/domain/-/update_additional_delete_contacts/',
    additional_outzone_contacts_ajax: () => '/api/registry/domain/-/get_additional_outzone_contacts/',
    additional_outzone_contacts_result_ajax: () => '/api/registry/domain/-/update_additional_outzone_contacts/',
    app_admin: () => '/admin/',
    countries_list: () => '/countries/',
    contact_representative_add_ajax: () => '/api/registry/representative/',
    contact_representative_by_contact_id_ajax: contact_id => `/api/registry/representative/${contact_id}/contact/`,
    contact_representative_update_delete_ajax:
        representative_id => `/api/registry/representative/${representative_id}/`,
    django_settings_ajax: () => '/api/common/settings/',
    domain_detail: domain_id => `/registry/domain/${domain_id}/`,
    domain_website: domain_id => `https://${domain_id}/`,
    domains_by_contact_ajax: contact_id => `/api/registry/domain/-/by_contact/${contact_id}/`,
    email_attachment: attachment_uuid => `/files/${attachment_uuid}/`,
    history_search_ajax: (object_type, params) => `/api/registry/${object_type}/-/search/history/?${params.toString()}`,
    iana_keyset_protocol: () => 'https://www.iana.org/assignments/dns-key-rr/dns-key-rr.xhtml',
    keyset_detail: keyset_id => `/registry/keyset/${keyset_id}/`,
    lang_setting: () => '/i18n/setlang/',
    log_detail_ajax: log_id => `/api/logger/log_entry/${encodeURIComponent(log_id)}/`,
    log_detail_view: log_id => `/logger/${encodeURIComponent(log_id)}/`,
    log_entries: () => '/api/logger/log_entry/',
    log_entry_types: service => `/api/logger/service/${service}/`,
    log_list_view: () => '/logger/',
    log_reference: (key_value, uuid) => `/registry/${key_value}/${uuid}/`,
    log_services: () => '/api/logger/service/',
    login_url: () => `/accounts/login/${location.search}`,
    logout_url: () => '/accounts/logout/',
    manual_in_zone: domain_id => `/api/registry/domain/-/set_manual_in_zone/${domain_id}/`,
    message_detail_ajax: (message_id, message_type) => `/api/messages/${message_type}/${message_id}/`,
    message_detail_view: (message_id, message_type) => `/messages/${message_type}/${message_id}/`,
    message_list: message_type => `/api/messages/${message_type}/`,
    message_list_by_view: params => `/messages/list/?${params.toString()}`,
    message_list_view: () => '/messages/list/',
    message_subtypes: message_type => `/api/messages/subtypes/?message_types=${message_type}`,
    notification_view: () => '/notification/',
    nsset_detail: nsset_id => `/registry/nsset/${nsset_id}/`,
    object_detail: (object_type, object_id) => `/registry/${object_type}/${object_id}/`,
    object_history: (object_type, object_id) => `/registry/${object_type}_history/${object_id}/`,
    object_info_ajax: (object_type, object_id) => `/api/registry/${object_type}/${object_id}/`,
    object_info_history_ajax: (object_type, object_id, history_spec) =>
        `/api/registry/${object_type}/${object_id}/history/${history_spec}/`,
    object_info_history_ajax_datetime: (object_type, object_id, datetime) =>
        `/api/registry/${object_type}/${object_id}/history/by_datetime/${datetime}/`,
    object_state_ajax: (object_type, object_id) => `/api/registry/${object_type}/${object_id}/state/`,
    object_state_history_ajax: (object_type, object_id) => `/api/registry/${object_type}/${object_id}/state/history/`,
    record_statement: (object_type, object_handle) =>
        `/api/registry/${object_type}/-/record_statement/${object_handle}/`,
    registrar_available_group_ajax: () => '/api/registry/registrar/-/groups/',
    registrar_certificate_get_create_ajax: registrar_handle =>
        `/api/registry/registrar/${registrar_handle}/epp_credentials/`,
    registrar_certificate_update_ajax: (registrar_handle, id) =>
        `/api/registry/registrar/${registrar_handle}/epp_credentials/${id}/`,
    registrar_certification_ajax: registrar_handle => `/api/registry/registrar/${registrar_handle}/certification/`,
    registrar_certification_file: (registrar_id, file_id) =>
        `/api/registry/registrar/${registrar_id}/certification_file/${file_id}/`,
    registrar_update_certification_ajax: (registrar_handle, certification_id) =>
        `/api/registry/registrar/${registrar_handle}/certification/${certification_id}/`,
    registrar_create: () => '/api/registry/registrar/',
    registrar_detail_ajax: registrar_handle => `/api/registry/registrar/${registrar_handle}/`,
    registrar_detail_view: registrar_handle =>  `/registry/registrar/${encodeURIComponent(registrar_handle)}/`,
    registrar_list_ajax: () => '/api/registry/registrar/',
    registrar_list: () => '/registry/registrar/',
    registrar_manage_group_ajax: (registrar_id, group) => `/api/registry/registrar/${registrar_id}/group/${group}/`,
    registrar_membership_group_ajax: registrar_id => `/api/registry/registrar/${registrar_id}/group/`,
    registrar_update_ajax: registrar_handle => `/api/registry/registrar/${registrar_handle}/`,
    registrar_zone_access: handle => `/api/registry/registrar/${handle}/zone_access/`,
    registry_search: () => '/registry/search/',
    registry_search_ajax: (object_type, params) => `/api/registry/${object_type}/-/search/?${params.toString()}`,
    related_domains: contact_id => `/registry/contact/${contact_id}/?domains=true`,
    related_emails: (object_type, object_id) => `/registry/${object_type}/${object_id}/?emails=true`,
    related_logs: params => `/logger/?${params.toString()}`,
    report_detail: (service, name) => `/reports/detail/${service}/${name}/`,
    report_list_view: () => '/reports/list/',
    report_run_result: (service, name) => `/reports/ajax/run/${service}/${name}/`,
    user_settings_ajax: () => '/api/common/user/',
})
