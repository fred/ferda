export default {
    methods: {
        condense_address(address) {
            if (!address) {
                return undefined
            } else {
                return [
                    address.organization,
                    address.company,
                    address.name,
                    Array.isArray(address.street) ? address.street.join(', ') : address.street,
                    address.city,
                    address.postal_code,
                    address.state_or_province,
                    address.country_code ? address.country_code : address.country,
                ].filter(item => item).join(', ')
            }
        },
    },
}
