export const LOG_FORM_FIELDS = Object.freeze({
    TIME_BEGIN_FROM: 'time_begin_from',
    TIME_BEGIN_TO: 'time_begin_to',
    SERVICE: 'service',
    LOG_ENTRY_TYPES: 'log_entry_types',
    PAGE_SIZE: 'page_size',
    USERNAME: 'username',
    REFERENCE: 'reference',
    PROPERTIES: 'properties',
    OFFSET: 'offset',
    DATE_RANGE: 'date_range',
})

import { HttpMixin } from 'ferda/HttpMixin'
import { URLS } from 'ferda/Urls'

export const LoggerMixin = {
    mixins: [HttpMixin],
    methods: {
        /**
        * Loads all available services from API
        *
        * @returns { Array } data - Array of all available services
        */
        async get_services() {
            let result
            try {
                result = await this.http_get(URLS.log_services())
            } catch (error) {
                // Leave the implementation of error to particular places
                // It may handle various data manipulation (E.g. loading state)
                throw new Error(error)
            }
            return result.data
        },
    },
}
