import { CONTACT_ADDRESS_ITEMS, OBJECT_TYPE } from 'ferda/RegistryMixin'

export const OBJECT_TITLE_ITEM = Object.freeze({
    CONTACT: 'contact_handle',
    DOMAIN: 'fqdn',
    NSSET: 'nsset_handle',
    KEYSET: 'keyset_handle',
})

export const SearchMixin = {
    created() {
        this.OBJECT_TITLE_ITEM = OBJECT_TITLE_ITEM
    },
    methods: {
        /**
         * Returns keys in which the actual search matched particular item
         *
         * @param { Object } result - found search result
         * @returns { Array<String> } - matched items
         */
        get_match_keys(result) {
            if (result.matched_items) {
                return result.matched_items
            }

            if (result.histories) {
                const last_match = result.histories[result.histories.length - 1]
                if (!last_match.valid_to) {
                    return last_match.matched_items
                }

                if (result.last_history.events.unregistered?.timestamp === last_match.valid_to) {
                    return last_match.matched_items
                }
            }
            return []
        },
        /**
         * Gets matched addresses of a contact
         *
         * @param { Object } result - found search result
         * @returns { Object } - object with matched label and address items
         */
        matched_addresses(result) {
            return CONTACT_ADDRESS_ITEMS
                .filter(item => result.matched_items.some(match => match.includes(item)))
                .map(item => (
                    { type: item, label: this.$t(`contact:${item}`), address: this.address_items(result, item) || [] }),
                )
        },
        /**
         * Extracts address items from matched search result
         *
         * @param { Object } result - found search result
         * @param { String } name - name of the field to look for
         * @returns { Object } - null in case address is not present
         * @returns { Array<Object> } - parsed address parts with the info whether they match
         */
        address_items(result, name) {
            const address = result.contact[name]
            if (!address) {
                return null
            }

            return [
                this.parse_address_item(result, name, 'street', address.street.join(', ')),
                this.parse_address_item(result, name, 'city'),
                this.parse_address_item(result, name, 'postal_code'),
                this.parse_address_item(result, name, 'state_or_province'),
                this.parse_address_item(result, name, 'country_code'),
            ].filter(Boolean)
        },
        /**
         * Parses the part of given address
         *
         * @param { Object } result - found search result
         * @param { String } name - name of the field to look for
         * @param { String } part - part of the address
         * @param { String } value - value of the part
         * @returns { Object } - value of the part with the info whether the part matches
         */
        parse_address_item(result, name, part, value = null) {
            return {
                is_matching: result.matched_items.includes(`${name}.${part}`),
                value: value === null ? result.contact[name][part] : value,
            }
        },
        /**
         * Gets the label of matched item
         *
         * @param { String } match - name of the match
         * @param { String } object_type - type of the object
         * @param { Boolean } verbose - how to display the result
         * @returns { String } - label for particular item
         */
        match_item_label(match, object_type, verbose = true) {
            const item = match.split('.').pop()
            if (object_type === OBJECT_TYPE.CONTACT) {
                const contact_item = match.split('.')[0]
                if (contact_item === 'additional_identifier') {
                    if (verbose) {
                        return `${this.$t(`contact:${contact_item}`)} (${this.$t(`contact:identifier_type.${item}`)})`
                    }
                    return `${this.$t(`contact:identifier_type.${item}`)}`
                } else {
                    return this.$t(`contact:${contact_item}`)
                }
            } else if (object_type === OBJECT_TYPE.DOMAIN) {
                return this.$t(`domain:${item}`)
            } else if (object_type === OBJECT_TYPE.NSSET) {
                return this.$t(`nsset:${item}`)
            } else if (object_type === OBJECT_TYPE.KEYSET) {
                return this.$t(`keyset:${item}`)
            }
        },
        /**
         * Gets the value of matched item
         *
         * @param { Object } result - found search result
         * @param { String } item - matched item
         * @returns { String } - value of the matched item
         */
        match_item_value(result, item) {
            if (item.startsWith('additional_identifier.')) {
                return result.contact.additional_identifier.value
            }

            if (item === 'notify_email') {
                return result.contact.notify_emails.join(', ')
            }

            return result.contact[item]
        },
        /**
         * Gets the other matches for other contact fields it can match in
         *
         * Note: Array `matched_items` contains full dotted path to the patched item, e.g. "place.city".
         * We are considering address as a whole and therefore we use only the first part of the path, e.g. "place"
         *
         * @param { Array<String> } matched_items - search result matched items
         * @returns { Array<String> } - array of other matches for contact
         */
        other_matches(matched_items) {
            if (!matched_items) {
                return []
            }

            const exclude = [
                'contact_handle', 'name', 'organization', 'telephone', 'email',
                'place.street', 'place.city', 'place.postal_code', 'place.state_or_province', 'place.country_code',
            ]
            return matched_items.filter(
                match => !exclude.includes(match) && !CONTACT_ADDRESS_ITEMS.includes(match.split('.')[0]),
            )
        },
    },
}
