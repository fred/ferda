import { URLS } from 'ferda/Urls'

import { ConsoleMixin } from './ConsoleMixin'
import { DatetimeMixin } from './DatetimeMixin'
import { HttpMixin } from './HttpMixin'
import { NotificationsMixin } from './NotificationsMixin'

export const OBJECT_TYPE = Object.freeze({
    CONTACT: 'contact',
    DOMAIN: 'domain',
    NSSET: 'nsset',
    KEYSET: 'keyset',
    REGISTRAR: 'registrar',
})

export const DOMAIN_QUERY = Object.freeze({
    START: 'start',
    END: 'end',
})

export const CONTACT_MAIN_ITEMS = Object.freeze({
    ORGANIZATION: 'organization',
    NAME: 'name',
    ADDITIONAL_IDENTIFIER: 'additional_identifier',
    VAT_IDENTIFICATION_NUMBER: 'vat_identification_number',
    TELEPHONE: 'telephone',
    FAX: 'fax',
    EMAILS: 'emails',
    NOTIFY_EMAILS: 'notify_emails',
    WARNING_LETTER: 'warning_letter',
})

export const CONTACT_ADDRESS_ITEMS = Object.freeze([
    'place',
    'mailing_address',
    'billing_address',
    'shipping_address1',
    'shipping_address2',
    'shipping_address3',
])

export const REGISTRAR_MAIN_ITEMS = Object.freeze({
    ORGANIZATION: 'organization',
    NAME: 'name',
    TELEPHONE: 'telephone',
    FAX: 'fax',
    EMAILS: 'emails',
    URL: 'url',
    IS_SYSTEM_REGISTRAR: 'is_system_registrar',
    IS_INTERNAL: 'is_internal',
    COMPANY_REGISTRATION_NUMBER: 'company_registration_number',
    VAT_IDENTIFICATION_NUMBER: 'vat_identification_number',
    IS_VAT_PAYER: 'is_vat_payer',
    VARIABLE_SYMBOL: 'variable_symbol',
    PAYMENT_MEMO_REGEX: 'payment_memo_regex',
})

export const CERTIFICATION_ITEMS = Object.freeze({
    VALID_FROM: 'valid_from',
    VALID_TO: 'valid_to',
    FILE: 'file',
    CLASSIFICATION: 'classification',
})

export const EVENT_MAIN_ITEMS = Object.freeze([
    'registered',
    'transferred',
    'updated',
    'unregistered',
])

export const CONTACT_ROLES = Object.freeze({
    HOLDER: 'holder',
    ADMIN: 'admin',
    KEYSET_TECH_CONTACT: 'keyset_tech_contact',
    NSSET_TECH_CONTACT: 'nsset_tech_contact',
})

export const RELATED_DOMAIN_TABLE_COLUMNS = Object.freeze({
    FQDN: 'fqdn',
    SPONSORING_REGISTRAR: 'sponsoring_registrar',
    CONTACT: 'contact',
    ROLES: 'roles',
    NSSET: 'nsset',
    KEYSET: 'keyset',
    EXPIRES_AT: 'expires_at',
    OUTZONE_AT: 'outzone_at',
    DELETE_CANDIDATE_AT: 'delete_at',
})

export const REGISTRAR_ALL_ITEMS = Object.freeze({
    REGISTRAR_HANDLE: 'registrar_handle',
    NAME: 'name',
    ORGANIZATION: 'organization',
    STREET1: 'street1',
    STREET2: 'street2',
    STREET3: 'street3',
    CITY: 'city',
    POSTAL_CODE: 'postal_code',
    COUNTRY: 'country',
    COUNTRY_CODE: 'country_code',
    STATE_OR_PROVINCE: 'state_or_province',
    TELEPHONE: 'telephone',
    FAX: 'fax',
    EMAILS: 'emails',
    URL: 'url',
    COMPANY_REGISTRATION_NUMBER: 'company_registration_number',
    VAT_IDENTIFICATION_NUMBER: 'vat_identification_number',
    VARIABLE_SYMBOL: 'variable_symbol',
    PAYMENT_MEMO_REGEX: 'payment_memo_regex',
    IS_SYSTEM_REGISTRAR: 'is_system_registrar',
    IS_INTERNAL: 'is_internal',
    IS_VAT_PAYER: 'is_vat_payer',
})

export const DEFAULT_REGISTRAR = Object.freeze({
    [REGISTRAR_ALL_ITEMS.REGISTRAR_HANDLE]: '',
    [REGISTRAR_ALL_ITEMS.NAME]: '',
    [REGISTRAR_ALL_ITEMS.ORGANIZATION]: '',
    [REGISTRAR_ALL_ITEMS.STREET1]: '',
    [REGISTRAR_ALL_ITEMS.STREET2]: '',
    [REGISTRAR_ALL_ITEMS.STREET3]: '',
    [REGISTRAR_ALL_ITEMS.CITY]: '',
    [REGISTRAR_ALL_ITEMS.POSTAL_CODE]: '',
    [REGISTRAR_ALL_ITEMS.COUNTRY_CODE]: '',
    [REGISTRAR_ALL_ITEMS.STATE_OR_PROVINCE]: '',
    [REGISTRAR_ALL_ITEMS.TELEPHONE]: '',
    [REGISTRAR_ALL_ITEMS.FAX]: '',
    [REGISTRAR_ALL_ITEMS.EMAILS]: [],
    [REGISTRAR_ALL_ITEMS.URL]: '',
    [REGISTRAR_ALL_ITEMS.COMPANY_REGISTRATION_NUMBER]: '',
    [REGISTRAR_ALL_ITEMS.VAT_IDENTIFICATION_NUMBER]: '',
    [REGISTRAR_ALL_ITEMS.VARIABLE_SYMBOL]: '',
    [REGISTRAR_ALL_ITEMS.PAYMENT_MEMO_REGEX]: '',
    [REGISTRAR_ALL_ITEMS.IS_SYSTEM_REGISTRAR]: false,
    [REGISTRAR_ALL_ITEMS.IS_INTERNAL]: false,
    [REGISTRAR_ALL_ITEMS.IS_VAT_PAYER]: false,
})

export const CONTACT_REPRESENTATIVE_ITEMS = Object.freeze({
    CITY: 'city',
    COMPANY: 'company',
    CONTACT_ID: 'contact_id',
    COUNTRY_CODE: 'country_code',
    EMAIL: 'email',
    NAME: 'name',
    ORGANIZATION: 'organization',
    PLACE: 'place',
    POSTAL_CODE: 'postal_code',
    STATE_OR_PROVINCE: 'state_or_province',
    STREET1: 'street1',
    STREET2: 'street2',
    STREET3: 'street3',
    TELEPHONE: 'telephone',
})

export const DEFAULT_CONTACT_REPRESENTATIVE = Object.freeze({
    [CONTACT_REPRESENTATIVE_ITEMS.CITY]: '',
    [CONTACT_REPRESENTATIVE_ITEMS.COMPANY]: '',
    [CONTACT_REPRESENTATIVE_ITEMS.COUNTRY_CODE]: '',
    [CONTACT_REPRESENTATIVE_ITEMS.EMAIL]: '',
    [CONTACT_REPRESENTATIVE_ITEMS.NAME]: '',
    [CONTACT_REPRESENTATIVE_ITEMS.POSTAL_CODE]: '',
    [CONTACT_REPRESENTATIVE_ITEMS.STATE_OR_PROVINCE]: '',
    [CONTACT_REPRESENTATIVE_ITEMS.STREET1]: '',
    [CONTACT_REPRESENTATIVE_ITEMS.STREET2]: '',
    [CONTACT_REPRESENTATIVE_ITEMS.STREET3]: '',
    [CONTACT_REPRESENTATIVE_ITEMS.TELEPHONE]: '',
})

export const REGISTRAR_CERTIFICATE_ITEMS = Object.freeze({
    FINGERPRINT: 'fingerprint',
    PASSWORD: 'password',
    CERT_DATA_PEM: 'cert-data-pem',
})

export const ZONE_ACCESS_ITEMS = Object.freeze({
    VALID_FROM: 'valid_from',
    VALID_TO: 'valid_to',
})

export const RegistryMixin = {
    mixins: [ConsoleMixin, DatetimeMixin, HttpMixin, NotificationsMixin],
    created() {
        this.OBJECT_TYPE = OBJECT_TYPE
        this.CONTACT_MAIN_ITEMS = CONTACT_MAIN_ITEMS
        this.CONTACT_ADDRESS_ITEMS = CONTACT_ADDRESS_ITEMS
        this.REGISTRAR_MAIN_ITEMS = REGISTRAR_MAIN_ITEMS
        this.EVENT_MAIN_ITEMS = EVENT_MAIN_ITEMS
    },
    methods: {
        /**
         * Gets search parameters according to users selection
         *
         * @param { Array<String> } search_items - user preference
         * @returns { Object } - created URLSearchParams
         */
        get_search_form_params(search_items) {
            const params = new URLSearchParams()
            for (const item of search_items) {
                if (item === 'address') {
                    params.append('search_items', 'place.street')
                    params.append('search_items', 'place.city')
                    params.append('search_items', 'place.state_or_province')
                    params.append('search_items', 'place.postal_code')
                } else if (item === 'identifier') {
                    params.append('search_items', 'additional_identifier')
                } else {
                    params.append('search_items', item)
                }
            }
            return params
        },
        /**
         * Gets search parameters according to users selection
         *
         * @param { Array<String> } query_values - user query to be searched
         * @param { String } object_type - searched object type
         * @param { Array<String> } search_items - searched checkboxes
         * @param { String } search_date - range to be serched in
         * @param { String } valid_from - datetime from which to search
         * @returns { Object } - created URLSearchParams
         */
        get_search_params(query_values, object_type, search_items, search_date = null, valid_from = null) {
            const params = this.get_search_form_params(search_items)

            for (const query_item of query_values) {
                params.append('query', query_item)
            }
            if (search_date && search_date !== this.SEARCH_HISTORY_PERIOD.NOW)
                params.append('history', search_date)
            if (valid_from)
                params.append('valid_from', valid_from)

            params.append('type', object_type)

            return params
        },
        // https://www.iana.org/assignments/dns-sec-alg-numbers/dns-sec-alg-numbers.xhtml
        get_dnskey_alg_label(alg_id) {
            if (alg_id < 0 || alg_id > 255)
                return `${alg_id} ${this.$t('keyset:is_out_of_range')}`

            if (alg_id >= 17 && alg_id <= 122)
                return this.$t('keyset:unassigned')

            return this.$t([`keyset:dns_key_alg.alg${alg_id}`, 'keyset:reserved'])
        },
        // https://www.iana.org/assignments/dnskey-flags/dnskey-flags.xhtml#dnskey-flags-1
        //  0-6  Unassigned
        //    7  ZONE
        //    8  REVOKE
        // 9-14  Unassigned
        //   15  Secure Entry Point (SEP)
        get_dnskey_flag_labels(flags) {
            if (flags < 0 || flags > 0xffff)
                return `${flags} ${this.$t('keyset:is_out_of_range')}`

            const labels = []
            if (flags & 0b0000000000000001)
                labels.push(this.$t('keyset:dns_key_flag.sep'))
            if (flags & 0b0000000100000000)
                labels.push(this.$t('keyset:dns_key_flag.zone'))
            if (flags & 0b0000000010000000)
                labels.push(this.$t('keyset:dns_key_flag.revoke'))

            return labels.join(', ')
        },
        // https://www.iana.org/assignments/dns-key-rr/dns-key-rr.xhtml#dns-key-rr-1
        // 1-2, 4, 255 Reserved
        // 5-254       Unassigned
        // 3           DNSSEC
        get_dnskey_protocol_label(protocol_id) {
            if (protocol_id < 0 || protocol_id > 255)
                return `${protocol_id} ${this.$t('keyset:is_out_of_range')}`

            if (protocol_id === 3)
                return this.$t('keyset:dnssec')
            else if (protocol_id >= 5 && protocol_id <= 254)
                return this.$t('keyset:unassigned')
            else
                return this.$t('keyset:reserved')
        },
        /**
         * Gets history API url
         *
         * @param { String } object_type - contact, domain, nsset or keyset
         * @param { String } uuid - object identifier
         * @param { String | Object } history_spec - history identifier (uuid or null)
         * @param { Boolean } is_datetime - whether history identifier is datetime or not
         * @returns { String } - particular URL
         */
        get_history_url(object_type, uuid, history_spec, is_datetime) {
            if (history_spec) {
                return is_datetime ?
                    URLS.object_info_history_ajax_datetime(object_type, uuid, history_spec) :
                    URLS.object_info_history_ajax(object_type, uuid, history_spec)
            }
            return URLS.object_info_ajax(object_type, uuid)
        },
        /**
         * Gets info about particular object
         *
         * @param { String } object_type - contact, domain, nsset or keyset
         * @param { String } uuid - object identifier
         * @param { String | Object } history_spec - history identifier (uuid or null)
         * @returns { Object } - null in case of server error
         * @returns { Object } - info about particular object
         */
        async get_object_info(object_type, uuid, history_spec = null, is_datetime = true) {
            let response

            try {
                response = await this.http_get(this.get_history_url(object_type, uuid, history_spec, is_datetime))
            } catch (error) {
                this.notify_message(this.ERROR_SOMETHING_WENT_WRONG)
                this.log_error(error)
                return null
            }
            return response.data
        },
        async get_nsset_info(uuid, history_spec = null) {
            return this.get_object_info(OBJECT_TYPE.NSSET, uuid, history_spec)
        },
        async get_keyset_info(uuid, history_spec = null) {
            return this.get_object_info(OBJECT_TYPE.KEYSET, uuid, history_spec)
        },
        async get_countries() {
            let response
            try {
                response = await this.http_get(URLS.countries_list())
            } catch (error) {
                this.notify_message(this.ERROR_SOMETHING_WENT_WRONG)
                this.log_error(error)
                return null
            }
            return response.data
        },
        fqdn_sort(arr) {
            return arr.map(fqdn => fqdn.split('.').reverse()).sort().map(fqdn => fqdn.reverse().join('.'))
        },
        /**
         * Get state flags data
         *
         * @param { String } object_type
         * @param { String } object_id
         * @returns { Object } response
         * @returns { Object } null - In case of server error
         */
        async get_state_flags_data(object_type, object_id) {
            let response
            try {
                response = await this.http_get(URLS.object_state_ajax(object_type, object_id))
            } catch (error) {
                this.notify_message(this.ERROR_SOMETHING_WENT_WRONG)
                this.log_error(error)
                return null
            }
            return response.data
        },
        /**
         * Gets a list of registrars
         *
         * @param { Object } signal - signal of AbortController
         * @returns { Object } null - in case of server error
         * @returns { Array<Object> } - a list of registrars
         */
        async get_registrar_list_data(signal = null) {
            let registrars
            try {
                registrars = await this.http_get(URLS.registrar_list_ajax(), { signal })
            } catch (error) {
                const error_message = this.is_http_cancel(error) ?
                    this.INFO_REQUEST_ABORTED : this.ERROR_SOMETHING_WENT_WRONG
                this.notify_message(error_message)
                this.log_error(error)
                return null
            }
            return registrars.data
        },
        /**
         * Finds error messages for particular fields inside error response
         *
         * @param { String } field_type - Field to search error for
         * @param { Array<String> } update_errors - Received server errors
         * @returns { Array<String> } - in case error has been found
         * @returns { Object } - null in case error wasn't found
         */
        get_error_messages(field_type, update_errors) {
            const error_exists = update_errors?.fields.some(item => item.includes(field_type))
            return error_exists ? [update_errors.detail] : null
        },
        /**
         * Finds error messages for registrar certificate submitted via file
         * Method get_error_messages can't be used because of the way backend handles errors for fingerprints
         *
         * @param { Array<String> } update_errors - Received server errors
         * @param { Boolean } use_certificate_file - Whether certificate file is being used or not
         * @returns { Array<String> } - in case duplicite fingerprint has been uploaded as a file
         * @returns { Array<String> } - any other errors were found
         * @returns { Array } - empty array in case error wasn't found
         */
        get_registrar_certificate_error_messages(update_errors, use_certificate_file) {
            // It is a duplicite certificate submitted via file
            if (
                update_errors?.fields.some(item => item.includes(REGISTRAR_CERTIFICATE_ITEMS.FINGERPRINT))
                && use_certificate_file
            ) {
                return [update_errors.detail]
            }

            // Any other errors, i.e. invalid certificate
            if (update_errors?.fields.length) {
                return [update_errors.detail]
            }

            return []
        },
        /**
         * Extracts normalized existing fingerprints
         *
         * @param { Array<String> } certificates - existing certificates
         * @returns { Array<String> } - existing fingerprints
         */
        get_existing_fingerprints(certificates) {
            return certificates.map(certificate => this.normalize_fingerprint(certificate.certificate.fingerprint))
        },
        /**
         * Gets normalized fingerprint for comparison
         *
         * @param { String } fingerprint - original fingerprint
         * @returns { String } - normalized fingerprint
         */
        normalize_fingerprint(fingerprint) {
            return fingerprint.trim().toLowerCase().replace(':', '')
        },
        /**
         * Shows countries
         *
         * @returns { void } - In case of error
         * @returns { Array<Object> } - countries
         */
        async set_countries() {
            const countries = await this.get_countries()
            if (countries === null) {
                return
            }
            return Object.keys(countries)
                .sort((a, b) => (countries[a] < countries[b] ? -1 : 1))
                .map(country_code => ({ text: countries[country_code], value: country_code }))
        },
        /**
         * Prepends protocol to the URL if it doesn't have it
         *
         * @param { String } original_url - original registrar URL
         * @returns { String } - empty string in case URL is not specified
         * @returns { String } - original_url in case protocol is included
         * @returns { String } - original_url with added protocol
         */
        get_registrar_url(original_url) {
            if (!original_url) {
                return ''
            }

            if (original_url.includes('http')) {
                return original_url
            }
            return `https://${original_url}`
        },
        /**
         * Gets the delete date of a given object in case it is deleted
         *
         * @param { Object } search_result - found search result
         * @param { String } object_type - particular object type
         * @returns { String } - delete date if it is present
         * @returns { Object } - null in case object hasn't been deleted
         */
        get_delete_date(search_result, object_type) {
            return search_result[object_type].events.unregistered?.timestamp || null
        },
        /**
         * Gets DNS keys algorithm labels
         *
         * @param { Array<Object> } dns_keys
         * @returns { Array<String> } - labels
         */
        dns_keys_algorithms(dns_keys) {
            return dns_keys.flatMap(item => this.$t(this.get_dnskey_alg_label(item.alg)))
        },
    },
}
