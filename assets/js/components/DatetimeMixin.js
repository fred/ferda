import moment from 'moment-timezone'
import { mapGetters } from 'vuex'

export const SEARCH_HISTORY_PERIOD = Object.freeze({
    NOW: 'now',
    LAST_MONTH: 'last_month',
    LAST_YEAR: 'last_year',
    LAST_5_YEARS: 'last_5_years',
    FULL_HISTORY: 'full',
})
export const DATE_RANGE = Object.freeze({
    CURRENT_HOUR: 'current_hour',
    PAST_HOUR: 'past_hour',
    CURRENT_DAY: 'current_day',
    PAST_DAY: 'past_day',
    CURRENT_MONTH: 'current_month',
    PAST_MONTH: 'past_month',
})
export const DATE_TYPE = Object.freeze({
    HOUR: 'hour',
    DAY: 'day',
    MONTH: 'month',
})

export const DatetimeMixin = {
    computed: {
        ...mapGetters('settings', ['TIME_ZONE']),
        ...mapGetters('preferences', ['selected_language']),
    },
    created() {
        this.SEARCH_HISTORY_PERIOD = SEARCH_HISTORY_PERIOD
        this.DATE_RANGE = DATE_RANGE
        this.DATE_RANGE_LIST = Object.freeze([
            {
                value: DATE_RANGE.CURRENT_HOUR,
                text: this.$t('common:datetime.current_hour'),
            },
            {
                value: DATE_RANGE.PAST_HOUR,
                text: this.$t('common:datetime.past_hour'),
            },
            {
                value: DATE_RANGE.CURRENT_DAY,
                text: this.$t('common:datetime.current_day'),
            },
            {
                value: DATE_RANGE.PAST_DAY,
                text: this.$t('common:datetime.past_day'),
            },
            {
                value: DATE_RANGE.CURRENT_MONTH,
                text: this.$t('common:datetime.current_month'),
            },
            {
                value: DATE_RANGE.PAST_MONTH,
                text: this.$t('common:datetime.past_month'),
            },
        ])
    },
    methods: {
        now() {
            return moment().tz(this.TIME_ZONE).locale(this.selected_language)
        },
        iso_to_moment(isodatetime) {
            // for naive datetimes assume UTC timezone
            const dt = moment.utc(isodatetime).tz(this.TIME_ZONE).locale(this.selected_language)
            return dt
        },
        iso_to_locale(isodatetime, with_zone = false) {
            if (with_zone) {
                return moment.tz(isodatetime, this.TIME_ZONE).locale(this.selected_language).format('ll LTS z')
            }

            const dt = moment.utc(isodatetime)  // for naive datetimes assume UTC timezone
            const date = dt.tz(this.TIME_ZONE).locale(this.selected_language).format('ll')
            const time = dt.tz(this.TIME_ZONE).locale(this.selected_language).format('LTS')
            return `${date} ${time}`
        },
        iso_to_locale_date(isodatetime) {
            const dt = moment.utc(isodatetime)  // for naive datetimes assume UTC timezone
            return dt.tz(this.TIME_ZONE).locale(this.selected_language).format('ll')
        },
        /**
         * Assumes isodate is given in user's zone and converts it into UTC
         * Ex: '2022-01-05T12:00:00' is transformed to '2022-01-05T11:00:00'
         *
         * @param { String } isodate - naive datetime in YYYY-MM-DDTHH:mm:ss format
         */
        naive_local_to_naive_utc(isodate) {
            return moment.tz(isodate, this.TIME_ZONE).utc().format('YYYY-MM-DDTHH:mm:ss')
        },
        /**
         * Assumes utcdatetime is given in UTC and converts it into user's zone
         * Ex: '2022-01-05T11:00:00' is transformed to '2022-01-05T12:00:00+01:00'
         *
         * @param { String } utcdatetime - naive datetime in YYYY-MM-DDTHH:mm:ss format
         */
        naive_utc_to_aware_local(isodate) {
            return moment.tz(isodate, 'UTC').tz(this.TIME_ZONE).format('YYYY-MM-DDTHH:mm:ssZ')
        },
        /**
         * Assumes naive_time is given in user's zone and converts it into UTC
         * Ex: '12:00:00' is transformed to '11:00:00'
         *
         * @param { String } naive_time - naive time in HH:mm:ss format
         */
        naive_local_time_to_naive_utc_time(naive_time) {
            return moment.tz(naive_time, 'HH:mm:ss', this.TIME_ZONE).utc().format('HH:mm:ss')
        },
        /**
         * Assumes naive_time is given in UTC and converts it into user's zone, either with or without zone
         * Ex: '11:00:00' is transformed to '12:00:00' in case with_zone is false
         * Ex: '11:00:00' is transformed to '12:00:00+01:00' in  case with_zone is true
         *
         * @param { String } naive_time
         * @param { Boolean } output_with_zone
         */
        naive_utc_time_to_aware_local_time(naive_time, output_with_zone = false) {
            const format = output_with_zone ? 'HH:mm:ssZ' : 'HH:mm:ss'
            return moment.tz(naive_time, 'HH:mm:ss', 'UTC').tz(this.TIME_ZONE).format(format)
        },
        time_to_locale(time, with_zone = false) {
            let output_format = 'LTS'
            if (with_zone) {
                output_format = 'LTS z'
            }

            return moment
                .parseZone(time, ['HH:mm:ss', 'HH:mm:ssZ'])
                .tz(this.TIME_ZONE)
                .locale(this.selected_language)
                .format(output_format)
        },
        date_range(from, to) {
            from = this.iso_to_locale_date(from)
            if (!to)
                to = 'now'
            else
                to = this.iso_to_locale_date(to)
            return `${from} - ${to}`
        },
        history_search_period(search_date) {
            const valid_from = this.now()
            switch (search_date) {
            case SEARCH_HISTORY_PERIOD.LAST_MONTH:
                valid_from.subtract(1, 'M')
                break
            case SEARCH_HISTORY_PERIOD.LAST_YEAR:
                valid_from.subtract(1, 'y')
                break
            case SEARCH_HISTORY_PERIOD.LAST_5_YEARS:
                valid_from.subtract(5, 'y')
                break
            // case full_history or no search_date - valid_from should not be used
            case SEARCH_HISTORY_PERIOD.FULL_HISTORY:
            default:
                return undefined
            }
            return valid_from.format()
        },
        get_time_zone() {
            return moment.tz(this.TIME_ZONE).locale(this.selected_language)
        },
        get_time_zone_offset() {
            return this.get_time_zone().format('Z')
        },
        get_time_zone_abbr() {
            return this.get_time_zone().format('z')
        },
        /**
         * Get full date from given string
         * If the date is incomplete (for example a day is missing), it fills '1'
         *
         * @param { String } date
         * @return String
         */
        get_iso_date(date) {
            return moment(date).format('YYYY-MM-DD')
        },
        is_valid_time(input) {
            return moment(input, 'HH:mm:ss', true).isValid()
        },
        /**
         * Checks that the dates have the correct range
         *
         * @param { String } value_to_be_less
         * @param { String } compared_value
         * @returns { Boolean } - true / false
         */
        is_less_than(value_to_be_less, compared_value) {
            return moment(value_to_be_less).isBefore(compared_value)
        },
        /**
         * Takes any valid date, adds specified days to it and moves the time to midnight
         *
         * @param { String } original_date - Any valid date
         * @param { Number } days_number - Days to add to date
         */
        create_next_day_midnight(original_date, days_number) {
            return moment(original_date).add(days_number, 'days').startOf('day').format()
        },
        /**
         * Takes input and checks ISO 8601 validity
         *
         * @param { String } input - Input being checked
         * @returns { Boolean } true / false - Whether input is valid ISO 8601 or not
         */
        is_valid_iso(input) {
            return moment(input, moment.ISO_8601).isValid()
        },
        /**
         * Validates input given from user by typing
         *
         * @param { String } input_value - Value inside field
         * @param { String } display_value - Currently displayed locale value
         * @returns { null } - In case value is empty
         * @returns { Boolean } - false, in case input value is the same as locale value
         * @returns { Array<String> } - Array with input error
         * @returns { String } - Valid ISO input
         */
        validate_datetime_input(input_value, display_value) {
            if (input_value.length === 0) {
                return null
            } else if (input_value === display_value) {
                return false
            } else if (!this.is_valid_iso(input_value)) {
                return [this.$t('common:iso_format_expected')]
            } else {
                return input_value
            }
        },
        /**
         * Gets default range of dates
         *
         * @param { Number } range
         * @returns { String } date
         */
        get_default_range(range) {
            return moment().add(range, 'seconds').format('YYYY-MM-DDTHH:mm:ssZ')
        },
        /**
         * Determines if the date is within the allowed 30 days
         *
         * @param { String } valid_from
         * @param { String } valid_to
         * @param { String } input_value - value from input
         * @returns { Boolean } result - true or false
         */
        is_valid_range(valid_from, valid_to) {
            const max_date = 2592000
            const result = (moment(valid_to).unix() - moment(valid_from).unix()) > max_date ? false : true
            return result
        },
        /**
         * Checks if the provided date is in the future
         *
         * @param { String } value
         * @param { String } input_value - value from input
         * @returns { Boolean } show error message or not
         */
        is_valid_future_date(value) {
            const today = moment().startOf('day')
            if (moment(value).isBefore(today)) {
                return false
            } else {
                return true
            }
        },
        /**
         * Get current unfinished hour/day/month starting at beginning of the current hour/day/month
         *
         * @param { string } value - hour, date or month
         * @returns { Object } - created_from, created_to
        */
        get_current_date_range(value) {
            const created_from = moment().startOf(value)
            const created_to = moment(created_from).add(1, value)
            return { created_from: created_from.format(), created_to: created_to.format() }
        },
        /**
         * Get the hour/day/month preceding the current hour/day/month
         *
         * @param { string } value - hour, date or month
         * @returns { Object } - created_from, created_to
        */
        get_past_date_range(value) {
            const created_from = moment().startOf(value).subtract(1, value)
            const created_to = moment(created_from).add(1, value)
            return { created_from: created_from.format(), created_to: created_to.format() }
        },
        /**
         * Gets the desired range between time_begin_from and time_begin_to
         *
         * @param { Object } date_range - selected date range
         * @returns { Object } results
        */
        get_date_range(date_range) {
            if (date_range === DATE_RANGE.CURRENT_HOUR) return this.get_current_date_range(DATE_TYPE.HOUR)
            if (date_range === DATE_RANGE.CURRENT_DAY) return this.get_current_date_range(DATE_TYPE.DAY)
            if (date_range === DATE_RANGE.CURRENT_MONTH) return this.get_current_date_range(DATE_TYPE.MONTH)
            if (date_range === DATE_RANGE.PAST_HOUR) return this.get_past_date_range(DATE_TYPE.HOUR)
            if (date_range === DATE_RANGE.PAST_DAY) return this.get_past_date_range(DATE_TYPE.DAY)
            if (date_range === DATE_RANGE.PAST_MONTH) return this.get_past_date_range(DATE_TYPE.MONTH)
        },
        /**
         * Wraps given date or datetime with current locale
         *
         * @param { String } value - date or datetime
         * @returns { String } - wrapped date or datetime
         */
        get_locale_moment(value) {
            return moment(value).tz(this.TIME_ZONE).locale(this.selected_language)
        },
        /**
         * Gets current delete date of the domain
         *
         * @param { Object } domain - currently rendered table row
         * @returns { String } - in case actual delete date is present
         * @returns { Object } - undefined in case actual delete date isn't present
         */
        get_current_delete_date(domain) {
            return this.iso_to_locale_date(
                domain.events.unregistered?.timestamp ||
                domain.delete_candidate_at ||
                domain.delete_candidate_scheduled_at,
            )
        },
    },
}
