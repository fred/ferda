import { router } from 'FerdaJS/app.config'
import RegistrarDetail from 'ferda/registrar/RegistrarDetail.vue'
import RegistrarList from 'ferda/registrar/RegistrarList.vue'

const routes = Object.freeze([
    { name: 'registrar-list', path: '/registry/registrar/', component: RegistrarList },
    { name: 'registrar-detail', path: '/registry/registrar/:registrar_handle/', component: RegistrarDetail },
])

for (const route of routes) router.addRoute(route)

export default router
