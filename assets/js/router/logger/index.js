import { router } from 'FerdaJS/app.config'
import LoggerDetail from 'ferda/logger/LoggerDetail.vue'
import LoggerList from 'ferda/logger/LoggerList.vue'

const routes = Object.freeze([
    { name: 'logger-list', path: '/logger/', component: LoggerList },
    { name: 'logger-detail', path: '/logger/:log_id/', component: LoggerDetail },
])

for (const route of routes) router.addRoute(route)

export default router
