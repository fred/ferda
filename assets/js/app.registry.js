import i18next from 'i18next'
import Vue from 'vue'
import Vuetify from 'vuetify/lib'

import { get_vuetify_options, i18n, store } from 'FerdaJS/app.config'
import router from 'FerdaJS/router/registry'
import ContactDetail from 'ferda/contact/ContactDetail.vue'
import ContactHistory from 'ferda/contact/ContactHistory.vue'
import DomainDetail from 'ferda/domain/DomainDetail.vue'
import DomainHistory from 'ferda/domain/DomainHistory.vue'
import KeysetDetail from 'ferda/keyset/KeysetDetail.vue'
import KeysetHistory from 'ferda/keyset/KeysetHistory.vue'
import NotificationList from 'ferda/notification/NotificationList.vue'
import NssetDetail from 'ferda/nsset/NssetDetail.vue'
import NssetHistory from 'ferda/nsset/NssetHistory.vue'
import RegistrarBaseView from 'ferda/registrar/RegistrarBaseView.vue'
import SearchRegistry from 'ferda/search/SearchRegistry.vue'
import { capitalize_string } from 'ferda/utils'

i18next.on('initialized', () => {
    const OBJECT_DETAIL_COMPONENTS = [
        {type: 'contact', component: ContactDetail},
        {type: 'domain', component: DomainDetail},
        {type: 'nsset', component: NssetDetail},
        {type: 'keyset', component: KeysetDetail},
    ]

    for (const object of OBJECT_DETAIL_COMPONENTS) {
        const object_detail_el = document.querySelector(`#${object.type}-detail`)
        if (object_detail_el) {
            new Vue({
                el: `#${object.type}-detail`,
                name: `${capitalize_string(object.type)}Detail`,
                vuetify: new Vuetify(get_vuetify_options()),
                i18n,
                store,
                render: createElement => {
                    const object_data = JSON.parse(document.querySelector(`#${object.type}-data`).textContent)
                    return createElement(object.component, {
                        props: {
                            uuid: object_data.object_uuid,
                            [object.type]: object_data.object_info,
                            history_id: object_data.object_history_id,
                            history_datetime: object_data.object_history_datetime,
                            has_history: object_data.object_has_history,
                        },
                    })
                },
            })
        }
    }

    const OBJECT_HISTORY_COMPONENTS = [
        {type: 'contact', component: ContactHistory},
        {type: 'domain', component: DomainHistory},
        {type: 'nsset', component: NssetHistory},
        {type: 'keyset', component: KeysetHistory},
    ]

    for (const object of OBJECT_HISTORY_COMPONENTS) {
        const object_history_el = document.querySelector(`#${object.type}-history`)
        if (object_history_el) {
            new Vue({
                el: `#${object.type}-history`,
                name: `${capitalize_string(object.type)}History`,
                vuetify: new Vuetify(get_vuetify_options()),
                i18n,
                store,
                render: createElement => {
                    const history_data = JSON.parse(document.querySelector('#history-data').textContent)
                    return createElement(object.component, {
                        props: {
                            uuid: history_data.object_uuid,
                            from_history_id: history_data.from_history_id,
                            to_history_id: history_data.to_history_id,
                            object_history: history_data.object_history,
                            object_handle: history_data.object_handle,
                        },
                    })
                },
            })
        }
    }

    const registrar_list_el = document.querySelector('#registrar-list')
    if (registrar_list_el) {
        new Vue({
            el: '#registrar-list',
            name: 'RegistrarList',
            vuetify: new Vuetify(get_vuetify_options()),
            i18n,
            router,
            store,
            render: createElement => createElement(RegistrarBaseView),
        })
    }

    const registrar_detail_el = document.querySelector('#registrar-detail')
    if (registrar_detail_el) {
        new Vue({
            el: '#registrar-detail',
            name: 'RegistrarDetail',
            vuetify: new Vuetify(get_vuetify_options()),
            i18n,
            router,
            store,
            render: createElement => createElement(RegistrarBaseView),
        })
    }

    const registry_search_el = document.querySelector('#registry-search')
    if (registry_search_el) {
        new Vue({
            el: '#registry-search',
            name: 'RegistrySearch',
            vuetify: new Vuetify(get_vuetify_options()),
            i18n,
            components: { SearchRegistry },
            store,
            render: createElement => createElement(SearchRegistry),
        })
    }

    const notification = document.querySelector('#notification')
    if (notification) {
        new Vue({
            el: '#notification',
            name: 'NotificationList',
            vuetify: new Vuetify(get_vuetify_options()),
            i18n,
            components: { NotificationList },
            store,
            render: createElement => createElement(NotificationList),
        })
    }
})
