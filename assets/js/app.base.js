import 'vuetify/dist/vuetify.min.css'
import '@mdi/font/css/materialdesignicons.css'

import Notifications from '@remotehq/vue-notification'
import i18next from 'i18next'
import Backend from 'i18next-chained-backend'
import Vue from 'vue'
import AsyncComputed from 'vue-async-computed'
import VueCookies from 'vue-cookies'
import Vuetify from 'vuetify/lib'

import { get_i18next_options, get_vuetify_options, i18n, initialize_store, store } from 'FerdaJS/app.config'
import AppLogin from 'ferda/auth/AppLogin.vue'
import FidoAppLogin from 'ferda/auth/FidoAppLogin.vue'

Vue.use(AsyncComputed)
Vue.use(VueCookies)
Vue.use(Vuetify)
Vue.use(Notifications)

document.addEventListener('DOMContentLoaded', async() => {
    // Provide store changes to the document
    store.subscribe(mutation => { document.dispatchEvent(new CustomEvent('storechange', { detail: mutation }))})

    // Make initial commits to the store
    // Don't try to load user settings when user is not logged in
    const path_name = location.pathname
    const is_at_login_view = path_name.includes('/accounts/login/') || path_name.includes('/accounts/login-fido/')
    const settings = await initialize_store(store, is_at_login_view)

    await i18next.use(Backend).init(get_i18next_options(settings.django_settings.version))

    const login_page_el = document.querySelector('#login-page')
    if (login_page_el) {
        new Vue({
            el: '#login-page',
            name: 'LoginPage',
            vuetify: new Vuetify(get_vuetify_options()),
            i18n,
            store,
            components: { AppLogin },
            render: createElement => {
                const form_errors = JSON.parse(document.querySelector('#form-errors').textContent)
                const username = JSON.parse(document.querySelector('#form-username').textContent)
                return createElement(AppLogin, {
                    props: {
                        form_errors,
                        username,
                    },
                })
            },
        })
    }

    const login_fido_page_el = document.querySelector('#login-fido-page')
    if (login_fido_page_el) {
        new Vue({
            el: '#login-fido-page',
            vuetify: new Vuetify(get_vuetify_options()),
            i18n,
            store,
            components: { FidoAppLogin },
            render: createElement => {
                const form_errors = JSON.parse(document.querySelector('#form-errors').textContent)
                return createElement(FidoAppLogin, {
                    props: {
                        form_errors,
                    },
                })
            },
        })
    }

    document.dispatchEvent(new CustomEvent('ferda-loaded', { detail: { store_getters: store.getters, settings }}))
})

window.addEventListener('popstate', () => {
    location.reload()
})
