import i18next from 'i18next'
import Vue from 'vue'
import Vuetify from 'vuetify/lib'

import { get_vuetify_options, i18n, store } from 'FerdaJS/app.config'
import MessageDetail from 'ferda/messages/MessageDetail.vue'
import MessageList from 'ferda/messages/MessageList.vue'

i18next.on('initialized', () => {
    const message_list_el = document.querySelector('#message-list')
    if (message_list_el) {
        new Vue({
            el: '#message-list',
            name: 'MessageList',
            vuetify: new Vuetify(get_vuetify_options()),
            i18n,
            store,
            render: createElement => createElement(MessageList),
        })
    }
    const message_detail_el = document.querySelector('#message-detail')
    if (message_detail_el) {
        new Vue({
            el: '#message-detail',
            name: 'MessageDetail',
            vuetify: new Vuetify(get_vuetify_options()),
            i18n,
            store,
            render: createElement => {
                const message_id = JSON.parse(document.querySelector('#message-id').textContent)
                const message_type = JSON.parse(document.querySelector('#message-type').textContent)
                return createElement(MessageDetail, { props: { message_id, message_type }})
            },
        })
    }
})
