export const SearchModule = {
    namespaced: true,
    state: {
        last_query_values: [],
    },
    mutations: {
        set_last_query_values(state, value) { state.last_query_values = value },
    },
    getters: {
        last_query_values: state => state.last_query_values,
    },
}

export const HistoryModule = {
    namespaced: true,
    state: {
        version_list: [],
        old_history_id: null,
        new_history_id: null,
    },
    mutations: {
        set_version_list(state, value) { state.version_list = value },
        set_old_history_id(state, value) { state.old_history_id = value },
        set_new_history_id(state, value) { state.new_history_id = value },
    },
    getters: {
        version_list: state => state.version_list,
        old_history_id: state => state.old_history_id,
        new_history_id: state => state.new_history_id,
    },
}

export const SettingsModule = {
    namespaced: true,
    state: {
        TIME_ZONE: 'UTC',
        LANGUAGE_CODE: 'en',
        LANGUAGES: ['en', 'cs'],
        FIDO2_AUTHENTICATION: false,
        MANUAL_IN_ZONE_DEFAULT_DURATION: null,
        MAX_FILE_UPLOAD_SIZE: 10000000,
        PAGINATION_PAGE_SIZE: 50,
        OBJECT_DETAIL_LOGGER_SERVICES: [],
    },
    mutations: {
        set_time_zone(state, value) { state.TIME_ZONE = value },
        set_language_code(state, value) { state.LANGUAGE_CODE = value },
        set_languages(state, value) { state.LANGUAGES = value },
        set_fido2_authentication(state, value) { state.FIDO2_AUTHENTICATION = value },
        set_manual_in_zone_default_duration(state, value) { state.MANUAL_IN_ZONE_DEFAULT_DURATION = value },
        set_max_file_upload_size(state, value) { state.MAX_FILE_UPLOAD_SIZE = value },
        set_pagination_page_size(state, value) { state.PAGINATION_PAGE_SIZE = value },
        set_object_detail_logger_services(state, value) { state.OBJECT_DETAIL_LOGGER_SERVICES = value },
    },
    getters: {
        TIME_ZONE: state => state.TIME_ZONE,
        LANGUAGE_CODE: state => state.LANGUAGE_CODE,
        LANGUAGES: state => state.LANGUAGES,
        FIDO2_AUTHENTICATION: state => state.FIDO2_AUTHENTICATION,
        MANUAL_IN_ZONE_DEFAULT_DURATION: state => state.MANUAL_IN_ZONE_DEFAULT_DURATION,
        MAX_FILE_UPLOAD_SIZE: state => state.MAX_FILE_UPLOAD_SIZE,
        PAGINATION_PAGE_SIZE: state => state.PAGINATION_PAGE_SIZE,
        OBJECT_DETAIL_LOGGER_SERVICES: state => state.OBJECT_DETAIL_LOGGER_SERVICES,
    },
}

export const GeneralModule = {
    namespaced: true,
    state: {
        user: {},
        permissions: [],
    },
    mutations: {
        set_user(state, value) { state.user = value },
        set_permissions(state, value) { state.permissions = value },
    },
    getters: {
        user: state => state.user,
        permissions: state => state.permissions,
    },
}

const get_preference_value = (state, user_id, name, preference_name, default_value) => {
    if (state[name] === null) {
        const storage_item_name = `preferences.${user_id}.${preference_name}`
        const saved_value = localStorage.getItem(storage_item_name)
        if (saved_value === null)
            return default_value
        else
            return saved_value
    } else {
        return state[name]
    }
}

const set_preference_value = (state, user_id, name, preference_name, value) => {
    const storage_item_name = `preferences.${user_id}.${preference_name}`
    state[name] = value
    localStorage.setItem(storage_item_name, value)
}

export const PreferencesModule = {
    namespaced: true,
    state: {
        verbose: null,
        show_search_form: null,
        show_drawer: null,
        selected_language: null,
    },
    actions: {
        set_verbose({commit, rootGetters}, value) {
            const user_id = rootGetters['general/user'].username
            commit('set_verbose', {value, user_id})
        },
        set_show_search_form({commit, rootGetters}, value) {
            const user_id = rootGetters['general/user'].username
            commit('set_show_search_form', {value, user_id})
        },
        set_show_drawer({commit, rootGetters}, value) {
            const user_id = rootGetters['general/user'].username
            commit('set_show_drawer', {value, user_id})
        },
        set_selected_language({commit, rootGetters}, value) {
            const user_id = rootGetters['general/user'].username
            commit('set_selected_language', {value, user_id})
        },
    },
    mutations: {
        set_verbose(state, {value, user_id}) {
            set_preference_value(state, user_id, 'verbose', 'verbose_view', value ? 'true' : 'false')
        },
        set_show_search_form(state, {value, user_id}) {
            set_preference_value(state, user_id, 'show_search_form', 'show_search_form', value ? 'true' : 'false')
        },
        set_show_drawer(state, {value, user_id}) {
            set_preference_value(state, user_id, 'show_drawer', 'show_drawer', value ? 'true' : 'false')
        },
        set_selected_language(state, {value, user_id}) {
            set_preference_value(state, user_id, 'selected_language', 'selected_language', value)
        },
    },
    getters: {
        verbose(state, getters, rootState, rootGetters) {
            const user_id = rootGetters['general/user'].username
            return get_preference_value(state, user_id, 'verbose', 'verbose_view', 'false')
                .toLowerCase() === 'true' ? true : false
        },
        show_search_form(state, getters, rootState, rootGetters) {
            const user_id = rootGetters['general/user'].username
            return get_preference_value(state, user_id, 'show_search_form', 'show_search_form', 'true')
                .toLowerCase() === 'true' ? true : false
        },
        show_drawer(state, getters, rootState, rootGetters) {
            const user_id = rootGetters['general/user'].username
            return get_preference_value(state, user_id, 'show_drawer', 'show_drawer', 'true')
                .toLowerCase() === 'true' ? true : false
        },
        selected_language(state, getters, rootState, rootGetters) {
            const user_id = rootGetters['general/user'].username
            return get_preference_value(state, user_id, 'selected_language', 'selected_language',
                rootGetters['settings/LANGUAGE_CODE'])
        },
    },
}

export const Store = {
    modules: {
        general: GeneralModule,
        preferences: PreferencesModule,
        search: SearchModule,
        settings: SettingsModule,
        history: HistoryModule,
    },
}
