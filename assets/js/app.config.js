import VueI18Next from '@panter/vue-i18next'
import axios from 'axios'
import i18next from 'i18next'
import Fetch from 'i18next-fetch-backend'
import LocalStorageBackend from 'i18next-localstorage-backend'
import Vue from 'vue'
import VueRouter from 'vue-router'
import vuetify_cs from 'vuetify/es5/locale/cs'
import vuetify_en from 'vuetify/es5/locale/en'
import vuetify_colors from 'vuetify/es5/util/colors'
import Vuex, { Store as VuexStore } from 'vuex'
import YAML from 'yaml'

import { Store } from 'FerdaJS/store'
import { URLS } from 'ferda/Urls'
import PageNotFound from 'ferda/common/PageNotFound.vue'
import { ICON_VALUES } from 'ferda/icons.js'

Vue.use(Vuex)
Vue.use(VueI18Next)
Vue.use(VueRouter)

// Setup default settings for router and extend them at module level
export const router = new VueRouter({ mode: 'history', routes: [{ path: '*', component: PageNotFound }] })

export const initialize_store = async(vuex_store, is_login_page = false) => {
    let user_settings_response
    let django_settings_response
    try {
        if (is_login_page) {
            django_settings_response = await axios.get(URLS.django_settings_ajax())
        } else {
            [user_settings_response, django_settings_response] = await Promise.all([
                axios.get(URLS.user_settings_ajax()),
                axios.get(URLS.django_settings_ajax()),
            ])
            const { permissions, ...user_profile } = user_settings_response.data
            vuex_store.commit('general/set_user', user_profile)
            vuex_store.commit('general/set_permissions', permissions)
        }
    } catch (error) {
        console.error(`Settings download failed: ${error}`)
        return
    }

    const { common, registry } = django_settings_response.data

    vuex_store.commit('settings/set_time_zone', common.time_zone)
    vuex_store.commit('settings/set_language_code', common.language_code)
    vuex_store.commit('settings/set_languages', common.languages)
    vuex_store.commit('settings/set_fido2_authentication', common.fido2_authentication)
    vuex_store.commit('settings/set_max_file_upload_size', common.max_file_upload_size)
    vuex_store.commit('settings/set_manual_in_zone_default_duration', registry.manual_in_zone_default_duration)
    vuex_store.commit('settings/set_object_detail_logger_services', registry.object_detail_logger_services)

    return { user_settings: user_settings_response?.data ?? null, django_settings: django_settings_response.data }
}

export const store = new VuexStore(Store)

export const get_vuetify_options = () => ({
    lang: {
        locales: { en: vuetify_en, cs: vuetify_cs },
        current: store.getters['preferences/selected_language']
            || store.getters['settings/LANGUAGE_CODE']
            || 'en',
    },
    theme: {
        themes: {
            dark: {
                accent: vuetify_colors.blue.lighten4,
                action_add: vuetify_colors.grey.darken3,
                action_anchor: vuetify_colors.grey.darken3,
                action_delete: vuetify_colors.grey.darken3,
                action_edit: vuetify_colors.grey.darken3,
                action_export: vuetify_colors.grey.darken3,
                action_refresh: vuetify_colors.grey.darken3,
                action_search: vuetify_colors.grey.darken3,
                action_toggle: vuetify_colors.grey.darken3,
                primary: vuetify_colors.blue.base,
                secondary: vuetify_colors.amber.base,
                tooltip_bg: '#000000',
            },
            light: {
                accent: vuetify_colors.blue.lighten4,
                action_add: vuetify_colors.grey.darken3,
                action_anchor: vuetify_colors.grey.darken3,
                action_delete: vuetify_colors.grey.darken3,
                action_edit: vuetify_colors.grey.darken3,
                action_export: vuetify_colors.grey.darken3,
                action_refresh: vuetify_colors.grey.darken3,
                action_search: vuetify_colors.grey.darken3,
                action_toggle: vuetify_colors.grey.darken3,
                primary: vuetify_colors.blue.base,
                secondary: vuetify_colors.amber.base,
                tooltip_bg: '#000000',
            },
        },
    },
    icons: {
        iconfont: 'mdiSvg',
        values: ICON_VALUES,
    },
    options: {
        customProperties: true,
    },
})

export const i18n = new VueI18Next(i18next)

export const get_i18next_options = (ferda_version) => ({
    lng: store.getters['preferences/selected_language'],
    fallbackLng: [store.getters['settings/LANGUAGE_CODE'], 'en'],
    ns: [
        'common',
        'notifications',
        'logger',
        'login',
        'search',
        'contact',
        'domain',
        'nsset',
        'keyset',
        'registrar',
        'rule',
        'message',
        'report',
    ],
    defaultNS: 'common',
    backend: {
        backends: [LocalStorageBackend, Fetch],
        backendOptions: [
            {
                versions: store.getters['settings/LANGUAGES'].reduce((language_versions, lang) => {
                    language_versions[lang] = ferda_version
                    return language_versions
                }, {}),
            },
            {
                loadPath: '/static/ferda/locales/{{lng}}/{{ns}}.yml',
                requestOptions: {
                    cache: 'reload',
                },
                parse: data => YAML.parse(data),
            },
        ],
    },
})
