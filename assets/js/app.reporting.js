import i18next from 'i18next'
import Vue from 'vue'
import Vuetify from 'vuetify/lib'

import { get_vuetify_options, i18n, store } from 'FerdaJS/app.config'
import ReportDetail from 'ferda/reports/ReportDetail.vue'
import ReportList from 'ferda/reports/ReportList.vue'

i18next.on('initialized', () => {
    const reports_list_el = document.querySelector('#reports-list')
    if (reports_list_el) {
        new Vue({
            el: '#reports-list',
            name: 'ReportList',
            vuetify: new Vuetify(get_vuetify_options()),
            i18n,
            components: {
                ReportList,
            },
            store,
            render: createElement => {
                const reports = JSON.parse(document.querySelector('#reports-data').textContent)
                return createElement(ReportList, {
                    props: {
                        reports,
                    },
                })
            },
        })
    }

    const report_detail_el = document.querySelector('#report-detail')
    if (report_detail_el) {
        new Vue({
            el: '#report-detail',
            name: 'ReportDetail',
            vuetify: new Vuetify(get_vuetify_options()),
            i18n,
            components: {
                ReportDetail,
            },
            store,
            render: createElement => {
                const report = JSON.parse(document.querySelector('#report-data').textContent)
                return createElement(ReportDetail, {
                    props: {
                        report,
                    },
                })
            },
        })
    }
})
