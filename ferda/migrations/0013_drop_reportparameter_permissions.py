# Generated by Django 2.2.23 on 2021-05-26 07:58

from django.db import migrations

DELETE_PERMISSIONS = (
    "add_reportparameter",
    "change_reportparameter",
    "delete_reportparameter",
    "view_reportparameter",
)


def delete_permissions(apps, schema_editor):
    ContentType = apps.get_model("contenttypes", "ContentType")
    Permission = apps.get_model("auth", "Permission")
    ReportParameter = apps.get_model("ferda", "ReportParameter")
    content_type = ContentType.objects.get_for_model(ReportParameter)

    Permission.objects.filter(content_type=content_type, codename__in=DELETE_PERMISSIONS).delete()


class Migration(migrations.Migration):
    dependencies = [
        ("auth", "0001_initial"),
        ("contenttypes", "0001_initial"),
        ("ferda", "0012_drop_can_run_reports_permission"),
    ]

    operations = [
        migrations.AlterModelOptions(
            name="reportparameter",
            options={
                "default_permissions": (),
                "ordering": ("order",),
                "verbose_name": "Report parameter",
                "verbose_name_plural": "Report parameters",
            },
        ),
        migrations.RunPython(delete_permissions, migrations.RunPython.noop),
    ]
