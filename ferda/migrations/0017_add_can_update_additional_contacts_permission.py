# Generated by Django 3.2.11 on 2022-05-10 11:51

from django.db import migrations


class Migration(migrations.Migration):
    dependencies = [
        ("ferda", "0016_add_can_set_manual_in_zone_permission"),
    ]

    operations = [
        migrations.AlterModelOptions(
            name="user",
            options={
                "permissions": [
                    ("can_view_registry", "Can view basic registry content"),
                    ("can_view_authinfo", "Can view AuthInfo"),
                    ("can_view_registrar_credit", "Can view registrar credit"),
                    ("can_set_manual_in_zone", "Can set manual in zone domain state flag"),
                    ("can_update_additional_contacts", "Can update additional outzone/delete notify contacts"),
                ]
            },
        ),
    ]
