# Generated by Django 2.2.19 on 2021-03-25 09:12

from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ("ferda", "0008_add_report_and_reportparameter"),
    ]

    operations = [
        migrations.RemoveConstraint(
            model_name="reportparameter",
            name="unique_report_parameter_name",
        ),
        migrations.AddConstraint(
            model_name="reportparameter",
            constraint=models.UniqueConstraint(
                fields=("report", "name", "is_input"), name="unique_report_parameter_name"
            ),
        ),
    ]
