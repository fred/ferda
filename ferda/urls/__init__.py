"""Ferda urls."""

from asgiref.sync import sync_to_async
from django.conf import settings
from django.contrib.auth import views as auth_views
from django.urls import include, path, register_converter, reverse_lazy
from django.urls.converters import UUIDConverter
from django.views.decorators.csrf import ensure_csrf_cookie
from ninja import NinjaAPI
from ninja.security import django_auth

from ferda.api import PydanticJsonRenderer
from ferda.common.api import router as common_router
from ferda.converters import DatetimeConverter, QuoteConverter
from ferda.logger.api import router as logger_router
from ferda.messages.api import router as messages_router
from ferda.registry.api import router as registry_router
from ferda.views.ajax.common import countries_list
from ferda.views.ajax.monitoring import MonitoringView
from ferda.views.auth import LoginFidoView, LoginView

register_converter(DatetimeConverter, "datetime")
register_converter(QuoteConverter, "quote")

# Due to a bug in django-ninja, `uuid` converter is not plausible for our existing views.
# Therefore we use `UUID` name for default django converter.
# See https://github.com/vitalik/django-ninja/issues/280
register_converter(UUIDConverter, "UUID")

app_name = "ferda"

api = NinjaAPI(
    version="1",
    urls_namespace="ferda:api",
    # Show API docs in debug mode only
    openapi_url=settings.DEBUG and "/openapi.json" or "",
    csrf=True,
    auth=sync_to_async(django_auth),
    renderer=PydanticJsonRenderer(),
)
api.add_router("common", common_router)
api.add_router("logger", logger_router)
api.add_router("messages", messages_router)
api.add_router("registry", registry_router)

urlpatterns = [
    # Common views
    path("accounts/login/", ensure_csrf_cookie(LoginView.as_view()), name="login"),
    path("accounts/login-fido/", ensure_csrf_cookie(LoginFidoView.as_view()), name="login-fido"),
    path("accounts/logout/", auth_views.LogoutView.as_view(next_page=reverse_lazy("ferda:login")), name="logout"),
    path("i18n/", include("django.conf.urls.i18n")),
    path("countries/", countries_list, name="countries"),
    # Monitoring API
    path("api/monitoring/", MonitoringView.as_view(), name="monitoring"),
    # Ferda app namespaces
    path("registry/", include("ferda.urls.registry", namespace="registry")),
    path("messages/", include("ferda.urls.messages", namespace="messages")),
    path("files/", include("ferda.urls.files", namespace="files")),
    path("reports/", include("ferda.urls.reports", namespace="reports")),
    path("logger/", include("ferda.urls.logger", namespace="logger")),
    path("notification/", include("ferda.urls.notification", namespace="notification")),
    path("api/", api.urls),
]
