"""Logger urls."""

from django.urls import re_path

from ferda.views.logger import LoggerIndexView

app_name = "logger"

# Serves both list and detail views, they are handled by vue-router.
urlpatterns = [
    re_path("", LoggerIndexView.as_view(), name="index"),
]
