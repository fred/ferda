"""Report urls."""

from django.urls import path

from ferda.views.ajax.reports import ReportRunView
from ferda.views.reports import ReportDetailView, ReportListView

app_name = "reports"

urlpatterns = [
    path("list/", ReportListView.as_view(), name="list"),
    path("detail/<str:service>/<str:report_name>/", ReportDetailView.as_view(), name="detail"),
    path("ajax/run/<str:service>/<str:report_name>/", ReportRunView.as_view(), name="ajax_run"),
]
