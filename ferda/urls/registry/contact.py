"""Registry urls - namespace contact."""

from django.urls import path

from ferda.views import ContactDetailView, ContactHistoryView
from ferda.views.ajax.contact import ContactInfoView, ContactStateHistoryView, ContactStateView, contact_badges

app_name = "contact"
urlpatterns = [
    path("contact/<UUID:object_id>/", ContactDetailView.as_view(), name="detail"),
    path("contact_history/<UUID:object_id>/", ContactHistoryView.as_view(), name="history"),
    path("ajax/contact_info/<UUID:object_id>/", ContactInfoView.as_view(), name="ajax_info"),
    path("ajax/contact_info/<UUID:object_id>/history/<UUID:history_id>/", ContactInfoView.as_view(), name="ajax_info"),
    path(
        "ajax/contact_info/<UUID:object_id>/history/<datetime:history_id>/",
        ContactInfoView.as_view(),
        name="ajax_info_by_datetime",
    ),
    path("ajax/contact_state/<UUID:object_id>/", ContactStateView.as_view(), name="ajax_state"),
    path("ajax/contact_state_history/<UUID:object_id>/", ContactStateHistoryView.as_view(), name="ajax_state_history"),
    path("ajax/contact_badges/<UUID:contact_id>/", contact_badges, name="ajax_badges"),
]
