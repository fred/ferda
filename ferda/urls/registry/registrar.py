"""Registry urls - namespace registrar."""

from django.urls import path

from ferda.views import RegistrarDetailView, RegistrarListView

app_name = "registrar"
urlpatterns = [
    path("<str:handle>/", RegistrarDetailView.as_view(), name="detail"),
    path("", RegistrarListView.as_view(), name="list"),
]
