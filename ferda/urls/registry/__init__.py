"""Ferda registry urls."""

from django.urls import include, path

app_name = "registry"
urlpatterns = [
    path("", include("ferda.urls.registry.search", namespace="search")),
    path("", include("ferda.urls.registry.contact", namespace="contact")),
    path("", include("ferda.urls.registry.domain", namespace="domain")),
    path("", include("ferda.urls.registry.keyset", namespace="keyset")),
    path("", include("ferda.urls.registry.nsset", namespace="nsset")),
    path("registrar/", include("ferda.urls.registry.registrar", namespace="registrar")),
]
