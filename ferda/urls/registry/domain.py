"""Registry urls - namespace domain."""

from django.urls import path

from ferda.views import DomainDetailView, DomainHistoryView
from ferda.views.ajax.domain import DomainInfoView, DomainStateHistoryView, DomainStateView

app_name = "domain"
urlpatterns = [
    path("domain/<UUID:object_id>/", DomainDetailView.as_view(), name="detail"),
    path("domain_history/<UUID:object_id>/", DomainHistoryView.as_view(), name="history"),
    path("ajax/domain_info/<UUID:object_id>/", DomainInfoView.as_view(), name="ajax_info"),
    path("ajax/domain_info/<UUID:object_id>/history/<UUID:history_id>/", DomainInfoView.as_view(), name="ajax_info"),
    path("ajax/domain_state/<UUID:object_id>/", DomainStateView.as_view(), name="ajax_state"),
    path("ajax/domain_state_history/<UUID:object_id>/", DomainStateHistoryView.as_view(), name="ajax_state_history"),
]
