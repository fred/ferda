"""Registry urls - namespace nsset."""

from django.urls import path

from ferda.views import NssetDetailView, NssetHistoryView
from ferda.views.ajax.nsset import NssetInfoView, NssetStateHistoryView, NssetStateView

app_name = "nsset"
urlpatterns = [
    path("nsset/<UUID:object_id>/", NssetDetailView.as_view(), name="detail"),
    path("nsset_history/<UUID:object_id>/", NssetHistoryView.as_view(), name="history"),
    path("ajax/nsset_info/<UUID:object_id>/", NssetInfoView.as_view(), name="ajax_info"),
    path("ajax/nsset_info/<UUID:object_id>/history/<UUID:history_id>/", NssetInfoView.as_view(), name="ajax_info"),
    path(
        "ajax/nsset_info/<UUID:object_id>/history/<datetime:history_id>/",
        NssetInfoView.as_view(),
        name="ajax_info_by_datetime",
    ),
    path("ajax/nsset_state/<UUID:object_id>/", NssetStateView.as_view(), name="ajax_state"),
    path("ajax/nsset_state_history/<UUID:object_id>/", NssetStateHistoryView.as_view(), name="ajax_state_history"),
]
