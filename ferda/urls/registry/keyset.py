"""Registry urls - namespace keyset."""

from django.urls import path

from ferda.views import KeysetDetailView, KeysetHistoryView
from ferda.views.ajax.keyset import KeysetInfoView, KeysetStateHistoryView, KeysetStateView

app_name = "keyset"
urlpatterns = [
    path("keyset/<UUID:object_id>/", KeysetDetailView.as_view(), name="detail"),
    path("keyset_history/<UUID:object_id>/", KeysetHistoryView.as_view(), name="history"),
    path("ajax/keyset_info/<UUID:object_id>/", KeysetInfoView.as_view(), name="ajax_info"),
    path("ajax/keyset_info/<UUID:object_id>/history/<UUID:history_id>/", KeysetInfoView.as_view(), name="ajax_info"),
    path(
        "ajax/keyset_info/<UUID:object_id>/history/<datetime:history_id>/",
        KeysetInfoView.as_view(),
        name="ajax_info_by_datetime",
    ),
    path("ajax/keyset_state/<UUID:object_id>/", KeysetStateView.as_view(), name="ajax_state"),
    path("ajax/keyset_state_history/<UUID:object_id>/", KeysetStateHistoryView.as_view(), name="ajax_state_history"),
]
