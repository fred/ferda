"""Notification urls."""

from django.urls import path

from ferda.views.notification import NotificationIndexView

app_name = "notification"

urlpatterns = [
    path("", NotificationIndexView.as_view(), name="index"),
]
