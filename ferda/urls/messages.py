"""Registry urls - namespace messages."""

from django.urls import path

from ferda.constants import MessageType
from ferda.views import MessageDetailView, MessageListView

app_name = "messages"

urlpatterns = [
    path(
        "email/<quote:message_id>/",
        MessageDetailView.as_view(),
        kwargs={"type": MessageType.EMAIL},
        name="email_detail",
    ),
    path("sms/<quote:message_id>/", MessageDetailView.as_view(), kwargs={"type": MessageType.SMS}, name="sms_detail"),
    path(
        "letter/<quote:message_id>/",
        MessageDetailView.as_view(),
        kwargs={"type": MessageType.LETTER},
        name="letter_detail",
    ),
    path("list/", MessageListView.as_view(), name="list"),
]
