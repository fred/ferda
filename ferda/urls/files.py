"""Registry urls - namespace files."""

from django.urls import path

from ferda.views.ajax.files import FileReadView

app_name = "files"

urlpatterns = [
    path("<UUID:file_uuid>/", FileReadView.as_view(), name="read"),
]
