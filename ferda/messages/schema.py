"""Messages API schemas."""

from typing import Any, Dict, List

from hermes import Email, EmailEnvelope, Letter, LetterEnvelope, Sms, SmsEnvelope
from pydantic import Field
from typist import TemplateCollection


class EmailSchema(Email):
    """Email schema with attachments info."""

    attachments: List[Dict[str, Any]] = []  # type: ignore[assignment]


class EmailEnvelopeSchema(EmailEnvelope):
    """Email envelope schema."""

    message: EmailSchema = Field(alias="message_data")  # type: ignore[assignment]


class LetterSchema(Letter):
    """Letter schema with file info."""

    file: Dict[str, Any]  # type: ignore


class LetterEnvelopeSchema(LetterEnvelope):
    """Letter envelope schema."""

    message: LetterSchema = Field(alias="message_data")  # type: ignore[assignment]


class SmsSchema(Sms):
    """Sms schema."""


class SmsEnvelopeSchema(SmsEnvelope):
    """Sms envelope schema."""

    message: SmsSchema = Field(alias="message_data")  # type: ignore[assignment]


class TemplateCollectionSchema(TemplateCollection):
    """Template collection schema."""

    tags: List[str] = []  # type: ignore[assignment]
