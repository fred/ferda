"""Messenger REST API views."""

import logging
from datetime import datetime, timedelta
from functools import partial
from typing import Any, Dict, List, Union, cast
from uuid import UUID

import pytz
from asgiref.sync import sync_to_async
from grill import References
from hermes import EmailEnvelope, LetterEnvelope, SmsEnvelope
from hermes.client import NoMessage
from hermes.message import Reference
from ninja import Query, Router, Schema
from ninja.responses import Response
from pydantic import Field, PositiveInt

from ferda.api import Error, PaginatedList, RegistryReference, build_full_url, check_permissions
from ferda.backend import FILE_STORAGE, LOGGER, MESSAGES, SECRETARY
from ferda.constants import LOGGER_SESSION_KEY, LogEntryType, LoggerResult, MessageType

from .schema import EmailEnvelopeSchema, LetterEnvelopeSchema, SmsEnvelopeSchema, TemplateCollectionSchema

_LOGGER = logging.getLogger(__name__)

DEFAULT_PAGE_SIZE = 10

DETAIL_LOG_ENTRY_TYPES = {
    MessageType.EMAIL: LogEntryType.EMAIL_MESSAGE_DETAIL,
    MessageType.LETTER: LogEntryType.LETTER_MESSAGE_DETAIL,
    MessageType.SMS: LogEntryType.SMS_MESSAGE_DETAIL,
}

LIST_LOG_ENTRY_TYPES = {
    MessageType.EMAIL: LogEntryType.EMAIL_MESSAGE_LIST,
    MessageType.LETTER: LogEntryType.LETTER_MESSAGE_LIST,
    MessageType.SMS: LogEntryType.SMS_MESSAGE_LIST,
}

router = Router(tags=["messages"])

messages_permissions = partial(check_permissions, permissions="ferda.can_view_registry")


def get_envelope_data(envelope: Union[EmailEnvelope, LetterEnvelope, SmsEnvelope]) -> Dict[str, Any]:
    """Get envelope data.

    This function encodes envelope to a dict and fetches attachment and file info from fileman.
    """
    result = envelope.model_dump()

    # Fetch attachment stats
    if hasattr(envelope.message, "attachments"):
        attachments = []
        for attachment in envelope.message.attachments:  # type: ignore[union-attr]
            attachments.append(file_stat(attachment))
        result["message"]["attachments"] = attachments

    # Fetch file stats
    if hasattr(envelope.message, "file"):
        result["message"]["file"] = file_stat(envelope.message.file)  # type: ignore[union-attr]

    return result


def file_stat(uid: Union[UUID, str]) -> Dict[str, Any]:
    """Fetch file stat."""
    uid = str(uid)

    try:
        file = FILE_STORAGE.open(UUID(uid))
        return {
            "uuid": uid,
            "name": file.name,
            "mimetype": file.mimetype,
            "size": file.size,
        }
    except FileNotFoundError:
        _LOGGER.warning("File %s was not found", uid)
        return {
            "uuid": uid,
            "error": "File not found",
        }
    except ConnectionAbortedError:
        _LOGGER.error("Connection failed when getting file %s", uid)
        return {
            "uuid": uid,
            "error": "Connection failed",
        }


@router.get("/subtypes/", url_name="get_message_subtypes", response=List[TemplateCollectionSchema])
def get_message_subtypes(request, message_types: List[MessageType] = Query(None)):
    """Get list of message subtypes."""
    messages_permissions(request)

    return Response(
        [template.model_dump(mode="json") for template in SECRETARY.get_template_collections(tags=message_types)]
    )


class ListMessagesQuery(Schema):
    """List log entries query params."""

    time_begin_from: datetime = datetime(1970, 1, 1, tzinfo=pytz.utc)
    time_begin_to: datetime = Field(default_factory=(lambda: datetime.now(tz=pytz.utc)))
    offset: float = 0.0
    recipients: List[str] = []
    body_templates: List[str] = []
    types: List[str] = []
    references: List[RegistryReference] = []
    page_size: PositiveInt = DEFAULT_PAGE_SIZE


@router.get(
    "/{message_type}/",
    url_name="list_messages",
    response={
        200: PaginatedList[Union[EmailEnvelopeSchema, LetterEnvelopeSchema, SmsEnvelopeSchema]],
        404: Error,
    },
)
async def list_messages(request, message_type: MessageType, query: ListMessagesQuery = Query(None)):
    """List messages."""
    await sync_to_async(messages_permissions)(request)

    if query.offset:
        timestamp_to = query.time_begin_to - timedelta(seconds=query.offset)
    else:
        timestamp_to = query.time_begin_to

    references: References = {}
    for ref in query.references:
        refs = cast(List[str], references.setdefault(ref.type, []))
        refs.append(ref.value)

    log_entry = LOGGER.create(
        LIST_LOG_ENTRY_TYPES[message_type],
        source_ip=request.META.get("REMOTE_ADDR", ""),
        session_id=request.session[LOGGER_SESSION_KEY],
        properties={
            "time_begin_from": query.time_begin_from.isoformat(),
            "time_begin_to": query.time_begin_to.isoformat(),
            "recipients": query.recipients,
            "body_templates": query.body_templates,
            "types": query.types,
            "page_size": str(query.page_size),
            "offset": str(query.offset),
        },
        references=references,
    )
    with log_entry:
        envelopes = [
            get_envelope_data(envelope)
            async for envelope in MESSAGES[message_type].list(
                created_from=query.time_begin_from,
                created_to=timestamp_to,
                recipients=query.recipients,
                body_templates=query.body_templates,
                types=query.types,
                references=[Reference(type=ref.type, value=ref.value) for ref in query.references],
                limit=query.page_size + 1,
            )
        ]

        next_url = None
        if len(envelopes) > query.page_size:
            envelopes.pop()
            next_offset = query.time_begin_to - envelopes[-1]["create_datetime"]
            next_url = build_full_url(
                request,
                {
                    "time_begin_to": query.time_begin_to.isoformat(),
                    "offset": next_offset.total_seconds(),
                },
            )

        result = {
            "next": next_url,
            "results": envelopes,
        }

        log_entry.result = LoggerResult.SUCCESS
        return Response(result)


@router.get(
    "/{message_type}/{message_id}/",
    url_name="get_message",
    response={
        200: Union[EmailEnvelopeSchema, LetterEnvelopeSchema, SmsEnvelopeSchema],
        404: Error,
    },
)
async def get_message(request, message_type: MessageType, message_id: str):
    """Get message by id."""
    await sync_to_async(messages_permissions)(request)

    log_entry = LOGGER.create(
        DETAIL_LOG_ENTRY_TYPES[message_type],
        source_ip=request.META.get("REMOTE_ADDR", ""),
        session_id=request.session[LOGGER_SESSION_KEY],
        references={"message": message_id},
    )
    with log_entry:
        try:
            envelope = await MESSAGES[message_type].get(message_id)
            data = get_envelope_data(envelope)

            log_entry.result = LoggerResult.SUCCESS
            return Response(data)
        except NoMessage:
            log_entry.result = LoggerResult.FAIL
            return 404, Error(detail="Message not found")
