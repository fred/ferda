"""Web admin interface for FRED."""

import pluggy

from .constants import HOOK_MARKER

__version__ = "4.3.0"

hookimpl = pluggy.HookimplMarker(HOOK_MARKER)
