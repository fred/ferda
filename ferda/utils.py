"""Various ferda utils."""

import asyncio
from typing import Any, Awaitable, Iterable, Protocol, Tuple, runtime_checkable

from django.apps.registry import Apps


@runtime_checkable
class AnyStr(Protocol):
    """If it looks like a string, swims like a string, and quacks like a string, it probably is a string."""

    def capitalize(self) -> str:  # noqa: D102
        ...

    def lower(self) -> str:  # noqa: D102
        ...

    def upper(self) -> str:  # noqa: D102
        ...


class AbcAttributeType:
    """Abstract attribute type.

    Use instance of this class as default value of ABC
    attribute in order to enforce class attribute definition.
    """

    __isabstractmethod__ = True


# Abstract attribute singleton
AbcAttribute: Any = AbcAttributeType()


def removeprefix(value: str, prefix: str) -> str:
    """Remove string prefix if it exists."""
    # XXX: Replace with `str.removeprefix` in Python 3.9+
    if value.startswith(prefix):
        return value.replace(prefix, "", 1)
    else:
        return value


async def await_with_semaphore(call: Awaitable, semaphore: asyncio.Semaphore) -> Any:
    """Await awaitable with semaphore."""
    async with semaphore:
        return await call


def create_permissions(permissions: Iterable[Tuple[str, str]], apps: Apps, **kwargs: Any) -> None:
    """Create permissions."""
    Permission = apps.get_model("auth", "Permission")
    ContentType = apps.get_model("contenttypes", "ContentType")

    for name, description in permissions:
        app_name, codename = name.split(".", maxsplit=1)
        content_type, _ = ContentType.objects.get_or_create(model=app_name, app_label=app_name)
        Permission.objects.get_or_create(codename=codename, name=description, content_type=content_type)
