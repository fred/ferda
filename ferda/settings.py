"""Ferda settings."""

import os
from pathlib import Path
from typing import Any, Dict, Iterable, Optional

from appsettings import (
    AppSettings,
    DictSetting,
    FileSetting,
    IntegerSetting,
    ListSetting,
    NestedListSetting,
    ObjectSetting,
    Setting,
    StringSetting,
)
from django.core.exceptions import ValidationError
from django.utils.translation import gettext_lazy as _
from typist import HTTPTokenAuth

from ferda.constants import StateFlag
from ferda.registry.plugins.badges import AbstractBadger

DEFAULT_STATE_FLAGS_DESCRIPTIONS = {
    "deleteCandidate": _("To be deleted"),
    "linked": _("Has relation to other records in the registry"),
    "serverBlocked": _("Administratively blocked"),
    "conditionallyIdentifiedContact": _("Contact is conditionally identified"),
    "identifiedContact": _("Contact is identified"),
    "validatedContact": _("Contact is validated"),
    "serverDeleteProhibited": _("Deletion forbidden"),
    "serverTransferProhibited": _("Sponsoring registrar change forbidden"),
    "serverUpdateProhibited": _("Update forbidden"),
    "serverRegistrantChangeProhibited": _("Registrant change forbidden"),
    "serverRenewProhibited": _("Registration renewal forbidden"),
    "expirationWarning": _("The domain expires in 30 days"),
    "deleteWarning": _("The domain will be deleted in 11 days"),
    "validationWarning1": _("The domain validation expires in 30 days"),
    "validationWarning2": _("The domain validation expires in 15 days"),
    "unguarded": _("The domain is 30 days after expiration"),
    "outzoneUnguarded": _("The domain is out of zone after 30 days in expiration state"),
    "nssetMissing": _("The domain doesn't have associated nsset"),
    "expired": _("Domain expired"),
    "outzone": _("The domain isn't generated in the zone"),
    "serverOutzoneManual": _("The domain is administratively kept out of zone"),
    StateFlag.SERVER_INZONE_MANUAL: _("The domain is administratively kept in zone"),
    "notValidated": _("Domain not validated"),
    "outzoneUnguardedWarning": _("The domain is to be out of zone soon."),
    "serverContactNameChangeProhibited": _("Name update forbidden"),
    "serverContactOrganizationChangeProhibited": _("Organization update forbidden"),
    "serverContactIdentChangeProhibited": _("Ident update forbidden"),
    "serverContactPermanentAddressChangeProhibited": _("Permanent address update forbidden"),
    "premiumDomain": _("Premium domain"),
    "serverLinkProhibited": _("Contact linking forbidden"),
}

DEFAULT_STATE_FLAGS_GROUPS = [
    [
        "linked",
        "serverBlocked",
        "deleteCandidate",
        "serverDeleteProhibited",
        "serverTransferProhibited",
        "serverUpdateProhibited",
        "serverRegistrantChangeProhibited",
        "serverRenewProhibited",
        "serverContactNameChangeProhibited",
        "serverContactOrganizationChangeProhibited",
        "serverContactIdentChangeProhibited",
        "serverContactPermanentAddressChangeProhibited",
    ],
    [
        "serverOutzoneManual",
        StateFlag.SERVER_INZONE_MANUAL,
    ],
]


def timeout_validator(value):
    """Validate timeouts - must contain a number or tuple with two numbers."""
    if isinstance(value, (float, int)):
        return
    if isinstance(value, tuple) and len(value) == 2 and all(isinstance(v, (float, int)) for v in value):
        return
    raise ValidationError(
        "Value %(value)s must be a float, int or a tuple with 2 float or int items.", params={"value": value}
    )


class UpdatableDictSetting(DictSetting):
    """Updatable dict setting."""

    def transform(self, value):
        """Update default value with setting value."""
        default = self.default.copy()
        default.update(value)
        return default


class SubclassSetting(ObjectSetting):
    """Object setting with subclass check."""

    message = "Class %(value)s is not a subclass of %(superclass)s."

    def __init__(self, *args, superclasses: Iterable = (), **kwargs):
        """Initialize the setting.

        Args:
            args: Args passed to parent.
            kwargs: Kwargs passed to parent.
            superclasses: Iterable of superclasses. Value has to be a subclass of all provided superclasses.
        """
        super().__init__(*args, **kwargs)
        self.superclasses = superclasses

    def validate(self, value):
        """Check if class is subclass of superclasses.

        At the moment this cannot be done in the validator, since this check requires transformed value.
        """
        value = self.get_value()  # We need to check transformed value
        for superclass in self.superclasses:
            if not issubclass(value, superclass):
                params = {"value": value.__name__, "superclass": superclass.__name__}
                raise ValidationError(self.message, params=params)


class DictWithGrpcServicesSetting(DictSetting):
    """Dictionary with gRPC services.

    Values have to be dicts with NETLOC (required) and SSL_CERT (optional) keys.
    """

    def __init__(self, *args: Any, **kwargs: Any) -> None:
        super().__init__(*args, key_type=str, value_type=dict, **kwargs)

    def validate(self, value: Dict[str, dict]) -> None:
        """Check whether value is a dictionary with gRPC settings as values."""
        for val in value.values():
            netloc = val.get("NETLOC")
            ssl_cert = val.get("SSL_CERT")
            if not isinstance(netloc, str):
                raise ValidationError("Value must contain key NETLOC of type str")
            if ssl_cert:
                if not isinstance(ssl_cert, (str, Path)):
                    raise ValidationError("Key SSL_CERT of value must be of type str or Path")
                ssl_cert = Path(ssl_cert)
                if not ssl_cert.exists() or not os.access(ssl_cert, os.R_OK):
                    raise ValidationError("Value of SSL_CERT key must be path to a readable file")
            if set(val.keys()) - {"NETLOC", "SSL_CERT"}:
                raise ValidationError("Only NETLOC and SSL_CERT keys are allowed in value")

    def transform(self, value):
        """Update default value with setting value."""
        for val in value.values():
            if "SSL_CERT" not in val:
                val["SSL_CERT"] = None
        return value


class TokenSetting(StringSetting):
    """Setting for HTTPTokenAuth."""

    def transform(self, value: Optional[str]) -> Optional[HTTPTokenAuth]:
        """Transform value according to it's type."""
        if value is None:
            return None
        else:
            return HTTPTokenAuth(value)


class FerdaSettings(AppSettings):
    """Application specific settings."""

    grpc_netloc = StringSetting(required=True)
    grpc_ssl_cert = FileSetting(default=None, mode=os.R_OK)
    logger_netloc = StringSetting(required=True)
    logger_ssl_cert = FileSetting(default=None, mode=os.R_OK)
    messenger_netloc = StringSetting(required=True)
    messenger_ssl_cert = FileSetting(default=None, mode=os.R_OK)
    fileman_netloc = StringSetting(required=True)
    fileman_ssl_cert = FileSetting(default=None, mode=os.R_OK)
    reports = DictWithGrpcServicesSetting(default=dict)
    secretary_url = StringSetting(required=True)
    secretary_timeout = Setting(default=3.05, validators=[timeout_validator])
    record_statement_timeout = Setting(default=(3.05, 30), validators=[timeout_validator])
    secretary_token = TokenSetting(default=None)
    state_flags_descriptions = UpdatableDictSetting(key_type=str, default=DEFAULT_STATE_FLAGS_DESCRIPTIONS)
    state_flags_groups = ListSetting(item_type=list, default=DEFAULT_STATE_FLAGS_GROUPS)
    badgers = NestedListSetting(default=[], inner_setting=SubclassSetting(superclasses=[AbstractBadger]))
    concurrent_grpc_calls_limit = IntegerSetting(default=10)
    manual_in_zone_default_duration = IntegerSetting(default=7 * 24 * 60 * 60)
    object_detail_logger_services = ListSetting(item_type=str, default=[])
    max_file_upload_size = IntegerSetting(default=10 * 10**6)  # 10 MB

    class Meta:
        """Meta class."""

        setting_prefix = "ferda_"


SETTINGS = FerdaSettings()
