"""Ferda app config."""

from functools import partial

from django.apps import AppConfig
from django.contrib.auth.signals import user_logged_in, user_logged_out
from django.db.models.signals import post_migrate
from django.utils.translation import gettext_lazy as _
from grill.exceptions import SessionDoesNotExist

from ferda.backend import LOGGER, LOGGER_CLIENT
from ferda.constants import (
    LOGGER_SERVICE,
    LOGGER_SERVICE_HANDLE,
    LOGGER_SESSION_KEY,
    LogEntryType,
    LoggerResult,
    LogReferenceType,
)
from ferda.settings import FerdaSettings
from ferda.utils import create_permissions


def create_session(sender, request, user, **kwargs):
    """Receive signal from user login and create logger session."""
    request.session[LOGGER_SESSION_KEY] = LOGGER.create_session(user.pk, user.username)


def close_session(sender, request, **kwargs):
    """Receive signal from user logout and close logger session."""
    session_id = request.session.pop(LOGGER_SESSION_KEY, None)
    if session_id:
        try:
            LOGGER.close_session(session_id)
        except SessionDoesNotExist:
            # Ignore the missing session id
            pass


class FerdaConfig(AppConfig):
    """Ferda app config."""

    name = "ferda"
    verbose_name = _("Web admin interface for FRED")

    def ready(self):
        """Check configuration and load plugins."""
        # We import here to avoid django app loading conflicts
        from ferda.plugin import get_plugin_manager

        LOGGER_CLIENT.register_service(LOGGER_SERVICE, handle=LOGGER_SERVICE_HANDLE)
        LOGGER_CLIENT.register_log_entry_types(LOGGER_SERVICE, log_entry_types=LogEntryType)
        LOGGER_CLIENT.register_results(LOGGER_SERVICE, results=LoggerResult)
        LOGGER_CLIENT.register_object_reference_types(reference_types=LogReferenceType)

        FerdaSettings.check()
        user_logged_in.connect(create_session)
        user_logged_out.connect(close_session)

        pm = get_plugin_manager()
        for permissions in pm.hook.create_permissions():
            post_migrate.connect(partial(create_permissions, permissions), sender=self, weak=False)
