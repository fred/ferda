"""Ferda test urls."""

from django.contrib import admin
from django.urls import include, path

urlpatterns = [
    path("", include("ferda.urls")),
    path("admin/", admin.site.urls),
    path("django_fido/", include("django_fido.urls")),
]
