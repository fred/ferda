from unittest.mock import MagicMock, call, sentinel

from django.apps import apps
from django.contrib.auth.models import Permission
from django.test import SimpleTestCase, TestCase

from ferda.utils import await_with_semaphore, create_permissions, removeprefix


class RemoveprefixTest(SimpleTestCase):
    def test_removeprefix(self):
        data = (
            ("abc", "x", "abc"),
            ("abc", "b", "abc"),
            ("abc", "a", "bc"),
            ("aac", "a", "ac"),
            ("aac", "a", "ac"),
            ("ababc", "ab", "abc"),
            ("ababab", "ab", "abab"),
        )
        for value, prefix, result in data:
            with self.subTest(value=value, prefix=prefix, result=result):
                self.assertEqual(removeprefix(value, prefix), result)


class AwaitWithSemaphore(SimpleTestCase):
    async def test_async_with_semaphore(self):
        semaphore = MagicMock(spec=("__aenter__", "__aexit__", "_call"))

        async def _awaitable(semaphore):
            semaphore._call(sentinel.result)
            return sentinel.result

        self.assertEqual(await await_with_semaphore(_awaitable(semaphore), semaphore), sentinel.result)
        self.assertEqual(
            semaphore.mock_calls,
            [
                call.__aenter__(),
                call._call(sentinel.result),
                call.__aexit__(None, None, None),
            ],
        )


class CreatePermissionsTest(TestCase):
    def test_create_permissions(self):
        permissions = (
            ("namespace.example1", "Example permission 1"),
            ("namespace.example2", "Example permission 2"),
        )
        self.assertEqual(
            Permission.objects.filter(
                codename__in=("example1", "example2"),
                content_type__model="namespace",
                content_type__app_label="namespace",
            ).count(),
            0,
        )

        create_permissions(permissions, apps=apps)

        self.assertEqual(
            Permission.objects.filter(
                codename__in=("example1", "example2"),
                content_type__model="namespace",
                content_type__app_label="namespace",
            ).count(),
            2,
        )
