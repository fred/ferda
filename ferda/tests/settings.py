"""Tests settings."""

SECRET_KEY = "Aguamenti!"

INSTALLED_APPS = [
    "django.contrib.contenttypes",
    "django.contrib.auth",
    "django.contrib.sessions",
    "django.contrib.messages",
    "django.contrib.staticfiles",
    "ferda.tests.apps.TestFerdaConfig",
    "django.contrib.admin",
    "django_countries",
    "adminsortable2",
    "django_fido.apps.DjangoFidoConfig",
    "guardian",
]

AUTH_USER_MODEL = "ferda.User"

AUTHENTICATION_BACKENDS = [
    "django.contrib.auth.backends.ModelBackend",
    "guardian.backends.ObjectPermissionBackend",
]
ANONYMOUS_USER_NAME = None

# Test speed optimization. Do not use in production!
PASSWORD_HASHERS = ["django.contrib.auth.hashers.MD5PasswordHasher"]

FERDA_REPORTS = {}  # type: ignore[var-annotated]
FERDA_FILEMAN_NETLOC = "localhost"
FERDA_GRPC_NETLOC = "localhost"
FERDA_LOGGER_NETLOC = "localhost"
FERDA_MESSENGER_NETLOC = "localhost"
FERDA_SECRETARY_URL = "http://localhost/api/"

STATIC_URL = "/static/"

MIDDLEWARE = [
    "django.contrib.sessions.middleware.SessionMiddleware",
    "django.contrib.auth.middleware.AuthenticationMiddleware",
    "django.contrib.messages.middleware.MessageMiddleware",
]

DATABASES = {"default": {"ENGINE": "django.db.backends.sqlite3", "NAME": ":memory:"}}

DEFAULT_AUTO_FIELD = "django.db.models.AutoField"

TEMPLATES = [
    {
        "BACKEND": "django.template.backends.django.DjangoTemplates",
        "DIRS": [],
        "APP_DIRS": True,
        "OPTIONS": {
            "context_processors": [
                "django.template.context_processors.request",
                "django.contrib.auth.context_processors.auth",
                "django.contrib.messages.context_processors.messages",
            ],
        },
    },
]

USE_TZ = True

# Apparently override_settings does not work for settings from django-countries
COUNTRIES_ONLY = ["CZ", "GB"]

TEST_RUNNER = "snapshottest.django.TestRunner"
