# snapshottest: v1 - https://goo.gl/zC4yUc

from snapshottest import Snapshot


snapshots = Snapshot()

snapshots["GetLogEntryTest::test_get_success 1"] = {
    "input_properties": {},
    "log_entry_id": "1#20220101T120000.000000#Ferda#0",
    "log_entry_type": "TypeA",
    "output_properties": {},
    "raw_request": None,
    "raw_response": None,
    "references": {},
    "result_code": 0,
    "result_name": "Success",
    "service": "ServiceA",
    "session_id": "session:1",
    "source_ip": "127.0.0.1",
    "time_begin": "2022-01-01T12:00:00Z",
    "time_end": "2022-01-01T13:00:00Z",
    "user_id": 0,
    "username": "RIMMER",
}

snapshots["ListLogEntriesTest::test_list_pagination 1"] = {
    "next": "http://testserver/api/logger/log_entry/?service=ServiceA&time_begin_from=2022-01-01T00%3A00%3A00%2B00%3A00&time_begin_to=2022-01-02T00%3A00%3A00%2B00%3A00&page_size=2&offset=82800.0",
    "results": [
        {
            "input_properties": {},
            "log_entry_id": "0",
            "log_entry_type": "TypeA",
            "output_properties": {},
            "raw_request": None,
            "raw_response": None,
            "references": {},
            "result_code": 0,
            "result_name": "Success",
            "service": "ServiceA",
            "session_id": "session:1",
            "source_ip": "127.0.0.1",
            "time_begin": "2022-01-01T00:00:00Z",
            "time_end": "2022-01-01T00:01:00Z",
            "user_id": 0,
            "username": "RIMMER",
        },
        {
            "input_properties": {},
            "log_entry_id": "1",
            "log_entry_type": "TypeA",
            "output_properties": {},
            "raw_request": None,
            "raw_response": None,
            "references": {},
            "result_code": 0,
            "result_name": "Success",
            "service": "ServiceA",
            "session_id": "session:1",
            "source_ip": "127.0.0.1",
            "time_begin": "2022-01-01T01:00:00Z",
            "time_end": "2022-01-01T01:01:00Z",
            "user_id": 0,
            "username": "RIMMER",
        },
    ],
}

snapshots["ListLogEntriesTest::test_list_pagination 2"] = {
    "next": None,
    "results": [
        {
            "input_properties": {},
            "log_entry_id": "2",
            "log_entry_type": "TypeA",
            "output_properties": {},
            "raw_request": None,
            "raw_response": None,
            "references": {},
            "result_code": 0,
            "result_name": "Success",
            "service": "ServiceA",
            "session_id": "session:1",
            "source_ip": "127.0.0.1",
            "time_begin": "2022-01-01T02:00:00Z",
            "time_end": "2022-01-01T02:01:00Z",
            "user_id": 0,
            "username": "RIMMER",
        }
    ],
}

snapshots["ListLogEntriesTest::test_list_success 1"] = {
    "next": None,
    "results": [
        {
            "input_properties": {},
            "log_entry_id": "1#20220101T120000.000000#Ferda#0",
            "log_entry_type": "TypeA",
            "output_properties": {},
            "raw_request": None,
            "raw_response": None,
            "references": {},
            "result_code": 0,
            "result_name": "Success",
            "service": "ServiceA",
            "session_id": "session:1",
            "source_ip": "127.0.0.1",
            "time_begin": "2022-01-01T12:00:00Z",
            "time_end": "2022-01-01T13:00:00Z",
            "user_id": 0,
            "username": "RIMMER",
        }
    ],
}
