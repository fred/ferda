"""Test admin views."""

from django.contrib.auth import get_user_model
from django.test import TestCase, override_settings
from django.urls import reverse

from ferda.tests.utils import LoggerMixin

User = get_user_model()


@override_settings(ROOT_URLCONF="ferda.tests.urls")
class TestUserAdmin(LoggerMixin, TestCase):
    """Test UserAdmin."""

    def setUp(self):
        super().setUp()
        self.admin = User.objects.create_superuser("admin", "admin@example.com", "password")

    def test_get_add(self):
        """Test GET request on add view."""
        self.client.force_login(self.admin)
        response = self.client.get(reverse("admin:ferda_user_add"))
        self.assertContains(
            response, "If you use external authentication system such as LDAP, you don't have to choose a password."
        )

    def test_post_add_password(self):
        """Test POST request on add view."""
        self.client.force_login(self.admin)
        response = self.client.post(
            reverse("admin:ferda_user_add"),
            data={"username": "yoda", "password1": "usetheforce", "password2": "usetheforce"},
        )
        user = User.objects.get(username="yoda")
        self.assertRedirects(response, reverse("admin:ferda_user_change", args=(user.pk,)))
        self.assertTrue(user.has_usable_password())

    def test_post_add_no_password(self):
        """Test POST request on add view without password."""
        self.client.force_login(self.admin)
        response = self.client.post(
            reverse("admin:ferda_user_add"),
            data={"username": "yoda", "password1": "", "password2": ""},
        )
        user = User.objects.get(username="yoda")
        self.assertRedirects(response, reverse("admin:ferda_user_change", args=(user.pk,)))
        self.assertFalse(user.has_usable_password())
