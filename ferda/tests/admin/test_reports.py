import textwrap
from unittest.mock import ANY, call, patch, sentinel

import grpc
from django import forms
from django.contrib.auth import get_user_model
from django.contrib.auth.models import Permission
from django.contrib.messages import get_messages
from django.test import TestCase, override_settings
from django.urls import reverse
from grill.clients import LogProperty
from grpc._channel import _SingleThreadedRendezvous
from reporter import Report as DbReport

from ferda.admin.reports import ManageReportsForm
from ferda.constants import LogEntryType, LoggerResult
from ferda.models import Report
from ferda.models.reports import SyncReportsResult
from ferda.tests.constants import REPORT_SERVICE
from ferda.tests.utils import LoggerMixin, ReportMixin

User = get_user_model()


@override_settings(ROOT_URLCONF="ferda.tests.urls")
class ReportAdminTest(LoggerMixin, TestCase):
    def setUp(self):
        super().setUp()
        self.admin = User.objects.create_superuser("admin", "admin@example.com", "password")
        self.client.force_login(self.admin)

    def test_changelist(self):
        Report.objects.create(name="test_report")
        response = self.client.get(reverse("admin:ferda_report_changelist"))
        self.assertContains(response, "test_report")
        self.assertContains(response, "Manage reports")

    def test_detail(self):
        report = Report.objects.create(name="test_report")
        report.parameters.create(name="param1", type="t1", is_input=True)
        report.parameters.create(name="param2", type="t2", is_input=False)

        response = self.client.get(reverse("admin:ferda_report_change", args=(report.pk,)))

        self.assertContains(response, "test_report")
        self.assertContains(response, "Input report parameters")
        self.assertContains(response, "param1")
        self.assertContains(response, "Output report parameters")
        self.assertContains(response, "param2")

    def test_add_forbidden(self):
        response = self.client.get(reverse("admin:ferda_report_add"))
        self.assertEqual(response.status_code, 403)

    def test_delete_forbidden(self):
        report = Report.objects.create(name="test_report")
        response = self.client.get(reverse("admin:ferda_report_delete", args=(report.pk,)))
        self.assertEqual(response.status_code, 403)

    def test_has_change_or_view_permission(self):
        report = Report.objects.create(name="test_report")
        report.parameters.create(name="param1", type="t1", is_input=True)
        report.parameters.create(name="param2", type="t2", is_input=False)
        user = User.objects.create_user("user", is_staff=True)

        for permission in ("change_report", "view_report"):
            with self.subTest(permission=permission):
                user.user_permissions.set(Permission.objects.filter(codename=permission))
                self.client.force_login(user)
                response = self.client.get(reverse("admin:ferda_report_change", args=(report.pk,)))
                self.assertContains(response, "test_report")
                self.assertContains(response, "param1")
                self.assertContains(response, "param2")

    def test_change_permission_denied(self):
        report = Report.objects.create(name="test_report")
        report.parameters.create(name="param1", type="t1", is_input=True)
        report.parameters.create(name="param2", type="t2", is_input=False)

        user = User.objects.create_user("user", is_staff=True)
        self.client.force_login(user)
        response = self.client.get(reverse("admin:ferda_report_change", args=(report.pk,)))
        self.assertEqual(response.status_code, 403)


@override_settings(
    ROOT_URLCONF="ferda.tests.urls",
    FERDA_REPORTS={REPORT_SERVICE: {"NETLOC": sentinel.netloc}},
)
class ManageReportsViewTest(LoggerMixin, ReportMixin, TestCase):
    def setUp(self):
        super().setUp()
        self.url = reverse("admin:ferda_report_manage_reports")
        self.admin = User.objects.create_superuser("admin", "admin@example.com", "password")
        self.client.force_login(self.admin)

        # Patch ManageReportsForm to have proper `service` field choices
        form_patcher = patch.dict(
            "ferda.admin.reports.ManageReportsForm.base_fields",
            {
                "service": forms.ChoiceField(
                    required=True,
                    choices=((REPORT_SERVICE, REPORT_SERVICE),),
                ),
            },
        )
        form_patcher.start()
        self.addCleanup(form_patcher.stop)

    def test_get(self):
        response = self.client.get(self.url)

        self.assertIsInstance(response.context["form"], ManageReportsForm)
        self.assertContains(response, "Manage reports")
        self.assertEqual(self.report_client.mock_calls, [])
        self.assertEqual(self.report_manager_client.mock_calls, [])

    def test_sql_post_error(self):
        state = grpc._channel._RPCState((), "", "", grpc.StatusCode.UNKNOWN, "SQL error")
        error = _SingleThreadedRendezvous(state, None, None, None)
        self.report_manager_client.manage_reports.side_effect = error

        response = self.client.post(self.url, data={"service": REPORT_SERVICE, "sql": "error"})

        self.assertEqual(response.context["error"], "StatusCode.UNKNOWN: SQL error")
        self.assertLog(
            LogEntryType.MANAGE_REPORTS,
            LoggerResult.FAIL,
            input_properties={"service": LogProperty(REPORT_SERVICE, None)},
            content="error",
        )
        self.assertEqual(self.report_client.mock_calls, [])
        self.assertEqual(
            self.report_manager_client.mock_calls,
            [
                call.manage_reports(sql="error"),
            ],
        )

    def test_sql_post_success(self):
        Report.objects.create(service=REPORT_SERVICE, name="test_report")
        report = DbReport(name="test_report2", input_parameters=[], output_parameters=[])

        self.report_client.list_reports.return_value = [report]
        self.report_manager_client.manage_reports.return_value = {
            "created": [report],
            "deleted": ["test_report"],
        }
        sql = "CREATE FUNCTION test_report2; DROP FUNCTION test_report;"

        response = self.client.post(
            self.url,
            data={
                "service": REPORT_SERVICE,
                "sql": sql,
            },
            follow=False,
        )

        self.assertLog(
            LogEntryType.MANAGE_REPORTS,
            LoggerResult.SUCCESS,
            input_properties={"service": LogProperty(REPORT_SERVICE, None)},
            content=sql,
        )
        messages = list(get_messages(response.wsgi_request))
        self.assertEqual(len(messages), 1)
        self.assertInHTML("Sync result", str(messages[0].message))
        self.assertRedirects(response, reverse("admin:ferda_report_changelist"))
        self.assertEqual(
            self.report_client.mock_calls,
            [
                call.list_reports(),
            ],
        )
        self.assertEqual(
            self.report_manager_client.mock_calls,
            [
                call.manage_reports(sql=sql),
            ],
        )

    def test_config_post_success(self):
        report = Report.objects.create(service=REPORT_SERVICE, name="test_report")
        report.parameters.create(name="day", type="int", is_input=True)
        report.parameters.create(name="month", type="int", is_input=True)
        report.parameters.create(name="count", type="int", is_input=False)

        config_yaml = textwrap.dedent(f"""
            - service: "{REPORT_SERVICE}"
              name: test_report
              label: Test report
              description: |
                  Some description.
                  Some more description...
              tags:
                  - heavy
                  - stuff
              input_params:
                  - name: day
                    label: Day of month
                  - name: month
                    label: Month of year
              output_params:
                  - name: count
                    label: Count of things
        """)

        response = self.client.post(
            self.url,
            data={"service": REPORT_SERVICE, "config": config_yaml},
            follow=False,
        )

        self.assertNoLogs()

        messages = list(get_messages(response.wsgi_request))
        self.assertEqual(len(messages), 1)
        self.assertIn("has been loaded", str(messages[0].message))
        self.assertRedirects(response, reverse("admin:ferda_report_changelist"))

        report = Report.objects.get()
        self.assertEqual(report.label, "Test report")
        self.assertEqual(report.description, "Some description.\nSome more description...\n")
        self.assertEqual(report.tags, "heavy;stuff")
        param_day = report.parameters.get(name="day")
        self.assertEqual(param_day.label, "Day of month")
        self.assertEqual(param_day.order, 1)
        param_month = report.parameters.get(name="month")
        self.assertEqual(param_month.label, "Month of year")
        self.assertEqual(param_month.order, 2)
        param_count = report.parameters.get(name="count")
        self.assertEqual(param_count.label, "Count of things")
        self.assertEqual(param_count.order, 1)

    def _test_config_post_error(self, config, error):
        response = self.client.post(
            self.url,
            data={"service": REPORT_SERVICE, "config": config},
            follow=False,
        )

        self.assertNoLogs()

        messages = list(get_messages(response.wsgi_request))
        self.assertEqual(len(messages), 1)
        self.assertIn(error, str(messages[0].message))
        self.assertEqual(response.status_code, 200)

    def test_config_post_invalid_yaml(self):
        config_yaml = "invalid: [{]}"
        self._test_config_post_error(config_yaml, "not a valid YAML")

    def test_config_post_invalid_config(self):
        config_yaml = "- service: none"
        self._test_config_post_error(config_yaml, "Configuration invalid")

    def test_config_post_no_report(self):
        config_yaml = textwrap.dedent(f"""
            - service: "{REPORT_SERVICE}"
              name: test_report
              label: Test report
              description: ""
              input_params: []
              output_params: []
        """)
        self._test_config_post_error(config_yaml, f"Report test_report in service {REPORT_SERVICE} does not exist")

    def test_config_post_param_missing(self):
        report = Report.objects.create(service=REPORT_SERVICE, name="test_report")
        report.parameters.create(name="day", type="int", is_input=True)

        config_yaml = textwrap.dedent(f"""
            - service: "{REPORT_SERVICE}"
              name: test_report
              label: Test report
              description: ""
              input_params: []
              output_params: []
        """)
        self._test_config_post_error(config_yaml, "Input parameter day is missing")

    def test_config_post_param_over(self):
        report = Report.objects.create(service=REPORT_SERVICE, name="test_report")
        report.parameters.create(name="day", type="int", is_input=True)

        config_yaml = textwrap.dedent(f"""
            - service: "{REPORT_SERVICE}"
              name: test_report
              label: Test report
              description: ""
              input_params:
                  - name: day
                    label: Day of month
                  - name: month
                    label: Month of year
              output_params: []
        """)
        self._test_config_post_error(config_yaml, "Config contains params that don't exist on server")


@override_settings(ROOT_URLCONF="ferda.tests.urls")
class SyncReportsViewTest(LoggerMixin, ReportMixin, TestCase):
    def setUp(self):
        super().setUp()
        self.url = reverse("admin:ferda_report_sync_reports")
        self.admin = User.objects.create_superuser("admin", "admin@example.com", "password")
        self.client.force_login(self.admin)

        Report.objects.create(service=REPORT_SERVICE, name="test_report")
        self.report = DbReport(name="test_report2", input_parameters=[], output_parameters=[])

        self.report_client.list_reports.return_value = [self.report]

    @patch("ferda.models.reports.ReportManager.sync_with_list", autospec=True)
    def test_get(self, sync_mock):
        sync_mock.return_value = sentinel.sync_result

        response = self.client.get(self.url)

        self.assertContains(response, "Sync reports")
        self.assertEqual(response.context["expected_sync_result"], {REPORT_SERVICE: sentinel.sync_result})
        self.assertEqual(
            sync_mock.mock_calls,
            [
                call(ANY, service=REPORT_SERVICE, reports=[self.report], dry_run=True),
            ],
        )

    @patch("ferda.models.reports.ReportManager.sync_with_list", autospec=True)
    def test_post(self, sync_mock):
        sync_mock.return_value = SyncReportsResult(created=["test_report2"], deleted=["test_report"])

        response = self.client.post(self.url, follow=False)

        messages = list(get_messages(response.wsgi_request))
        self.assertEqual(len(messages), 1)
        self.assertInHTML(f"Sync result of service {REPORT_SERVICE}", str(messages[0].message))
        self.assertEqual(sync_mock.mock_calls, [call(ANY, service=REPORT_SERVICE, reports=[self.report])])
        self.assertRedirects(response, reverse("admin:ferda_report_changelist"))
