from unittest.mock import patch, sentinel

from django.test import SimpleTestCase
from testfixtures import TempDirectory

from ferda.backend.utils import make_credentials


class TestMakeCredentials(SimpleTestCase):
    def setUp(self):
        self.temp_dir = TempDirectory()
        self.temp_dir.write("ssl_cert.pem", b"SSL CERT CONTENT")

    def tearDown(self):
        self.temp_dir.cleanup()

    @patch("grpc.ssl_channel_credentials")
    def test_file(self, ssl_credentials_mock):
        ssl_credentials_mock.return_value = sentinel.credentials
        self.assertEqual(make_credentials(self.temp_dir.getpath("ssl_cert.pem")), sentinel.credentials)
        ssl_credentials_mock.assert_called_once_with("SSL CERT CONTENT")

    def test_none(self):
        self.assertIsNone(make_credentials(None))
