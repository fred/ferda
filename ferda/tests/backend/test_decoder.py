from decimal import Decimal
from uuid import UUID

from django.test import SimpleTestCase
from fred_api.registry import common_types_pb2
from fred_api.registry.contact import contact_common_types_pb2
from fred_api.registry.registrar import registrar_common_types_pb2

from ferda.backend.decoder import FerdaBackendDecoder
from ferda.constants import WarningLetterPreference


class TestFerdaBackendDecoder(SimpleTestCase):
    """Test FerdaBackendDecoder."""

    def setUp(self):
        self.decoder = FerdaBackendDecoder()

    def test_decode_additional_identifier(self):
        self.assertEqual(
            self.decoder.decode(
                contact_common_types_pb2.ContactAdditionalIdentifier(
                    company_registration_number=common_types_pb2.CompanyRegistrationNumber(value="123456")
                )
            ),
            {"type": "company_registration_number", "value": "123456"},
        )

    def test_decode_empty_additional_identifier(self):
        self.assertEqual(self.decoder.decode(contact_common_types_pb2.ContactAdditionalIdentifier()), None)

    def test_decode_uuid(self):
        self.assertEqual(
            self.decoder.decode(common_types_pb2.Uuid(value="00000000-1111-2222-3333-444444444444")),
            UUID("00000000-1111-2222-3333-444444444444"),
        )
        self.assertEqual(self.decoder.decode(common_types_pb2.Uuid(value="")), None)
        self.assertEqual(self.decoder.decode(common_types_pb2.Uuid(value=None)), None)

    def test_decode_credit(self):
        self.assertEqual(
            self.decoder.decode(registrar_common_types_pb2.Credit(decimal_value="42000.40")), Decimal("42000.40")
        )
        self.assertEqual(self.decoder.decode(registrar_common_types_pb2.Credit(decimal_value="")), None)
        self.assertEqual(self.decoder.decode(registrar_common_types_pb2.Credit(decimal_value=None)), None)

    def test_decode_warning_letter(self):
        self.assertEqual(
            self.decoder.decode(contact_common_types_pb2.WarningLetter(preference=2)), WarningLetterPreference(2).name
        )
        self.assertEqual(
            self.decoder.decode(contact_common_types_pb2.WarningLetter(preference=1)), WarningLetterPreference(1).name
        )
        self.assertEqual(
            self.decoder.decode(contact_common_types_pb2.WarningLetter(preference=0)), WarningLetterPreference(0).name
        )
