from datetime import datetime
from unittest.mock import call
from uuid import UUID

import pytz
from django.test import SimpleTestCase
from fred_api.registry.common_types_pb2 import EmailAddress, Uuid
from fred_api.registry.contact.contact_common_types_pb2 import ContactHistoryId, ContactId
from fred_api.registry.contact.contact_history_types_pb2 import (
    ContactHistoryRecord,
    ContactHistoryReply,
    ContactHistoryRequest,
)
from fred_api.registry.contact.contact_info_types_pb2 import ContactInfoReply, ContactInfoRequest
from fred_api.registry.contact.contact_state_history_types_pb2 import (
    ContactStateHistoryReply,
    ContactStateHistoryRequest,
)
from fred_api.registry.contact.contact_state_types_pb2 import ContactStateReply, ContactStateRequest
from fred_api.registry.domain.domain_common_types_pb2 import DomainHistoryId, DomainId
from fred_api.registry.domain.domain_history_types_pb2 import DomainHistoryReply, DomainHistoryRequest
from fred_api.registry.domain.domain_info_types_pb2 import DomainInfoReply, DomainInfoRequest
from fred_api.registry.domain.domain_state_history_types_pb2 import DomainStateHistoryReply, DomainStateHistoryRequest
from fred_api.registry.domain.domain_state_types_pb2 import DomainStateReply, DomainStateRequest
from fred_api.registry.keyset.keyset_common_types_pb2 import KeysetHistoryId, KeysetId
from fred_api.registry.keyset.keyset_history_types_pb2 import KeysetHistoryReply, KeysetHistoryRequest
from fred_api.registry.keyset.keyset_info_types_pb2 import DnsKey, KeysetInfoReply, KeysetInfoRequest
from fred_api.registry.keyset.keyset_state_history_types_pb2 import KeysetStateHistoryReply, KeysetStateHistoryRequest
from fred_api.registry.keyset.keyset_state_types_pb2 import KeysetStateReply, KeysetStateRequest
from fred_api.registry.nsset.nsset_common_types_pb2 import NssetHistoryId, NssetId
from fred_api.registry.nsset.nsset_history_types_pb2 import NssetHistoryReply, NssetHistoryRequest
from fred_api.registry.nsset.nsset_info_types_pb2 import DnsHost, NssetInfoReply, NssetInfoRequest
from fred_api.registry.nsset.nsset_state_history_types_pb2 import NssetStateHistoryReply, NssetStateHistoryRequest
from fred_api.registry.nsset.nsset_state_types_pb2 import NssetStateReply, NssetStateRequest
from frgal.utils import TestClientMixin

from ferda.backend.client import FerdaBackendClient
from ferda.backend.decoder import FerdaBackendDecoder


class TestFerdaBackendClient(TestClientMixin, FerdaBackendClient):
    """Test FerdaBackendClient."""


class FerdaBackendClientContactTest(SimpleTestCase):
    """Test FerdaBackendClient's contact methods."""

    def setUp(self):
        self.client = TestFerdaBackendClient(decoder=FerdaBackendDecoder())

    def test_get_contact_info(self):
        contact_info = ContactInfoReply()
        contact_info.data.name = "Harry Potter"
        contact_info.data.emails.extend(
            [
                EmailAddress(value="harry@example.com"),
                EmailAddress(value="potter@example.com"),
            ]
        )
        contact_info.data.publish["name"] = True
        contact_info.data.publish["emails"] = True
        contact_info.data.publish["notify_emails"] = False
        self.client.mock.return_value = contact_info

        response = self.client.get_contact_info(UUID(int=42))
        self.assertEqual(response["name"], "Harry Potter")
        self.assertEqual(response["email"], "harry@example.com, potter@example.com")
        self.assertEqual(response["notify_email"], None)
        self.assertEqual(response["publish"], {"email": True, "name": True, "notify_email": False})

        response = self.client.get_contact_info(UUID(int=42), history_id=UUID(int=42))
        contact_id = ContactId(uuid=Uuid(value=str(UUID(int=42))))
        history_id = ContactHistoryId(uuid=Uuid(value=str(UUID(int=42))))
        self.client.mock.assert_has_calls(
            [
                call(
                    ContactInfoRequest(contact_id=contact_id),
                    method="/Fred.Registry.Api.Contact.Contact/get_contact_info",
                    timeout=None,
                ),
                call(
                    ContactInfoRequest(contact_id=contact_id, contact_history_id=history_id),
                    method="/Fred.Registry.Api.Contact.Contact/get_contact_info",
                    timeout=None,
                ),
            ]
        )

    def test_get_contact_history(self):
        reply = ContactHistoryReply()
        reply.data.contact_id.uuid.value = str(UUID(int=42))
        self.client.mock.return_value = reply

        self.assertEqual(
            self.client.get_contact_history(contact_id=UUID(int=42)),
            {
                "contact_id": UUID(int=42),
                "timeline": [],
                "valid_to": None,
            },
        )

        request = ContactHistoryRequest()
        request.contact_id.uuid.value = str(UUID(int=42))
        self.client.mock.assert_has_calls(
            [
                call(
                    request,
                    method="/Fred.Registry.Api.Contact.Contact/get_contact_history",
                    timeout=None,
                ),
            ]
        )

    def test_get_contact_history_id_by_datetime(self):
        contact_history_record = ContactHistoryRecord()
        contact_history_record.contact_history_id.uuid.value = str(UUID(int=43))
        contact_history_record.valid_from.FromDatetime(datetime(2019, 1, 1))

        reply = ContactHistoryReply()
        reply.data.contact_id.uuid.value = str(UUID(int=42))
        reply.data.timeline.extend([contact_history_record])
        reply.data.valid_to.FromDatetime(datetime(2020, 1, 1))

        self.client.mock.return_value = reply

        response = self.client.get_contact_history_id_by_datetime(UUID(int=42), datetime(2019, 5, 5))
        self.assertEqual(response, UUID(int=43))

        request = ContactHistoryRequest()
        request.contact_id.uuid.value = str(UUID(int=42))
        request.history.lower_limit.timestamp.FromDatetime(datetime(2019, 5, 5))
        request.history.upper_limit.timestamp.FromDatetime(datetime(2019, 5, 5))
        self.client.mock.assert_has_calls(
            [
                call(
                    request,
                    method="/Fred.Registry.Api.Contact.Contact/get_contact_history",
                    timeout=None,
                ),
            ]
        )

    def test_get_contact_state(self):
        reply = ContactStateReply()
        reply.data.contact_id.uuid.value = str(UUID(int=42))
        reply.data.state.flags["linked"] = True
        reply.data.state.flags["validated"] = False
        self.client.mock.return_value = reply

        self.assertEqual(self.client.get_contact_state(UUID(int=42)), {"linked": True, "validated": False})

        request = ContactStateRequest()
        request.contact_id.uuid.value = str(UUID(int=42))
        self.client.mock.assert_called_once_with(
            request,
            method="/Fred.Registry.Api.Contact.Contact/get_contact_state",
            timeout=None,
        )

    def test_get_contact_state_history(self):
        reply = ContactStateHistoryReply()
        reply.data.contact_id.uuid.value = str(UUID(int=42))
        self.client.mock.return_value = reply

        self.assertEqual(
            self.client.get_contact_state_history(contact_id=UUID(int=42)),
            {
                "contact_id": UUID(int=42),
                "history": {"timeline": [], "valid_to": None},
            },
        )

        request = ContactStateHistoryRequest()
        request.contact_id.uuid.value = str(UUID(int=42))
        self.client.mock.assert_called_once_with(
            request,
            method="/Fred.Registry.Api.Contact.Contact/get_contact_state_history",
            timeout=None,
        )


class FerdaBackendClientDomainTest(SimpleTestCase):
    """Test FerdaBackendClient's domain methods."""

    def setUp(self):
        self.client = TestFerdaBackendClient(decoder=FerdaBackendDecoder())

    def test_get_domain_info(self):
        reply = DomainInfoReply()
        reply.data.fqdn.value = "example.com"
        uuid = UUID(int=196)
        reply.data.registrant.id.uuid.value = str(uuid)
        reply.data.registrant.handle.value = "2X4C"
        reply.data.registrant.name = "Kryten"
        reply.data.registrant.organization = "JMC"
        self.client.mock.return_value = reply

        response = self.client.get_domain_info(UUID(int=42))
        self.assertEqual(response["fqdn"], "example.com")
        self.assertEqual(response["registrant"], uuid)
        self.assertEqual(
            response["registrant_ref"], {"id": uuid, "handle": "2X4C", "name": "Kryten", "organization": "JMC"}
        )

        self.client.get_domain_info(UUID(int=42), history_id=UUID(int=43))

        domain_id = DomainId()
        domain_id.uuid.value = str(UUID(int=42))
        history_id = DomainHistoryId()
        history_id.uuid.value = str(UUID(int=43))
        self.client.mock.assert_has_calls(
            [
                call(
                    DomainInfoRequest(domain_id=domain_id),
                    method="/Fred.Registry.Api.Domain.Domain/get_domain_info",
                    timeout=None,
                ),
                call(
                    DomainInfoRequest(domain_id=domain_id, domain_history_id=history_id),
                    method="/Fred.Registry.Api.Domain.Domain/get_domain_info",
                    timeout=None,
                ),
            ]
        )

    def test_get_domain_history(self):
        reply = DomainHistoryReply()
        reply.data.domain_id.uuid.value = str(UUID(int=42))
        self.client.mock.return_value = reply

        self.assertEqual(
            self.client.get_domain_history(domain_id=UUID(int=42)),
            {
                "domain_id": UUID(int=42),
                "timeline": [],
                "valid_to": None,
            },
        )

        request = DomainHistoryRequest()
        request.domain_id.uuid.value = str(UUID(int=42))
        self.client.mock.assert_has_calls(
            [
                call(
                    request,
                    method="/Fred.Registry.Api.Domain.Domain/get_domain_history",
                    timeout=None,
                ),
            ]
        )

    def test_get_domain_state(self):
        reply = DomainStateReply()
        reply.data.domain_id.uuid.value = str(UUID(int=42))
        reply.data.state.flags["linked"] = True
        reply.data.state.flags["serverBlocked"] = False
        self.client.mock.return_value = reply

        self.assertEqual(self.client.get_domain_state(UUID(int=42)), {"linked": True, "serverBlocked": False})

        request = DomainStateRequest()
        request.domain_id.uuid.value = str(UUID(int=42))
        self.client.mock.assert_called_once_with(
            request,
            method="/Fred.Registry.Api.Domain.Domain/get_domain_state",
            timeout=None,
        )

    def test_get_domain_state_history(self):
        reply = DomainStateHistoryReply()
        reply.data.domain_id.uuid.value = str(UUID(int=42))
        self.client.mock.return_value = reply

        self.assertEqual(
            self.client.get_domain_state_history(domain_id=UUID(int=42)),
            {
                "domain_id": UUID(int=42),
                "history": {"timeline": [], "valid_to": None},
            },
        )

        request = DomainStateHistoryRequest()
        request.domain_id.uuid.value = str(UUID(int=42))
        self.client.mock.assert_called_once_with(
            request,
            method="/Fred.Registry.Api.Domain.Domain/get_domain_state_history",
            timeout=None,
        )


class FerdaBackendClientKeysetTest(SimpleTestCase):
    """Test FerdaBackendClient's keyset methods."""

    def setUp(self):
        self.client = TestFerdaBackendClient(decoder=FerdaBackendDecoder())

    def test_get_keyset_info(self):
        dns_key = DnsKey()
        dns_key.flags = 257
        dns_key.protocol = 3
        dns_key.alg = 13
        dns_key.key = "whatever=="
        reply = KeysetInfoReply()
        reply.data.keyset_id.uuid.value = str(UUID(int=42))
        reply.data.dns_keys.extend([dns_key])
        self.client.mock.return_value = reply

        response = self.client.get_keyset_info(UUID(int=42))
        self.assertEqual(response["keyset_id"], UUID(int=42))
        self.assertEqual(response["dns_keys"], [{"flags": 257, "protocol": 3, "alg": 13, "key": "whatever=="}])

        self.client.get_keyset_info(UUID(int=42), history_id=UUID(int=43))

        keyset_id = KeysetId()
        keyset_id.uuid.value = str(UUID(int=42))
        history_id = KeysetHistoryId()
        history_id.uuid.value = str(UUID(int=43))
        self.client.mock.assert_has_calls(
            [
                call(
                    KeysetInfoRequest(keyset_id=keyset_id),
                    method="/Fred.Registry.Api.Keyset.Keyset/get_keyset_info",
                    timeout=None,
                ),
                call(
                    KeysetInfoRequest(keyset_id=keyset_id, keyset_history_id=history_id),
                    method="/Fred.Registry.Api.Keyset.Keyset/get_keyset_info",
                    timeout=None,
                ),
            ]
        )

    def test_get_keyset_history(self):
        reply = KeysetHistoryReply()
        reply.data.keyset_id.uuid.value = str(UUID(int=42))
        self.client.mock.return_value = reply

        self.assertEqual(
            self.client.get_keyset_history(keyset_id=UUID(int=42)),
            {
                "keyset_id": UUID(int=42),
                "timeline": [],
                "valid_to": None,
            },
        )

        request = KeysetHistoryRequest()
        request.keyset_id.uuid.value = str(UUID(int=42))
        self.client.mock.assert_has_calls(
            [
                call(
                    request,
                    method="/Fred.Registry.Api.Keyset.Keyset/get_keyset_history",
                    timeout=None,
                ),
            ]
        )

    def test_get_keyset_state(self):
        reply = KeysetStateReply()
        reply.data.keyset_id.uuid.value = str(UUID(int=42))
        reply.data.state.flags["linked"] = True
        reply.data.state.flags["serverBlocked"] = False
        self.client.mock.return_value = reply

        self.assertEqual(self.client.get_keyset_state(UUID(int=42)), {"linked": True, "serverBlocked": False})

        request = KeysetStateRequest()
        request.keyset_id.uuid.value = str(UUID(int=42))
        self.client.mock.assert_called_once_with(
            request,
            method="/Fred.Registry.Api.Keyset.Keyset/get_keyset_state",
            timeout=None,
        )

    def test_get_keyset_state_history(self):
        reply = KeysetStateHistoryReply()
        reply.data.keyset_id.uuid.value = str(UUID(int=42))
        self.client.mock.return_value = reply

        self.assertEqual(
            self.client.get_keyset_state_history(keyset_id=UUID(int=42)),
            {
                "keyset_id": UUID(int=42),
                "history": {"timeline": [], "valid_to": None},
            },
        )

        request = KeysetStateHistoryRequest()
        request.keyset_id.uuid.value = str(UUID(int=42))
        self.client.mock.assert_called_once_with(
            request,
            method="/Fred.Registry.Api.Keyset.Keyset/get_keyset_state_history",
            timeout=None,
        )


class FerdaBackendClientNssetTest(SimpleTestCase):
    """Test FerdaBackendClient's nsset methods."""

    def setUp(self):
        self.client = TestFerdaBackendClient(decoder=FerdaBackendDecoder())

    def test_get_nsset_info(self):
        dns_host = DnsHost()
        dns_host.fqdn = "dns.example.com"
        dns_host.ip_addresses[:] = ["127.0.0.1"]
        reply = NssetInfoReply()
        reply.data.nsset_id.uuid.value = str(UUID(int=42))
        reply.data.dns_hosts.extend([dns_host])
        self.client.mock.return_value = reply

        response = self.client.get_nsset_info(UUID(int=42))
        self.assertEqual(response["nsset_id"], UUID(int=42))
        self.assertEqual(response["dns_hosts"], [{"fqdn": "dns.example.com", "ip_addresses": ["127.0.0.1"]}])

        self.client.get_nsset_info(UUID(int=42), history_id=UUID(int=43))

        nsset_id = NssetId()
        nsset_id.uuid.value = str(UUID(int=42))
        history_id = NssetHistoryId()
        history_id.uuid.value = str(UUID(int=43))
        self.client.mock.assert_has_calls(
            [
                call(
                    NssetInfoRequest(nsset_id=nsset_id),
                    method="/Fred.Registry.Api.Nsset.Nsset/get_nsset_info",
                    timeout=None,
                ),
                call(
                    NssetInfoRequest(nsset_id=nsset_id, nsset_history_id=history_id),
                    method="/Fred.Registry.Api.Nsset.Nsset/get_nsset_info",
                    timeout=None,
                ),
            ]
        )

    def test_get_nsset_history(self):
        reply = NssetHistoryReply()
        reply.data.nsset_id.uuid.value = str(UUID(int=42))
        self.client.mock.return_value = reply

        self.assertEqual(
            self.client.get_nsset_history(nsset_id=UUID(int=42)),
            {
                "nsset_id": UUID(int=42),
                "timeline": [],
                "valid_to": None,
            },
        )

        request = NssetHistoryRequest()
        request.nsset_id.uuid.value = str(UUID(int=42))
        self.client.mock.assert_has_calls(
            [
                call(
                    request,
                    method="/Fred.Registry.Api.Nsset.Nsset/get_nsset_history",
                    timeout=None,
                ),
            ]
        )

    def test_get_nsset_state(self):
        reply = NssetStateReply()
        reply.data.nsset_id.uuid.value = str(UUID(int=42))
        reply.data.state.flags["linked"] = True
        reply.data.state.flags["serverBlocked"] = False
        self.client.mock.return_value = reply

        self.assertEqual(self.client.get_nsset_state(UUID(int=42)), {"linked": True, "serverBlocked": False})

        request = NssetStateRequest()
        request.nsset_id.uuid.value = str(UUID(int=42))
        self.client.mock.assert_called_once_with(
            request,
            method="/Fred.Registry.Api.Nsset.Nsset/get_nsset_state",
            timeout=None,
        )

    def test_get_nsset_state_history(self):
        reply = NssetStateHistoryReply()
        reply.data.nsset_id.uuid.value = str(UUID(int=42))
        self.client.mock.return_value = reply

        self.assertEqual(
            self.client.get_nsset_state_history(nsset_id=UUID(int=42)),
            {
                "nsset_id": UUID(int=42),
                "history": {"timeline": [], "valid_to": None},
            },
        )

        request = NssetStateHistoryRequest()
        request.nsset_id.uuid.value = str(UUID(int=42))
        self.client.mock.assert_called_once_with(
            request,
            method="/Fred.Registry.Api.Nsset.Nsset/get_nsset_state_history",
            timeout=None,
        )


class FerdaBackendClientCommonTest(SimpleTestCase):
    """Test FerdaBackendClient's common methods."""

    def setUp(self):
        self.client = TestFerdaBackendClient(decoder=FerdaBackendDecoder())

    def test_set_history_limits_by_history_id(self):
        request = ContactHistoryRequest()
        result = self.client._set_history_limits(request, start=UUID(int=43), end=UUID(int=44))
        self.assertEqual(result.history.lower_limit.contact_history_id.uuid.value, str(UUID(int=43)))
        self.assertEqual(result.history.upper_limit.contact_history_id.uuid.value, str(UUID(int=44)))

    def test_set_history_limits_by_timestamp(self):
        request = ContactHistoryRequest()
        result = self.client._set_history_limits(
            request,
            start=datetime.fromtimestamp(0, tz=pytz.utc),
            end=datetime.fromtimestamp(44442222.1199, tz=pytz.utc),
        )
        self.assertEqual(result.history.lower_limit.timestamp.seconds, 0)
        self.assertEqual(result.history.upper_limit.timestamp.seconds, 44442222)
        self.assertEqual(result.history.upper_limit.timestamp.nanos, 119900000)
