"""Test url converters."""

from datetime import datetime
from urllib.parse import quote

from django.test import SimpleTestCase
from pytz import utc

from ferda.converters import DatetimeConverter, QuoteConverter


class DatetimeConverterTest(SimpleTestCase):
    def setUp(self):
        self.converter = DatetimeConverter()

    def test_to_python(self):
        self.assertEqual(self.converter.to_python("2000-01-01T10:00:00Z"), datetime(2000, 1, 1, 10, 0, 0, tzinfo=utc))
        self.assertEqual(
            self.converter.to_python("2000-01-01T10:00:00+00:00"), datetime(2000, 1, 1, 10, 0, 0, tzinfo=utc)
        )
        self.assertEqual(
            self.converter.to_python("2000-01-01T10:00:00.042+01:00"), datetime(2000, 1, 1, 9, 0, 0, 42000, tzinfo=utc)
        )
        self.assertEqual(self.converter.to_python("2000-01-01 10:00:00Z"), datetime(2000, 1, 1, 10, 0, 0, tzinfo=utc))

    def test_to_url(self):
        self.assertEqual(
            self.converter.to_url(datetime(2000, 1, 1, 10, 0, 0, tzinfo=utc)),
            "2000-01-01T10:00:00+00:00",
        )
        self.assertEqual(
            self.converter.to_url(datetime(2000, 1, 1, 10, 0, 0, 42000, tzinfo=utc)),
            "2000-01-01T10:00:00.042000+00:00",
        )


class QuoteConverterTest(SimpleTestCase):
    def setUp(self):
        self.converter = QuoteConverter()
        self.patterns = [
            "some text",
            'some#complicated:id;with%special;"chars"/',
        ]

    def test_to_python(self):
        for pattern in self.patterns:
            with self.subTest(pattern=pattern):
                self.assertEqual(self.converter.to_python(quote(pattern)), pattern)

    def test_to_url(self):
        for pattern in self.patterns:
            with self.subTest(pattern=pattern):
                self.assertEqual(self.converter.to_url(pattern), quote(pattern))
