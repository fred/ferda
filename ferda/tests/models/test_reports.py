from unittest.mock import sentinel

from django.test import SimpleTestCase, TestCase
from reporter import Report as DbReport
from reporter.report import Param

from ferda.models import Report, ReportParameter
from ferda.models.reports import SyncReportsResult


class ReportTest(TestCase):
    def test_str(self):
        self.assertEqual(str(Report(name="test_report")), "test_report")
        self.assertEqual(str(Report(label="Test report")), "Test report")
        self.assertEqual(str(Report(name="test_report", label="Test report")), "Test report")

    def test_to_dbreport(self):
        report = Report.objects.create(name="test_report")
        report.parameters.create(name="p1", type="t1", is_input=True)
        report.parameters.create(name="p2", type="t2", is_input=False)
        self.assertEqual(
            report.to_db_report(),
            DbReport(
                name="test_report",
                input_parameters=[Param(name="p1", type="t1")],
                output_parameters=[Param(name="p2", type="t2")],
            ),
        )

    def test_to_dict(self):
        report = Report.objects.create(
            service="service",
            name="test_report",
            label="Test report",
            description="Long description...",
            tags="lab; quarters ;kitchen ",
        )
        report.parameters.create(name="p1", type="t1", label="P1", is_input=True)
        report.parameters.create(name="p2", type="t2", label="P2", is_input=False, order=2)
        report.parameters.create(name="p3", type="t3", label="P3", is_input=False, order=3)
        report.parameters.create(name="p4", type="t4", label="P4", is_input=False, order=1)
        self.assertEqual(
            report.to_dict(),
            {
                "service": "service",
                "name": "test_report",
                "label": "Test report",
                "description": "Long description...",
                "input_parameters": [{"name": "p1", "type": "t1", "label": "P1"}],
                "output_parameters": [
                    {"name": "p4", "type": "t4", "label": "P4"},
                    {"name": "p2", "type": "t2", "label": "P2"},
                    {"name": "p3", "type": "t3", "label": "P3"},
                ],
                "tags": ["lab", "quarters", "kitchen"],
            },
        )

    def test_update_parameters(self):
        report = Report.objects.create(name="test_report")
        report.parameters.create(name="p1", type="t1", is_input=True)
        report.parameters.create(name="p2", type="t2", is_input=False)
        report.parameters.create(name="p3", type="t3", is_input=True)
        report.parameters.create(name="p4", type="t4", is_input=False)
        report.parameters.create(name="p7", type="t7", is_input=False)

        report.update_parameters(
            DbReport(
                name="test_report",
                input_parameters=[Param(name="p1", type="t1"), Param(name="p5", type="t5")],
                output_parameters=[
                    Param(name="p2", type="t2"),
                    Param(name="p6", type="t6"),
                    Param(name="p7", type="t8"),
                ],
            )
        )

        self.assertQuerysetEqual(
            Report.objects.values_list("name", "parameters__name", "parameters__type", "parameters__is_input"),
            (
                ("test_report", "p1", "t1", True),
                ("test_report", "p2", "t2", False),
                ("test_report", "p5", "t5", True),
                ("test_report", "p6", "t6", False),
                ("test_report", "p7", "t8", False),
            ),
            transform=tuple,
            ordered=False,
        )


class ReportParameterTest(SimpleTestCase):
    def test_str(self):
        self.assertEqual(str(ReportParameter(name="test_report_param")), "test_report_param")

    def test_to_dict(self):
        self.assertEqual(
            ReportParameter(name=sentinel.name, label=sentinel.label, type=sentinel.type, is_input=True).to_dict(),
            {"name": sentinel.name, "label": sentinel.label, "type": sentinel.type},
        )


class ReportManagerTest(TestCase):
    def setUp(self):
        self.service = "service"
        self.db_report = DbReport(
            name="test_report",
            input_parameters=[Param(name="p1", type="t1")],
            output_parameters=[Param(name="p2", type="t2")],
        )

    def test_sync_with_list_create(self):
        result = Report.objects.sync_with_list(self.service, [self.db_report])
        self.assertEqual(result, SyncReportsResult(created=["service.test_report"]))

        self.assertQuerysetEqual(
            Report.objects.values_list("name", "parameters__name", "parameters__type", "parameters__is_input"),
            (
                ("test_report", "p1", "t1", True),
                ("test_report", "p2", "t2", False),
            ),
            transform=tuple,
            ordered=False,
        )

    def test_sync_with_list_delete(self):
        report = Report.objects.create(service=self.service, name="test_report")
        report.parameters.create(name="p1", type="t1", is_input=True)
        report.parameters.create(name="p2", type="t2", is_input=False)
        Report.objects.create(service="another_service", name="test_report")

        result = Report.objects.sync_with_list(self.service, [])

        self.assertEqual(result, SyncReportsResult(deleted=["service.test_report"]))

        self.assertEqual(Report.objects.count(), 1)
        self.assertEqual(Report.objects.filter(service=self.service).count(), 0)
        self.assertEqual(ReportParameter.objects.count(), 0)

    def test_sync_with_list_update(self):
        report = Report.objects.create(service=self.service, name="test_report")
        report.parameters.create(name="p3", type="t3", is_input=True)
        report.parameters.create(name="p4", type="t4", is_input=False)

        result = Report.objects.sync_with_list(self.service, [self.db_report])

        self.assertEqual(result, SyncReportsResult(updated=["service.test_report"]))
        self.assertQuerysetEqual(
            Report.objects.values_list("name", "parameters__name", "parameters__type", "parameters__is_input"),
            (
                ("test_report", "p1", "t1", True),
                ("test_report", "p2", "t2", False),
            ),
            transform=tuple,
            ordered=False,
        )

    def test_sync_with_list_no_change(self):
        report = Report.objects.create(service=self.service, name="test_report")
        report.parameters.create(name="p1", type="t1", is_input=True)
        report.parameters.create(name="p2", type="t2", is_input=False)

        result = Report.objects.sync_with_list(self.service, [self.db_report])
        self.assertEqual(result, SyncReportsResult())

        self.assertQuerysetEqual(
            Report.objects.values_list("name", "parameters__name", "parameters__type", "parameters__is_input"),
            (
                ("test_report", "p1", "t1", True),
                ("test_report", "p2", "t2", False),
            ),
            transform=tuple,
            ordered=False,
        )

    def test_sync_with_list_dry_run(self):
        result = Report.objects.sync_with_list(self.service, [self.db_report], dry_run=True)
        self.assertEqual(result, SyncReportsResult(created=["service.test_report"]))

        self.assertEqual(Report.objects.count(), 0)
        self.assertEqual(ReportParameter.objects.count(), 0)
