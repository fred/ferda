from datetime import datetime, timezone
from unittest.mock import call, patch

from django.contrib.auth import get_user_model
from django.test import TestCase, override_settings, tag
from django.urls import reverse
from frgal.utils import make_awaitable
from regal.constants import NOOP
from regal.exceptions import HistoryChangeProhibited, InvalidData, RegistrarDoesNotExist
from regal.registrar import ZoneAccess

from ferda.constants import LogEntryType, LoggerResult
from ferda.tests.utils import LoggerMixin

User = get_user_model()
NOW = datetime(2022, 1, 1, tzinfo=timezone.utc)


def _get_zone_access(zone: str = "cz", valid_from: datetime = NOW, **kwargs):
    return ZoneAccess(zone=zone, valid_from=valid_from, **kwargs)


@tag("api", "registry")
@patch("ferda.registry.registrar.zone_access.REGISTRAR")
@override_settings(ROOT_URLCONF="ferda.tests.urls")
class GetZoneAccessTest(LoggerMixin, TestCase):
    def setUp(self):
        super().setUp()
        user = User.objects.create_superuser("admin")  # type: ignore
        self.client.force_login(user)

    def test_get_zone_access(self, client):
        zone_access = _get_zone_access()

        client.get_registrar_zone_access_history.return_value = make_awaitable(
            {
                "cz": [zone_access],
            }
        )

        response = self.client.get(reverse("ferda:api:registrar_zone_access", kwargs={"handle": "REG-JMC"}))

        self.assertEqual(response.status_code, 200)
        self.assertEqual(
            response.json(),
            {
                "cz": [
                    {
                        "zone": "cz",
                        "valid_from": "2022-01-01T00:00:00Z",
                        "valid_to": None,
                    }
                ]
            },
        )
        self.assertEqual(
            client.mock_calls,
            [
                call.get_registrar_zone_access_history("REG-JMC"),
            ],
        )
        self.assertLog(
            LogEntryType.REGISTRAR_ZONE_ACCESS_VIEW,
            LoggerResult.SUCCESS,
            input_references={"registrar": "REG-JMC"},
        )

    def test_get_zone_access_registrar_not_found(self, client):
        client.get_registrar_zone_access_history.side_effect = RegistrarDoesNotExist

        response = self.client.get(reverse("ferda:api:registrar_zone_access", kwargs={"handle": "REG-JMC"}))

        self.assertEqual(response.status_code, 404)
        self.assertEqual(response.json(), {"detail": "Registrar does not exist"})
        self.assertEqual(
            client.mock_calls,
            [
                call.get_registrar_zone_access_history("REG-JMC"),
            ],
        )
        self.assertLog(
            LogEntryType.REGISTRAR_ZONE_ACCESS_VIEW,
            LoggerResult.FAIL,
            input_references={"registrar": "REG-JMC"},
        )


@tag("api", "registry")
@patch("ferda.registry.registrar.zone_access.REGISTRAR_ADMIN")
@override_settings(ROOT_URLCONF="ferda.tests.urls")
class CreateZoneAccessTest(LoggerMixin, TestCase):
    def setUp(self):
        super().setUp()
        user = User.objects.create_superuser("admin")  # type: ignore
        self.client.force_login(user)

    def test_create_zone_access(self, client):
        zone_access = _get_zone_access()
        client.add_zone_access.return_value = make_awaitable(zone_access)

        response = self.client.post(
            reverse("ferda:api:registrar_zone_access", kwargs={"handle": "REG-JMC"}),
            data=zone_access.model_dump(exclude_unset=True),
            content_type="application/json",
        )

        self.assertEqual(response.status_code, 200)
        self.assertEqual(
            response.json(),
            {
                "zone": "cz",
                "valid_from": "2022-01-01T00:00:00Z",
                "valid_to": None,
            },
        )
        self.assertEqual(
            client.mock_calls,
            [
                call.add_zone_access("REG-JMC", zone_access),
            ],
        )
        self.assertLog(
            LogEntryType.REGISTRAR_ZONE_ACCESS_CREATE,
            LoggerResult.SUCCESS,
            input_properties={
                "zone": "cz",
                "valid_from": "2022-01-01T00:00:00+00:00",
                "valid_to": "",
            },
            input_references={"registrar": "REG-JMC"},
        )

    def test_create_zone_access_registrar_not_found(self, client):
        self._test_create_zone_access_error(
            client,
            RegistrarDoesNotExist,
            404,
            {"detail": "Registrar does not exist"},
        )

    def test_create_zone_access_zone_not_found(self, client):
        self._test_create_zone_access_error(
            client,
            ValueError,
            404,
            {"detail": "Zone does not exist"},
        )

    def test_create_zone_access_invalid_data(self, client):
        self._test_create_zone_access_error(
            client,
            InvalidData(fields=["valid_from"]),
            400,
            {"detail": "Invalid data", "fields": ["valid_from"]},
        )

    def _test_create_zone_access_error(self, client, error, status_code, error_data):
        zone_access = _get_zone_access()
        client.add_zone_access.side_effect = error

        response = self.client.post(
            reverse("ferda:api:registrar_zone_access", kwargs={"handle": "REG-JMC"}),
            data=zone_access.model_dump(exclude_unset=True),
            content_type="application/json",
        )

        self.assertEqual(response.status_code, status_code)
        self.assertEqual(response.json(), error_data)
        self.assertEqual(
            client.mock_calls,
            [
                call.add_zone_access("REG-JMC", zone_access),
            ],
        )
        self.assertLog(
            LogEntryType.REGISTRAR_ZONE_ACCESS_CREATE,
            LoggerResult.FAIL,
            input_properties={
                "zone": "cz",
                "valid_from": "2022-01-01T00:00:00+00:00",
                "valid_to": "",
            },
            input_references={"registrar": "REG-JMC"},
        )


@tag("api", "registry")
@patch("ferda.registry.registrar.zone_access.REGISTRAR_ADMIN")
@override_settings(ROOT_URLCONF="ferda.tests.urls")
class UpdateZoneAccessTest(LoggerMixin, TestCase):
    def setUp(self):
        super().setUp()
        user = User.objects.create_superuser("admin")  # type: ignore
        self.client.force_login(user)

    def test_update_zone_access_valid_from(self, client):
        zone_access = _get_zone_access()
        new_valid_from = datetime(2022, 2, 2, tzinfo=timezone.utc)
        client.update_zone_access.return_value = make_awaitable(zone_access)
        data = {
            "access": zone_access.model_dump(exclude_unset=True),
            "valid_from": new_valid_from,
        }

        response = self.client.put(
            reverse("ferda:api:registrar_zone_access", kwargs={"handle": "REG-JMC"}),
            data=data,
            content_type="application/json",
        )

        self.assertEqual(response.status_code, 200)
        self.assertEqual(
            client.mock_calls,
            [
                call.update_zone_access("REG-JMC", zone_access, valid_from=new_valid_from, valid_to=NOOP),
            ],
        )
        self.assertLog(
            LogEntryType.REGISTRAR_ZONE_ACCESS_UPDATE,
            LoggerResult.SUCCESS,
            input_properties={
                "zone": "cz",
                "valid_from": "2022-01-01T00:00:00+00:00",
                "valid_to": "",
                "update_valid_from": "2022-02-02T00:00:00+00:00",
            },
            input_references={"registrar": "REG-JMC"},
        )

    def test_update_zone_access_valid_to(self, client):
        zone_access = _get_zone_access(valid_to=datetime(2022, 2, 2, tzinfo=timezone.utc))
        client.update_zone_access.return_value = make_awaitable(zone_access)
        data = {
            "access": zone_access.model_dump(exclude_unset=True),
            "valid_to": None,
        }

        response = self.client.put(
            reverse("ferda:api:registrar_zone_access", kwargs={"handle": "REG-JMC"}),
            data=data,
            content_type="application/json",
        )

        self.assertEqual(response.status_code, 200)
        self.assertEqual(
            client.mock_calls,
            [
                call.update_zone_access("REG-JMC", zone_access, valid_from=NOOP, valid_to=None),
            ],
        )
        self.assertLog(
            LogEntryType.REGISTRAR_ZONE_ACCESS_UPDATE,
            LoggerResult.SUCCESS,
            input_properties={
                "zone": "cz",
                "valid_from": "2022-01-01T00:00:00+00:00",
                "valid_to": "2022-02-02T00:00:00+00:00",
                "update_valid_to": "",
            },
            input_references={"registrar": "REG-JMC"},
        )

    def test_update_zone_access_history_change_prohibited(self, client):
        self._test_update_zone_access_error(
            client,
            HistoryChangeProhibited,
            400,
            {"detail": "History change is prohibited"},
        )

    def test_update_zone_access_not_found(self, client):
        self._test_update_zone_access_error(
            client,
            ValueError,
            404,
            {"detail": "Registrar zone access does not exist"},
        )

    def test_update_zone_access_invalid_data(self, client):
        self._test_update_zone_access_error(
            client,
            InvalidData(fields=["valid_from"]),
            400,
            {"detail": "Invalid data", "fields": ["valid_from"]},
        )

    def _test_update_zone_access_error(self, client, error, status_code, error_data):
        zone_access = _get_zone_access()
        new_valid_from = datetime(2022, 2, 2, tzinfo=timezone.utc)
        client.update_zone_access.side_effect = error
        data = {
            "access": zone_access.model_dump(exclude_unset=True),
            "valid_from": new_valid_from,
        }

        response = self.client.put(
            reverse("ferda:api:registrar_zone_access", kwargs={"handle": "REG-JMC"}),
            data=data,
            content_type="application/json",
        )

        self.assertEqual(response.status_code, status_code)
        self.assertEqual(response.json(), error_data)
        self.assertEqual(
            client.mock_calls,
            [
                call.update_zone_access("REG-JMC", zone_access, valid_from=new_valid_from, valid_to=NOOP),
            ],
        )
        self.assertLog(
            LogEntryType.REGISTRAR_ZONE_ACCESS_UPDATE,
            LoggerResult.FAIL,
            input_properties={
                "zone": "cz",
                "valid_from": "2022-01-01T00:00:00+00:00",
                "valid_to": "",
                "update_valid_from": "2022-02-02T00:00:00+00:00",
            },
            input_references={"registrar": "REG-JMC"},
        )


@tag("api", "registry")
@patch("ferda.registry.registrar.zone_access.REGISTRAR_ADMIN")
@override_settings(ROOT_URLCONF="ferda.tests.urls")
class DeleteZoneAccessTest(LoggerMixin, TestCase):
    def setUp(self):
        super().setUp()
        user = User.objects.create_superuser("admin")  # type: ignore
        self.client.force_login(user)

    def test_delete_zone_access(self, client):
        zone_access = _get_zone_access()
        client.delete_zone_access.return_value = make_awaitable(zone_access)

        response = self.client.delete(
            reverse("ferda:api:registrar_zone_access", kwargs={"handle": "REG-JMC"}),
            data=zone_access.model_dump(exclude_unset=True),
            content_type="application/json",
        )

        self.assertEqual(response.status_code, 200)
        self.assertEqual(
            client.mock_calls,
            [
                call.delete_zone_access("REG-JMC", zone_access),
            ],
        )
        self.assertLog(
            LogEntryType.REGISTRAR_ZONE_ACCESS_DELETE,
            LoggerResult.SUCCESS,
            input_properties={
                "zone": "cz",
                "valid_from": "2022-01-01T00:00:00+00:00",
                "valid_to": "",
            },
            input_references={"registrar": "REG-JMC"},
        )

    def test_delete_zone_access_history_change_prohibited(self, client):
        self._test_delete_zone_access_error(
            client,
            HistoryChangeProhibited,
            400,
            {"detail": "History change is prohibited"},
        )

    def test_delete_zone_access_not_found(self, client):
        self._test_delete_zone_access_error(
            client,
            ValueError,
            404,
            {"detail": "Registrar zone access does not exist"},
        )

    def test_delete_zone_access_invalid_data(self, client):
        self._test_delete_zone_access_error(
            client,
            InvalidData(fields=["valid_from"]),
            400,
            {"detail": "Invalid data", "fields": ["valid_from"]},
        )

    def _test_delete_zone_access_error(self, client, error, status_code, error_data):
        zone_access = _get_zone_access()
        client.delete_zone_access.side_effect = error

        response = self.client.delete(
            reverse("ferda:api:registrar_zone_access", kwargs={"handle": "REG-JMC"}),
            data=zone_access.model_dump(exclude_unset=True),
            content_type="application/json",
        )

        self.assertEqual(response.status_code, status_code)
        self.assertEqual(response.json(), error_data)
        self.assertEqual(
            client.mock_calls,
            [
                call.delete_zone_access("REG-JMC", zone_access),
            ],
        )
        self.assertLog(
            LogEntryType.REGISTRAR_ZONE_ACCESS_DELETE,
            LoggerResult.FAIL,
            input_properties={
                "zone": "cz",
                "valid_from": "2022-01-01T00:00:00+00:00",
                "valid_to": "",
            },
            input_references={"registrar": "REG-JMC"},
        )
