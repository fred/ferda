from datetime import datetime, timedelta, timezone
from typing import Any, Dict
from unittest.mock import call, patch
from uuid import UUID

from django.contrib.auth import get_user_model
from django.core.files.uploadedfile import SimpleUploadedFile
from django.test import TestCase, override_settings, tag
from django.urls import reverse
from regal import FileId, RegistrarCertification, RegistrarCertificationId
from regal.exceptions import InvalidData, RegistrarDoesNotExist

from ferda.constants import LogEntryType, LoggerResult, LogReferenceType
from ferda.registry.registrar.certification import AddCertificationParams, UpdateCertificationParams
from ferda.tests.utils import LoggerMixin

User = get_user_model()


def _make_certification(id: int, valid_days: int = 0) -> RegistrarCertification:
    # Python API model
    valid_from = datetime(2023, 1, 1, tzinfo=timezone.utc) + timedelta(days=id)
    return RegistrarCertification(
        certification_id=RegistrarCertificationId("Cert-" + str(id)),
        registrar_handle="Reg-1",
        valid_from=valid_from,
        valid_to=None if valid_days <= 0 else valid_from + timedelta(days=valid_days),
        classification=50,
        file_id=FileId(str(UUID(int=id))),
    )


@tag("api", "registry")
@patch("ferda.registry.registrar.certification.REGISTRAR", autospec=True)
@override_settings(ROOT_URLCONF="ferda.tests.urls")
class GetCertificationTest(LoggerMixin, TestCase):
    def setUp(self):
        super().setUp()
        user = User.objects.create_superuser("admin")
        self.client.force_login(user)

    # --- get_certifications

    def test_get_certifications(self, client):
        certifications = [_make_certification(i + 1) for i in range(3)]
        client.get_registrar_certifications.return_value = certifications

        response = self.client.get(reverse("ferda:api:registrar_certification", kwargs={"registrar_id": "Reg-1"}))

        self.assertEqual(response.status_code, 200)
        self.assertEqual([RegistrarCertification(**i) for i in response.json()], certifications)
        self.assertEqual(client.mock_calls, [call.get_registrar_certifications("Reg-1")])
        self.assertLog(
            LogEntryType.REGISTRAR_CERTIFICATION_VIEW,
            LoggerResult.SUCCESS,
            input_properties={"registrar_id": "Reg-1"},
            output_properties={"items_count": str(len(certifications))},
            output_references={LogReferenceType.REGISTRAR: "Reg-1"},
        )

    def test_get_certifications_empty(self, client):
        client.get_registrar_certifications.return_value = []

        response = self.client.get(reverse("ferda:api:registrar_certification", kwargs={"registrar_id": "Reg-1"}))

        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.json(), [])
        self.assertLog(
            LogEntryType.REGISTRAR_CERTIFICATION_VIEW,
            LoggerResult.SUCCESS,
            input_properties={"registrar_id": "Reg-1"},
            output_references={LogReferenceType.REGISTRAR: "Reg-1"},
        )

    def test_get_certifications_not_found(self, client):
        client.get_registrar_certifications.side_effect = RegistrarDoesNotExist

        response = self.client.get(reverse("ferda:api:registrar_certification", kwargs={"registrar_id": "Reg-1"}))

        self.assertEqual(response.status_code, 404)
        self.assertEqual(response.json(), {"detail": "Registrar does not exist"})
        self.assertLog(
            LogEntryType.REGISTRAR_CERTIFICATION_VIEW,
            LoggerResult.FAIL,
            input_properties={"registrar_id": "Reg-1"},
        )


@tag("api", "registry")
@patch("ferda.registry.registrar.certification.REGISTRAR", autospec=True)
@patch("ferda.registry.registrar.certification.FILE_STORAGE", autospec=True)
@override_settings(ROOT_URLCONF="ferda.tests.urls")
class GetCertificationFileTest(LoggerMixin, TestCase):
    def setUp(self):
        super().setUp()
        user = User.objects.create_superuser("admin")
        self.client.force_login(user)
        self.file_id = str(UUID(int=1))

    # --- get_certification file

    def test_get_file_ok(self, storage_mock, client):
        file_content = b"PDF binary content"
        file_name = "test.pdf"
        file_mime = "application/pdf"
        storage_mock.open.return_value.__enter__.return_value.__iter__.return_value = iter(
            [file_content[:3], file_content[3:6], file_content[6:]]
        )
        storage_mock.open.return_value.__enter__.return_value.name = file_name
        storage_mock.open.return_value.__enter__.return_value.mimetype = file_mime
        client.get_registrar_certifications.return_value = [_make_certification(1)]

        response = self.client.get(
            reverse("ferda:api:registrar_certification_file", kwargs={"registrar_id": "Reg-1", "file_id": self.file_id})
        )
        self.assertEqual(client.mock_calls, [call.get_registrar_certifications("Reg-1")])
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.headers["Content-Disposition"], 'inline; filename="{}"'.format(file_name))
        self.assertEqual(response.headers["Content-Type"], file_mime)
        self.assertEqual(b"".join(response.streaming_content), file_content)
        storage_calls = [
            call.open(self.file_id),
            call.open().__enter__(),
            call.open().__enter__().__iter__(),
            call.open().__exit__(None, None, None),
            call.open().__enter__().close(),
        ]
        self.assertEqual(storage_mock.mock_calls, storage_calls)

    def test_get_file_registrar_not_found(self, storage_mock, client):
        # registrar not found
        client.get_registrar_certifications.side_effect = RegistrarDoesNotExist

        response = self.client.get(
            reverse("ferda:api:registrar_certification_file", kwargs={"registrar_id": "Reg-1", "file_id": self.file_id})
        )

        self.assertEqual(response.status_code, 404)
        self.assertEqual(response.json(), {"detail": "Registrar does not exist"})
        self.assertEqual(storage_mock.mock_calls, [])

    def test_get_file_not_found(self, storage_mock, client):
        # file_id not found in registrar certifications
        client.get_registrar_certifications.return_value = [_make_certification(2)]

        response = self.client.get(
            reverse("ferda:api:registrar_certification_file", kwargs={"registrar_id": "Reg-1", "file_id": self.file_id})
        )

        self.assertEqual(response.status_code, 404)
        self.assertEqual(response.json(), {"detail": "Certification does not exist"})
        self.assertEqual(storage_mock.mock_calls, [])

    def test_get_file_not_open(self, storage_mock, client):
        # file not found by fileman
        storage_mock.open.side_effect = FileNotFoundError
        client.get_registrar_certifications.return_value = [_make_certification(1)]

        response = self.client.get(
            reverse("ferda:api:registrar_certification_file", kwargs={"registrar_id": "Reg-1", "file_id": self.file_id})
        )

        self.assertEqual(response.status_code, 404)
        self.assertEqual(response.json(), {"detail": "File " + self.file_id + " not found"})
        self.assertEqual(storage_mock.mock_calls, [call.open(self.file_id)])


@tag("api", "registry")
@patch("ferda.registry.registrar.certification.REGISTRAR_ADMIN", autospec=True)
@patch("ferda.registry.registrar.certification.FILE_STORAGE", autospec=True)
@override_settings(ROOT_URLCONF="ferda.tests.urls")
class AddCertificationTest(LoggerMixin, TestCase):
    """Test class for registrar certifications."""

    def setUp(self):
        super().setUp()
        user = User.objects.create_superuser("admin")
        self.client.force_login(user)

    # --- add_registrar_certification

    def test_add_certification_ok(self, storage_mock, client):
        params = AddCertificationParams(
            classification=50,
            valid_from=datetime(2023, 1, 1, 12, 0, 0, 0, tzinfo=timezone.utc),
            valid_to=datetime(2023, 6, 30, 0, 0, 0, tzinfo=timezone.utc),
        )
        data = params.model_dump()
        data["file"] = SimpleUploadedFile("test.pdf", b"PDF binary content")
        file_id = str(UUID(int=1))
        storage_mock.create.return_value.__enter__.return_value.uid = file_id
        client.add_registrar_certification.return_value = None

        response = self.client.post(
            reverse("ferda:api:registrar_certification", kwargs={"registrar_id": "Reg-1"}), data=data
        )

        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.content, b"")
        self.assertLog(
            LogEntryType.REGISTRAR_CERTIFICATION_CREATE,
            LoggerResult.SUCCESS,
            input_properties={
                "registrar_id": "Reg-1",
                "classification": str(params.classification),
                "valid_from": params.valid_from.isoformat(),  # type: ignore [union-attr]
                "valid_to": params.valid_to.isoformat(),  # type: ignore [union-attr]
            },
            output_properties={"file_id": file_id},
            output_references={LogReferenceType.REGISTRAR: "Reg-1"},
        )
        self.assertEqual(
            client.mock_calls, [call.add_registrar_certification("Reg-1", file_id=str(file_id), **params.model_dump())]
        )
        storage_calls = [
            call.create("test.pdf", mimetype="application/pdf"),
            call.create().__enter__(),
            call.create().__enter__().write(b"PDF binary content"),  # fits well into one chunk
            call.create().__exit__(None, None, None),
        ]
        self.assertEqual(storage_mock.mock_calls, storage_calls)

    def test_add_certification_not_found(self, storage_mock, client):
        data = {"classification": 50, "valid_from": "", "valid_to": ""}  # time values as empty string
        data["file"] = SimpleUploadedFile("test.pdf", b"PDF binary content")
        file_id = str(UUID(int=1))
        storage_mock.create.return_value.__enter__.return_value.uid = file_id
        client.add_registrar_certification.side_effect = RegistrarDoesNotExist

        response = self.client.post(
            reverse("ferda:api:registrar_certification", kwargs={"registrar_id": "Reg-1"}), data=data
        )

        self.assertEqual(response.status_code, 404)
        self.assertEqual(response.json(), {"detail": "Registrar does not exist"})
        self.assertLog(
            LogEntryType.REGISTRAR_CERTIFICATION_CREATE,
            LoggerResult.FAIL,
            input_properties={
                "registrar_id": "Reg-1",
                "classification": str(data["classification"]),
            },
        )

    def test_add_certification_invalid(self, storage_mock, client):
        data = {"classification": 50}  # time values not included
        data["file"] = SimpleUploadedFile("test.pdf", b"PDF binary content")
        file_id = str(UUID(int=1))
        storage_mock.create.return_value.__enter__.return_value.uid = file_id
        client.add_registrar_certification.side_effect = InvalidData(fields=["classification"])

        response = self.client.post(
            reverse("ferda:api:registrar_certification", kwargs={"registrar_id": "Reg-1"}), data=data
        )

        self.assertEqual(response.status_code, 400)
        self.assertEqual(response.json(), {"detail": "Invalid data", "fields": ["classification"]})
        self.assertLog(
            LogEntryType.REGISTRAR_CERTIFICATION_CREATE,
            LoggerResult.FAIL,
            input_properties={
                "registrar_id": "Reg-1",
                "classification": str(data["classification"]),
            },
        )

    def test_add_certification_store_failed(self, storage_mock, client):
        data = {"classification": 50, "valid_from": "", "valid_to": ""}
        data["file"] = SimpleUploadedFile("test.pdf", b"PDF binary content")
        storage_mock.create.return_value.__enter__.return_value.write.side_effect = OSError
        client.add_registrar_certification.return_value = None

        with self.assertRaises(OSError):
            self.client.post(reverse("ferda:api:registrar_certification", kwargs={"registrar_id": "Reg-1"}), data=data)


@tag("api", "registry")
@patch("ferda.registry.registrar.certification.REGISTRAR", autospec=True)
@patch("ferda.registry.registrar.certification.REGISTRAR_ADMIN", autospec=True)
@override_settings(ROOT_URLCONF="ferda.tests.urls")
class UpdateDeleteCertificationTest(LoggerMixin, TestCase):
    """Test class for registrar certifications."""

    def setUp(self):
        super().setUp()
        user = User.objects.create_superuser("admin")
        self.client.force_login(user)

    # --- update_registrar_certification

    def test_update_certification_ok(self, admin_client, client):
        params = UpdateCertificationParams(
            classification=50, valid_to=datetime(2023, 6, 30, 0, 0, 0, tzinfo=timezone.utc)
        )
        client.get_registrar_certifications.return_value = [_make_certification(1)]
        admin_client.update_registrar_certification.return_value = None

        response = self.client.post(
            reverse(
                "ferda:api:registrar_certification", kwargs={"registrar_id": "Reg-1", "certification_id": "Cert-1"}
            ),
            data=params.model_dump(),
        )

        self.assertEqual(client.mock_calls, [call.get_registrar_certifications("Reg-1")])
        self.assertEqual(
            admin_client.mock_calls,
            [call.update_registrar_certification("Cert-1", file_id=None, **params.model_dump())],
        )
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.content, b"")
        self.assertLog(
            LogEntryType.REGISTRAR_CERTIFICATION_UPDATE,
            LoggerResult.SUCCESS,
            input_properties={
                "registrar_id": "Reg-1",
                "certification_id": "Cert-1",
                "set_classification": str(params.classification),
                "set_valid_to": params.valid_to.isoformat(),  # type: ignore [union-attr]
            },
            output_references={LogReferenceType.REGISTRAR: "Reg-1"},
        )

    def test_update_certification_cert_not_found(self, admin_client, client):
        data: Dict[str, Any] = {"classification": "", "valid_to": ""}  # None values as empty strings
        client.get_registrar_certifications.return_value = []

        response = self.client.post(
            reverse(
                "ferda:api:registrar_certification", kwargs={"registrar_id": "Reg-1", "certification_id": "Cert-1"}
            ),
            data=data,
        )

        self.assertEqual(admin_client.mock_calls, [])
        self.assertEqual(response.status_code, 404)
        self.assertEqual(response.json(), {"detail": "Certification does not exist"})
        self.assertLog(
            LogEntryType.REGISTRAR_CERTIFICATION_UPDATE,
            LoggerResult.FAIL,
            input_properties={"registrar_id": "Reg-1", "certification_id": "Cert-1"},
        )

    def test_update_certification_reg_not_found(self, admin_client, client):
        data: Dict[str, Any] = {}  # None values not included
        client.get_registrar_certifications.side_effect = RegistrarDoesNotExist

        response = self.client.post(
            reverse(
                "ferda:api:registrar_certification", kwargs={"registrar_id": "Reg-1", "certification_id": "Cert-1"}
            ),
            data=data,
        )

        self.assertEqual(admin_client.mock_calls, [])
        self.assertEqual(response.status_code, 404)
        self.assertEqual(response.json(), {"detail": "Registrar does not exist"})
        self.assertLog(
            LogEntryType.REGISTRAR_CERTIFICATION_UPDATE,
            LoggerResult.FAIL,
            input_properties={"registrar_id": "Reg-1", "certification_id": "Cert-1"},
        )

    def test_update_certification_invalid(self, admin_client, client):
        data: Dict[str, Any] = {"classification": -50}
        client.get_registrar_certifications.return_value = [_make_certification(1)]
        admin_client.update_registrar_certification.side_effect = InvalidData(fields=["classification"])

        response = self.client.post(
            reverse(
                "ferda:api:registrar_certification", kwargs={"registrar_id": "Reg-1", "certification_id": "Cert-1"}
            ),
            data=data,
        )

        self.assertEqual(response.status_code, 400)
        self.assertEqual(response.json(), {"detail": "Invalid data", "fields": ["classification"]})
        self.assertLog(
            LogEntryType.REGISTRAR_CERTIFICATION_UPDATE,
            LoggerResult.FAIL,
            input_properties={
                "registrar_id": "Reg-1",
                "certification_id": "Cert-1",
                "set_classification": str(data["classification"]),
            },
        )

    @patch("ferda.registry.registrar.certification.FILE_STORAGE", autospec=True)
    def test_update_certification_file_ok(self, storage_mock, admin_client, client):
        file_id = str(UUID(int=1))
        storage_mock.create.return_value.__enter__.return_value.uid = file_id
        data: Dict[str, Any] = {"file": SimpleUploadedFile("test.pdf", b"PDF binary content"), "classification": 50}
        client.get_registrar_certifications.return_value = [_make_certification(1)]
        admin_client.update_registrar_certification.return_value = None

        response = self.client.post(
            reverse(
                "ferda:api:registrar_certification", kwargs={"registrar_id": "Reg-1", "certification_id": "Cert-1"}
            ),
            data=data,
        )

        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.content, b"")
        self.assertLog(
            LogEntryType.REGISTRAR_CERTIFICATION_UPDATE,
            LoggerResult.SUCCESS,
            input_properties={"registrar_id": "Reg-1", "certification_id": "Cert-1", "set_classification": "50"},
            output_properties={"set_file_id": file_id},
            output_references={LogReferenceType.REGISTRAR: "Reg-1"},
        )
        self.assertEqual(client.mock_calls, [call.get_registrar_certifications("Reg-1")])
        self.assertEqual(
            admin_client.mock_calls,
            [call.update_registrar_certification("Cert-1", file_id=file_id, classification=50, valid_to=None)],
        )
        storage_calls = [
            call.create("test.pdf", mimetype="application/pdf"),
            call.create().__enter__(),
            call.create().__enter__().write(b"PDF binary content"),  # fits well into one chunk
            call.create().__exit__(None, None, None),
        ]
        self.assertEqual(storage_mock.mock_calls, storage_calls)

    @patch("ferda.registry.registrar.certification.FILE_STORAGE", autospec=True)
    def test_update_certification_file_failed(self, storage_mock, admin_client, client):
        storage_mock.create.return_value.__enter__.return_value.uid = str(UUID(int=1))
        storage_mock.create.return_value.__enter__.return_value.write.side_effect = OSError
        data: Dict[str, Any] = {"file": SimpleUploadedFile("test.pdf", b"PDF binary content")}
        client.get_registrar_certifications.return_value = [_make_certification(1)]
        admin_client.update_registrar_certification.return_value = None

        with self.assertRaises(OSError):
            self.client.post(
                reverse(
                    "ferda:api:registrar_certification",
                    kwargs={"registrar_id": "Reg-1", "certification_id": "Cert-1"},
                ),
                data=data,
            )

    # --- delete_registrar_certification

    def test_delete_certification_ok(self, admin_client, client):
        client.get_registrar_certifications.return_value = [_make_certification(1)]
        admin_client.delete_registrar_certification.return_value = None

        response = self.client.delete(
            reverse("ferda:api:registrar_certification", kwargs={"registrar_id": "Reg-1", "certification_id": "Cert-1"})
        )

        self.assertEqual(client.mock_calls, [call.get_registrar_certifications("Reg-1")])
        self.assertEqual(admin_client.mock_calls, [call.delete_registrar_certification("Cert-1")])
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.content, b"")
        self.assertLog(
            LogEntryType.REGISTRAR_CERTIFICATION_DELETE,
            LoggerResult.SUCCESS,
            input_properties={"registrar_id": "Reg-1", "certification_id": "Cert-1"},
            output_references={LogReferenceType.REGISTRAR: "Reg-1"},
        )

    def test_delete_certification_cert_not_found(self, admin_client, client):
        client.get_registrar_certifications.return_value = []

        response = self.client.delete(
            reverse("ferda:api:registrar_certification", kwargs={"registrar_id": "Reg-1", "certification_id": "Cert-1"})
        )

        self.assertEqual(admin_client.mock_calls, [])
        self.assertEqual(response.status_code, 404)
        self.assertEqual(response.json(), {"detail": "Certification does not exist"})
        self.assertLog(
            LogEntryType.REGISTRAR_CERTIFICATION_DELETE,
            LoggerResult.FAIL,
            input_properties={"registrar_id": "Reg-1", "certification_id": "Cert-1"},
        )

    def test_delete_certification_reg_not_found(self, admin_client, client):
        client.get_registrar_certifications.side_effect = RegistrarDoesNotExist

        response = self.client.delete(
            reverse("ferda:api:registrar_certification", kwargs={"registrar_id": "Reg-1", "certification_id": "Cert-1"})
        )

        self.assertEqual(admin_client.mock_calls, [])
        self.assertEqual(response.status_code, 404)
        self.assertEqual(response.json(), {"detail": "Registrar does not exist"})
        self.assertLog(
            LogEntryType.REGISTRAR_CERTIFICATION_DELETE,
            LoggerResult.FAIL,
            input_properties={"registrar_id": "Reg-1", "certification_id": "Cert-1"},
        )

    def test_delete_certification_invalid(self, admin_client, client):
        client.get_registrar_certifications.return_value = [_make_certification(1)]
        admin_client.delete_registrar_certification.side_effect = InvalidData(fields=["certification_id"])

        response = self.client.delete(
            reverse("ferda:api:registrar_certification", kwargs={"registrar_id": "Reg-1", "certification_id": "Cert-1"})
        )

        self.assertEqual(response.status_code, 400)
        self.assertEqual(response.json(), {"detail": "Invalid data", "fields": ["certification_id"]})
        self.assertLog(
            LogEntryType.REGISTRAR_CERTIFICATION_DELETE,
            LoggerResult.FAIL,
            input_properties={"registrar_id": "Reg-1", "certification_id": "Cert-1"},
        )
