from django.test import SimpleTestCase
from pydantic import ValidationError

from ferda.registry.registrar.schema import EppCredentialsCreate


class EppCredentialsCreateTest(SimpleTestCase):
    def test_validators_ok(self):
        data = (
            {"fingerprint": "00:01:02:03", "password": "Gazpacho"},
            {"cert_data_pem": "cert", "password": "Gazpacho"},
            {"fingerprint": "00:01:02:03", "template_credentials_id": "epp:1"},
            {"cert_data_pem": "cert", "template_credentials_id": "epp:1"},
        )

        for kwargs in data:
            with self.subTest(kwargs=kwargs):
                self.assertEqual(kwargs, EppCredentialsCreate(**kwargs).model_dump(exclude_unset=True))  # type: ignore[arg-type]

    def test_validators_error(self):
        cert_error = ValueError("Exactly one of fingerprint and cert_data_pem has to be defined")
        password_error = ValueError("Exactly one of password and template_credentials_id has to be defined")
        data = (  # type: ignore
            ({"fingerprint": "00:01:02:03"}, {password_error}),
            ({"cert_data_pem": "cert"}, {password_error}),
            ({"password": "Gazpacho"}, {cert_error}),
            ({"template_credentials_id": "epp:1"}, {cert_error}),
        )

        for kwargs, errors in data:
            with self.subTest(kwargs=kwargs, errors=errors):
                with self.assertRaises(ValidationError) as cm:
                    EppCredentialsCreate(**kwargs)  # type: ignore[arg-type]
                self.assertEqual(
                    {error["ctx"]["error"].args for error in cm.exception.errors()},
                    {error.args for error in errors},
                )
