from unittest.mock import call, patch

from django.contrib.auth import get_user_model
from django.test import TestCase, override_settings, tag
from django.urls import reverse
from regal.exceptions import GroupDoesNotExist, RegistrarDoesNotExist

from ferda.constants import LogEntryType, LoggerResult, LogReferenceType
from ferda.tests.utils import LoggerMixin

User = get_user_model()


@tag("api", "registry")
@patch("ferda.registry.registrar.groups.REGISTRAR", autospec=True)
@override_settings(ROOT_URLCONF="ferda.tests.urls")
class GroupsTest(LoggerMixin, TestCase):
    """Test class for registrar groups."""

    def setUp(self):
        super().setUp()
        user = User.objects.create_superuser("admin")
        self.client.force_login(user)

    # --- get_registrar_groups

    def test_get_registrar_groups(self, client):
        groups = ["group 1", "group 2", "group 3", "group 4"]
        client.get_registrar_groups.return_value = groups

        response = self.client.get(reverse("ferda:api:registrar_groups"))

        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.json(), groups)
        self.assertEqual(client.mock_calls, [call.get_registrar_groups()])

    # --- get_registrar_groups_membership

    def test_get_group_membership(self, client):
        groups = ["group 1", "group 2", "group 3", "group 4"]
        client.get_registrar_groups_membership.return_value = groups

        response = self.client.get(reverse("ferda:api:registrar_groups", kwargs={"registrar_id": "Reg-1"}))

        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.json(), groups)
        self.assertEqual(client.mock_calls, [call.get_registrar_groups_membership("Reg-1")])
        self.assertLog(
            LogEntryType.REGISTRAR_GROUP_VIEW,
            LoggerResult.SUCCESS,
            input_properties={"registrar_id": "Reg-1"},
            output_properties={"items_count": str(len(groups))},
            output_references={LogReferenceType.REGISTRAR: "Reg-1"},
        )

    def test_get_group_membership_empty(self, client):
        client.get_registrar_groups_membership.return_value = []

        response = self.client.get(reverse("ferda:api:registrar_groups", kwargs={"registrar_id": "Reg-1"}))

        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.json(), [])
        self.assertLog(
            LogEntryType.REGISTRAR_GROUP_VIEW,
            LoggerResult.SUCCESS,
            input_properties={"registrar_id": "Reg-1"},
            output_references={LogReferenceType.REGISTRAR: "Reg-1"},
        )

    def test_get_group_membership_not_found(self, client):
        client.get_registrar_groups_membership.side_effect = RegistrarDoesNotExist

        response = self.client.get(reverse("ferda:api:registrar_groups", kwargs={"registrar_id": "Reg-1"}))

        self.assertEqual(response.status_code, 404)
        self.assertEqual(response.json(), {"detail": "Registrar does not exist"})
        self.assertLog(
            LogEntryType.REGISTRAR_GROUP_VIEW,
            LoggerResult.FAIL,
            input_properties={"registrar_id": "Reg-1"},
        )


@tag("api", "registry")
@patch("ferda.registry.registrar.groups.REGISTRAR_ADMIN", autospec=True)
@override_settings(ROOT_URLCONF="ferda.tests.urls")
class GroupsAdminTest(LoggerMixin, TestCase):
    """Test class for registrar groups."""

    def setUp(self):
        super().setUp()
        user = User.objects.create_superuser("admin")
        self.client.force_login(user)

    # --- add_registrar_group_membership

    def test_add_group_membership(self, client):
        groups = ["group 1", "group 2", "group 3", "group 4"]
        client.add_registrar_group_membership.return_value = groups

        response = self.client.post(
            reverse("ferda:api:registrar_groups", kwargs={"registrar_id": "Reg-1", "group": "group 4"})
        )

        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.json(), groups)
        self.assertEqual(client.mock_calls, [call.add_registrar_group_membership("Reg-1", "group 4")])
        self.assertLog(
            LogEntryType.REGISTRAR_GROUP_CREATE,
            LoggerResult.SUCCESS,
            input_properties={"registrar_id": "Reg-1", "group": "group 4"},
            output_properties={"items_count": str(len(groups))},
            output_references={LogReferenceType.REGISTRAR: "Reg-1"},
        )

    def test_add_group_membership_no_reg(self, client):
        client.add_registrar_group_membership.side_effect = RegistrarDoesNotExist

        response = self.client.post(
            reverse("ferda:api:registrar_groups", kwargs={"registrar_id": "Reg-1", "group": "group 4"})
        )

        self.assertEqual(response.status_code, 404)
        self.assertEqual(response.json(), {"detail": "Registrar does not exist"})
        self.assertLog(
            LogEntryType.REGISTRAR_GROUP_CREATE,
            LoggerResult.FAIL,
            input_properties={"registrar_id": "Reg-1", "group": "group 4"},
        )

    def test_add_group_membership_no_grp(self, client):
        client.add_registrar_group_membership.side_effect = GroupDoesNotExist

        response = self.client.post(
            reverse("ferda:api:registrar_groups", kwargs={"registrar_id": "Reg-1", "group": "group 4"})
        )

        self.assertEqual(response.status_code, 404)
        self.assertEqual(response.json(), {"detail": "Group does not exist"})
        self.assertLog(
            LogEntryType.REGISTRAR_GROUP_CREATE,
            LoggerResult.FAIL,
            input_properties={"registrar_id": "Reg-1", "group": "group 4"},
        )

    # --- delete_registrar_group_membership

    def test_delete_group_membership(self, client):
        groups = ["group 1", "group 2", "group 3"]
        client.delete_registrar_group_membership.return_value = groups

        response = self.client.delete(
            reverse("ferda:api:registrar_groups", kwargs={"registrar_id": "Reg-1", "group": "group 4"})
        )

        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.json(), groups)
        self.assertEqual(client.mock_calls, [call.delete_registrar_group_membership("Reg-1", "group 4")])
        self.assertLog(
            LogEntryType.REGISTRAR_GROUP_DELETE,
            LoggerResult.SUCCESS,
            input_properties={"registrar_id": "Reg-1", "group": "group 4"},
            output_properties={"items_count": str(len(groups))},
            output_references={LogReferenceType.REGISTRAR: "Reg-1"},
        )

    def test_delete_group_membership_empty(self, client):
        client.delete_registrar_group_membership.return_value = []

        response = self.client.delete(
            reverse("ferda:api:registrar_groups", kwargs={"registrar_id": "Reg-1", "group": "group 4"})
        )

        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.json(), [])
        self.assertLog(
            LogEntryType.REGISTRAR_GROUP_DELETE,
            LoggerResult.SUCCESS,
            input_properties={"registrar_id": "Reg-1", "group": "group 4"},
            output_references={LogReferenceType.REGISTRAR: "Reg-1"},
        )

    def test_delete_group_membership_no_reg(self, client):
        client.delete_registrar_group_membership.side_effect = RegistrarDoesNotExist

        response = self.client.delete(
            reverse("ferda:api:registrar_groups", kwargs={"registrar_id": "Reg-1", "group": "group 4"})
        )

        self.assertEqual(response.status_code, 404)
        self.assertEqual(response.json(), {"detail": "Registrar does not exist"})
        self.assertLog(
            LogEntryType.REGISTRAR_GROUP_DELETE,
            LoggerResult.FAIL,
            input_properties={"registrar_id": "Reg-1", "group": "group 4"},
        )

    def test_delete_group_membership_no_grp(self, client):
        client.delete_registrar_group_membership.side_effect = GroupDoesNotExist

        response = self.client.delete(
            reverse("ferda:api:registrar_groups", kwargs={"registrar_id": "Reg-1", "group": "group 4"})
        )

        self.assertEqual(response.status_code, 404)
        self.assertEqual(response.json(), {"detail": "Group does not exist"})
        self.assertLog(
            LogEntryType.REGISTRAR_GROUP_DELETE,
            LoggerResult.FAIL,
            input_properties={"registrar_id": "Reg-1", "group": "group 4"},
        )
