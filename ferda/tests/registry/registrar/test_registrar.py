from decimal import Decimal
from unittest.mock import call, patch

from django.contrib.auth import get_user_model
from django.contrib.auth.models import Permission
from django.test import TestCase, override_settings, tag
from django.urls import reverse
from frgal.utils import make_awaitable
from grill import LogProperty
from regal import Address, Registrar
from regal.exceptions import InvalidData, RegistrarAlreadyExists, RegistrarDoesNotExist

from ferda.constants import LogEntryType, LoggerResult
from ferda.tests.utils import LoggerMixin

User = get_user_model()


@tag("api", "registry")
@patch("ferda.registry.registrar.registrar.REGISTRAR_ADMIN")
@override_settings(ROOT_URLCONF="ferda.tests.urls")
class CreateRegistrarTest(LoggerMixin, TestCase):
    def setUp(self):
        super().setUp()
        user = User.objects.create_superuser("admin")  # type: ignore
        self.client.force_login(user)

    def test_create_registrar(self, client):
        client.create_registrar.return_value = make_awaitable(None)
        data = {
            "registrar_handle": "MY-REG",
            "name": "My registrar",
            "is_internal": False,
        }

        response = self.client.post(reverse("ferda:api:registrars"), data=data, content_type="application/json")

        self.assertEqual(response.status_code, 200)
        self.assertEqual(client.mock_calls, [call.create_registrar(Registrar(**data))])  # type: ignore[arg-type]
        self.assertLog(
            LogEntryType.REGISTRAR_CREATE,
            LoggerResult.SUCCESS,
            input_properties={
                "registrar_id": "MY-REG",
                "registrar_handle": "MY-REG",
                "name": "My registrar",
                "is_internal": "False",
            },
            input_references={"registrar": "MY-REG"},
        )

    def _test_create_registrar_error(self, client, error, status_code, error_data):
        data = {"registrar_handle": "MY-REG", "name": "My registrar", "is_internal": False}
        client.create_registrar.side_effect = error

        response = self.client.post(reverse("ferda:api:registrars"), data=data, content_type="application/json")

        self.assertEqual(response.status_code, status_code)
        self.assertEqual(response.json(), error_data)
        self.assertLog(
            LogEntryType.REGISTRAR_CREATE,
            LoggerResult.FAIL,
            input_properties={
                "registrar_id": "MY-REG",
                "registrar_handle": "MY-REG",
                "name": "My registrar",
                "is_internal": "False",
            },
            input_references={"registrar": "MY-REG"},
        )

    def test_create_registrar_invalid_data(self, client):
        self._test_create_registrar_error(
            client,
            InvalidData(fields=["registrar_handle"]),
            400,
            {"detail": "Invalid data", "fields": ["registrar_handle"]},
        )

    def test_create_registrar_already_exists(self, client):
        self._test_create_registrar_error(client, RegistrarAlreadyExists, 400, {"detail": "Registrar already exists"})


@tag("api", "registry")
@patch("ferda.registry.registrar.registrar.REGISTRAR")
@override_settings(ROOT_URLCONF="ferda.tests.urls")
class ListRegistrarsTest(LoggerMixin, TestCase):
    def setUp(self):
        super().setUp()
        user = User.objects.create_superuser("admin")  # type: ignore
        self.client.force_login(user)

    def test_get_registrars_list(self, client):
        client.get_registrar_handles.return_value = make_awaitable(["REG-TEST_A"])
        client.get_registrar_info.return_value = make_awaitable(
            Registrar(registrar_handle="REG-TEST_A", name="Test name")  # type: ignore[call-arg]
        )
        client.get_registrar_zone_access.return_value = make_awaitable({"cz": True})
        client.get_registrar_credit.return_value = make_awaitable({"cz": Decimal("1000.00")})

        response = self.client.get(reverse("ferda:api:registrars"))

        self.assertLog(LogEntryType.REGISTRAR_LIST, LoggerResult.SUCCESS, references={"registrar": ["REG-TEST_A"]})
        self.assertEqual(response.status_code, 200)
        self.assertEqual(
            response.json(),
            [
                {
                    "info": {
                        "registrar_id": "REG-TEST_A",
                        "registrar_handle": "REG-TEST_A",
                        "name": "Test name",
                        "organization": "",
                        "place": None,
                        "telephone": None,
                        "fax": None,
                        "emails": [],
                        "url": None,
                        "company_registration_number": None,
                        "vat_identification_number": None,
                        "is_system_registrar": False,
                        "variable_symbol": "",
                        "payment_memo_regex": "",
                        "is_vat_payer": False,
                        "is_internal": False,
                    },
                    "credit": {"cz": "1000.00"},
                    "zone_access": {"cz": True},
                }
            ],
        )

    def test_get_registrar_list_credit_authorized_user(self, client):
        client.get_registrar_handles.return_value = make_awaitable(["REG-TEST_A"])
        client.get_registrar_info.return_value = make_awaitable(
            Registrar(registrar_handle="REG-TEST_A", name="Test name")  # type: ignore[call-arg]
        )
        client.get_registrar_zone_access.return_value = make_awaitable({"cz": True})
        client.get_registrar_credit.return_value = make_awaitable({"cz": Decimal("1000.00")})

        response = self.client.get(reverse("ferda:api:registrars"))

        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.json()[0]["credit"], {"cz": "1000.00"})

    def test_get_registrar_list_credit_unauthorized_user(self, client):
        unauthorized_user = User.objects.create_user("johndoe", "john@example.com", "password")
        unauthorized_user.user_permissions.set(Permission.objects.filter(codename__in=("can_view_registry",)))
        self.client.force_login(unauthorized_user)

        client.get_registrar_handles.return_value = make_awaitable(["REG-TEST_A"])
        client.get_registrar_info.return_value = make_awaitable(
            Registrar(registrar_handle="REG-TEST_A", name="Test name")  # type: ignore[call-arg]
        )
        client.get_registrar_zone_access.return_value = make_awaitable({"cz": True})

        response = self.client.get(reverse("ferda:api:registrars"))

        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.json()[0]["credit"], None)


@tag("api", "registry")
@patch("ferda.registry.registrar.registrar.REGISTRAR")
@override_settings(ROOT_URLCONF="ferda.tests.urls")
class GetRegistrarTest(LoggerMixin, TestCase):
    def setUp(self):
        super().setUp()
        user = User.objects.create_superuser("admin")  # type: ignore
        self.client.force_login(user)

    def test_get_registrar(self, client):
        client.get_registrar_info.return_value = make_awaitable(
            Registrar(registrar_handle="REG-TEST_A", name="Test name")  # type: ignore[call-arg]
        )
        client.get_registrar_credit.return_value = make_awaitable({"cz": Decimal("1000.00")})
        client.get_registrar_zone_access.return_value = make_awaitable({"cz": True})

        response = self.client.get(reverse("ferda:api:registrar", kwargs={"registrar_id": "REG-TEST_A"}))

        self.assertLog(
            LogEntryType.REGISTRAR_DETAIL, LoggerResult.SUCCESS, input_references={"registrar": "REG-TEST_A"}
        )
        self.assertEqual(response.status_code, 200)
        self.assertEqual(
            response.json(),
            {
                "info": {
                    "registrar_id": "REG-TEST_A",
                    "name": "Test name",
                    "registrar_handle": "REG-TEST_A",
                    "company_registration_number": None,
                    "emails": [],
                    "fax": None,
                    "is_system_registrar": False,
                    "is_vat_payer": False,
                    "organization": "",
                    "payment_memo_regex": "",
                    "place": None,
                    "telephone": None,
                    "url": None,
                    "variable_symbol": "",
                    "vat_identification_number": None,
                    "is_internal": False,
                },
                "credit": {"cz": "1000.00"},
                "zone_access": {"cz": True},
            },
        )

    def test_get_registrar_does_not_exist(self, client):
        client.get_registrar_info.side_effect = RegistrarDoesNotExist

        response = self.client.get(reverse("ferda:api:registrar", kwargs={"registrar_id": "REG-TEST_A"}))

        self.assertLog(LogEntryType.REGISTRAR_DETAIL, LoggerResult.FAIL, input_references={"registrar": "REG-TEST_A"})
        self.assertEqual(response.status_code, 404)
        self.assertEqual(response.json(), {"detail": "Registrar does not exist"})

    def test_get_registrar_credit_authorized_user(self, client):
        client.get_registrar_info.return_value = make_awaitable(
            Registrar(registrar_handle="REG-TEST_A", name="Test name")  # type: ignore[call-arg]
        )
        client.get_registrar_credit.return_value = make_awaitable({"cz": Decimal("1000.00")})
        client.get_registrar_zone_access.return_value = make_awaitable({"cz": True})

        response = self.client.get(reverse("ferda:api:registrar", kwargs={"registrar_id": "REG-TEST_A"}))

        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.json()["credit"], {"cz": "1000.00"})

    def test_get_registrar_credit_unauthorized_user(self, client):
        unauthorized_user = User.objects.create_user("johndoe", "john@example.com", "password")
        unauthorized_user.user_permissions.set(Permission.objects.filter(codename__in=("can_view_registry",)))
        self.client.force_login(unauthorized_user)

        client.get_registrar_info.return_value = make_awaitable(
            Registrar(registrar_handle="REG-TEST_A", name="Test name")  # type: ignore[call-arg]
        )
        client.get_registrar_zone_access.return_value = make_awaitable({"cz": True})

        response = self.client.get(reverse("ferda:api:registrar", kwargs={"registrar_id": "REG-TEST_A"}))

        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.json()["credit"], None)


@tag("api", "registry")
@patch("ferda.registry.registrar.registrar.REGISTRAR_ADMIN")
@override_settings(ROOT_URLCONF="ferda.tests.urls")
class UpdateRegistrarTest(LoggerMixin, TestCase):
    def setUp(self):
        super().setUp()
        user = User.objects.create_superuser("admin")  # type: ignore
        self.client.force_login(user)

    def test_update_registrar(self, client):
        client.update_registrar.return_value = make_awaitable(None)
        data = {
            "organization": "Jupiter Mining Corp.",
            "is_internal": True,
        }

        response = self.client.put(
            reverse("ferda:api:registrar", kwargs={"registrar_id": "JMC"}),
            data=data,
            content_type="application/json",
        )

        self.assertEqual(response.status_code, 200)
        self.assertEqual(client.mock_calls, [call.update_registrar("JMC", **data)])
        self.assertLog(
            LogEntryType.REGISTRAR_UPDATE,
            LoggerResult.SUCCESS,
            input_properties={"organization": "Jupiter Mining Corp.", "is_internal": "True"},
            input_references={"registrar": "JMC"},
        )

    def test_update_registrar_complex_properties(self, client):
        client.update_registrar.return_value = make_awaitable(None)
        data = {
            "emails": ["holly@example.com"],
            "place": {
                "street": ["Deck 5", "Bulkhead 3"],
                "city": "Red Dwarf",
                "postal_code": "12345",
                "country_code": "JMC",
            },
        }

        response = self.client.put(
            reverse("ferda:api:registrar", kwargs={"registrar_id": "JMC"}),
            data=data,
            content_type="application/json",
        )

        self.assertEqual(response.status_code, 200)
        call_address = Address(
            street=["Deck 5", "Bulkhead 3"], city="Red Dwarf", postal_code="12345", country_code="JMC"
        )
        self.assertEqual(
            client.mock_calls,
            [
                call.update_registrar("JMC", emails=["holly@example.com"], place=call_address),
            ],
        )
        self.assertLog(
            LogEntryType.REGISTRAR_UPDATE,
            LoggerResult.SUCCESS,
            input_properties={
                "emails": ["holly@example.com"],
                "place": LogProperty(
                    "",
                    children={
                        "street": "Deck 5, Bulkhead 3",
                        "city": "Red Dwarf",
                        "state_or_province": "",
                        "postal_code": "12345",
                        "country_code": "JMC",
                    },
                ),
            },
            input_references={"registrar": "JMC"},
        )

    def _test_update_registrar_error(self, client, error, status_code, error_data):
        client.update_registrar.side_effect = error

        response = self.client.put(
            reverse("ferda:api:registrar", kwargs={"registrar_id": "MY-REG"}),
            data={"emails": ["holly@example.com"]},
            content_type="application/json",
        )

        self.assertLog(
            LogEntryType.REGISTRAR_UPDATE,
            LoggerResult.FAIL,
            input_properties={"emails": ["holly@example.com"]},
            input_references={"registrar": "MY-REG"},
        )
        self.assertEqual(response.status_code, status_code)
        self.assertEqual(response.json(), error_data)

    def test_update_registrar_does_not_exist(self, client):
        self._test_update_registrar_error(client, RegistrarDoesNotExist, 404, {"detail": "Registrar does not exist"})

    def test_update_registrar_invalid_data(self, client):
        self._test_update_registrar_error(
            client, InvalidData(fields=["emails"]), 400, {"detail": "Invalid data", "fields": ["emails"]}
        )
