from datetime import datetime, timezone
from pathlib import Path
from unittest.mock import call, patch

from django.contrib.auth import get_user_model
from django.test import TestCase, override_settings, tag
from django.urls import reverse
from frgal.utils import make_awaitable
from regal.exceptions import EppCredentialsDoesNotExist, InvalidData, RegistrarDoesNotExist
from regal.registrar import EppCredentialsId, SslCertificate

from ferda.constants import LogEntryType, LoggerResult
from ferda.tests.utils import LoggerMixin

User = get_user_model()
DATA_DIR = Path(__file__).parent / "data"


@tag("api", "registry")
@patch("ferda.registry.registrar.epp_credentials.REGISTRAR")
@override_settings(ROOT_URLCONF="ferda.tests.urls")
class GetEppCredentialsTest(LoggerMixin, TestCase):
    def setUp(self):
        super().setUp()
        user = User.objects.create_superuser("admin")  # type: ignore
        self.client.force_login(user)

    def test_get_epp_credentials(self, client):
        epp_credentials = [
            {
                "credentials_id": EppCredentialsId("epp:1"),
                "create_time": datetime(2022, 1, 1, tzinfo=timezone.utc),
                "certificate": SslCertificate(fingerprint="00:01:02:03"),
            }
        ]
        client.get_registrar_epp_credentials.return_value = make_awaitable(epp_credentials)

        response = self.client.get(reverse("ferda:api:registrar_epp_credentials", kwargs={"handle": "REG-JMC"}))

        self.assertEqual(response.status_code, 200)
        self.assertEqual(
            response.json(),
            [
                {
                    "credentials_id": "epp:1",
                    "create_time": "2022-01-01T00:00:00Z",
                    "certificate": {
                        "fingerprint": "00:01:02:03",
                        "cert_data_pem": "",
                        "subject": None,
                        "issuer": None,
                        "not_valid_before": None,
                        "not_valid_after": None,
                    },
                }
            ],
        )
        self.assertEqual(client.mock_calls, [call.get_registrar_epp_credentials("REG-JMC")])
        self.assertLog(
            LogEntryType.REGISTRAR_EPP_CREDENTIALS_VIEW,
            LoggerResult.SUCCESS,
            input_references={"registrar": "REG-JMC"},
        )

    def test_get_epp_credentials_registrar_not_found(self, client):
        client.get_registrar_epp_credentials.side_effect = RegistrarDoesNotExist

        response = self.client.get(reverse("ferda:api:registrar_epp_credentials", kwargs={"handle": "REG-JMC"}))

        self.assertEqual(response.status_code, 404)
        self.assertEqual(response.json(), {"detail": "Registrar does not exist"})
        self.assertEqual(client.mock_calls, [call.get_registrar_epp_credentials("REG-JMC")])
        self.assertLog(
            LogEntryType.REGISTRAR_EPP_CREDENTIALS_VIEW,
            LoggerResult.FAIL,
            input_references={"registrar": "REG-JMC"},
        )


@tag("api", "registry")
@patch("ferda.registry.registrar.epp_credentials.REGISTRAR_ADMIN")
@override_settings(ROOT_URLCONF="ferda.tests.urls")
class CreateEppCredentialsTest(LoggerMixin, TestCase):
    def setUp(self):
        super().setUp()
        user = User.objects.create_superuser("admin")  # type: ignore
        self.client.force_login(user)

    def test_create_epp_credentials_fingerprint_password(self, client):
        client.add_epp_credentials.return_value = make_awaitable("epp:1")

        response = self.client.post(
            reverse("ferda:api:registrar_epp_credentials", kwargs={"handle": "REG-JMC"}),
            data={"fingerprint": "00:01:02:03", "password": "Gazpacho"},
            content_type="application/json",
        )

        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.json(), "epp:1")
        self.assertEqual(
            client.mock_calls,
            [
                call.add_epp_credentials(
                    "REG-JMC",
                    SslCertificate(fingerprint="00:01:02:03"),
                    password="Gazpacho",
                    template_credentials_id=None,
                ),
            ],
        )
        self.assertLog(
            LogEntryType.REGISTRAR_EPP_CREDENTIALS_CREATE,
            LoggerResult.SUCCESS,
            input_properties={"fingerprint": "00:01:02:03"},
            input_references={"registrar": "REG-JMC"},
        )

    def test_create_epp_credentials_pem_template(self, client):
        client.add_epp_credentials.return_value = make_awaitable("epp:1")

        with open(DATA_DIR / "cert.pem") as fh:
            cert = fh.read()

        response = self.client.post(
            reverse("ferda:api:registrar_epp_credentials", kwargs={"handle": "REG-JMC"}),
            data={"cert_data_pem": cert, "template_credentials_id": "epp:2"},
            content_type="application/json",
        )

        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.json(), "epp:1")
        self.assertEqual(
            client.mock_calls,
            [
                call.add_epp_credentials(
                    "REG-JMC",
                    SslCertificate.from_pem(cert),
                    password=None,
                    template_credentials_id="epp:2",
                ),
            ],
        )
        self.assertLog(
            LogEntryType.REGISTRAR_EPP_CREDENTIALS_CREATE,
            LoggerResult.SUCCESS,
            content=cert,
            input_properties={"template_credentials_id": "epp:2"},
            input_references={"registrar": "REG-JMC"},
        )

    def test_create_epp_credentials_invalid_cert(self, client):
        response = self.client.post(
            reverse("ferda:api:registrar_epp_credentials", kwargs={"handle": "REG-JMC"}),
            data={"cert_data_pem": "invalid", "password": "Gazpacho"},
            content_type="application/json",
        )

        self.assertEqual(response.status_code, 400)
        self.assertEqual(response.json(), {"detail": "Invalid certificate", "fields": ["cert_data_pem"]})
        self.assertEqual(client.mock_calls, [])
        self.assertLog(
            LogEntryType.REGISTRAR_EPP_CREDENTIALS_CREATE,
            LoggerResult.FAIL,
            content="invalid",
            input_properties={},
            input_references={"registrar": "REG-JMC"},
        )

    def test_create_epp_credentials_invalid_fingerprint(self, client):
        response = self.client.post(
            reverse("ferda:api:registrar_epp_credentials", kwargs={"handle": "REG-JMC"}),
            data={"fingerprint": "invalid", "password": "Gazpacho"},
            content_type="application/json",
        )

        self.assertEqual(response.status_code, 400)
        self.assertEqual(response.json(), {"detail": "Invalid fingerprint", "fields": ["fingerprint"]})
        self.assertEqual(client.mock_calls, [])
        self.assertLog(
            LogEntryType.REGISTRAR_EPP_CREDENTIALS_CREATE,
            LoggerResult.FAIL,
            input_properties={"fingerprint": "invalid"},
            input_references={"registrar": "REG-JMC"},
        )

    def _test_create_epp_credentials_error(self, client, error, status_code, data):
        client.add_epp_credentials.side_effect = error

        response = self.client.post(
            reverse("ferda:api:registrar_epp_credentials", kwargs={"handle": "REG-JMC"}),
            data={"fingerprint": "00:01:02:03", "password": "Gazpacho"},
            content_type="application/json",
        )

        self.assertEqual(response.status_code, status_code)
        self.assertEqual(response.json(), data)
        self.assertEqual(
            client.mock_calls,
            [
                call.add_epp_credentials(
                    "REG-JMC",
                    SslCertificate(fingerprint="00:01:02:03"),
                    password="Gazpacho",
                    template_credentials_id=None,
                ),
            ],
        )
        self.assertLog(
            LogEntryType.REGISTRAR_EPP_CREDENTIALS_CREATE,
            LoggerResult.FAIL,
            input_properties={"fingerprint": "00:01:02:03"},
            input_references={"registrar": "REG-JMC"},
        )

    def test_create_epp_credentials_registrar_not_found(self, client):
        self._test_create_epp_credentials_error(
            client,
            RegistrarDoesNotExist,
            404,
            {"detail": "Registrar does not exist"},
        )

    def test_create_epp_credentials_not_found(self, client):
        self._test_create_epp_credentials_error(
            client,
            EppCredentialsDoesNotExist,
            404,
            {"detail": "EPP credentials does not exist"},
        )

    def test_create_epp_credentials_invalid_data(self, client):
        self._test_create_epp_credentials_error(
            client,
            InvalidData(fields=["password", "certificate.fingerprint"]),
            400,
            {"detail": "Invalid data", "fields": ["password", "fingerprint"]},
        )


@tag("api", "registry")
@patch("ferda.registry.registrar.epp_credentials.REGISTRAR_ADMIN")
@override_settings(ROOT_URLCONF="ferda.tests.urls")
class UpdateEppCredentialsTest(LoggerMixin, TestCase):
    def setUp(self):
        super().setUp()
        user = User.objects.create_superuser("admin")  # type: ignore
        self.client.force_login(user)

    def test_update_epp_credentials(self, client):
        client.update_epp_credentials.return_value = make_awaitable(None)

        response = self.client.put(
            reverse("ferda:api:registrar_epp_credential", kwargs={"handle": "REG-JMC", "credentials_id": "epp:1"}),
            data={"password": "Gazpacho"},
            content_type="application/json",
        )

        self.assertEqual(response.status_code, 200)
        self.assertEqual(client.mock_calls, [call.update_epp_credentials("epp:1", password="Gazpacho")])
        self.assertLog(
            LogEntryType.REGISTRAR_EPP_CREDENTIALS_UPDATE,
            LoggerResult.SUCCESS,
            input_properties={"credentials_id": "epp:1"},
            input_references={"registrar": "REG-JMC"},
        )

    def _test_update_epp_credentials_error(self, client, error, status_code, data):
        client.update_epp_credentials.side_effect = error

        response = self.client.put(
            reverse("ferda:api:registrar_epp_credential", kwargs={"handle": "REG-JMC", "credentials_id": "epp:1"}),
            data={"password": "Gazpacho"},
            content_type="application/json",
        )

        self.assertEqual(response.status_code, status_code)
        self.assertEqual(response.json(), data)
        self.assertEqual(client.mock_calls, [call.update_epp_credentials("epp:1", password="Gazpacho")])
        self.assertLog(
            LogEntryType.REGISTRAR_EPP_CREDENTIALS_UPDATE,
            LoggerResult.FAIL,
            input_properties={"credentials_id": "epp:1"},
            input_references={"registrar": "REG-JMC"},
        )

    def test_update_epp_credentials_does_not_exist(self, client):
        self._test_update_epp_credentials_error(
            client,
            EppCredentialsDoesNotExist,
            404,
            {"detail": "EPP credentials does not exist"},
        )

    def test_update_epp_credentials_invalid_data(self, client):
        self._test_update_epp_credentials_error(
            client,
            InvalidData(fields=["password", "certificate.fingerprint"]),
            400,
            {"detail": "Invalid data", "fields": ["password", "fingerprint"]},
        )


@tag("api", "registry")
@patch("ferda.registry.registrar.epp_credentials.REGISTRAR_ADMIN")
@override_settings(ROOT_URLCONF="ferda.tests.urls")
class DeleteEppCredentialsTest(LoggerMixin, TestCase):
    def setUp(self):
        super().setUp()
        user = User.objects.create_superuser("admin")  # type: ignore
        self.client.force_login(user)

    def test_delete_epp_credentials(self, client):
        client.delete_epp_credentials.return_value = make_awaitable(None)

        response = self.client.delete(
            reverse("ferda:api:registrar_epp_credential", kwargs={"handle": "REG-JMC", "credentials_id": "epp:1"}),
        )

        self.assertEqual(response.status_code, 200)
        self.assertEqual(client.mock_calls, [call.delete_epp_credentials("epp:1")])
        self.assertLog(
            LogEntryType.REGISTRAR_EPP_CREDENTIALS_DELETE,
            LoggerResult.SUCCESS,
            input_properties={"credentials_id": "epp:1"},
            input_references={"registrar": "REG-JMC"},
        )

    def test_delete_epp_credentials_does_not_exist(self, client):
        client.delete_epp_credentials.side_effect = EppCredentialsDoesNotExist

        response = self.client.delete(
            reverse("ferda:api:registrar_epp_credential", kwargs={"handle": "REG-JMC", "credentials_id": "epp:1"}),
        )

        self.assertEqual(response.status_code, 404)
        self.assertEqual(response.json(), {"detail": "EPP credentials does not exist"})
        self.assertEqual(client.mock_calls, [call.delete_epp_credentials("epp:1")])
        self.assertLog(
            LogEntryType.REGISTRAR_EPP_CREDENTIALS_DELETE,
            LoggerResult.FAIL,
            input_properties={"credentials_id": "epp:1"},
            input_references={"registrar": "REG-JMC"},
        )
