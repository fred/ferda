from uuid import UUID

from django.test import SimpleTestCase, override_settings

from ferda.registry.plugins.badges import AbstractBadger, Badge
from ferda.registry.plugins.badges.utils import get_contact_badges


class DummyBadger(AbstractBadger):
    @staticmethod
    def get_contact_badges(contact_id: UUID):
        return [Badge(name="prefect", image="http://example.com/badge.png")]


class TestAbstractBadger(SimpleTestCase):
    def test_get_contact_badges(self):
        self.assertEqual(AbstractBadger.get_contact_badges(UUID(int=42)), [])


class TestGetContactBadges(SimpleTestCase):
    def test_empty(self):
        self.assertEqual(get_contact_badges(UUID(int=42)), [])

    @override_settings(FERDA_BADGERS=["ferda.tests.registry.plugins.test_badges.DummyBadger"])
    def test_non_empty(self):
        self.assertEqual(
            get_contact_badges(UUID(int=42)), [Badge(name="prefect", image="http://example.com/badge.png")]
        )
