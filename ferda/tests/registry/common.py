"""Generic test suite for registry objects."""

from datetime import datetime, timezone
from io import BytesIO
from typing import Any, Dict, Type, Union
from unittest.mock import call, patch, sentinel
from uuid import UUID

from django.contrib.auth import get_user_model
from django.test import TestCase, override_settings
from django.urls import reverse
from fred_types import BaseObjectHistoryId, BaseObjectId
from regal import Contact, Domain, Keyset, Nsset
from regal.exceptions import InvalidHistoryInterval, ObjectDoesNotExist

from ferda.constants import LogEntryType, LoggerResult, LogReferenceType
from ferda.registry.constants import ObjectType
from ferda.tests.utils import LoggerMixin


class RecordStatementTest:
    """Wrapper class for."""

    @override_settings(ROOT_URLCONF="ferda.tests.urls", FERDA_RECORD_STATEMENT_TIMEOUT=sentinel.timeout)
    class Base(LoggerMixin, TestCase):
        """Parameters for specialization of record statement test.

        Attributes:
            object_id: Name of object identifier
            log_reference_type: Type of object referenced in log entry
            statementor_mock: Dotted path to statementor client (to mock callables)
            statement_call: Name of client method to create record statement (to mock)
            reverse_url: Reverse URL
        """

        object_id: str
        log_reference_type: LogReferenceType
        statementor_mock: str
        statement_call: str
        reverse_url: str

        object_uuid = str(UUID(int=1))

        def setUp(self):
            super().setUp()
            user = get_user_model().objects.create_superuser("admin")  # type: ignore
            self.client.force_login(user)

        def test_get_object_pdf(self):
            with patch(self.statementor_mock + "." + self.statement_call, autospec=True) as object_statement_mock:
                object_statement_mock.return_value = BytesIO(b"PDF content")
                response = self.client.get(reverse(self.reverse_url, kwargs={self.object_id: self.object_uuid}))
                self.assertLog(
                    LogEntryType.RECORD_STATEMENT,
                    LoggerResult.SUCCESS,
                    input_references={self.log_reference_type: self.object_uuid},
                )
                self.assertEqual(response.status_code, 200)
                self.assertEqual(response.headers["Content-Disposition"], 'inline; filename="record-statement.pdf"')
                self.assertEqual(response.content, b"PDF content")
                self.assertEqual(
                    object_statement_mock.mock_calls,
                    [
                        call(BaseObjectId(self.object_uuid), private=True, timeout=sentinel.timeout),
                    ],
                )

        def test_object_not_found(self):
            with patch(self.statementor_mock + "." + self.statement_call, autospec=True) as object_statement_mock:
                object_statement_mock.side_effect = ObjectDoesNotExist
                response = self.client.get(reverse(self.reverse_url, kwargs={self.object_id: self.object_uuid}))
                self.assertLog(
                    LogEntryType.RECORD_STATEMENT,
                    LoggerResult.FAIL,
                    input_references={self.log_reference_type: self.object_uuid},
                )
                self.assertEqual(response.status_code, 404)
                self.assertEqual(
                    object_statement_mock.mock_calls,
                    [
                        call(BaseObjectId(self.object_uuid), private=True, timeout=sentinel.timeout),
                    ],
                )


class GetObjectInfoTest:
    """Wrapper class for getting object information."""

    @override_settings(ROOT_URLCONF="ferda.tests.urls")
    class Base(LoggerMixin, TestCase):
        """Parameters for specialization of registry object tests.

        Attributes:
            object_model: Object type (in `regal` as Pydantic model)
            object_type: Type of registry object being tested
            object_id: Name of object identifier
            object_params: Additional parameters to create object instance
            log_entry_type: Log entry type for reading object information
            log_reference_type: Type of object referenced in log entry
            object_client_mock: Dotted path to registry object processing client (to mock callables)
            info_call: Name of client method to get object info (to mock)
            reverse_url: Reverse URL
        """

        object_model: Type[Union[Contact, Domain, Keyset, Nsset]]
        object_type: ObjectType
        object_id: str
        object_params: Dict[str, Any]
        log_entry_type: LogEntryType
        log_reference_type: LogReferenceType
        object_client_mock: str
        info_call: str
        reverse_url: str

        def setUp(self):
            super().setUp()
            user = get_user_model().objects.create_superuser("admin")  # type: ignore
            self.client.force_login(user)

        def test_get_info(self):
            with patch(self.object_client_mock + "." + self.info_call, autospec=True) as get_info_mock:
                id = "id:1"
                self.object_params[self.object_id] = id
                registry_object = self.object_model(**self.object_params)
                get_info_mock.return_value = registry_object

                response = self.client.get(reverse(self.reverse_url, kwargs={self.object_id: id}))

                self.assertEqual(get_info_mock.mock_calls, [call(BaseObjectId(id))])
                self.assertEqual(response.status_code, 200)
                self.assertEqual(response.json(), registry_object.model_dump())
                self.assertLog(
                    self.log_entry_type,
                    LoggerResult.SUCCESS,
                    input_properties={self.object_type: id},
                    output_references={self.log_reference_type: id},
                )

        def test_get_info_not_found(self):
            with patch(self.object_client_mock + "." + self.info_call, autospec=True) as get_info_mock:
                id = "id:1"
                get_info_mock.side_effect = ObjectDoesNotExist

                response = self.client.get(reverse(self.reverse_url, kwargs={self.object_id: id}))

                self.assertEqual(get_info_mock.mock_calls, [call(BaseObjectId(id))])
                self.assertEqual(response.status_code, 404)
                self.assertEqual(response.json(), {"detail": "Object does not exist"})
                self.assertLog(
                    self.log_entry_type,
                    LoggerResult.FAIL,
                    input_properties={self.object_type: id},
                )


class GetObjectHistoryTest:
    """Wrapper class for getting object history list."""

    @override_settings(ROOT_URLCONF="ferda.tests.urls")
    class Base(LoggerMixin, TestCase):
        """Parameters for specialization of registry object tests.

        Attributes:
            object_type: Type of registry object being tested
            object_id: Name of object identifier
            object_history_id: Name of object history identifier
            log_entry_type: Log entry type for reading object information
            log_reference_type: Type of object referenced in log entry
            object_client_mock: Dotted path to registry object processing client (to mock callables)
            history_list_call: Name of client method to get object info (to mock)
            reverse_url: Reverse URL
        """

        object_type: ObjectType
        object_id: str
        object_history_id: str
        log_entry_type: LogEntryType
        log_reference_type: LogReferenceType
        object_client_mock: str
        history_list_call: str
        reverse_url: str

        def setUp(self):
            super().setUp()
            user = get_user_model().objects.create_superuser("admin")  # type: ignore
            self.client.force_login(user)

        def test_get_history(self):
            with patch(self.object_client_mock + "." + self.history_list_call, autospec=True) as get_history_mock:
                id = "id:1"
                object_history = {
                    self.object_id: id,
                    "timeline": [
                        {
                            self.object_history_id: "hid:1",
                            "valid_from": "2023-01-01T00:00:00Z",
                            "log_entry_id": "lid:1",
                        },
                        {
                            self.object_history_id: "hid:2",
                            "valid_from": "2023-06-30T00:00:00Z",
                            "log_entry_id": "lid:2",
                        },
                    ],
                    "valid_to": None,
                }
                get_history_mock.return_value = object_history

                response = self.client.get(reverse(self.reverse_url, kwargs={self.object_id: id}))

                self.assertEqual(get_history_mock.mock_calls, [call(BaseObjectId(id))])
                self.assertEqual(response.status_code, 200)
                self.assertEqual(response.json(), object_history)
                self.assertLog(
                    self.log_entry_type,
                    LoggerResult.SUCCESS,
                    input_properties={self.object_type: id},
                    output_references={self.log_reference_type: id},
                )

        def test_get_history_not_found(self):
            with patch(self.object_client_mock + "." + self.history_list_call, autospec=True) as get_history_mock:
                id = "id:1"
                get_history_mock.side_effect = ObjectDoesNotExist

                response = self.client.get(reverse(self.reverse_url, kwargs={self.object_id: id}))

                self.assertEqual(get_history_mock.mock_calls, [call(BaseObjectId(id))])
                self.assertEqual(response.status_code, 404)
                self.assertEqual(response.json(), {"detail": "Object does not exist"})
                self.assertLog(
                    self.log_entry_type,
                    LoggerResult.FAIL,
                    input_properties={self.object_type: id},
                )


class GetObjectHistoryInfoTest:
    """Wrapper class for getting history object information (by identifier)."""

    @override_settings(ROOT_URLCONF="ferda.tests.urls")
    class Base(LoggerMixin, TestCase):
        """Parameters for specialization of registry object tests.

        Attributes:
            object_model: Object type (in `regal` as Pydantic model)
            object_type: Type of registry object being tested
            object_id: Name of object identifier
            object_history_id: Name of object history identifier
            object_params: Additional parameters to create object instance
            log_entry_type: Log entry type for reading object information
            log_reference_type: Type of object referenced in log entry
            object_client_mock: Dotted path to registry object processing client (to mock callables)
            info_call: Name of client method to get object info (to mock)
            reverse_url: Reverse URL
        """

        object_model: Type[Union[Contact, Domain, Keyset, Nsset]]
        object_type: ObjectType
        object_id: str
        object_history_id: str
        object_params: Dict[str, Any]
        log_entry_type: LogEntryType
        log_reference_type: LogReferenceType
        object_client_mock: str
        info_call: str
        reverse_url: str

        def setUp(self):
            super().setUp()
            user = get_user_model().objects.create_superuser("admin")  # type: ignore
            self.client.force_login(user)

        def test_get_info(self):
            with patch(self.object_client_mock + "." + self.info_call, autospec=True) as get_info_mock:
                id = "id:1"
                hid = "hid:1"
                self.object_params[self.object_id] = id
                self.object_params[self.object_history_id] = hid
                registry_object = self.object_model(**self.object_params)
                get_info_mock.return_value = registry_object

                response = self.client.get(reverse(self.reverse_url, kwargs={self.object_id: id, "history_id": hid}))

                self.assertEqual(get_info_mock.mock_calls, [call(BaseObjectId(id), BaseObjectHistoryId(hid))])
                self.assertEqual(response.status_code, 200)
                self.assertEqual(response.json(), registry_object.model_dump())
                self.assertLog(
                    self.log_entry_type,
                    LoggerResult.SUCCESS,
                    input_properties={self.object_type: id, "history_id": hid},
                    output_references={self.log_reference_type: id},
                )

        def test_get_info_not_found(self):
            with patch(self.object_client_mock + "." + self.info_call, autospec=True) as get_info_mock:
                id = "id:1"
                hid = "hid:1"
                get_info_mock.side_effect = ObjectDoesNotExist

                response = self.client.get(reverse(self.reverse_url, kwargs={self.object_id: id, "history_id": hid}))

                self.assertEqual(get_info_mock.mock_calls, [call(BaseObjectId(id), BaseObjectHistoryId(hid))])
                self.assertEqual(response.status_code, 404)
                self.assertEqual(response.json(), {"detail": "Object does not exist"})
                self.assertLog(
                    self.log_entry_type,
                    LoggerResult.FAIL,
                    input_properties={self.object_type: id, "history_id": hid},
                )


class GetObjectHistoryInfoDatetimeTest:
    """Wrapper class for getting history object information (by datetime)."""

    @override_settings(ROOT_URLCONF="ferda.tests.urls")
    class Base(LoggerMixin, TestCase):
        """Parameters for specialization of registry object tests.

        Attributes:
            object_model: Object type (in `regal` as Pydantic model)
            object_type: Type of registry object being tested
            object_id: Name of object identifier
            object_history_id: Name of object history identifier
            object_params: Additional parameters to create object instance
            log_entry_type: Log entry type for reading object information
            log_reference_type: Type of object referenced in log entry
            object_client_mock: Dotted path to registry object processing client (to mock callables)
            history_list_call: Name of client method to get object info (to mock)
            info_call: Name of client method to get object info (to mock)
            reverse_url: Reverse URL
        """

        object_model: Type[Union[Contact, Domain, Keyset, Nsset]]
        object_type: ObjectType
        object_id: str
        object_history_id: str
        object_params: Dict[str, Any]
        log_entry_type: LogEntryType
        log_reference_type: LogReferenceType
        object_client_mock: str
        history_list_call: str
        info_call: str
        reverse_url: str

        def setUp(self):
            super().setUp()
            user = get_user_model().objects.create_superuser("admin")  # type: ignore
            self.client.force_login(user)

        def test_get_info(self):
            with patch(self.object_client_mock + "." + self.info_call, autospec=True) as get_info_mock, patch(
                self.object_client_mock + "." + self.history_list_call, autospec=True
            ) as get_history_mock:
                id = "id:1"
                hid = "hid:1"
                when = datetime(2023, 1, 1, tzinfo=timezone.utc)
                get_history_mock.return_value = {
                    self.object_id: id,
                    "timeline": [
                        {self.object_history_id: hid, "valid_from": datetime(2023, 1, 1, tzinfo=timezone.utc)},
                        {self.object_history_id: "hid:2", "valid_from": datetime(2023, 1, 15, tzinfo=timezone.utc)},
                    ],
                    "valid_to": None,
                }
                self.object_params[self.object_id] = id
                self.object_params[self.object_history_id] = hid
                registry_object = self.object_model(**self.object_params)
                get_info_mock.return_value = registry_object

                response = self.client.get(reverse(self.reverse_url, kwargs={self.object_id: id, "when": when}))

                self.assertEqual(get_history_mock.mock_calls, [call(BaseObjectId(id), start=when)])
                self.assertEqual(get_info_mock.mock_calls, [call(BaseObjectId(id), BaseObjectHistoryId(hid))])
                self.assertEqual(response.status_code, 200)
                self.assertEqual(response.json(), registry_object.model_dump())
                self.assertLog(
                    self.log_entry_type,
                    LoggerResult.SUCCESS,
                    input_properties={self.object_type: id, "history_datetime": when.isoformat()},
                    output_properties={"found_history_id": hid},
                    output_references={self.log_reference_type: id},
                )

        def test_get_info_not_found(self):
            with patch(self.object_client_mock + "." + self.history_list_call, autospec=True) as get_history_mock:
                id = "id:1"
                when = datetime(2023, 1, 1, tzinfo=timezone.utc)
                get_history_mock.side_effect = ObjectDoesNotExist

                response = self.client.get(reverse(self.reverse_url, kwargs={self.object_id: id, "when": when}))

                self.assertEqual(get_history_mock.mock_calls, [call(BaseObjectId(id), start=when)])
                self.assertEqual(response.status_code, 404)
                self.assertEqual(response.json(), {"detail": "Object does not exist"})
                self.assertLog(
                    self.log_entry_type,
                    LoggerResult.FAIL,
                    input_properties={self.object_type: id, "history_datetime": when.isoformat()},
                )

        def test_get_info_invalid_interval(self):
            with patch(self.object_client_mock + "." + self.history_list_call, autospec=True) as get_history_mock:
                id = "id:1"
                when = datetime(2023, 1, 1, tzinfo=timezone.utc)
                get_history_mock.side_effect = InvalidHistoryInterval

                response = self.client.get(reverse(self.reverse_url, kwargs={self.object_id: id, "when": when}))

                self.assertEqual(get_history_mock.mock_calls, [call(BaseObjectId(id), start=when)])
                self.assertEqual(response.status_code, 404)
                self.assertEqual(response.json(), {"detail": "Invalid history interval"})
                self.assertLog(
                    self.log_entry_type,
                    LoggerResult.FAIL,
                    input_properties={self.object_type: id, "history_datetime": when.isoformat()},
                )

        def test_get_info_no_history(self):
            with patch(self.object_client_mock + "." + self.history_list_call, autospec=True) as get_history_mock:
                id = "id:1"
                when = datetime(2023, 1, 1, tzinfo=timezone.utc)
                get_history_mock.return_value = {self.object_id: id, "timeline": [], "valid_to": None}

                response = self.client.get(reverse(self.reverse_url, kwargs={self.object_id: id, "when": when}))

                self.assertEqual(get_history_mock.mock_calls, [call(BaseObjectId(id), start=when)])
                self.assertEqual(response.status_code, 404)
                self.assertEqual(response.json(), {"detail": "Object does not have a history"})
                self.assertLog(
                    self.log_entry_type,
                    LoggerResult.FAIL,
                    input_properties={self.object_type: id, "history_datetime": when.isoformat()},
                )


class GetObjectHistoryInfoLastTest:
    """Wrapper class for getting history object information (last known)."""

    @override_settings(ROOT_URLCONF="ferda.tests.urls")
    class Base(LoggerMixin, TestCase):
        """Parameters for specialization of registry object tests.

        Attributes:
            object_model: Object type (in `regal` as Pydantic model)
            object_type: Type of registry object being tested
            object_id: Name of object identifier
            object_history_id: Name of object history identifier
            object_params: Additional parameters to create object instance
            log_entry_type: Log entry type for reading object information
            log_reference_type: Type of object referenced in log entry
            object_client_mock: Dotted path to registry object processing client (to mock callables)
            history_list_call: Name of client method to get object info (to mock)
            info_call: Name of client method to get object info (to mock)
            reverse_url: Reverse URL
        """

        object_model: Type[Union[Contact, Domain, Keyset, Nsset]]
        object_type: ObjectType
        object_id: str
        object_history_id: str
        object_params: Dict[str, Any]
        log_entry_type: LogEntryType
        log_reference_type: LogReferenceType
        object_client_mock: str
        history_list_call: str
        info_call: str
        reverse_url: str

        def setUp(self):
            super().setUp()
            user = get_user_model().objects.create_superuser("admin")  # type: ignore
            self.client.force_login(user)

        def test_get_info(self):
            with patch(self.object_client_mock + "." + self.info_call, autospec=True) as get_info_mock, patch(
                self.object_client_mock + "." + self.history_list_call, autospec=True
            ) as get_history_mock:
                id = "id:1"
                hid = "hid:1"
                get_history_mock.return_value = {
                    self.object_id: id,
                    "timeline": [
                        {self.object_history_id: "hid:2", "valid_from": datetime(2023, 1, 1, tzinfo=timezone.utc)},
                        {self.object_history_id: hid, "valid_from": datetime(2023, 1, 15, tzinfo=timezone.utc)},
                    ],
                    "valid_to": None,
                }
                self.object_params[self.object_id] = id
                self.object_params[self.object_history_id] = hid
                registry_object = self.object_model(**self.object_params)
                get_info_mock.return_value = registry_object

                response = self.client.get(reverse(self.reverse_url, kwargs={self.object_id: id}))

                self.assertEqual(get_history_mock.mock_calls, [call(BaseObjectId(id))])
                self.assertEqual(get_info_mock.mock_calls, [call(BaseObjectId(id), BaseObjectHistoryId(hid))])
                self.assertEqual(response.status_code, 200)
                self.assertEqual(response.json(), registry_object.model_dump())
                self.assertLog(
                    self.log_entry_type,
                    LoggerResult.SUCCESS,
                    input_properties={self.object_type: id},
                    output_properties={"found_history_id": hid},
                    output_references={self.log_reference_type: id},
                )

        def test_get_info_not_found(self):
            with patch(self.object_client_mock + "." + self.history_list_call, autospec=True) as get_history_mock:
                id = "id:1"
                get_history_mock.side_effect = ObjectDoesNotExist

                response = self.client.get(reverse(self.reverse_url, kwargs={self.object_id: id}))

                self.assertEqual(get_history_mock.mock_calls, [call(BaseObjectId(id))])
                self.assertEqual(response.status_code, 404)
                self.assertEqual(response.json(), {"detail": "Object does not exist"})
                self.assertLog(
                    self.log_entry_type,
                    LoggerResult.FAIL,
                    input_properties={self.object_type: id},
                )

        def test_get_info_no_history(self):
            with patch(self.object_client_mock + "." + self.history_list_call, autospec=True) as get_history_mock:
                id = "id:1"
                get_history_mock.return_value = {self.object_id: id, "timeline": [], "valid_to": None}

                response = self.client.get(reverse(self.reverse_url, kwargs={self.object_id: id}))

                self.assertEqual(get_history_mock.mock_calls, [call(BaseObjectId(id))])
                self.assertEqual(response.status_code, 404)
                self.assertEqual(response.json(), {"detail": "Object does not have a history"})
                self.assertLog(
                    self.log_entry_type,
                    LoggerResult.FAIL,
                    input_properties={self.object_type: id},
                )


class GetObjectStateTest:
    """Wrapper class for getting object state flags."""

    @override_settings(ROOT_URLCONF="ferda.tests.urls")
    class Base(LoggerMixin, TestCase):
        """Parameters for specialization of registry object state tests.

        Attributes:
            object_type: Type of registry object being tested
            object_id: Name of object identifier
            log_entry_type: Log entry type for reading object information
            log_reference_type: Type of object referenced in log entry
            object_client_mock: Dotted path to registry object processing client (to mock callables)
            state_call: Name of client method to get object info (to mock)
            reverse_url: Reverse URL
        """

        object_type: ObjectType
        object_id: str
        log_entry_type: LogEntryType
        log_reference_type: LogReferenceType
        object_client_mock: str
        state_call: str
        reverse_url: str

        def setUp(self):
            super().setUp()
            user = get_user_model().objects.create_superuser("admin")  # type: ignore
            self.client.force_login(user)

        def test_get_state(self):
            with patch(self.object_client_mock + "." + self.state_call, autospec=True) as get_state_mock:
                id = "id:1"
                flags = {"flag-1": True, "flag-2": True, "flag-3": False, "flag-4": False}
                get_state_mock.return_value = flags

                response = self.client.get(reverse(self.reverse_url, kwargs={self.object_id: id}))

                self.assertEqual(get_state_mock.mock_calls, [call(BaseObjectId(id))])
                self.assertEqual(response.status_code, 200)
                self.assertEqual(response.json(), flags)
                self.assertLog(
                    self.log_entry_type,
                    LoggerResult.SUCCESS,
                    input_properties={self.object_type: id},
                    output_references={self.log_reference_type: id},
                )

        def test_get_state_not_found(self):
            with patch(self.object_client_mock + "." + self.state_call, autospec=True) as get_state_mock:
                id = "id:1"
                get_state_mock.side_effect = ObjectDoesNotExist

                response = self.client.get(reverse(self.reverse_url, kwargs={self.object_id: id}))

                self.assertEqual(get_state_mock.mock_calls, [call(BaseObjectId(id))])
                self.assertEqual(response.status_code, 404)
                self.assertEqual(response.json(), {"detail": "Object does not exist"})
                self.assertLog(
                    self.log_entry_type,
                    LoggerResult.FAIL,
                    input_properties={self.object_type: id},
                )


class GetObjectStateHistoryTest:
    """Wrapper class for getting object state flag history."""

    @override_settings(ROOT_URLCONF="ferda.tests.urls")
    class Base(LoggerMixin, TestCase):
        """Parameters for specialization of registry object state history tests.

        Attributes:
            object_type: Type of registry object being tested
            object_id: Name of object identifier
            log_entry_type: Log entry type for reading object information
            log_reference_type: Type of object referenced in log entry
            object_client_mock: Dotted path to registry object processing client (to mock callables)
            state_history_call: Name of client method to get object info (to mock)
            reverse_url: Reverse URL
        """

        object_type: ObjectType
        object_id: str
        log_entry_type: LogEntryType
        log_reference_type: LogReferenceType
        object_client_mock: str
        state_history_call: str
        reverse_url: str

        def setUp(self):
            super().setUp()
            user = get_user_model().objects.create_superuser("admin")  # type: ignore
            self.client.force_login(user)

        def test_get_state(self):
            with patch(
                self.object_client_mock + "." + self.state_history_call, autospec=True
            ) as get_state_history_mock:
                id = "id:1"
                flags_history: Dict[str, Any] = {
                    self.object_id: id,
                    "timeline": [
                        {
                            "valid_from": "2023-01-01T00:00:00Z",
                            "flags": {"linked": True, "serverBlocked": False},
                        },
                        {
                            "valid_from": "2023-02-10T00:00:00Z",
                            "flags": {"linked": False, "serverBlocked": False},
                        },
                        {
                            "valid_from": "2023-03-20T00:00:00Z",
                            "flags": {"linked": True, "serverBlocked": True},
                        },
                    ],
                    "valid_to": None,
                }
                get_state_history_mock.return_value = flags_history

                response = self.client.get(reverse(self.reverse_url, kwargs={self.object_id: id}))

                self.assertEqual(get_state_history_mock.mock_calls, [call(BaseObjectId(id))])
                self.assertEqual(response.status_code, 200)
                self.assertEqual(response.json(), flags_history)
                self.assertLog(
                    self.log_entry_type,
                    LoggerResult.SUCCESS,
                    input_properties={self.object_type: id, "history": "true"},
                    output_references={self.log_reference_type: id},
                )

        def test_get_state_not_found(self):
            with patch(
                self.object_client_mock + "." + self.state_history_call, autospec=True
            ) as get_state_history_mock:
                id = "id:1"
                get_state_history_mock.side_effect = ObjectDoesNotExist

                response = self.client.get(reverse(self.reverse_url, kwargs={self.object_id: id}))

                self.assertEqual(get_state_history_mock.mock_calls, [call(BaseObjectId(id))])
                self.assertEqual(response.status_code, 404)
                self.assertEqual(response.json(), {"detail": "Object does not exist"})
                self.assertLog(
                    self.log_entry_type,
                    LoggerResult.FAIL,
                    input_properties={self.object_type: id, "history": "true"},
                )
