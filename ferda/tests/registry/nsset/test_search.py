from django.urls import reverse_lazy
from regal import Nsset

from ferda.constants import LogEntryType
from ferda.tests.registry.search import SearchHistoryTest, SearchTest


class NssetSearchTest(SearchTest.Base):
    object_type = Nsset
    object_id = "nsset_id"
    object_handle = "nsset_handle"
    log_entry_type = LogEntryType.NSSET_SEARCH
    object_search_mock = "ferda.registry.nsset.search.NSSET_SEARCH"
    object_mock = "ferda.registry.nsset.search.NSSET"
    search_call = "search_nsset"
    info_call = "get_nsset_info"
    url = reverse_lazy("ferda:api:search_nsset")


class NssetHistorySearchTest(SearchHistoryTest.Base):
    object_type = Nsset
    object_id = "nsset_id"
    object_history_id = "nsset_history_id"
    object_handle = "nsset_handle"
    log_entry_type = LogEntryType.NSSET_SEARCH_HISTORY
    object_search_mock = "ferda.registry.nsset.search.NSSET_SEARCH"
    object_mock = "ferda.registry.nsset.search.NSSET"
    search_call = "search_nsset_history"
    info_call = "get_nsset_info"
    history_call = "get_nsset_history"
    url = reverse_lazy("ferda:api:search_nsset_history")
