from regal import Nsset

from ferda.constants import LogEntryType, LogReferenceType
from ferda.registry.constants import ObjectType
from ferda.tests.registry.common import (
    GetObjectHistoryInfoDatetimeTest,
    GetObjectHistoryInfoLastTest,
    GetObjectHistoryInfoTest,
    GetObjectHistoryTest,
    GetObjectInfoTest,
    GetObjectStateHistoryTest,
    GetObjectStateTest,
    RecordStatementTest,
)


class NssetRecordStatementTest(RecordStatementTest.Base):
    """Specialized test suite for nsset record statement."""

    object_id = "nsset_id"
    log_reference_type = LogReferenceType.NSSET
    statementor_mock = "ferda.registry.nsset.api.STATEMENTOR"
    statement_call = "get_nsset_statement"
    reverse_url = "ferda:api:get_nsset_record_statement_pdf"


class GetNssetInfoTest(GetObjectInfoTest.Base):
    """Specialized test suite for getting nsset info."""

    object_model = Nsset
    object_type = ObjectType.NSSET
    object_id = "nsset_id"
    object_params = {"nsset_handle": "handle:1"}
    log_entry_type = LogEntryType.NSSET_DETAIL
    log_reference_type = LogReferenceType.NSSET
    object_client_mock = "ferda.registry.nsset.api.NSSET"
    info_call = "get_nsset_info"
    reverse_url = "ferda:api:get_nsset_info"


class GetNssetHistoryTest(GetObjectHistoryTest.Base):
    """Specialized test suite for getting nsset history."""

    object_type = ObjectType.NSSET
    object_id = "nsset_id"
    object_history_id = "nsset_history_id"
    log_entry_type = LogEntryType.NSSET_HISTORY
    log_reference_type = LogReferenceType.NSSET
    object_client_mock = "ferda.registry.nsset.api.NSSET"
    history_list_call = "get_nsset_history"
    reverse_url = "ferda:api:get_nsset_history"


class GetNssetHistoryInfoTest(GetObjectHistoryInfoTest.Base):
    """Specialized test suite for getting nsset history info (by identifier)."""

    object_model = Nsset
    object_type = ObjectType.NSSET
    object_id = "nsset_id"
    object_history_id = "nsset_history_id"
    object_params = {"nsset_handle": "handle:1"}
    log_entry_type = LogEntryType.NSSET_DETAIL
    log_reference_type = LogReferenceType.NSSET
    object_client_mock = "ferda.registry.nsset.api.NSSET"
    info_call = "get_nsset_info"
    reverse_url = "ferda:api:get_nsset_history_info"


class GetNssetHistoryInfoDatetimeTest(GetObjectHistoryInfoDatetimeTest.Base):
    """Specialized test suite for getting nsset history info (by datetime)."""

    object_model = Nsset
    object_type = ObjectType.NSSET
    object_id = "nsset_id"
    object_history_id = "nsset_history_id"
    object_params = {"nsset_handle": "handle:1"}
    log_entry_type = LogEntryType.NSSET_DETAIL
    log_reference_type = LogReferenceType.NSSET
    object_client_mock = "ferda.registry.nsset.api.NSSET"
    history_list_call = "get_nsset_history"
    info_call = "get_nsset_info"
    reverse_url = "ferda:api:get_nsset_history_info_datetime"


class GetNssetHistoryInfoLastTest(GetObjectHistoryInfoLastTest.Base):
    """Specialized test suite for getting nsset history info (last known)."""

    object_model = Nsset
    object_type = ObjectType.NSSET
    object_id = "nsset_id"
    object_history_id = "nsset_history_id"
    object_params = {"nsset_handle": "handle:1"}
    log_entry_type = LogEntryType.NSSET_DETAIL
    log_reference_type = LogReferenceType.NSSET
    object_client_mock = "ferda.registry.nsset.api.NSSET"
    history_list_call = "get_nsset_history"
    info_call = "get_nsset_info"
    reverse_url = "ferda:api:get_nsset_history_info_last"


class GetNssetStateTest(GetObjectStateTest.Base):
    """Specialized test suite for getting nsset state."""

    object_type = ObjectType.NSSET
    object_id = "nsset_id"
    log_entry_type = LogEntryType.NSSET_STATE
    log_reference_type = LogReferenceType.NSSET
    object_client_mock = "ferda.registry.nsset.api.NSSET"
    state_call = "get_nsset_state"
    reverse_url = "ferda:api:get_nsset_state"


class GetNssetStateHistoryTest(GetObjectStateHistoryTest.Base):
    """Specialized test suite for getting nsset state history."""

    object_type = ObjectType.NSSET
    object_id = "nsset_id"
    log_entry_type = LogEntryType.NSSET_STATE
    log_reference_type = LogReferenceType.NSSET
    object_client_mock = "ferda.registry.nsset.api.NSSET"
    state_history_call = "get_nsset_state_history"
    reverse_url = "ferda:api:get_nsset_state_history"
