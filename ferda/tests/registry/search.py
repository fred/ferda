from datetime import datetime, timezone
from typing import Type, Union
from unittest.mock import call, patch

from django.contrib.auth import get_user_model
from django.test import override_settings
from regal import Contact, Domain, FuzzyValue, Keyset, Nsset
from regal.search import HistorySearchPeriod, SearchCurrentResult, SearchHistoryResult, SearchResults
from snapshottest.django import TestCase as SnapshotTestCase

from ferda.constants import LogEntryType, LoggerResult
from ferda.tests.utils import LoggerMixin


class SearchTestBase:
    """Base class for search tests.

    Attributes:
        object_type: Type of registry object.
        object_id: Name of `object_type` attribute containing object id
        object_handle: Name of `object_type` attribute containing object handle / fqdn
        log_entry_type: Log entry type.
        object_search_mock: Dotted path to registry search client instance.
        object_mock: Dotted path to registry info client instance.
        search_call: Search method name.
        info_call: Info method name.
        url: API view URL.
    """

    object_type: Type[Union[Contact, Domain, Keyset, Nsset]]
    object_id: str
    object_handle: str
    log_entry_type: LogEntryType
    object_search_mock: str
    object_mock: str
    search_call: str
    info_call: str
    url: str


class SearchTest:
    @override_settings(ROOT_URLCONF="ferda.tests.urls")
    class Base(LoggerMixin, SearchTestBase, SnapshotTestCase):
        def setUp(self):
            super().setUp()
            user = get_user_model().objects.create_superuser("admin")  # type: ignore
            self.client.force_login(user)

        def test_search_empty(self):
            with patch(self.object_search_mock, autospec=True) as search_mock:
                search_results = SearchResults[SearchCurrentResult]()
                getattr(search_mock, self.search_call).return_value = search_results

                response = self.client.get(self.url + "?query=some&query=words")

                self.assertEqual(response.status_code, 200)
                self.assertMatchSnapshot(response.json())
                self.assertEqual(
                    search_mock.mock_calls,
                    [
                        getattr(call, self.search_call)(["some", "words"], searched_items=[], limit=100),
                    ],
                )
                self.assertLog(
                    self.log_entry_type,
                    LoggerResult.SUCCESS,
                    input_properties={"query_values": ["some", "words"], "search_item": [], "limit": "100"},
                )

        def test_search(self):
            with patch(self.object_search_mock, autospec=True) as search_mock, patch(
                self.object_mock, autospec=True
            ) as object_mock:
                search_results = SearchResults[SearchCurrentResult](
                    results=[
                        SearchCurrentResult(object_id="o:1", matched_items=["handle"]),
                    ],
                    searched_items=["handle"],
                    estimated_total=FuzzyValue(lower_estimate=1, upper_estimate=1),
                )
                getattr(search_mock, self.search_call).return_value = search_results
                getattr(object_mock, self.info_call).return_value = self.object_type(
                    **{
                        self.object_id: "o:1",
                        self.object_handle: "OBJECT-1",
                    }
                )  # type: ignore[arg-type]

                response = self.client.get(self.url + "?query=some&query=words")

                self.assertEqual(response.status_code, 200)
                self.assertMatchSnapshot(response.json())
                self.assertEqual(
                    search_mock.mock_calls,
                    [
                        getattr(call, self.search_call)(["some", "words"], searched_items=[], limit=100),
                    ],
                )
                self.assertEqual(
                    object_mock.mock_calls,
                    [
                        getattr(call, self.info_call)("o:1"),
                    ],
                )
                self.assertLog(
                    self.log_entry_type,
                    LoggerResult.SUCCESS,
                    input_properties={"query_values": ["some", "words"], "search_item": [], "limit": "100"},
                )


class SearchHistoryTest:
    @override_settings(ROOT_URLCONF="ferda.tests.urls")
    class Base(LoggerMixin, SearchTestBase, SnapshotTestCase):
        """History search tests.

        Attributes:
            object_history_id: Name of `object_type` attribute containing object history id
            history_call: History method name.
        """

        object_history_id: str
        history_call: str

        def setUp(self):
            super().setUp()
            user = get_user_model().objects.create_superuser("admin")  # type: ignore
            self.client.force_login(user)

        def test_search_history_empty(self):
            with patch(self.object_search_mock, autospec=True) as search_mock:
                search_results = SearchResults[SearchHistoryResult]()
                getattr(search_mock, self.search_call).return_value = search_results

                response = self.client.get(self.url + "?query=some&query=words")

                self.assertEqual(response.status_code, 200)
                self.assertMatchSnapshot(response.json())
                self.assertEqual(
                    search_mock.mock_calls,
                    [
                        getattr(call, self.search_call)(
                            ["some", "words"],
                            searched_items=[],
                            limit=100,
                            start=None,
                            end=None,
                        ),
                    ],
                )
                self.assertLog(
                    self.log_entry_type,
                    LoggerResult.SUCCESS,
                    input_properties={"query_values": ["some", "words"], "search_item": [], "limit": "100"},
                )

        def test_search_history(self):
            search_results = SearchResults[SearchHistoryResult](
                results=[
                    SearchHistoryResult(
                        object_id="o:1",
                        histories=[
                            HistorySearchPeriod(
                                valid_from=datetime(2021, 1, 1, tzinfo=timezone.utc),
                                valid_to=datetime(2021, 4, 1, tzinfo=timezone.utc),
                                matched_items=["handle"],
                                history_ids=["o:2", "o:3"],
                            ),
                            HistorySearchPeriod(
                                valid_from=datetime(2021, 8, 1, tzinfo=timezone.utc),
                                valid_to=datetime(2021, 12, 1, tzinfo=timezone.utc),
                                matched_items=["handle"],
                                history_ids=["o:5", "o:6"],
                            ),
                        ],
                    ),
                ],
                searched_items=["handle"],
                estimated_total=FuzzyValue(lower_estimate=1, upper_estimate=1),
            )

            with patch(self.object_search_mock, autospec=True) as search_mock, patch(
                self.object_mock, autospec=True
            ) as object_mock:
                getattr(search_mock, self.search_call).return_value = search_results
                getattr(object_mock, self.info_call).side_effect = [
                    self.object_type(
                        **{
                            self.object_id: "o:1",
                            self.object_history_id: "o:7",
                            self.object_handle: "OBJECT-1",
                        }
                    ),  # type: ignore[arg-type]
                    self.object_type(
                        **{
                            self.object_id: "o:1",
                            self.object_history_id: "o:2",
                            self.object_handle: "OBJECT-1",
                        }
                    ),  # type: ignore[arg-type]
                    self.object_type(
                        **{
                            self.object_id: "o:1",
                            self.object_history_id: "o:5",
                            self.object_handle: "OBJECT-1",
                        }
                    ),  # type: ignore[arg-type]
                ]
                getattr(object_mock, self.history_call).return_value = {
                    self.object_id: "o:1",
                    "timeline": [
                        {self.object_history_id: "o:6", "valid_from": datetime(2021, 11, 1, tzinfo=timezone.utc)},
                        {self.object_history_id: "o:7", "valid_from": datetime(2022, 1, 1, tzinfo=timezone.utc)},
                    ],
                    "valid_to": None,
                }

                response = self.client.get(
                    self.url + "?query=some&query=words&valid_from=2021-01-01T00:00:00Z&valid_to=2022-01-01T00:00:00Z"
                )

            self.assertEqual(response.status_code, 200)
            self.assertMatchSnapshot(response.json())
            self.assertEqual(
                search_mock.mock_calls,
                [
                    getattr(call, self.search_call)(
                        ["some", "words"],
                        searched_items=[],
                        limit=100,
                        start=datetime(2021, 1, 1, tzinfo=timezone.utc),
                        end=datetime(2022, 1, 1, tzinfo=timezone.utc),
                    ),
                ],
            )
            self.assertEqual(
                object_mock.mock_calls,
                [
                    getattr(call, self.history_call)("o:1"),
                    getattr(call, self.info_call)("o:1", "o:7"),
                    getattr(call, self.info_call)("o:1", "o:2"),
                    getattr(call, self.info_call)("o:1", "o:5"),
                ],
            )
            self.assertLog(
                self.log_entry_type,
                LoggerResult.SUCCESS,
                input_properties={
                    "query_values": ["some", "words"],
                    "search_item": [],
                    "limit": "100",
                    "valid_from": datetime(2021, 1, 1, tzinfo=timezone.utc).isoformat(),
                    "valid_to": datetime(2022, 1, 1, tzinfo=timezone.utc).isoformat(),
                },
            )
