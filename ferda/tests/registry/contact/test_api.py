from regal import Contact

from ferda.constants import LogEntryType, LogReferenceType
from ferda.registry.constants import ObjectType
from ferda.tests.registry.common import (
    GetObjectHistoryInfoDatetimeTest,
    GetObjectHistoryInfoLastTest,
    GetObjectHistoryInfoTest,
    GetObjectHistoryTest,
    GetObjectInfoTest,
    GetObjectStateHistoryTest,
    GetObjectStateTest,
    RecordStatementTest,
)


class ContactRecordStatementTest(RecordStatementTest.Base):
    """Specialized test suite for contact record statement."""

    object_id = "contact_id"
    log_reference_type = LogReferenceType.CONTACT
    statementor_mock = "ferda.registry.contact.api.STATEMENTOR"
    statement_call = "get_contact_statement"
    reverse_url = "ferda:api:get_contact_record_statement_pdf"


class GetContactInfoTest(GetObjectInfoTest.Base):
    """Specialized test suite for getting contact info."""

    object_model = Contact
    object_type = ObjectType.CONTACT
    object_id = "contact_id"
    object_params = {"contact_handle": "handle:1"}
    log_entry_type = LogEntryType.CONTACT_DETAIL
    log_reference_type = LogReferenceType.CONTACT
    object_client_mock = "ferda.registry.contact.api.CONTACT"
    info_call = "get_contact_info"
    reverse_url = "ferda:api:get_contact_info"


class GetContactHistoryTest(GetObjectHistoryTest.Base):
    """Specialized test suite for getting contact history."""

    object_type = ObjectType.CONTACT
    object_id = "contact_id"
    object_history_id = "contact_history_id"
    log_entry_type = LogEntryType.CONTACT_HISTORY
    log_reference_type = LogReferenceType.CONTACT
    object_client_mock = "ferda.registry.contact.api.CONTACT"
    history_list_call = "get_contact_history"
    reverse_url = "ferda:api:get_contact_history"


class GetContactHistoryInfoTest(GetObjectHistoryInfoTest.Base):
    """Specialized test suite for getting contact history info (by identifier)."""

    object_model = Contact
    object_type = ObjectType.CONTACT
    object_id = "contact_id"
    object_history_id = "contact_history_id"
    object_params = {"contact_handle": "handle:1"}
    log_entry_type = LogEntryType.CONTACT_DETAIL
    log_reference_type = LogReferenceType.CONTACT
    object_client_mock = "ferda.registry.contact.api.CONTACT"
    info_call = "get_contact_info"
    reverse_url = "ferda:api:get_contact_history_info"


class GetContactHistoryInfoDatetimeTest(GetObjectHistoryInfoDatetimeTest.Base):
    """Specialized test suite for getting contact history info (by datetime)."""

    object_model = Contact
    object_type = ObjectType.CONTACT
    object_id = "contact_id"
    object_history_id = "contact_history_id"
    object_params = {"contact_handle": "handle:1"}
    log_entry_type = LogEntryType.CONTACT_DETAIL
    log_reference_type = LogReferenceType.CONTACT
    object_client_mock = "ferda.registry.contact.api.CONTACT"
    history_list_call = "get_contact_history"
    info_call = "get_contact_info"
    reverse_url = "ferda:api:get_contact_history_info_datetime"


class GetContactHistoryInfoLastTest(GetObjectHistoryInfoLastTest.Base):
    """Specialized test suite for getting contact history info (last known)."""

    object_model = Contact
    object_type = ObjectType.CONTACT
    object_id = "contact_id"
    object_history_id = "contact_history_id"
    object_params = {"contact_handle": "handle:1"}
    log_entry_type = LogEntryType.CONTACT_DETAIL
    log_reference_type = LogReferenceType.CONTACT
    object_client_mock = "ferda.registry.contact.api.CONTACT"
    history_list_call = "get_contact_history"
    info_call = "get_contact_info"
    reverse_url = "ferda:api:get_contact_history_info_last"


class GetContactStateTest(GetObjectStateTest.Base):
    """Specialized test suite for getting contact state."""

    object_type = ObjectType.CONTACT
    object_id = "contact_id"
    log_entry_type = LogEntryType.CONTACT_STATE
    log_reference_type = LogReferenceType.CONTACT
    object_client_mock = "ferda.registry.contact.api.CONTACT"
    state_call = "get_contact_state"
    reverse_url = "ferda:api:get_contact_state"


class GetContactStateHistoryTest(GetObjectStateHistoryTest.Base):
    """Specialized test suite for getting contact state history."""

    object_type = ObjectType.CONTACT
    object_id = "contact_id"
    log_entry_type = LogEntryType.CONTACT_STATE
    log_reference_type = LogReferenceType.CONTACT
    object_client_mock = "ferda.registry.contact.api.CONTACT"
    state_history_call = "get_contact_state_history"
    reverse_url = "ferda:api:get_contact_state_history"
