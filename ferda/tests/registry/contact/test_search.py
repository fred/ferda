from django.urls import reverse_lazy
from regal import Contact

from ferda.constants import LogEntryType
from ferda.tests.registry.search import SearchHistoryTest, SearchTest


class ContactSearchTest(SearchTest.Base):
    object_type = Contact
    object_id = "contact_id"
    object_handle = "contact_handle"
    log_entry_type = LogEntryType.CONTACT_SEARCH
    object_search_mock = "ferda.registry.contact.search.CONTACT_SEARCH"
    object_mock = "ferda.registry.contact.search.CONTACT"
    search_call = "search_contact"
    info_call = "get_contact_info"
    url = reverse_lazy("ferda:api:search_contact")


class ContactHistorySearchTest(SearchHistoryTest.Base):
    object_type = Contact
    object_id = "contact_id"
    object_history_id = "contact_history_id"
    object_handle = "contact_handle"
    log_entry_type = LogEntryType.CONTACT_SEARCH_HISTORY
    object_search_mock = "ferda.registry.contact.search.CONTACT_SEARCH"
    object_mock = "ferda.registry.contact.search.CONTACT"
    search_call = "search_contact_history"
    info_call = "get_contact_info"
    history_call = "get_contact_history"
    url = reverse_lazy("ferda:api:search_contact_history")
