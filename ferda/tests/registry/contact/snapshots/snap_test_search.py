# snapshottest: v1 - https://goo.gl/zC4yUc

from snapshottest import Snapshot


snapshots = Snapshot()

snapshots["ContactHistorySearchTest::test_search_history 1"] = {
    "estimated_total": {"lower_estimate": 1, "upper_estimate": 1},
    "results": [
        {
            "histories": [
                {
                    "contact": {
                        "additional_identifier": None,
                        "auth_info": None,
                        "billing_address": None,
                        "contact_handle": "OBJECT-1",
                        "contact_history_id": "o:2",
                        "contact_id": "o:1",
                        "emails": [],
                        "events": None,
                        "fax": None,
                        "mailing_address": None,
                        "name": "",
                        "notify_emails": [],
                        "organization": "",
                        "place": None,
                        "publish": {},
                        "shipping_address1": None,
                        "shipping_address2": None,
                        "shipping_address3": None,
                        "sponsoring_registrar": None,
                        "telephone": None,
                        "vat_identification_number": None,
                        "warning_letter": 2,
                    },
                    "history_ids": ["o:2", "o:3"],
                    "matched_items": ["handle"],
                    "valid_from": "2021-01-01T00:00:00Z",
                    "valid_to": "2021-04-01T00:00:00Z",
                },
                {
                    "contact": {
                        "additional_identifier": None,
                        "auth_info": None,
                        "billing_address": None,
                        "contact_handle": "OBJECT-1",
                        "contact_history_id": "o:5",
                        "contact_id": "o:1",
                        "emails": [],
                        "events": None,
                        "fax": None,
                        "mailing_address": None,
                        "name": "",
                        "notify_emails": [],
                        "organization": "",
                        "place": None,
                        "publish": {},
                        "shipping_address1": None,
                        "shipping_address2": None,
                        "shipping_address3": None,
                        "sponsoring_registrar": None,
                        "telephone": None,
                        "vat_identification_number": None,
                        "warning_letter": 2,
                    },
                    "history_ids": ["o:5", "o:6"],
                    "matched_items": ["handle"],
                    "valid_from": "2021-08-01T00:00:00Z",
                    "valid_to": "2021-12-01T00:00:00Z",
                },
            ],
            "last_history": {
                "additional_identifier": None,
                "auth_info": None,
                "billing_address": None,
                "contact_handle": "OBJECT-1",
                "contact_history_id": "o:7",
                "contact_id": "o:1",
                "emails": [],
                "events": None,
                "fax": None,
                "mailing_address": None,
                "name": "",
                "notify_emails": [],
                "organization": "",
                "place": None,
                "publish": {},
                "shipping_address1": None,
                "shipping_address2": None,
                "shipping_address3": None,
                "sponsoring_registrar": None,
                "telephone": None,
                "vat_identification_number": None,
                "warning_letter": 2,
            },
            "object_id": "o:1",
        }
    ],
    "searched_items": ["handle"],
}

snapshots["ContactHistorySearchTest::test_search_history_empty 1"] = {
    "estimated_total": None,
    "results": [],
    "searched_items": [],
}

snapshots["ContactSearchTest::test_search 1"] = {
    "estimated_total": {"lower_estimate": 1, "upper_estimate": 1},
    "results": [
        {
            "contact": {
                "additional_identifier": None,
                "auth_info": None,
                "billing_address": None,
                "contact_handle": "OBJECT-1",
                "contact_history_id": None,
                "contact_id": "o:1",
                "emails": [],
                "events": None,
                "fax": None,
                "mailing_address": None,
                "name": "",
                "notify_emails": [],
                "organization": "",
                "place": None,
                "publish": {},
                "shipping_address1": None,
                "shipping_address2": None,
                "shipping_address3": None,
                "sponsoring_registrar": None,
                "telephone": None,
                "vat_identification_number": None,
                "warning_letter": 2,
            },
            "matched_items": ["handle"],
            "object_id": "o:1",
        }
    ],
    "searched_items": ["handle"],
}

snapshots["ContactSearchTest::test_search_empty 1"] = {"estimated_total": None, "results": [], "searched_items": []}
