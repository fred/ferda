# snapshottest: v1 - https://goo.gl/zC4yUc

from snapshottest import Snapshot


snapshots = Snapshot()

snapshots["KeysetHistorySearchTest::test_search_history 1"] = {
    "estimated_total": {"lower_estimate": 1, "upper_estimate": 1},
    "results": [
        {
            "histories": [
                {
                    "history_ids": ["o:2", "o:3"],
                    "keyset": {
                        "auth_info": None,
                        "dns_keys": [],
                        "events": None,
                        "keyset_handle": "OBJECT-1",
                        "keyset_history_id": "o:2",
                        "keyset_id": "o:1",
                        "sponsoring_registrar": None,
                        "technical_contacts": [],
                    },
                    "matched_items": ["handle"],
                    "valid_from": "2021-01-01T00:00:00Z",
                    "valid_to": "2021-04-01T00:00:00Z",
                },
                {
                    "history_ids": ["o:5", "o:6"],
                    "keyset": {
                        "auth_info": None,
                        "dns_keys": [],
                        "events": None,
                        "keyset_handle": "OBJECT-1",
                        "keyset_history_id": "o:5",
                        "keyset_id": "o:1",
                        "sponsoring_registrar": None,
                        "technical_contacts": [],
                    },
                    "matched_items": ["handle"],
                    "valid_from": "2021-08-01T00:00:00Z",
                    "valid_to": "2021-12-01T00:00:00Z",
                },
            ],
            "last_history": {
                "auth_info": None,
                "dns_keys": [],
                "events": None,
                "keyset_handle": "OBJECT-1",
                "keyset_history_id": "o:7",
                "keyset_id": "o:1",
                "sponsoring_registrar": None,
                "technical_contacts": [],
            },
            "object_id": "o:1",
        }
    ],
    "searched_items": ["handle"],
}

snapshots["KeysetHistorySearchTest::test_search_history_empty 1"] = {
    "estimated_total": None,
    "results": [],
    "searched_items": [],
}

snapshots["KeysetSearchTest::test_search 1"] = {
    "estimated_total": {"lower_estimate": 1, "upper_estimate": 1},
    "results": [
        {
            "keyset": {
                "auth_info": None,
                "dns_keys": [],
                "events": None,
                "keyset_handle": "OBJECT-1",
                "keyset_history_id": None,
                "keyset_id": "o:1",
                "sponsoring_registrar": None,
                "technical_contacts": [],
            },
            "matched_items": ["handle"],
            "object_id": "o:1",
        }
    ],
    "searched_items": ["handle"],
}

snapshots["KeysetSearchTest::test_search_empty 1"] = {"estimated_total": None, "results": [], "searched_items": []}
