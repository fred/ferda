from django.urls import reverse_lazy
from regal import Keyset

from ferda.constants import LogEntryType
from ferda.tests.registry.search import SearchHistoryTest, SearchTest


class KeysetSearchTest(SearchTest.Base):
    object_type = Keyset
    object_id = "keyset_id"
    object_handle = "keyset_handle"
    log_entry_type = LogEntryType.KEYSET_SEARCH
    object_search_mock = "ferda.registry.keyset.search.KEYSET_SEARCH"
    object_mock = "ferda.registry.keyset.search.KEYSET"
    search_call = "search_keyset"
    info_call = "get_keyset_info"
    url = reverse_lazy("ferda:api:search_keyset")


class KeysetHistorySearchTest(SearchHistoryTest.Base):
    object_type = Keyset
    object_id = "keyset_id"
    object_history_id = "keyset_history_id"
    object_handle = "keyset_handle"
    log_entry_type = LogEntryType.KEYSET_SEARCH_HISTORY
    object_search_mock = "ferda.registry.keyset.search.KEYSET_SEARCH"
    object_mock = "ferda.registry.keyset.search.KEYSET"
    search_call = "search_keyset_history"
    info_call = "get_keyset_info"
    history_call = "get_keyset_history"
    url = reverse_lazy("ferda:api:search_keyset_history")
