from regal import Keyset

from ferda.constants import LogEntryType, LogReferenceType
from ferda.registry.constants import ObjectType
from ferda.tests.registry.common import (
    GetObjectHistoryInfoDatetimeTest,
    GetObjectHistoryInfoLastTest,
    GetObjectHistoryInfoTest,
    GetObjectHistoryTest,
    GetObjectInfoTest,
    GetObjectStateHistoryTest,
    GetObjectStateTest,
    RecordStatementTest,
)


class KeysetRecordStatementTest(RecordStatementTest.Base):
    """Specialized test suite for keyset record statement."""

    object_id = "keyset_id"
    log_reference_type = LogReferenceType.KEYSET
    statementor_mock = "ferda.registry.keyset.api.STATEMENTOR"
    statement_call = "get_keyset_statement"
    reverse_url = "ferda:api:get_keyset_record_statement_pdf"


class GetKeysetInfoTest(GetObjectInfoTest.Base):
    """Specialized test suite for getting keyset info."""

    object_model = Keyset
    object_type = ObjectType.KEYSET
    object_id = "keyset_id"
    object_params = {"keyset_handle": "handle:1"}
    log_entry_type = LogEntryType.KEYSET_DETAIL
    log_reference_type = LogReferenceType.KEYSET
    object_client_mock = "ferda.registry.keyset.api.KEYSET"
    info_call = "get_keyset_info"
    reverse_url = "ferda:api:get_keyset_info"


class GetKeysetHistoryTest(GetObjectHistoryTest.Base):
    """Specialized test suite for getting keyset history."""

    object_type = ObjectType.KEYSET
    object_id = "keyset_id"
    object_history_id = "keyset_history_id"
    log_entry_type = LogEntryType.KEYSET_HISTORY
    log_reference_type = LogReferenceType.KEYSET
    object_client_mock = "ferda.registry.keyset.api.KEYSET"
    history_list_call = "get_keyset_history"
    reverse_url = "ferda:api:get_keyset_history"


class GetKeysetHistoryInfoTest(GetObjectHistoryInfoTest.Base):
    """Specialized test suite for getting keyset history info (by identifier)."""

    object_model = Keyset
    object_type = ObjectType.KEYSET
    object_id = "keyset_id"
    object_history_id = "keyset_history_id"
    object_params = {"keyset_handle": "handle:1"}
    log_entry_type = LogEntryType.KEYSET_DETAIL
    log_reference_type = LogReferenceType.KEYSET
    object_client_mock = "ferda.registry.keyset.api.KEYSET"
    info_call = "get_keyset_info"
    reverse_url = "ferda:api:get_keyset_history_info"


class GetKeysetHistoryInfoDatetimeTest(GetObjectHistoryInfoDatetimeTest.Base):
    """Specialized test suite for getting keyset history info (by datetime)."""

    object_model = Keyset
    object_type = ObjectType.KEYSET
    object_id = "keyset_id"
    object_history_id = "keyset_history_id"
    object_params = {"keyset_handle": "handle:1"}
    log_entry_type = LogEntryType.KEYSET_DETAIL
    log_reference_type = LogReferenceType.KEYSET
    object_client_mock = "ferda.registry.keyset.api.KEYSET"
    history_list_call = "get_keyset_history"
    info_call = "get_keyset_info"
    reverse_url = "ferda:api:get_keyset_history_info_datetime"


class GetKeysetHistoryInfoLastTest(GetObjectHistoryInfoLastTest.Base):
    """Specialized test suite for getting keyset history info (last known)."""

    object_model = Keyset
    object_type = ObjectType.KEYSET
    object_id = "keyset_id"
    object_history_id = "keyset_history_id"
    object_params = {"keyset_handle": "handle:1"}
    log_entry_type = LogEntryType.KEYSET_DETAIL
    log_reference_type = LogReferenceType.KEYSET
    object_client_mock = "ferda.registry.keyset.api.KEYSET"
    history_list_call = "get_keyset_history"
    info_call = "get_keyset_info"
    reverse_url = "ferda:api:get_keyset_history_info_last"


class GetKeysetStateTest(GetObjectStateTest.Base):
    """Specialized test suite for getting keyset state."""

    object_type = ObjectType.KEYSET
    object_id = "keyset_id"
    log_entry_type = LogEntryType.KEYSET_STATE
    log_reference_type = LogReferenceType.KEYSET
    object_client_mock = "ferda.registry.keyset.api.KEYSET"
    state_call = "get_keyset_state"
    reverse_url = "ferda:api:get_keyset_state"


class GetKeysetStateHistoryTest(GetObjectStateHistoryTest.Base):
    """Specialized test suite for getting keyset state history."""

    object_type = ObjectType.KEYSET
    object_id = "keyset_id"
    log_entry_type = LogEntryType.KEYSET_STATE
    log_reference_type = LogReferenceType.KEYSET
    object_client_mock = "ferda.registry.keyset.api.KEYSET"
    state_history_call = "get_keyset_state_history"
    reverse_url = "ferda:api:get_keyset_state_history"
