# snapshottest: v1 - https://goo.gl/zC4yUc

from snapshottest import Snapshot


snapshots = Snapshot()

snapshots["DomainHistorySearchTest::test_search_history 1"] = {
    "estimated_total": {"lower_estimate": 1, "upper_estimate": 1},
    "results": [
        {
            "histories": [
                {
                    "domain": {
                        "administrative_contacts": [],
                        "auth_info": None,
                        "delete_candidate_at": None,
                        "delete_candidate_scheduled_at": None,
                        "delete_warning_scheduled_at": None,
                        "domain_history_id": "o:2",
                        "domain_id": "o:1",
                        "events": None,
                        "expiration_warning_scheduled_at": None,
                        "expires_at": None,
                        "fqdn": "OBJECT-1",
                        "keyset": None,
                        "nsset": None,
                        "outzone_at": None,
                        "outzone_scheduled_at": None,
                        "outzone_unguarded_warning_scheduled_at": None,
                        "registrant": None,
                        "sponsoring_registrar": None,
                        "validation_expires_at": None,
                    },
                    "history_ids": ["o:2", "o:3"],
                    "matched_items": ["handle"],
                    "valid_from": "2021-01-01T00:00:00Z",
                    "valid_to": "2021-04-01T00:00:00Z",
                },
                {
                    "domain": {
                        "administrative_contacts": [],
                        "auth_info": None,
                        "delete_candidate_at": None,
                        "delete_candidate_scheduled_at": None,
                        "delete_warning_scheduled_at": None,
                        "domain_history_id": "o:5",
                        "domain_id": "o:1",
                        "events": None,
                        "expiration_warning_scheduled_at": None,
                        "expires_at": None,
                        "fqdn": "OBJECT-1",
                        "keyset": None,
                        "nsset": None,
                        "outzone_at": None,
                        "outzone_scheduled_at": None,
                        "outzone_unguarded_warning_scheduled_at": None,
                        "registrant": None,
                        "sponsoring_registrar": None,
                        "validation_expires_at": None,
                    },
                    "history_ids": ["o:5", "o:6"],
                    "matched_items": ["handle"],
                    "valid_from": "2021-08-01T00:00:00Z",
                    "valid_to": "2021-12-01T00:00:00Z",
                },
            ],
            "last_history": {
                "administrative_contacts": [],
                "auth_info": None,
                "delete_candidate_at": None,
                "delete_candidate_scheduled_at": None,
                "delete_warning_scheduled_at": None,
                "domain_history_id": "o:7",
                "domain_id": "o:1",
                "events": None,
                "expiration_warning_scheduled_at": None,
                "expires_at": None,
                "fqdn": "OBJECT-1",
                "keyset": None,
                "nsset": None,
                "outzone_at": None,
                "outzone_scheduled_at": None,
                "outzone_unguarded_warning_scheduled_at": None,
                "registrant": None,
                "sponsoring_registrar": None,
                "validation_expires_at": None,
            },
            "object_id": "o:1",
        }
    ],
    "searched_items": ["handle"],
}

snapshots["DomainHistorySearchTest::test_search_history_empty 1"] = {
    "estimated_total": None,
    "results": [],
    "searched_items": [],
}

snapshots["DomainSearchTest::test_search 1"] = {
    "estimated_total": {"lower_estimate": 1, "upper_estimate": 1},
    "results": [
        {
            "domain": {
                "administrative_contacts": [],
                "auth_info": None,
                "delete_candidate_at": None,
                "delete_candidate_scheduled_at": None,
                "delete_warning_scheduled_at": None,
                "domain_history_id": None,
                "domain_id": "o:1",
                "events": None,
                "expiration_warning_scheduled_at": None,
                "expires_at": None,
                "fqdn": "OBJECT-1",
                "keyset": None,
                "nsset": None,
                "outzone_at": None,
                "outzone_scheduled_at": None,
                "outzone_unguarded_warning_scheduled_at": None,
                "registrant": None,
                "sponsoring_registrar": None,
                "validation_expires_at": None,
            },
            "matched_items": ["handle"],
            "object_id": "o:1",
        }
    ],
    "searched_items": ["handle"],
}

snapshots["DomainSearchTest::test_search_empty 1"] = {"estimated_total": None, "results": [], "searched_items": []}
