import json
from datetime import datetime
from typing import List, Optional
from unittest.mock import ANY, Mock, call, patch
from urllib.parse import urlencode

from aioitertools import iter as aiter
from django.contrib.auth import get_user_model
from django.http import HttpResponse
from django.test import TestCase, override_settings, tag
from django.urls import reverse
from fred_types import ContactId, DomainHistoryId, DomainId
from freezegun import freeze_time
from frgal.utils import make_awaitable
from grill import LogProperty
from pytz import utc
from regal import Domain
from regal.common import DatetimeRange
from regal.domain import ContactInfo, ContactInfoType, DomainContactRole, DomainRef
from regal.exceptions import ContactDoesNotExist, DomainDoesNotExist

from ferda.constants import LogEntryType, LoggerResult, LogReferenceType, StateFlag
from ferda.registry.constants import ObjectType
from ferda.tests.registry.common import (
    GetObjectHistoryInfoDatetimeTest,
    GetObjectHistoryInfoLastTest,
    GetObjectHistoryInfoTest,
    GetObjectHistoryTest,
    GetObjectInfoTest,
    GetObjectStateHistoryTest,
    GetObjectStateTest,
    RecordStatementTest,
)
from ferda.tests.utils import LoggerMixin

User = get_user_model()


@tag("api", "registry")
@patch("ferda.registry.domain.api.DOMAIN")
@override_settings(ROOT_URLCONF="ferda.tests.urls")
class GetDomainIdByFqdnTest(LoggerMixin, TestCase):
    def setUp(self):
        super().setUp()
        user = User.objects.create_superuser("admin")  # type: ignore
        self.client.force_login(user)

    def test_get(self, client):
        client.get_domain_id.return_value = make_awaitable(DomainId("domain.1"))

        url = reverse("ferda:api:get_domain_id_by_fqdn", kwargs={"fqdn": "example.org"})
        response = self.client.get(url)

        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.json(), {"fqdn": "example.org", "domain_id": "domain.1"})
        self.assertEqual(client.mock_calls, [call.get_domain_id("example.org")])

    def test_get_does_not_exist(self, client):
        client.get_domain_id.side_effect = DomainDoesNotExist

        url = reverse("ferda:api:get_domain_id_by_fqdn", kwargs={"fqdn": "example.org"})
        response = self.client.get(url)

        self.assertEqual(response.status_code, 404)
        self.assertEqual(response.json(), {"detail": "Domain 'example.org' does not exist"})
        self.assertEqual(client.mock_calls, [call.get_domain_id("example.org")])


@tag("api", "registry")
@patch("ferda.registry.domain.api.DOMAIN")
@override_settings(ROOT_URLCONF="ferda.tests.urls")
class GetDomainsByContactTest(LoggerMixin, TestCase):
    def setUp(self):
        super().setUp()
        user = User.objects.create_superuser("admin")  # type: ignore
        self.client.force_login(user)

    def _test_get_domains_by_contact(
        self,
        client: Mock,
        contact_id: str,
        roles: List[DomainContactRole],
        params: Optional[dict] = None,
        log_result: LoggerResult = LoggerResult.SUCCESS,
    ) -> HttpResponse:
        params = params or {}
        url = reverse("ferda:api:get_domains_by_contact", kwargs={"contact_id": contact_id})
        url += "?" + urlencode({**params, **{"roles": [role.value for role in roles]}}, doseq=True)
        response = self.client.get(url)

        self.assertEqual(
            client.mock_calls[0],
            call.get_domains_by_contact(
                "contact.1",
                set(roles),
                **{
                    "order_by": None,
                    "fqdn_filter": None,
                    "page_token": None,
                    "page_size": None,
                    "include_deleted": False,
                    **params,
                },
            ),
        )
        self.assertLog(
            LogEntryType.DOMAIN_LIST,
            log_result,
            input_properties={
                "roles": roles,
                "include_deleted": "False",
                "order_by": "",
                "fqdn_filter": "",
                "page_token": "",
                "page_size": "",
                **{k: str(v) for k, v in params.items()},
            },
            input_references={LogReferenceType.CONTACT: contact_id},
        )
        return response

    def test_get_empty(self, client):
        client.get_domains_by_contact.return_value = make_awaitable({"domains": []})

        response = self._test_get_domains_by_contact(client, "contact.1", [DomainContactRole.HOLDER])

        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.json(), {"results": [], "pagination": None})
        self.assertEqual(client.mock_calls[1:], [])

    def test_get_full(self, client):
        pagination = {
            "next_page_token": "",
            "items_left": 0,
        }
        domain = {
            "domain": DomainRef(id=DomainId("domain.1"), fqdn="example.org"),
            "domain_history_id": DomainHistoryId("domain.history.1"),
            "roles": {DomainContactRole.HOLDER, DomainContactRole.NSSET_TECH},
            "is_deleted": False,
        }
        reply = {
            "domains": [domain.copy()],
            "pagination": pagination.copy(),
        }
        client.get_domains_by_contact.return_value = make_awaitable(reply)
        client.get_domain_info.return_value = make_awaitable(
            Domain(
                domain_id="domain.1",
                domain_history_id="domain.history.1",
                fqdn="example.org",  # type: ignore[arg-type]
                registrant={"id": "contact.1", "handle": "CONTACT"},  # type: ignore[arg-type]
            )
        )

        params = {"order_by": "+fqdn", "fqdn_filter": "*.cz", "page_token": "token.1", "page_size": 10}
        response = self._test_get_domains_by_contact(client, "contact.1", list(DomainContactRole), params)

        self.assertEqual(response.status_code, 200)
        self.assertEqual(
            response.json(),
            {
                "results": [
                    {
                        "domain": Domain(
                            domain_id="domain.1",
                            domain_history_id="domain.history.1",  # type: ignore[arg-type]
                            fqdn="example.org",
                            registrant={"id": "contact.1", "handle": "CONTACT"},  # type: ignore[arg-type]
                        ).model_dump(),
                        "roles": ANY,
                    }
                ],
                "pagination": pagination,
            },
        )
        self.assertEqual(
            set(response.json()["results"][0]["roles"]),
            {DomainContactRole.HOLDER, DomainContactRole.NSSET_TECH},
        )
        self.assertEqual(client.mock_calls[1:], [call.get_domain_info("domain.1", "domain.history.1")])

    def test_get_full_with_keyset_nsset(self, domain_client):
        pagination = {
            "next_page_token": "",
            "items_left": 0,
        }
        domain = {
            "domain": DomainRef(id=DomainId("domain.1"), fqdn="example.org"),
            "domain_history_id": DomainHistoryId("domain.history.1"),
            "roles": {DomainContactRole.HOLDER, DomainContactRole.NSSET_TECH},
            "is_deleted": False,
        }
        reply = {
            "domains": [domain.copy()],
            "pagination": pagination.copy(),
        }
        domain_client.get_domains_by_contact.return_value = make_awaitable(reply)
        domain_client.get_domain_info.return_value = make_awaitable(
            Domain(
                domain_id="domain.1",
                domain_history_id="domain.history.1",
                fqdn="example.org",  # type: ignore[arg-type]
                registrant={"id": "contact.1", "handle": "CONTACT"},  # type: ignore[arg-type]
                keyset={"id": "keyset.1", "handle": "KEYSET"},  # type: ignore[arg-type]
                nsset={"id": "nsset.1", "handle": "NSSET"},  # type: ignore[arg-type]
            )
        )

        params = {"order_by": "+fqdn", "fqdn_filter": "*.cz", "page_token": "token.1", "page_size": 10}
        response = self._test_get_domains_by_contact(domain_client, "contact.1", list(DomainContactRole), params)

        self.assertEqual(response.status_code, 200)
        self.assertEqual(
            response.json(),
            {
                "results": [
                    {
                        "domain": Domain(
                            domain_id="domain.1",
                            domain_history_id="domain.history.1",  # type: ignore[arg-type]
                            fqdn="example.org",
                            registrant={"id": "contact.1", "handle": "CONTACT"},  # type: ignore[arg-type]
                            keyset={"id": "keyset.1", "handle": "KEYSET"},  # type: ignore[arg-type]
                            nsset={"id": "nsset.1", "handle": "NSSET"},  # type: ignore[arg-type]
                        ).model_dump(),
                        "roles": ANY,
                    }
                ],
                "pagination": pagination,
            },
        )
        self.assertEqual(
            set(response.json()["results"][0]["roles"]),
            {DomainContactRole.HOLDER, DomainContactRole.NSSET_TECH},
        )
        self.assertEqual(domain_client.mock_calls[1:], [call.get_domain_info("domain.1", "domain.history.1")])

    def test_get_deleted_full_with_deleted_keyset_nsset(self, domain_client):
        deleted_at = datetime(2022, 1, 1, tzinfo=utc)
        pagination = {
            "next_page_token": "",
            "items_left": 0,
        }
        domain = {
            "domain": DomainRef(id=DomainId("domain.1"), fqdn="example.org"),
            "domain_history_id": DomainHistoryId("domain.history.1"),
            "roles": {DomainContactRole.HOLDER, DomainContactRole.NSSET_TECH},
            "is_deleted": True,
        }
        reply = {
            "domains": [domain.copy()],
            "pagination": pagination.copy(),
        }
        domain_client.get_domains_by_contact.return_value = make_awaitable(reply)
        domain_client.get_domain_info.return_value = make_awaitable(
            Domain(
                domain_id="domain.1",
                domain_history_id="domain.history.1",
                fqdn="example.org",  # type: ignore[arg-type]
                registrant={"id": "contact.1", "handle": "CONTACT"},  # type: ignore[arg-type]
                keyset={"id": "keyset.1", "handle": "KEYSET"},  # type: ignore[arg-type]
                nsset={"id": "nsset.1", "handle": "NSSET"},  # type: ignore[arg-type]
                events={  # type: ignore[arg-type]
                    "registered": {"timestamp": deleted_at, "registrar_handle": "REG"},
                    "transferred": {"timestamp": deleted_at, "registrar_handle": "REG"},
                    "unregistered": {"timestamp": deleted_at, "registrar_handle": "REG"},
                },
            )
        )

        params = {
            "order_by": "+fqdn",
            "fqdn_filter": "*.cz",
            "page_token": "token.1",
            "page_size": 10,
            "include_deleted": True,
        }
        response = self._test_get_domains_by_contact(domain_client, "contact.1", list(DomainContactRole), params)

        self.assertEqual(response.status_code, 200)
        self.assertEqual(
            response.json(),
            {
                "results": [
                    {
                        "domain": ANY,
                        "roles": ANY,
                    }
                ],
                "pagination": pagination,
            },
        )
        self.assertEqual(
            Domain(**response.json()["results"][0]["domain"]),
            Domain(
                domain_id="domain.1",
                domain_history_id="domain.history.1",
                fqdn="example.org",  # type: ignore[arg-type]
                registrant={"id": "contact.1", "handle": "CONTACT"},  # type: ignore[arg-type]
                keyset={"id": "keyset.1", "handle": "KEYSET"},  # type: ignore[arg-type]
                nsset={"id": "nsset.1", "handle": "NSSET"},  # type: ignore[arg-type]
                events={  # type: ignore[arg-type]
                    "registered": {"timestamp": deleted_at, "registrar_handle": "REG"},
                    "transferred": {"timestamp": deleted_at, "registrar_handle": "REG"},
                    "unregistered": {"timestamp": deleted_at, "registrar_handle": "REG"},
                },
            ),
        )
        self.assertEqual(
            set(response.json()["results"][0]["roles"]),
            {DomainContactRole.HOLDER, DomainContactRole.NSSET_TECH},
        )
        self.assertEqual(domain_client.mock_calls[1:], [call.get_domain_info("domain.1", "domain.history.1")])

    def test_get_contact_does_not_exist(self, client):
        client.get_domains_by_contact.side_effect = ContactDoesNotExist

        response = self._test_get_domains_by_contact(
            client,
            "contact.1",
            [DomainContactRole.HOLDER],
            log_result=LoggerResult.FAIL,
        )

        self.assertEqual(response.status_code, 404)
        self.assertEqual(
            response.json(),
            {"detail": "Contact 'contact.1' does not exist"},
        )
        self.assertEqual(client.mock_calls[1:], [])

    def test_get_contact_invalid_value(self, client):
        client.get_domains_by_contact.side_effect = ValueError

        response = self._test_get_domains_by_contact(
            client,
            "contact.1",
            [DomainContactRole.HOLDER],
            {"order_by": "xxx"},
            log_result=LoggerResult.FAIL,
        )

        self.assertEqual(response.status_code, 400)
        self.assertEqual(
            response.json(),
            {"detail": "invalid value"},
        )
        self.assertEqual(client.mock_calls[1:], [])


@tag("api", "registry")
@freeze_time("2022-01-01T00:00:00Z")
@patch("ferda.registry.domain.api.DOMAIN_ADMIN")
@override_settings(ROOT_URLCONF="ferda.tests.urls", FERDA_MANUAL_IN_ZONE_DEFAULT_DURATION=3 * 24 * 60 * 60)
class SetDomainManualInZoneTest(LoggerMixin, TestCase):
    def setUp(self):
        super().setUp()
        user = User.objects.create_superuser("admin")  # type: ignore
        self.client.force_login(user)

    def _test_set_domain_manual_in_zone(
        self,
        domain_admin: Mock,
        *,
        start: Optional[datetime] = None,
        end: Optional[datetime] = None,
        log_result: LoggerResult = LoggerResult.SUCCESS,
    ) -> HttpResponse:
        url = reverse("ferda:api:set_domain_manual_in_zone", kwargs={"domain_id": "domain.1"})
        params = {}
        if start is not None:
            params["start"] = start.isoformat()
        if end is not None:
            params["end"] = end.isoformat()
        url = "{}?{}".format(url, urlencode(params))
        response = self.client.post(url)
        self.assertEqual(
            domain_admin.mock_calls,
            [
                call.manage_domain_state_flags(
                    domains=[Domain(domain_id="domain.1", fqdn="")],  # type: ignore[arg-type]
                    state_flags={
                        StateFlag.SERVER_INZONE_MANUAL: DatetimeRange(
                            start=start or datetime(2022, 1, 1, tzinfo=utc),
                            end=end or datetime(2022, 1, 4, tzinfo=utc),
                        )
                    },
                ),
            ],
        )
        self.assertLog(
            LogEntryType.DOMAIN_STATE_CHANGE,
            log_result,
            input_properties={
                StateFlag.SERVER_INZONE_MANUAL: LogProperty(
                    "",
                    children={
                        "start": (start or datetime(2022, 1, 1, tzinfo=utc)).isoformat(),
                        "end": (end or datetime(2022, 1, 4, tzinfo=utc)).isoformat(),
                    },
                )
            },
            input_references={LogReferenceType.DOMAIN: "domain.1"},
        )
        return response

    def test_set_domain_manual_in_zone(self, domain_admin):
        domain_admin.manage_domain_state_flags.return_value = aiter([])
        response = self._test_set_domain_manual_in_zone(domain_admin)
        self.assertEqual(response.status_code, 200)

    def test_set_domain_manual_in_zone_datetime(self, domain_admin):
        domain_admin.manage_domain_state_flags.return_value = aiter([])
        response = self._test_set_domain_manual_in_zone(
            domain_admin,
            start=datetime(2022, 2, 2, tzinfo=utc),
            end=datetime(2022, 2, 4, tzinfo=utc),
        )
        self.assertEqual(response.status_code, 200)

    def test_set_domain_manual_in_zone_not_found(self, domain_admin):
        domain_admin.manage_domain_state_flags.side_effect = DomainDoesNotExist
        response = self._test_set_domain_manual_in_zone(domain_admin, log_result=LoggerResult.FAIL)
        self.assertEqual(response.status_code, 404)
        self.assertEqual(response.json(), {"detail": "Domain 'domain.1' does not exist"})


class DomainRecordStatementTest(RecordStatementTest.Base):
    """Specialized test suite for domain record statement."""

    object_id = "domain_id"
    log_reference_type = LogReferenceType.DOMAIN
    statementor_mock = "ferda.registry.domain.api.STATEMENTOR"
    statement_call = "get_domain_statement"
    reverse_url = "ferda:api:get_domain_record_statement_pdf"


class GetDomainInfoTest(GetObjectInfoTest.Base):
    """Specialized test suite for getting domain info."""

    object_model = Domain
    object_type = ObjectType.DOMAIN
    object_id = "domain_id"
    object_params = {"domain_handle": "handle:1", "fqdn": "example.org"}
    log_entry_type = LogEntryType.DOMAIN_DETAIL
    log_reference_type = LogReferenceType.DOMAIN
    object_client_mock = "ferda.registry.domain.api.DOMAIN"
    info_call = "get_domain_info"
    reverse_url = "ferda:api:get_domain_info"


class GetDomainHistoryTest(GetObjectHistoryTest.Base):
    """Specialized test suite for getting domain history."""

    object_type = ObjectType.DOMAIN
    object_id = "domain_id"
    object_history_id = "domain_history_id"
    log_entry_type = LogEntryType.DOMAIN_HISTORY
    log_reference_type = LogReferenceType.DOMAIN
    object_client_mock = "ferda.registry.domain.api.DOMAIN"
    history_list_call = "get_domain_history"
    reverse_url = "ferda:api:get_domain_history"


class GetDomainHistoryInfoTest(GetObjectHistoryInfoTest.Base):
    """Specialized test suite for getting domain history info (by identifier)."""

    object_model = Domain
    object_type = ObjectType.DOMAIN
    object_id = "domain_id"
    object_history_id = "domain_history_id"
    object_params = {"domain_handle": "handle:1", "fqdn": "example.org"}
    log_entry_type = LogEntryType.DOMAIN_DETAIL
    log_reference_type = LogReferenceType.DOMAIN
    object_client_mock = "ferda.registry.domain.api.DOMAIN"
    info_call = "get_domain_info"
    reverse_url = "ferda:api:get_domain_history_info"


class GetDomainHistoryInfoDatetimeTest(GetObjectHistoryInfoDatetimeTest.Base):
    """Specialized test suite for getting domain history info (by datetime)."""

    object_model = Domain
    object_type = ObjectType.DOMAIN
    object_id = "domain_id"
    object_history_id = "domain_history_id"
    object_params = {"domain_handle": "handle:1", "fqdn": "example.org"}
    log_entry_type = LogEntryType.DOMAIN_DETAIL
    log_reference_type = LogReferenceType.DOMAIN
    object_client_mock = "ferda.registry.domain.api.DOMAIN"
    history_list_call = "get_domain_history"
    info_call = "get_domain_info"
    reverse_url = "ferda:api:get_domain_history_info_datetime"


class GetDomainHistoryInfoLastTest(GetObjectHistoryInfoLastTest.Base):
    """Specialized test suite for getting domain history info (last known)."""

    object_model = Domain
    object_type = ObjectType.DOMAIN
    object_id = "domain_id"
    object_history_id = "domain_history_id"
    object_params = {"domain_handle": "handle:1", "fqdn": "example.org"}
    log_entry_type = LogEntryType.DOMAIN_DETAIL
    log_reference_type = LogReferenceType.DOMAIN
    object_client_mock = "ferda.registry.domain.api.DOMAIN"
    history_list_call = "get_domain_history"
    info_call = "get_domain_info"
    reverse_url = "ferda:api:get_domain_history_info_last"


class GetDomainStateTest(GetObjectStateTest.Base):
    """Specialized test suite for getting domain state."""

    object_type = ObjectType.DOMAIN
    object_id = "domain_id"
    log_entry_type = LogEntryType.DOMAIN_STATE
    log_reference_type = LogReferenceType.DOMAIN
    object_client_mock = "ferda.registry.domain.api.DOMAIN"
    state_call = "get_domain_state"
    reverse_url = "ferda:api:get_domain_state"


class GetDomainStateHistoryTest(GetObjectStateHistoryTest.Base):
    """Specialized test suite for getting domain state history."""

    object_type = ObjectType.DOMAIN
    object_id = "domain_id"
    log_entry_type = LogEntryType.DOMAIN_STATE
    log_reference_type = LogReferenceType.DOMAIN
    object_client_mock = "ferda.registry.domain.api.DOMAIN"
    state_history_call = "get_domain_state_history"
    reverse_url = "ferda:api:get_domain_state_history"


@tag("api", "registry")
@patch("ferda.registry.domain.api.DOMAIN_ADMIN")
@override_settings(ROOT_URLCONF="ferda.tests.urls")
class GetAdditionalContactsTest(LoggerMixin, TestCase):
    def setUp(self):
        super().setUp()
        user = User.objects.create_superuser("admin")  # type: ignore
        self.client.force_login(user)

    def _test_get(self, domain_admin: Mock, url: str, method: str, event: str) -> HttpResponse:
        getattr(domain_admin, method).return_value = make_awaitable(
            {
                "domains": [
                    {
                        "domain_id": DomainId("domain.1"),
                        "fqdn": "example.org",
                        "registrant_id": ContactId("contact.1"),
                        "registrant_handle": "RIMMER",
                        "registrant_name": "Arnold Rimmer",
                        "registrant_organization": "Jupiter mining company",
                        "contacts": [ContactInfo(type=ContactInfoType.EMAIL, value="rimmer@example.org")],
                        "additional_contacts": [ContactInfo(type=ContactInfoType.EMAIL, value="kryten@example.org")],
                    }
                ],
                "pagination": {"items_left": 0, "next_page_token": ""},
            }
        )

        start = datetime(2022, 1, 1, tzinfo=utc)
        end = datetime(2022, 1, 2, tzinfo=utc)

        response = self.client.get(
            "{}?{}".format(
                url,
                urlencode(
                    {
                        "start": start.isoformat(),
                        "end": end.isoformat(),
                        "page_size": 5,
                    }
                ),
            )
        )

        self.assertEqual(
            domain_admin.mock_calls,
            [
                getattr(call, method)(start, end, page_size=5, page_token=None),
            ],
        )
        self.assertLog(
            LogEntryType.DOMAIN_CONTACT_INFO,
            LoggerResult.SUCCESS,
            input_properties={
                "event": event,
                "start": start.isoformat(),
                "end": end.isoformat(),
                "page_token": "",
                "page_size": "5",
            },
        )
        self.assertEqual(response.status_code, 200)
        self.assertEqual(
            response.json(),
            {
                "results": [
                    {
                        "domain_id": "domain.1",
                        "fqdn": "example.org",
                        "registrant_id": "contact.1",
                        "registrant_handle": "RIMMER",
                        "registrant_name": "Arnold Rimmer",
                        "registrant_organization": "Jupiter mining company",
                        "contacts": [{"type": "email", "value": "rimmer@example.org"}],
                        "additional_contacts": [{"type": "email", "value": "kryten@example.org"}],
                    }
                ],
                "pagination": {"items_left": 0, "next_page_token": ""},
            },
        )
        return response

    def test_get_outzone(self, domain_admin):
        self._test_get(
            domain_admin,
            reverse("ferda:api:get_additional_outzone_contacts"),
            "get_domains_outzone_notify_info",
            event="outzone",
        )

    def test_get_delete(self, domain_admin):
        self._test_get(
            domain_admin,
            reverse("ferda:api:get_additional_delete_contacts"),
            "get_domains_delete_notify_info",
            event="delete",
        )


@tag("api", "registry")
@patch("ferda.registry.domain.api.DOMAIN_ADMIN")
@override_settings(ROOT_URLCONF="ferda.tests.urls")
class UpdateAdditionalContactsTest(LoggerMixin, TestCase):
    def setUp(self):
        super().setUp()
        user = User.objects.create_superuser("admin")  # type: ignore
        self.client.force_login(user)

    def _test_update(
        self,
        domain_admin: Mock,
        url: str,
        method: str,
        event: str,
        *,
        log_result: LoggerResult = LoggerResult.SUCCESS,
    ) -> HttpResponse:
        start = datetime(2022, 1, 1, tzinfo=utc)
        end = datetime(2022, 1, 2, tzinfo=utc)
        domains = {
            "domain.1": [
                ContactInfo(type=ContactInfoType.EMAIL, value="kryten@example.org"),
                ContactInfo(type=ContactInfoType.EMAIL, value="rimmer@example.org"),
            ],
            "domain.2": [
                ContactInfo(type=ContactInfoType.TELEPHONE, value="+420.000000"),
            ],
        }

        response = self.client.post(
            url,
            data={
                "domains": {
                    key: [{"type": val.type, "value": val.value} for val in value] for key, value in domains.items()
                },
                "start": start.isoformat(),
                "end": end.isoformat(),
            },
            content_type="application/json",
        )

        self.assertEqual(
            domain_admin.mock_calls,
            [
                getattr(call, method)(domains, start=start, end=end),
            ],
        )
        self.assertLog(
            LogEntryType.DOMAIN_CONTACT_INFO_UPDATE,
            log_result,
            input_properties={
                "event": event,
                "start": start.isoformat(),
                "end": end.isoformat(),
            },
            input_references={LogReferenceType.DOMAIN: ["domain.1", "domain.2"]},
            content=json.dumps(
                {
                    "domain.1": {"email": ["kryten@example.org", "rimmer@example.org"], "telephone": []},
                    "domain.2": {"email": [], "telephone": ["+420.000000"]},
                }
            ),
        )
        return response

    def test_update_outzone_success(self, domain_admin):
        domain_admin.update_domains_outzone_additional_notify_info.return_value = make_awaitable(None)

        response = self._test_update(
            domain_admin,
            reverse("ferda:api:update_additional_outzone_contacts"),
            "update_domains_outzone_additional_notify_info",
            event="outzone",
        )
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.content, b"")

    def test_update_outzone_domain_not_found(self, domain_admin):
        domain_admin.update_domains_outzone_additional_notify_info.side_effect = DomainDoesNotExist

        response = self._test_update(
            domain_admin,
            reverse("ferda:api:update_additional_outzone_contacts"),
            "update_domains_outzone_additional_notify_info",
            event="outzone",
            log_result=LoggerResult.FAIL,
        )
        self.assertEqual(response.status_code, 404)
        self.assertEqual(response.json(), {"detail": "Domain does not exist"})

    def test_update_outzone_invalid_value(self, domain_admin):
        domain_admin.update_domains_outzone_additional_notify_info.side_effect = ValueError

        response = self._test_update(
            domain_admin,
            reverse("ferda:api:update_additional_outzone_contacts"),
            "update_domains_outzone_additional_notify_info",
            event="outzone",
            log_result=LoggerResult.FAIL,
        )
        self.assertEqual(response.status_code, 400)
        self.assertEqual(response.json(), {"detail": "Invalid value"})

    def test_update_delete_success(self, domain_admin):
        domain_admin.update_domains_delete_additional_notify_info.return_value = make_awaitable(None)

        response = self._test_update(
            domain_admin,
            reverse("ferda:api:update_additional_delete_contacts"),
            "update_domains_delete_additional_notify_info",
            event="delete",
        )
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.content, b"")

    def test_update_delete_domain_not_found(self, domain_admin):
        domain_admin.update_domains_delete_additional_notify_info.side_effect = DomainDoesNotExist

        response = self._test_update(
            domain_admin,
            reverse("ferda:api:update_additional_delete_contacts"),
            "update_domains_delete_additional_notify_info",
            event="delete",
            log_result=LoggerResult.FAIL,
        )
        self.assertEqual(response.status_code, 404)
        self.assertEqual(response.json(), {"detail": "Domain does not exist"})

    def test_update_delete_invalid_value(self, domain_admin):
        domain_admin.update_domains_delete_additional_notify_info.side_effect = ValueError

        response = self._test_update(
            domain_admin,
            reverse("ferda:api:update_additional_delete_contacts"),
            "update_domains_delete_additional_notify_info",
            event="delete",
            log_result=LoggerResult.FAIL,
        )
        self.assertEqual(response.status_code, 400)
        self.assertEqual(response.json(), {"detail": "Invalid value"})
