from django.urls import reverse_lazy
from regal import Domain

from ferda.constants import LogEntryType
from ferda.tests.registry.search import SearchHistoryTest, SearchTest


class DomainSearchTest(SearchTest.Base):
    object_type = Domain
    object_id = "domain_id"
    object_handle = "fqdn"
    log_entry_type = LogEntryType.DOMAIN_SEARCH
    object_search_mock = "ferda.registry.domain.search.DOMAIN_SEARCH"
    object_mock = "ferda.registry.domain.search.DOMAIN"
    search_call = "search_domain"
    info_call = "get_domain_info"
    url = reverse_lazy("ferda:api:search_domain")


class DomainHistorySearchTest(SearchHistoryTest.Base):
    object_type = Domain
    object_id = "domain_id"
    object_history_id = "domain_history_id"
    object_handle = "fqdn"
    log_entry_type = LogEntryType.DOMAIN_SEARCH_HISTORY
    object_search_mock = "ferda.registry.domain.search.DOMAIN_SEARCH"
    object_mock = "ferda.registry.domain.search.DOMAIN"
    search_call = "search_domain_history"
    info_call = "get_domain_info"
    history_call = "get_domain_history"
    url = reverse_lazy("ferda:api:search_domain_history")
