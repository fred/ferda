"""Tests for proper URL resolving."""

from typing import List, Tuple

from django.test import TestCase, override_settings
from django.urls import resolve


@override_settings(ROOT_URLCONF="ferda.tests.urls")
class ApiUrlTest(TestCase):
    """Test class for forward URL resolving of API endpoints."""

    def test_forward_url_resolving_contact(self):
        """Test contact URLs."""
        data: List[Tuple[str, str]] = [  # (URL part, API endpoint url_name in namespace)
            ("/contact-id-1/", "get_contact_info"),  # GET
            ("/contact-id-1/history/", "get_contact_history"),  # GET
            ("/contact-id-1/history/history-id-1/", "get_contact_history_info"),  # GET
            ("/contact-id-1/history/by_datetime/2023-01-01/", "get_contact_history_info_datetime"),  # GET
            ("/contact-id-1/history/-/last/", "get_contact_history_info_last"),  # GET
            ("/contact-id-1/state/", "get_contact_state"),  # GET
            ("/contact-id-1/state/history/", "get_contact_state_history"),  # GET
            ("/-/search/", "search_contact"),  # GET
            ("/-/search/history/", "search_contact_history"),  # GET
            ("/-/record_statement/contact-id-1/", "get_contact_record_statement_pdf"),  # GET
        ]
        for url, reverse_url in data:
            with self.subTest(url=url):
                self.assertEqual(resolve("/api/registry/contact" + url).view_name, "ferda:api:" + reverse_url)

    def test_forward_url_resolving_domain(self):
        """Test domain URLs."""
        data: List[Tuple[str, str]] = [  # (URL part, API endpoint url_name in namespace)
            ("/domain-id-1/", "get_domain_info"),  # GET
            ("/domain-id-1/history/", "get_domain_history"),  # GET
            ("/domain-id-1/history/history-id-1/", "get_domain_history_info"),  # GET
            ("/domain-id-1/history/by_datetime/2023-01-01/", "get_domain_history_info_datetime"),  # GET
            ("/domain-id-1/history/-/last/", "get_domain_history_info_last"),  # GET
            ("/domain-id-1/state/", "get_domain_state"),  # GET
            ("/domain-id-1/state/history/", "get_domain_state_history"),  # GET
            ("/-/search/", "search_domain"),  # GET
            ("/-/search/history/", "search_domain_history"),  # GET
            ("/-/record_statement/domain-id-1/", "get_domain_record_statement_pdf"),  # GET
            ("/-/by_contact/contact-id-1/", "get_domains_by_contact"),  # GET
            ("/-/set_manual_in_zone/domain-id-1/", "set_domain_manual_in_zone"),  # POST
            ("/-/get_additional_outzone_contacts/", "get_additional_outzone_contacts"),  # GET
            ("/-/get_additional_delete_contacts/", "get_additional_delete_contacts"),  # GET
            ("/-/update_additional_outzone_contacts/", "update_additional_outzone_contacts"),  # POST
            ("/-/update_additional_delete_contacts/", "update_additional_delete_contacts"),  # POST
        ]
        for url, reverse_url in data:
            with self.subTest(url=url):
                self.assertEqual(resolve("/api/registry/domain" + url).view_name, "ferda:api:" + reverse_url)

    def test_forward_url_resolving_keyset(self):
        """Test keyset URLs."""
        data: List[Tuple[str, str]] = [  # (URL part, API endpoint url_name in namespace)
            ("/keyset-id-1/", "get_keyset_info"),  # GET
            ("/keyset-id-1/history/", "get_keyset_history"),  # GET
            ("/keyset-id-1/history/history-id-1/", "get_keyset_history_info"),  # GET
            ("/keyset-id-1/history/by_datetime/2023-01-01/", "get_keyset_history_info_datetime"),  # GET
            ("/keyset-id-1/history/-/last/", "get_keyset_history_info_last"),  # GET
            ("/keyset-id-1/state/", "get_keyset_state"),  # GET
            ("/keyset-id-1/state/history/", "get_keyset_state_history"),  # GET
            ("/-/search/", "search_keyset"),  # GET
            ("/-/search/history/", "search_keyset_history"),  # GET
            ("/-/record_statement/keyset-id-1/", "get_keyset_record_statement_pdf"),  # GET
        ]
        for url, reverse_url in data:
            with self.subTest(url=url):
                self.assertEqual(resolve("/api/registry/keyset" + url).view_name, "ferda:api:" + reverse_url)

    def test_forward_url_resolving_nsset(self):
        """Test nsset URLs."""
        data: List[Tuple[str, str]] = [  # (URL part, API endpoint url_name in namespace)
            ("/nsset-id-1/", "get_nsset_info"),  # GET
            ("/nsset-id-1/history/", "get_nsset_history"),  # GET
            ("/nsset-id-1/history/history-id-1/", "get_nsset_history_info"),  # GET
            ("/nsset-id-1/history/by_datetime/2023-01-01/", "get_nsset_history_info_datetime"),  # GET
            ("/nsset-id-1/history/-/last/", "get_nsset_history_info_last"),  # GET
            ("/nsset-id-1/state/", "get_nsset_state"),  # GET
            ("/nsset-id-1/state/history/", "get_nsset_state_history"),  # GET
            ("/-/search/", "search_nsset"),  # GET
            ("/-/search/history/", "search_nsset_history"),  # GET
            ("/-/record_statement/nsset-id-1/", "get_nsset_record_statement_pdf"),  # GET
        ]
        for url, reverse_url in data:
            with self.subTest(url=url):
                self.assertEqual(resolve("/api/registry/nsset" + url).view_name, "ferda:api:" + reverse_url)

    def test_forward_url_resolving_registrar(self):
        """Test registrar URLs."""
        data: List[Tuple[str, str]] = [  # (URL part, API endpoint url_name in namespace)
            ("/", "registrars"),  # GET
            ("/registrar-id-1/", "registrar"),  # GET, PUT
            ("/handle-1/epp_credentials/", "registrar_epp_credentials"),  # GET, POST
            ("/handle-1/epp_credentials/credentials-id-1/", "registrar_epp_credential"),  # PUT, DELETE
            ("/handle-1/zone_access/", "registrar_zone_access"),  # GET, POST, PUT, DELETE
            ("/registrar-id-1/certification/", "registrar_certification"),  # GET, POST
            ("/registrar-id-1/certification_file/file-id-1/", "registrar_certification_file"),  # GET
            ("/registrar-id-1/certification/certification-id-1/", "registrar_certification"),  # POST, DELETE
            ("/-/groups/", "registrar_groups"),  # GET
            ("/registrar-id-1/group/", "registrar_groups"),  # GET
            ("/registrar-id-1/group/group-1/", "registrar_groups"),  # POST, DELETE
        ]
        for url, reverse_url in data:
            with self.subTest(url=url):
                self.assertEqual(resolve("/api/registry/registrar" + url).view_name, "ferda:api:" + reverse_url)

    def test_forward_url_resolving_representative(self):
        """Test representative URLs."""
        data: List[Tuple[str, str]] = [  # (URL part, API endpoint url_name in namespace)
            ("/", "add_representative"),  # POST
            ("/representative-id-1/", "representative"),  # GET, PUT, DELETE
            ("/representative-id-1/history/", "representative_history"),  # GET
            ("/representative-id-1/contact/", "representative_contact"),  # GET
        ]
        for url, reverse_url in data:
            with self.subTest(url=url):
                self.assertEqual(resolve("/api/registry/representative" + url).view_name, "ferda:api:" + reverse_url)
