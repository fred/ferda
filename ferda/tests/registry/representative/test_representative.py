from unittest.mock import call, patch, sentinel

from django.contrib.auth import get_user_model
from django.test import TestCase, override_settings, tag
from django.urls import reverse
from fred_types import ContactId, RepresentativeId
from regal import Representative
from regal.exceptions import InvalidData, RepresentativeDoesNotExist

from ferda.constants import LogEntryType, LoggerResult, LogReferenceType
from ferda.registry.representative.api import RepresentativeData
from ferda.tests.utils import LoggerMixin

User = get_user_model()


@tag("api", "registry")
@patch("ferda.registry.representative.api.REPRESENTATIVE", autospec=True)
@override_settings(ROOT_URLCONF="ferda.tests.urls")
class RepresentativeTest(LoggerMixin, TestCase):
    """Test class for contact representatives."""

    def setUp(self):
        super().setUp()
        user = User.objects.create_superuser("admin")
        self.client.force_login(user)

    # --- get_representative_info

    def test_get_representative_info(self, client):
        representative = Representative(
            id=RepresentativeId("Repr-1"),
            contact_id=ContactId("c:1"),
        )
        client.get.return_value = representative

        response = self.client.get(reverse("ferda:api:representative", kwargs={"representative_id": "Repr-1"}))

        self.assertEqual(response.status_code, 200)
        self.assertEqual(Representative(**response.json()), representative)
        self.assertEqual(client.mock_calls, [call.get("Repr-1")])
        self.assertLog(
            LogEntryType.CONTACT_REPRESENTATIVE_DETAIL,
            LoggerResult.SUCCESS,
            input_properties={"representative_id": "Repr-1"},
            output_references={LogReferenceType.REPRESENTATIVE: "Repr-1", LogReferenceType.CONTACT: "c:1"},
        )

    def test_get_representative_info_does_not_exist(self, client):
        client.get.side_effect = RepresentativeDoesNotExist

        response = self.client.get(reverse("ferda:api:representative", kwargs={"representative_id": "Repr-1"}))

        self.assertEqual(response.status_code, 404)
        self.assertEqual(response.json(), {"detail": "Object does not exist"})
        self.assertLog(
            LogEntryType.CONTACT_REPRESENTATIVE_DETAIL,
            LoggerResult.FAIL,
            input_properties={"representative_id": "Repr-1"},
        )

    # --- get_representative_info_history

    def test_get_representative_info_history(self, client):
        representative = Representative(
            id=RepresentativeId("Repr-1"),
            contact_id=ContactId("c:1"),
        )
        client.get.return_value = representative

        response = self.client.get(reverse("ferda:api:representative_history", kwargs={"history_id": "Hist-1"}))

        self.assertEqual(response.status_code, 200)
        self.assertEqual(Representative(**response.json()), representative)
        self.assertEqual(client.mock_calls, [call.get("Hist-1")])
        self.assertLog(
            LogEntryType.CONTACT_REPRESENTATIVE_DETAIL,
            LoggerResult.SUCCESS,
            input_properties={"history_id": "Hist-1"},
            output_references={LogReferenceType.REPRESENTATIVE: "Repr-1", LogReferenceType.CONTACT: "c:1"},
        )

    # --- get_representative_info_contact

    def test_get_representative_info_contact(self, client):
        representative = Representative(
            id=RepresentativeId("Repr-1"),
            contact_id=ContactId("c:1"),
        )
        client.get.return_value = representative

        response = self.client.get(reverse("ferda:api:representative_contact", kwargs={"contact_id": "Cont-1"}))

        self.assertEqual(response.status_code, 200)
        self.assertEqual(Representative(**response.json()), representative)
        self.assertEqual(client.mock_calls, [call.get("Cont-1")])
        self.assertLog(
            LogEntryType.CONTACT_REPRESENTATIVE_DETAIL,
            LoggerResult.SUCCESS,
            input_properties={"contact_id": "Cont-1"},
            output_references={LogReferenceType.REPRESENTATIVE: "Repr-1", LogReferenceType.CONTACT: "c:1"},
        )

    # --- create_representative

    def test_create_representative(self, client):
        representative_data = RepresentativeData(contact_id=ContactId("c:1"))
        representative = Representative(id=RepresentativeId("Repr-1"), **representative_data.model_dump())
        client.create.return_value = representative

        response = self.client.post(
            reverse("ferda:api:add_representative"),
            data=representative_data.model_dump_json(),
            content_type="application/json",
        )

        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.json(), representative.model_dump())
        representative.id = RepresentativeId("")  # 'id' not yet set in method call
        self.assertEqual(client.mock_calls, [call.create(representative, log_entry_id=sentinel.log_entry_id)])
        self.assertLog(
            LogEntryType.CONTACT_REPRESENTATIVE_CREATE,
            LoggerResult.SUCCESS,
            output_references={LogReferenceType.REPRESENTATIVE: "Repr-1", LogReferenceType.CONTACT: "c:1"},
        )

    def test_create_representative_does_not_exist(self, client):
        representative = RepresentativeData(contact_id=ContactId("c:1"))
        client.create.side_effect = RepresentativeDoesNotExist

        response = self.client.post(
            reverse("ferda:api:add_representative"),
            data=representative.model_dump_json(),
            content_type="application/json",
        )

        self.assertEqual(response.status_code, 404)
        self.assertEqual(response.json(), {"detail": "Object does not exist"})
        self.assertLog(
            LogEntryType.CONTACT_REPRESENTATIVE_CREATE,
            LoggerResult.FAIL,
        )

    def test_create_representative_invalid_data(self, client):
        representative = RepresentativeData(contact_id=ContactId("c:1"))
        client.create.side_effect = (InvalidData(fields=["name", "email"]),)

        response = self.client.post(
            reverse("ferda:api:add_representative"),
            data=representative.model_dump_json(),
            content_type="application/json",
        )

        self.assertEqual(response.status_code, 400)
        self.assertEqual(response.json(), {"detail": "Invalid data", "fields": ["name", "email"]})
        self.assertLog(
            LogEntryType.CONTACT_REPRESENTATIVE_CREATE,
            LoggerResult.FAIL,
        )

    # --- update_representative

    def test_update_representative(self, client):
        representative_data = RepresentativeData(
            contact_id=ContactId("c:1"),
            name="Jekyll",
        )
        representative_in = Representative(id=RepresentativeId("Repr-1"), **representative_data.model_dump())
        representative_out = Representative(
            id=RepresentativeId("Repr-1"),
            contact_id=ContactId("c:1"),
            name="Hyde",
        )
        client.update.return_value = representative_out

        response = self.client.put(
            reverse("ferda:api:representative", kwargs={"representative_id": "Repr-1"}),
            data=representative_data.model_dump_json(),
            content_type="application/json",
        )

        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.json(), representative_out.model_dump())
        self.assertEqual(client.mock_calls, [call.update(representative_in, log_entry_id=sentinel.log_entry_id)])
        self.assertLog(
            LogEntryType.CONTACT_REPRESENTATIVE_CHANGE,
            LoggerResult.SUCCESS,
            input_properties={"representative_id": "Repr-1"},
            output_references={LogReferenceType.REPRESENTATIVE: "Repr-1", LogReferenceType.CONTACT: "c:1"},
        )

    def test_update_representative_does_not_exist(self, client):
        representative = RepresentativeData(contact_id=ContactId("c:1"))
        client.update.side_effect = RepresentativeDoesNotExist

        response = self.client.put(
            reverse("ferda:api:representative", kwargs={"representative_id": "Repr-1"}),
            data=representative.model_dump_json(),
            content_type="application/json",
        )

        self.assertEqual(response.status_code, 404)
        self.assertEqual(response.json(), {"detail": "Object does not exist"})
        self.assertLog(
            LogEntryType.CONTACT_REPRESENTATIVE_CHANGE,
            LoggerResult.FAIL,
            input_properties={"representative_id": "Repr-1"},
        )

    def test_update_representative_invalid_data(self, client):
        representative = RepresentativeData(contact_id=ContactId("c:1"))
        client.update.side_effect = (InvalidData(fields=["name", "email"]),)

        response = self.client.put(
            reverse("ferda:api:representative", kwargs={"representative_id": "Repr-1"}),
            data=representative.model_dump_json(),
            content_type="application/json",
        )

        self.assertEqual(response.status_code, 400)
        self.assertEqual(response.json(), {"detail": "Invalid data", "fields": ["name", "email"]})
        self.assertLog(
            LogEntryType.CONTACT_REPRESENTATIVE_CHANGE,
            LoggerResult.FAIL,
            input_properties={"representative_id": "Repr-1"},
        )

    # --- delete_representative

    def test_delete_representative(self, client):
        client.get.return_value = None

        response = self.client.delete(reverse("ferda:api:representative", kwargs={"representative_id": "Repr-1"}))

        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.content, b"")
        self.assertEqual(client.mock_calls, [call.delete("Repr-1", log_entry_id=sentinel.log_entry_id)])
        self.assertLog(
            LogEntryType.CONTACT_REPRESENTATIVE_DELETE,
            LoggerResult.SUCCESS,
            input_properties={"representative_id": "Repr-1"},
            output_references={LogReferenceType.REPRESENTATIVE: "Repr-1"},
        )

    def test_delete_representative_does_not_exist(self, client):
        client.delete.side_effect = RepresentativeDoesNotExist

        response = self.client.delete(reverse("ferda:api:representative", kwargs={"representative_id": "Repr-1"}))

        self.assertEqual(response.status_code, 404)
        self.assertEqual(response.json(), {"detail": "Object does not exist"})
        self.assertLog(
            LogEntryType.CONTACT_REPRESENTATIVE_DELETE,
            LoggerResult.FAIL,
            input_properties={"representative_id": "Repr-1"},
        )
