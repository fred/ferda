from functools import partial
from typing import Sequence
from unittest.mock import ANY, call, patch, sentinel

from django.apps import apps
from django.db.models.signals import post_migrate
from django.test import RequestFactory, SimpleTestCase, TestCase, override_settings
from grill.exceptions import SessionDoesNotExist
from testfixtures.comparison import compare

import ferda
from ferda.apps import close_session, create_session
from ferda.constants import (
    LOGGER_SERVICE,
    LOGGER_SERVICE_HANDLE,
    LOGGER_SESSION_KEY,
    LogEntryType,
    LoggerResult,
    LogReferenceType,
)
from ferda.models import User
from ferda.utils import create_permissions

from .utils import get_plugin_manager


class TestPlugin:
    @ferda.hookimpl
    def create_permissions(self):
        return (
            ("ferda_plugin.view", "View ferda plugin"),
            ("ferda_plugin.edit", "Edit ferda plugin"),
        )


class TestCreateSession(SimpleTestCase):
    def setUp(self):
        logger_patcher = patch("ferda.backend.LOGGER.client", autospec=True)
        self.addCleanup(logger_patcher.stop)
        self.logger = logger_patcher.start()
        self.logger.create_session.return_value = sentinel.session_id

    def test_create_session(self):
        request = RequestFactory().get("/dummy/")
        request.session = {}
        user = User(username=sentinel.username, pk=sentinel.pk)

        create_session(sender=sentinel.sender, request=request, user=user)

        self.assertEqual(request.session[LOGGER_SESSION_KEY], sentinel.session_id)
        calls = [call.create_session(sentinel.pk, sentinel.username)]
        self.assertEqual(self.logger.mock_calls, calls)


class TestCloseSession(SimpleTestCase):
    def setUp(self):
        logger_patcher = patch("ferda.backend.LOGGER.client", autospec=True)
        self.addCleanup(logger_patcher.stop)
        self.logger = logger_patcher.start()

    def test_close_session(self):
        request = RequestFactory().get("/dummy/")
        request.session = {LOGGER_SESSION_KEY: sentinel.session_id}

        close_session(sender=sentinel.sender, request=request)

        self.assertNotIn(LOGGER_SESSION_KEY, request.session)
        calls = [call.close_session(sentinel.session_id)]
        self.assertEqual(self.logger.mock_calls, calls)

    def test_close_session_not_exist(self):
        # Test session not exist is ignored.
        self.logger.close_session.side_effect = SessionDoesNotExist
        request = RequestFactory().get("/dummy/")
        request.session = {LOGGER_SESSION_KEY: sentinel.session_id}

        close_session(sender=sentinel.sender, request=request)

        self.assertNotIn(LOGGER_SESSION_KEY, request.session)
        calls = [call.close_session(sentinel.session_id)]
        self.assertEqual(self.logger.mock_calls, calls)

    def test_close_session_no_id(self):
        # Test logger session is not closed, if its ID isn't present in session.
        request = RequestFactory().get("/dummy/")
        request.session = {}

        close_session(sender=sentinel.sender, request=request)

        self.assertEqual(self.logger.mock_calls, [])


class FerdaConfigTest(TestCase):
    def setUp(self):
        apps.clear_cache()

    def tearDown(self):
        apps.clear_cache()

    def compare_in(self, member: object, sequence: Sequence) -> bool:
        for item in sequence:
            try:
                compare(member, item)
            except AssertionError:
                pass
            else:
                return True
        raise AssertionError("Member {} not found in {}".format(member, sequence))  # pragma: no cover

    @patch("ferda.plugin.get_plugin_manager", autospec=True)
    @patch("ferda.apps.LOGGER_CLIENT", autospec=True)
    def test_ready(self, logger_mock, plugin_manager):
        pm = get_plugin_manager()
        pm.register(TestPlugin(), name="ferda_plugin")
        plugin_manager.return_value = pm

        with override_settings(INSTALLED_APPS=["ferda.apps.FerdaConfig"]):
            self.assertEqual(
                logger_mock.mock_calls,
                [
                    call.register_service(LOGGER_SERVICE, handle=LOGGER_SERVICE_HANDLE),
                    call.register_log_entry_types(LOGGER_SERVICE, log_entry_types=LogEntryType),
                    call.register_results(LOGGER_SERVICE, results=LoggerResult),
                    call.register_object_reference_types(reference_types=LogReferenceType),
                ],
            )
            self.compare_in(
                (
                    ANY,
                    partial(
                        create_permissions,
                        (
                            ("ferda_plugin.view", "View ferda plugin"),
                            ("ferda_plugin.edit", "Edit ferda plugin"),
                        ),
                    ),
                ),
                post_migrate.receivers,
            )
