from datetime import datetime
from unittest.mock import call, patch, sentinel
from urllib.parse import quote, urlencode

import pytz
from django.apps import apps
from django.contrib.auth import get_user_model
from django.contrib.auth.models import Permission
from django.test import TestCase, override_settings, tag
from django.urls import reverse
from grill import LogProperty
from grill.clients import ListProperty
from grill.exceptions import InvalidArgument, LogEntryDoesNotExist
from snapshottest.django import TestCase as SnapshotTestCase

from ferda.constants import LogEntryType, LoggerResult
from ferda.logger.api import DEFAULT_PAGE_SIZE
from ferda.tests.utils import LOGGER_SESSION_ID, LoggerMixin
from ferda.utils import create_permissions

from .utils import make_awaitable, url_strip_hostname

User = get_user_model()


def get_log_entry(**kwargs) -> dict:
    log_entry = {
        "log_entry_id": "1#20220101T120000.000000#Ferda#0",
        "time_begin": datetime(2022, 1, 1, 12, tzinfo=pytz.utc),
        "time_end": datetime(2022, 1, 1, 13, tzinfo=pytz.utc),
        "source_ip": "127.0.0.1",
        "service": "ServiceA",
        "session_id": "session:1",
        "log_entry_type": "TypeA",
        "result_code": 0,
        "result_name": "Success",
        "raw_request": None,
        "raw_response": None,
        "username": "RIMMER",
        "user_id": 0,
        "input_properties": {},
        "output_properties": {},
        "references": {},
    }
    log_entry.update(kwargs)
    return log_entry


@tag("api", "logger")
@override_settings(ROOT_URLCONF="ferda.tests.urls")
class ListServicesTest(LoggerMixin, TestCase):
    def setUp(self):
        super().setUp()
        create_permissions(
            [
                ("ferda.can_view_logger_service__ServiceA", "Can view logger service ServiceA"),
                ("ferda.can_view_logger_service__ServiceB", "Can view logger service ServiceB"),
                ("ferda.can_view_logger_service__ServiceC", "Can view logger service ServiceC"),
            ],
            apps=apps,
        )
        user = User.objects.create_superuser("admin")  # type: ignore
        self.client.force_login(user)

    @patch("ferda.logger.api.LOGGER_CLIENT")
    def test_list(self, logger_client):
        logger_client.get_services.return_value = ["ServiceA", "ServiceB"]

        response = self.client.get(reverse("ferda:api:list_services"))
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.json(), ["ServiceA", "ServiceB"])
        self.assertEqual(logger_client.mock_calls, [call.get_services()])

    @patch("ferda.logger.api.LOGGER_CLIENT")
    def test_list_limited_access(self, logger_client):
        logger_client.get_services.return_value = ["ServiceA", "ServiceB"]
        user = User.objects.create_user("user")  # type: ignore
        user.user_permissions.add(Permission.objects.get(codename="can_view_registry"))
        user.user_permissions.add(Permission.objects.get(codename="can_view_logger_service__ServiceA"))
        user.user_permissions.add(Permission.objects.get(codename="can_view_logger_service__ServiceC"))
        self.client.force_login(user)

        response = self.client.get(reverse("ferda:api:list_services"))
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.json(), ["ServiceA"])
        self.assertEqual(logger_client.mock_calls, [call.get_services()])


@tag("api", "logger")
@override_settings(ROOT_URLCONF="ferda.tests.urls")
class GetServiceTest(LoggerMixin, TestCase):
    def setUp(self):
        super().setUp()
        user = User.objects.create_superuser("admin")  # type: ignore
        self.client.force_login(user)

    @patch("ferda.logger.api.LOGGER_CLIENT")
    def test_get(self, logger_client):
        logger_client.get_log_entry_types.return_value = ["TypeA", "TypeB"]
        logger_client.get_results.return_value = {"Success": 0, "Error": 1}
        logger_client.get_object_reference_types.return_value = ["contact", "domain"]

        response = self.client.get(reverse("ferda:api:get_service", kwargs={"service_name": "ServiceA"}))
        self.assertEqual(response.status_code, 200)
        self.assertEqual(
            response.json(),
            {
                "name": "ServiceA",
                "log_entry_types": ["TypeA", "TypeB"],
                "results": {"Success": 0, "Error": 1},
                "object_reference_types": ["contact", "domain"],
            },
        )
        self.assertEqual(
            logger_client.mock_calls,
            [
                call.get_log_entry_types("ServiceA"),
                call.get_results("ServiceA"),
                call.get_object_reference_types(),
            ],
        )

    @patch("ferda.logger.api.LOGGER_CLIENT")
    def test_get_invalid_argument(self, logger_client):
        logger_client.get_log_entry_types.side_effect = InvalidArgument

        response = self.client.get(reverse("ferda:api:get_service", kwargs={"service_name": "ServiceA"}))
        self.assertEqual(response.status_code, 404)
        self.assertEqual(response.json(), {"detail": "Service 'ServiceA' does not exist"})
        self.assertEqual(logger_client.mock_calls, [call.get_log_entry_types("ServiceA")])


@tag("api", "logger")
@override_settings(ROOT_URLCONF="ferda.tests.urls")
@patch("ferda.logger.api.LOGGER_SEARCH")
class ListLogEntriesTest(LoggerMixin, SnapshotTestCase):
    def setUp(self):
        super().setUp()
        self.service = "ServiceA"
        self.log_entry_id = "1#20220101T120000.000000#Ferda#0"

        self.user = User.objects.create_superuser("admin")  # type: ignore
        self.async_client.force_login(self.user)

    async def test_list_success(self, logger_search):
        log_entry = get_log_entry(log_entry_id=self.log_entry_id, service=self.service)
        logger_search.get_log_entry_info.return_value = make_awaitable(log_entry)
        logger_search.list_log_entries.return_value = make_awaitable([self.log_entry_id])

        response = await self.async_client.get(
            reverse("ferda:api:list_log_entries")
            + "?%s"
            % urlencode(
                {
                    "service": self.service,
                    "time_begin_from": datetime(2022, 1, 1, tzinfo=pytz.utc).isoformat(),
                    "time_begin_to": datetime(2022, 1, 2, tzinfo=pytz.utc).isoformat(),
                    "log_entry_types": ["TypeA", "TypeB"],
                    "username": "RIMMER",
                    "reference": "contact.some_uid",
                    "properties": ["soup.gazpacho"],
                },
                doseq=True,
            )
        )

        self.assertEqual(response.status_code, 200)
        self.assertMatchSnapshot(response.json())
        self.assertEqual(
            logger_search.mock_calls,
            [
                call.list_log_entries(
                    service=self.service,
                    timestamp_from=datetime(2022, 1, 1, tzinfo=pytz.utc),
                    timestamp_to=datetime(2022, 1, 2, tzinfo=pytz.utc),
                    log_entry_types=["TypeA", "TypeB"],
                    user="RIMMER",
                    reference_type="contact",
                    reference_value="some_uid",
                    properties=[ListProperty("soup", "gazpacho")],
                    limit=DEFAULT_PAGE_SIZE + 1,
                ),
                call.get_log_entry_info(log_entry_id=self.log_entry_id),
            ],
        )
        self.assertLog(
            LogEntryType.LOGGER_LIST,
            LoggerResult.SUCCESS,
            input_properties={
                "service": self.service,
                "time_begin_from": datetime(2022, 1, 1, tzinfo=pytz.utc).isoformat(),
                "time_begin_to": datetime(2022, 1, 2, tzinfo=pytz.utc).isoformat(),
                "log_entry_types": ["TypeA", "TypeB"],
                "username": "RIMMER",
                "page_size": str(DEFAULT_PAGE_SIZE),
                "offset": str(0.0),
                "reference": LogProperty(
                    "",
                    children={
                        "type": "contact",
                        "value": "some_uid",
                    },
                ),
                "properties": [
                    LogProperty("", children={"name": "soup", "value": "gazpacho"}),
                ],
            },
        )

    async def test_list_invalid_argument(self, logger_search):
        logger_search.list_log_entries.side_effect = InvalidArgument

        response = await self.async_client.get(
            reverse("ferda:api:list_log_entries")
            + "?%s"
            % urlencode(
                {
                    "service": self.service,
                    "time_begin_from": datetime(2022, 1, 1, tzinfo=pytz.utc).isoformat(),
                    "time_begin_to": datetime(2022, 1, 2, tzinfo=pytz.utc).isoformat(),
                },
                doseq=True,
            )
        )

        self.assertEqual(response.status_code, 400)
        self.assertEqual(response.json(), {"detail": "Invalid argument"})
        self.assertEqual(
            logger_search.mock_calls,
            [
                call.list_log_entries(
                    service=self.service,
                    timestamp_from=datetime(2022, 1, 1, tzinfo=pytz.utc),
                    timestamp_to=datetime(2022, 1, 2, tzinfo=pytz.utc),
                    log_entry_types=[],
                    user=None,
                    reference_type=None,
                    reference_value=None,
                    properties=[],
                    limit=DEFAULT_PAGE_SIZE + 1,
                ),
            ],
        )
        self.assertLog(
            LogEntryType.LOGGER_LIST,
            LoggerResult.FAIL,
            input_properties={
                "service": self.service,
                "time_begin_from": datetime(2022, 1, 1, tzinfo=pytz.utc).isoformat(),
                "time_begin_to": datetime(2022, 1, 2, tzinfo=pytz.utc).isoformat(),
                "log_entry_types": [],
                "page_size": str(DEFAULT_PAGE_SIZE),
                "offset": str(0.0),
            },
        )

    async def test_list_pagination(self, logger_search):
        log_entries = [
            get_log_entry(
                log_entry_id=str(i),
                time_begin=datetime(2022, 1, 1, i, tzinfo=pytz.utc),
                time_end=datetime(2022, 1, 1, i, 1, tzinfo=pytz.utc),
            )
            for i in range(3)
        ]
        logger_search.get_log_entry_info.side_effect = [
            make_awaitable(log_entry) for log_entry in log_entries[:3] + log_entries[2:]
        ]
        logger_search.list_log_entries.side_effect = [
            make_awaitable([log_entry["log_entry_id"] for log_entry in log_entries[:3]]),
            make_awaitable([log_entry["log_entry_id"] for log_entry in log_entries[2:]]),
        ]

        # Fetch first page
        response = await self.async_client.get(
            reverse("ferda:api:list_log_entries")
            + "?%s"
            % urlencode(
                {
                    "service": self.service,
                    "time_begin_from": datetime(2022, 1, 1, tzinfo=pytz.utc).isoformat(),
                    "time_begin_to": datetime(2022, 1, 2, tzinfo=pytz.utc).isoformat(),
                    "page_size": 2,
                }
            )
        )
        self.assertEqual(response.status_code, 200)
        self.assertMatchSnapshot(response.json())

        # Fetch second page
        response = await self.async_client.get(url_strip_hostname(response.json()["next"]))
        self.assertEqual(response.status_code, 200)
        self.assertMatchSnapshot(response.json())

        self.assertEqual(
            logger_search.mock_calls,
            [
                call.list_log_entries(
                    service=self.service,
                    timestamp_from=datetime(2022, 1, 1, tzinfo=pytz.utc),
                    timestamp_to=datetime(2022, 1, 2, tzinfo=pytz.utc),
                    log_entry_types=[],
                    user=None,
                    reference_type=None,
                    reference_value=None,
                    properties=[],
                    limit=3,
                ),
                call.get_log_entry_info(log_entry_id="0"),
                call.get_log_entry_info(log_entry_id="1"),
                call.get_log_entry_info(log_entry_id="2"),
                call.list_log_entries(
                    service=self.service,
                    timestamp_from=datetime(2022, 1, 1, tzinfo=pytz.utc),
                    timestamp_to=datetime(2022, 1, 1, 1, tzinfo=pytz.utc),
                    log_entry_types=[],
                    user=None,
                    reference_type=None,
                    reference_value=None,
                    properties=[],
                    limit=3,
                ),
                call.get_log_entry_info(log_entry_id="2"),
            ],
        )
        self.assertEqual(
            self.logger.mock_calls,
            [
                call.create_session(1, "admin"),
                call.create_log_entry(
                    "Ferda",
                    LogEntryType.LOGGER_LIST,
                    source_ip="127.0.0.1",
                    session_id=LOGGER_SESSION_ID,
                    content=None,
                    properties={
                        "service": self.service,
                        "time_begin_from": datetime(2022, 1, 1, tzinfo=pytz.utc).isoformat(),
                        "time_begin_to": datetime(2022, 1, 2, tzinfo=pytz.utc).isoformat(),
                        "log_entry_types": [],
                        "page_size": "2",
                        "offset": "0.0",
                    },
                    references=None,
                ),
                call.close_log_entry(
                    sentinel.log_entry_id,
                    LoggerResult.SUCCESS,
                    session_id=LOGGER_SESSION_ID,
                    content=None,
                    properties={},
                    references={},
                ),
                call.create_log_entry(
                    "Ferda",
                    LogEntryType.LOGGER_LIST,
                    source_ip="127.0.0.1",
                    session_id=LOGGER_SESSION_ID,
                    content=None,
                    properties={
                        "service": self.service,
                        "time_begin_from": datetime(2022, 1, 1, tzinfo=pytz.utc).isoformat(),
                        "time_begin_to": datetime(2022, 1, 2, tzinfo=pytz.utc).isoformat(),
                        "log_entry_types": [],
                        "page_size": "2",
                        "offset": str(23 * 60 * 60.0),
                    },
                    references=None,
                ),
                call.close_log_entry(
                    sentinel.log_entry_id,
                    LoggerResult.SUCCESS,
                    session_id=LOGGER_SESSION_ID,
                    content=None,
                    properties={},
                    references={},
                ),
            ],
        )


@tag("api", "logger")
@override_settings(ROOT_URLCONF="ferda.tests.urls")
@patch("ferda.logger.api.LOGGER_SEARCH")
class GetLogEntryTest(LoggerMixin, SnapshotTestCase):
    def setUp(self):
        super().setUp()
        self.log_entry_id = "1#20220101T120000.000000#Ferda#0"

        self.user = User.objects.create_superuser("admin")  # type: ignore
        self.async_client.force_login(self.user)

    async def test_get_success(self, logger_search):
        log_entry = get_log_entry(log_entry_id=self.log_entry_id, service="ServiceA")
        logger_search.get_log_entry_info.return_value = make_awaitable(log_entry)

        response = await self.async_client.get(
            reverse("ferda:api:get_log_entry", kwargs={"log_entry_id": quote(self.log_entry_id)}),
        )
        self.assertEqual(response.status_code, 200)
        self.assertMatchSnapshot(response.json())
        self.assertEqual(
            logger_search.mock_calls,
            [
                call.get_log_entry_info(log_entry_id=self.log_entry_id),
            ],
        )
        self.assertLog(
            LogEntryType.LOGGER_DETAIL,
            LoggerResult.SUCCESS,
            input_properties={"log_entry_id": self.log_entry_id},
        )

    async def test_get_not_found(self, logger_search):
        logger_search.get_log_entry_info.side_effect = LogEntryDoesNotExist

        response = await self.async_client.get(
            reverse("ferda:api:get_log_entry", kwargs={"log_entry_id": quote(self.log_entry_id)}),
        )
        self.assertEqual(response.status_code, 404)
        self.assertEqual(response.json(), {"detail": "Log entry not found"})
        self.assertEqual(
            logger_search.mock_calls,
            [
                call.get_log_entry_info(log_entry_id=self.log_entry_id),
            ],
        )
        self.assertLog(
            LogEntryType.LOGGER_DETAIL,
            LoggerResult.FAIL,
            input_properties={"log_entry_id": self.log_entry_id},
        )


@tag("api", "logger")
@override_settings(ROOT_URLCONF="ferda.tests.urls")
@patch("ferda.logger.api.LOGGER_SEARCH")
class GetLogEntryPermissionsTest(LoggerMixin, SnapshotTestCase):
    def setUp(self):
        super().setUp()
        self.log_entry_id = "1#20220101T120000.000000#Ferda#0"

        create_permissions(
            [
                ("ferda.can_view_logger_service__ServiceA", "Can view logger service ServiceA"),
                ("ferda.can_view_logger_service__ServiceB", "Can view logger service ServiceB"),
            ],
            apps=apps,
        )
        self.user = User.objects.create_user("admin")  # type: ignore
        self.user.user_permissions.add(Permission.objects.get(codename="can_view_registry"))
        self.user.user_permissions.add(Permission.objects.get(codename="can_view_logger_service__ServiceA"))
        self.async_client.force_login(self.user)

    async def test_get_unauthorized(self, logger_search):
        log_entry = get_log_entry(log_entry_id=self.log_entry_id, service="ServiceB")
        logger_search.get_log_entry_info.return_value = make_awaitable(log_entry)

        response = await self.async_client.get(
            reverse("ferda:api:get_log_entry", kwargs={"log_entry_id": quote(self.log_entry_id)}),
        )
        self.assertEqual(response.status_code, 401)
        self.assertEqual(response.json(), {"detail": "Unauthorized"})
        self.assertEqual(
            logger_search.mock_calls,
            [
                call.get_log_entry_info(log_entry_id=self.log_entry_id),
            ],
        )
        self.assertLog(
            LogEntryType.LOGGER_DETAIL,
            LoggerResult.FAIL,
            input_properties={"log_entry_id": self.log_entry_id},
        )
