from unittest.mock import call, patch

from django.test import SimpleTestCase, override_settings
from django.urls import include, path
from snapshottest.django import SimpleTestCase as SnapshotTestCase
from testfixtures import replace_in_environ

import ferda
from ferda.constants import Env
from ferda.plugin.hookimpl import _get_version, add_urls_to_settings, create_permissions, show_settings


class GetVersionTest(SimpleTestCase):
    def test_with_deploy_id(self):
        with replace_in_environ(Env.FERDA_DEPLOY_ID, "gazpacho"):
            with patch.object(ferda, "__version__", new="1.2.3"):
                self.assertEqual(_get_version(), "1.2.3+gazpacho")

    def test_without_deploy_id(self):
        with replace_in_environ(Env.FERDA_DEPLOY_ID, ""):
            with patch.object(ferda, "__version__", new="1.2.3"):
                self.assertEqual(_get_version(), "1.2.3")


class ShowSettingsTest(SnapshotTestCase):
    @override_settings(
        TIME_ZONE="UTC",
        LANGUAGE_CODE="tlh",
        LANGUAGES=[("tlh", "Klingon")],
        AUTHENTICATION_BACKENDS=["django.contrib.auth.backends.ModelBackend"],
        FERDA_MANUAL_IN_ZONE_DEFAULT_DURATION=42,
        FERDA_OBJECT_DETAIL_LOGGER_SERVICES=["ferda"],
        FERDA_MAX_FILE_UPLOAD_SIZE=42000,
    )
    def test_show_settings(self):
        with replace_in_environ(Env.FERDA_DEPLOY_ID, ""):
            with patch.object(ferda, "__version__", new="1.2.3"):
                self.assertMatchSnapshot(show_settings())


urlpatterns = [
    path("", include("ferda.urls")),
    path("mess_url/", include("ferda.urls.messages")),
    path("report_url/", include("ferda.urls.reports")),
]


class AddUrlsToSettingsTest(SimpleTestCase):
    @override_settings(ROOT_URLCONF=__name__)
    def test_urls(self):
        self.assertEqual(
            add_urls_to_settings(),
            {
                "common": {
                    "app_url_prefixes": {
                        "ferda": "",
                        "messages": "mess_url/",
                        "reports": "report_url/",
                    },
                },
            },
        )


class CreatePermissionsTest(SimpleTestCase):
    def test_create_permissions(self):
        with patch("ferda.plugin.hookimpl.LOGGER_CLIENT", autospec=True) as logger_mock:
            logger_mock.get_services.return_value = ["RedDwarf", "Starbug1", "Starbug2"]

            self.assertEqual(
                create_permissions(),
                [
                    ("ferda.can_view_logger_service__RedDwarf", "Can view logger service RedDwarf"),
                    ("ferda.can_view_logger_service__Starbug1", "Can view logger service Starbug1"),
                    ("ferda.can_view_logger_service__Starbug2", "Can view logger service Starbug2"),
                ],
            )

            self.assertEqual(logger_mock.mock_calls, [call.get_services()])
