# snapshottest: v1 - https://goo.gl/zC4yUc

from snapshottest import Snapshot


snapshots = Snapshot()

snapshots["ShowSettingsTest::test_show_settings 1"] = {
    "common": {
        "fido2_authentication": False,
        "language_code": "tlh",
        "languages": ["tlh"],
        "max_file_upload_size": 42000,
        "time_zone": "UTC",
    },
    "registry": {"manual_in_zone_default_duration": 42, "object_detail_logger_services": ["ferda"]},
    "version": "1.2.3",
}
