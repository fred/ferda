from unittest.mock import patch

from django.contrib.auth import get_user_model
from django.contrib.auth.models import Permission
from django.test import SimpleTestCase, TestCase, override_settings, tag
from django.urls import reverse
from testfixtures import SequenceComparison as S, compare

import ferda

from .utils import LoggerMixin, get_plugin_manager

User = get_user_model()


class TestPlugin:
    @ferda.hookimpl(specname="show_settings")
    def show_settings_1(self):
        return {"common": {"language": "english"}, "registry": {"lower_limit": 1}, "languages": ["english"]}

    @ferda.hookimpl(specname="show_settings")
    def show_settings_2(self):
        return {"common": {"language": "klingon"}, "registry": {"upper_limit": 10}, "languages": ["klingon"]}


@tag("api")
@override_settings(ROOT_URLCONF="ferda.tests.urls")
class GetSettingsTest(SimpleTestCase):
    @patch("ferda.common.api.get_plugin_manager", autospec=True)
    def test_get_settings(self, plugin_manager):
        pm = get_plugin_manager()
        pm.register(TestPlugin())
        plugin_manager.return_value = pm

        response = self.client.get(reverse("ferda:api:get_settings"))

        # Hooks are executed in LIFO order
        # show_settings_2 is the last registered, so klingon wins over english
        self.assertEqual(
            response.json(),
            {
                "common": {"language": "klingon"},
                "registry": {"lower_limit": 1, "upper_limit": 10},
                "languages": ["klingon", "english"],
            },
        )


@tag("api")
@override_settings(ROOT_URLCONF="ferda.tests.urls")
class GetUserTest(LoggerMixin, TestCase):
    def setUp(self):
        super().setUp()

        self.user = User.objects.create_user(
            "rimmer",
            "arnold.rimmer@example.com",
            "password",
            first_name="Arnold",
            last_name="Rimmer",
        )
        self.user.user_permissions.set(
            Permission.objects.filter(codename__in=("can_view_registry", "can_edit_registrar"))
        )

    def test_get_user(self):
        self.client.force_login(self.user)

        response = self.client.get(reverse("ferda:api:get_user"))

        compare(
            expected={
                "username": "rimmer",
                "email": "arnold.rimmer@example.com",
                "first_name": "Arnold",
                "last_name": "Rimmer",
                "is_staff": False,
                "permissions": S("ferda.can_view_registry", "ferda.can_edit_registrar", ordered=False),
            },
            actual=response.json(),
        )
