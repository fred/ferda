from datetime import datetime
from typing import Optional, cast
from unittest.mock import Mock, call, patch, sentinel
from urllib.parse import urlencode
from uuid import UUID

import pytz
from django.contrib.auth import get_user_model
from django.test import SimpleTestCase, TestCase, override_settings, tag
from django.urls import reverse
from freezegun import freeze_time
from hermes import Email, EmailEnvelope, Letter, LetterEnvelope, Sms, SmsEnvelope
from hermes.client import NoMessage
from hermes.letter import Address
from hermes.message import MessageStatus, Reference
from snapshottest.django import TestCase as SnapshotTestCase
from testfixtures import LogCapture
from typist import TemplateCollection

from ferda.constants import LogEntryType, LoggerResult, MessageType
from ferda.messages.api import DEFAULT_PAGE_SIZE, file_stat
from ferda.tests.utils import LoggerMixin, async_gen, make_awaitable

User = get_user_model()


class MessagesMixin:
    """Mixin for testing messages."""

    def setUp(self):
        super().setUp()  # type: ignore

        # Patch secretary
        self.secretary_client = Mock(autospec=True)

        # Patch clients
        self.email_client = Mock(autospec=True)
        self.letter_client = Mock(autospec=True)
        self.sms_client = Mock(autospec=True)

        self.messages = {
            MessageType.EMAIL: self.email_client,
            MessageType.LETTER: self.letter_client,
            MessageType.SMS: self.sms_client,
        }
        messages_patcher = patch.dict("ferda.backend.messages.MESSAGES", self.messages, clear=True)
        messages_patcher.start()
        cast(TestCase, self).addCleanup(messages_patcher.stop)

    def get_email_envelope(self, message: Optional[dict] = None, **data) -> EmailEnvelope:
        message_dict = {
            "recipient": "kryton@example.org",
            "subject_template": "subject_template",
            "body_template": "body_template",
        }
        envelope_dict = {
            "uid": "message:1",
            "status": MessageStatus.SENT,
            "create_datetime": datetime(2000, 1, 1, tzinfo=pytz.utc),
        }
        message_dict.update(message or {})
        envelope_dict.update(data)

        envelope = EmailEnvelope(message=Email(**message_dict), **envelope_dict)
        envelope.message._secretary_client = self.secretary_client
        return envelope

    def get_sms_envelope(self, message: Optional[dict] = None, **data) -> SmsEnvelope:
        message_dict = {
            "recipient": "+420111222333",
            "body_template": "body_template",
        }
        envelope_dict = {
            "uid": "message:1",
            "status": MessageStatus.SENT,
            "create_datetime": datetime(2000, 1, 1, tzinfo=pytz.utc),
        }
        message_dict.update(message or {})
        envelope_dict.update(data)

        envelope = SmsEnvelope(message=Sms(**message_dict), **envelope_dict)
        envelope.message._secretary_client = self.secretary_client
        return envelope


@tag("api", "messages")
@override_settings(ROOT_URLCONF="ferda.tests.urls")
class GetMessageSubtypesTest(LoggerMixin, TestCase):
    def setUp(self):
        super().setUp()
        user = User.objects.create_superuser("admin")  # type: ignore
        self.client.force_login(user)

    @patch("ferda.messages.api.SECRETARY")
    def test_get_message_subtypes(self, secretary_mock):
        for message_type in MessageType:
            with self.subTest(message_type=message_type):
                secretary_mock.get_template_collections.return_value = [
                    TemplateCollection(name="subtype:1", label="Subtype 1", tags={message_type}),
                    TemplateCollection(name="subtype:2", label="Subtype 2", tags={message_type}),
                ]

                response = self.client.get(
                    "%s?%s"
                    % (
                        reverse("ferda:api:get_message_subtypes"),
                        urlencode({"message_types": message_type}, doseq=True),
                    )
                )

                self.assertEqual(
                    response.json(),
                    [
                        {"name": "subtype:1", "label": "Subtype 1", "tags": [message_type.value]},
                        {"name": "subtype:2", "label": "Subtype 2", "tags": [message_type.value]},
                    ],
                )
                self.assertEqual(secretary_mock.mock_calls[-1], call.get_template_collections(tags=[message_type]))


@tag("api", "messages")
@freeze_time("2000-01-01T00:00:00Z")
@override_settings(ROOT_URLCONF="ferda.tests.urls")
class ListMessagesTest(LoggerMixin, MessagesMixin, SnapshotTestCase):
    def setUp(self):
        super().setUp()
        user = User.objects.create_superuser("admin")  # type: ignore
        self.client.force_login(user)

    def test_list_emails_empty_query(self):
        self.secretary_client.render.side_effect = ["SUBJECT", "BODY"] * 2
        self.messages[MessageType.EMAIL].list.return_value = async_gen(
            [
                self.get_email_envelope(uid="message:1"),
                self.get_email_envelope(uid="message:2"),
            ]
        )

        response = self.client.get(
            reverse(
                "ferda:api:list_messages",
                kwargs={"message_type": MessageType.EMAIL.value},
            )
        )

        self.assertEqual(response.status_code, 200)
        self.assertMatchSnapshot(response.json())
        self.assertEqual(
            self.messages[MessageType.EMAIL].mock_calls,
            [
                call.list(
                    created_from=datetime(1970, 1, 1, tzinfo=pytz.utc),
                    created_to=datetime(2000, 1, 1, tzinfo=pytz.utc),
                    recipients=[],
                    body_templates=[],
                    types=[],
                    references=[],
                    limit=DEFAULT_PAGE_SIZE + 1,
                )
            ],
        )
        self.assertEqual(
            self.secretary_client.mock_calls,
            [
                call.render("subject_template", context={}),
                call.render("body_template", context={}),
                call.render("subject_template", context={}),
                call.render("body_template", context={}),
            ],
        )
        self.assertLog(
            LogEntryType.EMAIL_MESSAGE_LIST,
            LoggerResult.SUCCESS,
            input_properties={
                "time_begin_from": datetime(1970, 1, 1, tzinfo=pytz.utc).isoformat(),
                "time_begin_to": datetime(2000, 1, 1, tzinfo=pytz.utc).isoformat(),
                "recipients": [],
                "body_templates": [],
                "types": [],
                "page_size": str(DEFAULT_PAGE_SIZE),
                "offset": "0.0",
            },
            input_references={},
        )

    def test_list_emails_full_query(self):
        self.secretary_client.render.side_effect = ["SUBJECT", "BODY"]
        self.messages[MessageType.EMAIL].list.return_value = async_gen([self.get_email_envelope()])

        response = self.client.get(
            "%s?%s"
            % (
                reverse("ferda:api:list_messages", kwargs={"message_type": MessageType.EMAIL.value}),
                urlencode(
                    {
                        "time_begin_from": datetime(1990, 1, 1, tzinfo=pytz.utc),
                        "time_begin_to": datetime(1990, 1, 4, tzinfo=pytz.utc),
                        "offset": 24 * 60 * 60.0,
                        "recipients": ["kryton@example.com", "holly@example.com"],
                        "body_templates": ["body_template1", "body_template2"],
                        "types": ["type1", "type2"],
                        "references": ["contact.uid"],
                        "page_size": 4,
                    },
                    doseq=True,
                ),
            )
        )

        self.assertEqual(response.status_code, 200)
        self.assertEqual(
            self.messages[MessageType.EMAIL].mock_calls,
            [
                call.list(
                    created_from=datetime(1990, 1, 1, tzinfo=pytz.utc),
                    created_to=datetime(1990, 1, 3, tzinfo=pytz.utc),
                    recipients=["kryton@example.com", "holly@example.com"],
                    body_templates=["body_template1", "body_template2"],
                    types=["type1", "type2"],
                    references=[Reference(type="contact", value="uid")],
                    limit=5,
                )
            ],
        )
        self.assertLog(
            LogEntryType.EMAIL_MESSAGE_LIST,
            LoggerResult.SUCCESS,
            input_properties={
                "time_begin_from": datetime(1990, 1, 1, tzinfo=pytz.utc).isoformat(),
                "time_begin_to": datetime(1990, 1, 4, tzinfo=pytz.utc).isoformat(),
                "recipients": ["kryton@example.com", "holly@example.com"],
                "body_templates": ["body_template1", "body_template2"],
                "types": ["type1", "type2"],
                "page_size": "4",
                "offset": str(24 * 60 * 60.0),
            },
            input_references={"contact": ["uid"]},
        )

    def test_list_emails_pagination(self):
        self.secretary_client.render.side_effect = ["SUBJECT", "BODY"] * 5
        envelopes = [
            self.get_email_envelope(uid="message:%d" % i, create_datetime=datetime(1990, 1, i, tzinfo=pytz.utc))
            for i in range(1, 4)
        ]
        self.messages[MessageType.EMAIL].list.side_effect = [
            async_gen(envelopes[:3]),
            async_gen(envelopes[2:]),
        ]
        response = self.client.get(
            reverse(
                "ferda:api:list_messages",
                kwargs={"message_type": MessageType.EMAIL.value},
            )
            + "?page_size=2"
        )

        self.assertEqual(response.status_code, 200)
        self.assertMatchSnapshot(response.json())

        # Fetch next page
        next_url = response.json()["next"]
        response = self.client.get(next_url)

        self.assertEqual(response.status_code, 200)
        self.assertMatchSnapshot(response.json())


@tag("api", "messages")
@override_settings(ROOT_URLCONF="ferda.tests.urls")
class GetMessageTest(LoggerMixin, MessagesMixin, SnapshotTestCase):
    def setUp(self):
        super().setUp()
        user = User.objects.create_superuser("admin")  # type: ignore
        self.client.force_login(user)

        self.file = Mock(autospec=True)
        self.file.name = "filename.pdf"
        self.file.mimetype = "application/pdf"
        self.file.size = 12345

    def test_get_email(self):
        self.secretary_client.render.side_effect = ["SUBJECT", "BODY"]
        self.messages[MessageType.EMAIL].get.return_value = make_awaitable(self.get_email_envelope())

        response = self.client.get(
            reverse(
                "ferda:api:get_message",
                kwargs={"message_type": MessageType.EMAIL.value, "message_id": "message:1"},
            )
        )

        self.assertEqual(response.status_code, 200)
        self.assertMatchSnapshot(response.json())
        self.assertEqual(self.messages[MessageType.EMAIL].mock_calls, [call.get("message:1")])
        self.assertEqual(
            self.secretary_client.mock_calls,
            [
                call.render("subject_template", context={}),
                call.render("body_template", context={}),
            ],
        )
        self.assertLog(
            LogEntryType.EMAIL_MESSAGE_DETAIL,
            LoggerResult.SUCCESS,
            input_references={"message": "message:1"},
        )

    @patch("ferda.messages.api.FILE_STORAGE")
    def test_get_email_with_attachments(self, storage_mock):
        storage_mock.open.return_value = self.file
        self.secretary_client.render.side_effect = ["SUBJECT", "BODY"]
        self.messages[MessageType.EMAIL].get.return_value = make_awaitable(
            self.get_email_envelope(message={"attachments": [str(UUID(int=1))]})
        )

        response = self.client.get(
            reverse(
                "ferda:api:get_message",
                kwargs={"message_type": MessageType.EMAIL.value, "message_id": "message:1"},
            )
        )

        self.assertEqual(response.status_code, 200)
        self.assertMatchSnapshot(response.json())
        self.assertEqual(self.messages[MessageType.EMAIL].mock_calls, [call.get("message:1")])
        self.assertEqual(
            self.secretary_client.mock_calls,
            [
                call.render("subject_template", context={}),
                call.render("body_template", context={}),
            ],
        )
        self.assertLog(
            LogEntryType.EMAIL_MESSAGE_DETAIL,
            LoggerResult.SUCCESS,
            input_references={"message": "message:1"},
        )

    @patch("ferda.messages.api.FILE_STORAGE")
    def test_get_letter(self, storage_mock):
        storage_mock.open.return_value = self.file

        self.messages[MessageType.LETTER].get.return_value = make_awaitable(
            LetterEnvelope(
                uid="message:1",
                status=MessageStatus.SENT,
                message=Letter(
                    recipient=Address(name="Kryton", city="Red Dwarf", postal_code="JMC"),
                    file=str(UUID(int=1)),
                ),
            )
        )

        response = self.client.get(
            reverse(
                "ferda:api:get_message",
                kwargs={"message_type": MessageType.LETTER.value, "message_id": "message:1"},
            )
        )

        self.assertEqual(response.status_code, 200)
        self.assertMatchSnapshot(response.json())
        self.assertEqual(self.messages[MessageType.LETTER].mock_calls, [call.get("message:1")])
        self.assertEqual(storage_mock.mock_calls, [call.open(UUID(int=1))])
        self.assertLog(
            LogEntryType.LETTER_MESSAGE_DETAIL,
            LoggerResult.SUCCESS,
            input_references={"message": "message:1"},
        )

    def test_get_sms(self):
        self.secretary_client.render.return_value = "BODY"
        self.messages[MessageType.SMS].get.return_value = make_awaitable(
            self.get_sms_envelope(message={"body_template": "body_template"}, uid="message:1"),
        )

        response = self.client.get(
            reverse(
                "ferda:api:get_message",
                kwargs={"message_type": MessageType.SMS.value, "message_id": "message:1"},
            )
        )

        self.assertEqual(response.status_code, 200)
        self.assertMatchSnapshot(response.json())
        self.assertEqual(self.messages[MessageType.SMS].mock_calls, [call.get("message:1")])
        self.assertEqual(self.secretary_client.mock_calls, [call.render("body_template", context={})])
        self.assertLog(
            LogEntryType.SMS_MESSAGE_DETAIL,
            LoggerResult.SUCCESS,
            input_references={"message": "message:1"},
        )

    def test_get_not_found(self):
        for message_type in MessageType:
            with self.subTest(message_type=message_type):
                self.messages[message_type].get.side_effect = NoMessage

                response = self.client.get(
                    reverse(
                        "ferda:api:get_message",
                        kwargs={"message_type": message_type.value, "message_id": "message:1"},
                    )
                )

                self.assertEqual(response.json(), {"detail": "Message not found"})
                self.assertEqual(response.status_code, 404)


class FileStatTest(SimpleTestCase):
    """Test file_stat utility function."""

    def setUp(self):
        self.file = Mock(autospec=True)
        self.file.name = sentinel.filename
        self.file.mimetype = sentinel.mimetype
        self.file.size = sentinel.size
        self.uid = UUID(int=42)

    def test_file_stat_str(self):
        for uid in (self.uid, str(self.uid)):
            with self.subTest(uid=uid):
                with patch("ferda.messages.api.FILE_STORAGE") as mock:
                    mock.open.return_value = self.file
                    self.assertEqual(
                        file_stat(str(self.uid)),
                        {
                            "uuid": str(self.uid),
                            "name": sentinel.filename,
                            "mimetype": sentinel.mimetype,
                            "size": sentinel.size,
                        },
                    )
                self.assertEqual(mock.mock_calls, [call.open(self.uid)])

    def test_file_stat_file_not_found(self):
        with LogCapture("ferda.messages.api") as log_handler:
            with patch("ferda.messages.api.FILE_STORAGE") as mock:
                mock.open.side_effect = FileNotFoundError
                self.assertEqual(file_stat(str(self.uid)), {"uuid": str(self.uid), "error": "File not found"})
        log_handler.check(
            ("ferda.messages.api", "WARNING", "File %s was not found" % str(self.uid)),
        )

    def test_file_stat_connection_aborted(self):
        with LogCapture("ferda.messages.api") as log_handler:
            with patch("ferda.messages.api.FILE_STORAGE") as mock:
                mock.open.side_effect = ConnectionAbortedError
                self.assertEqual(file_stat(str(self.uid)), {"uuid": str(self.uid), "error": "Connection failed"})
        log_handler.check(
            ("ferda.messages.api", "ERROR", "Connection failed when getting file %s" % str(self.uid)),
        )
