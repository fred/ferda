from pathlib import Path
from typing import cast

from appsettings import AppSettings
from django.core.exceptions import ImproperlyConfigured, ValidationError
from django.test import SimpleTestCase, override_settings
from testfixtures import TempDirectory
from typist import HTTPTokenAuth

from ferda.registry.plugins.badges import AbstractBadger
from ferda.settings import (
    DEFAULT_STATE_FLAGS_DESCRIPTIONS,
    DictWithGrpcServicesSetting,
    SubclassSetting,
    TokenSetting,
    UpdatableDictSetting,
    timeout_validator,
)


class TimeoutValidatorTest(SimpleTestCase):
    def test_single_value(self):
        self.assertIsNone(timeout_validator(42))
        self.assertIsNone(timeout_validator(42.3))

    def test_tuple(self):
        self.assertIsNone(timeout_validator((42, 10)))
        self.assertIsNone(timeout_validator((42.3, 10.1)))
        self.assertIsNone(timeout_validator((42.5, 10)))
        self.assertIsNone(timeout_validator((10, 42.5)))

    def test_error(self):
        with self.assertRaises(ValidationError):
            timeout_validator("timeout")
        with self.assertRaises(ValidationError):
            timeout_validator((1, 2, 3))


class DummyBadger(AbstractBadger):
    pass


class DummyObject:
    pass


class DummySettings(AppSettings):
    updatabledict = UpdatableDictSetting(key_type=str, default=DEFAULT_STATE_FLAGS_DESCRIPTIONS)
    subclass = SubclassSetting(default=None, superclasses=(AbstractBadger,))
    grpc_services = DictWithGrpcServicesSetting(default=dict)

    class Meta:
        setting_prefix = "test_"


class TestUpdatableDictSetting(SimpleTestCase):
    def setUp(self):
        self.settings = DummySettings()

    def test_default(self):
        self.settings.check()
        self.assertEqual(self.settings.updatabledict, DEFAULT_STATE_FLAGS_DESCRIPTIONS)

    def test_update(self):
        override = {"linked": None, "emperor": "Long live the emperor!"}
        result = {}
        result.update(DEFAULT_STATE_FLAGS_DESCRIPTIONS)
        result.update(override)
        with override_settings(TEST_UPDATABLEDICT=override):
            self.settings.check()
            self.assertEqual(self.settings.updatabledict, result)


class TestSubclassSetting(SimpleTestCase):
    def setUp(self):
        self.settings = DummySettings()

    def test_empty(self):
        self.settings.check()
        self.assertEqual(self.settings.subclass, None)

    @override_settings(TEST_SUBCLASS="ferda.tests.test_settings.DummyBadger")
    def test_valid(self):
        self.settings.check()
        self.assertEqual(self.settings.subclass, DummyBadger)

    @override_settings(TEST_SUBCLASS="ferda.tests.test_settings.DummyObject")
    def test_invalid(self):
        with self.assertRaises(ImproperlyConfigured):
            self.settings.check()

    @override_settings(TEST_SUBCLASS="ferda.tests.test_settings.DummyObject")
    def test_no_superclass(self):
        self.settings.settings["subclass"].superclasses = []
        self.settings.check()
        self.assertEqual(self.settings.subclass, DummyObject)


class DictWithGrpcServicesSettingTest(SimpleTestCase):
    def setUp(self):
        self.settings = DummySettings()

    def test_empty(self):
        self.settings.check()
        self.assertEqual(self.settings.grpc_services, {})

    def test_valid_ssl_cert(self):
        with TempDirectory() as temp:
            temp.write("ssl_cert", b"XXX")
            for path in [Path(temp.path) / "ssl_cert", f"{temp.path}/ssl_cert", None]:
                with self.subTest(path=path):
                    with override_settings(TEST_GRPC_SERVICES={"service": {"NETLOC": "example.com", "SSL_CERT": path}}):
                        self.settings.check()
                        self.assertEqual(
                            self.settings.grpc_services,
                            {
                                "service": {"NETLOC": "example.com", "SSL_CERT": path},
                            },
                        )

    def test_valid_no_ssl_cert(self):
        with override_settings(TEST_GRPC_SERVICES={"service": {"NETLOC": "example.com"}}):
            self.settings.check()
            self.assertEqual(
                self.settings.grpc_services,
                {
                    "service": {"NETLOC": "example.com", "SSL_CERT": None},
                },
            )

    @override_settings(
        TEST_GRPC_SERVICES={
            "service": {"NETLOC": 123},
        }
    )
    def test_invalid_netloc_type(self):
        with self.assertRaisesMessage(ImproperlyConfigured, "Value must contain key NETLOC of type str"):
            self.settings.check()

    @override_settings(
        TEST_GRPC_SERVICES={
            "service": {"NETLOC": "example.com", "SSL_CERT": 123},
        }
    )
    def test_invalid_ssl_cert_type(self):
        with self.assertRaisesMessage(ImproperlyConfigured, "Key SSL_CERT of value must be of type str or Path"):
            self.settings.check()

    @override_settings(
        TEST_GRPC_SERVICES={
            "service": {"NETLOC": "example.com", "SSL_CERT": "/some/invalid/cert"},
        }
    )
    def test_invalid_file_path(self):
        with self.assertRaisesMessage(ImproperlyConfigured, "Value of SSL_CERT key must be path to a readable file"):
            self.settings.check()

    @override_settings(
        TEST_GRPC_SERVICES={
            "service": {"NETLOC": "example.com", "another_key": None},
        }
    )
    def test_invalid_extra_key(self):
        with self.assertRaisesMessage(ImproperlyConfigured, "Only NETLOC and SSL_CERT keys are allowed in value"):
            self.settings.check()


class TokenSettingTest(SimpleTestCase):
    def test_transform_empty(self):
        setting = TokenSetting()
        self.assertIsNone(setting.transform(None))

    def test_transform(self):
        setting = TokenSetting()
        token = "something_secret"
        auth = setting.transform(token)

        self.assertIsInstance(auth, HTTPTokenAuth)
        self.assertEqual(cast(HTTPTokenAuth, auth).token, token)
