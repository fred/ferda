"""Test utilities."""

from typing import AsyncGenerator, Awaitable, Iterable, TypeVar, cast
from unittest.mock import Mock, call, patch, sentinel
from urllib.parse import urlsplit, urlunsplit

import pluggy
from django.test import TestCase
from reporter import ReportClient, ReportManagerClient

import ferda.plugin.hookspec
from ferda.constants import HOOK_MARKER, LOGGER_SERVICE
from ferda.tests.constants import REPORT_SERVICE

T = TypeVar("T")


LOGGER_SESSION_ID = "LOGGER-SESSION-IDENTIFIER"


class LoggerMixin:
    """Mixin for tests with logger."""

    def setUp(self):
        super().setUp()  # type: ignore

        logger_patcher = patch("ferda.backend.LOGGER.client", autospec=True)
        cast(TestCase, self).addCleanup(logger_patcher.stop)
        self.logger = logger_patcher.start()
        self.logger.create_log_entry.return_value = sentinel.log_entry_id
        self.logger.create_session.return_value = LOGGER_SESSION_ID

    def assertNoLogs(self):
        cast(TestCase, self).assertEqual(self.logger.mock_calls, [call.create_session(1, "admin")])  # pragma: no cover

    def assertLog(
        self,
        log_entry_type,
        result,
        input_properties=None,
        references=None,
        content=None,
        input_references=None,
        output_references=None,
        output_properties=None,
    ):
        calls = [
            call.create_session(1, "admin"),
            call.create_log_entry(
                LOGGER_SERVICE,
                log_entry_type,
                source_ip="127.0.0.1",
                session_id=LOGGER_SESSION_ID,
                content=content,
                properties=input_properties,
                references=input_references,
            ),
            call.close_log_entry(
                sentinel.log_entry_id,
                result,
                session_id=LOGGER_SESSION_ID,
                content=None,
                properties=output_properties or {},
                references=output_references or references or {},
            ),
        ]
        cast(TestCase, self).assertEqual(self.logger.mock_calls, calls)


class ReportMixin:
    """Mixin for testing reports."""

    def setUp(self):
        super().setUp()  # type: ignore

        # Patch REPORTS
        self.report_client = Mock(spec=ReportClient)
        report_patcher = patch.dict("ferda.backend.reports.REPORTS", {REPORT_SERVICE: self.report_client}, clear=True)
        report_patcher.start()
        cast(TestCase, self).addCleanup(report_patcher.stop)

        # Patch REPORT_MANAGERS
        self.report_manager_client = Mock(spec=ReportManagerClient)
        report_manager_patcher = patch.dict(
            "ferda.backend.reports.REPORT_MANAGERS",
            {REPORT_SERVICE: self.report_manager_client},
            clear=True,
        )
        report_manager_patcher.start()
        cast(TestCase, self).addCleanup(report_manager_patcher.stop)


def make_awaitable(value: T) -> Awaitable[T]:
    async def _wrapper():
        return value

    return cast(Awaitable[T], _wrapper())


async def async_gen(iterable: Iterable[T]) -> AsyncGenerator[T, None]:
    for i in iterable:
        yield i


def url_strip_hostname(url: str) -> str:
    """Strip hostname and protocol from url."""
    parts = urlsplit(url)
    return urlunsplit(("", "", parts.path, parts.query, parts.fragment))


def get_plugin_manager() -> pluggy.PluginManager:
    """Get pluggy plugin mananager without any registered hookimpls."""
    pm = pluggy.PluginManager(HOOK_MARKER)
    pm.add_hookspecs(ferda.plugin.hookspec)
    return pm
