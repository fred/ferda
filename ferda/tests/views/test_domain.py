from django.test import TestCase, override_settings

from ferda.backend.exceptions import DomainDoesNotExist
from ferda.constants import LogEntryType

from .common import GenericDetailViewTest, GenericHistoryViewTest


@override_settings(ROOT_URLCONF="ferda.tests.urls")
class TestDomainDetailView(GenericDetailViewTest, TestCase):
    object_type = "domain"
    object_does_not_exist = DomainDoesNotExist
    log_entry_type = LogEntryType.DOMAIN_DETAIL


@override_settings(ROOT_URLCONF="ferda.tests.urls")
class TestDomainHistoryView(GenericHistoryViewTest, TestCase):
    object_type = "domain"
    log_entry_type = LogEntryType.DOMAIN_HISTORY
    object_does_not_exist = DomainDoesNotExist
    object_handle_name = "fqdn"
    object_history_id_name = "domain_history_id"
