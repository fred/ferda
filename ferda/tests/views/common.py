from datetime import datetime
from typing import Type, cast
from unittest.mock import patch
from uuid import UUID

import pytz
from django.test import TestCase
from django.urls import reverse
from grill import LogProperty

from ferda.backend.exceptions import FerdaBackendException
from ferda.constants import LogEntryType, LoggerResult
from ferda.models import User
from ferda.tests.utils import LoggerMixin


class GenericDetailViewTest(LoggerMixin):
    """Generic object detail view test."""

    object_type: str
    object_does_not_exist: Type[FerdaBackendException]
    log_entry_type: LogEntryType

    def setUp(self):
        super().setUp()

        self.user = User.objects.create_superuser("admin", "admin@example.com", "password")
        cast(TestCase, self).client.force_login(self.user)

    def test_get(self):
        with patch(f"ferda.backend.BACKEND.get_{self.object_type}_info") as info_mock:
            with patch(f"ferda.backend.BACKEND.get_{self.object_type}_history") as history_mock:
                info_mock.return_value = {"handle": "HANDLE"}
                history_mock.return_value = {
                    "timeline": [
                        {
                            f"{self.object_type}_history_id": UUID(int=40),
                            "valid_from": datetime(2017, 1, 1, tzinfo=pytz.utc),
                        },
                        {
                            f"{self.object_type}_history_id": UUID(int=41),
                            "valid_from": datetime(2018, 1, 1, tzinfo=pytz.utc),
                        },
                    ],
                    "valid_to": datetime(2019, 1, 1, tzinfo=pytz.utc),
                }
                response = cast(TestCase, self).client.get(
                    reverse(f"ferda:registry:{self.object_type}:detail", kwargs={"object_id": str(UUID(int=42))})
                )
        cast(TestCase, self).assertEqual(
            response.context["object_data"],
            {
                "object_info": {"handle": "HANDLE"},
                "object_uuid": UUID(int=42),
                "object_history_id": UUID(int=41),
                "object_history_datetime": datetime(2019, 1, 1, tzinfo=pytz.utc),
                "object_has_history": True,
            },
        )
        cast(TestCase, self).assertContains(response, "<title>Ferda</title>", html=True)
        self.assertLog(
            self.log_entry_type,
            LoggerResult.SUCCESS,
            input_properties={"uuid": LogProperty(str(UUID(int=42)), None)},
            references={self.object_type: str(UUID(int=42))},
        )

    def test_object_not_found(self):
        with patch(f"ferda.backend.BACKEND.get_{self.object_type}_history") as backend_mock:
            backend_mock.side_effect = self.object_does_not_exist
            response = cast(TestCase, self).client.get(
                reverse(f"ferda:registry:{self.object_type}:detail", kwargs={"object_id": str(UUID(int=42))})
            )
        cast(TestCase, self).assertEqual(response.status_code, 404)
        self.assertLog(
            self.log_entry_type,
            LoggerResult.FAIL,
            input_properties={"uuid": LogProperty(str(UUID(int=42)), None)},
        )


class GenericHistoryViewTest(LoggerMixin):
    """Generic object history view test."""

    object_type: str
    log_entry_type: LogEntryType
    object_handle_name: str
    object_history_id_name: str
    object_does_not_exist: Type[FerdaBackendException]

    def setUp(self):
        super().setUp()

        self.user = User.objects.create_superuser("admin", "admin@example.com", "password")
        cast(TestCase, self).client.force_login(self.user)

    def test_get(self):
        with patch(f"ferda.backend.BACKEND.get_{self.object_type}_info") as info_mock:
            with patch(f"ferda.backend.BACKEND.get_{self.object_type}_history") as history_mock:
                info_mock.return_value = {self.object_handle_name: "HARRY", "name": "Harry Potter"}
                history_mock.return_value = {
                    "timeline": [
                        {self.object_history_id_name: UUID(int=1)},
                        {self.object_history_id_name: UUID(int=2)},
                        {self.object_history_id_name: UUID(int=3)},
                    ]
                }
                response = cast(TestCase, self).client.get(
                    reverse(f"ferda:registry:{self.object_type}:history", kwargs={"object_id": str(UUID(int=42))})
                )
        cast(TestCase, self).assertEqual(response.status_code, 200)
        cast(TestCase, self).assertEqual(
            response.context["history_data"],
            {
                "object_uuid": UUID(int=42),
                "object_handle": "HARRY",
                "from_history_id": UUID(int=2),
                "to_history_id": UUID(int=3),
                "object_history": {
                    "timeline": [
                        {self.object_history_id_name: UUID(int=1)},
                        {self.object_history_id_name: UUID(int=2)},
                        {self.object_history_id_name: UUID(int=3)},
                    ]
                },
            },
        )
        self.assertLog(
            self.log_entry_type,
            LoggerResult.SUCCESS,
            input_properties={"uuid": LogProperty(str(UUID(int=42)), None)},
            references={self.object_type: str(UUID(int=42))},
        )

    def test_get_from_to(self):
        with patch(f"ferda.backend.BACKEND.get_{self.object_type}_info") as info_mock:
            with patch(f"ferda.backend.BACKEND.get_{self.object_type}_history") as history_mock:
                info_mock.return_value = {self.object_handle_name: "HARRY", "name": "Harry Potter"}
                history_mock.return_value = {
                    "timeline": [
                        {self.object_history_id_name: UUID(int=1)},
                        {self.object_history_id_name: UUID(int=2)},
                        {self.object_history_id_name: UUID(int=3)},
                    ]
                }
                response = cast(TestCase, self).client.get(
                    reverse(f"ferda:registry:{self.object_type}:history", kwargs={"object_id": str(UUID(int=42))}),
                    data={
                        "from_history_id": str(UUID(int=1)),
                        "to_history_id": str(UUID(int=2)),
                    },
                )
        cast(TestCase, self).assertEqual(response.status_code, 200)
        cast(TestCase, self).assertEqual(response.context["history_data"]["from_history_id"], UUID(int=1))
        cast(TestCase, self).assertEqual(response.context["history_data"]["to_history_id"], UUID(int=2))
        self.assertLog(
            self.log_entry_type,
            LoggerResult.SUCCESS,
            input_properties={"uuid": LogProperty(str(UUID(int=42)), None)},
            references={self.object_type: str(UUID(int=42))},
        )

    def test_history_object_not_found(self):
        with patch(f"ferda.backend.BACKEND.get_{self.object_type}_info") as info_mock:
            with patch(f"ferda.backend.BACKEND.get_{self.object_type}_history") as history_mock:
                info_mock.side_effect = self.object_does_not_exist
                history_mock.side_effect = self.object_does_not_exist
                response = cast(TestCase, self).client.get(
                    reverse(f"ferda:registry:{self.object_type}:history", kwargs={"object_id": str(UUID(int=42))})
                )
        cast(TestCase, self).assertEqual(response.status_code, 404)
        self.assertLog(
            self.log_entry_type,
            LoggerResult.FAIL,
            input_properties={"uuid": LogProperty(str(UUID(int=42)), None)},
        )

    def test_info_object_not_found(self):
        with patch(f"ferda.backend.BACKEND.get_{self.object_type}_info") as info_mock:
            with patch(f"ferda.backend.BACKEND.get_{self.object_type}_history") as history_mock:
                info_mock.side_effect = self.object_does_not_exist
                history_mock.return_value = {
                    "timeline": [
                        {self.object_history_id_name: UUID(int=1)},
                        {self.object_history_id_name: UUID(int=2)},
                    ]
                }
                response = cast(TestCase, self).client.get(
                    reverse(f"ferda:registry:{self.object_type}:history", kwargs={"object_id": str(UUID(int=42))})
                )
        cast(TestCase, self).assertEqual(response.status_code, 404)
        self.assertLog(
            self.log_entry_type,
            LoggerResult.FAIL,
            input_properties={"uuid": LogProperty(str(UUID(int=42)), None)},
        )

    def test_invalid_versions(self):
        with patch(f"ferda.backend.BACKEND.get_{self.object_type}_info") as info_mock:
            with patch(f"ferda.backend.BACKEND.get_{self.object_type}_history") as history_mock:
                info_mock.return_value = {self.object_handle_name: "HARRY", "name": "Harry Potter"}
                history_mock.return_value = {
                    "timeline": [
                        {self.object_history_id_name: UUID(int=1)},
                        {self.object_history_id_name: UUID(int=2)},
                    ]
                }
                response = cast(TestCase, self).client.get(
                    reverse(f"ferda:registry:{self.object_type}:history", kwargs={"object_id": str(UUID(int=42))}),
                    data={"from_history_id": UUID(int=2), "to_history_id": UUID(int=1)},
                )
        cast(TestCase, self).assertEqual(response.status_code, 404)
        self.assertLog(
            self.log_entry_type,
            LoggerResult.FAIL,
            input_properties={"uuid": LogProperty(str(UUID(int=42)), None)},
        )

    def test_no_history(self):
        with patch(f"ferda.backend.BACKEND.get_{self.object_type}_info") as info_mock:
            with patch(f"ferda.backend.BACKEND.get_{self.object_type}_history") as history_mock:
                info_mock.return_value = {self.object_handle_name: "HARRY", "name": "Harry Potter"}
                history_mock.return_value = {
                    "timeline": [
                        {self.object_history_id_name: UUID(int=1)},
                    ]
                }
                response = cast(TestCase, self).client.get(
                    reverse(f"ferda:registry:{self.object_type}:history", kwargs={"object_id": str(UUID(int=42))})
                )
        cast(TestCase, self).assertEqual(response.status_code, 404)
        self.assertLog(
            self.log_entry_type,
            LoggerResult.FAIL,
            input_properties={"uuid": LogProperty(str(UUID(int=42)), None)},
        )
