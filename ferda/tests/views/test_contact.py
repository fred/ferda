from django.test import TestCase, override_settings

from ferda.backend.exceptions import ContactDoesNotExist
from ferda.constants import LogEntryType

from .common import GenericDetailViewTest, GenericHistoryViewTest


@override_settings(ROOT_URLCONF="ferda.tests.urls")
class TestContactDetailView(GenericDetailViewTest, TestCase):
    object_type = "contact"
    object_does_not_exist = ContactDoesNotExist
    log_entry_type = LogEntryType.CONTACT_DETAIL


@override_settings(ROOT_URLCONF="ferda.tests.urls")
class TestContactHistoryView(GenericHistoryViewTest, TestCase):
    object_type = "contact"
    log_entry_type = LogEntryType.CONTACT_HISTORY
    object_does_not_exist = ContactDoesNotExist
    object_handle_name = "contact_handle"
    object_history_id_name = "contact_history_id"
