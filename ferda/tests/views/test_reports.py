from unittest.mock import sentinel

from django.contrib.auth.models import Permission
from django.test import TestCase, override_settings
from django.urls import reverse
from guardian.shortcuts import assign_perm, remove_perm

from ferda.models import Report, User
from ferda.tests.constants import REPORT_SERVICE
from ferda.tests.utils import LoggerMixin


@override_settings(
    ROOT_URLCONF="ferda.tests.urls",
    FERDA_REPORTS={REPORT_SERVICE: {"NETLOC": sentinel.netloc}},
)
class ReportListViewTest(LoggerMixin, TestCase):
    """Test ReportListView."""

    def setUp(self):
        super().setUp()

        self.user = User.objects.create_user("admin", "admin@example.com", "password")
        self.user.user_permissions.add(Permission.objects.get(codename="view_report"))
        self.report = Report.objects.create(service=REPORT_SERVICE, name="report", label="Report")
        self.report.parameters.create(name="days", type="integer", label="Days", is_input=True)
        self.report.parameters.create(name="fqdn", type="varchar", label="FQDN", is_input=False)
        self.report2 = Report.objects.create(service=REPORT_SERVICE, name="report2", label="Another report")

    def test_get(self):
        for reports in ([], [self.report], [self.report2, self.report]):
            with self.subTest(reports=reports):
                remove_perm("run_report", self.user, self.report)
                remove_perm("run_report", self.user, self.report2)
                for report in reports:
                    assign_perm("run_report", self.user, report)
                self.client.force_login(self.user)

                response = self.client.get(reverse("ferda:reports:list"))
                self.assertEqual(response.context["reports"], [report.to_dict() for report in reports])

    @override_settings(FERDA_REPORTS={"my_service": {"NETLOC": sentinel.netloc}})
    def test_get_unknown_service(self):
        report = Report.objects.create(service="my_service", name="report", label="Report")
        Report.objects.create(service="unknown_service", name="report", label="Report")
        assign_perm("run_report", self.user, report)
        self.client.force_login(self.user)

        response = self.client.get(reverse("ferda:reports:list"))

        self.assertEqual(response.context["reports"], [report.to_dict()])


@override_settings(ROOT_URLCONF="ferda.tests.urls")
class ReportDetailViewTest(LoggerMixin, TestCase):
    """Test ReportListView."""

    def setUp(self):
        super().setUp()

        self.user = User.objects.create_user("admin", "admin@example.com", "password")
        self.report = Report.objects.create(service=REPORT_SERVICE, name="report", label="Report")
        self.report.parameters.create(name="days", type="integer", label="Days", is_input=True)
        self.report.parameters.create(name="fqdn", type="varchar", label="FQDN", is_input=False)
        self.report2 = Report.objects.create(service=REPORT_SERVICE, name="report2", label="Another report")

        assign_perm("run_report", self.user, self.report)
        self.client.force_login(self.user)

    def test_get(self):
        response = self.client.get(
            reverse(
                "ferda:reports:detail",
                kwargs={"service": REPORT_SERVICE, "report_name": self.report.name},
            )
        )
        self.assertEqual(response.context["report"], self.report.to_dict())

    def test_not_found(self):
        response = self.client.get(
            reverse(
                "ferda:reports:detail",
                kwargs={"service": REPORT_SERVICE, "report_name": "wrong_report"},
            )
        )
        self.assertEqual(response.status_code, 404)
