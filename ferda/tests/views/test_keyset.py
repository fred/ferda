from django.test import TestCase, override_settings

from ferda.backend.exceptions import KeysetDoesNotExist
from ferda.constants import LogEntryType

from .common import GenericDetailViewTest, GenericHistoryViewTest


@override_settings(ROOT_URLCONF="ferda.tests.urls")
class TestKeysetDetailView(GenericDetailViewTest, TestCase):
    object_type = "keyset"
    object_does_not_exist = KeysetDoesNotExist
    log_entry_type = LogEntryType.KEYSET_DETAIL


@override_settings(ROOT_URLCONF="ferda.tests.urls")
class TestKeysetHistoryView(GenericHistoryViewTest, TestCase):
    object_type = "keyset"
    log_entry_type = LogEntryType.KEYSET_HISTORY
    object_does_not_exist = KeysetDoesNotExist
    object_handle_name = "keyset_handle"
    object_history_id_name = "keyset_history_id"
