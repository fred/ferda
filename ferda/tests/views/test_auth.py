from unittest.mock import patch

from django.contrib.auth import get_user_model
from django.contrib.auth.models import Permission
from django.test import TestCase, override_settings
from django.urls import reverse
from django_fido.constants import AUTHENTICATION_USER_SESSION_KEY

from ferda.tests.utils import LoggerMixin

User = get_user_model()


@override_settings(ROOT_URLCONF="ferda.tests.urls")
class LoginViewTest(LoggerMixin, TestCase):
    def setUp(self):
        super().setUp()
        self.login_url = reverse("ferda:login")

    @patch("ferda.views.auth.is_fido_backend_used", return_value=True)
    def test_form_valid_fido_backend(self, mock):
        user = User.objects.create_user(username="rimmer", password="gazpacho")
        response = self.client.post(self.login_url, {"username": "rimmer", "password": "gazpacho"})

        self.assertEqual(response.status_code, 302)
        self.assertEqual(self.client.session.get(AUTHENTICATION_USER_SESSION_KEY), user.pk)
        self.assertEqual(response.url, reverse("ferda:login-fido"))

    @patch("ferda.views.auth.is_fido_backend_used", return_value=False)
    def test_form_valid_non_fido_backend(self, mock):
        user = User.objects.create_user(username="rimmer", password="gazpacho")
        user.user_permissions.set(Permission.objects.filter(codename="can_view_registry"))

        with override_settings(LOGIN_REDIRECT_URL=reverse("ferda:registry:search:search")):
            response = self.client.post(self.login_url, {"username": "rimmer", "password": "gazpacho"}, follow=True)

        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.wsgi_request.path, reverse("ferda:registry:search:search"))
        self.assertTrue(response.wsgi_request.user.is_authenticated)

    @patch("ferda.views.auth.is_fido_backend_used", return_value=True)
    def test_form_valid_with_next_param(self, mock):
        user = User.objects.create_user(username="rimmer", password="gazpacho")
        response = self.client.post(self.login_url + "?next=/starbug/", {"username": "rimmer", "password": "gazpacho"})

        self.assertEqual(response.status_code, 302)
        self.assertEqual(self.client.session.get(AUTHENTICATION_USER_SESSION_KEY), user.pk)
        self.assertEqual(response.url, reverse("ferda:login-fido") + "?next=/starbug/")
