from django.test import TestCase, override_settings

from ferda.backend.exceptions import NssetDoesNotExist
from ferda.constants import LogEntryType

from .common import GenericDetailViewTest, GenericHistoryViewTest


@override_settings(ROOT_URLCONF="ferda.tests.urls")
class TestNssetDetailView(GenericDetailViewTest, TestCase):
    object_type = "nsset"
    object_does_not_exist = NssetDoesNotExist
    log_entry_type = LogEntryType.NSSET_DETAIL


@override_settings(ROOT_URLCONF="ferda.tests.urls")
class TestNssetHistoryView(GenericHistoryViewTest, TestCase):
    object_type = "nsset"
    log_entry_type = LogEntryType.NSSET_HISTORY
    object_does_not_exist = NssetDoesNotExist
    object_handle_name = "nsset_handle"
    object_history_id_name = "nsset_history_id"
