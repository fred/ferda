from django.test import TestCase, override_settings

from ferda.backend.exceptions import NssetDoesNotExist
from ferda.constants import LogEntryType

from .common import GenericInfoViewTest, GenericStateHistoryViewTest, GenericStateViewTest


@override_settings(ROOT_URLCONF="ferda.tests.urls")
class TestNssetInfoView(GenericInfoViewTest, TestCase):
    object_type = "nsset"
    object_does_not_exist = NssetDoesNotExist
    log_entry_type = LogEntryType.NSSET_DETAIL


@override_settings(ROOT_URLCONF="ferda.tests.urls")
class TestNssetStateView(GenericStateViewTest, TestCase):
    object_type = "nsset"
    object_does_not_exist = NssetDoesNotExist
    log_entry_type = LogEntryType.NSSET_STATE


@override_settings(ROOT_URLCONF="ferda.tests.urls")
class TestNssetStateHistoryView(GenericStateHistoryViewTest, TestCase):
    object_type = "nsset"
    object_does_not_exist = NssetDoesNotExist
    log_entry_type = LogEntryType.NSSET_STATE
