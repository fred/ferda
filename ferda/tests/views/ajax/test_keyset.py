from django.test import TestCase, override_settings

from ferda.backend.exceptions import KeysetDoesNotExist
from ferda.constants import LogEntryType

from .common import GenericInfoViewTest, GenericStateHistoryViewTest, GenericStateViewTest


@override_settings(ROOT_URLCONF="ferda.tests.urls")
class TestKeysetInfoView(GenericInfoViewTest, TestCase):
    object_type = "keyset"
    object_does_not_exist = KeysetDoesNotExist
    log_entry_type = LogEntryType.KEYSET_DETAIL


@override_settings(ROOT_URLCONF="ferda.tests.urls")
class TestKeysetStateView(GenericStateViewTest, TestCase):
    object_type = "keyset"
    object_does_not_exist = KeysetDoesNotExist
    log_entry_type = LogEntryType.KEYSET_STATE


@override_settings(ROOT_URLCONF="ferda.tests.urls")
class TestKeysetStateHistoryView(GenericStateHistoryViewTest, TestCase):
    object_type = "keyset"
    object_does_not_exist = KeysetDoesNotExist
    log_entry_type = LogEntryType.KEYSET_STATE
