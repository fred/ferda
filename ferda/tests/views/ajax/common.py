from datetime import datetime
from typing import Type, cast
from unittest.mock import call, patch
from uuid import UUID

from django.test import TestCase, override_settings
from django.urls import reverse
from grill import LogProperty
from pytz import utc

from ferda.backend.exceptions import FerdaBackendException, InvalidHistoryInterval
from ferda.constants import LogEntryType, LoggerResult
from ferda.models import User
from ferda.tests.utils import LoggerMixin


class GenericInfoViewTest(LoggerMixin):
    """Generic object info view test."""

    object_type: str
    object_does_not_exist: Type[FerdaBackendException]
    log_entry_type: LogEntryType

    def setUp(self):
        super().setUp()

        self.user = User.objects.create_superuser("admin", "admin@example.com", "password")
        cast(TestCase, self).client.force_login(self.user)

    def test_get(self):
        with patch(f"ferda.backend.BACKEND.get_{self.object_type}_info") as backend_mock:
            backend_mock.return_value = {"handle": "HANDLE"}
            response = cast(TestCase, self).client.get(
                reverse(f"ferda:registry:{self.object_type}:ajax_info", kwargs={"object_id": UUID(int=42)})
            )
        cast(TestCase, self).assertEqual(response.status_code, 200)
        cast(TestCase, self).assertJSONEqual(
            response.content,
            {
                "handle": "HANDLE",
            },
        )
        self.assertLog(
            self.log_entry_type,
            LoggerResult.SUCCESS,
            input_properties={"uuid": LogProperty(str(UUID(int=42)), None)},
            references={self.object_type: str(UUID(int=42))},
        )

    def test_get_history(self):
        with patch(f"ferda.backend.BACKEND.get_{self.object_type}_info") as backend_mock:
            backend_mock.return_value = {"handle": "HANDLE"}
            response = cast(TestCase, self).client.get(
                reverse(
                    f"ferda:registry:{self.object_type}:ajax_info",
                    kwargs={"object_id": UUID(int=42), "history_id": UUID(int=42)},
                )
            )
        cast(TestCase, self).assertEqual(response.status_code, 200)
        cast(TestCase, self).assertJSONEqual(response.content, {"handle": "HANDLE"})
        properties = {
            "uuid": LogProperty(str(UUID(int=42)), None),
            "history_id": LogProperty(str(UUID(int=42)), None),
        }
        self.assertLog(
            self.log_entry_type,
            LoggerResult.SUCCESS,
            input_properties=properties,
            references={self.object_type: str(UUID(int=42))},
        )

    def test_object_not_found(self):
        with patch(f"ferda.backend.BACKEND.get_{self.object_type}_info") as backend_mock:
            backend_mock.side_effect = self.object_does_not_exist
            response = cast(TestCase, self).client.get(
                reverse(
                    f"ferda:registry:{self.object_type}:ajax_info",
                    kwargs={"object_id": UUID(int=42), "history_id": UUID(int=42)},
                )
            )
        cast(TestCase, self).assertEqual(response.status_code, 404)
        properties = {
            "uuid": LogProperty(str(UUID(int=42)), None),
            "history_id": LogProperty(str(UUID(int=42)), None),
        }
        self.assertLog(
            self.log_entry_type,
            LoggerResult.FAIL,
            input_properties=properties,
        )

    def test_get_history_by_datetime(self):
        with patch(f"ferda.backend.BACKEND.get_{self.object_type}_history") as history_mock:
            with patch(f"ferda.backend.BACKEND.get_{self.object_type}_info") as info_mock:
                info_mock.return_value = {"handle": "HANDLE"}
                history_mock.return_value = {
                    "timeline": [
                        {
                            f"{self.object_type}_history_id": UUID(int=44),
                            "valid_from": datetime(2018, 1, 1, 0, 0, 0, tzinfo=utc),
                        },
                        {
                            f"{self.object_type}_history_id": UUID(int=45),
                            "valid_from": datetime(2020, 1, 1, 0, 0, 0, tzinfo=utc),
                        },
                    ],
                    "valid_to": None,
                }
                response = cast(TestCase, self).client.get(
                    reverse(
                        f"ferda:registry:{self.object_type}:ajax_info_by_datetime",
                        kwargs={"object_id": UUID(int=42), "history_id": datetime(2019, 1, 1, 0, 0, 0, tzinfo=utc)},
                    )
                )

        cast(TestCase, self).assertEqual(response.status_code, 200)
        cast(TestCase, self).assertJSONEqual(response.content, {"handle": "HANDLE"})
        info_call_kwargs = {
            f"{self.object_type}_id": UUID(int=42),
            "history_id": UUID(int=44),
        }
        cast(TestCase, self).assertEqual(info_mock.mock_calls, [call(**info_call_kwargs)])
        history_call_kwargs = {
            f"{self.object_type}_id": UUID(int=42),
            "start": datetime(2019, 1, 1, 0, 0, 0, tzinfo=utc),
            "end": datetime(2019, 1, 1, 0, 0, 0, tzinfo=utc),
        }
        cast(TestCase, self).assertEqual(history_mock.mock_calls, [call(**history_call_kwargs)])
        properties = {
            "uuid": LogProperty(str(UUID(int=42)), None),
            "history_datetime": LogProperty(str(datetime(2019, 1, 1, 0, 0, 0, tzinfo=utc)), None),
        }
        self.assertLog(
            self.log_entry_type,
            LoggerResult.SUCCESS,
            input_properties=properties,
            references={self.object_type: str(UUID(int=42))},
        )

    def test_invalid_history_interval(self):
        with patch(f"ferda.backend.BACKEND.get_{self.object_type}_history") as history_mock:
            history_mock.side_effect = InvalidHistoryInterval
            response = cast(TestCase, self).client.get(
                reverse(
                    f"ferda:registry:{self.object_type}:ajax_info_by_datetime",
                    kwargs={"object_id": UUID(int=42), "history_id": datetime(2019, 1, 1, tzinfo=utc)},
                )
            )
        cast(TestCase, self).assertEqual(response.status_code, 404)
        properties = {
            "uuid": LogProperty(str(UUID(int=42)), None),
            "history_datetime": LogProperty(str(datetime(2019, 1, 1, tzinfo=utc)), None),
        }
        self.assertLog(
            self.log_entry_type,
            LoggerResult.FAIL,
            input_properties=properties,
        )


class GenericStateViewTest(LoggerMixin):
    """Generic object state view test."""

    object_type: str
    object_does_not_exist: Type[FerdaBackendException]
    log_entry_type: LogEntryType

    def setUp(self):
        super().setUp()

        self.user = User.objects.create_superuser("admin", "admin@example.com", "password")
        cast(TestCase, self).client.force_login(self.user)

    @override_settings(
        FERDA_STATE_FLAGS_GROUPS=[],
        FERDA_STATE_FLAGS_DESCRIPTIONS={"linked": "Linked", "serverBlocked": None},
    )
    def test_get(self):
        with patch(f"ferda.backend.BACKEND.get_{self.object_type}_state") as backend_mock:
            backend_mock.return_value = {"linked": True, "serverBlocked": False}
            response = cast(TestCase, self).client.get(
                reverse(f"ferda:registry:{self.object_type}:ajax_state", kwargs={"object_id": UUID(int=42)})
            )
        cast(TestCase, self).assertJSONEqual(
            response.content,
            {
                "flags": {
                    "linked": {"active": True, "description": "Linked"},
                },
                "groups": [],
            },
        )
        self.assertLog(
            self.log_entry_type,
            LoggerResult.SUCCESS,
            input_properties={"uuid": LogProperty(str(UUID(int=42)), None)},
            references={self.object_type: str(UUID(int=42))},
        )

    def test_object_not_found(self):
        with patch(f"ferda.backend.BACKEND.get_{self.object_type}_state") as backend_mock:
            backend_mock.side_effect = self.object_does_not_exist
            response = cast(TestCase, self).client.get(
                reverse(f"ferda:registry:{self.object_type}:ajax_state", kwargs={"object_id": UUID(int=42)})
            )
        cast(TestCase, self).assertEqual(response.status_code, 404)
        self.assertLog(
            self.log_entry_type,
            LoggerResult.FAIL,
            input_properties={"uuid": LogProperty(str(UUID(int=42)), None)},
        )


class GenericStateHistoryViewTest(LoggerMixin):
    """Generic object state history view test."""

    object_type: str
    object_does_not_exist: Type[FerdaBackendException]
    log_entry_type: LogEntryType

    def setUp(self):
        super().setUp()

        self.user = User.objects.create_superuser("admin", "admin@example.com", "password")
        cast(TestCase, self).client.force_login(self.user)

    def test_get_unfinished_history(self):
        with patch(f"ferda.backend.BACKEND.get_{self.object_type}_state_history") as backend_mock:
            backend_mock.return_value = {
                "history": {
                    "timeline": [
                        {
                            "valid_from": datetime(2019, 1, 1, 12, 0, 0, tzinfo=utc),
                            "flags": {"linked": True, "serverBlocked": False, "deleteCandidate": True},
                        },
                        {
                            "valid_from": datetime(2019, 1, 2, 13, 0, 0, tzinfo=utc),
                            "flags": {"linked": True, "serverBlocked": True, "deleteCandidate": False},
                        },
                        {
                            "valid_from": datetime(2019, 1, 3, 14, 0, 0, tzinfo=utc),
                            "flags": {"linked": True, "serverBlocked": True, "deleteCandidate": False},
                        },
                        {
                            "valid_from": datetime(2019, 1, 4, 15, 0, 0, tzinfo=utc),
                            "flags": {"linked": True, "serverBlocked": True, "deleteCandidate": True},
                        },
                    ]
                }
            }
            response = cast(TestCase, self).client.get(
                reverse(f"ferda:registry:{self.object_type}:ajax_state_history", kwargs={"object_id": UUID(int=42)})
            )
        cast(TestCase, self).assertJSONEqual(
            response.content,
            {
                "linked": [["2019-01-01T12:00:00Z", None]],
                "serverBlocked": [["2019-01-02T13:00:00Z", None]],
                "deleteCandidate": [["2019-01-01T12:00:00Z", "2019-01-02T13:00:00Z"], ["2019-01-04T15:00:00Z", None]],
            },
        )
        properties = {"uuid": LogProperty(str(UUID(int=42)), None), "history": LogProperty("true", None)}
        self.assertLog(
            self.log_entry_type,
            LoggerResult.SUCCESS,
            input_properties=properties,
            references={self.object_type: str(UUID(int=42))},
        )

    def test_get_finished_history(self):
        with patch(f"ferda.backend.BACKEND.get_{self.object_type}_state_history") as backend_mock:
            backend_mock.return_value = {
                "history": {
                    "timeline": [
                        {
                            "valid_from": datetime(2019, 1, 1, 12, 0, 0, tzinfo=utc),
                            "flags": {"linked": True, "serverBlocked": False, "deleteCandidate": False},
                        }
                    ],
                    "valid_to": datetime(2019, 1, 2, 13, 0, 0, tzinfo=utc),
                }
            }
            response = cast(TestCase, self).client.get(
                reverse(f"ferda:registry:{self.object_type}:ajax_state_history", kwargs={"object_id": UUID(int=42)})
            )
        cast(TestCase, self).assertJSONEqual(
            response.content,
            {
                "linked": [["2019-01-01T12:00:00Z", "2019-01-02T13:00:00Z"]],
                "serverBlocked": [],
                "deleteCandidate": [],
            },
        )
        properties = {"uuid": LogProperty(str(UUID(int=42)), None), "history": LogProperty("true", None)}
        self.assertLog(
            self.log_entry_type,
            LoggerResult.SUCCESS,
            input_properties=properties,
            references={self.object_type: str(UUID(int=42))},
        )

    def test_object_not_found(self):
        with patch(f"ferda.backend.BACKEND.get_{self.object_type}_state_history") as backend_mock:
            backend_mock.side_effect = self.object_does_not_exist
            response = cast(TestCase, self).client.get(
                reverse(f"ferda:registry:{self.object_type}:ajax_state_history", kwargs={"object_id": UUID(int=42)})
            )
        cast(TestCase, self).assertEqual(response.status_code, 404)
        properties = {"uuid": LogProperty(str(UUID(int=42)), None), "history": LogProperty("true", None)}
        self.assertLog(
            self.log_entry_type,
            LoggerResult.FAIL,
            input_properties=properties,
        )
