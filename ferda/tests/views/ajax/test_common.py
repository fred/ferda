from django.test import TestCase, override_settings
from django.urls import reverse

from ferda.models import User
from ferda.tests.utils import LoggerMixin


@override_settings(ROOT_URLCONF="ferda.tests.urls")
class TestCountriesList(LoggerMixin, TestCase):
    def setUp(self):
        super().setUp()
        self.user = User.objects.create_superuser("admin", "admin@example.com", "password")
        self.client.force_login(self.user)

    def test_get(self):
        response = self.client.get(reverse("ferda:countries"))
        self.assertJSONEqual(response.content, {"CZ": "Czechia", "GB": "United Kingdom"})
