import json
from decimal import Decimal
from unittest.mock import call
from uuid import UUID

from django.test import SimpleTestCase, TestCase, override_settings
from django.urls import reverse
from django.utils import translation
from grill import LogProperty
from guardian.shortcuts import assign_perm
from reporter import AmbiguousColumnNames, Report as DbReport
from reporter.report import Param

from ferda.constants import LogEntryType, LoggerResult
from ferda.models import Report, User
from ferda.tests.constants import REPORT_SERVICE
from ferda.tests.utils import LoggerMixin, ReportMixin
from ferda.views.ajax.reports import format_row


@override_settings(USE_L10N=True, USE_THOUSAND_SEPARATOR=False)
class FormatNumbersTest(SimpleTestCase):
    def test_format_row(self):
        self.assertEqual(format_row({0: Decimal("3.14"), 1: "string", 2: 12345}), {0: "3.14", 1: "string", 2: "12345"})

        with translation.override("cs"):
            self.assertEqual(format_row({0: Decimal("3.14")}), {0: "3,14"})

        with override_settings(USE_THOUSAND_SEPARATOR=True):
            self.assertEqual(format_row({0: Decimal("3141592.65"), 1: 12345}), {0: "3,141,592.65", 1: "12,345"})
            with translation.override("cs"):
                # Czech format uses no-brake spaces
                self.assertEqual(
                    format_row({0: Decimal("3141592.65"), 1: 12345}), {0: "3\u00a0141\u00a0592,65", 1: "12\u00a0345"}
                )


@override_settings(ROOT_URLCONF="ferda.tests.urls")
class ReportRunViewTest(LoggerMixin, ReportMixin, TestCase):
    def setUp(self):
        super().setUp()
        self.user = User.objects.create_user("admin", "admin@example.com", "password")
        report = Report.objects.create(service=REPORT_SERVICE, name="domains_to_expire")
        assign_perm("run_report", self.user, report)
        self.client.force_login(self.user)

        self.report = DbReport(
            name="domains_to_expire",
            input_parameters=[Param("days", "int"), Param("warning_sent", "boolean")],
            output_parameters=[
                Param("uuid", "uuid"),
                Param("fqdn", "varchar"),
                Param("exdate", "date"),
                Param("rating", "numeric"),
            ],
        )
        self.result_rows = [
            {"uuid": str(UUID(int=1)), "fqdn": "example.com", "exdate": "2020-01-01", "rating": Decimal("3.14")},
            {"uuid": str(UUID(int=2)), "fqdn": "example.org", "exdate": "2020-02-02", "rating": Decimal("7.00")},
        ]

    def test_run_success(self):
        self.report_client.get_report.return_value = self.report
        self.report_client.run_report.return_value = iter(self.result_rows)

        response = self.client.post(
            reverse("ferda:reports:ajax_run", kwargs={"service": REPORT_SERVICE, "report_name": "domains_to_expire"}),
            data={"warning_sent": False, "days": 30},
            content_type="application/json",
        )

        self.assertEqual(response["Content-Type"], "text/plain")
        self.assertEqual(
            b"".join(response.streaming_content),
            "{}\n{}\n".format(
                json.dumps(format_row(self.result_rows[0])),
                json.dumps(format_row(self.result_rows[1])),
            ).encode("utf-8"),
        )
        self.assertLog(
            LogEntryType.REPORT_RUN,
            LoggerResult.SUCCESS,
            input_properties={
                "service": LogProperty(REPORT_SERVICE, None),
                "report": LogProperty("domains_to_expire", None),
                "args": [LogProperty("30", None), LogProperty("False", None)],
            },
        )
        self.assertEqual(
            self.report_client.mock_calls,
            [
                call.get_report("domains_to_expire"),
                call.run_report("domains_to_expire", 30, False),
            ],
        )

    def test_not_found_in_db(self):
        response = self.client.post(
            reverse(
                "ferda:reports:ajax_run",
                kwargs={"service": REPORT_SERVICE, "report_name": "undefined_report"},
            )
        )

        self.assertEqual(response.status_code, 404)

    def test_not_found_in_backend(self):
        report = Report.objects.create(service=REPORT_SERVICE, name="undefined_report")
        assign_perm("run_report", self.user, report)
        self.client.force_login(self.user)
        self.report_client.get_report.return_value = None

        response = self.client.post(
            reverse(
                "ferda:reports:ajax_run",
                kwargs={"service": REPORT_SERVICE, "report_name": "undefined_report"},
            )
        )

        self.assertEqual(response.status_code, 404)
        self.assertEqual(
            self.report_client.mock_calls,
            [
                call.get_report("undefined_report"),
            ],
        )

    def test_missing_arg(self):
        self.report_client.get_report.return_value = self.report

        response = self.client.post(
            reverse("ferda:reports:ajax_run", kwargs={"service": REPORT_SERVICE, "report_name": "domains_to_expire"}),
            data={"warning_sent": False},
            content_type="application/json",
        )

        self.assertEqual(response.status_code, 400)
        self.assertEqual(response.json(), {"errors": {"days": ["Argument is missing."]}})

        self.assertEqual(
            self.report_client.mock_calls,
            [
                call.get_report("domains_to_expire"),
            ],
        )

    def test_ambiguous_column_names(self):
        self.report_client.get_report.return_value = self.report
        self.report_client.run_report.side_effect = AmbiguousColumnNames

        with self.assertRaises(AmbiguousColumnNames):
            self.client.post(
                reverse(
                    "ferda:reports:ajax_run", kwargs={"service": REPORT_SERVICE, "report_name": "domains_to_expire"}
                ),
                data={"warning_sent": False, "days": 30},
                content_type="application/json",
            )

        self.assertLog(
            LogEntryType.REPORT_RUN,
            LoggerResult.FAIL,
            input_properties={
                "service": LogProperty(REPORT_SERVICE, None),
                "report": LogProperty("domains_to_expire", None),
                "args": [LogProperty("30", None), LogProperty("False", None)],
            },
        )
        self.assertEqual(
            self.report_client.mock_calls,
            [
                call.get_report("domains_to_expire"),
                call.run_report("domains_to_expire", 30, False),
            ],
        )
