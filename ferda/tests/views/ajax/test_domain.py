from unittest import skip

from django.test import TestCase, override_settings

from ferda.backend.exceptions import DomainDoesNotExist
from ferda.constants import LogEntryType

from .common import GenericInfoViewTest, GenericStateHistoryViewTest, GenericStateViewTest


@override_settings(ROOT_URLCONF="ferda.tests.urls")
class TestDomainInfoView(GenericInfoViewTest, TestCase):
    object_type = "domain"
    object_does_not_exist = DomainDoesNotExist
    log_entry_type = LogEntryType.DOMAIN_DETAIL

    @skip("Domain info is not accessible by datetime yet")
    def test_get_history_by_datetime(self):
        """Skipped test."""

    @skip("Domain info is not accessible by datetime yet")
    def test_invalid_history_interval(self):
        """Skipped test."""


@override_settings(ROOT_URLCONF="ferda.tests.urls")
class TestDomainStateView(GenericStateViewTest, TestCase):
    object_type = "domain"
    object_does_not_exist = DomainDoesNotExist
    log_entry_type = LogEntryType.DOMAIN_STATE


@override_settings(ROOT_URLCONF="ferda.tests.urls")
class TestDomainStateHistoryView(GenericStateHistoryViewTest, TestCase):
    object_type = "domain"
    object_does_not_exist = DomainDoesNotExist
    log_entry_type = LogEntryType.DOMAIN_STATE
