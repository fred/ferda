import json
from typing import cast
from unittest.mock import patch

from django.db import OperationalError, connection
from django.http import JsonResponse
from django.test import TestCase, override_settings, tag
from django.urls import reverse


class ErrorInMonitoringResponseMixin:
    """Mixin to be able to assert an error in JsonResponse in the monitoring view."""

    def _assertTestErrorInMonitoringResponse(self, response, test_name):
        """Assert JsonResponse with summary set to False and an error inside of a test."""
        cast(TestCase, self).assertIsInstance(response, JsonResponse)
        cast(TestCase, self).assertEqual(response.status_code, 200)
        cast(TestCase, self).assertFalse(json.loads(response.content)["summary"])
        cast(TestCase, self).assertFalse(json.loads(response.content)[test_name]["status"])


@tag("api")
@override_settings(ROOT_URLCONF="ferda.tests.urls")
class TestMonitoringView(ErrorInMonitoringResponseMixin, TestCase):
    @patch("ferda.views.ajax.monitoring.connection.cursor", return_value=connection)
    def test_get_success(self, cursor_mock):
        """Test response when every component is all right."""
        response = self.client.get(reverse("ferda:monitoring"))
        self.assertIsInstance(response, JsonResponse)
        self.assertEqual(response.status_code, 200)
        self.assertJSONEqual(
            response.content,
            {
                "test_ferda_database": {"status": True, "extra_data": {}},
                "summary": True,
            },
        )

    @patch("ferda.views.ajax.monitoring.connection.cursor", side_effect=OperationalError)
    def test_ferda_database_error(self, cursor_mock):
        """Test response when database is unavailable."""
        response = self.client.get("{}?test=test_ferda_database".format(reverse("ferda:monitoring")))
        self._assertTestErrorInMonitoringResponse(response, "test_ferda_database")
        self.assertEqual(json.loads(response.content)["test_ferda_database"]["extra_data"], {})
