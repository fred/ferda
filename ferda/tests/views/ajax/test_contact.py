from datetime import datetime
from uuid import UUID

import pytz
from django.test import TestCase, override_settings
from django_countries.fields import Country

from ferda.backend.exceptions import ContactDoesNotExist
from ferda.constants import LogEntryType

from .common import GenericInfoViewTest, GenericStateHistoryViewTest, GenericStateViewTest

CONTACT_REPRESENTATIVE_DATA = {
    "contact_uuid": UUID(int=1),
    "organization": "Sherlock Holmes",
    "street1": "221B Baker Street",
    "city": "London",
    "postal_code": "NW1 6XE",
    "country": Country("GB"),
    "valid_from": datetime(2020, 1, 1, tzinfo=pytz.utc),
}


@override_settings(ROOT_URLCONF="ferda.tests.urls")
class TestContactInfoView(GenericInfoViewTest, TestCase):
    object_type = "contact"
    object_does_not_exist = ContactDoesNotExist
    log_entry_type = LogEntryType.CONTACT_DETAIL


@override_settings(ROOT_URLCONF="ferda.tests.urls")
class TestContactStateView(GenericStateViewTest, TestCase):
    object_type = "contact"
    object_does_not_exist = ContactDoesNotExist
    log_entry_type = LogEntryType.CONTACT_STATE


@override_settings(ROOT_URLCONF="ferda.tests.urls")
class TestContactStateHistoryView(GenericStateHistoryViewTest, TestCase):
    object_type = "contact"
    object_does_not_exist = ContactDoesNotExist
    log_entry_type = LogEntryType.CONTACT_STATE
