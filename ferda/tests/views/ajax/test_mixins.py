from datetime import datetime
from unittest.mock import call, patch
from uuid import UUID

from django.test import SimpleTestCase

from ferda.views.ajax.mixins import RelatedContactsMixin


class TestRelatedContactsMixin(SimpleTestCase):
    contact_info = {
        "contact_id": UUID(int=42),
        "contact_handle": "IDAHO",
        "name": "Duncan Idaho",
    }

    def test_get_related_contacts_overview_empty(self):
        self.assertEqual(RelatedContactsMixin.get_related_contacts_overview([]), [])

    @patch("ferda.backend.client.BACKEND.get_contact_info_future", autospec=True)
    def test_get_related_contacts_overview_current(self, info_mock):
        info_mock.return_value.result.return_value = self.contact_info

        self.assertEqual(RelatedContactsMixin.get_related_contacts_overview([UUID(int=42)]), [self.contact_info])
        self.assertEqual(
            info_mock.mock_calls,
            [
                call(UUID(int=42), history_id=None),
                call().result(),
            ],
        )

    @patch("ferda.backend.client.BACKEND.get_contact_history_id_by_datetime_future", autospec=True)
    @patch("ferda.backend.client.BACKEND.get_contact_info_future", autospec=True)
    def test_get_related_contacts_overview_history(self, info_mock, history_id_mock):
        info_mock.return_value.result.return_value = self.contact_info
        history_id_mock.return_value.result.return_value = UUID(int=43)

        self.assertEqual(
            RelatedContactsMixin.get_related_contacts_overview([UUID(int=42)], datetime(2020, 1, 1)),
            [self.contact_info],
        )
        self.assertEqual(
            history_id_mock.mock_calls,
            [
                call(UUID(int=42), history_datetime=datetime(2020, 1, 1)),
                call().result(),
            ],
        )
        self.assertEqual(
            info_mock.mock_calls,
            [
                call(UUID(int=42), history_id=UUID(int=43)),
                call().result(),
            ],
        )
