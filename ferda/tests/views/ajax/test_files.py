from unittest.mock import patch
from uuid import UUID

from django.test import TestCase, override_settings
from django.urls import reverse
from grill import LogProperty

from ferda.constants import LogEntryType, LoggerResult
from ferda.models import User
from ferda.tests.utils import LoggerMixin


@override_settings(ROOT_URLCONF="ferda.tests.urls")
class FileReadViewTest(LoggerMixin, TestCase):
    def setUp(self):
        super().setUp()
        self.user = User.objects.create_superuser("admin", "admin@example.com", "password")
        self.client.force_login(self.user)

    @patch("ferda.backend.FILEMAN.read", autospec=True)
    @patch("ferda.backend.FILEMAN.stat", autospec=True)
    def test_get(self, stat_mock, read_mock):
        stat_mock.return_value = {
            "name": "attachment.txt",
            "mimetype": "text/plain",
            "size": 31,
        }
        read_mock.return_value = [b"all your ", b"base are", b" belong to us\n"]

        response = self.client.get(reverse("ferda:files:read", kwargs={"file_uuid": UUID(int=1)}))

        self.assertEqual(response["Content-Type"], "text/plain")
        self.assertEqual(response["Content-Disposition"], 'attachment; filename="attachment.txt"')
        self.assertEqual(b"".join(response.streaming_content), b"all your base are belong to us\n")
        self.assertLog(
            LogEntryType.FILE_READ,
            LoggerResult.SUCCESS,
            input_properties={"uuid": LogProperty(str(UUID(int=1)), None)},
        )

    @patch("ferda.backend.FILEMAN.stat", autospec=True)
    def test_not_found(self, stat_mock):
        stat_mock.side_effect = FileNotFoundError()
        response = self.client.get(reverse("ferda:files:read", kwargs={"file_uuid": UUID(int=1)}))
        self.assertEqual(response.status_code, 404)
        self.assertLog(
            LogEntryType.FILE_READ,
            LoggerResult.FAIL,
            input_properties={"uuid": LogProperty(str(UUID(int=1)), None)},
        )

    @patch("ferda.backend.FILEMAN.stat", autospec=True)
    def test_connection_aborted(self, stat_mock):
        stat_mock.side_effect = ConnectionAbortedError()
        response = self.client.get(reverse("ferda:files:read", kwargs={"file_uuid": UUID(int=1)}))
        self.assertEqual(response.status_code, 500)
        self.assertLog(
            LogEntryType.FILE_READ,
            LoggerResult.ERROR,
            input_properties={"uuid": LogProperty(str(UUID(int=1)), None)},
        )
