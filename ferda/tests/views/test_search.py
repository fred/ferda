from django.test import TestCase, override_settings
from django.urls import reverse

from ferda.models import User
from ferda.tests.utils import LoggerMixin


@override_settings(ROOT_URLCONF="ferda.tests.urls")
class TestSearchView(LoggerMixin, TestCase):
    """Test SearchView."""

    def setUp(self):
        super().setUp()

        self.user = User.objects.create_superuser("admin", "admin@example.com", "password")
        self.client.force_login(self.user)

    def test_get(self):
        response = self.client.get(
            reverse("ferda:registry:search:search"),
            data={"query": ["harry"], "history": "last_month", "type": "contact", "items": ["name", "email"]},
        )
        self.assertEqual(response.context["query"], ["harry"])
        self.assertEqual(response.context["search_date"], "last_month")
        self.assertEqual(response.context["object_type"], "contact")
        self.assertEqual(response.context["search_items"], ["name", "email"])
