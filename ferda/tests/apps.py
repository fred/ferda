from unittest.mock import patch

from ferda.apps import FerdaConfig


class TestFerdaConfig(FerdaConfig):
    def ready(self):
        with patch("ferda.apps.LOGGER_CLIENT", autospec=True):
            with patch("ferda.plugin.hookimpl.LOGGER_CLIENT", autospec=True):
                super().ready()
