from django.contrib.auth import get_user_model
from django.contrib.auth.models import AnonymousUser, Permission
from django.test import RequestFactory, SimpleTestCase, TestCase, override_settings
from ninja import Schema
from ninja.errors import HttpError
from pydantic import ValidationError

from ferda.api import RegistryReference, build_full_url, check_permissions

User = get_user_model()


@override_settings(ALLOWED_HOSTS=["host"])
class BuildFullUrlTest(SimpleTestCase):
    def setUp(self):
        self.factory = RequestFactory()
        self.factory.defaults["SERVER_NAME"] = "host"
        self.request = self.factory.get("/?arg=value")

    def test_build_full_url(self):
        self.assertEqual(build_full_url(self.request), "http://host/?arg=value")
        self.assertEqual(build_full_url(self.request, {"arg2": "value2"}), "http://host/?arg=value&arg2=value2")
        self.assertEqual(build_full_url(self.request, {"arg": "override"}), "http://host/?arg=override")
        self.assertEqual(
            build_full_url(self.request, {"args": ["value1", "value2"]}),
            "http://host/?arg=value&args=value1&args=value2",
        )


class CheckLoggerPermissionsTest(TestCase):
    def setUp(self):
        self.factory = RequestFactory()
        self.request = self.factory.get("/")

    def test_anonymous(self):
        self.request.user = AnonymousUser()
        with self.assertRaises(HttpError) as cm:
            check_permissions(self.request, "ferda.can_view_registry")
        self.assertEqual(cm.exception.status_code, 401)

    def test_non_privileged(self):
        self.request.user = User.objects.create_user(username="rimmer")
        with self.assertRaises(HttpError) as cm:
            check_permissions(self.request, "ferda.can_view_registry")
        self.assertEqual(cm.exception.status_code, 401)

        self.request.user.user_permissions.set(Permission.objects.filter(codename="can_view_registry"))
        with self.assertRaises(HttpError) as cm:
            check_permissions(self.request, ("ferda.can_view_registry", "ferda.can_edit_registrar"))
        self.assertEqual(cm.exception.status_code, 401)

    def test_privileged(self):
        self.request.user = User.objects.create_user(username="rimmer")
        self.request.user.user_permissions.set(
            Permission.objects.filter(codename__in=("can_view_registry", "can_edit_registrar")),
        )
        check_permissions(self.request, "ferda.can_view_registry")
        check_permissions(self.request, ("ferda.can_view_registry", "ferda.can_edit_registrar"))


class RegistryReferenceTest(TestCase):
    def setUp(self):
        self.reference = RegistryReference("contact.some_uid")

    def test_type(self):
        self.assertEqual(self.reference.type, "contact")

    def test_value(self):
        self.assertEqual(self.reference.value, "some_uid")

    def test_repr(self):
        self.assertEqual(repr(self.reference), 'RegistryReference(type="contact", value="some_uid")')

    def test_validator(self):
        class Model(Schema):
            ref: RegistryReference

        self.assertEqual(Model(ref=self.reference).ref, RegistryReference("contact.some_uid"))
        self.assertEqual(Model(ref="contact.some_uid").ref, RegistryReference("contact.some_uid"))  # type: ignore[arg-type]
        with self.assertRaises(ValidationError):
            Model(ref=RegistryReference("dummy"))
        with self.assertRaises(ValidationError):
            Model(ref="dummy")  # type: ignore[arg-type]
