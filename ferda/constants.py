"""Constants for Ferda."""

from enum import Enum, IntEnum, unique

# Pluggy hook marker
HOOK_MARKER = "ferda"

# Session key to a logger session identifier.
LOGGER_SESSION_KEY = "logger_session"
# Name of the logger service.
LOGGER_SERVICE = "Ferda"
LOGGER_SERVICE_HANDLE = "ferda"

# Max file chunk size; set to 32KB
MAX_FILE_CHUNK_SIZE = 32768


@unique
class Env(str, Enum):
    """Enum with Ferda environment variable names."""

    FERDA_DEPLOY_ID = "FERDA_DEPLOY_ID"


@unique
class LogEntryType(str, Enum):
    """Enum with Ferda log entry types."""

    CONTACT_DETAIL = "ContactDetail"
    CONTACT_HISTORY = "ContactHistory"
    CONTACT_STATE = "ContactState"

    CONTACT_REPRESENTATIVE_CHANGE = "ContactRepresentativeChange"
    CONTACT_REPRESENTATIVE_CREATE = "ContactRepresentativeCreate"
    CONTACT_REPRESENTATIVE_DELETE = "ContactRepresentativeDelete"
    CONTACT_REPRESENTATIVE_DETAIL = "ContactRepresentativeDetail"

    DOMAIN_DETAIL = "DomainDetail"
    DOMAIN_HISTORY = "DomainHistory"
    DOMAIN_LIST = "DomainList"
    DOMAIN_STATE = "DomainState"
    DOMAIN_STATE_CHANGE = "DomainStateChange"
    DOMAIN_CONTACT_INFO = "DomainContactInfo"
    DOMAIN_CONTACT_INFO_UPDATE = "DomainContactInfoUpdate"

    KEYSET_DETAIL = "KeysetDetail"
    KEYSET_HISTORY = "KeysetHistory"
    KEYSET_STATE = "KeysetState"

    NSSET_DETAIL = "NssetDetail"
    NSSET_HISTORY = "NssetHistory"
    NSSET_STATE = "NssetState"

    CONTACT_SEARCH = "ContactSearch"
    DOMAIN_SEARCH = "DomainSearch"
    KEYSET_SEARCH = "KeysetSearch"
    NSSET_SEARCH = "NssetSearch"

    CONTACT_SEARCH_HISTORY = "ContactSearchHistory"
    DOMAIN_SEARCH_HISTORY = "DomainSearchHistory"
    KEYSET_SEARCH_HISTORY = "KeysetSearchHistory"
    NSSET_SEARCH_HISTORY = "NssetSearchHistory"

    RECORD_STATEMENT = "RecordStatement"

    REGISTRAR_CREDIT_LIST = "RegistrarCreditList"
    REGISTRAR_DETAIL = "RegistrarDetail"
    REGISTRAR_LIST = "RegistrarList"
    REGISTRAR_CREATE = "RegistrarCreate"
    REGISTRAR_UPDATE = "RegistrarUpdate"
    REGISTRAR_ZONE_ACCESS_VIEW = "RegistrarZoneAccessView"
    REGISTRAR_ZONE_ACCESS_CREATE = "RegistrarZoneAccessCreate"
    REGISTRAR_ZONE_ACCESS_UPDATE = "RegistrarZoneAccessUpdate"
    REGISTRAR_ZONE_ACCESS_DELETE = "RegistrarZoneAccessDelete"
    REGISTRAR_EPP_CREDENTIALS_VIEW = "RegistrarEppCredentialsView"
    REGISTRAR_EPP_CREDENTIALS_CREATE = "RegistrarEppCredentialsCreate"
    REGISTRAR_EPP_CREDENTIALS_UPDATE = "RegistrarEppCredentialsUpdate"
    REGISTRAR_EPP_CREDENTIALS_DELETE = "RegistrarEppCredentialsDelete"
    REGISTRAR_GROUP_VIEW = "RegistrarGroupView"
    REGISTRAR_GROUP_CREATE = "RegistrarGroupCreate"
    REGISTRAR_GROUP_DELETE = "RegistrarGroupDelete"
    REGISTRAR_CERTIFICATION_VIEW = "RegistrarCertificationView"
    REGISTRAR_CERTIFICATION_CREATE = "RegistrarCertificationCreate"
    REGISTRAR_CERTIFICATION_UPDATE = "RegistrarCertificationUpdate"
    REGISTRAR_CERTIFICATION_DELETE = "RegistrarCertificationDelete"

    EMAIL_MESSAGE_DETAIL = "EmailMessageDetail"
    EMAIL_MESSAGE_LIST = "EmailMessageList"
    LETTER_MESSAGE_DETAIL = "LetterMessageDetail"
    LETTER_MESSAGE_LIST = "LetterMessageList"
    SMS_MESSAGE_DETAIL = "SmsMessageDetail"
    SMS_MESSAGE_LIST = "SmsMessageList"

    FILE_READ = "FileRead"

    REPORT_RUN = "ReportRun"
    MANAGE_REPORTS = "ManageReports"

    LOGGER_DETAIL = "LoggerDetail"
    LOGGER_LIST = "LoggerList"


@unique
class LogReferenceType(str, Enum):
    """Log reference type."""

    CONTACT = "contact"
    DOMAIN = "domain"
    KEYSET = "keyset"
    NSSET = "nsset"
    REGISTRAR = "registrar"
    REPRESENTATIVE = "representative"


@unique
class LoggerResult(str, Enum):
    """Enum with log entry results."""

    SUCCESS = "Success"
    FAIL = "Fail"
    ERROR = "Error"


@unique
class WarningLetterPreference(IntEnum):
    """Enum with warning letter results."""

    to_send = 0
    not_to_send = 1
    not_specified = 2


@unique
class MessageType(str, Enum):
    """Message type constants."""

    EMAIL = "email"
    LETTER = "letter"
    SMS = "sms"


@unique
class StateFlag(str, Enum):
    """Registry object state flags."""

    SERVER_INZONE_MANUAL = "serverInzoneManual"
