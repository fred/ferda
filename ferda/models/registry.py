"""Registry related models."""

from django.db import models
from django.utils.translation import gettext_lazy as _
from django_countries.fields import CountryField


class ContactRepresentative(models.Model):
    """Contact representative."""

    contact_uuid = models.UUIDField(verbose_name=_("Contact UUID"))
    organization = models.CharField(max_length=1024, verbose_name=_("Organization"))
    street1 = models.CharField(max_length=1024, verbose_name=_("Street"))
    street2 = models.CharField(max_length=1024, blank=True, verbose_name=_("Street 2"))
    street3 = models.CharField(max_length=1024, blank=True, verbose_name=_("Street 3"))
    city = models.CharField(max_length=1024, verbose_name=_("City"))
    state_or_province = models.CharField(max_length=1024, blank=True, verbose_name=_("State or province"))
    postal_code = models.CharField(max_length=32, verbose_name=_("Postal code"))
    country = CountryField(verbose_name=_("Country"))
    valid_from = models.DateTimeField()
    valid_to = models.DateTimeField(null=True)

    class Meta:
        constraints = [
            models.UniqueConstraint(
                fields=["contact_uuid"],
                condition=models.Q(valid_to=None),
                name="unique_current_representative",
            )
        ]
