"""User related models."""

from django.contrib.auth.models import AbstractUser


class User(AbstractUser):
    """User model."""

    class Meta:
        """Meta class."""

        permissions = [
            ("can_view_registry", "Can view basic registry content"),
            ("can_view_registrar_credit", "Can view registrar credit"),
            ("can_set_manual_in_zone", "Can set manual in zone domain state flag"),
            ("can_update_additional_contacts", "Can update additional outzone/delete notify contacts"),
            ("can_edit_registrar", "Can edit registrar"),
        ]
