"""Report models."""

from dataclasses import dataclass, field
from typing import Any, Dict, List

from django.db import models, transaction
from django.utils.translation import gettext_lazy as _
from reporter import Report as DbReport
from reporter.report import Param


@dataclass
class SyncReportsResult:
    """Sync reports result."""

    created: List[str] = field(default_factory=list)
    deleted: List[str] = field(default_factory=list)
    updated: List[str] = field(default_factory=list)


class ReportManager(models.Manager):
    """Database reports manager."""

    @transaction.atomic
    def sync_with_list(self, service: str, reports: List[DbReport], dry_run: bool = False) -> SyncReportsResult:
        """Synchronize reports in the database with reports in the list.

        Args:
            service: Reporting service.
            reports: List of db reports.
            dry_run: Whether to perform actual changes in database.

        Returns:
            Dict with names of created, deleted and updated reports.
        """
        result = SyncReportsResult()

        for list_report in reports:
            report, created = self.get_or_create(service=service, name=list_report.name)
            if created:
                # Report existed in the list, but not in the database. Update it.
                report.update_parameters(report=list_report)
                result.created.append(f"{service}.{list_report.name}")

            elif list_report != report.to_db_report():
                # Report with the same name differs in the list and in the database. Merge them.
                # First delete any parameters that are not in the report from the list.
                report.update_parameters(report=list_report)
                result.updated.append(f"{service}.{list_report.name}")

        # Delete reports that are not present in the list from database.
        for report in self.filter(service=service).exclude(name__in=[r.name for r in reports]):
            result.deleted.append(f"{service}.{report.name}")
            report.delete()

        if dry_run:
            transaction.set_rollback(True)

        return result


class Report(models.Model):
    """Database report."""

    objects = ReportManager()
    service = models.SlugField(max_length=1024, null=False, blank=False, verbose_name=_("Service"))
    name = models.SlugField(max_length=1024, null=False, blank=False, verbose_name=_("Name"))
    label = models.CharField(max_length=1024, blank=True, verbose_name=_("Label"))
    description = models.TextField(blank=True, verbose_name=_("Long description"))
    tags = models.CharField(
        max_length=2048, blank=True, verbose_name=_("Tags"), help_text=_("List of tags separated by semicolons")
    )

    def __str__(self) -> str:
        return self.label or self.name

    def to_db_report(self) -> DbReport:
        """Transform report object to DbReport."""
        return DbReport(
            name=self.name,
            input_parameters=[Param(name=p.name, type=p.type) for p in self.parameters.filter(is_input=True)],
            output_parameters=[Param(name=p.name, type=p.type) for p in self.parameters.filter(is_input=False)],
        )

    def to_dict(self) -> Dict[str, Any]:
        """Transform report object to dict."""
        report_dict = {
            "service": self.service,
            "name": self.name,
            "label": self.label,
            "description": self.description,
            "input_parameters": [p.to_dict() for p in self.parameters.filter(is_input=True)],
            "output_parameters": [p.to_dict() for p in self.parameters.filter(is_input=False)],
            "tags": [tag.strip() for tag in [v for v in self.tags.split(";") if v]],
        }
        return report_dict

    @transaction.atomic
    def update_parameters(self, report: DbReport):
        """Update report parameters.

        Args:
            report: DbReport object to synchronize with.
        """
        self_dbreport = self.to_db_report()

        # First delete any parameters that are not in the report from the list.
        self.parameters.filter(is_input=True).exclude(name__in=[p.name for p in report.input_parameters]).delete()
        self.parameters.filter(is_input=False).exclude(name__in=[p.name for p in report.output_parameters]).delete()

        # Then add all parameters that are missing from the database.
        for p in report.input_parameters:
            if p not in self_dbreport.input_parameters:
                self.parameters.update_or_create(name=p.name, is_input=True, defaults={"type": p.type})
        for p in report.output_parameters:
            if p not in self_dbreport.output_parameters:
                self.parameters.update_or_create(name=p.name, is_input=False, defaults={"type": p.type})

    class Meta:
        verbose_name = _("Report")
        verbose_name_plural = _("Reports")
        permissions = (("run_report", "Run report"),)
        constraints = [
            models.UniqueConstraint(fields=["service", "name"], name="unique_name_in_service"),
        ]


class ReportParameter(models.Model):
    """Database report parameter."""

    report = models.ForeignKey("Report", on_delete=models.CASCADE, related_name="parameters")
    name = models.SlugField(max_length=1024, verbose_name=_("Name"))
    type = models.CharField(max_length=1024, verbose_name=_("Type"))
    is_input = models.BooleanField(verbose_name=_("Is input"))
    label = models.CharField(max_length=1024, blank=True, verbose_name=_("Label"))
    order = models.PositiveIntegerField(default=0, blank=False, null=False)

    def __str__(self) -> str:
        return self.name

    def to_dict(self) -> Dict[str, Any]:
        """Transform report parameter object to dict."""
        return {"name": self.name, "type": self.type, "label": self.label}

    class Meta:
        verbose_name = _("Report parameter")
        verbose_name_plural = _("Report parameters")
        default_permissions = ()
        constraints = [
            models.UniqueConstraint(
                fields=["report", "name", "is_input"],
                name="unique_report_parameter_name",
            )
        ]
        ordering = ("order",)
