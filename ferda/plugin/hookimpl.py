"""Ferda plugin hooks implementation."""

import os
from functools import lru_cache
from typing import Any, Dict, List, Tuple

from django.conf import settings as django_settings
from django.urls import get_resolver
from django_fido.backends import is_fido_backend_used

import ferda
from ferda.backend import LOGGER_CLIENT
from ferda.constants import Env
from ferda.settings import SETTINGS


def _get_version() -> str:
    if deploy_id := os.environ.get(Env.FERDA_DEPLOY_ID):
        return "{}+{}".format(ferda.__version__, deploy_id)
    else:
        return ferda.__version__


@ferda.hookimpl
@lru_cache
def show_settings() -> Dict[str, Any]:
    """Get selected Ferda settings that should be passed to frontend."""
    settings = {
        "version": _get_version(),
        "common": {
            "time_zone": django_settings.TIME_ZONE,
            "language_code": django_settings.LANGUAGE_CODE,
            "languages": [lang[0] for lang in django_settings.LANGUAGES],
            "fido2_authentication": is_fido_backend_used(),
            "max_file_upload_size": SETTINGS.max_file_upload_size,
        },
        "registry": {
            "manual_in_zone_default_duration": SETTINGS.manual_in_zone_default_duration,
            "object_detail_logger_services": SETTINGS.object_detail_logger_services,
        },
    }
    return settings


@ferda.hookimpl(specname="show_settings")
@lru_cache
def add_urls_to_settings() -> Dict[str, Any]:
    """Add url prefixes to settings API."""
    # pattern[0] contains URL prefix for included patterns
    urls = {app: pattern[0] for app, pattern in get_resolver().namespace_dict.items()}
    return {"common": {"app_url_prefixes": urls}}


@ferda.hookimpl
def create_permissions() -> List[Tuple[str, str]]:
    """Create ferda permissions."""
    permissions = []

    logger_services = LOGGER_CLIENT.get_services()
    for service in logger_services:
        permissions.append((f"ferda.can_view_logger_service__{service}", f"Can view logger service {service}"))

    return permissions
