"""Ferda plugin hooks specification."""

from typing import Any, Iterable, Mapping, Tuple

import pluggy

from ferda.constants import HOOK_MARKER

hookspec = pluggy.HookspecMarker(HOOK_MARKER)


@hookspec
def create_permissions() -> Iterable[Tuple[str, str]]:  # type: ignore[empty-body]
    """Add defined permissions to database.

    Returns: List of tuples (name, description) of permissions.
        Permission names should preferrably contain exactly one dot separating
        content type from permission codename, e.g. `ferda_registry.can_view`.
        If there is more than one dot, codename is the part before the first dot.
    """


@hookspec
def show_settings() -> Mapping[str, Any]:  # type: ignore[empty-body]
    """Define settings that should be exposed in public API.

    Plugins are encouraged to return nested dicts with plugin name as the top level key.
    This prevents name conflicts. Conservative deep merge strategy is used to merge the
    dicts. This means that in case of an unmergeable conflict, the original value stays
    in place.

    Returns:
        Dict that will be merged with other settings dicts and provided in settings API.
    """
