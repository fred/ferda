"""Ferda plugin framework."""

import pluggy

import ferda.plugin.hookimpl
import ferda.plugin.hookspec
from ferda.constants import HOOK_MARKER


def get_plugin_manager():
    """Get pluggy plugin mananager."""
    pm = pluggy.PluginManager(HOOK_MARKER)
    pm.add_hookspecs(ferda.plugin.hookspec)
    pm.load_setuptools_entrypoints(HOOK_MARKER)
    pm.register(ferda.plugin.hookimpl, name="ferda")
    return pm
