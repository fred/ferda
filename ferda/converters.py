"""Ferda url converters."""

from urllib.parse import quote, unquote

from django.urls.converters import StringConverter
from django.utils.dateparse import parse_datetime


class DatetimeConverter:
    """Datetime url converter.

    We can't use regex from django.utils.dateparse, because it contains capture groups.
    Other thant that, this regex is identical.
    """

    regex = r"\d{4}-\d{1,2}-\d{1,2}[T ]\d{1,2}:\d{1,2}(:\d{1,2}(\.\d{1,6}\d{0,6})?)?(Z|[+-]\d{2}(:?\d{2})?)?"

    def to_python(self, value):
        """Convert datetime string in ISO format to datetime object."""
        return parse_datetime(value)

    def to_url(self, value):
        """Convert datetime object to datetime string."""
        return value.isoformat()


class QuoteConverter(StringConverter):
    """Quote url converter for strings that contain special characters."""

    def to_python(self, value):
        """Unquote string."""
        return unquote(value)

    def to_url(self, value):
        """Quote string."""
        return quote(value)
