"""Ferda backend exceptions."""

from frgal.exceptions import FrgalException


class FerdaBackendException(FrgalException):
    """Base exception for all Ferda backend exceptions."""


class ContactDoesNotExist(FerdaBackendException):
    """Exception thrown when contact does not exist in backend."""


class DomainDoesNotExist(FerdaBackendException):
    """Exception thrown when domain does not exist in backend."""


class KeysetDoesNotExist(FerdaBackendException):
    """Exception thrown when keyset does not exist in backend."""


class NssetDoesNotExist(FerdaBackendException):
    """Exception thrown when nsset does not exist in backend."""


class RegistrarDoesNotExist(FerdaBackendException):
    """Exception thrown when registrar does not exist in backend."""


class InvalidHistoryInterval(FerdaBackendException):
    """Exception thrown for invalid provided interval."""
