"""Ferda messenger client."""

from typing import Dict

from hermes import EmailMessengerClient, LetterMessengerClient, SmsMessengerClient
from hermes.client import MessengerClientBase

from ferda.constants import MessageType
from ferda.settings import SETTINGS

from .secretary import SECRETARY
from .utils import make_credentials

_EMAIL = EmailMessengerClient(
    SETTINGS.messenger_netloc,
    credentials=make_credentials(SETTINGS.messenger_ssl_cert),
    secretary_client=SECRETARY,
)
_LETTER = LetterMessengerClient(
    SETTINGS.messenger_netloc,
    credentials=make_credentials(SETTINGS.messenger_ssl_cert),
    secretary_client=SECRETARY,
)
_SMS = SmsMessengerClient(
    SETTINGS.messenger_netloc,
    credentials=make_credentials(SETTINGS.messenger_ssl_cert),
    secretary_client=SECRETARY,
)

MESSAGES: Dict[MessageType, MessengerClientBase] = {
    MessageType.EMAIL: _EMAIL,
    MessageType.LETTER: _LETTER,
    MessageType.SMS: _SMS,
}
