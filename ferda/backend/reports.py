"""Ferda dbreport client."""

from typing import Dict

from reporter import ReportClient, ReportManagerClient

from ferda.settings import SETTINGS

from .utils import make_credentials

REPORTS: Dict[str, ReportClient] = {
    key: ReportClient(value["NETLOC"], credentials=make_credentials(value["SSL_CERT"]))
    for key, value in SETTINGS.reports.items()
}
REPORT_MANAGERS: Dict[str, ReportManagerClient] = {
    key: ReportManagerClient(value["NETLOC"], credentials=make_credentials(value["SSL_CERT"]))
    for key, value in SETTINGS.reports.items()
}
