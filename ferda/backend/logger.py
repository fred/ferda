"""Ferda logger client."""

from grill import Logger, LoggerClient, SearchLoggerHistoryAsyncClient

from ferda.constants import LOGGER_SERVICE, LoggerResult
from ferda.settings import SETTINGS

from .utils import make_credentials

LOGGER_CLIENT = LoggerClient(
    SETTINGS.logger_netloc,
    credentials=make_credentials(SETTINGS.logger_ssl_cert),
)

LOGGER = Logger(LOGGER_CLIENT, service=LOGGER_SERVICE, default_result=LoggerResult.ERROR)

LOGGER_SEARCH = SearchLoggerHistoryAsyncClient(
    SETTINGS.logger_netloc,
    credentials=make_credentials(SETTINGS.logger_ssl_cert),
)
