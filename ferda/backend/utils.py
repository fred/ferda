"""Ferda backend utils."""

from pathlib import Path
from typing import Union

import grpc


def make_credentials(ssl_cert: Union[str, Path, None]):
    """Create credentials from ssl certificate file.

    Args:
        ssl_cert: Path to ssl certificate file.

    Returns:
        None if ssl_cert is None.
        Credentials otherwise.
    """
    if ssl_cert is not None:
        with open(ssl_cert) as fh:
            return grpc.ssl_channel_credentials(fh.read())
    else:
        return None
