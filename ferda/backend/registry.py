"""Ferda registry clients."""

from frgal import make_credentials
from regal import (
    ContactClient,
    DomainAdminClient,
    DomainClient,
    KeysetClient,
    NssetClient,
    RegistrarAdminClient,
    RegistrarClient,
    RepresentativeClient,
    SearchContactClient,
    SearchDomainClient,
    SearchKeysetClient,
    SearchNssetClient,
)
from statementor import Statementor

from ferda.settings import SETTINGS

from .secretary import SECRETARY

CONTACT = ContactClient(SETTINGS.grpc_netloc, credentials=make_credentials(SETTINGS.grpc_ssl_cert))
CONTACT_SEARCH = SearchContactClient(SETTINGS.grpc_netloc, credentials=make_credentials(SETTINGS.grpc_ssl_cert))
DOMAIN = DomainClient(SETTINGS.grpc_netloc, credentials=make_credentials(SETTINGS.grpc_ssl_cert))
DOMAIN_ADMIN = DomainAdminClient(SETTINGS.grpc_netloc, credentials=make_credentials(SETTINGS.grpc_ssl_cert))
DOMAIN_SEARCH = SearchDomainClient(SETTINGS.grpc_netloc, credentials=make_credentials(SETTINGS.grpc_ssl_cert))
KEYSET = KeysetClient(SETTINGS.grpc_netloc, credentials=make_credentials(SETTINGS.grpc_ssl_cert))
KEYSET_SEARCH = SearchKeysetClient(SETTINGS.grpc_netloc, credentials=make_credentials(SETTINGS.grpc_ssl_cert))
NSSET = NssetClient(SETTINGS.grpc_netloc, credentials=make_credentials(SETTINGS.grpc_ssl_cert))
NSSET_SEARCH = SearchNssetClient(SETTINGS.grpc_netloc, credentials=make_credentials(SETTINGS.grpc_ssl_cert))
REGISTRAR = RegistrarClient(SETTINGS.grpc_netloc, credentials=make_credentials(SETTINGS.grpc_ssl_cert))
REGISTRAR_ADMIN = RegistrarAdminClient(SETTINGS.grpc_netloc, credentials=make_credentials(SETTINGS.grpc_ssl_cert))
REPRESENTATIVE = RepresentativeClient(SETTINGS.grpc_netloc, credentials=make_credentials(SETTINGS.grpc_ssl_cert))
STATEMENTOR = Statementor(
    secretary_client=SECRETARY,
    contact_client=CONTACT,
    domain_client=DOMAIN,
    keyset_client=KEYSET,
    nsset_client=NSSET,
    registrar_client=REGISTRAR,
)
