"""Ferda backend decoder."""

from decimal import Decimal
from typing import Dict, Optional
from uuid import UUID

from fred_api.registry.common_types_pb2 import Uuid
from fred_api.registry.contact.contact_common_types_pb2 import ContactAdditionalIdentifier, WarningLetter
from fred_api.registry.contact.contact_info_types_pb2 import ContactInfoReply
from fred_api.registry.registrar.registrar_common_types_pb2 import Credit
from frgal import GrpcDecoder

from ferda.constants import WarningLetterPreference

from .exceptions import (
    ContactDoesNotExist,
    DomainDoesNotExist,
    InvalidHistoryInterval,
    KeysetDoesNotExist,
    NssetDoesNotExist,
    RegistrarDoesNotExist,
)


class FerdaBackendDecoder(GrpcDecoder):
    """Ferda backend decoder."""

    def __init__(self):
        """Initialize the decoder object by registering custom decoders."""
        super().__init__()
        self.set_decoder(Uuid, self._decode_uuid)
        self.set_decoder(ContactAdditionalIdentifier, self._decode_contact_additional_identifier)
        self.set_decoder(Credit, self._decode_credit)
        self.set_decoder(WarningLetter, self._decode_warning_letter)
        self.set_exception_decoder("contact_does_not_exist", ContactDoesNotExist)
        self.set_exception_decoder("domain_does_not_exist", DomainDoesNotExist)
        self.set_exception_decoder("keyset_does_not_exist", KeysetDoesNotExist)
        self.set_exception_decoder("nsset_does_not_exist", NssetDoesNotExist)
        self.set_exception_decoder("registrar_does_not_exist", RegistrarDoesNotExist)
        self.set_exception_decoder("invalid_history_interval", InvalidHistoryInterval)
        self.set_shallow_fields(
            ContactInfoReply.Data,
            {
                "place",
                "mailing_address",
                "billing_address",
                "shipping_address1",
                "shipping_address2",
                "shipping_address3",
            },
        )

    def _decode_uuid(self, value: Uuid) -> Optional[UUID]:
        """Decode Uuid."""
        if not value.value:
            return None
        return UUID(value.value)

    def _decode_contact_additional_identifier(self, value: ContactAdditionalIdentifier) -> Optional[Dict[str, str]]:
        """Decode ContactAdditionalIdentifier."""
        field = value.WhichOneof("Variant")
        if field is not None:
            return {"type": field, "value": self.decode(getattr(value, field))}
        else:
            return None

    def _decode_credit(self, value: Credit) -> Optional[Decimal]:
        """Decode registrar credit."""
        if not value.decimal_value:
            return None
        return Decimal(value.decimal_value)

    def _decode_warning_letter(self, value: WarningLetter) -> str:
        """Decode WarningLetter."""
        return WarningLetterPreference(value.preference).name
