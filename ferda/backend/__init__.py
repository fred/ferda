"""Ferda backend clients."""

from .client import BACKEND, FerdaBackendClient
from .fileman import FILE_STORAGE, FILEMAN
from .logger import LOGGER, LOGGER_CLIENT, LOGGER_SEARCH
from .messages import MESSAGES
from .registry import (
    CONTACT,
    CONTACT_SEARCH,
    DOMAIN,
    DOMAIN_ADMIN,
    DOMAIN_SEARCH,
    KEYSET,
    KEYSET_SEARCH,
    NSSET,
    NSSET_SEARCH,
    REGISTRAR,
    REGISTRAR_ADMIN,
    REPRESENTATIVE,
    STATEMENTOR,
)
from .reports import REPORT_MANAGERS, REPORTS
from .secretary import SECRETARY

__all__ = [
    "BACKEND",
    "CONTACT",
    "CONTACT_SEARCH",
    "DOMAIN",
    "DOMAIN_ADMIN",
    "DOMAIN_SEARCH",
    "FILEMAN",
    "FILE_STORAGE",
    "FerdaBackendClient",
    "KEYSET",
    "KEYSET",
    "KEYSET_SEARCH",
    "LOGGER",
    "LOGGER_CLIENT",
    "LOGGER_SEARCH",
    "MESSAGES",
    "NSSET",
    "NSSET",
    "NSSET_SEARCH",
    "REGISTRAR",
    "REGISTRAR_ADMIN",
    "REPORTS",
    "REPORT_MANAGERS",
    "REPRESENTATIVE",
    "SECRETARY",
    "STATEMENTOR",
]
