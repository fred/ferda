"""Ferda fileman client."""

from filed import FileClient, Storage

from ferda.settings import SETTINGS

from .utils import make_credentials

FILEMAN = FileClient(
    SETTINGS.fileman_netloc,
    credentials=make_credentials(SETTINGS.fileman_ssl_cert),
)

FILE_STORAGE = Storage(FILEMAN)
