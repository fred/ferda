"""Ferda backend client."""

from datetime import datetime
from typing import Any, Callable, Mapping, Optional, Union
from uuid import UUID

import fred_api.registry.contact.service_contact_grpc_pb2_grpc
import fred_api.registry.domain.service_domain_grpc_pb2_grpc
import fred_api.registry.keyset.service_keyset_grpc_pb2_grpc
import fred_api.registry.nsset.service_nsset_grpc_pb2_grpc
import fred_api.registry.registrar.service_registrar_grpc_pb2_grpc
from fred_api.registry.contact.contact_history_types_pb2 import ContactHistoryRequest
from fred_api.registry.contact.contact_info_types_pb2 import ContactInfoRequest
from fred_api.registry.contact.contact_state_history_types_pb2 import ContactStateHistoryRequest
from fred_api.registry.contact.contact_state_types_pb2 import ContactStateRequest
from fred_api.registry.domain.domain_history_types_pb2 import DomainHistoryRequest
from fred_api.registry.domain.domain_info_types_pb2 import DomainInfoRequest
from fred_api.registry.domain.domain_state_history_types_pb2 import DomainStateHistoryRequest
from fred_api.registry.domain.domain_state_types_pb2 import DomainStateRequest
from fred_api.registry.keyset.keyset_history_types_pb2 import KeysetHistoryRequest
from fred_api.registry.keyset.keyset_info_types_pb2 import KeysetInfoRequest
from fred_api.registry.keyset.keyset_state_history_types_pb2 import KeysetStateHistoryRequest
from fred_api.registry.keyset.keyset_state_types_pb2 import KeysetStateRequest
from fred_api.registry.nsset.nsset_history_types_pb2 import NssetHistoryRequest
from fred_api.registry.nsset.nsset_info_types_pb2 import NssetInfoRequest
from fred_api.registry.nsset.nsset_state_history_types_pb2 import NssetStateHistoryRequest
from fred_api.registry.nsset.nsset_state_types_pb2 import NssetStateRequest
from frgal import GrpcClient, GrpcDecoder
from frgal.client import FutureCallWrapper

from ferda.settings import SETTINGS

from .decoder import FerdaBackendDecoder
from .utils import make_credentials


def itemgetter(*args) -> Callable[[Mapping], Any]:
    """Return function that recursively accesses item in nested dictionary."""

    def _getitem(d: Mapping) -> Any:
        for key in args:
            d = d[key]
        return d

    return _getitem


class FerdaBackendClient(GrpcClient):
    """Ferda backend wrapper."""

    def __init__(self, decoder: GrpcDecoder) -> None:
        """Initialize Ferda backend client instance.

        Args:
            decoder: Decoder instance.
        """
        grpc_modules = [
            fred_api.registry.contact.service_contact_grpc_pb2_grpc,
            fred_api.registry.domain.service_domain_grpc_pb2_grpc,
            fred_api.registry.keyset.service_keyset_grpc_pb2_grpc,
            fred_api.registry.nsset.service_nsset_grpc_pb2_grpc,
            fred_api.registry.registrar.service_registrar_grpc_pb2_grpc,
        ]

        super().__init__(
            SETTINGS.grpc_netloc,
            decoder=decoder,
            grpc_modules=grpc_modules,
            credentials=make_credentials(SETTINGS.grpc_ssl_cert),
        )

    def get_contact_info(
        self,
        contact_id: UUID,
        history_id: Optional[UUID] = None,
    ) -> dict:
        """Get contact info.

        Args:
            contact_id: Contact UUID.
            history_id: Contact history UUID.

        Returns:
            Basic contact info.
        """
        return self.get_contact_info_future(contact_id, history_id).result()

    def get_contact_info_future(
        self,
        contact_id: UUID,
        history_id: Optional[UUID] = None,
    ) -> FutureCallWrapper:
        """Get contact info.

        Args:
            contact_id: Contact UUID.
            history_id: Contact history UUID.

        Returns:
            Basic contact info.
        """
        request = ContactInfoRequest()
        request.contact_id.uuid.value = str(contact_id)
        if history_id is not None:
            request.contact_history_id.uuid.value = str(history_id)

        def format_emails(result):
            """Replace emails list with one email field of comma separated emails. Analogously for notify_emails."""
            result["email"] = ", ".join(result.pop("emails")) or None
            result["notify_email"] = ", ".join(result.pop("notify_emails")) or None
            result["publish"]["email"] = result["publish"].pop("emails")
            result["publish"]["notify_email"] = result["publish"].pop("notify_emails")
            return result

        result = self.call_future("Contact", "get_contact_info", request, modify_result=format_emails)

        return result

    def get_contact_state(self, contact_id: UUID) -> dict:
        """Get contact state.

        Contact state consists of state flags and their boolean values.

        Args:
            contact_id: Contact UUID.
        """
        request = ContactStateRequest()
        request.contact_id.uuid.value = str(contact_id)
        result = self.call("Contact", "get_contact_state", request)
        return result["state"]

    def get_contact_history(
        self,
        contact_id: UUID,
        start: Optional[Union[UUID, datetime]] = None,
        end: Optional[Union[UUID, datetime]] = None,
        *,
        modify_result: Optional[Callable] = None,
    ) -> Any:
        """Get contact history.

        Args:
            contact_id: Contact UUID.
            start: Datetime or history_id of history interval start.
            end: Datetime or history_id of history interval end.
            modify_result: Callable for modifying result of `get_contact_history` method.

        Returns:
            Contact history.
        """
        return self.get_contact_history_future(contact_id, start, end, modify_result=modify_result).result()

    def get_contact_history_future(
        self,
        contact_id: UUID,
        start: Optional[Union[UUID, datetime]] = None,
        end: Optional[Union[UUID, datetime]] = None,
        *,
        modify_result: Optional[Callable] = None,
    ) -> FutureCallWrapper:
        """Get contact history.

        Args:
            contact_id: Contact UUID.
            start: Datetime or history_id of history interval start.
            end: Datetime or history_id of history interval end.
            modify_result: Callable for modifying result of `get_contact_history` method.

        Returns:
            Contact history.
        """
        request = ContactHistoryRequest()
        request.contact_id.uuid.value = str(contact_id)
        self._set_history_limits(request, start, end)

        return self.call_future("Contact", "get_contact_history", request, modify_result=modify_result)

    def get_contact_history_id_by_datetime(
        self,
        contact_id: UUID,
        history_datetime: datetime,
    ) -> UUID:
        """Get contact history id by datetime.

        Args:
            contact_id: Contact UUID.
            history_datetime: Fetch data valid at this datetime.

        Returns:
            Contact history id.
        """
        return self.get_contact_history_id_by_datetime_future(contact_id, history_datetime).result()

    def get_contact_history_id_by_datetime_future(
        self,
        contact_id: UUID,
        history_datetime: datetime,
    ) -> FutureCallWrapper:
        """Get contact history id by datetime.

        Args:
            contact_id: Contact UUID.
            history_datetime: Fetch data valid at this datetime.

        Returns:
            Contact history id.
        """
        history_id = self.get_contact_history_future(
            contact_id=contact_id,
            start=history_datetime,
            end=history_datetime,
            modify_result=itemgetter("timeline", 0, "contact_history_id"),
        )
        return history_id

    def get_contact_state_history(
        self,
        contact_id: UUID,
        start: Optional[Union[UUID, datetime]] = None,
        end: Optional[Union[UUID, datetime]] = None,
    ) -> dict:
        """Get contact state history.

        Args:
            contact_id: Contact UUID.
            start: Datetime or history_id of history interval start.
            end: Datetime or history_id of history interval end.

        Returns:
            Contact state history.
        """
        request = ContactStateHistoryRequest()
        request.contact_id.uuid.value = str(contact_id)
        self._set_history_limits(request, start, end)

        result = self.call("Contact", "get_contact_state_history", request)
        return result

    @staticmethod
    def _set_history_limits(
        request,
        start: Optional[Union[UUID, datetime]] = None,
        end: Optional[Union[UUID, datetime]] = None,
    ):
        """Set history limits to request.

        Args:
            request: Request with `history` item of ContactHistoryInterval type.
            start: Datetime or history_id of history interval start.
            end: Datetime or history_id of history interval end.

        Returns:
            Update request.
        """
        if start is not None:
            if isinstance(start, UUID):
                request.history.lower_limit.contact_history_id.uuid.value = str(start)
            else:
                request.history.lower_limit.timestamp.FromDatetime(start)
        if end is not None:
            if isinstance(end, UUID):
                request.history.upper_limit.contact_history_id.uuid.value = str(end)
            else:
                request.history.upper_limit.timestamp.FromDatetime(end)
        return request

    def get_domain_info(self, domain_id: UUID, history_id: Optional[UUID] = None) -> dict:
        """Get domain info.

        Args:
            domain_id: Domain UUID.
            history_id: Domain history UUID.

        Returns:
            Basic domain info.
        """
        request = DomainInfoRequest()
        request.domain_id.uuid.value = str(domain_id)
        if history_id is not None:
            request.domain_history_id.uuid.value = str(history_id)
        result = self.call("Domain", "get_domain_info", request)
        result["nsset"] = result["nsset"]["id"]
        result["keyset"] = result["keyset"]["id"]
        result["registrant_ref"] = result["registrant"]
        result["registrant"] = result["registrant"]["id"]
        result["administrative_contacts"] = [contact["id"] for contact in result["administrative_contacts"]]
        return result

    def get_domain_state(self, domain_id: UUID) -> dict:
        """Get domain state.

        Domain state consists of state flags and their boolean values.

        Args:
            domain_id: Domain UUID.
        """
        request = DomainStateRequest()
        request.domain_id.uuid.value = str(domain_id)
        result = self.call("Domain", "get_domain_state", request)
        return result["state"]

    def get_domain_history(
        self,
        domain_id: UUID,
        start: Optional[Union[UUID, datetime]] = None,
        end: Optional[Union[UUID, datetime]] = None,
    ) -> dict:
        """Get domain history.

        Args:
            domain_id: Domain UUID.
            start: Datetime or history_id of history interval start.
            end: Datetime or history_id of history interval end.

        Returns:
            Domain history.
        """
        request = DomainHistoryRequest()
        request.domain_id.uuid.value = str(domain_id)
        self._set_history_limits(request, start, end)

        result = self.call("Domain", "get_domain_history", request)
        return result

    def get_domain_state_history(
        self,
        domain_id: UUID,
        start: Optional[Union[UUID, datetime]] = None,
        end: Optional[Union[UUID, datetime]] = None,
    ) -> dict:
        """Get domain state history.

        Args:
            domain_id: Domain UUID.
            start: Datetime or history_id of history interval start.
            end: Datetime or history_id of history interval end.

        Returns:
            Domain state history.
        """
        request = DomainStateHistoryRequest()
        request.domain_id.uuid.value = str(domain_id)
        self._set_history_limits(request, start, end)

        result = self.call("Domain", "get_domain_state_history", request)
        return result

    def get_keyset_info(self, keyset_id: UUID, history_id: Optional[UUID] = None) -> dict:
        """Get keyset info.

        Args:
            keyset_id: Keyset UUID.
            history_id: Keyset history UUID.

        Returns:
            Basic keyset info.
        """
        request = KeysetInfoRequest()
        request.keyset_id.uuid.value = str(keyset_id)
        if history_id is not None:
            request.keyset_history_id.uuid.value = str(history_id)
        result = self.call("Keyset", "get_keyset_info", request)
        result["technical_contacts"] = [contact["id"] for contact in result["technical_contacts"]]
        return result

    def get_keyset_state(self, keyset_id: UUID) -> dict:
        """Get keyset state.

        Keyset state consists of state flags and their boolean values.

        Args:
            keyset_id: Keyset UUID.
        """
        request = KeysetStateRequest()
        request.keyset_id.uuid.value = str(keyset_id)
        result = self.call("Keyset", "get_keyset_state", request)
        return result["state"]

    def get_keyset_history(
        self,
        keyset_id: UUID,
        start: Optional[Union[UUID, datetime]] = None,
        end: Optional[Union[UUID, datetime]] = None,
    ) -> dict:
        """Get keyset history.

        Args:
            keyset_id: Keyset UUID.
            start: Datetime or history_id of history interval start.
            end: Datetime or history_id of history interval end.

        Returns:
            Keyset history.
        """
        request = KeysetHistoryRequest()
        request.keyset_id.uuid.value = str(keyset_id)
        self._set_history_limits(request, start, end)

        result = self.call("Keyset", "get_keyset_history", request)
        return result

    def get_keyset_state_history(
        self,
        keyset_id: UUID,
        start: Optional[Union[UUID, datetime]] = None,
        end: Optional[Union[UUID, datetime]] = None,
    ) -> dict:
        """Get keyset state history.

        Args:
            keyset_id: Keyset UUID.
            start: Datetime or history_id of history interval start.
            end: Datetime or history_id of history interval end.

        Returns:
            Keyset state history.
        """
        request = KeysetStateHistoryRequest()
        request.keyset_id.uuid.value = str(keyset_id)
        self._set_history_limits(request, start, end)

        result = self.call("Keyset", "get_keyset_state_history", request)
        return result

    def get_nsset_info(self, nsset_id: UUID, history_id: Optional[UUID] = None) -> dict:
        """Get nsset info.

        Args:
            nsset_id: Nsset UUID.
            history_id: Nsset history UUID.

        Returns:
            Basic nsset info.
        """
        request = NssetInfoRequest()
        request.nsset_id.uuid.value = str(nsset_id)
        if history_id is not None:
            request.nsset_history_id.uuid.value = str(history_id)
        result = self.call("Nsset", "get_nsset_info", request)
        result["technical_contacts"] = [contact["id"] for contact in result["technical_contacts"]]
        return result

    def get_nsset_state(self, nsset_id: UUID) -> dict:
        """Get nsset state.

        Nsset state consists of state flags and their boolean values.

        Args:
            nsset_id: Nsset UUID.
        """
        request = NssetStateRequest()
        request.nsset_id.uuid.value = str(nsset_id)
        result = self.call("Nsset", "get_nsset_state", request)
        return result["state"]

    def get_nsset_history(
        self,
        nsset_id: UUID,
        start: Optional[Union[UUID, datetime]] = None,
        end: Optional[Union[UUID, datetime]] = None,
    ) -> dict:
        """Get nsset history.

        Args:
            nsset_id: Nsset UUID.
            start: Datetime or history_id of history interval start.
            end: Datetime or history_id of history interval end.

        Returns:
            Nsset history.
        """
        request = NssetHistoryRequest()
        request.nsset_id.uuid.value = str(nsset_id)
        self._set_history_limits(request, start, end)

        result = self.call("Nsset", "get_nsset_history", request)
        return result

    def get_nsset_state_history(
        self,
        nsset_id: UUID,
        start: Optional[Union[UUID, datetime]] = None,
        end: Optional[Union[UUID, datetime]] = None,
    ) -> dict:
        """Get nsset state history.

        Args:
            nsset_id: Nsset UUID.
            start: Datetime or history_id of history interval start.
            end: Datetime or history_id of history interval end.

        Returns:
            Nsset state history.
        """
        request = NssetStateHistoryRequest()
        request.nsset_id.uuid.value = str(nsset_id)
        self._set_history_limits(request, start, end)

        result = self.call("Nsset", "get_nsset_state_history", request)
        return result


BACKEND = FerdaBackendClient(decoder=FerdaBackendDecoder())
