"""Ferda secretary client."""

from typist import SecretaryClient

from ferda.settings import SETTINGS

SECRETARY = SecretaryClient(SETTINGS.secretary_url, auth=SETTINGS.secretary_token, timeout=SETTINGS.secretary_timeout)
