"""Ferda views."""

from .common import AbstractObjectDetailView, AbstractObjectHistoryView
from .contact import ContactDetailView, ContactHistoryView
from .domain import DomainDetailView, DomainHistoryView
from .keyset import KeysetDetailView, KeysetHistoryView
from .messages import MessageDetailView, MessageListView
from .notification import NotificationIndexView
from .nsset import NssetDetailView, NssetHistoryView
from .registrar import RegistrarDetailView, RegistrarListView
from .search import SearchView

__all__ = [
    "AbstractObjectDetailView",
    "AbstractObjectHistoryView",
    "ContactDetailView",
    "ContactHistoryView",
    "DomainDetailView",
    "DomainHistoryView",
    "KeysetDetailView",
    "KeysetHistoryView",
    "MessageDetailView",
    "MessageListView",
    "NotificationIndexView",
    "NssetDetailView",
    "NssetHistoryView",
    "RegistrarDetailView",
    "RegistrarListView",
    "SearchView",
]
