"""Registry domain views."""

from uuid import UUID

from django.contrib.auth.mixins import LoginRequiredMixin, PermissionRequiredMixin

from ferda.backend import BACKEND
from ferda.backend.exceptions import DomainDoesNotExist
from ferda.constants import LogEntryType
from ferda.views import AbstractObjectDetailView, AbstractObjectHistoryView


class DomainDetailView(LoginRequiredMixin, PermissionRequiredMixin, AbstractObjectDetailView):
    """Domain detail view."""

    permission_required = "ferda.can_view_registry"
    template_name = "ferda/registry/domain_detail.html"

    object_history_id_name = "domain_history_id"
    log_entry_type = LogEntryType.DOMAIN_DETAIL
    log_entry_reference_name = "domain"
    object_does_not_exist_exception = DomainDoesNotExist

    def get_object_info(self, object_id: UUID, history_id: UUID) -> dict:
        """Get domain info."""
        return BACKEND.get_domain_info(domain_id=object_id, history_id=history_id)

    def get_object_history(self, object_id: UUID) -> dict:
        """Get domain history."""
        return BACKEND.get_domain_history(domain_id=object_id)


class DomainHistoryView(LoginRequiredMixin, PermissionRequiredMixin, AbstractObjectHistoryView):
    """Domain history view."""

    permission_required = "ferda.can_view_registry"
    template_name = "ferda/registry/domain_history.html"

    object_handle_name = "fqdn"
    object_history_id_name = "domain_history_id"
    log_entry_type = LogEntryType.DOMAIN_HISTORY
    log_entry_reference_name = "domain"
    object_does_not_exist_exception = DomainDoesNotExist

    def get_object_info(self, object_id: UUID, history_id: UUID) -> dict:
        """Get domain info."""
        return BACKEND.get_domain_info(domain_id=object_id, history_id=history_id)

    def get_object_history(self, object_id: UUID) -> dict:
        """Get domain history."""
        return BACKEND.get_domain_history(domain_id=object_id)
