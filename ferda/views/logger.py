"""Logger module views."""

from django.contrib.auth.mixins import LoginRequiredMixin, PermissionRequiredMixin
from django.views.generic import TemplateView


class LoggerIndexView(LoginRequiredMixin, PermissionRequiredMixin, TemplateView):
    """Logger index."""

    permission_required = "ferda.can_view_registry"
    template_name = "ferda/logger.html"
