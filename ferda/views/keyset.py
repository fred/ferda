"""Registry keyset views."""

from uuid import UUID

from django.contrib.auth.mixins import LoginRequiredMixin, PermissionRequiredMixin

from ferda.backend import BACKEND
from ferda.backend.exceptions import KeysetDoesNotExist
from ferda.constants import LogEntryType
from ferda.views import AbstractObjectDetailView, AbstractObjectHistoryView


class KeysetDetailView(LoginRequiredMixin, PermissionRequiredMixin, AbstractObjectDetailView):
    """Keyset detail view."""

    permission_required = "ferda.can_view_registry"
    template_name = "ferda/registry/keyset_detail.html"

    object_history_id_name = "keyset_history_id"
    log_entry_type = LogEntryType.KEYSET_DETAIL
    log_entry_reference_name = "keyset"
    object_does_not_exist_exception = KeysetDoesNotExist

    def get_object_info(self, object_id: UUID, history_id: UUID) -> dict:
        """Get keyset info."""
        return BACKEND.get_keyset_info(keyset_id=object_id, history_id=history_id)

    def get_object_history(self, object_id: UUID) -> dict:
        """Get keyset history."""
        return BACKEND.get_keyset_history(keyset_id=object_id)


class KeysetHistoryView(LoginRequiredMixin, PermissionRequiredMixin, AbstractObjectHistoryView):
    """Keyset history view."""

    permission_required = "ferda.can_view_registry"
    template_name = "ferda/registry/keyset_history.html"

    object_handle_name = "keyset_handle"
    object_history_id_name = "keyset_history_id"
    log_entry_type = LogEntryType.KEYSET_HISTORY
    log_entry_reference_name = "keyset"
    object_does_not_exist_exception = KeysetDoesNotExist

    def get_object_info(self, object_id: UUID, history_id: UUID) -> dict:
        """Get keyset info."""
        return BACKEND.get_keyset_info(keyset_id=object_id, history_id=history_id)

    def get_object_history(self, object_id: UUID) -> dict:
        """Get keyset history."""
        return BACKEND.get_keyset_history(keyset_id=object_id)
