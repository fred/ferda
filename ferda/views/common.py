"""Registry common views and mixins."""

from abc import ABC, abstractmethod
from typing import Optional, Type
from uuid import UUID

from django.http import Http404
from django.utils.translation import gettext as _
from django.views.generic import TemplateView
from grill import LogProperty

from ferda.backend import LOGGER
from ferda.backend.exceptions import FerdaBackendException
from ferda.constants import LOGGER_SESSION_KEY, LogEntryType, LoggerResult


class AbstractObjectDetailView(ABC, TemplateView):
    """Object detail view."""

    object_history_id_name: str
    log_entry_type: LogEntryType
    log_entry_reference_name: str
    object_does_not_exist_exception: Type[FerdaBackendException]

    @abstractmethod
    def get_object_info(self, object_id: UUID, history_id: UUID) -> dict:
        """Get registry object info."""

    @abstractmethod
    def get_object_history(self, object_id: UUID) -> dict:
        """Get registry object history."""

    def get_context_data(self, object_id: UUID, history_id: Optional[UUID] = None, **kwargs):  # type: ignore
        """Add basic object info to context."""
        log_entry = LOGGER.create(
            self.log_entry_type,
            source_ip=self.request.META.get("REMOTE_ADDR", ""),
            session_id=self.request.session[LOGGER_SESSION_KEY],
            properties={"uuid": LogProperty(str(object_id), None)},
        )
        with log_entry:
            try:
                history = self.get_object_history(object_id=object_id)
                if history_id is None:
                    history_id = history["timeline"][-1][self.object_history_id_name]
                object_info = self.get_object_info(object_id=object_id, history_id=history_id)
            except self.object_does_not_exist_exception:
                log_entry.result = LoggerResult.FAIL
                raise Http404(_("Object does not exist")) from None
            log_entry.references[self.log_entry_reference_name] = str(object_id)
            log_entry.result = LoggerResult.SUCCESS

        object_history_datetime = None
        for idx, version in list(enumerate(history["timeline"]))[:-1]:
            if version[self.object_history_id_name] == history_id:
                object_history_datetime = history["timeline"][idx + 1]["valid_from"]
                break
        else:
            object_history_datetime = history["valid_to"]

        context = super().get_context_data(object_id=object_id, **kwargs)
        context["object_data"] = {
            "object_info": object_info,
            "object_uuid": object_id,
            "object_history_id": history_id,
            "object_history_datetime": object_history_datetime,
            "object_has_history": len(history["timeline"]) > 1,
        }
        return context


class AbstractObjectHistoryView(ABC, TemplateView):
    """Object history view."""

    object_handle_name: str
    object_history_id_name: str
    log_entry_type: str
    log_entry_reference_name: str
    object_does_not_exist_exception: Type[FerdaBackendException]

    @abstractmethod
    def get_object_info(self, object_id: UUID, history_id: UUID) -> dict:
        """Get registry object info."""

    @abstractmethod
    def get_object_history(self, object_id: UUID) -> dict:
        """Get registry object history."""

    def get_context_data(self, object_id: UUID, **kwargs):  # type: ignore
        """Add basic object history info to context."""
        log_entry = LOGGER.create(
            self.log_entry_type,
            source_ip=self.request.META.get("REMOTE_ADDR", ""),
            session_id=self.request.session[LOGGER_SESSION_KEY],
            properties={"uuid": LogProperty(str(object_id), None)},
        )
        with log_entry:
            try:
                object_history = self.get_object_history(object_id=object_id)
            except self.object_does_not_exist_exception:
                log_entry.result = LoggerResult.FAIL
                raise Http404(_("Object does not exist")) from None

            version_list = [version[self.object_history_id_name] for version in object_history["timeline"]]
            if len(version_list) < 2:
                log_entry.result = LoggerResult.FAIL
                raise Http404(_("Object does not have a history"))

            from_history_id = self.request.GET.get("from_history_id", None)
            to_history_id = self.request.GET.get("to_history_id", None)

            if from_history_id is not None:
                from_history_id = UUID(from_history_id)
            else:
                from_history_id = object_history["timeline"][-2][self.object_history_id_name]

            if to_history_id is not None:
                to_history_id = UUID(to_history_id)
            else:
                to_history_id = object_history["timeline"][-1][self.object_history_id_name]

            if version_list.index(from_history_id) >= version_list.index(to_history_id):
                log_entry.result = LoggerResult.FAIL
                raise Http404(_("Invalid history interval"))

            try:
                object_info = self.get_object_info(object_id=object_id, history_id=to_history_id)
            except self.object_does_not_exist_exception:
                log_entry.result = LoggerResult.FAIL
                raise Http404(_("Object does not exist")) from None

            log_entry.references[self.log_entry_reference_name] = str(object_id)
            log_entry.result = LoggerResult.SUCCESS

        context = super().get_context_data(object_id=object_id, **kwargs)
        context["history_data"] = {
            "object_uuid": object_id,
            "from_history_id": from_history_id,
            "to_history_id": to_history_id,
            "object_history": object_history,
            "object_handle": object_info[self.object_handle_name],
        }
        return context
