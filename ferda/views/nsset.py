"""Registry nsset views."""

from uuid import UUID

from django.contrib.auth.mixins import LoginRequiredMixin, PermissionRequiredMixin

from ferda.backend import BACKEND
from ferda.backend.exceptions import NssetDoesNotExist
from ferda.constants import LogEntryType
from ferda.views import AbstractObjectDetailView, AbstractObjectHistoryView


class NssetDetailView(LoginRequiredMixin, PermissionRequiredMixin, AbstractObjectDetailView):
    """Nsset detail view."""

    permission_required = "ferda.can_view_registry"
    template_name = "ferda/registry/nsset_detail.html"

    object_history_id_name = "nsset_history_id"
    log_entry_type = LogEntryType.NSSET_DETAIL
    log_entry_reference_name = "nsset"
    object_does_not_exist_exception = NssetDoesNotExist

    def get_object_info(self, object_id: UUID, history_id: UUID) -> dict:
        """Get nsset info."""
        return BACKEND.get_nsset_info(nsset_id=object_id, history_id=history_id)

    def get_object_history(self, object_id: UUID) -> dict:
        """Get nsset history."""
        return BACKEND.get_nsset_history(nsset_id=object_id)


class NssetHistoryView(LoginRequiredMixin, PermissionRequiredMixin, AbstractObjectHistoryView):
    """Nsset history view."""

    permission_required = "ferda.can_view_registry"
    template_name = "ferda/registry/nsset_history.html"

    object_handle_name = "nsset_handle"
    object_history_id_name = "nsset_history_id"
    log_entry_type = LogEntryType.NSSET_HISTORY
    log_entry_reference_name = "nsset"
    object_does_not_exist_exception = NssetDoesNotExist

    def get_object_info(self, object_id: UUID, history_id: UUID) -> dict:
        """Get nsset info."""
        return BACKEND.get_nsset_info(nsset_id=object_id, history_id=history_id)

    def get_object_history(self, object_id: UUID) -> dict:
        """Get nsset history."""
        return BACKEND.get_nsset_history(nsset_id=object_id)
