"""Registrar views."""

from django.contrib.auth.mixins import LoginRequiredMixin, PermissionRequiredMixin
from django.views.generic import TemplateView


class RegistrarDetailView(LoginRequiredMixin, PermissionRequiredMixin, TemplateView):
    """Registrar detail view."""

    permission_required = "ferda.can_view_registry"
    template_name = "ferda/registry/registrar_detail.html"


class RegistrarListView(LoginRequiredMixin, PermissionRequiredMixin, TemplateView):
    """Registrar list view."""

    permission_required = "ferda.can_view_registry"
    template_name = "ferda/registry/registrar_list.html"
