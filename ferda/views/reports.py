"""Ferda report views."""

from django.contrib.auth.mixins import LoginRequiredMixin, PermissionRequiredMixin
from django.http import Http404
from django.views.generic import TemplateView
from guardian.mixins import PermissionRequiredMixin as GuardianPermissionRequiredMixin
from guardian.shortcuts import get_objects_for_user

from ferda.models import Report
from ferda.settings import SETTINGS


class ReportListView(LoginRequiredMixin, PermissionRequiredMixin, TemplateView):
    """List all reports."""

    permission_required = "ferda.view_report"
    template_name = "ferda/reports/list.html"

    def get_context_data(self, **kwargs):
        """List all reports."""
        context = super().get_context_data(**kwargs)
        report_list = (
            get_objects_for_user(self.request.user, "ferda.run_report")
            .filter(
                service__in=SETTINGS.reports.keys(),
            )
            .order_by("label", "name", "service")
        )
        context["reports"] = [report.to_dict() for report in report_list]
        return context


class ReportDetailView(LoginRequiredMixin, GuardianPermissionRequiredMixin, TemplateView):
    """Show report detail."""

    permission_required = "ferda.run_report"
    accept_global_perms = True
    return_403 = True
    template_name = "ferda/reports/detail.html"

    def get_object(self):
        """Get report object from database in order to check permissions."""
        if not hasattr(self, "_report"):
            service = self.kwargs.get("service")
            report_name = self.kwargs.get("report_name")
            try:
                report_object = Report.objects.get(service=service, name=report_name)
            except Report.DoesNotExist:
                raise Http404("Report %(name)s not found" % {"name": report_name}) from None
            self._report = report_object
        return self._report

    def get_context_data(self, **kwargs):
        """Get report detail."""
        context = super().get_context_data(**kwargs)
        context["report"] = self.get_object().to_dict()
        return context
