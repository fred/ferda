"""Registry contact views."""

from uuid import UUID

from django.contrib.auth.mixins import LoginRequiredMixin, PermissionRequiredMixin

from ferda.backend import BACKEND
from ferda.backend.exceptions import ContactDoesNotExist
from ferda.constants import LogEntryType
from ferda.views import AbstractObjectDetailView, AbstractObjectHistoryView


class ContactDetailView(LoginRequiredMixin, PermissionRequiredMixin, AbstractObjectDetailView):
    """Contact detail view."""

    permission_required = "ferda.can_view_registry"
    template_name = "ferda/registry/contact_detail.html"

    object_history_id_name = "contact_history_id"
    log_entry_type = LogEntryType.CONTACT_DETAIL
    log_entry_reference_name = "contact"
    object_does_not_exist_exception = ContactDoesNotExist

    def get_object_info(self, object_id: UUID, history_id: UUID) -> dict:
        """Get contact info."""
        return BACKEND.get_contact_info(contact_id=object_id, history_id=history_id)

    def get_object_history(self, object_id: UUID) -> dict:
        """Get contact history."""
        return BACKEND.get_contact_history(contact_id=object_id)


class ContactHistoryView(LoginRequiredMixin, PermissionRequiredMixin, AbstractObjectHistoryView):
    """Contact history view."""

    permission_required = "ferda.can_view_registry"
    template_name = "ferda/registry/contact_history.html"

    object_handle_name = "contact_handle"
    object_history_id_name = "contact_history_id"
    log_entry_type = LogEntryType.CONTACT_HISTORY
    log_entry_reference_name = "contact"
    object_does_not_exist_exception = ContactDoesNotExist

    def get_object_info(self, object_id: UUID, history_id: UUID) -> dict:
        """Get contact info."""
        return BACKEND.get_contact_info(contact_id=object_id, history_id=history_id)

    def get_object_history(self, object_id: UUID) -> dict:
        """Get contact history."""
        return BACKEND.get_contact_history(contact_id=object_id)
