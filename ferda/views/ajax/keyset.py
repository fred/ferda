"""Keyset related ajax views."""

from datetime import datetime
from typing import Dict, Optional, Union, cast
from uuid import UUID

from django.contrib.auth.mixins import LoginRequiredMixin, PermissionRequiredMixin

from ferda.backend import BACKEND
from ferda.backend.exceptions import KeysetDoesNotExist
from ferda.constants import LogEntryType

from .common import AbstractInfoView, AbstractStateHistoryView, AbstractStateView


class KeysetInfoView(LoginRequiredMixin, PermissionRequiredMixin, AbstractInfoView):
    """Keyset info view."""

    permission_required = "ferda.can_view_registry"

    log_entry_type = LogEntryType.KEYSET_DETAIL
    log_entry_reference_name = "keyset"
    object_does_not_exist_exception = KeysetDoesNotExist

    def get_object_info(self, object_id: UUID, history_id: Optional[Union[UUID, datetime]] = None) -> dict:
        """Get keyset info."""
        if isinstance(history_id, datetime):
            keyset_history = BACKEND.get_keyset_history(keyset_id=object_id, start=history_id, end=history_id)
            history_id = cast(UUID, keyset_history["timeline"][0]["keyset_history_id"])
        return BACKEND.get_keyset_info(keyset_id=object_id, history_id=history_id)


class KeysetStateView(LoginRequiredMixin, PermissionRequiredMixin, AbstractStateView):
    """Keyset state view."""

    permission_required = "ferda.can_view_registry"

    log_entry_type = LogEntryType.KEYSET_STATE
    log_entry_reference_name = "keyset"
    object_does_not_exist_exception = KeysetDoesNotExist

    def get_object_state(self, object_id: UUID) -> Dict[str, bool]:
        """Get keyset state."""
        return BACKEND.get_keyset_state(keyset_id=object_id)


class KeysetStateHistoryView(LoginRequiredMixin, PermissionRequiredMixin, AbstractStateHistoryView):
    """Keyset state history view."""

    permission_required = "ferda.can_view_registry"

    log_entry_type = LogEntryType.KEYSET_STATE
    log_entry_reference_name = "keyset"
    object_does_not_exist_exception = KeysetDoesNotExist

    def get_object_state_history(self, object_id: UUID) -> dict:
        """Get keyset state history."""
        return BACKEND.get_keyset_state_history(keyset_id=object_id)
