"""Monitoring ajax view."""

from typing import Any, Callable, Dict, Iterable, Optional

from django.db import connection
from django.db.utils import OperationalError
from django.http import JsonResponse
from django.views import View


class MonitoringTestResult:
    """Result of test method."""

    def __init__(self, extra_data_keys: Iterable = ()):
        """Initialize MonitoringTestResult.

        Set default status on True and prepares extra_data (sets all keys to None).
        """
        self.status = True
        self.extra_data: Dict[str, Optional[Any]] = {}
        for key in extra_data_keys:
            self.extra_data[key] = None

    def add_extra_data(self, key: str, data: Optional[Any]):
        """Update extra data information."""
        self.extra_data[key] = data

    def get_test_result(self):
        """Return dictionary with results of the test."""
        return {"status": self.status, "extra_data": self.extra_data}


class MonitoringView(View):
    """View for monitoring correct behaviour of all important components."""

    def get(self, request):
        """Return dictionary in JSON with test results and summary and corresponding HTTP response status code.

        Status code 200 (plus summary set to True) when all the tests were correct, 500 (and summary set to False)
        otherwise.

        Returns:
            {
                'test_{{ test_name }}': {
                    'status': bool,
                    'extra_data': Dict(str, Optional[Any]),
                },
                'summary': bool
            }

        Example of a return:
            {
                'test_backend': {
                    'status': True,
                    'extra_data': {...},
                },
                test_database': {
                    'status': True,
                    'extra_data': None,
                },
                'test_logger': {
                    'status': False,
                    'extra_data': None,
                },
                'summary': False
            }
        """
        results: Dict[str, Any] = {}
        tests = self.request.GET.getlist("test")
        if tests:
            test_methods = [test for test in self.get_test_methods().items() if test[0] in tests]
        else:
            test_methods = list(self.get_test_methods().items())

        for test_name, test_method in test_methods:
            test_result = test_method()
            results[test_name] = test_result.get_test_result()

        results["summary"] = all(results[test_name]["status"] for test_name in results)

        return JsonResponse(results)

    @classmethod
    def get_test_methods(cls) -> Dict[str, Callable]:
        """Get dictionary of test methods."""
        return {
            "test_ferda_database": cls._test_ferda_database,
        }

    @staticmethod
    def _test_ferda_database() -> MonitoringTestResult:
        test_result = MonitoringTestResult()
        try:
            cursor = connection.cursor()
            cursor.close()
        except OperationalError:
            test_result.status = False

        return test_result
