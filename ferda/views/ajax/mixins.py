"""Ferda view AJAX mixins."""

from datetime import datetime
from typing import Any, List, Optional, Tuple
from uuid import UUID

from ferda.backend import BACKEND


class RelatedContactsMixin:
    """Technical contacts transformer mixin."""

    @staticmethod
    def get_related_contacts_overview(contact_ids: List[UUID], valid_at: Optional[datetime] = None) -> List[dict]:
        """Return contacts array with name, handle and id based on ids in parameter."""
        related_contacts: List[dict] = []
        history_ids = {}
        tasks: List[Tuple[UUID, Any]] = []

        if valid_at:
            for object_id in contact_ids:
                history_ids[object_id] = BACKEND.get_contact_history_id_by_datetime_future(
                    object_id,
                    history_datetime=valid_at,
                )

        for object_id in contact_ids:
            history_id = None
            if object_id in history_ids:
                history_id = history_ids[object_id].result()
            tasks.append(
                (
                    object_id,
                    BACKEND.get_contact_info_future(object_id, history_id=history_id),
                )
            )

        for _object_id, contact_info in tasks:
            info = contact_info.result()
            related_contacts.append(
                {"contact_handle": info["contact_handle"], "contact_id": info["contact_id"], "name": info["name"]}
            )
        return related_contacts
