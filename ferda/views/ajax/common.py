"""Ajax views common for multiple registry objects."""

from abc import ABC, abstractmethod
from datetime import datetime
from typing import Dict, List, Optional, Type, Union
from uuid import UUID

from django.contrib.auth.decorators import login_required
from django.http import Http404, JsonResponse
from django.utils.translation import gettext as _
from django.views.generic.base import View
from django_countries import countries
from grill import LogProperty, Properties

from ferda.backend import LOGGER
from ferda.backend.exceptions import FerdaBackendException, InvalidHistoryInterval
from ferda.constants import LOGGER_SESSION_KEY, LogEntryType, LoggerResult
from ferda.registry.state_flags import get_state_flag_description
from ferda.settings import SETTINGS


class AbstractInfoView(ABC, View):
    """Abstract info view class."""

    log_entry_type: LogEntryType
    log_entry_reference_name: str
    object_does_not_exist_exception: Type[FerdaBackendException]

    @abstractmethod
    def get_object_info(self, object_id: UUID, history_id: Optional[Union[UUID, datetime]] = None) -> dict:
        """Get registry object info."""

    def get(self, request, object_id: UUID, history_id: Optional[Union[UUID, datetime]] = None):
        """Fetch registry object info.

        Args:
            request: Django HttpRequest.
            object_id: Object UUID in backend.
            history_id: History identifier. Either history UUID or datetime.
        """
        log_properties: Properties = {"uuid": LogProperty(str(object_id), None)}
        if isinstance(history_id, UUID):
            log_properties["history_id"] = LogProperty(str(history_id), None)
        elif isinstance(history_id, datetime):
            log_properties["history_datetime"] = LogProperty(str(history_id), None)
        log_entry = LOGGER.create(
            self.log_entry_type,
            source_ip=request.META.get("REMOTE_ADDR", ""),
            session_id=request.session[LOGGER_SESSION_KEY],
            properties=log_properties,
        )
        with log_entry:
            try:
                object_info = self.get_object_info(object_id=object_id, history_id=history_id)
            except (self.object_does_not_exist_exception, InvalidHistoryInterval):
                log_entry.result = LoggerResult.FAIL
                raise Http404(_("Object does not exist")) from None
            log_entry.references[self.log_entry_reference_name] = str(object_id)
            log_entry.result = LoggerResult.SUCCESS

        return JsonResponse(object_info)


class AbstractStateView(ABC, View):
    """Abstract state view class."""

    log_entry_type: LogEntryType
    log_entry_reference_name: str
    object_does_not_exist_exception: Type[FerdaBackendException]

    @abstractmethod
    def get_object_state(self, object_id: UUID) -> Dict[str, bool]:
        """Get registry object state."""

    def get(self, request, object_id: UUID):
        """Fetch object state from backend.

        Args:
            request: Django HttpRequest.
            object_id: Object UUID in backend.
        """
        log_entry = LOGGER.create(
            self.log_entry_type,
            source_ip=request.META.get("REMOTE_ADDR", ""),
            session_id=request.session[LOGGER_SESSION_KEY],
            properties={"uuid": LogProperty(str(object_id), None)},
        )
        with log_entry:
            try:
                object_state = self.get_object_state(object_id=object_id)
            except self.object_does_not_exist_exception:
                log_entry.result = LoggerResult.FAIL
                raise Http404(_("Object does not exist")) from None
            log_entry.references[self.log_entry_reference_name] = str(object_id)
            log_entry.result = LoggerResult.SUCCESS

        state_flags = {}
        for flag, active in object_state.items():
            description = get_state_flag_description(flag)
            if description is not None:
                state_flags[flag] = {
                    "active": active,
                    "description": description,
                }
        return JsonResponse(
            {
                "flags": state_flags,
                "groups": SETTINGS.state_flags_groups,
            }
        )


class AbstractStateHistoryView(ABC, View):
    """Abstract state history view class."""

    log_entry_type: LogEntryType
    log_entry_reference_name: str
    object_does_not_exist_exception: Type[FerdaBackendException]

    @abstractmethod
    def get_object_state_history(self, object_id: UUID) -> dict:
        """Get registry object state history."""

    def get(self, request, object_id: UUID):
        """Fetch object state history from backend.

        Args:
            request: Django HttpRequest.
            object_id: Object UUID in backend.
        """
        log_properties: Properties = {"uuid": LogProperty(str(object_id), None), "history": LogProperty("true", None)}
        log_entry = LOGGER.create(
            self.log_entry_type,
            source_ip=request.META.get("REMOTE_ADDR", ""),
            session_id=request.session[LOGGER_SESSION_KEY],
            properties=log_properties,
        )
        with log_entry:
            try:
                object_state_history = self.get_object_state_history(object_id=object_id)
            except self.object_does_not_exist_exception:
                log_entry.result = LoggerResult.FAIL
                raise Http404(_("Object does not exist")) from None
            log_entry.references[self.log_entry_reference_name] = str(object_id)
            log_entry.result = LoggerResult.SUCCESS

        history = object_state_history["history"]

        intervals: Dict[str, List[list]] = {}

        flags = list(history["timeline"][0]["flags"])
        history_end = history.get("valid_to", None)

        for flag in flags:
            intervals[flag] = []
            for block in history["timeline"]:
                enabled = block["flags"].get(flag, False)
                if not intervals[flag]:
                    if enabled:
                        # Open first interval.
                        intervals[flag].append([block["valid_from"], None])
                else:
                    if enabled:
                        if intervals[flag][-1][1] is not None:
                            # Open new interval.
                            intervals[flag].append([block["valid_from"], None])
                    else:
                        if intervals[flag][-1][1] is None:
                            # Close last interval.
                            intervals[flag][-1][1] = block["valid_from"]

            if history_end and intervals[flag] and intervals[flag][-1][1] is None:
                # If history is final, close last open interval.
                intervals[flag][-1][1] = history_end

        return JsonResponse(intervals)


@login_required
def countries_list(request):
    """Provide list of country codes and localized country names.

    Args:
        request: Http request.
    """
    return JsonResponse(countries.countries)
