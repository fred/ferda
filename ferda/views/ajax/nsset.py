"""Nsset related ajax views."""

from datetime import datetime
from typing import Dict, Optional, Union, cast
from uuid import UUID

from django.contrib.auth.mixins import LoginRequiredMixin, PermissionRequiredMixin

from ferda.backend import BACKEND
from ferda.backend.exceptions import NssetDoesNotExist
from ferda.constants import LogEntryType

from .common import AbstractInfoView, AbstractStateHistoryView, AbstractStateView


class NssetInfoView(LoginRequiredMixin, PermissionRequiredMixin, AbstractInfoView):
    """Nsset info view."""

    permission_required = "ferda.can_view_registry"

    log_entry_type = LogEntryType.NSSET_DETAIL
    log_entry_reference_name = "nsset"
    object_does_not_exist_exception = NssetDoesNotExist

    def get_object_info(self, object_id: UUID, history_id: Optional[Union[UUID, datetime]] = None) -> dict:
        """Get nsset info."""
        if isinstance(history_id, datetime):
            nsset_history = BACKEND.get_nsset_history(nsset_id=object_id, start=history_id, end=history_id)
            history_id = cast(UUID, nsset_history["timeline"][0]["nsset_history_id"])
        return BACKEND.get_nsset_info(nsset_id=object_id, history_id=history_id)


class NssetStateView(LoginRequiredMixin, PermissionRequiredMixin, AbstractStateView):
    """Nsset state view."""

    permission_required = "ferda.can_view_registry"

    log_entry_type = LogEntryType.NSSET_STATE
    log_entry_reference_name = "nsset"
    object_does_not_exist_exception = NssetDoesNotExist

    def get_object_state(self, object_id: UUID) -> Dict[str, bool]:
        """Get nsset state."""
        return BACKEND.get_nsset_state(nsset_id=object_id)


class NssetStateHistoryView(LoginRequiredMixin, PermissionRequiredMixin, AbstractStateHistoryView):
    """Nsset state history view."""

    permission_required = "ferda.can_view_registry"

    log_entry_type = LogEntryType.NSSET_STATE
    log_entry_reference_name = "nsset"
    object_does_not_exist_exception = NssetDoesNotExist

    def get_object_state_history(self, object_id: UUID) -> dict:
        """Get nsset state history."""
        return BACKEND.get_nsset_state_history(nsset_id=object_id)
