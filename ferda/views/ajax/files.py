"""File ajax views."""

from uuid import UUID

from django.contrib.auth.mixins import LoginRequiredMixin, PermissionRequiredMixin
from django.http import Http404, HttpResponseServerError, StreamingHttpResponse
from django.utils.translation import gettext as _
from django.views.generic import View
from grill import LogProperty

from ferda.backend import FILEMAN, LOGGER
from ferda.constants import LOGGER_SESSION_KEY, MAX_FILE_CHUNK_SIZE, LogEntryType, LoggerResult


class FileReadView(LoginRequiredMixin, PermissionRequiredMixin, View):
    """Read file from fileman."""

    permission_required = "ferda.can_view_registry"

    def get(self, request, file_uuid: UUID):
        """Get file by uuid."""
        log_entry = LOGGER.create(
            LogEntryType.FILE_READ,
            source_ip=self.request.META.get("REMOTE_ADDR", ""),
            session_id=self.request.session[LOGGER_SESSION_KEY],
            properties={"uuid": LogProperty(str(file_uuid), None)},
        )
        with log_entry:
            try:
                stat = FILEMAN.stat(file_uuid)
                response = StreamingHttpResponse(
                    FILEMAN.read(str(file_uuid), size=MAX_FILE_CHUNK_SIZE),
                    content_type=stat["mimetype"],
                )
            except FileNotFoundError:
                log_entry.result = LoggerResult.FAIL
                raise Http404(_("File %(uid)s not found") % {"uid": str(file_uuid)}) from None
            except ConnectionAbortedError:
                return HttpResponseServerError(
                    _("Connection failed when getting file %(uid)s") % {"uid": str(file_uuid)}
                )

            log_entry.result = LoggerResult.SUCCESS

        response["Content-Disposition"] = 'attachment; filename="{}"'.format(stat["name"])
        return response
