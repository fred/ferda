"""Contact related ajax views."""

import dataclasses
from datetime import datetime
from typing import Dict, Optional, Union, cast
from uuid import UUID

from django.contrib.auth.decorators import login_required, permission_required
from django.contrib.auth.mixins import LoginRequiredMixin, PermissionRequiredMixin
from django.forms.models import model_to_dict
from django.http import Http404, HttpResponse, HttpResponseBadRequest, JsonResponse
from django.utils import timezone
from django.utils.translation import gettext as _
from django.views.generic import View
from grill import LogProperty

from ferda.backend import BACKEND, LOGGER
from ferda.backend.exceptions import ContactDoesNotExist
from ferda.constants import LOGGER_SESSION_KEY, LogEntryType, LoggerResult
from ferda.forms import ContactRepresentativeForm
from ferda.models import ContactRepresentative
from ferda.registry.plugins.badges.utils import get_contact_badges

from .common import AbstractInfoView, AbstractStateHistoryView, AbstractStateView


class ContactStateView(LoginRequiredMixin, PermissionRequiredMixin, AbstractStateView):
    """Contact state view."""

    permission_required = "ferda.can_view_registry"

    log_entry_type = LogEntryType.CONTACT_STATE
    log_entry_reference_name = "contact"
    object_does_not_exist_exception = ContactDoesNotExist

    def get_object_state(self, object_id: UUID) -> Dict[str, bool]:
        """Get contact state."""
        return BACKEND.get_contact_state(contact_id=object_id)


class ContactInfoView(LoginRequiredMixin, PermissionRequiredMixin, AbstractInfoView):
    """Contact info view."""

    permission_required = "ferda.can_view_registry"

    log_entry_type = LogEntryType.CONTACT_DETAIL
    log_entry_reference_name = "contact"
    object_does_not_exist_exception = ContactDoesNotExist

    def get_object_info(self, object_id: UUID, history_id: Optional[Union[UUID, datetime]] = None) -> dict:
        """Get contact info."""
        if isinstance(history_id, datetime):
            contact_history = BACKEND.get_contact_history(contact_id=object_id, start=history_id, end=history_id)
            history_id = cast(UUID, contact_history["timeline"][0]["contact_history_id"])
        return BACKEND.get_contact_info(contact_id=object_id, history_id=history_id)


class ContactStateHistoryView(LoginRequiredMixin, PermissionRequiredMixin, AbstractStateHistoryView):
    """Contact state history view."""

    permission_required = "ferda.can_view_registry"

    log_entry_type = LogEntryType.CONTACT_STATE
    log_entry_reference_name = "contact"
    object_does_not_exist_exception = ContactDoesNotExist

    def get_object_state_history(self, object_id: UUID) -> dict:
        """Get contact state history."""
        return BACKEND.get_contact_state_history(contact_id=object_id)


class ContactRepresentativeView(LoginRequiredMixin, PermissionRequiredMixin, View):
    """Contact representative view."""

    permission_required = "ferda.view_contactrepresentative"

    def get(self, request, contact_id: UUID):
        """Get contact representative info."""
        try:
            representative = ContactRepresentative.objects.get(contact_uuid=contact_id, valid_to=None)
        except ContactRepresentative.DoesNotExist:
            raise Http404(_("Contact does not have a representative")) from None

        return JsonResponse(
            {
                "contact_id": representative.contact_uuid,
                "company": representative.organization,
                "street": [
                    street
                    for street in (representative.street1, representative.street2, representative.street3)
                    if street
                ],
                "city": representative.city,
                "state_or_province": representative.state_or_province,
                "postal_code": representative.postal_code,
                "country": representative.country.name,
                "country_code": representative.country.code,
            }
        )


class ContactRepresentativeChangeView(LoginRequiredMixin, PermissionRequiredMixin, View):
    """Contact representative change view."""

    permission_required = "ferda.change_contactrepresentative"

    def post(self, request, contact_id: UUID):
        """Update contact representative info."""
        form_data = request.POST.dict()
        form_data["contact_uuid"] = contact_id

        log_entry = LOGGER.create(
            LogEntryType.CONTACT_REPRESENTATIVE_CHANGE,
            source_ip=self.request.META.get("REMOTE_ADDR", ""),
            session_id=self.request.session[LOGGER_SESSION_KEY],
            properties={"uuid": LogProperty(str(contact_id), None)},
        )

        with log_entry:
            log_entry.references["contact"] = str(contact_id)
            try:
                representative = ContactRepresentative.objects.get(contact_uuid=contact_id, valid_to=None)
            except ContactRepresentative.DoesNotExist:
                # There is no representative for this contact
                # We just create a new one
                representative = None
                form = ContactRepresentativeForm(data=form_data)
            else:
                form = ContactRepresentativeForm(data=form_data, initial=model_to_dict(representative))

            if form.is_valid():
                if form.has_changed():
                    now = timezone.now()
                    if representative:
                        representative.valid_to = now
                        representative.save()
                    instance = form.save(commit=False)
                    instance.valid_from = now
                    instance.save()
                log_entry.result = LoggerResult.SUCCESS
                return HttpResponse()
            else:
                log_entry.result = LoggerResult.FAIL
                return HttpResponseBadRequest()


class ContactRepresentativeDeleteView(LoginRequiredMixin, PermissionRequiredMixin, View):
    """Contact representative delete view."""

    permission_required = "ferda.delete_contactrepresentative"

    def post(self, request, contact_id: UUID):
        """Update contact representative info."""
        log_entry = LOGGER.create(
            LogEntryType.CONTACT_REPRESENTATIVE_DELETE,
            source_ip=self.request.META.get("REMOTE_ADDR", ""),
            session_id=self.request.session[LOGGER_SESSION_KEY],
            properties={"uuid": LogProperty(str(contact_id), None)},
        )

        with log_entry:
            log_entry.references["contact"] = str(contact_id)
            try:
                representative = ContactRepresentative.objects.get(contact_uuid=contact_id, valid_to=None)
            except ContactRepresentative.DoesNotExist:
                log_entry.result = LoggerResult.FAIL
                raise Http404(_("Contact does not have a representative")) from None
            else:
                representative.valid_to = timezone.now()
                representative.save()
                log_entry.result = LoggerResult.SUCCESS
                return HttpResponse()


@login_required
@permission_required("ferda.can_view_registry")
def contact_badges(request, contact_id: UUID):
    """Fetch contact badges.

    Args:
        request: Http request.
        contact_id: Contact UUID in backend.
    """
    badges = get_contact_badges(contact_id)
    return JsonResponse([dataclasses.asdict(badge) for badge in badges], safe=False)
