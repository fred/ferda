"""Report ajax views."""

import json
from decimal import Decimal
from http import HTTPStatus

from django.contrib.auth.mixins import LoginRequiredMixin
from django.http import Http404, JsonResponse, StreamingHttpResponse
from django.utils.formats import number_format
from django.views.generic import View
from grill import LogProperty
from guardian.mixins import PermissionRequiredMixin
from reporter import AmbiguousColumnNames

from ferda.backend import LOGGER, REPORTS
from ferda.constants import LOGGER_SESSION_KEY, LogEntryType, LoggerResult
from ferda.models import Report


def format_row(result_row):
    """Format report result row."""
    result = result_row.copy()
    for key, value in result_row.items():
        if isinstance(value, (Decimal, int, float)):
            result[key] = number_format(value)
    return result


class ReportRunView(LoginRequiredMixin, PermissionRequiredMixin, View):
    """Run report."""

    permission_required = "ferda.run_report"
    accept_global_perms = True
    return_403 = True

    def get_object(self):
        """Get report object from database in order to check permissions."""
        report_name = self.kwargs.get("report_name")
        try:
            report_object = Report.objects.get(service=self.kwargs.get("service"), name=report_name)
        except Report.DoesNotExist:
            raise Http404("Report %(name)s not found" % {"name": report_name}) from None
        return report_object

    def post(self, request, service, report_name):
        """Run report.

        Request should contain JSON encoded dictionary with report kwargs.
        """
        report = REPORTS[service].get_report(report_name)
        if not report:
            raise Http404("Report %(name)s not found" % {"name": report_name})

        data = json.loads(request.body.decode("utf-8"))
        report_args = []
        missing_args = []
        try:
            for param in report.input_parameters:
                report_args.append(data[param.name])
        except KeyError:
            missing_args.append(param)

        if missing_args:
            return JsonResponse(
                {"errors": {arg.name: ["Argument is missing."] for arg in missing_args}},
                status=HTTPStatus.BAD_REQUEST,
            )

        log_entry = LOGGER.create(
            LogEntryType.REPORT_RUN,
            source_ip=self.request.META.get("REMOTE_ADDR", ""),
            session_id=self.request.session[LOGGER_SESSION_KEY],
            properties={
                "service": LogProperty(service, None),
                "report": LogProperty(report_name, None),
                "args": [LogProperty(str(arg), None) for arg in report_args],
            },
        )
        with log_entry:
            try:
                result = REPORTS[service].run_report(report_name, *report_args)
            except AmbiguousColumnNames:
                log_entry.result = LoggerResult.FAIL
                raise  # This should not happen, it means the report is defective
            log_entry.result = LoggerResult.SUCCESS

        return StreamingHttpResponse(
            # StreamingHttpResponse expects bytestring iterator
            ("{}\n".format(json.dumps(format_row(row))).encode("utf-8") for row in result),
            content_type="text/plain",
        )
