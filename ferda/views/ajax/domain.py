"""Domain related ajax views."""

from datetime import datetime
from typing import Dict, Optional, Union
from uuid import UUID

from django.contrib.auth.mixins import LoginRequiredMixin, PermissionRequiredMixin

from ferda.backend import BACKEND
from ferda.backend.exceptions import DomainDoesNotExist
from ferda.constants import LogEntryType

from .common import AbstractInfoView, AbstractStateHistoryView, AbstractStateView


class DomainStateView(LoginRequiredMixin, PermissionRequiredMixin, AbstractStateView):
    """Domain state view."""

    permission_required = "ferda.can_view_registry"

    log_entry_type = LogEntryType.DOMAIN_STATE
    log_entry_reference_name = "domain"
    object_does_not_exist_exception = DomainDoesNotExist

    def get_object_state(self, object_id: UUID) -> Dict[str, bool]:
        """Get domain state."""
        return BACKEND.get_domain_state(domain_id=object_id)


class DomainInfoView(LoginRequiredMixin, PermissionRequiredMixin, AbstractInfoView):
    """Domain info view."""

    permission_required = "ferda.can_view_registry"

    log_entry_type = LogEntryType.DOMAIN_DETAIL
    log_entry_reference_name = "domain"
    object_does_not_exist_exception = DomainDoesNotExist

    def get_object_info(self, object_id: UUID, history_id: Optional[Union[UUID, datetime]] = None) -> dict:
        """Get domain info."""
        if history_id is not None:
            assert isinstance(history_id, UUID)  # noqa: S101
        return BACKEND.get_domain_info(domain_id=object_id, history_id=history_id)


class DomainStateHistoryView(LoginRequiredMixin, PermissionRequiredMixin, AbstractStateHistoryView):
    """Domain state history view."""

    permission_required = "ferda.can_view_registry"

    log_entry_type = LogEntryType.DOMAIN_STATE
    log_entry_reference_name = "domain"
    object_does_not_exist_exception = DomainDoesNotExist

    def get_object_state_history(self, object_id: UUID) -> dict:
        """Get domain state history."""
        return BACKEND.get_domain_state_history(domain_id=object_id)
