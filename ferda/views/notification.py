"""Notification module views."""

from django.contrib.auth.mixins import LoginRequiredMixin, PermissionRequiredMixin
from django.views.generic import TemplateView


class NotificationIndexView(LoginRequiredMixin, PermissionRequiredMixin, TemplateView):
    """Notification index."""

    permission_required = ("ferda.can_update_additional_contacts", "ferda.can_view_registry")
    template_name = "ferda/registry/notification.html"
