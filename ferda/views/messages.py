"""Ferda message views."""

from django.contrib.auth.mixins import LoginRequiredMixin, PermissionRequiredMixin
from django.views.generic import TemplateView


class MessageListView(LoginRequiredMixin, PermissionRequiredMixin, TemplateView):
    """Message list view."""

    permission_required = "ferda.can_view_registry"
    template_name = "ferda/messages/list.html"


class MessageDetailView(LoginRequiredMixin, PermissionRequiredMixin, TemplateView):
    """Message detail view."""

    permission_required = "ferda.can_view_registry"
    template_name = "ferda/messages/detail.html"
