"""Ferda search views."""

from django.contrib.auth.mixins import LoginRequiredMixin, PermissionRequiredMixin
from django.views.generic import TemplateView


class SearchView(LoginRequiredMixin, PermissionRequiredMixin, TemplateView):
    """Search view."""

    permission_required = "ferda.can_view_registry"
    template_name = "ferda/registry/search.html"

    def get_context_data(self, **kwargs):
        """Copy important GET items to context."""
        context = super().get_context_data(**kwargs)
        context["query"] = self.request.GET.getlist("query", None)
        context["search_date"] = self.request.GET.get("history", None)
        context["object_type"] = self.request.GET.get("type", None)
        context["search_items"] = self.request.GET.getlist("items", None)
        return context
