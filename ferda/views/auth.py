"""Ferda auth views."""

from urllib.parse import urljoin

from django.contrib.auth import views as auth_views
from django.shortcuts import redirect
from django.urls import reverse
from django_fido.backends import is_fido_backend_used
from django_fido.constants import AUTHENTICATION_USER_SESSION_KEY
from django_fido.views import Fido2AuthenticationView


class LoginView(auth_views.LoginView):
    """Ferda login view."""

    redirect_authenticated_user = True
    template_name = "ferda/login.html"

    def form_valid(self, form):
        """If fido login is used, redirect to second step. Otherwise log user in."""
        if is_fido_backend_used():
            self.request.session[AUTHENTICATION_USER_SESSION_KEY] = form.get_user().pk
            if next := self.request.GET.get("next"):
                return redirect(urljoin(reverse("ferda:login-fido"), f"?next={next}"))
            else:
                return redirect(reverse("ferda:login-fido"))
        else:
            return super().form_valid(form)


class LoginFidoView(Fido2AuthenticationView):
    """Override second authentication step template."""

    template_name = "ferda/login-fido.html"
