"""Generic registry object processing code."""

from datetime import datetime
from typing import Any, Callable, Dict, Optional

from asgiref.sync import sync_to_async
from django.http import HttpRequest
from django.utils.translation import gettext as _
from fred_types import BaseObjectHistoryId, BaseObjectId
from regal.exceptions import InvalidHistoryInterval, ObjectDoesNotExist

from ferda.api import Error, check_permissions
from ferda.backend import LOGGER
from ferda.constants import LOGGER_SESSION_KEY, LogEntryType, LoggerResult, LogReferenceType

from .constants import ObjectType


def _create_log_entry(request: HttpRequest, log_entry_type: LogEntryType, properties: Optional[Dict[str, Any]] = None):
    """Help to create log entry with common set of parameters."""
    return LOGGER.create(
        log_entry_type,
        source_ip=request.META.get("REMOTE_ADDR", ""),
        session_id=request.session[LOGGER_SESSION_KEY],
        properties=properties,
    )


async def get_object_info(
    request: HttpRequest,
    object_id: BaseObjectId,
    object_type: ObjectType,
    log_entry_type: LogEntryType,
    log_reference_type: LogReferenceType,
    info_call: Callable,
) -> Any:
    """Get object current information.

    Args:
        request: HTTP request
        object_id: Registry object identifier
        object_type: Type of registry object being processed
        log_entry_type: Log entry type
        log_reference_type: Type of object referenced in log entry
        info_call: Function to retrieve registry object information

    Returns:
        object details when object sucessfully retrieved or
        HTTP error code with error description as tuple when anything goes wrong

    Note: Function fails with HTTP 404 response code if object is not currently present (was deleted).
    """
    await sync_to_async(check_permissions)(request, permissions=["ferda.can_view_registry"])
    with _create_log_entry(request, log_entry_type, {object_type: object_id}) as log_entry:
        try:
            registry_object = await info_call(object_id)
        except ObjectDoesNotExist:
            log_entry.result = LoggerResult.FAIL
            return 404, Error(detail=_("Object does not exist"))
        log_entry.result = LoggerResult.SUCCESS
        log_entry.references = {log_reference_type: object_id}  # output reference only when object found
        return registry_object


async def get_object_history_list(
    request: HttpRequest,
    object_id: BaseObjectId,
    object_type: ObjectType,
    log_entry_type: LogEntryType,
    log_reference_type: LogReferenceType,
    history_list_call: Callable,
) -> Any:
    """Get object history list (full).

    Args:
        request: HTTP request
        object_id: Registry object identifier
        object_type: Type of registry object being processed
        log_entry_type: Log entry type
        log_reference_type: Type of object referenced in log entry
        history_list_call: Function to retrieve object history list

    Returns:
        object history list (full) when object exists in registry or
        HTTP error code with error description as tuple when anything goes wrong
    """
    await sync_to_async(check_permissions)(request, permissions=["ferda.can_view_registry"])
    with _create_log_entry(request, log_entry_type, {object_type: object_id}) as log_entry:
        try:
            history_list = await history_list_call(object_id)
        except ObjectDoesNotExist:
            log_entry.result = LoggerResult.FAIL
            return 404, Error(detail=_("Object does not exist"))
        log_entry.result = LoggerResult.SUCCESS
        log_entry.references = {log_reference_type: object_id}  # output reference only when object found
        return history_list


async def get_object_history_info(
    request: HttpRequest,
    object_id: BaseObjectId,
    object_history_id: BaseObjectHistoryId,
    object_type: ObjectType,
    log_entry_type: LogEntryType,
    log_reference_type: LogReferenceType,
    info_call: Callable,
) -> Any:
    """Get object history information (by identifier).

    Args:
        request: HTTP request
        object_id: Registry object identifier
        object_history_id: Registry object history identifier
        object_type: Type of registry object being processed
        log_entry_type: Log entry type
        log_reference_type: Type of object referenced in log entry
        info_call: Function to retrieve registry object information

    Returns:
        object details when object sucessfully retrieved or
        HTTP error code with error description as tuple when anything goes wrong
    """
    await sync_to_async(check_permissions)(request, permissions=["ferda.can_view_registry"])
    properties = {object_type: object_id, "history_id": object_history_id}
    with _create_log_entry(request, log_entry_type, properties) as log_entry:
        try:
            registry_object = await info_call(object_id, object_history_id)
        except ObjectDoesNotExist:
            log_entry.result = LoggerResult.FAIL
            return 404, Error(detail=_("Object does not exist"))
        log_entry.result = LoggerResult.SUCCESS
        log_entry.references = {log_reference_type: object_id}  # output reference only when object found
        return registry_object


async def get_object_history_info_datetime(
    request: HttpRequest,
    object_id: BaseObjectId,
    when: datetime,
    object_type: ObjectType,
    object_history_id_name: str,
    log_entry_type: LogEntryType,
    log_reference_type: LogReferenceType,
    history_list_call: Callable,
    info_call: Callable,
) -> Any:
    """Get object history information (by datetime).

    Args:
        request: HTTP request
        object_id: Registry object identifier
        when: Datetime point to get history for
        object_type: Type of registry object being processed
        object_history_id_name: Name of object history identifier
        log_entry_type: Log entry type
        log_reference_type: Type of object referenced in log entry
        history_list_call: Function to retrieve object history list
        info_call: Function to retrieve registry object information

    Returns:
        object details when object sucessfully retrieved or
        HTTP error code with error description as tuple when anything goes wrong
    """
    await sync_to_async(check_permissions)(request, permissions=["ferda.can_view_registry"])
    properties = {object_type: object_id, "history_datetime": when.isoformat()}
    with _create_log_entry(request, log_entry_type, properties) as log_entry:
        try:
            # get object history identifier from history list
            history_list = await history_list_call(object_id, start=when)
            if not history_list["timeline"]:  # on empty history list
                log_entry.result = LoggerResult.FAIL
                return 404, Error(detail=_("Object does not have a history"))
            object_history_id: BaseObjectHistoryId = history_list["timeline"][0][object_history_id_name]
            # get object history info
            registry_object = await info_call(object_id, object_history_id)
        except ObjectDoesNotExist:
            log_entry.result = LoggerResult.FAIL
            return 404, Error(detail=_("Object does not exist"))
        except InvalidHistoryInterval:
            log_entry.result = LoggerResult.FAIL
            return 404, Error(detail=_("Invalid history interval"))
        log_entry.result = LoggerResult.SUCCESS
        log_entry.properties = {"found_history_id": object_history_id}
        log_entry.references = {log_reference_type: object_id}  # output reference only when object found
        return registry_object


async def get_object_history_info_last(
    request: HttpRequest,
    object_id: BaseObjectId,
    object_type: ObjectType,
    object_history_id_name: str,
    log_entry_type: LogEntryType,
    log_reference_type: LogReferenceType,
    history_list_call: Callable,
    info_call: Callable,
) -> Any:
    """Get object history information (last known).

    Args:
        request: HTTP request
        object_id: Registry object identifier
        object_type: Type of registry object being processed
        object_history_id_name: Name of object history identifier
        log_entry_type: Log entry type
        log_reference_type: Type of object referenced in log entry
        history_list_call: Function to retrieve object history list
        info_call: Function to retrieve registry object information

    Returns:
        object details when object sucessfully retrieved or
        HTTP error code with error description as tuple when anything goes wrong
    """
    await sync_to_async(check_permissions)(request, permissions=["ferda.can_view_registry"])
    with _create_log_entry(request, log_entry_type, {object_type: object_id}) as log_entry:
        try:
            # get object history identifier from history list
            history_list = await history_list_call(object_id)
            if not history_list["timeline"]:  # on empty history list
                log_entry.result = LoggerResult.FAIL
                return 404, Error(detail=_("Object does not have a history"))
            object_history_id: BaseObjectHistoryId = history_list["timeline"][-1][object_history_id_name]
            # get object history info
            registry_object = await info_call(object_id, object_history_id)
        except ObjectDoesNotExist:
            log_entry.result = LoggerResult.FAIL
            return 404, Error(detail=_("Object does not exist"))
        log_entry.result = LoggerResult.SUCCESS
        log_entry.properties = {"found_history_id": object_history_id}
        log_entry.references = {log_reference_type: object_id}  # output reference only when object found
        return registry_object


async def get_object_state(
    request: HttpRequest,
    object_id: BaseObjectId,
    object_type: ObjectType,
    log_entry_type: LogEntryType,
    log_reference_type: LogReferenceType,
    state_call: Callable,
) -> Any:
    """Get object current state.

    Args:
        request: HTTP request
        object_id: Registry object identifier
        object_type: Type of registry object being processed
        log_entry_type: Log entry type
        log_reference_type: Type of object referenced in log entry
        state_call: Function to retrieve registry object information

    Returns:
        object state when object sucessfully retrieved or
        HTTP error code with error description as tuple when anything goes wrong
    """
    await sync_to_async(check_permissions)(request, permissions=["ferda.can_view_registry"])
    with _create_log_entry(request, log_entry_type, {object_type: object_id}) as log_entry:
        try:
            object_state = await state_call(object_id)
        except ObjectDoesNotExist:
            log_entry.result = LoggerResult.FAIL
            return 404, Error(detail=_("Object does not exist"))
        log_entry.result = LoggerResult.SUCCESS
        log_entry.references = {log_reference_type: object_id}  # output reference only when object found
        return object_state


async def get_object_state_history(
    request: HttpRequest,
    object_id: BaseObjectId,
    object_type: ObjectType,
    log_entry_type: LogEntryType,
    log_reference_type: LogReferenceType,
    state_history_call: Callable,
) -> Any:
    """Get object state history.

    Args:
        request: HTTP request
        object_id: Registry object identifier
        object_type: Type of registry object being processed
        log_entry_type: Log entry type
        log_reference_type: Type of object referenced in log entry
        state_history_call: Function to retrieve registry object history information

    Returns:
        object state history when sucessfully retrieved or
        HTTP error code with error description as tuple when anything goes wrong
    """
    await sync_to_async(check_permissions)(request, permissions=["ferda.can_view_registry"])
    with _create_log_entry(request, log_entry_type, {object_type: object_id, "history": "true"}) as log_entry:
        try:
            object_state_history = await state_history_call(object_id)
        except ObjectDoesNotExist:
            log_entry.result = LoggerResult.FAIL
            return 404, Error(detail=_("Object does not exist"))
        log_entry.result = LoggerResult.SUCCESS
        log_entry.references = {log_reference_type: object_id}  # output reference only when object found
        return object_state_history
