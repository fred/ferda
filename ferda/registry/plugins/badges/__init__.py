"""Badges plugin."""

from .common import AbstractBadger, Badge

__all__ = [
    "AbstractBadger",
    "Badge",
]
