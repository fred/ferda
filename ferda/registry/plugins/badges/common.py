"""Ferda badges plugin common classes."""

from abc import ABC
from dataclasses import dataclass
from typing import List
from uuid import UUID


@dataclass
class Badge:
    """Registry object badge.

    Attributes:
        name: Name of the badge.
        image: Image URL.
    """

    name: str
    image: str


class AbstractBadger(ABC):  # noqa B024
    """Abstract class for registry badge providers.

    Defines methods returning badge list for individual registry objects.

    This class is DEPRECATED.
    """

    @staticmethod
    def get_contact_badges(contact_id: UUID) -> List[Badge]:
        """Return contact badges."""
        return []
