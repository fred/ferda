"""Ferda badges plugin utils."""

from typing import List
from uuid import UUID

import ferda.settings

from .common import Badge


def get_contact_badges(contact_id: UUID) -> List[Badge]:
    """Get contact badges from all badgers."""
    badges: List[Badge] = []
    for badger in ferda.settings.SETTINGS.badgers:
        badges += badger.get_contact_badges(contact_id)
    return badges
