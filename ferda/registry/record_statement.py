"""Registry record statement module."""

from typing import Callable

from django.http import HttpResponse
from regal.exceptions import ObjectDoesNotExist

from ferda.api import Error
from ferda.backend import LOGGER
from ferda.constants import LOGGER_SESSION_KEY, LogEntryType, LoggerResult
from ferda.settings import SETTINGS


async def get_object_statement_pdf(request, object_id, log_reference_type, get_pdf_file: Callable):
    """Assemble HttpResponse from PDF file.

    Args:
        request: http request.
        object_id: Particular ID of the object.
        log_reference_type: Type of the reference.
        get_pdf_file: STATEMENTOR method for particular object_type

    Returns:
        HttpResponse with PDF file.
    """
    response = HttpResponse(content_type="application/pdf")
    response["Content-Disposition"] = 'inline; filename="record-statement.pdf"'

    log_entry = LOGGER.create(
        LogEntryType.RECORD_STATEMENT,
        source_ip=request.META.get("REMOTE_ADDR", ""),
        session_id=request.session[LOGGER_SESSION_KEY],
        references={log_reference_type: object_id},
    )

    with log_entry:
        try:
            response.content = await get_pdf_file(object_id, private=True, timeout=SETTINGS.record_statement_timeout)
            log_entry.result = LoggerResult.SUCCESS
        except ObjectDoesNotExist:
            log_entry.result = LoggerResult.FAIL
            return 404, Error(detail="Object does not exist.")
        return response
