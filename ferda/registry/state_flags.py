"""Registry state flags module."""

from typing import Optional

from ferda.settings import SETTINGS


def get_state_flag_description(flag: str) -> Optional[str]:
    """Return state flag description.

    Args:
        flag: State flag name.

    Returns:
        State flag description or flag name, if description is not available.
    """
    return SETTINGS.state_flags_descriptions.get(flag, flag)
