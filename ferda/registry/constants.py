"""Registry constants."""

from enum import Enum, unique


@unique
class ObjectType(str, Enum):
    """Registry object types."""

    CONTACT = "contact"
    DOMAIN = "domain"
    NSSET = "nsset"
    KEYSET = "keyset"
