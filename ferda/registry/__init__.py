"""Registry module."""

from .constants import ObjectType

__all__ = ["ObjectType"]
