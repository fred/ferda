"""Registry registrar schemas."""

from datetime import datetime
from typing import Any, Dict, Optional, Sequence

import regal
from ninja import Schema
from pydantic import Field, model_validator
from regal.constants import NOOP
from regal.registrar import EppCredentialsId, ZoneAccess


class RegistrarDetail(Schema):
    """Registrar schema."""

    info: regal.Registrar
    zone_access: Dict[str, Any]
    credit: Optional[Dict[str, Any]] = None


class RegistrarUpdate(Schema):
    """Registrar update schema."""

    registrar_handle: Optional[str] = None
    name: Optional[str] = None
    organization: Optional[str] = None
    place: Optional[regal.Address] = None
    telephone: Optional[str] = None
    fax: Optional[str] = None
    emails: Optional[Sequence[str]] = None
    url: Optional[str] = None
    is_system_registrar: Optional[bool] = None
    company_registration_number: Optional[str] = None
    vat_identification_number: Optional[str] = None
    variable_symbol: Optional[str] = None
    payment_memo_regex: Optional[str] = None
    is_vat_payer: Optional[bool] = None
    is_internal: Optional[bool] = None


class ZoneAccessUpdate(Schema):
    """Zone access update schema."""

    access: ZoneAccess
    # XXX: Pydantic makes copy of field value which destroys purpose
    # of the singleton. We work around this limitation by using
    # default_factory instead of the default value.
    valid_from: Optional[datetime] = Field(default_factory=lambda: NOOP)
    valid_to: Optional[datetime] = Field(default_factory=lambda: NOOP)


class SslCertificate(Schema):
    """SSL certificate with some important properties."""

    fingerprint: str
    cert_data_pem: str = ""
    subject: Optional[str] = None
    issuer: Optional[str] = None
    not_valid_before: Optional[datetime] = None
    not_valid_after: Optional[datetime] = None


class EppCredentials(Schema):
    """Registrar EPP credentials."""

    credentials_id: EppCredentialsId
    create_time: Optional[datetime] = None
    certificate: SslCertificate


class EppCredentialsCreate(Schema):
    """Create registrar EPP credentials."""

    fingerprint: Optional[str] = None
    cert_data_pem: Optional[str] = None
    password: Optional[str] = None
    template_credentials_id: Optional[EppCredentialsId] = None

    @model_validator(mode="after")
    def _check_required_fingerprint_cert(self):
        if (self.fingerprint is None) == (self.cert_data_pem is None):
            raise ValueError("Exactly one of fingerprint and cert_data_pem has to be defined")
        return self

    @model_validator(mode="after")
    def _check_required_password_template(self):
        if (self.password is None) == (self.template_credentials_id is None):
            raise ValueError("Exactly one of password and template_credentials_id has to be defined")
        return self


class EppCredentialsUpdate(Schema):
    """Update registrar EPP credentials."""

    password: str
