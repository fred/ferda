"""Registrar EPP credentials API."""

from typing import List, cast

from asgiref.sync import sync_to_async
from django.utils.translation import gettext as _
from ninja import Router
from regal.exceptions import EppCredentialsDoesNotExist, InvalidData, RegistrarDoesNotExist
from regal.registrar import EppCredentialsId, SslCertificate

from ferda.api import Error, InvalidDataError, check_permissions
from ferda.backend import LOGGER, REGISTRAR, REGISTRAR_ADMIN
from ferda.constants import LOGGER_SESSION_KEY, LogEntryType, LoggerResult
from ferda.utils import removeprefix

from .schema import EppCredentials, EppCredentialsCreate, EppCredentialsUpdate

router = Router(tags=["registry", "registrar"])


@router.get(
    "/{handle}/epp_credentials/",
    url_name="registrar_epp_credentials",
    response={200: List[EppCredentials], 404: Error},
)
async def get_epp_credentials(request, handle: str):
    """Get registrar EPP credentials."""
    await sync_to_async(check_permissions)(request, permissions=["ferda.can_view_registry"])

    log_entry = LOGGER.create(
        LogEntryType.REGISTRAR_EPP_CREDENTIALS_VIEW,
        source_ip=request.META.get("REMOTE_ADDR", ""),
        session_id=request.session[LOGGER_SESSION_KEY],
        references={"registrar": handle},
    )
    with log_entry:
        try:
            epp_credentials = await REGISTRAR.get_registrar_epp_credentials(handle)
        except RegistrarDoesNotExist:
            log_entry.result = LoggerResult.FAIL
            return 404, Error(detail=_("Registrar does not exist"))

        for credentials in epp_credentials:
            cert = credentials["certificate"]
            credentials["certificate"] = dict(
                **cert.model_dump(),
                subject=cert.subject,
                issuer=cert.issuer,
                not_valid_before=cert.not_valid_before,
                not_valid_after=cert.not_valid_after,
            )
        log_entry.result = LoggerResult.SUCCESS

    return epp_credentials


@router.post(
    "/{handle}/epp_credentials/",
    url_name="registrar_epp_credentials",
    response={200: EppCredentialsId, 404: Error, 400: InvalidDataError},
)
async def create_epp_credentials(request, handle: str, epp_credentials: EppCredentialsCreate):
    """Create registrar EPP credentials."""
    await sync_to_async(check_permissions)(request, permissions=["ferda.can_edit_registrar"])

    log_entry = LOGGER.create(
        LogEntryType.REGISTRAR_EPP_CREDENTIALS_CREATE,
        source_ip=request.META.get("REMOTE_ADDR", ""),
        session_id=request.session[LOGGER_SESSION_KEY],
        properties={
            key: str(value)
            for key, value in epp_credentials.model_dump(
                exclude_unset=True,
                exclude={"password", "cert_data_pem"},
            ).items()
        },
        content=epp_credentials.cert_data_pem,  # type: ignore[arg-type]
        references={"registrar": handle},
    )
    with log_entry:
        if epp_credentials.cert_data_pem is not None:
            try:
                cert = SslCertificate.from_pem(epp_credentials.cert_data_pem)
            except ValueError:
                log_entry.result = LoggerResult.FAIL
                return 400, InvalidDataError(detail=_("Invalid certificate"), fields=["cert_data_pem"])
        else:
            try:
                cert = SslCertificate(fingerprint=cast(str, epp_credentials.fingerprint))
            except ValueError:
                log_entry.result = LoggerResult.FAIL
                return 400, InvalidDataError(detail=_("Invalid fingerprint"), fields=["fingerprint"])

        try:
            credentials_id = await REGISTRAR_ADMIN.add_epp_credentials(
                handle,
                cert,
                password=epp_credentials.password,
                template_credentials_id=epp_credentials.template_credentials_id,
            )
            log_entry.result = LoggerResult.SUCCESS
            return credentials_id
        except RegistrarDoesNotExist:
            log_entry.result = LoggerResult.FAIL
            return 404, Error(detail=_("Registrar does not exist"))
        except EppCredentialsDoesNotExist:
            log_entry.result = LoggerResult.FAIL
            return 404, Error(detail=_("EPP credentials does not exist"))
        except InvalidData as err:
            log_entry.result = LoggerResult.FAIL
            return 400, InvalidDataError(
                detail=_("Invalid data"),
                fields=[removeprefix(field, "certificate.") for field in err.fields],
            )


@router.put(
    "/{handle}/epp_credentials/{credentials_id}/",
    url_name="registrar_epp_credential",
    response={200: None, 404: Error, 400: InvalidDataError},
)
async def update_epp_credentials(request, handle: str, credentials_id: EppCredentialsId, data: EppCredentialsUpdate):
    """Update registrar EPP credentials."""
    await sync_to_async(check_permissions)(request, permissions=["ferda.can_edit_registrar"])

    log_entry = LOGGER.create(
        LogEntryType.REGISTRAR_EPP_CREDENTIALS_UPDATE,
        source_ip=request.META.get("REMOTE_ADDR", ""),
        session_id=request.session[LOGGER_SESSION_KEY],
        properties={"credentials_id": credentials_id},
        references={"registrar": handle},
    )
    with log_entry:
        try:
            await REGISTRAR_ADMIN.update_epp_credentials(credentials_id, password=data.password)
            log_entry.result = LoggerResult.SUCCESS
        except EppCredentialsDoesNotExist:
            log_entry.result = LoggerResult.FAIL
            return 404, Error(detail=_("EPP credentials does not exist"))
        except InvalidData as err:
            log_entry.result = LoggerResult.FAIL
            return 400, InvalidDataError(
                detail=_("Invalid data"),
                fields=[removeprefix(field, "certificate.") for field in err.fields],
            )


@router.delete(
    "/{handle}/epp_credentials/{credentials_id}/",
    url_name="registrar_epp_credential",
    response={200: None, 404: Error},
)
async def delete_epp_credentials(request, handle: str, credentials_id: EppCredentialsId):
    """Delete registrar EPP credentials."""
    await sync_to_async(check_permissions)(request, permissions=["ferda.can_edit_registrar"])

    log_entry = LOGGER.create(
        LogEntryType.REGISTRAR_EPP_CREDENTIALS_DELETE,
        source_ip=request.META.get("REMOTE_ADDR", ""),
        session_id=request.session[LOGGER_SESSION_KEY],
        properties={"credentials_id": credentials_id},
        references={"registrar": handle},
    )
    with log_entry:
        try:
            await REGISTRAR_ADMIN.delete_epp_credentials(credentials_id)
            log_entry.result = LoggerResult.SUCCESS
        except EppCredentialsDoesNotExist:
            log_entry.result = LoggerResult.FAIL
            return 404, Error(detail=_("EPP credentials does not exist"))
