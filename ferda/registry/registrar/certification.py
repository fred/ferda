"""Registrar certification REST API."""

from datetime import datetime
from mimetypes import guess_type
from typing import List, Optional

from asgiref.sync import sync_to_async
from django.http import StreamingHttpResponse
from django.utils.translation import gettext as _
from grill import Properties
from ninja import File, Form, Router, Schema, UploadedFile
from pydantic import field_validator
from regal import RegistrarCertification
from regal.exceptions import InvalidData, RegistrarCertificationDoesNotExist, RegistrarDoesNotExist

from ferda.api import Error, InvalidDataError, check_permissions
from ferda.backend import FILE_STORAGE, LOGGER, REGISTRAR, REGISTRAR_ADMIN
from ferda.constants import LOGGER_SESSION_KEY, LogEntryType, LoggerResult, LogReferenceType

router = Router(tags=["registry", "registrar"])


async def _check_registrar_of_certification(registrar_id: str, certification_id: str) -> None:
    """Check that certification is linked to registrar.

    Args:
        registrar_id: Registrar identification.
        certification_id: Certification identification.

    Raises:
        RegistrarDoesNotExist on registrar not found
        RegistrarCertificationDoesNotExist on certification not found for a registrar
    """
    certifications = await REGISTRAR.get_registrar_certifications(registrar_id)  # might raise RegistrarDoesNotExist
    if certification_id not in [i.certification_id for i in certifications]:
        raise RegistrarCertificationDoesNotExist  # certification not found (for this registrar)


def _store_uploaded_file(file: UploadedFile) -> str:
    """Store uploaded file using file manager.

    Args:
        file: File object encapsulating uploaded file.

    Returns:
        UID of stored file.

    Raises:
        I/O related exceptions when file storage/transfer fails.
    """
    # try to determine MIME type with fallback to arbitrary binary data (known to be far from perfect)
    mimetype, _ = guess_type(file.name)
    with FILE_STORAGE.create(file.name, mimetype=mimetype or "application/octet-stream") as storage:
        for chunk in file.chunks():
            storage.write(chunk)
    assert storage.uid is not None, "The file was just created, it has to exist"  # noqa: S101
    return storage.uid


@router.get(
    "/{registrar_id}/certification/",
    url_name="registrar_certification",
    response={200: List[RegistrarCertification], 404: Error},
    summary="Get registrar certifications",
)
async def get_registrar_certifications(request, registrar_id: str):
    """Get registrar certifications."""
    await sync_to_async(check_permissions)(request, permissions=["ferda.can_view_registry"])

    log_entry = LOGGER.create(
        LogEntryType.REGISTRAR_CERTIFICATION_VIEW,
        source_ip=request.META.get("REMOTE_ADDR", ""),
        session_id=request.session[LOGGER_SESSION_KEY],
        properties={"registrar_id": registrar_id},
    )
    with log_entry:
        try:
            certifications = await REGISTRAR.get_registrar_certifications(registrar_id)
        except RegistrarDoesNotExist:
            log_entry.result = LoggerResult.FAIL
            return 404, Error(detail=_("Registrar does not exist"))
        log_entry.result = LoggerResult.SUCCESS
        log_entry.references = {LogReferenceType.REGISTRAR: registrar_id}
        if certifications:
            log_entry.properties = {"items_count": str(len(certifications))}

        return certifications


@router.get(
    "/{registrar_id}/certification_file/{file_id}/",
    url_name="registrar_certification_file",
    response={200: bytes, 404: Error},
    summary="Get registrar certifications file",
)
async def get_registrar_certification_file(request, registrar_id: str, file_id: str):
    """Get registrar certification file."""
    try:
        # get all certifications of registrar and try to find file_id
        certifications = await REGISTRAR.get_registrar_certifications(registrar_id)
        if file_id not in [i.file_id for i in certifications]:
            raise RegistrarCertificationDoesNotExist  # certification file not found (for this registrar)
        # get certification file using file manager
        with FILE_STORAGE.open(file_id) as file:
            response = StreamingHttpResponse(file, content_type=file.mimetype)
            response["Content-Disposition"] = 'inline; filename="{}"'.format(file.name)
            return response
    except RegistrarDoesNotExist:
        return 404, Error(detail=_("Registrar does not exist"))
    except RegistrarCertificationDoesNotExist:
        return 404, Error(detail=_("Certification does not exist"))
    except FileNotFoundError:
        return 404, Error(detail=_("File %(uid)s not found") % {"uid": file_id})


class AddCertificationParams(Schema):
    """Body parameters to 'add_registrar_certification'."""

    classification: int
    valid_from: Optional[datetime] = None
    valid_to: Optional[datetime] = None

    @field_validator("valid_from", "valid_to", mode="before")
    @classmethod
    def empty_string_to_none(cls, value):
        """Convert empty string to None."""
        # multipart/form-data cannot encode 'None' - may pass "" instead (or omit parameter completely)
        return value if value else None


@router.post(
    "/{registrar_id}/certification/",
    url_name="registrar_certification",
    response={200: None, 400: InvalidDataError, 404: Error},
    summary="Add registrar certification",
)
async def add_registrar_certification(
    request, registrar_id: str, params: AddCertificationParams = Form(...), file: UploadedFile = File(...)
):
    """Add registrar certification."""
    await sync_to_async(check_permissions)(request, permissions=["ferda.can_edit_registrar"])

    properties: Properties = {
        "registrar_id": registrar_id,
        "classification": str(params.classification),
    }
    if params.valid_from:
        properties["valid_from"] = params.valid_from.isoformat()
    if params.valid_to:
        properties["valid_to"] = params.valid_to.isoformat()
    log_entry = LOGGER.create(
        LogEntryType.REGISTRAR_CERTIFICATION_CREATE,
        source_ip=request.META.get("REMOTE_ADDR", ""),
        session_id=request.session[LOGGER_SESSION_KEY],
        properties=properties,
    )
    with log_entry:
        try:
            file_id = _store_uploaded_file(file)
            await REGISTRAR_ADMIN.add_registrar_certification(registrar_id, file_id=file_id, **params.model_dump())
        except InvalidData as err:
            log_entry.result = LoggerResult.FAIL
            return 400, InvalidDataError(detail=_("Invalid data"), fields=err.fields)
        except RegistrarDoesNotExist:
            log_entry.result = LoggerResult.FAIL
            return 404, Error(detail=_("Registrar does not exist"))
        # file storage errors lead to 500 Internal server error with full traceback in Sentry
        log_entry.result = LoggerResult.SUCCESS
        log_entry.properties = {"file_id": file_id}
        log_entry.references = {LogReferenceType.REGISTRAR: registrar_id}


class UpdateCertificationParams(Schema):
    """Body parameters to 'update_registrar_certification'."""

    classification: Optional[int] = None
    valid_to: Optional[datetime] = None

    @field_validator("classification", "valid_to", mode="before")
    @classmethod
    def empty_string_to_none(cls, value):
        """Convert empty string to None."""
        # multipart/form-data cannot encode 'None' - may pass "" instead (or omit parameter completely)
        return value if value else None


@router.post(
    "/{registrar_id}/certification/{certification_id}/",
    url_name="registrar_certification",
    response={200: None, 400: InvalidDataError, 404: Error},
    summary="Update registrar certification",
)
# REST common practice uses PUT for resource modification but here POST is employed because of file upload
async def update_registrar_certification(
    request,
    registrar_id: str,
    certification_id: str,
    params: UpdateCertificationParams = Form(...),
    file: Optional[UploadedFile] = File(None),
):
    """Update registrar certification."""
    await sync_to_async(check_permissions)(request, permissions=["ferda.can_edit_registrar"])

    properties: Properties = {"registrar_id": registrar_id, "certification_id": certification_id}
    if params.classification:
        properties["set_classification"] = str(params.classification)
    if params.valid_to:
        properties["set_valid_to"] = params.valid_to.isoformat()
    log_entry = LOGGER.create(
        LogEntryType.REGISTRAR_CERTIFICATION_UPDATE,
        source_ip=request.META.get("REMOTE_ADDR", ""),
        session_id=request.session[LOGGER_SESSION_KEY],
        properties=properties,
    )
    with log_entry:
        file_id = None
        try:
            await _check_registrar_of_certification(registrar_id, certification_id)
            if file:
                file_id = _store_uploaded_file(file)
            await REGISTRAR_ADMIN.update_registrar_certification(
                certification_id, file_id=file_id, **params.model_dump()
            )
        except InvalidData as err:
            log_entry.result = LoggerResult.FAIL
            return 400, InvalidDataError(detail=_("Invalid data"), fields=err.fields)
        except RegistrarCertificationDoesNotExist:
            log_entry.result = LoggerResult.FAIL
            return 404, Error(detail=_("Certification does not exist"))
        except RegistrarDoesNotExist:
            log_entry.result = LoggerResult.FAIL
            return 404, Error(detail=_("Registrar does not exist"))
        log_entry.result = LoggerResult.SUCCESS
        if file_id:
            log_entry.properties = {"set_file_id": str(file_id)}
        log_entry.references = {LogReferenceType.REGISTRAR: registrar_id}


@router.delete(
    "/{registrar_id}/certification/{certification_id}/",
    url_name="registrar_certification",
    response={200: None, 400: InvalidDataError, 404: Error},
    summary="Delete registrar certification",
)
async def delete_registrar_certification(request, registrar_id: str, certification_id: str):
    """Delete registrar certification."""
    await sync_to_async(check_permissions)(request, permissions=["ferda.can_edit_registrar"])

    log_entry = LOGGER.create(
        LogEntryType.REGISTRAR_CERTIFICATION_DELETE,
        source_ip=request.META.get("REMOTE_ADDR", ""),
        session_id=request.session[LOGGER_SESSION_KEY],
        properties={"registrar_id": registrar_id, "certification_id": certification_id},
    )
    with log_entry:
        try:
            await _check_registrar_of_certification(registrar_id, certification_id)
            await REGISTRAR_ADMIN.delete_registrar_certification(certification_id)
        except InvalidData as err:
            log_entry.result = LoggerResult.FAIL
            return 400, InvalidDataError(detail=_("Invalid data"), fields=err.fields)
        except RegistrarCertificationDoesNotExist:
            log_entry.result = LoggerResult.FAIL
            return 404, Error(detail=_("Certification does not exist"))
        except RegistrarDoesNotExist:
            log_entry.result = LoggerResult.FAIL
            return 404, Error(detail=_("Registrar does not exist"))
        log_entry.result = LoggerResult.SUCCESS
        log_entry.references = {LogReferenceType.REGISTRAR: registrar_id}
