"""Registry registrar REST API views."""

import asyncio
from typing import Any, Dict, List, Sequence, Union

from asgiref.sync import sync_to_async
from django.utils.translation import gettext as _
from grill import LogProperty, Properties
from ninja import Router
from regal import Address, Registrar
from regal.exceptions import InvalidData, RegistrarAlreadyExists, RegistrarDoesNotExist
from regal.registrar import RegistrarId

from ferda.api import Error, InvalidDataError, check_permissions
from ferda.backend import LOGGER, REGISTRAR, REGISTRAR_ADMIN
from ferda.constants import LOGGER_SESSION_KEY, LogEntryType, LoggerResult
from ferda.settings import SETTINGS
from ferda.utils import await_with_semaphore

from .schema import RegistrarDetail, RegistrarUpdate

router = Router(tags=["registry", "registrar"])


async def _get_registrar(handle: str, *, with_credit: bool, semaphore: asyncio.Semaphore) -> Dict[str, Any]:
    """Get a registrar info, zone_access and credit in case user has permission for it."""
    calls = [REGISTRAR.get_registrar_info(handle), REGISTRAR.get_registrar_zone_access(handle)]
    if with_credit:
        calls.append(REGISTRAR.get_registrar_credit(handle))

    results = await asyncio.gather(*[await_with_semaphore(call, semaphore) for call in calls])
    return {"info": results[0], "zone_access": results[1], "credit": results[2] if with_credit else None}


@router.post("/", url_name="registrars", response={200: None, 400: Union[InvalidDataError, Error]})
async def create_registrar(request, registrar: Registrar):
    """Create new registrar."""
    await sync_to_async(check_permissions)(request, permissions=["ferda.can_edit_registrar"])

    log_entry = LOGGER.create(
        LogEntryType.REGISTRAR_CREATE,
        source_ip=request.META.get("REMOTE_ADDR", ""),
        session_id=request.session[LOGGER_SESSION_KEY],
        properties={key: str(value) for key, value in registrar.model_dump(exclude_unset=True).items()},
        references={"registrar": registrar.registrar_handle},
    )
    with log_entry:
        try:
            await REGISTRAR_ADMIN.create_registrar(registrar)
            log_entry.result = LoggerResult.SUCCESS
        except InvalidData as err:
            log_entry.result = LoggerResult.FAIL
            return 400, InvalidDataError(detail=_("Invalid data"), fields=err.fields)
        except RegistrarAlreadyExists:
            log_entry.result = LoggerResult.FAIL
            return 400, Error(detail=_("Registrar already exists"))


@router.get("/", url_name="registrars", response=List[RegistrarDetail])
async def list_registrars(request):
    """Get a list of all registrars with their info, zone access and credit."""
    await sync_to_async(check_permissions)(request, permissions=["ferda.can_view_registry"])

    log_entry = LOGGER.create(
        LogEntryType.REGISTRAR_LIST,
        source_ip=request.META.get("REMOTE_ADDR", ""),
        session_id=request.session[LOGGER_SESSION_KEY],
    )

    with log_entry:
        handles = await REGISTRAR.get_registrar_handles()
        semaphore = asyncio.Semaphore(SETTINGS.concurrent_grpc_calls_limit)
        with_credit = await sync_to_async(request.user.has_perm)("ferda.can_view_registrar_credit")
        registrar_list = await asyncio.gather(
            *[_get_registrar(handle, with_credit=with_credit, semaphore=semaphore) for handle in handles]
        )

        log_entry.result = LoggerResult.SUCCESS
        log_entry.references["registrar"] = handles

    return registrar_list


@router.get("/{registrar_id}/", url_name="registrar", response={200: RegistrarDetail, 404: Error})
async def get_registrar(request, registrar_id: RegistrarId):
    """Get a single registrar info, zone access and credit."""
    await sync_to_async(check_permissions)(request, permissions=["ferda.can_view_registry"])

    log_entry = LOGGER.create(
        LogEntryType.REGISTRAR_DETAIL,
        source_ip=request.META.get("REMOTE_ADDR", ""),
        session_id=request.session[LOGGER_SESSION_KEY],
        references={"registrar": registrar_id},
    )

    with log_entry:
        try:
            semaphore = asyncio.Semaphore(SETTINGS.concurrent_grpc_calls_limit)
            with_credit = await sync_to_async(request.user.has_perm)("ferda.can_view_registrar_credit")
            registrar = await _get_registrar(registrar_id, with_credit=with_credit, semaphore=semaphore)

        except RegistrarDoesNotExist:
            log_entry.result = LoggerResult.FAIL
            return 404, Error(detail=_("Registrar does not exist"))

        log_entry.result = LoggerResult.SUCCESS
        return registrar


@router.put("/{registrar_id}/", url_name="registrar", response={200: None, 400: InvalidDataError, 404: Error})
async def update_registrar(request, registrar_id: RegistrarId, update: RegistrarUpdate):
    """Update registrar."""
    await sync_to_async(check_permissions)(request, permissions=["ferda.can_edit_registrar"])

    properties: Properties = {}
    for key, value in update:
        if value is None:
            continue

        if isinstance(value, Address):
            children = {
                "street": ", ".join(value.street),
                "city": value.city,
                "state_or_province": value.state_or_province or "",
                "postal_code": value.postal_code or "",
                "country_code": value.country_code or "",
            }
            properties[key] = LogProperty("", children=children)
        elif isinstance(value, Sequence) and not isinstance(value, str):
            properties[key] = [str(i) for i in value]
        else:
            properties[key] = str(value)

    update_kwargs = update.model_dump(exclude_unset=True)
    if update.place is not None:
        # Override 'place' since it's expected to be an Address, not dict.
        update_kwargs["place"] = update.place

    log_entry = LOGGER.create(
        LogEntryType.REGISTRAR_UPDATE,
        source_ip=request.META.get("REMOTE_ADDR", ""),
        session_id=request.session[LOGGER_SESSION_KEY],
        properties=properties,
        references={"registrar": registrar_id},
    )
    with log_entry:
        try:
            await REGISTRAR_ADMIN.update_registrar(registrar_id, **update_kwargs)
            log_entry.result = LoggerResult.SUCCESS
        except InvalidData as err:
            log_entry.result = LoggerResult.FAIL
            return 400, InvalidDataError(detail=_("Invalid data"), fields=err.fields)
        except RegistrarDoesNotExist:
            log_entry.result = LoggerResult.FAIL
            return 404, Error(detail=_("Registrar does not exist"))
