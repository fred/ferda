"""Registrar zone access REST API."""

from datetime import datetime
from typing import Dict, List, Optional, Union

from asgiref.sync import sync_to_async
from django.utils.translation import gettext as _
from grill import Properties
from ninja import Router
from regal.constants import NOOP
from regal.exceptions import HistoryChangeProhibited, InvalidData, RegistrarDoesNotExist
from regal.registrar import ZoneAccess

from ferda.api import Error, InvalidDataError, check_permissions
from ferda.backend import LOGGER, REGISTRAR, REGISTRAR_ADMIN
from ferda.constants import LOGGER_SESSION_KEY, LogEntryType, LoggerResult

from .schema import ZoneAccessUpdate

router = Router(tags=["registry", "registrar"])


def _datetimestr(d: Optional[datetime]) -> str:
    if isinstance(d, datetime):
        return d.isoformat()
    else:
        return ""


@router.get(
    "/{handle}/zone_access/",
    url_name="registrar_zone_access",
    response={200: Dict[str, List[ZoneAccess]], 404: Error},
)
async def get_zone_access(request, handle: str):
    """Get registrar zone access."""
    await sync_to_async(check_permissions)(request, permissions=["ferda.can_view_registry"])

    log_entry = LOGGER.create(
        LogEntryType.REGISTRAR_ZONE_ACCESS_VIEW,
        source_ip=request.META.get("REMOTE_ADDR", ""),
        session_id=request.session[LOGGER_SESSION_KEY],
        references={"registrar": handle},
    )
    with log_entry:
        try:
            zone_access = await REGISTRAR.get_registrar_zone_access_history(handle)
        except RegistrarDoesNotExist:
            log_entry.result = LoggerResult.FAIL
            return 404, Error(detail=_("Registrar does not exist"))
        log_entry.result = LoggerResult.SUCCESS

    return zone_access


@router.post(
    "/{handle}/zone_access/",
    url_name="registrar_zone_access",
    response={200: ZoneAccess, 400: InvalidDataError, 404: Error},
)
async def create_zone_access(request, handle: str, access: ZoneAccess):
    """Create registrar zone access."""
    await sync_to_async(check_permissions)(request, permissions=["ferda.can_edit_registrar"])

    properties: Properties = {
        "zone": access.zone,
        "valid_from": _datetimestr(access.valid_from),
        "valid_to": _datetimestr(access.valid_to),
    }
    log_entry = LOGGER.create(
        LogEntryType.REGISTRAR_ZONE_ACCESS_CREATE,
        source_ip=request.META.get("REMOTE_ADDR", ""),
        session_id=request.session[LOGGER_SESSION_KEY],
        properties=properties,
        references={"registrar": handle},
    )
    with log_entry:
        try:
            created_access = await REGISTRAR_ADMIN.add_zone_access(handle, access)
        except RegistrarDoesNotExist:
            log_entry.result = LoggerResult.FAIL
            return 404, Error(detail=_("Registrar does not exist"))
        except InvalidData as err:
            log_entry.result = LoggerResult.FAIL
            return 400, InvalidDataError(detail=_("Invalid data"), fields=err.fields)
        except ValueError:
            log_entry.result = LoggerResult.FAIL
            return 404, Error(detail=_("Zone does not exist"))
        log_entry.result = LoggerResult.SUCCESS

    return created_access


@router.put(
    "/{handle}/zone_access/",
    url_name="registrar_zone_access",
    response={200: ZoneAccess, 400: Union[InvalidDataError, Error], 404: Error},
)
async def update_zone_access(request, handle: str, update: ZoneAccessUpdate):
    """Update registrar zone access."""
    await sync_to_async(check_permissions)(request, permissions=["ferda.can_edit_registrar"])

    properties: Properties = {
        "zone": update.access.zone,
        "valid_from": _datetimestr(update.access.valid_from),
        "valid_to": _datetimestr(update.access.valid_to),
    }
    if update.valid_from is not NOOP:
        properties["update_valid_from"] = _datetimestr(update.valid_from)
    if update.valid_to is not NOOP:
        properties["update_valid_to"] = _datetimestr(update.valid_to)

    log_entry = LOGGER.create(
        LogEntryType.REGISTRAR_ZONE_ACCESS_UPDATE,
        source_ip=request.META.get("REMOTE_ADDR", ""),
        session_id=request.session[LOGGER_SESSION_KEY],
        properties=properties,
        references={"registrar": handle},
    )
    with log_entry:
        try:
            updated_access = await REGISTRAR_ADMIN.update_zone_access(
                handle,
                update.access,
                valid_from=update.valid_from,
                valid_to=update.valid_to,
            )
        except InvalidData as err:
            log_entry.result = LoggerResult.FAIL
            return 400, InvalidDataError(detail=_("Invalid data"), fields=err.fields)
        except ValueError:
            log_entry.result = LoggerResult.FAIL
            return 404, Error(detail=_("Registrar zone access does not exist"))
        except HistoryChangeProhibited:
            log_entry.result = LoggerResult.FAIL
            return 400, Error(detail=_("History change is prohibited"))
        log_entry.result = LoggerResult.SUCCESS

    return updated_access


@router.delete(
    "/{handle}/zone_access/",
    url_name="registrar_zone_access",
    response={200: None, 400: Union[InvalidDataError, Error], 404: Error},
)
async def delete_zone_access(request, handle: str, access: ZoneAccess):
    """Delete registrar zone access."""
    await sync_to_async(check_permissions)(request, permissions=["ferda.can_edit_registrar"])

    properties: Properties = {
        "zone": access.zone,
        "valid_from": _datetimestr(access.valid_from),
        "valid_to": _datetimestr(access.valid_to),
    }
    log_entry = LOGGER.create(
        LogEntryType.REGISTRAR_ZONE_ACCESS_DELETE,
        source_ip=request.META.get("REMOTE_ADDR", ""),
        session_id=request.session[LOGGER_SESSION_KEY],
        properties=properties,
        references={"registrar": handle},
    )
    with log_entry:
        try:
            await REGISTRAR_ADMIN.delete_zone_access(handle, access)
        except InvalidData as err:
            log_entry.result = LoggerResult.FAIL
            return 400, InvalidDataError(detail=_("Invalid data"), fields=err.fields)
        except ValueError:
            log_entry.result = LoggerResult.FAIL
            return 404, Error(detail=_("Registrar zone access does not exist"))
        except HistoryChangeProhibited:
            log_entry.result = LoggerResult.FAIL
            return 400, Error(detail=_("History change is prohibited"))
        log_entry.result = LoggerResult.SUCCESS
