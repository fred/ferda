"""Registrar REST API."""

from ninja import Router

from .certification import router as certification_router
from .epp_credentials import router as epp_credentials_router
from .groups import router as groups_router
from .registrar import router as registrar_router
from .zone_access import router as zone_access_router

router = Router(tags=["registry", "registrar"])

router.add_router("", registrar_router)
router.add_router("", zone_access_router)
router.add_router("", epp_credentials_router)
router.add_router("", groups_router)
router.add_router("", certification_router)
