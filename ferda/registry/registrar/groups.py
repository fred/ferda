"""Registry registrar groups REST API views."""

from typing import List

from asgiref.sync import sync_to_async
from django.utils.translation import gettext as _
from ninja import Router
from regal.exceptions import GroupDoesNotExist, RegistrarDoesNotExist

from ferda.api import Error, check_permissions
from ferda.backend import LOGGER, REGISTRAR, REGISTRAR_ADMIN
from ferda.constants import LOGGER_SESSION_KEY, LogEntryType, LoggerResult, LogReferenceType

router = Router(tags=["registry", "registrar"])


@router.get("/-/groups/", url_name="registrar_groups", response={200: List[str]}, summary="Available registrar groups")
async def get_registrar_groups(request):
    """Get list of all available registrar groups."""
    # no need to create log entry
    await sync_to_async(check_permissions)(request, permissions=["ferda.can_view_registry"])

    return await REGISTRAR.get_registrar_groups()


@router.get(
    "/{registrar_id}/group/",
    url_name="registrar_groups",
    response={200: List[str], 404: Error},
    summary="Get groups registrar belongs to",
)
async def get_registrar_groups_membership(request, registrar_id: str):
    """Get list of groups registrar belongs to."""
    await sync_to_async(check_permissions)(request, permissions=["ferda.can_view_registry"])

    log_entry = LOGGER.create(
        LogEntryType.REGISTRAR_GROUP_VIEW,
        source_ip=request.META.get("REMOTE_ADDR", ""),
        session_id=request.session[LOGGER_SESSION_KEY],
        properties={"registrar_id": registrar_id},
    )
    with log_entry:
        try:
            groups = await REGISTRAR.get_registrar_groups_membership(registrar_id)
        except RegistrarDoesNotExist:
            log_entry.result = LoggerResult.FAIL
            return 404, Error(detail=_("Registrar does not exist"))
        log_entry.result = LoggerResult.SUCCESS
        log_entry.references = {LogReferenceType.REGISTRAR: registrar_id}
        if groups:
            log_entry.properties = {"items_count": str(len(groups))}

        return groups


@router.post(
    "/{registrar_id}/group/{group}/",
    url_name="registrar_groups",
    response={200: List[str], 404: Error},
    summary="Add registrar to group",
)
async def add_registrar_group_membership(request, registrar_id: str, group: str):
    """Add registrar to group."""
    await sync_to_async(check_permissions)(request, permissions=["ferda.can_edit_registrar"])

    log_entry = LOGGER.create(
        LogEntryType.REGISTRAR_GROUP_CREATE,
        source_ip=request.META.get("REMOTE_ADDR", ""),
        session_id=request.session[LOGGER_SESSION_KEY],
        properties={"registrar_id": registrar_id, "group": group},
    )
    with log_entry:
        try:
            groups = await REGISTRAR_ADMIN.add_registrar_group_membership(registrar_id, group)
        except RegistrarDoesNotExist:
            log_entry.result = LoggerResult.FAIL
            return 404, Error(detail=_("Registrar does not exist"))
        except GroupDoesNotExist:
            log_entry.result = LoggerResult.FAIL
            return 404, Error(detail=_("Group does not exist"))
        log_entry.result = LoggerResult.SUCCESS
        log_entry.references = {LogReferenceType.REGISTRAR: registrar_id}
        log_entry.properties = {"items_count": str(len(groups))}

        return groups


@router.delete(
    "/{registrar_id}/group/{group}/",
    url_name="registrar_groups",
    response={200: List[str], 404: Error},
    summary="Delete registrar from group",
)
async def delete_registrar_group_membership(request, registrar_id: str, group: str):
    """Delete registrar from group."""
    await sync_to_async(check_permissions)(request, permissions=["ferda.can_edit_registrar"])

    log_entry = LOGGER.create(
        LogEntryType.REGISTRAR_GROUP_DELETE,
        source_ip=request.META.get("REMOTE_ADDR", ""),
        session_id=request.session[LOGGER_SESSION_KEY],
        properties={"registrar_id": registrar_id, "group": group},
    )
    with log_entry:
        try:
            groups = await REGISTRAR_ADMIN.delete_registrar_group_membership(registrar_id, group)
        except RegistrarDoesNotExist:
            log_entry.result = LoggerResult.FAIL
            return 404, Error(detail=_("Registrar does not exist"))
        except GroupDoesNotExist:
            log_entry.result = LoggerResult.FAIL
            return 404, Error(detail=_("Group does not exist"))
        log_entry.result = LoggerResult.SUCCESS
        log_entry.references = {LogReferenceType.REGISTRAR: registrar_id}
        if groups:
            log_entry.properties = {"items_count": str(len(groups))}

        return groups
