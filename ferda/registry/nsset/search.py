"""Nsset search."""

from typing import Sequence

from fred_types import NssetHistoryId
from ninja import Query, Router
from regal import Nsset, SearchResults
from regal.search import HistorySearchPeriod, SearchCurrentResult, SearchHistoryResult

from ferda.backend import NSSET, NSSET_SEARCH
from ferda.constants import LogEntryType
from ferda.registry.search import SearchHistoryQuery, SearchQuery, log_search, search_object, search_object_history

router = Router(tags=["registry", "nsset", "search"])


class NssetSearchResult(SearchCurrentResult):
    """Single nsset search result."""

    nsset: Nsset


class NssetHistorySearchPeriod(HistorySearchPeriod[NssetHistoryId]):
    """Nsset history search period."""

    nsset: Nsset


class NssetSearchHistoryResult(SearchHistoryResult):
    """Single nsset history search result."""

    last_history: Nsset
    histories: Sequence[NssetHistorySearchPeriod] = []


@router.get("/", url_name="search_nsset", response=SearchResults[NssetSearchResult])
@log_search(LogEntryType.NSSET_SEARCH)
async def search_nsset(request, query: SearchQuery = Query(None)):
    """Search nssets."""
    return await search_object(
        query=query, search_call=NSSET_SEARCH.search_nsset, info_call=NSSET.get_nsset_info, object_type="nsset"
    )


@router.get("/history/", url_name="search_nsset_history", response=SearchResults[NssetSearchHistoryResult])
@log_search(LogEntryType.NSSET_SEARCH_HISTORY)
async def search_nsset_history(request, query: SearchHistoryQuery = Query(None)):
    """Search nssets history."""
    return await search_object_history(
        query=query,
        search_call=NSSET_SEARCH.search_nsset_history,
        info_call=NSSET.get_nsset_info,
        history_call=NSSET.get_nsset_history,
        object_type="nsset",
        object_history_id="nsset_history_id",
    )
