"""Registry nsset REST API views."""

from datetime import datetime
from typing import Any, Dict

from asgiref.sync import sync_to_async
from django.http import HttpRequest
from fred_types import NssetHistoryId, NssetId
from ninja import Router
from regal import Nsset

from ferda.api import Error, check_permissions
from ferda.backend import NSSET, STATEMENTOR
from ferda.constants import LogEntryType, LogReferenceType
from ferda.registry.record_statement import get_object_statement_pdf

from ..common import (
    get_object_history_info,
    get_object_history_info_datetime,
    get_object_history_info_last,
    get_object_history_list,
    get_object_info,
    get_object_state,
    get_object_state_history,
)
from ..constants import ObjectType
from .search import router as search_router

router = Router(tags=["registry", "nsset"])
router.add_router("-/search", search_router)


@router.get(
    "/-/record_statement/{nsset_id}/",
    url_name="get_nsset_record_statement_pdf",
    response={
        200: bytes,
        404: Error,
    },
)
async def get_nsset_record_statement_pdf(request, nsset_id: NssetId):
    """Get verified record statement of the nsset."""
    await sync_to_async(check_permissions)(request, permissions=["ferda.can_view_registry"])
    return await get_object_statement_pdf(
        request, NssetId(nsset_id), LogReferenceType.NSSET, STATEMENTOR.get_nsset_statement
    )


@router.get("/{nsset_id}/", response={200: Nsset, 404: Error}, summary="Get nsset current info")
async def get_nsset_info(request: HttpRequest, nsset_id: NssetId):
    """Get current nsset details."""
    return await get_object_info(
        request=request,
        object_id=nsset_id,
        object_type=ObjectType.NSSET,
        log_entry_type=LogEntryType.NSSET_DETAIL,
        log_reference_type=LogReferenceType.NSSET,
        info_call=NSSET.get_nsset_info,
    )


@router.get("/{nsset_id}/history/", response={200: Dict[str, Any], 404: Error}, summary="Get nsset history list")
async def get_nsset_history(request: HttpRequest, nsset_id: NssetId):
    """Get nsset history_list."""
    return await get_object_history_list(
        request=request,
        object_id=nsset_id,
        object_type=ObjectType.NSSET,
        log_entry_type=LogEntryType.NSSET_HISTORY,
        log_reference_type=LogReferenceType.NSSET,
        history_list_call=NSSET.get_nsset_history,
    )


@router.get(
    "/{nsset_id}/history/{history_id}/",
    response={200: Nsset, 404: Error},
    summary="Get nsset history info (by identifier)",
)
async def get_nsset_history_info(request: HttpRequest, nsset_id: NssetId, history_id: NssetHistoryId):
    """Get nsset history details (by identifier)."""
    return await get_object_history_info(
        request=request,
        object_id=nsset_id,
        object_history_id=history_id,
        object_type=ObjectType.NSSET,
        log_entry_type=LogEntryType.NSSET_DETAIL,
        log_reference_type=LogReferenceType.NSSET,
        info_call=NSSET.get_nsset_info,
    )


@router.get(
    "/{nsset_id}/history/by_datetime/{when}/",
    response={200: Nsset, 404: Error},
    summary="Get nsset history info (by datetime)",
)
async def get_nsset_history_info_datetime(request: HttpRequest, nsset_id: NssetId, when: datetime):
    """Get nsset history details (by datetime)."""
    return await get_object_history_info_datetime(
        request=request,
        object_id=nsset_id,
        when=when,
        object_type=ObjectType.NSSET,
        object_history_id_name="nsset_history_id",
        log_entry_type=LogEntryType.NSSET_DETAIL,
        log_reference_type=LogReferenceType.NSSET,
        history_list_call=NSSET.get_nsset_history,
        info_call=NSSET.get_nsset_info,
    )


@router.get(
    "/{nsset_id}/history/-/last/", response={200: Nsset, 404: Error}, summary="Get nsset history info (last known)"
)
async def get_nsset_history_info_last(request: HttpRequest, nsset_id: NssetId):
    """Get nsset history details (last known)."""
    return await get_object_history_info_last(
        request=request,
        object_id=nsset_id,
        object_type=ObjectType.NSSET,
        object_history_id_name="nsset_history_id",
        log_entry_type=LogEntryType.NSSET_DETAIL,
        log_reference_type=LogReferenceType.NSSET,
        history_list_call=NSSET.get_nsset_history,
        info_call=NSSET.get_nsset_info,
    )


@router.get("/{nsset_id}/state/", response={200: Dict[str, bool], 404: Error}, summary="Get nsset state")
async def get_nsset_state(request: HttpRequest, nsset_id: NssetId):
    """Get nsset current state."""
    return await get_object_state(
        request=request,
        object_id=nsset_id,
        object_type=ObjectType.NSSET,
        log_entry_type=LogEntryType.NSSET_STATE,
        log_reference_type=LogReferenceType.NSSET,
        state_call=NSSET.get_nsset_state,
    )


@router.get("/{nsset_id}/state/history/", response={200: Dict[str, Any], 404: Error}, summary="Get nsset state history")
async def get_nsset_state_history(request: HttpRequest, nsset_id: NssetId):
    """Get nsset state history."""
    return await get_object_state_history(
        request=request,
        object_id=nsset_id,
        object_type=ObjectType.NSSET,
        log_entry_type=LogEntryType.NSSET_STATE,
        log_reference_type=LogReferenceType.NSSET,
        state_history_call=NSSET.get_nsset_state_history,
    )
