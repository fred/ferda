"""Domain search."""

from typing import Sequence

from fred_types import DomainHistoryId
from ninja import Query, Router
from regal import Domain, SearchResults
from regal.search import HistorySearchPeriod, SearchCurrentResult, SearchHistoryResult

from ferda.backend import DOMAIN, DOMAIN_SEARCH
from ferda.constants import LogEntryType
from ferda.registry.search import SearchHistoryQuery, SearchQuery, log_search, search_object, search_object_history

router = Router(tags=["registry", "domain", "search"])


class DomainSearchResult(SearchCurrentResult):
    """Single domain search result."""

    domain: Domain


class DomainHistorySearchPeriod(HistorySearchPeriod[DomainHistoryId]):
    """Domain history search period."""

    domain: Domain


class DomainSearchHistoryResult(SearchHistoryResult):
    """Single domain history search result."""

    last_history: Domain
    histories: Sequence[DomainHistorySearchPeriod] = []


@router.get("/", url_name="search_domain", response=SearchResults[DomainSearchResult])
@log_search(LogEntryType.DOMAIN_SEARCH)
async def search_domain(request, query: SearchQuery = Query(None)):
    """Search domains."""
    return await search_object(
        query=query, search_call=DOMAIN_SEARCH.search_domain, info_call=DOMAIN.get_domain_info, object_type="domain"
    )


@router.get("/history/", url_name="search_domain_history", response=SearchResults[DomainSearchHistoryResult])
@log_search(LogEntryType.DOMAIN_SEARCH_HISTORY)
async def search_domain_history(request, query: SearchHistoryQuery = Query(None)):
    """Search domains history."""
    return await search_object_history(
        query=query,
        search_call=DOMAIN_SEARCH.search_domain_history,
        info_call=DOMAIN.get_domain_info,
        history_call=DOMAIN.get_domain_history,
        object_type="domain",
        object_history_id="domain_history_id",
    )
