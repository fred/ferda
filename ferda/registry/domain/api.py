"""Registry domain REST API views."""

import asyncio
import json
import logging
from datetime import datetime, timedelta
from typing import Any, Callable, Dict, List, Optional, Set, cast

import pytz
from asgiref.sync import sync_to_async
from django.http import HttpRequest
from fred_types import ContactId, DomainHistoryId, DomainId
from grill import LogProperty
from ninja import Query, Router, Schema
from regal import Domain
from regal.common import DatetimeRange
from regal.domain import ContactInfo, ContactInfoType, DomainContactRole
from regal.exceptions import ContactDoesNotExist, DomainDoesNotExist

from ferda.api import Error, TokenPaginatedList, check_permissions
from ferda.backend import DOMAIN, DOMAIN_ADMIN, LOGGER, STATEMENTOR
from ferda.constants import LOGGER_SESSION_KEY, LogEntryType, LoggerResult, LogReferenceType, StateFlag
from ferda.registry.record_statement import get_object_statement_pdf
from ferda.settings import SETTINGS

from ..common import (
    get_object_history_info,
    get_object_history_info_datetime,
    get_object_history_info_last,
    get_object_history_list,
    get_object_info,
    get_object_state,
    get_object_state_history,
)
from ..constants import ObjectType
from .schema import DomainByContact, DomainContactInfo, DomainIdByFqdn, UpdateDomainAdditionalContacts
from .search import router as search_router

_LOGGER = logging.getLogger(__name__)

router = Router(tags=["registry", "domain"])
router.add_router("-/search", search_router)


async def _get_domain_by_contact_info(domain_result: Dict[str, Any], semaphore: asyncio.Semaphore) -> Dict[str, Any]:
    """Get dictionary with domain info and roles of contact in relation to that domain."""
    async with semaphore:
        domain = await DOMAIN.get_domain_info(
            cast(DomainId, domain_result["domain"].id),
            cast(DomainHistoryId, domain_result["domain_history_id"]),
        )

    return {
        "domain": domain.model_dump(),
        "roles": set(cast(set, domain_result.get("roles"))),
    }


@router.get("/{domain_id}/", response={200: Domain, 404: Error}, summary="Get domain current info")
async def get_domain_info(request: HttpRequest, domain_id: DomainId):
    """Get current domain details."""
    return await get_object_info(
        request=request,
        object_id=domain_id,
        object_type=ObjectType.DOMAIN,
        log_entry_type=LogEntryType.DOMAIN_DETAIL,
        log_reference_type=LogReferenceType.DOMAIN,
        info_call=DOMAIN.get_domain_info,
    )


@router.get("/{domain_id}/history/", response={200: Dict[str, Any], 404: Error}, summary="Get domain history list")
async def get_domain_history(request: HttpRequest, domain_id: DomainId):
    """Get domain history_list."""
    return await get_object_history_list(
        request=request,
        object_id=domain_id,
        object_type=ObjectType.DOMAIN,
        log_entry_type=LogEntryType.DOMAIN_HISTORY,
        log_reference_type=LogReferenceType.DOMAIN,
        history_list_call=DOMAIN.get_domain_history,
    )


@router.get(
    "/{domain_id}/history/{history_id}/",
    response={200: Domain, 404: Error},
    summary="Get domain history info (by identifier)",
)
async def get_domain_history_info(request: HttpRequest, domain_id: DomainId, history_id: DomainHistoryId):
    """Get domain history details (by identifier)."""
    return await get_object_history_info(
        request=request,
        object_id=domain_id,
        object_history_id=history_id,
        object_type=ObjectType.DOMAIN,
        log_entry_type=LogEntryType.DOMAIN_DETAIL,
        log_reference_type=LogReferenceType.DOMAIN,
        info_call=DOMAIN.get_domain_info,
    )


@router.get(
    "/{domain_id}/history/by_datetime/{when}/",
    response={200: Domain, 404: Error},
    summary="Get domain history info (by datetime)",
)
async def get_domain_history_info_datetime(request: HttpRequest, domain_id: DomainId, when: datetime):
    """Get domain history details (by datetime)."""
    return await get_object_history_info_datetime(
        request=request,
        object_id=domain_id,
        when=when,
        object_type=ObjectType.DOMAIN,
        object_history_id_name="domain_history_id",
        log_entry_type=LogEntryType.DOMAIN_DETAIL,
        log_reference_type=LogReferenceType.DOMAIN,
        history_list_call=DOMAIN.get_domain_history,
        info_call=DOMAIN.get_domain_info,
    )


@router.get(
    "/{domain_id}/history/-/last/", response={200: Domain, 404: Error}, summary="Get domain history info (last known)"
)
async def get_domain_history_info_last(request: HttpRequest, domain_id: DomainId):
    """Get domain history details (last known)."""
    return await get_object_history_info_last(
        request=request,
        object_id=domain_id,
        object_type=ObjectType.DOMAIN,
        object_history_id_name="domain_history_id",
        log_entry_type=LogEntryType.DOMAIN_DETAIL,
        log_reference_type=LogReferenceType.DOMAIN,
        history_list_call=DOMAIN.get_domain_history,
        info_call=DOMAIN.get_domain_info,
    )


@router.get("/{domain_id}/state/", response={200: Dict[str, bool], 404: Error}, summary="Get domain state")
async def get_domain_state(request: HttpRequest, domain_id: DomainId):
    """Get domain current state."""
    return await get_object_state(
        request=request,
        object_id=domain_id,
        object_type=ObjectType.DOMAIN,
        log_entry_type=LogEntryType.DOMAIN_STATE,
        log_reference_type=LogReferenceType.DOMAIN,
        state_call=DOMAIN.get_domain_state,
    )


@router.get(
    "/{domain_id}/state/history/", response={200: Dict[str, Any], 404: Error}, summary="Get domain state history"
)
async def get_domain_state_history(request: HttpRequest, domain_id: DomainId):
    """Get domain state history."""
    return await get_object_state_history(
        request=request,
        object_id=domain_id,
        object_type=ObjectType.DOMAIN,
        log_entry_type=LogEntryType.DOMAIN_STATE,
        log_reference_type=LogReferenceType.DOMAIN,
        state_history_call=DOMAIN.get_domain_state_history,
    )


class DomainsByContactQuery(Schema):
    """Get domains by contact query."""

    roles: Set[DomainContactRole]
    include_deleted: bool = False
    order_by: Optional[str] = None
    fqdn_filter: Optional[str] = None
    page_token: Optional[str] = None
    page_size: Optional[int] = None


@router.get(
    "/-/record_statement/{domain_id}/",
    url_name="get_domain_record_statement_pdf",
    response={
        200: bytes,
        404: Error,
    },
)
async def get_domain_record_statement_pdf(request, domain_id: DomainId):
    """Get verified record statement of the domain."""
    await sync_to_async(check_permissions)(request, permissions=["ferda.can_view_registry"])
    return await get_object_statement_pdf(
        request, DomainId(domain_id), LogReferenceType.DOMAIN, STATEMENTOR.get_domain_statement
    )


@router.get(
    "/-/id_by_fqdn/{fqdn}/",
    url_name="get_domain_id_by_fqdn",
    response={200: DomainIdByFqdn, 404: Error},
)
async def get_domain_id_by_fqdn(request, fqdn: str):
    """Get domain id by FQDN."""
    await sync_to_async(check_permissions)(request, permissions=["ferda.can_view_registry"])
    # No need to log this view, there are no secret information included
    try:
        domain_id = await DOMAIN.get_domain_id(fqdn)
        return DomainIdByFqdn(fqdn=fqdn, domain_id=domain_id)
    except DomainDoesNotExist:
        return 404, Error(detail="Domain '%s' does not exist" % fqdn)


@router.get(
    "/-/by_contact/{contact_id}/",
    url_name="get_domains_by_contact",
    response={200: TokenPaginatedList[DomainByContact], 400: Error, 404: Error},
)
async def get_domains_by_contact(request, contact_id: ContactId, query: DomainsByContactQuery = Query(None)):
    """Get domains by contact."""
    await sync_to_async(check_permissions)(request, permissions=["ferda.can_view_registry"])

    log_entry = LOGGER.create(
        LogEntryType.DOMAIN_LIST,
        source_ip=request.META.get("REMOTE_ADDR", ""),
        session_id=request.session[LOGGER_SESSION_KEY],
        properties={
            "roles": [
                role
                for role in DomainContactRole  # type: ignore[attr-defined]
                if role in query.roles
            ],  # Maintain stable ordering
            "include_deleted": str(query.include_deleted),
            "order_by": query.order_by or "",
            "fqdn_filter": query.fqdn_filter or "",
            "page_token": query.page_token or "",
            "page_size": "" if query.page_size is None else str(query.page_size),
        },
        references={LogReferenceType.CONTACT: contact_id},
    )
    with log_entry:
        try:
            result = cast(
                Dict[str, Any],
                await DOMAIN.get_domains_by_contact(
                    contact_id,
                    query.roles,
                    include_deleted=query.include_deleted,
                    order_by=query.order_by,
                    fqdn_filter=query.fqdn_filter,
                    page_token=query.page_token,
                    page_size=query.page_size,
                ),
            )
            semaphore = asyncio.Semaphore(SETTINGS.concurrent_grpc_calls_limit)
            result["results"] = await asyncio.gather(
                *[_get_domain_by_contact_info(domain, semaphore=semaphore) for domain in result.pop("domains", [])],
            )
            log_entry.result = LoggerResult.SUCCESS
            return result
        except ContactDoesNotExist:
            log_entry.result = LoggerResult.FAIL
            return 404, Error(detail="Contact '%s' does not exist" % contact_id)
        except ValueError:
            log_entry.result = LoggerResult.FAIL
            return 400, Error(detail="invalid value")


@router.post(
    "/-/set_manual_in_zone/{domain_id}/", url_name="set_domain_manual_in_zone", response={200: None, 404: Error}
)
async def set_domain_manual_in_zone(
    request, domain_id: DomainId, start: Optional[datetime] = None, end: Optional[datetime] = None
):
    """Set domain manual in zone state flag."""
    await sync_to_async(check_permissions)(request, permissions=["ferda.can_set_manual_in_zone"])

    if start is None:
        start = datetime.now(tz=pytz.utc)
    if end is None:
        end = start + timedelta(seconds=SETTINGS.manual_in_zone_default_duration)

    log_entry = LOGGER.create(
        LogEntryType.DOMAIN_STATE_CHANGE,
        source_ip=request.META.get("REMOTE_ADDR", ""),
        session_id=request.session[LOGGER_SESSION_KEY],
        properties={
            StateFlag.SERVER_INZONE_MANUAL: LogProperty(
                "",
                children={
                    "start": start.isoformat(),
                    "end": end.isoformat(),
                },
            )
        },
        references={LogReferenceType.DOMAIN: domain_id},
    )
    with log_entry:
        try:
            result = DOMAIN_ADMIN.manage_domain_state_flags(
                domains=[Domain(domain_id=domain_id, fqdn="")],
                state_flags={StateFlag.SERVER_INZONE_MANUAL: DatetimeRange(start=start, end=end)},
            )
            # We don't care about the result here,
            # but we still need to exhaust async generator
            async for _ in result:
                pass
            log_entry.result = LoggerResult.SUCCESS
        except DomainDoesNotExist:
            log_entry.result = LoggerResult.FAIL
            return 404, Error(detail="Domain '%s' does not exist" % domain_id)


async def get_additional_contacts(
    request,
    *,
    method: Callable,
    event: str,
    start: datetime,
    end: datetime,
    page_token: Optional[str] = None,
    page_size: Optional[int] = None,
) -> Dict[str, Any]:
    """Get additional domain notify contacts."""
    log_entry = LOGGER.create(
        LogEntryType.DOMAIN_CONTACT_INFO,
        source_ip=request.META.get("REMOTE_ADDR", ""),
        session_id=request.session[LOGGER_SESSION_KEY],
        properties={
            "event": event,
            "start": start.isoformat(),
            "end": end.isoformat(),
            "page_token": page_token or "",
            "page_size": str(page_size) if page_size is not None else "",
        },
    )
    with log_entry:
        result = await method(start, end, page_size=page_size, page_token=page_token)
        result["results"] = result.pop("domains")

        log_entry.result = LoggerResult.SUCCESS
        return result


@router.get(
    "/-/get_additional_outzone_contacts/",
    url_name="get_additional_outzone_contacts",
    response=TokenPaginatedList[DomainContactInfo],
)
async def get_additional_outzone_contacts(
    request, start: datetime, end: datetime, page_token: Optional[str] = None, page_size: Optional[int] = None
):
    """Get additional domain outzone notify contacts."""
    await sync_to_async(check_permissions)(request, permissions=["ferda.can_view_registry"])
    return await get_additional_contacts(
        request,
        method=DOMAIN_ADMIN.get_domains_outzone_notify_info,
        event="outzone",
        start=start,
        end=end,
        page_token=page_token,
        page_size=page_size,
    )


@router.get(
    "/-/get_additional_delete_contacts/",
    url_name="get_additional_delete_contacts",
    response=TokenPaginatedList[DomainContactInfo],
)
async def get_additional_delete_contacts(
    request, start: datetime, end: datetime, page_token: Optional[str] = None, page_size: Optional[int] = None
):
    """Get additional domain delete notify contacts."""
    await sync_to_async(check_permissions)(request, permissions=["ferda.can_view_registry"])
    return await get_additional_contacts(
        request,
        method=DOMAIN_ADMIN.get_domains_delete_notify_info,
        event="delete",
        start=start,
        end=end,
        page_token=page_token,
        page_size=page_size,
    )


async def update_additional_contacts(
    request,
    *,
    event: str,
    method: Callable,
    domains: Dict[DomainId, List[ContactInfo]],
    start: datetime,
    end: datetime,
):
    """Update additional notify contacts."""
    log_content = {
        domain_id: {
            info_type: [contact.value for contact in contacts if contact.type == info_type]
            for info_type in ContactInfoType  # type: ignore[attr-defined]
        }
        for domain_id, contacts in domains.items()
    }

    log_entry = LOGGER.create(
        LogEntryType.DOMAIN_CONTACT_INFO_UPDATE,
        source_ip=request.META.get("REMOTE_ADDR", ""),
        session_id=request.session[LOGGER_SESSION_KEY],
        properties={"event": event, "start": start.isoformat(), "end": end.isoformat()},
        references={LogReferenceType.DOMAIN: list(domains.keys())},
        content=json.dumps(log_content),
    )
    with log_entry:
        try:
            await method(domains, start=start, end=end)

            log_entry.result = LoggerResult.SUCCESS
            return 200, None
        except DomainDoesNotExist:
            log_entry.result = LoggerResult.FAIL
            return 404, Error(detail="Domain does not exist")
        except ValueError:
            log_entry.result = LoggerResult.FAIL
            return 400, Error(detail="Invalid value")


@router.post(
    "/-/update_additional_outzone_contacts/",
    url_name="update_additional_outzone_contacts",
    response={200: None, 400: Error, 404: Error},
)
async def update_additional_outzone_contacts(request, body: UpdateDomainAdditionalContacts):
    """Update additional outzone notify contacts."""
    await sync_to_async(check_permissions)(
        request, permissions=["ferda.can_view_registry", "ferda.can_update_additional_contacts"]
    )
    return await update_additional_contacts(
        request,
        method=DOMAIN_ADMIN.update_domains_outzone_additional_notify_info,
        event="outzone",
        domains=body.domains,
        start=body.start,
        end=body.end,
    )


@router.post(
    "/-/update_additional_delete_contacts/",
    url_name="update_additional_delete_contacts",
    response={200: None, 400: Error, 404: Error},
)
async def update_additional_delete_contacts(request, body: UpdateDomainAdditionalContacts):
    """Update additional delete notify contacts."""
    await sync_to_async(check_permissions)(
        request, permissions=["ferda.can_view_registry", "ferda.can_update_additional_contacts"]
    )
    return await update_additional_contacts(
        request,
        method=DOMAIN_ADMIN.update_domains_delete_additional_notify_info,
        event="delete",
        domains=body.domains,
        start=body.start,
        end=body.end,
    )
