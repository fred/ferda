"""Registry domain schemas."""

from datetime import datetime
from typing import Dict, List

from fred_types import ContactId, DomainId
from ninja import Schema
from regal import Domain
from regal.domain import ContactInfo, DomainContactRole


class DomainByContact(Schema):
    """Domain by contact schema."""

    domain: Domain
    roles: List[DomainContactRole] = []


class DomainContactInfo(Schema):
    """Domain contact info."""

    domain_id: DomainId
    fqdn: str
    registrant_id: ContactId
    registrant_handle: str
    registrant_name: str
    registrant_organization: str
    contacts: List[ContactInfo]
    additional_contacts: List[ContactInfo]


class UpdateDomainAdditionalContacts(Schema):
    """Additional domain contacts for outzone or delete warning."""

    domains: Dict[DomainId, List[ContactInfo]]
    start: datetime
    end: datetime


class DomainIdByFqdn(Schema):
    """Domain id by fqdn response schema."""

    fqdn: str
    domain_id: DomainId
