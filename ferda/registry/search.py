"""Registry search common utils."""

import asyncio
from datetime import datetime
from functools import wraps
from typing import Any, Callable, List, Optional, Sequence

from asgiref.sync import sync_to_async
from grill import Properties
from ninja import Schema
from regal.object import BaseObjectHistoryId, BaseObjectId

from ferda.backend import LOGGER
from ferda.constants import LOGGER_SESSION_KEY, LogEntryType, LoggerResult
from ferda.settings import SETTINGS
from ferda.utils import await_with_semaphore


def log_search(log_entry_type: LogEntryType):
    """Add logging to search operation."""

    def _wrapper(call):
        @wraps(call)
        async def _inner(request, query: SearchQuery):
            session_id = await sync_to_async(request.session.get)(LOGGER_SESSION_KEY)
            properties: Properties = {
                "query_values": query.query,
                "search_item": query.search_items,
                "limit": str(query.limit),
            }
            if valid_from := getattr(query, "valid_from", None):
                properties["valid_from"] = valid_from.isoformat()
            if valid_to := getattr(query, "valid_to", None):
                properties["valid_to"] = valid_to.isoformat()
            log_entry = LOGGER.create(
                log_entry_type,
                source_ip=request.META.get("REMOTE_ADDR", ""),
                session_id=session_id,
                properties=properties,
            )
            with log_entry:
                result = await call(request=request, query=query)
                log_entry.result = LoggerResult.SUCCESS
            return result

        return _inner

    return _wrapper


class SearchQuery(Schema):
    """Search query."""

    query: List[str]
    search_items: List[str] = []
    limit: int = 100


class SearchHistoryQuery(SearchQuery):
    """History search query."""

    valid_from: Optional[datetime] = None
    valid_to: Optional[datetime] = None


async def search_object(query: SearchQuery, search_call: Callable, info_call: Callable, object_type: str) -> Any:
    """Search registry objects."""
    search_results = await search_call(query.query, searched_items=query.search_items, limit=query.limit)

    semaphore = asyncio.Semaphore(SETTINGS.concurrent_grpc_calls_limit)
    objects = await asyncio.gather(
        *[await_with_semaphore(info_call(result.object_id), semaphore=semaphore) for result in search_results.results]
    )

    results = search_results.model_dump()
    for result, object in zip(results["results"], objects):
        result[object_type] = object

    return results


async def search_object_history(
    query: SearchHistoryQuery,
    search_call: Callable,
    info_call: Callable,
    history_call: Callable,
    object_type: str,
    object_history_id: str,
) -> Any:
    """Search registry objects history."""
    search_results = await search_call(
        query.query,
        searched_items=query.search_items,
        limit=query.limit,
        start=query.valid_from,
        end=query.valid_to,
    )

    semaphore = asyncio.Semaphore(SETTINGS.concurrent_grpc_calls_limit)

    async def _get_object_last_history(id: BaseObjectId) -> Any:
        async with semaphore:
            history = await history_call(id)
            last_history_id = history["timeline"][-1][object_history_id]
            return await info_call(id, last_history_id)

    async def _get_object_histories(id: BaseObjectId, history_ids: Sequence[BaseObjectHistoryId]) -> list:
        return await asyncio.gather(
            *[await_with_semaphore(info_call(id, history_id), semaphore=semaphore) for history_id in history_ids]
        )

    object_last, object_histories = await asyncio.gather(
        asyncio.gather(*[_get_object_last_history(result.object_id) for result in search_results.results]),
        asyncio.gather(
            *[
                _get_object_histories(
                    result.object_id,
                    [history.history_ids[0] for history in result.histories],
                )
                for result in search_results.results
            ]
        ),
    )

    results = search_results.model_dump()
    for result, last, object_history in zip(results["results"], object_last, object_histories):
        result["last_history"] = last
        for history, object in zip(result["histories"], object_history):
            history[object_type] = object

    return results
