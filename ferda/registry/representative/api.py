"""Registry contact representative REST API views."""

from typing import Any, Dict, Optional, Union

from asgiref.sync import sync_to_async
from django.http import HttpRequest
from django.utils.translation import gettext as _
from fred_types import ContactId, RepresentativeHistoryId, RepresentativeId
from ninja import Router, Schema
from regal import Address, Representative
from regal.exceptions import ContactDoesNotExist, InvalidData, RepresentativeDoesNotExist

from ferda.api import Error, InvalidDataError, check_permissions
from ferda.backend import LOGGER, REPRESENTATIVE
from ferda.constants import LOGGER_SESSION_KEY, LogEntryType, LoggerResult, LogReferenceType

router = Router(tags=["registry", "representative"])


def _create_log_entry(request: HttpRequest, log_entry_type: LogEntryType, properties: Optional[Dict[str, Any]] = None):
    """Help to create log entry with common set of parameters."""
    return LOGGER.create(
        log_entry_type,
        source_ip=request.META.get("REMOTE_ADDR", ""),
        session_id=request.session[LOGGER_SESSION_KEY],
        properties=properties,
    )


async def _get_representative_info(
    request: HttpRequest, id: Union[RepresentativeHistoryId, RepresentativeId, ContactId], property: str
):
    await sync_to_async(check_permissions)(request, permissions=["ferda.view_contactrepresentative"])
    with _create_log_entry(request, LogEntryType.CONTACT_REPRESENTATIVE_DETAIL, {property: id}) as log_entry:
        try:
            representative = await REPRESENTATIVE.get(id)
        except RepresentativeDoesNotExist:
            log_entry.result = LoggerResult.FAIL
            return 404, Error(detail=_("Object does not exist"))
        log_entry.result = LoggerResult.SUCCESS
        log_entry.references = {
            LogReferenceType.REPRESENTATIVE: representative.id,
            LogReferenceType.CONTACT: representative.contact_id,
        }
        return representative


@router.get(
    "/{representative_id}/",
    url_name="representative",
    response={200: Representative, 404: Error},
    summary="Get contact representative current info",
)
async def get_representative_info(request: HttpRequest, representative_id: RepresentativeId):
    """Get current contact representative details."""
    return await _get_representative_info(request, representative_id, "representative_id")


@router.get(
    "/{history_id}/history/",
    url_name="representative_history",
    response={200: Representative, 404: Error},
    summary="Get contact representative history info",
)
async def get_representative_info_history(request: HttpRequest, history_id: RepresentativeHistoryId):
    """Get current contact representative details."""
    return await _get_representative_info(request, history_id, "history_id")


@router.get(
    "/{contact_id}/contact/",
    url_name="representative_contact",
    response={200: Representative, 404: Error},
    summary="Get contact representative info (by contact)",
)
async def get_representative_info_contact(request: HttpRequest, contact_id: ContactId):
    """Get current contact representative details."""
    return await _get_representative_info(request, contact_id, "contact_id")


class RepresentativeData(Schema):
    """Body parameters to 'create_representative'."""

    contact_id: ContactId
    name: str = ""
    organization: str = ""
    place: Optional[Address] = None
    telephone: str = ""
    email: str = ""


@router.post(
    "/",
    url_name="add_representative",
    response={200: Representative, 404: Error, 400: InvalidDataError},
    summary="Create contact representative",
)
async def create_representative(
    request: HttpRequest,
    representative: RepresentativeData,
):
    """Create contact representative."""
    await sync_to_async(check_permissions)(request, permissions=["ferda.change_contactrepresentative"])
    with _create_log_entry(request, LogEntryType.CONTACT_REPRESENTATIVE_CREATE) as log_entry:
        try:
            regal_representative = Representative(id=RepresentativeId(""), **representative.model_dump())
            regal_representative = await REPRESENTATIVE.create(regal_representative, log_entry_id=log_entry.entry_id)
        except (ContactDoesNotExist, RepresentativeDoesNotExist):
            log_entry.result = LoggerResult.FAIL
            return 404, Error(detail=_("Object does not exist"))
        except InvalidData as err:
            log_entry.result = LoggerResult.FAIL
            return 400, InvalidDataError(
                detail=_("Invalid data"),
                fields=list(err.fields),
            )
        log_entry.result = LoggerResult.SUCCESS
        log_entry.references = {
            LogReferenceType.REPRESENTATIVE: regal_representative.id,
            LogReferenceType.CONTACT: regal_representative.contact_id,
        }

        return regal_representative


@router.put(
    "/{representative_id}/",
    url_name="representative",
    response={200: Representative, 404: Error, 400: InvalidDataError},
    summary="Update contact representative",
)
async def update_representative(
    request: HttpRequest,
    representative_id: RepresentativeId,
    representative: RepresentativeData,
):
    """Update contact representative."""
    await sync_to_async(check_permissions)(request, permissions=["ferda.change_contactrepresentative"])
    with _create_log_entry(
        request, LogEntryType.CONTACT_REPRESENTATIVE_CHANGE, {"representative_id": representative_id}
    ) as log_entry:
        try:
            regal_representative = Representative(id=representative_id, **representative.model_dump())
            regal_representative = await REPRESENTATIVE.update(regal_representative, log_entry_id=log_entry.entry_id)
        except RepresentativeDoesNotExist:
            log_entry.result = LoggerResult.FAIL
            return 404, Error(detail=_("Object does not exist"))
        except InvalidData as err:
            log_entry.result = LoggerResult.FAIL
            return 400, InvalidDataError(
                detail=_("Invalid data"),
                fields=list(err.fields),
            )
        log_entry.result = LoggerResult.SUCCESS
        log_entry.references = {
            LogReferenceType.REPRESENTATIVE: representative_id,
            LogReferenceType.CONTACT: representative.contact_id,
        }

        return regal_representative


@router.delete(
    "/{representative_id}/",
    url_name="representative",
    response={200: None, 404: Error},
    summary="Delete contact representative",
)
async def delete_representative(request: HttpRequest, representative_id: RepresentativeId):
    """Delete contact representative."""
    await sync_to_async(check_permissions)(request, permissions=["ferda.delete_contactrepresentative"])
    with _create_log_entry(
        request, LogEntryType.CONTACT_REPRESENTATIVE_DELETE, {"representative_id": representative_id}
    ) as log_entry:
        try:
            await REPRESENTATIVE.delete(representative_id, log_entry_id=log_entry.entry_id)
        except RepresentativeDoesNotExist:
            log_entry.result = LoggerResult.FAIL
            return 404, Error(detail=_("Object does not exist"))
        log_entry.result = LoggerResult.SUCCESS
        log_entry.references = {LogReferenceType.REPRESENTATIVE: representative_id}
