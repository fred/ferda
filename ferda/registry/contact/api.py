"""Registry contact REST API views."""

from datetime import datetime
from typing import Any, Dict

from asgiref.sync import sync_to_async
from django.http import HttpRequest
from fred_types import ContactHistoryId, ContactId
from ninja import Router
from regal import Contact

from ferda.api import Error, check_permissions
from ferda.backend import CONTACT, STATEMENTOR
from ferda.constants import LogEntryType, LogReferenceType
from ferda.registry.record_statement import get_object_statement_pdf

from ..common import (
    get_object_history_info,
    get_object_history_info_datetime,
    get_object_history_info_last,
    get_object_history_list,
    get_object_info,
    get_object_state,
    get_object_state_history,
)
from ..constants import ObjectType
from .search import router as search_router

router = Router(tags=["registry", "contact"])
router.add_router("-/search", search_router)


@router.get(
    "/-/record_statement/{contact_id}/",
    url_name="get_contact_record_statement_pdf",
    response={
        200: bytes,
        404: Error,
    },
)
async def get_contact_record_statement_pdf(request, contact_id: ContactId):
    """Get verified record statement of the contact."""
    await sync_to_async(check_permissions)(request, permissions=["ferda.can_view_registry"])
    return await get_object_statement_pdf(
        request, ContactId(contact_id), LogReferenceType.CONTACT, STATEMENTOR.get_contact_statement
    )


@router.get("/{contact_id}/", response={200: Contact, 404: Error}, summary="Get contact current info")
async def get_contact_info(request: HttpRequest, contact_id: ContactId):
    """Get current contact details."""
    return await get_object_info(
        request=request,
        object_id=contact_id,
        object_type=ObjectType.CONTACT,
        log_entry_type=LogEntryType.CONTACT_DETAIL,
        log_reference_type=LogReferenceType.CONTACT,
        info_call=CONTACT.get_contact_info,
    )


@router.get("/{contact_id}/history/", response={200: Dict[str, Any], 404: Error}, summary="Get contact history list")
async def get_contact_history(request: HttpRequest, contact_id: ContactId):
    """Get contact history_list."""
    return await get_object_history_list(
        request=request,
        object_id=contact_id,
        object_type=ObjectType.CONTACT,
        log_entry_type=LogEntryType.CONTACT_HISTORY,
        log_reference_type=LogReferenceType.CONTACT,
        history_list_call=CONTACT.get_contact_history,
    )


@router.get(
    "/{contact_id}/history/{history_id}/",
    response={200: Contact, 404: Error},
    summary="Get contact history info (by identifier)",
)
async def get_contact_history_info(request: HttpRequest, contact_id: ContactId, history_id: ContactHistoryId):
    """Get contact history details (by identifier)."""
    return await get_object_history_info(
        request=request,
        object_id=contact_id,
        object_history_id=history_id,
        object_type=ObjectType.CONTACT,
        log_entry_type=LogEntryType.CONTACT_DETAIL,
        log_reference_type=LogReferenceType.CONTACT,
        info_call=CONTACT.get_contact_info,
    )


@router.get(
    "/{contact_id}/history/by_datetime/{when}/",
    response={200: Contact, 404: Error},
    summary="Get contact history info (by datetime)",
)
async def get_contact_history_info_datetime(request: HttpRequest, contact_id: ContactId, when: datetime):
    """Get contact history details (by datetime)."""
    return await get_object_history_info_datetime(
        request=request,
        object_id=contact_id,
        when=when,
        object_type=ObjectType.CONTACT,
        object_history_id_name="contact_history_id",
        log_entry_type=LogEntryType.CONTACT_DETAIL,
        log_reference_type=LogReferenceType.CONTACT,
        history_list_call=CONTACT.get_contact_history,
        info_call=CONTACT.get_contact_info,
    )


@router.get(
    "/{contact_id}/history/-/last/",
    response={200: Contact, 404: Error},
    summary="Get contact history info (last known)",
)
async def get_contact_history_info_last(request: HttpRequest, contact_id: ContactId):
    """Get contact history details (last known)."""
    return await get_object_history_info_last(
        request=request,
        object_id=contact_id,
        object_type=ObjectType.CONTACT,
        object_history_id_name="contact_history_id",
        log_entry_type=LogEntryType.CONTACT_DETAIL,
        log_reference_type=LogReferenceType.CONTACT,
        history_list_call=CONTACT.get_contact_history,
        info_call=CONTACT.get_contact_info,
    )


@router.get("/{contact_id}/state/", response={200: Dict[str, bool], 404: Error}, summary="Get contact state")
async def get_contact_state(request: HttpRequest, contact_id: ContactId):
    """Get contact current state."""
    return await get_object_state(
        request=request,
        object_id=contact_id,
        object_type=ObjectType.CONTACT,
        log_entry_type=LogEntryType.CONTACT_STATE,
        log_reference_type=LogReferenceType.CONTACT,
        state_call=CONTACT.get_contact_state,
    )


@router.get(
    "/{contact_id}/state/history/", response={200: Dict[str, Any], 404: Error}, summary="Get contact state history"
)
async def get_contact_state_history(request: HttpRequest, contact_id: ContactId):
    """Get contact state history."""
    return await get_object_state_history(
        request=request,
        object_id=contact_id,
        object_type=ObjectType.CONTACT,
        log_entry_type=LogEntryType.CONTACT_STATE,
        log_reference_type=LogReferenceType.CONTACT,
        state_history_call=CONTACT.get_contact_state_history,
    )
