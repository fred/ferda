"""Contact search."""

from typing import Sequence

from fred_types import ContactHistoryId
from ninja import Query, Router
from regal import Contact, SearchResults
from regal.search import HistorySearchPeriod, SearchCurrentResult, SearchHistoryResult

from ferda.backend import CONTACT, CONTACT_SEARCH
from ferda.constants import LogEntryType
from ferda.registry.search import SearchHistoryQuery, SearchQuery, log_search, search_object, search_object_history

router = Router(tags=["registry", "contact", "search"])


class ContactSearchResult(SearchCurrentResult):
    """Single contact search result."""

    contact: Contact


class ContactHistorySearchPeriod(HistorySearchPeriod[ContactHistoryId]):
    """Contact history search period."""

    contact: Contact


class ContactSearchHistoryResult(SearchHistoryResult):
    """Single contact history search result."""

    last_history: Contact
    histories: Sequence[ContactHistorySearchPeriod] = []


@router.get("/", url_name="search_contact", response=SearchResults[ContactSearchResult])
@log_search(LogEntryType.CONTACT_SEARCH)
async def search_contact(request, query: SearchQuery = Query(None)):
    """Search contacts."""
    return await search_object(
        query=query,
        search_call=CONTACT_SEARCH.search_contact,
        info_call=CONTACT.get_contact_info,
        object_type="contact",
    )


@router.get("/history/", url_name="search_contact_history", response=SearchResults[ContactSearchHistoryResult])
@log_search(LogEntryType.CONTACT_SEARCH_HISTORY)
async def search_contact_history(request, query: SearchHistoryQuery = Query(None)):
    """Search contacts history."""
    return await search_object_history(
        query=query,
        search_call=CONTACT_SEARCH.search_contact_history,
        info_call=CONTACT.get_contact_info,
        history_call=CONTACT.get_contact_history,
        object_type="contact",
        object_history_id="contact_history_id",
    )
