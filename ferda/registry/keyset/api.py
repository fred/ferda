"""Registry keyset REST API views."""

from datetime import datetime
from typing import Any, Dict

from asgiref.sync import sync_to_async
from django.http import HttpRequest
from fred_types import KeysetHistoryId, KeysetId
from ninja import Router
from regal import Keyset

from ferda.api import Error, check_permissions
from ferda.backend import KEYSET, STATEMENTOR
from ferda.constants import LogEntryType, LogReferenceType
from ferda.registry.record_statement import get_object_statement_pdf

from ..common import (
    get_object_history_info,
    get_object_history_info_datetime,
    get_object_history_info_last,
    get_object_history_list,
    get_object_info,
    get_object_state,
    get_object_state_history,
)
from ..constants import ObjectType
from .search import router as search_router

router = Router(tags=["registry", "keyset"])
router.add_router("-/search", search_router)


@router.get(
    "/-/record_statement/{keyset_id}/",
    url_name="get_keyset_record_statement_pdf",
    response={
        200: bytes,
        404: Error,
    },
)
async def get_keyset_record_statement_pdf(request, keyset_id: KeysetId):
    """Get verified record statement of the keyset."""
    await sync_to_async(check_permissions)(request, permissions=["ferda.can_view_registry"])
    return await get_object_statement_pdf(
        request, KeysetId(keyset_id), LogReferenceType.KEYSET, STATEMENTOR.get_keyset_statement
    )


@router.get("/{keyset_id}/", response={200: Keyset, 404: Error}, summary="Get keyset current info")
async def get_keyset_info(request: HttpRequest, keyset_id: KeysetId):
    """Get current keyset details."""
    return await get_object_info(
        request=request,
        object_id=keyset_id,
        object_type=ObjectType.KEYSET,
        log_entry_type=LogEntryType.KEYSET_DETAIL,
        log_reference_type=LogReferenceType.KEYSET,
        info_call=KEYSET.get_keyset_info,
    )


@router.get("/{keyset_id}/history/", response={200: Dict[str, Any], 404: Error}, summary="Get keyset history list")
async def get_keyset_history(request: HttpRequest, keyset_id: KeysetId):
    """Get keyset history_list."""
    return await get_object_history_list(
        request=request,
        object_id=keyset_id,
        object_type=ObjectType.KEYSET,
        log_entry_type=LogEntryType.KEYSET_HISTORY,
        log_reference_type=LogReferenceType.KEYSET,
        history_list_call=KEYSET.get_keyset_history,
    )


@router.get(
    "/{keyset_id}/history/{history_id}/",
    response={200: Keyset, 404: Error},
    summary="Get keyset history info (by identifier)",
)
async def get_keyset_history_info(request: HttpRequest, keyset_id: KeysetId, history_id: KeysetHistoryId):
    """Get keyset history details (by identifier)."""
    return await get_object_history_info(
        request=request,
        object_id=keyset_id,
        object_history_id=history_id,
        object_type=ObjectType.KEYSET,
        log_entry_type=LogEntryType.KEYSET_DETAIL,
        log_reference_type=LogReferenceType.KEYSET,
        info_call=KEYSET.get_keyset_info,
    )


@router.get(
    "/{keyset_id}/history/by_datetime/{when}/",
    response={200: Keyset, 404: Error},
    summary="Get keyset history info (by datetime)",
)
async def get_keyset_history_info_datetime(request: HttpRequest, keyset_id: KeysetId, when: datetime):
    """Get keyset history details (by datetime)."""
    return await get_object_history_info_datetime(
        request=request,
        object_id=keyset_id,
        when=when,
        object_type=ObjectType.KEYSET,
        object_history_id_name="keyset_history_id",
        log_entry_type=LogEntryType.KEYSET_DETAIL,
        log_reference_type=LogReferenceType.KEYSET,
        history_list_call=KEYSET.get_keyset_history,
        info_call=KEYSET.get_keyset_info,
    )


@router.get(
    "/{keyset_id}/history/-/last/", response={200: Keyset, 404: Error}, summary="Get keyset history info (last known)"
)
async def get_keyset_history_info_last(request: HttpRequest, keyset_id: KeysetId):
    """Get keyset history details (last known)."""
    return await get_object_history_info_last(
        request=request,
        object_id=keyset_id,
        object_type=ObjectType.KEYSET,
        object_history_id_name="keyset_history_id",
        log_entry_type=LogEntryType.KEYSET_DETAIL,
        log_reference_type=LogReferenceType.KEYSET,
        history_list_call=KEYSET.get_keyset_history,
        info_call=KEYSET.get_keyset_info,
    )


@router.get("/{keyset_id}/state/", response={200: Dict[str, bool], 404: Error}, summary="Get keyset state")
async def get_keyset_state(request: HttpRequest, keyset_id: KeysetId):
    """Get keyset current state."""
    return await get_object_state(
        request=request,
        object_id=keyset_id,
        object_type=ObjectType.KEYSET,
        log_entry_type=LogEntryType.KEYSET_STATE,
        log_reference_type=LogReferenceType.KEYSET,
        state_call=KEYSET.get_keyset_state,
    )


@router.get(
    "/{keyset_id}/state/history/", response={200: Dict[str, Any], 404: Error}, summary="Get keyset state history"
)
async def get_keyset_state_history(request: HttpRequest, keyset_id: KeysetId):
    """Get keyset state history."""
    return await get_object_state_history(
        request=request,
        object_id=keyset_id,
        object_type=ObjectType.KEYSET,
        log_entry_type=LogEntryType.KEYSET_STATE,
        log_reference_type=LogReferenceType.KEYSET,
        state_history_call=KEYSET.get_keyset_state_history,
    )
