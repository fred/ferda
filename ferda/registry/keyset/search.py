"""Keyset search."""

from typing import Sequence

from fred_types import KeysetHistoryId
from ninja import Query, Router
from regal import Keyset, SearchResults
from regal.search import HistorySearchPeriod, SearchCurrentResult, SearchHistoryResult

from ferda.backend import KEYSET, KEYSET_SEARCH
from ferda.constants import LogEntryType
from ferda.registry.search import SearchHistoryQuery, SearchQuery, log_search, search_object, search_object_history

router = Router(tags=["registry", "keyset", "search"])


class KeysetSearchResult(SearchCurrentResult):
    """Single keyset search result."""

    keyset: Keyset


class KeysetHistorySearchPeriod(HistorySearchPeriod[KeysetHistoryId]):
    """Keyset history search period."""

    keyset: Keyset


class KeysetSearchHistoryResult(SearchHistoryResult):
    """Single keyset history search result."""

    last_history: Keyset
    histories: Sequence[KeysetHistorySearchPeriod] = []


@router.get("/", url_name="search_keyset", response=SearchResults[KeysetSearchResult])
@log_search(LogEntryType.KEYSET_SEARCH)
async def search_keyset(request, query: SearchQuery = Query(None)):
    """Search keysets."""
    return await search_object(
        query=query, search_call=KEYSET_SEARCH.search_keyset, info_call=KEYSET.get_keyset_info, object_type="keyset"
    )


@router.get("/history/", url_name="search_keyset_history", response=SearchResults[KeysetSearchHistoryResult])
@log_search(LogEntryType.KEYSET_SEARCH_HISTORY)
async def search_keyset_history(request, query: SearchHistoryQuery = Query(None)):
    """Search keysets history."""
    return await search_object_history(
        query=query,
        search_call=KEYSET_SEARCH.search_keyset_history,
        info_call=KEYSET.get_keyset_info,
        history_call=KEYSET.get_keyset_history,
        object_type="keyset",
        object_history_id="keyset_history_id",
    )
