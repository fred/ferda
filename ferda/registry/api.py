"""Registry REST API."""

from ninja import Router

from .contact.api import router as contact_router
from .domain.api import router as domain_router
from .keyset.api import router as keyset_router
from .nsset.api import router as nsset_router
from .registrar.api import router as registrar_router
from .representative.api import router as representative_router

router = Router(tags=["registry"])
router.add_router("contact", contact_router)
router.add_router("domain", domain_router)
router.add_router("keyset", keyset_router)
router.add_router("nsset", nsset_router)
router.add_router("registrar", registrar_router)
router.add_router("representative", representative_router)
