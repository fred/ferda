"""Ferda forms."""

from django import forms

from ferda.models import ContactRepresentative


class ContactRepresentativeForm(forms.ModelForm):
    """Contact representative form."""

    class Meta:
        model = ContactRepresentative
        exclude = ["valid_from", "valid_to"]
