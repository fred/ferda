"""Ferda common API."""

from typing import Any, Dict

from asgiref.sync import sync_to_async
from deepmerge import conservative_merger
from ninja import Router

from ferda.api import check_permissions
from ferda.plugin import get_plugin_manager

router = Router(tags=["common"])


@router.get("/settings/", response=Dict[str, Any], auth=None)
async def get_settings(request):
    """Get selected django settings from Ferda and its plugins."""
    settings: Dict[str, Any] = {}
    pm = get_plugin_manager()
    for value in pm.hook.show_settings():
        conservative_merger.merge(settings, value)
    return settings


@router.get("/user/", response=Dict[str, Any])
async def get_user(request):
    """Get logged-in user details."""
    # Empty check_permissions call just checks whether user is authenticated
    await sync_to_async(check_permissions)(request)

    permissions = list(await sync_to_async(request.user.get_all_permissions)())
    user = {
        "username": request.user.username,
        "first_name": request.user.first_name,
        "last_name": request.user.last_name,
        "email": request.user.email,
        "is_staff": request.user.is_staff,
        "permissions": permissions,
    }
    return user
