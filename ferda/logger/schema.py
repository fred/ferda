"""Logger REST API schemas."""

from datetime import datetime
from typing import Dict, List, Optional

from ninja import Schema


class Service(Schema):
    """Logger service."""

    name: str
    log_entry_types: List[str]
    results: Dict[str, int]
    object_reference_types: List[str]


class LogEntryProperty(Schema):
    """Log entry property."""

    value: Optional[str]
    children: Dict[str, str] = {}


class LogEntry(Schema):
    """Log entry."""

    log_entry_id: str
    time_begin: datetime
    time_end: Optional[datetime] = None
    source_ip: Optional[str] = None
    service: str
    log_entry_type: str
    session_id: Optional[str] = None
    result_code: Optional[int] = None
    result_name: Optional[str] = None
    raw_request: Optional[str] = None
    raw_response: Optional[str] = None
    username: Optional[str] = None
    user_id: Optional[int] = None
    input_properties: Dict[str, List[LogEntryProperty]] = {}
    output_properties: Dict[str, List[LogEntryProperty]] = {}
    references: Dict[str, List[str]] = {}
