"""Logger REST API views."""

import asyncio
from datetime import datetime, timedelta
from functools import cached_property, partial
from typing import Any, List, Optional, cast
from urllib.parse import unquote

from asgiref.sync import sync_to_async
from django.contrib.auth.models import AbstractUser
from django.http import HttpRequest
from grill import LogProperty, Properties
from grill.clients import ListProperty
from grill.exceptions import InvalidArgument, LogEntryDoesNotExist
from ninja import Query, Router, Schema
from pydantic import GetCoreSchemaHandler, PositiveInt
from pydantic_core import CoreSchema, core_schema

from ferda.api import Error, PaginatedList, RegistryReference, build_full_url, check_permissions
from ferda.backend import LOGGER, LOGGER_CLIENT, LOGGER_SEARCH
from ferda.constants import LOGGER_SESSION_KEY, LogEntryType, LoggerResult
from ferda.settings import SETTINGS

from .schema import LogEntry, Service

DEFAULT_PAGE_SIZE = 10

router = Router(tags=["logger"])

logger_permissions = partial(check_permissions, permissions="ferda.can_view_registry")


async def _get_log_entry(log_entry_id: str, semaphore: Optional[asyncio.Semaphore] = None) -> LogEntry:
    if semaphore:
        async with semaphore:
            log_entry = await LOGGER_SEARCH.get_log_entry_info(log_entry_id=log_entry_id)
    else:
        log_entry = await LOGGER_SEARCH.get_log_entry_info(log_entry_id=log_entry_id)
    log_entry["log_entry_id"] = log_entry_id
    return LogEntry(**log_entry)


@router.get("/service/", url_name="list_services", response=List[str])
def list_services(request: HttpRequest):
    """Return list of logger services."""
    logger_permissions(request)

    logger_services = LOGGER_CLIENT.get_services()
    prefix = "ferda.can_view_logger_service__"
    allowed_services = {
        perm[len(prefix) :]
        for perm in cast(AbstractUser, request.user).get_all_permissions()
        if perm.startswith(prefix)
    }
    return [service for service in logger_services if service in allowed_services]


@router.get("/service/{service_name}/", url_name="get_service", response={200: Service, 404: Error})
def get_service(request, service_name: str):
    """Return list of logger services."""
    check_permissions(
        request,
        ["ferda.can_view_registry", f"ferda.can_view_logger_service__{service_name}"],
    )

    try:
        response = {
            "name": service_name,
            "log_entry_types": LOGGER_CLIENT.get_log_entry_types(service_name),
            "results": LOGGER_CLIENT.get_results(service_name),
            "object_reference_types": LOGGER_CLIENT.get_object_reference_types(),
        }
    except InvalidArgument:
        return 404, Error(detail="Service '%s' does not exist" % service_name)
    return response


class LogPropertyField(str):
    """Log property field type."""

    @cached_property
    def name(self) -> str:
        """Parse property name."""
        return self.partition(".")[0]

    @cached_property
    def value(self) -> str:
        """Parse property value."""
        return self.partition(".")[2]

    @classmethod
    def validate(cls, v: str) -> "LogPropertyField":
        """Create instance of LogProperty."""
        if "." not in v:
            raise ValueError("Property name and value must be separated by a dot.")
        return cls(v)

    @classmethod
    def __get_pydantic_core_schema__(cls, source_type: Any, handler: GetCoreSchemaHandler) -> CoreSchema:
        return core_schema.no_info_after_validator_function(cls.validate, handler(str))

    def __repr__(self):
        """Return distinct representation."""
        return '{}(name="{}", value="{}")'.format(self.__class__.__name__, self.name, self.value)


class ListLogEntriesQuery(Schema):
    """List log entries query params."""

    service: str
    time_begin_from: datetime
    time_begin_to: datetime
    offset: float = 0.0
    log_entry_types: List[str] = []
    username: Optional[str] = None
    reference: Optional[RegistryReference] = None
    properties: List[LogPropertyField] = []
    page_size: PositiveInt = DEFAULT_PAGE_SIZE


@router.get("/log_entry/", url_name="list_log_entries", response={200: PaginatedList[LogEntry], 400: Error})
async def list_log_entries(request, query: ListLogEntriesQuery = Query(None)):
    """Return paginated list of log entries."""
    await sync_to_async(check_permissions)(
        request,
        ["ferda.can_view_registry", f"ferda.can_view_logger_service__{query.service}"],
    )

    if query.offset:
        timestamp_to = query.time_begin_to - timedelta(seconds=query.offset)
    else:
        timestamp_to = query.time_begin_to

    properties: Properties = {
        "service": query.service,
        "time_begin_from": query.time_begin_from.isoformat(),
        "time_begin_to": query.time_begin_to.isoformat(),
        "log_entry_types": query.log_entry_types,
        "page_size": str(query.page_size),
        "offset": str(query.offset),
    }
    if query.reference:
        properties["reference"] = LogProperty(
            "",
            children={
                "type": query.reference.type,
                "value": query.reference.value,
            },
        )
    if query.properties:
        properties["properties"] = [
            LogProperty("", children={"name": prop.name, "value": prop.value}) for prop in query.properties
        ]
    if query.username:
        properties["username"] = query.username

    log_entry = LOGGER.create(
        LogEntryType.LOGGER_LIST,
        source_ip=request.META.get("REMOTE_ADDR", ""),
        session_id=request.session[LOGGER_SESSION_KEY],
        properties=properties,
    )
    with log_entry:
        try:
            log_entry_ids = (
                await LOGGER_SEARCH.list_log_entries(
                    service=query.service,
                    timestamp_from=query.time_begin_from,
                    timestamp_to=timestamp_to,
                    log_entry_types=query.log_entry_types,
                    user=query.username,
                    reference_type=query.reference.type if query.reference else None,
                    reference_value=query.reference.value if query.reference else None,
                    properties=[ListProperty(prop.name, prop.value) for prop in query.properties],
                    limit=query.page_size + 1,
                )
                or []
            )
            semaphore = asyncio.Semaphore(SETTINGS.concurrent_grpc_calls_limit)
            log_entries = await asyncio.gather(*[_get_log_entry(id, semaphore=semaphore) for id in log_entry_ids])
            log_entry.result = LoggerResult.SUCCESS
        except InvalidArgument:
            log_entry.result = LoggerResult.FAIL
            return 400, Error(detail="Invalid argument")

    next_url = None
    if len(log_entries) > query.page_size:
        next_offset = query.time_begin_to - log_entries[query.page_size - 1].time_begin
        next_url = build_full_url(request, {"offset": next_offset.total_seconds()})
        log_entries = log_entries

    return {
        "next": next_url,
        "results": log_entries[: query.page_size],
    }


@router.get("/log_entry/{log_entry_id}/", url_name="get_log_entry", response={200: LogEntry, 404: Error})
async def get_log_entry(request, log_entry_id: str):
    """Retrieve single log entry."""
    await sync_to_async(logger_permissions)(request)  # We don't know the service yet

    log_entry_id = unquote(log_entry_id)

    log_entry = LOGGER.create(
        LogEntryType.LOGGER_DETAIL,
        source_ip=request.META.get("REMOTE_ADDR", ""),
        session_id=request.session[LOGGER_SESSION_KEY],
        properties={"log_entry_id": log_entry_id},
    )
    with log_entry:
        try:
            detail = await _get_log_entry(log_entry_id)

            # Check permissions to view service log entries
            log_entry.result = LoggerResult.FAIL
            await sync_to_async(check_permissions)(
                request,
                ["ferda.can_view_registry", f"ferda.can_view_logger_service__{detail.service}"],
            )

            log_entry.result = LoggerResult.SUCCESS
            return detail
        except LogEntryDoesNotExist:
            log_entry.result = LoggerResult.FAIL
            return 404, Error(detail="Log entry not found")
