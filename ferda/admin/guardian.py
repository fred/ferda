"""Guardian customization for ferda."""

from django import forms
from django.contrib.auth import get_user_model
from django.contrib.auth.models import Group
from django.utils.translation import gettext_lazy as _
from guardian.admin import (
    AdminGroupObjectPermissionsForm,
    AdminUserObjectPermissionsForm,
    GroupManage,
    GuardedModelAdmin,
    UserManage,
)
from guardian.forms import BaseObjectPermissionsForm

User = get_user_model()


########################################
# Customized guardian forms
########################################
class CustomPermissionsFormMixin:
    """Custom permissions form mixin."""

    def get_obj_perms_field_choices(self: BaseObjectPermissionsForm):
        """Filter out global report permissions."""
        return self.obj._meta.permissions


class AdminUserCustomObjectPermissionsForm(CustomPermissionsFormMixin, AdminUserObjectPermissionsForm):
    """Custom admin user permission form."""


class AdminGroupCustomObjectPermissionsForm(CustomPermissionsFormMixin, AdminGroupObjectPermissionsForm):
    """Custom admin group permission form."""


class ChoiceUserManage(UserManage):
    """User selection form with select field."""

    user = forms.ModelChoiceField(
        queryset=User.objects.order_by(User.USERNAME_FIELD),
        label=_("User"),
        error_messages={"does_not_exist": _("This user does not exist")},
    )


class ChoiceGroupManage(GroupManage):
    """Group selection form with select field."""

    group = forms.ModelChoiceField(
        queryset=Group.objects.order_by("name"),
        error_messages={"does_not_exist": _("This group does not exist")},
    )


########################################
# Guardian model admin replacement
########################################
class FerdaGuardedModelAdmin(GuardedModelAdmin):
    """Guarded model admin customized for Ferda."""

    def get_obj_perms_user_select_form(self, request):
        """Return custom user select form for object level permissions."""
        return ChoiceUserManage

    def get_obj_perms_group_select_form(self, request):
        """Return custom group select form for object level permissions."""
        return ChoiceGroupManage

    def get_obj_perms_base_context(self, request, obj):
        """Filter out global report permissions."""
        context = super().get_obj_perms_base_context(request, obj)  # type: ignore
        context["model_perms"] = context["model_perms"].filter(codename__in=[p[0] for p in obj._meta.permissions])
        return context

    def get_obj_perms_manage_user_form(self, request):
        """Use custom form."""
        return AdminUserCustomObjectPermissionsForm

    def get_obj_perms_manage_group_form(self, request):
        """Use custom form."""
        return AdminGroupCustomObjectPermissionsForm
