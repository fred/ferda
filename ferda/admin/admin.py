"""Custom model admins."""

from django.contrib.auth.admin import UserAdmin as DjangoUserAdmin
from django.utils.translation import gettext_lazy as _

from .forms import UserCreationForm


class UserAdmin(DjangoUserAdmin):
    """Custom user admin for custom user."""

    add_form = UserCreationForm

    add_fieldsets = (
        (
            None,
            {
                "fields": ("username",),
            },
        ),
        (
            _("Password"),
            {
                "description": _(
                    "If you use external authentication system such as LDAP, you don't have to choose a password."
                ),
                "fields": (
                    "password1",
                    "password2",
                ),
            },
        ),
    )

    def save_model(self, request, obj, form, change):
        """If password isn't provided, set unusable password."""
        if not change and (not form.cleaned_data["password1"] or not obj.has_usable_password()):
            obj.set_unusable_password()
        super().save_model(request, obj, form, change)
