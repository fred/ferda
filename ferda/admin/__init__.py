"""Django admin."""

from django.contrib.admin import site

from ferda.models import Report, User

from .admin import UserAdmin
from .reports import ReportAdmin

site.register(User, UserAdmin)
site.register(Report, ReportAdmin)
