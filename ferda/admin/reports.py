"""Reports admin classes."""

from typing import List

import yaml
from adminsortable2.admin import SortableAdminBase, SortableTabularInline
from django import forms
from django.contrib import messages
from django.contrib.auth.mixins import PermissionRequiredMixin
from django.db import transaction
from django.shortcuts import redirect
from django.urls import path, reverse, reverse_lazy
from django.utils.html import format_html
from django.utils.safestring import mark_safe
from django.utils.translation import gettext, gettext_lazy as _
from django.views.generic import FormView, TemplateView
from grill.clients import LogProperty
from grpc import RpcError
from pydantic import BaseModel, ValidationError
from yaml.parser import ParserError

from ferda.backend import LOGGER, REPORT_MANAGERS, REPORTS
from ferda.constants import LOGGER_SESSION_KEY, LogEntryType, LoggerResult
from ferda.models import Report, ReportParameter
from ferda.models.reports import SyncReportsResult

from .guardian import FerdaGuardedModelAdmin


class DbReportParam(BaseModel):
    """DB report parameter."""

    name: str
    label: str


class DbReport(BaseModel):
    """DB report."""

    service: str
    name: str
    label: str
    description: str
    tags: List[str] = []

    input_params: List[DbReportParam]
    output_params: List[DbReportParam]


class BaseReportParameterAdmin(SortableTabularInline):
    """Base report parameter inline admin."""

    model = ReportParameter
    ordering = ("order", "name")
    fields = ("name", "type", "label")
    readonly_fields = ("name", "type")

    def has_add_permission(self, request, obj):
        """Report parameter can't be added manually through django admin."""
        return False

    def has_change_permission(self, request, obj):
        """Report parameter does not have its own permissions."""
        return request.user.has_perm("ferda.change_report")

    def has_delete_permission(self, request, obj=None):
        """Report parameter can't be deleted manually through django admin."""
        return False

    def has_view_permission(self, request, obj):
        """Report parameter does not have its own permissions."""
        return request.user.has_perm("ferda.view_report")


class ReportInputParameterAdmin(BaseReportParameterAdmin):
    """Report input parameter inline admin."""

    verbose_name_plural = _("Input report parameters")

    def get_queryset(self, request):
        """Filter input parameters."""
        return super().get_queryset(request).filter(is_input=True)


class ReportOutputParameterAdmin(BaseReportParameterAdmin):
    """Report output parameter inline admin."""

    verbose_name_plural = _("Output report parameters")

    def get_queryset(self, request):
        """Filter output parameters."""
        return super().get_queryset(request).filter(is_input=False)


class ReportAdmin(SortableAdminBase, FerdaGuardedModelAdmin):
    """Report admin."""

    list_display = ("name", "service", "label")
    ordering = (
        "service",
        "name",
    )
    inlines = (ReportInputParameterAdmin, ReportOutputParameterAdmin)
    fields = ("service", "name", "label", "description", "tags")
    readonly_fields = (
        "service",
        "name",
    )

    def get_urls(self):
        """Add custom urls."""
        urls = super().get_urls()
        custom_urls = [
            path(
                "manage_reports/",
                self.admin_site.admin_view(ManageReportsView.as_view()),
                name="ferda_report_manage_reports",
            ),
            path(
                "sync_reports/",
                self.admin_site.admin_view(SyncReportsView.as_view()),
                name="ferda_report_sync_reports",
            ),
        ]
        return custom_urls + urls

    def has_add_permission(self, request):
        """Report cannot be added manually through django admin."""
        return False

    def has_delete_permission(self, request, obj=None):
        """Report cannot be deleted manually through django admin."""
        return False


def format_sync_result(result: SyncReportsResult, title=None):
    """Format sync result."""
    return format_html(
        """<p>
            <strong>{}</strong><br/>
            {}: <tt>{}</tt><br/>
            {}: <tt>{}</tt><br/>
            {}: <tt>{}</tt><br/>
           </p>""",
        title or gettext("Sync result"),
        gettext("Created"),
        ", ".join(result.created) or mark_safe("&ndash;"),  # noqa: S308
        gettext("Deleted"),
        ", ".join(result.deleted) or mark_safe("&ndash;"),  # noqa: S308
        gettext("Updated header"),
        ", ".join(result.updated) or mark_safe("&ndash;"),  # noqa: S308
    )


class ManageReportsForm(forms.Form):
    """Manage reports form."""

    service = forms.ChoiceField(required=True, choices=((service, service) for service in REPORTS))
    sql = forms.CharField(required=False, widget=forms.Textarea)
    config = forms.CharField(
        required=False,
        widget=forms.Textarea,
        help_text=_(
            "Report metadata in YAML format. Can contain multiple report definitions and "
            "doesn't have to be connected to selected service or report in SQL field (if any)."
        ),
    )


class ManageReportsView(PermissionRequiredMixin, FormView):
    """Manage reports view.

    Configuration field enables loading YAML config with fields description.

    Example:

    .. code-block:: yaml

        - service: registry
          name: domains_by_holder
          label: List domains by holder
          description: |
              List domains by holder.
              Further description...
          input_params:
              - name: holder
                label: Domain holder
          output_params:
              - name: domain
                label: Domain
              - name: sponsoring_registrar
                label: Registrar
              - name: state_flags
                label: Status
              - name: exdate
                label: Expiration date
    """

    form_class = ManageReportsForm
    template_name = "admin/ferda/report/manage_reports.html"
    permission_required = ("ferda.add_report", "ferda.change_report", "ferda.delete_report")
    success_url = reverse_lazy("admin:ferda_report_changelist")

    def get_context_data(self, **kwargs):
        """Return view context."""
        context = super().get_context_data(**kwargs)
        context["opts"] = Report._meta
        return context

    def form_valid(self, form):
        """Run SQL in the backend database and display results."""
        context = self.get_context_data(form=form)
        service = form.cleaned_data["service"]

        if form.cleaned_data["sql"]:
            log_entry = LOGGER.create(
                LogEntryType.MANAGE_REPORTS,
                source_ip=self.request.META.get("REMOTE_ADDR", ""),
                session_id=self.request.session[LOGGER_SESSION_KEY],
                properties={"service": LogProperty(service, None)},
                content=form.cleaned_data["sql"],
            )
            with log_entry:
                try:
                    REPORT_MANAGERS[service].manage_reports(sql=form.cleaned_data["sql"])
                except RpcError as err:
                    log_entry.result = LoggerResult.FAIL
                    context["error"] = "{}: {}".format(err.code(), err.details())
                    return self.render_to_response(context)
                else:
                    log_entry.result = LoggerResult.SUCCESS

            sync_result = Report.objects.sync_with_list(service=service, reports=REPORTS[service].list_reports())
            messages.success(self.request, format_sync_result(sync_result))

        error = False
        if form.cleaned_data["config"]:
            try:
                config = [
                    DbReport(**report)
                    for document in yaml.safe_load_all(form.cleaned_data["config"])
                    for report in document
                ]
            except (ValidationError, TypeError) as err:
                messages.error(self.request, _("Configuration invalid: %(err)s") % {"err": err})
                return super().form_invalid(form)
            except ParserError:
                messages.error(self.request, _("Configuration is not a valid YAML"))
                return super().form_invalid(form)

            for conf in config:
                try:
                    with transaction.atomic():
                        report = Report.objects.get(service=conf.service, name=conf.name)
                        report.label = conf.label
                        report.description = conf.description
                        report.tags = ";".join(conf.tags)
                        for param in report.parameters.all():
                            for order, p in enumerate(
                                conf.input_params if param.is_input else conf.output_params, start=1
                            ):
                                if param.name == p.name:
                                    param.label = p.label
                                    param.order = order
                                    param.save()
                                    break
                            else:
                                raise ValueError(
                                    "{} parameter {} is missing".format(
                                        "Input" if param.is_input else "Output", param.name
                                    )
                                )
                        report.save()

                        if len(conf.input_params) + len(conf.output_params) != report.parameters.count():
                            raise ValueError("Config contains params that don't exist on server")

                        messages.success(
                            self.request,
                            _("Configuration of report %(report)s in service %(service)s has been loaded")
                            % {"report": conf.name, "service": conf.service},
                        )
                except Report.DoesNotExist:
                    error = True
                    messages.error(
                        self.request,
                        _(
                            "Report %(report)s in service %(service)s does not exist. Try synchronizing the reports database."
                        )
                        % {"report": conf.name, "service": conf.service},
                    )
                except ValueError as err:
                    error = True
                    messages.error(
                        self.request,
                        _("Configuration of report %(report)s in service %(service)s has error: %(err)s")
                        % {"report": conf.name, "service": conf.service, "err": err},
                    )

        if error:
            return super().form_invalid(form)
        else:
            return super().form_valid(form)


class SyncReportsView(PermissionRequiredMixin, TemplateView):
    """Sync reports view."""

    template_name = "admin/ferda/report/sync_reports.html"
    permission_required = ("ferda.add_report", "ferda.change_report", "ferda.delete_report")

    def get_context_data(self, **kwargs):
        """Return view context."""
        context = super().get_context_data(**kwargs)
        context["opts"] = Report._meta
        return context

    def get(self, request):
        """Show expected sync results."""
        context = self.get_context_data()
        expected_sync_result = {
            service: Report.objects.sync_with_list(service=service, reports=client.list_reports(), dry_run=True)
            for service, client in REPORTS.items()
        }
        context["expected_sync_result"] = expected_sync_result

        return self.render_to_response(context)

    def post(self, request):
        """Perform actual db sync."""
        sync_result = {
            service: Report.objects.sync_with_list(service=service, reports=client.list_reports())
            for service, client in REPORTS.items()
        }
        for service in sync_result:
            messages.success(
                request,
                format_sync_result(
                    sync_result[service],
                    title=_("Sync result of service %(service)s") % {"service": service},
                ),
            )
        return redirect(reverse("admin:ferda_report_changelist"))
