"""Common api schemas and utils."""

from decimal import Decimal
from functools import cached_property
from typing import Any, Dict, Generic, List, Optional, Sequence, TypeVar, Union, cast
from urllib.parse import urlencode, urljoin

from django.contrib.auth.models import AbstractUser
from django.http import HttpRequest
from ninja import Schema
from ninja.errors import HttpError
from ninja.renderers import JSONRenderer
from ninja.responses import NinjaJSONEncoder
from pydantic import BaseModel, GetCoreSchemaHandler
from pydantic_core import CoreSchema, core_schema

T = TypeVar("T", bound=BaseModel)


class PydanticJsonEncoder(NinjaJSONEncoder):
    """Pydantic JSON encoder."""

    def default(self, o):
        """Encode to json using pydantic encoder."""
        if isinstance(o, BaseModel):
            return o.model_dump(mode="json")
        if isinstance(o, Decimal):
            return str(o)
        return super().default(o)


class PydanticJsonRenderer(JSONRenderer):
    """Pydantic JSON renderer."""

    encoder_class = PydanticJsonEncoder


class PaginatedList(Generic[T], Schema):
    """Paginated result."""

    next: Optional[str] = None
    results: List[T]


class TokenPagination(Schema):
    """Token pagination schema."""

    next_page_token: str = ""
    items_left: Optional[int] = None


class TokenPaginatedList(BaseModel, Generic[T]):
    """Token paginated result."""

    results: List[T]
    pagination: Optional[TokenPagination] = None


class Error(Schema):
    """Error response."""

    detail: str


class InvalidDataError(Error):
    """Invalid data error response."""

    fields: Sequence[str]


class RegistryReference(str):
    """Registry reference field type."""

    @cached_property
    def type(self) -> str:
        """Parse reference type."""
        return self.partition(".")[0]

    @cached_property
    def value(self) -> str:
        """Parse reference value."""
        return self.partition(".")[2]

    @classmethod
    def validate(cls, v: str) -> "RegistryReference":
        """Check registry reference format."""
        if "." not in v:
            raise ValueError("Reference type and value must be separated by a dot.")
        return cls(v)

    @classmethod
    def __get_pydantic_core_schema__(cls, source_type: Any, handler: GetCoreSchemaHandler) -> CoreSchema:
        return core_schema.no_info_after_validator_function(cls.validate, handler(str))

    def __repr__(self):
        """Return distinct representation."""
        return '{}(type="{}", value="{}")'.format(self.__class__.__name__, self.type, self.value)


def build_full_url(request, query_params: Optional[Dict[str, Any]] = None) -> str:
    """Build full URL from request by updating query params."""
    # We need to convert request.GET to dict to correctly process multiple values for the same key
    params = dict(request.GET)
    if query_params:
        params.update(query_params)
    url = urljoin(request.build_absolute_uri(request.path), "?%s" % urlencode(params, doseq=True))
    return url


def check_permissions(request: HttpRequest, permissions: Optional[Union[str, Sequence[str]]] = None) -> None:
    """Check permissions and throw exception when they are not sufficient.

    Attributes:
        request: HttpRequest with `user` attribute.
        permissions: Permission name or sequence of permission names that are required.

    Raises:
        HttpError: When user is not authorized.
    """
    if isinstance(permissions, str):
        permissions_list: Sequence[str] = [permissions]
    else:
        permissions_list = permissions or []

    if not (request.user and request.user.is_authenticated):
        raise HttpError(401, "Unauthorized")

    for perm in permissions_list:
        if not cast(AbstractUser, request.user).has_perm(perm):
            raise HttpError(401, "Unauthorized")
