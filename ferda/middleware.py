"""Ferda middlewares."""

from django.utils.timezone import now


class SessionExpiresMiddleware:
    """Middleware for attaching cookie with session expiry date.

    SessionMiddleware attaches http-only cookie and it's unsafe to enable access to it to JavaScript code.
    Hence, this middleware attaches safe cookie which contains only the session expiry date.
    """

    def __init__(self, get_response):
        self.get_response = get_response

    def __call__(self, request):
        """Attach session_expires cookie to each response."""
        response = self.get_response(request)
        if request.user.is_authenticated:
            response.set_cookie("session_expires", request.session.get_expiry_date(), secure=True, samesite="Strict")
        response.set_cookie("server_time", now(), secure=True, samesite="Strict")
        return response
