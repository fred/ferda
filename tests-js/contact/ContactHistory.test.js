import { createLocalVue } from '@vue/test-utils'
import axios from 'axios'
import MockAdapter from 'axios-mock-adapter'
import flushPromises from 'flush-promises'

import { CONTACT_HARRY } from 'ferda-test/TestObjects'
import { initWrapper } from 'ferda-test/TestUtils'
import { OBJECT_TYPE } from 'ferda/RegistryMixin'
import { URLS } from 'ferda/Urls'
import ObjectHistory from 'ferda/common/ObjectHistory.vue'
import ContactHistory from 'ferda/contact/ContactHistory.vue'

const localVue = createLocalVue()
localVue.component('ObjectHistory', ObjectHistory)

describe('ContactHistory', () => {
    let axios_mock
    let wrapper

    beforeEach(async() => {
        axios_mock = new MockAdapter(axios)
        axios_mock.onGet(URLS.object_info_history_ajax(OBJECT_TYPE.CONTACT, 'UUID-HARRY', 'V1'))
            .reply(200, CONTACT_HARRY)
        axios_mock.onGet(URLS.object_info_history_ajax(OBJECT_TYPE.CONTACT, 'UUID-HARRY', 'V2'))
            .reply(200, CONTACT_HARRY)

        wrapper = initWrapper(ContactHistory, {
            localVue,
            propsData: {
                uuid: 'UUID-HARRY',
                object_handle: 'HARRY',
                from_history_id: 'V1',
                to_history_id: 'V2',
                object_history: {
                    contact_id: 'UUID-HARRY',
                    timeline: [{
                        contact_history_id: 'V1',
                        valid_from: '2000-01-01T03:00:00Z',
                    }, {
                        contact_history_id: 'V2',
                        valid_from: '2001-01-01T03:00:00+02:00',
                    }],
                    valid_to: null,
                },
            },
            stubs: {
                /* ObjectHistory specific */
                AppHeader: true,
                EmbeddedSearchBar: true,
                HistoryNavigation: true,
                RelatedRegistrarHistory: true,
                ObjectHistoryEvents: true,
                StateFlagsHistory: true,
                /* ContactHistory specific */
                ContactHistoryInfo: true,
                ContactHistoryAddresses: true,
            },
        }, false)
        await flushPromises()
    })

    test('matches snapshot', () => {
        expect(wrapper.element).toMatchSnapshot()
    })
})
