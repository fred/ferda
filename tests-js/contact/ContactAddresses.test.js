import { initWrapper } from 'ferda-test/TestUtils'
import ContactDetailAddresses from 'ferda/contact/ContactDetailAddresses.vue'

const contact = {
    organization: 'Hogwarts School of Witchcraft and Wizardry',
    name: 'Harry Potter',
    place: {
        street: ['Privet Drive 4', 'Diagon Alley'],
        city: 'Little Whinging',
        country_code: 'UK',
    },
    shipping_address1: {
        street: ['Platform 9¾', "King's Cross Station"],
        city: 'London',
        country_code: 'UK',
    },
    publish: {
        organization: true,
        name: false,
        place: true,
    },
}

describe('ContactAddresses', () => {
    let wrapper

    beforeEach(() => {
        wrapper = initWrapper(ContactDetailAddresses, {
            propsData: {
                contact: contact,
                verbose: false,
            },
        })
    })

    test('matches snapshot', () => {
        expect(wrapper.element).toMatchSnapshot()
    })

    test('data', () => {
        expect(wrapper.vm.verbose).toBe(false)
        expect(wrapper.vm.contact).toEqual(contact)
    })

    test('method condense_address', () => {
        const address = {
            street: ['Privet Drive 4', 'Diagon Alley'],
            city: 'Little Whinging',
            country_code: 'UK',
        }
        expect(wrapper.vm.condense_address(address)).toEqual('Privet Drive 4, Diagon Alley, Little Whinging, UK')
    })
})

describe('ContactAddresses verbose', () => {
    let wrapper

    beforeEach(() => {
        wrapper = initWrapper(ContactDetailAddresses, {
            propsData: {
                contact: contact,
                verbose: true,
            },
        })
    })

    test('matches snapshot', () => {
        expect(wrapper.element).toMatchSnapshot()
    })
})
