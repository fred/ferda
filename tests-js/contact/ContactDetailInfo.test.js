import { initWrapper } from 'ferda-test/TestUtils'
import { MESSAGE_FORM_FIELDS, MESSAGE_TYPE } from 'ferda/MessagesMixin'
import { CONTACT_MAIN_ITEMS } from 'ferda/RegistryMixin'
import ContactDetailInfo from 'ferda/contact/ContactDetailInfo.vue'

const contact = {
    organization: 'Hogwarts School of Witchcraft and Wizardry',
    name: 'Harry Potter',
    contact_handle: 'HARRY',
    warning_letter: 'not_specified',
    publish: {
        organization: true,
        name: false,
    },
    events: {},
}

describe('ContactDetailInfo', () => {
    let wrapper

    beforeEach(() => {
        const contact_copy = Object.assign({}, contact)
        contact_copy.additional_identifier = {value: '123456', type: 'passport_number'}
        wrapper = initWrapper(ContactDetailInfo, {
            propsData: {
                contact: contact_copy,
                verbose: false,
            },
            stubs: {
                FontAwesomeIcon: true,
            },
        })
    })

    test('matches snapshot', () => {
        expect(wrapper.element).toMatchSnapshot()
    })

    test('computed main_info_list', () => {
        expect(wrapper.vm.main_info_list).toEqual([
            {
                label: 'contact:organization',
                value: 'Hogwarts School of Witchcraft and Wizardry',
                public_access: true,
                type: 'organization',
            }, {
                label: 'contact:name',
                value: 'Harry Potter',
                public_access: false,
                type: 'name',
            }, {
                label: 'contact:additional_identifier',
                value: '123456 (contact:identifier_type.passport_number)',
                public_access: undefined,
                type: 'additional_identifier',
            },
        ])
    })

    test('method get_search_message_params returns correct values', () => {
        const params_emails = wrapper.vm.get_search_message_params(CONTACT_MAIN_ITEMS.EMAILS, 'test@email.org')
        const params_notify_emails =
            wrapper.vm.get_search_message_params(CONTACT_MAIN_ITEMS.NOTIFY_EMAILS, 'test@notify.org')
        const params_phone = wrapper.vm.get_search_message_params(CONTACT_MAIN_ITEMS.TELEPHONE, '+123.666666666')

        expect(params_emails.get(MESSAGE_FORM_FIELDS.RECIPIENTS)).toBe('test@email.org')
        expect(params_emails.get(MESSAGE_FORM_FIELDS.MESSAGE_TYPE)).toBe(MESSAGE_TYPE.EMAIL)

        expect(params_notify_emails.get(MESSAGE_FORM_FIELDS.RECIPIENTS)).toBe('test@notify.org')
        expect(params_notify_emails.get(MESSAGE_FORM_FIELDS.MESSAGE_TYPE)).toBe(MESSAGE_TYPE.EMAIL)

        expect(params_phone.get(MESSAGE_FORM_FIELDS.RECIPIENTS)).toBe('+123.666666666')
        expect(params_phone.get(MESSAGE_FORM_FIELDS.MESSAGE_TYPE)).toBe(MESSAGE_TYPE.SMS)
    })

    test('method is_list_icon_visible returns correct values', () => {
        expect(wrapper.vm.is_list_icon_visible(CONTACT_MAIN_ITEMS.EMAILS, 'johndoe@email.org')).toStrictEqual(true)
        expect(wrapper.vm.is_list_icon_visible(CONTACT_MAIN_ITEMS.TELEPHONE, '+420.123456789')).toStrictEqual(true)
        expect(wrapper.vm.is_list_icon_visible(CONTACT_MAIN_ITEMS.NOTIFY_EMAILS, 'john@email.org')).toStrictEqual(true)

        expect(wrapper.vm.is_list_icon_visible('something invalid', 'johndoe@email.org')).toStrictEqual(false)
        expect(wrapper.vm.is_list_icon_visible(CONTACT_MAIN_ITEMS.EMAILS, null)).toStrictEqual(false)
        expect(wrapper.vm.is_list_icon_visible(CONTACT_MAIN_ITEMS.EMAILS, undefined)).toStrictEqual(false)
        expect(wrapper.vm.is_list_icon_visible(CONTACT_MAIN_ITEMS.EMAILS, '')).toStrictEqual(false)
    })
})

describe('ContactDetailInfo verbose', () => {
    let wrapper

    beforeEach(() => {
        const contact_copy = Object.assign({}, contact)
        wrapper = initWrapper(ContactDetailInfo, {
            propsData: {
                contact: contact_copy,
                verbose: true,
            },
            stubs: {
                FontAwesomeIcon: true,
            },
        })
    })

    test('matches snapshot', () => {
        expect(wrapper.element).toMatchSnapshot()
    })

    test('computed main_info_list', () => {
        expect(wrapper.vm.main_info_list).toContainEqual({
            public_access: true,
            label: 'contact:organization',
            value: 'Hogwarts School of Witchcraft and Wizardry',
            type: 'organization',
        })
        expect(wrapper.vm.main_info_list).toContainEqual({
            public_access: false,
            label: 'contact:name',
            value: 'Harry Potter',
            type: 'name',
        })
        expect(wrapper.vm.main_info_list).toContainEqual({
            public_access: undefined,
            label: 'contact:telephone',
            value: undefined,
            type: 'telephone',
        })
        expect(wrapper.vm.main_info_list).toContainEqual({
            public_access: undefined,
            label: 'contact:email',
            value: undefined,
            type: 'emails',
        })
        expect(wrapper.vm.main_info_list).toContainEqual({
            public_access: undefined,
            label: 'contact:warning_letter',
            value: 'contact:warning_letter_preference.2',
            type: 'warning_letter',
        })
    })

    test('method get_label works correctly', () => {
        expect(wrapper.vm.get_label('emails')).toBe('contact:email')
        expect(wrapper.vm.get_label('notify_emails')).toBe('contact:notify_email')
        expect(wrapper.vm.get_label('name')).toBe('contact:name')
    })

    test('method get_public_access works correctly', () => {
        expect(wrapper.vm.get_public_access('emails')).toStrictEqual(undefined)
        expect(wrapper.vm.get_public_access('notify_emails')).toStrictEqual(undefined)
        expect(wrapper.vm.get_public_access('organization')).toStrictEqual(true)
    })
})
