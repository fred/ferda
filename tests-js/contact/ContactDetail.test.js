import { CONTACT_OBIWAN } from 'ferda-test/TestObjects'
import { getClonedStoreOptions, initWrapper } from 'ferda-test/TestUtils'
import ContactDetail from 'ferda/contact/ContactDetail.vue'

describe('ContactDetail', () => {
    let wrapper
    const StoreOptions = getClonedStoreOptions()

    beforeEach(() => {
        StoreOptions.modules.general.getters.permissions = () => ['ferda.view_contactrepresentative']
        wrapper = initWrapper(ContactDetail, {
            propsData: {
                uuid: '00000000-0000-0000-0000-000000000000',
                history_id: '00000000-0000-0000-0000-000000000001',
                contact: CONTACT_OBIWAN,
                has_history: true,
            },
            stubs: {
                FontAwesomeIcon: true,
            },
            customStoreOptions: StoreOptions,
        })
    })

    test('matches snapshot', () => {
        expect(wrapper.element).toMatchSnapshot()
    })
})
