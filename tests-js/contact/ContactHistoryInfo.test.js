import { initWrapper } from 'ferda-test/TestUtils'
import ContactHistoryInfo from 'ferda/contact/ContactHistoryInfo.vue'

describe('ContactHistoryInfo', () => {
    let wrapper

    beforeEach(() => {
        wrapper = initWrapper(ContactHistoryInfo, {
            propsData: {
                info_diff: {
                    organization: {old_value: null, new_value: null, differs: false},
                    name: {old_value: 'Harry', new_value: 'Ron', differs: true},
                    additional_identifier: {old_value: null, new_value: null, differs: false},
                    vat_identification_number: {old_value: null, new_value: null, differs: false},
                    telephone: {old_value: '123456789', new_value: '123456789', differs: false},
                    fax: {old_value: null, new_value: null, differs: false},
                    emails: {old_value: null, new_value: null, differs: false},
                    notify_emails: {old_value: null, new_value: null, differs: false},
                    warning_letter: { old_value: 'Not specified', new_value: 'Not specified', differs: false},

                    publish: {
                        organization: {differs: false},
                        name: {differs: false},
                        additional_identifier: {differs: false},
                        vat_identification_number: {differs: false},
                        telephone: {differs: true},
                        fax: {differs: false},
                        emails: {differs: false},
                        notify_emails: {differs: false},
                    },
                },
            },
            stubs: {
                FontAwesomeIcon: true,
            },
        })
    })

    test('matches snapshot', () => {
        expect(wrapper.element).toMatchSnapshot()
    })
})
