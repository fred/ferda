import axios from 'axios'
import MockAdapter from 'axios-mock-adapter'

import { CONTACT_REPRESENTATIVE } from 'ferda-test/TestObjects'
import { initWrapper } from 'ferda-test/TestUtils'
import { REGISTRAR_ALL_ITEMS } from 'ferda/RegistryMixin'
import { URLS } from 'ferda/Urls'
import ContactRepresentativeEditForm from 'ferda/contact/ContactRepresentativeEditForm.vue'

describe('ContactRepresentativeEditForm', () => {
    let axios_mock
    let wrapper

    beforeEach(async() => {
        axios_mock = new MockAdapter(axios)
        axios_mock.onGet(URLS.countries_list()).reply(200, { 'US': 'United States of America' })
        wrapper = initWrapper(ContactRepresentativeEditForm, {
            propsData: {
                address: {
                    id: CONTACT_REPRESENTATIVE.id,
                    history_id: CONTACT_REPRESENTATIVE.history_id,
                    contact_id: CONTACT_REPRESENTATIVE.contact_id,
                    name: CONTACT_REPRESENTATIVE.name,
                    organization: CONTACT_REPRESENTATIVE.organization,
                    street: CONTACT_REPRESENTATIVE.place.street,
                    city: CONTACT_REPRESENTATIVE.place.city,
                    state_or_province: CONTACT_REPRESENTATIVE.place.state_or_province,
                    postal_code: CONTACT_REPRESENTATIVE.place.postal_code,
                    country_code: CONTACT_REPRESENTATIVE.place.country_code,
                    company: CONTACT_REPRESENTATIVE.place.company,
                    telephone: CONTACT_REPRESENTATIVE.telephone,
                    email: CONTACT_REPRESENTATIVE.email,
                },
                dialog_opened: false,
                is_loading: false,
            },
            mocks: {
                '$cookies': {
                    get: () => jest.fn(),
                },
            },
        })
        wrapper.vm.$refs.form.validate = () => jest.fn()
        wrapper.vm.$refs.form.resetValidation = () => jest.fn()
    })

    test('matches snapshot', () => {
        expect(wrapper.element).toMatchSnapshot()
    })

    test('method submit_dialog works correctly - is valid', async() => {
        wrapper.vm.$refs = {
            form: { validate: jest.fn(() => true) },
        }
        await wrapper.setData({ street1: 'Monster lane 22', street2: '', street3: '' })
        await wrapper.vm.submit_dialog()

        expect(wrapper.emitted('submit-dialog')[0][0]).toStrictEqual({
            city: CONTACT_REPRESENTATIVE.place.city,
            country_code: CONTACT_REPRESENTATIVE.place.country_code,
            email: CONTACT_REPRESENTATIVE.email,
            name: CONTACT_REPRESENTATIVE.name,
            organization: CONTACT_REPRESENTATIVE.place.company,
            postal_code: CONTACT_REPRESENTATIVE.place.postal_code,
            state_or_province: CONTACT_REPRESENTATIVE.place.state_or_province,
            street1: 'Monster lane 22',
            street2: '',
            street3: '',
            telephone: CONTACT_REPRESENTATIVE.telephone,
        })
    })

    test('method submit_dialog works correctly - is not valid', async() => {
        wrapper.vm.$refs = {
            form: { validate: jest.fn(() => false) },
        }
        await wrapper.vm.submit_dialog()
        expect(wrapper.emitted('submit-dialog')).toBeFalsy()
    })

    test('method get_initial_value returns correct values', () => {
        for (const key of Object.keys(REGISTRAR_ALL_ITEMS)) {
            expect(wrapper.vm.get_initial_value(key)).toBe(CONTACT_REPRESENTATIVE[key])
        }
    })

    test('open dialog displays stored data', async() => {
        await wrapper.setData({ company: 'test' })
        await wrapper.setProps({ dialog_opened: true })

        expect(wrapper.vm.company).toStrictEqual(CONTACT_REPRESENTATIVE.place.company)
    })
})
