import { CONTACT_RELATED_DOMAINS } from 'ferda-test/TestObjects'
import { initWrapper } from 'ferda-test/TestUtils'
import { RELATED_DOMAIN_TABLE_COLUMNS } from 'ferda/RegistryMixin'
import RelatedDomainTable from 'ferda/contact/RelatedDomainTable.vue'

describe('RelatedDomainTable', () => {
    let wrapper

    beforeEach(() => {
        wrapper = initWrapper(RelatedDomainTable, {
            propsData: {
                related_domains: CONTACT_RELATED_DOMAINS,
                page_size: 5,
                current_page: 1,
                next_page: 'c9dd025e-2db3-42bd-bfde-0f04a1f0aa9345c1',
                items_left: 15,
                sort_by: `domain.${RELATED_DOMAIN_TABLE_COLUMNS.FQDN}`,
                sort_desc: false,
            },
        })
    })

    test('matches snapshot', () => {
        expect(wrapper.element).toMatchSnapshot()
    })

    test('empty matches snapshot', () => {
        wrapper = initWrapper(RelatedDomainTable, { propsData: {
            page_size: 10,
            current_page: 1,
            items_left: 15,
            sort_by: 'domain.fqdn',
            sort_desc: false,
        }})
        expect(wrapper.element).toMatchSnapshot()
    })

    test('method get_translated_contact_roles returns correct result', () => {
        expect(wrapper.vm.get_translated_contact_roles(['holder', 'admin'])).toStrictEqual(
            'domain:domain_owner, domain:domain_administrative_contact',
        )
    })

    test('method get_current_delete_date returns correct values', () => {
        expect(wrapper.vm.get_current_delete_date(
            { events: { unregistered: null }, delete_candidate_at: '2021-01-01T12:00:00+01:00' }),
        ).toBe('Jan 1, 2021')

        expect(wrapper.vm.get_current_delete_date(
            {
                events: { unregistered: { timestamp: '2022-01-01T12:00:00+01:00' }},
                delete_candidate_at: '2023-01-01T12:00:00+01:00',
            }),
        ).toBe('Jan 1, 2022')

        expect(wrapper.vm.get_current_delete_date(
            { events: { unregistered: null }, delete_candidate_scheduled_at: '2021-01-01T12:00:00+01:00' }),
        ).toBe('Jan 1, 2021')
    })
})
