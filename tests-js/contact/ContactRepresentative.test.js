import axios from 'axios'
import MockAdapter from 'axios-mock-adapter'
import flushPromises from 'flush-promises'

import { CONTACT_REPRESENTATIVE } from 'ferda-test/TestObjects'
import { getClonedStoreOptions, initWrapper } from 'ferda-test/TestUtils'
import { URLS } from 'ferda/Urls'
import ContactRepresentative from 'ferda/contact/ContactRepresentative.vue'

const REPRESENTATIVE_FORM_STRUCTURE = Object.freeze({
    city: CONTACT_REPRESENTATIVE.place.city,
    company: CONTACT_REPRESENTATIVE.organization,
    country_code: CONTACT_REPRESENTATIVE.place.country_code,
    email: CONTACT_REPRESENTATIVE.email,
    name: CONTACT_REPRESENTATIVE.name,
    postal_code: CONTACT_REPRESENTATIVE.place.postal_code,
    representative_id: CONTACT_REPRESENTATIVE.id,
    state_or_province: CONTACT_REPRESENTATIVE.place.state_or_province,
    street: CONTACT_REPRESENTATIVE.place.street,
    telephone: CONTACT_REPRESENTATIVE.telephone,
})

describe('ContactRepresentative', () => {
    let axios_mock
    let wrapper
    let store
    const initial_log = console.log

    beforeEach(async() => {
        axios_mock = new MockAdapter(axios)
        axios_mock.onGet(URLS.contact_representative_by_contact_id_ajax(CONTACT_REPRESENTATIVE.id))
            .reply(200, CONTACT_REPRESENTATIVE)
        axios_mock.onPost(URLS.contact_representative_add_ajax()).reply(200)
        axios_mock.onPut(URLS.contact_representative_update_delete_ajax(CONTACT_REPRESENTATIVE.id)).reply(200)
        axios_mock.onDelete(URLS.contact_representative_update_delete_ajax(CONTACT_REPRESENTATIVE.id)).reply(200)
        store = getClonedStoreOptions()
        store.modules.general.getters.permissions = () => [
            'ferda.add_contactrepresentative',
            'ferda.delete_contactrepresentative',
            'ferda.change_contactrepresentative',
        ]

        wrapper = initWrapper(ContactRepresentative, {
            propsData: { contact_id: CONTACT_REPRESENTATIVE.id },
            mocks: {
                $notify: jest.fn(),
                $cookies: { get: jest.fn() },
            },
            customStoreOptions: store,
        })
        await flushPromises()
    })

    afterEach(() => {
        console.log = initial_log
    })

    test('found matches snapshot', () => {
        expect(wrapper.element).toMatchSnapshot()
    })

    test('not found matches snapshot', () => {
        wrapper = initWrapper(ContactRepresentative, { propsData: { contact_id: 'SULLY' }})
        expect(wrapper.element).toMatchSnapshot()
    })

    test('method delete_representative success', async() => {
        console.log = jest.fn()
        await wrapper.vm.delete_representative(CONTACT_REPRESENTATIVE.id)

        expect(wrapper.vm.address).toStrictEqual(null)
        expect(wrapper.vm.$notify).toHaveBeenCalledWith(wrapper.vm.SUCCESS_CHANGES_SAVED)
        expect(console.log).toHaveBeenCalledWith('success: notifications:changes_saved')
    })

    test('method delete_representative fails by server error', async() => {
        console.log = jest.fn()
        axios_mock.onDelete(URLS.contact_representative_update_delete_ajax(CONTACT_REPRESENTATIVE.id)).reply(500)
        await wrapper.vm.delete_representative()

        expect(wrapper.vm.$notify).toHaveBeenCalledWith(wrapper.vm.ERROR_SOMETHING_WENT_WRONG)
    })

    test('method load_representative works correctly', async() => {
        await wrapper.vm.load_representative()

        expect(wrapper.vm.address).toStrictEqual(REPRESENTATIVE_FORM_STRUCTURE)
        expect(wrapper.vm.$notify).not.toHaveBeenCalled()
    })

    test('method add_representative fails by server error', async() => {
        console.log = jest.fn()
        axios_mock.onPost(URLS.contact_representative_add_ajax()).reply(500)
        jest.spyOn(wrapper.vm, 'load_representative')
        await wrapper.vm.add_representative(REPRESENTATIVE_FORM_STRUCTURE)

        expect(wrapper.vm.load_representative).not.toHaveBeenCalled()
        expect(wrapper.vm.is_loading).toStrictEqual(false)
        expect(wrapper.vm.$notify).toHaveBeenCalledWith(wrapper.vm.ERROR_SOMETHING_WENT_WRONG)
        expect(console.log).toHaveBeenCalledWith('error: notifications:something_went_wrong')
    })

    test('method add_representative works correctly', async() => {
        console.log = jest.fn()
        jest.spyOn(wrapper.vm, 'load_representative')
        await wrapper.setData({ address: null })
        await wrapper.vm.add_representative(REPRESENTATIVE_FORM_STRUCTURE)

        expect(wrapper.vm.address).toStrictEqual(REPRESENTATIVE_FORM_STRUCTURE)
        expect(wrapper.vm.load_representative).toHaveBeenCalled()
        expect(wrapper.vm.is_loading).toStrictEqual(false)
        expect(wrapper.vm.dialog_opened).toStrictEqual(false)
        expect(wrapper.vm.$notify).toHaveBeenCalledWith(wrapper.vm.SUCCESS_CHANGES_SAVED)
        expect(console.log).toHaveBeenCalledWith('success: notifications:changes_saved')
    })

    test('method update_representative fails by server error', async() => {
        console.log = jest.fn()
        axios_mock.onPut(URLS.contact_representative_update_delete_ajax(CONTACT_REPRESENTATIVE.id)).reply(500)
        jest.spyOn(wrapper.vm, 'load_representative')
        await wrapper.vm.update_representative(REPRESENTATIVE_FORM_STRUCTURE)

        expect(wrapper.vm.load_representative).not.toHaveBeenCalled()
        expect(wrapper.vm.is_loading).toStrictEqual(false)
        expect(wrapper.vm.$notify).toHaveBeenCalledWith(wrapper.vm.ERROR_SOMETHING_WENT_WRONG)
        expect(console.log).toHaveBeenCalledWith('error: notifications:something_went_wrong')
    })

    test('method update_representative works correctly', async() => {
        console.log = jest.fn()
        jest.spyOn(wrapper.vm, 'load_representative')
        await wrapper.vm.update_representative(REPRESENTATIVE_FORM_STRUCTURE)

        expect(wrapper.vm.address).toStrictEqual(REPRESENTATIVE_FORM_STRUCTURE)
        expect(wrapper.vm.load_representative).toHaveBeenCalled()
        expect(wrapper.vm.is_loading).toStrictEqual(false)
        expect(wrapper.vm.dialog_opened).toStrictEqual(false)
        expect(wrapper.vm.$notify).toHaveBeenCalledWith(wrapper.vm.SUCCESS_CHANGES_SAVED)
        expect(console.log).toHaveBeenCalledWith('success: notifications:changes_saved')
    })

    test('method load_representative fails by server error', async() => {
        console.log = jest.fn()
        axios_mock.onGet(URLS.contact_representative_by_contact_id_ajax(CONTACT_REPRESENTATIVE.id)).reply(500)
        await wrapper.vm.load_representative()

        expect(wrapper.vm.$notify).toHaveBeenCalledWith(wrapper.vm.ERROR_SOMETHING_WENT_WRONG)
    })

    test('representative_title has correct values', async() => {
        await wrapper.setData({ last_action_type: wrapper.vm.REPRESENTATIVE_ACTION.ADD })
        expect(wrapper.vm.representative_title).toBe('contact:add_representative')

        await wrapper.setData({ last_action_type: wrapper.vm.REPRESENTATIVE_ACTION.EDIT })
        expect(wrapper.vm.representative_title).toBe('contact:update_representative')

        await wrapper.setData({ last_action_type: wrapper.vm.REPRESENTATIVE_ACTION.DELETE })
        expect(wrapper.vm.representative_title).toBe('contact:delete_representative.header')

        await wrapper.setData({ last_action_type: 'something else' })
        expect(wrapper.vm.representative_title).toBe('')
    })

    test('method show_representative_form works correctly', () => {
        wrapper.vm.show_representative_form(wrapper.vm.REPRESENTATIVE_ACTION.EDIT)

        expect(wrapper.vm.last_action_type).toBe(wrapper.vm.REPRESENTATIVE_ACTION.EDIT)
        expect(wrapper.vm.dialog_opened).toStrictEqual(true)
    })
})
