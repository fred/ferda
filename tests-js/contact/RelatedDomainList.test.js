import axios from 'axios'
import MockAdapter from 'axios-mock-adapter'
import flushPromises from 'flush-promises'
import Vuetify from 'vuetify'

import { CONTACT_RELATED_DOMAINS_RESPONSE } from 'ferda-test/TestObjects'
import { initWrapper } from 'ferda-test/TestUtils'
import { CONTACT_ROLES, RELATED_DOMAIN_TABLE_COLUMNS } from 'ferda/RegistryMixin'
import { URLS } from 'ferda/Urls'
import RelatedDomainList from 'ferda/contact/RelatedDomainList.vue'

const object_id = '705f420e-fc37-4276-1bc7-84c42bc51351'

describe('RelatedDomainList', () => {
    let wrapper
    let axios_mock
    const initial_log = console.log
    const initial_search = window.location.search

    beforeEach(() => {
        axios_mock = new MockAdapter(axios)
        axios_mock.onGet(URLS.domains_by_contact_ajax(object_id)).reply(200, CONTACT_RELATED_DOMAINS_RESPONSE)
        wrapper = initWrapper(RelatedDomainList, {
            propsData: {
                object_handle: 'HANDLE',
                object_id,
                label: 'Domains',
            },
            mocks: { $notify: jest.fn() },
        })
    })

    afterEach(() => {
        delete window.location
        window.location = { search: initial_search }
        console.log = initial_log
    })

    test('matches snapshot', () => {
        expect(wrapper.element).toMatchSnapshot()
    })

    test('related domains section is loaded when particular URL param is present', async() => {
        delete window.location
        window.location = { search: '?domains=true' }

        wrapper = initWrapper(RelatedDomainList, {
            propsData: { object_handle: 'HANDLE', object_id, label: 'Domains' },
            mocks: { $notify: jest.fn() },
        })
        await flushPromises()

        expect(wrapper.vm.related_domains.length).not.toBe(0)
    })

    test('method get_related_domains loads data correctly - standard refresh', async() => {
        console.log = jest.fn()
        await wrapper.vm.get_related_domains()

        expect(wrapper.vm.related_domains).toStrictEqual(CONTACT_RELATED_DOMAINS_RESPONSE.results)
        expect(wrapper.vm.next_page).toBe(CONTACT_RELATED_DOMAINS_RESPONSE.pagination.next_page_token)
        expect(wrapper.vm.items_left).toBe(CONTACT_RELATED_DOMAINS_RESPONSE.pagination.items_left)
        expect(wrapper.vm.$notify).not.toHaveBeenCalled()
        expect(console.log).not.toHaveBeenCalled()
    })

    test('method get_related_domains loads data correctly - pagination', async() => {
        const original_current_page = wrapper.vm.current_page
        console.log = jest.fn()
        await wrapper.vm.get_related_domains(true, false)

        expect(wrapper.vm.related_domains).toStrictEqual(CONTACT_RELATED_DOMAINS_RESPONSE.results)
        expect(wrapper.vm.next_page).toBe(CONTACT_RELATED_DOMAINS_RESPONSE.pagination.next_page_token)
        expect(wrapper.vm.items_left).toBe(CONTACT_RELATED_DOMAINS_RESPONSE.pagination.items_left)
        expect(wrapper.vm.current_page).toStrictEqual(original_current_page + 1)
        expect(wrapper.vm.$notify).not.toHaveBeenCalled()
        expect(console.log).not.toHaveBeenCalled()
    })

    test('method get_related_domains fails by server error', async() => {
        console.log = jest.fn()
        axios_mock.onGet(URLS.domains_by_contact_ajax(object_id)).reply(500)
        await wrapper.vm.get_related_domains()

        expect(wrapper.vm.related_domains).toStrictEqual([])
        expect(wrapper.vm.next_page).toStrictEqual(null)
        expect(wrapper.vm.items_left).toStrictEqual(-1)
        expect(wrapper.vm.$notify).toHaveBeenCalledWith({
            duration: -1,
            text: 'notifications:something_went_wrong',
            title: 'notifications:operation_failed',
            type: 'error',
        })
        expect(console.log).toHaveBeenCalledWith('error: notifications:something_went_wrong')
    })

    test('method order_by_transformed returns correct values - fqdn', async() => {
        expect(wrapper.vm.order_by_transformed).toBe('+fqdn')
        await wrapper.setData({ sort_desc: true })
        expect(wrapper.vm.order_by_transformed).toBe('-fqdn')
    })

    test('method order_by_transformed returns correct values - registrar', async() => {
        wrapper = initWrapper(RelatedDomainList, {
            propsData: { object_handle: 'HANDLE', object_id, label: 'Domains' },
            data() {
                return {
                    sort_by: `domain.${RELATED_DOMAIN_TABLE_COLUMNS.SPONSORING_REGISTRAR}`,
                }
            },
            mocks: { $notify: jest.fn() },
        })
        expect(wrapper.vm.order_by_transformed).toBe('+registrar')

        await wrapper.setData({ sort_desc: true })
        expect(wrapper.vm.order_by_transformed).toBe('-registrar')
    })

    test('method order_by_transformed returns correct values - expires_at', async() => {
        wrapper = initWrapper(RelatedDomainList, {
            propsData: { object_handle: 'HANDLE', object_id, label: 'Domains' },
            data() {
                return {
                    sort_by: `domain.${RELATED_DOMAIN_TABLE_COLUMNS.EXPIRES_AT}`,
                }
            },
            mocks: { $notify: jest.fn() },
        })
        expect(wrapper.vm.order_by_transformed).toBe('+exdate')

        await wrapper.setData({ sort_desc: true })
        expect(wrapper.vm.order_by_transformed).toBe('-exdate')
    })

    test('method order_by_transformed returns correct values - default value', async() => {
        wrapper = initWrapper(RelatedDomainList, {
            propsData: { object_handle: 'HANDLE', object_id, label: 'Domains' },
            data() {
                return {
                    sort_by: `domain.something`,
                }
            },
            mocks: { $notify: jest.fn() },
        })
        expect(wrapper.vm.order_by_transformed).toBe('+fqdn')

        await wrapper.setData({ sort_desc: true })
        expect(wrapper.vm.order_by_transformed).toBe('-fqdn')
    })

    test('method toggle_box behaves correctly', async() => {
        const goTo = jest.fn()
        goTo.init = jest.fn()
        goTo.framework = {}
        const vuetify = new Vuetify()
        vuetify.framework.goTo = goTo

        wrapper = initWrapper(RelatedDomainList, {
            vuetify,
            propsData: { object_handle: 'HANDLE', object_id, label: 'Domains' },
            data() {
                return {
                    roles: [],
                }
            },
            mocks: { $notify: jest.fn() },
        })

        wrapper.vm.get_related_domains = jest.fn()

        expect(wrapper.vm.is_opened).toStrictEqual(false)
        await wrapper.vm.toggle_box()

        expect(wrapper.vm.is_opened).toStrictEqual(true)
        expect(wrapper.vm.roles).toStrictEqual([CONTACT_ROLES.HOLDER])
        expect(wrapper.vm.$vuetify.goTo).toHaveBeenCalled()
    })

    test('method change_role works correctly', async() => {
        wrapper.vm.get_related_domains = jest.fn()
        await wrapper.vm.change_role([])

        expect(wrapper.vm.roles).toStrictEqual([])
        expect(wrapper.vm.related_domains).toStrictEqual([])
        expect(wrapper.vm.get_related_domains).not.toHaveBeenCalled()

        await wrapper.vm.change_role(CONTACT_ROLES.HOLDER)
        expect(wrapper.vm.get_related_domains).toHaveBeenCalled()
    })

    test('method update_sort_by works correctly', async() => {
        const sort_by = `domain.${RELATED_DOMAIN_TABLE_COLUMNS.SPONSORING_REGISTRAR}`
        wrapper.vm.get_related_domains = jest.fn()
        await wrapper.vm.update_sort_by(sort_by)

        expect(wrapper.vm.sort_by).toBe(sort_by)
        expect(wrapper.vm.get_related_domains).toHaveBeenCalled()
    })

    test('method update_sort_desc works correctly', async() => {
        const sort_desc = true
        wrapper.vm.get_related_domains = jest.fn()
        await wrapper.vm.update_sort_desc(sort_desc)

        expect(wrapper.vm.sort_desc).toBe(sort_desc)
        expect(wrapper.vm.get_related_domains).toHaveBeenCalled()
    })

    test('method change_page_size works correctly', async() => {
        const page_size = 5
        wrapper.vm.get_related_domains = jest.fn()
        await wrapper.vm.change_page_size(page_size)

        expect(wrapper.vm.page_size).toBe(page_size)
        expect(wrapper.vm.current_page).toBe(1)
        expect(wrapper.vm.get_related_domains).toHaveBeenCalled()
    })
})

describe('RelatedDomainList - CSV export', () => {
    let wrapper
    let axios_mock
    let blob_mock
    const initial_url = { ...URL }

    beforeEach(() => {
        axios_mock = new MockAdapter(axios)
        axios_mock.onGet(URLS.domains_by_contact_ajax(object_id)).reply(200, CONTACT_RELATED_DOMAINS_RESPONSE)

        global.URL.createObjectURL = jest.fn()
        global.URL.revokeObjectURL = jest.fn()

        blob_mock = jest.fn()
        jest.spyOn(global, 'Blob').mockImplementationOnce((...args) => blob_mock(...args))

        wrapper = initWrapper(RelatedDomainList, {
            propsData: { object_handle: 'HANDLE', object_id, label: 'Domains' },
            mocks: { $notify: jest.fn() },
        })
    })

    afterEach(() => {
        global.URL = initial_url
        jest.restoreAllMocks()
    })

    test('method export_table_csv works correctly', async() => {
        await wrapper.vm.export_table_csv()

        expect(blob_mock).toHaveBeenCalledWith([
            'fqdn;sponsoring_registrar;contact;roles;nsset;keyset;expires_at;outzone_at;delete_at\n',
            'test.org;REG-TEST_A;HANDLE;holder;null;null;2025-01-01;null;null\n',
        ], { type: 'text/csv;charset=utf-8' })
        expect(URL.revokeObjectURL).toHaveBeenCalled()
    })

    test('method export_table_csv fails by server error', async() => {
        console.log = jest.fn()
        axios_mock.onGet(URLS.domains_by_contact_ajax(object_id)).reply(500)
        await wrapper.vm.export_table_csv()

        expect(URL.createObjectURL).not.toHaveBeenCalled()
        expect(URL.revokeObjectURL).not.toHaveBeenCalled()
        expect(wrapper.vm.$notify).toHaveBeenCalledWith(wrapper.vm.ERROR_SOMETHING_WENT_WRONG)
        expect(console.log).toHaveBeenCalledWith('error: notifications:something_went_wrong')
    })
})
