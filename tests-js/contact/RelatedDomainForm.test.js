import { initWrapper } from 'ferda-test/TestUtils'
import { CONTACT_ROLES } from 'ferda/RegistryMixin'
import RelatedDomainForm from 'ferda/contact/RelatedDomainForm.vue'

describe('RelatedDomainForm', () => {
    let wrapper

    beforeEach(() => {
        wrapper = initWrapper(RelatedDomainForm, { roles: [CONTACT_ROLES.HOLDER] })
    })

    test('matches snapshot', () => {
        expect(wrapper.element).toMatchSnapshot()
    })

    test('empty matches snapshot', () => {
        wrapper = initWrapper(RelatedDomainForm)
        expect(wrapper.element).toMatchSnapshot()
    })

    test('property is_indeterminate has correct values', async() => {
        await wrapper.setProps({ roles: [] })
        expect(wrapper.vm.is_indeterminate).toStrictEqual(false)

        await wrapper.setProps({ roles: [CONTACT_ROLES.HOLDER] })
        expect(wrapper.vm.is_indeterminate).toStrictEqual(true)
    })

    test('property is_all_checked has correct values', async() => {
        await wrapper.setProps({ roles: [] })
        expect(wrapper.vm.is_all_checked).toStrictEqual(false)

        await wrapper.setProps({ roles: Object.values(CONTACT_ROLES) })
        expect(wrapper.vm.is_all_checked).toStrictEqual(true)
    })

    test('method change_all emits correctly', () => {
        wrapper.vm.change_all(true)
        wrapper.vm.change_all(false)

        expect(wrapper.emitted('change-role')[0][0]).toStrictEqual(Object.values(CONTACT_ROLES))
        expect(wrapper.emitted('change-role')[1][0]).toStrictEqual([])
    })
})
