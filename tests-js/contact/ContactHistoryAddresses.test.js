import { initWrapper } from 'ferda-test/TestUtils'
import ContactHistoryAddresses from 'ferda/contact/ContactHistoryAddresses.vue'

describe('ContactHistoryAddresses', () => {
    let wrapper

    beforeEach(() => {
        wrapper = initWrapper(ContactHistoryAddresses, {
            propsData: {
                info_diff: {
                    place: {old_value: null, new_value: null, differs: false},
                    mailing_address: {old_value: null, new_value: null, differs: false},
                    billing_address: {old_value: null, new_value: null, differs: false},
                    shipping_address1: {old_value: null, new_value: null, differs: false},
                    shipping_address2: {old_value: null, new_value: null, differs: false},
                    shipping_address3: {old_value: null, new_value: null, differs: false},
                    publish: {
                        place: {differs: true},
                    },
                },
            },
            stubs: {
                FontAwesomeIcon: true,
            },
        })
    })

    test('matches snapshot', () => {
        expect(wrapper.element).toMatchSnapshot()
    })

    test('method address_differs', () => {
        expect(wrapper.vm.address_differs('place')).toBe(true)
        expect(wrapper.vm.address_differs('mailing_address')).toBe(false)
    })
})
