import axios from 'axios'
import MockAdapter from 'axios-mock-adapter'

import { initWrapper } from 'ferda-test/TestUtils'
import { URLS } from 'ferda/Urls'
import ContactRepresentativeAddForm from 'ferda/contact/ContactRepresentativeAddForm.vue'

describe('ContactRepresentativeAddForm', () => {
    let axios_mock
    let wrapper

    beforeEach(async() => {
        axios_mock = new MockAdapter(axios)
        axios_mock.onGet(URLS.countries_list()).reply(200, { 'US': 'United States of America' })
        wrapper = initWrapper(ContactRepresentativeAddForm, {
            propsData: {
                address: {},
                dialog_opened: true,
                is_loading: false,
            },
            data() {
                return {
                    company: 'Monsters, Inc.',
                    street1: 'Monster lane 22',
                    street2: '',
                    street3: '',
                    postal_code: '0022',
                    city: 'New York',
                    state_or_province: '',
                    country: 'US',
                    name: 'Test name',
                    telephone: '+123.123456789',
                    email: 'test@test.test',
                }
            },
            mocks: {
                '$cookies': {
                    get: () => jest.fn(),
                },
            },
        })
        wrapper.vm.$refs.form.validate = () => jest.fn()
        wrapper.vm.$refs.form.resetValidation = () => jest.fn()
    })

    test('matches snapshot', async() => {
        expect(wrapper.element).toMatchSnapshot()
    })

    test('method submit_dialog works correctly - is valid', async() => {
        wrapper.vm.$refs = {
            form: { validate: jest.fn(() => true) },
        }
        await wrapper.vm.submit_dialog()

        expect(wrapper.emitted('submit-dialog')[0][0]).toStrictEqual({
            organization: 'Monsters, Inc.',
            street1: 'Monster lane 22',
            street2: '',
            street3: '',
            postal_code: '0022',
            city: 'New York',
            state_or_province: '',
            country_code: 'US',
            name: 'Test name',
            telephone: '+123.123456789',
            email: 'test@test.test',
        })
    })

    test('method submit_dialog works correctly - is not valid', async() => {
        wrapper.vm.$refs = {
            form: { validate: jest.fn(() => false) },
        }
        await wrapper.vm.submit_dialog()
        expect(wrapper.emitted('submit-dialog')).toBeFalsy()
    })

    test('form is cleared after new representative is added', async() => {
        wrapper.vm.reset_form = jest.fn()
        wrapper.vm.$refs.form.resetValidation = jest.fn()
        await wrapper.setProps({ address: {} })

        expect(wrapper.vm.reset_form).toHaveBeenCalled()
        expect(wrapper.vm.$refs.form.resetValidation).toHaveBeenCalled()
    })

    test('method reset_form works correctly', () => {
        wrapper.vm.reset_form()
        expect(wrapper.vm.company).toStrictEqual('')
    })
})
