import { initWrapper } from 'ferda-test/TestUtils'
import ContactRepresentativeDeleteForm from 'ferda/contact/ContactRepresentativeDeleteForm.vue'

describe('ContactRepresentativeDeleteForm', () => {
    let wrapper

    beforeEach(() => {
        wrapper = initWrapper(ContactRepresentativeDeleteForm)
    })

    test('matches snapshot', () => {
        expect(wrapper.element).toMatchSnapshot()
    })
})
