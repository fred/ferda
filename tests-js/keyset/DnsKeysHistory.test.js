import { KEYSET } from 'ferda-test/TestObjects'
import { initWrapper } from 'ferda-test/TestUtils'
import DnsKeysHistory from 'ferda/keyset/DnsKeysHistory.vue'

describe('DnsKeysHistory', () => {
    let wrapper

    beforeEach(() => {
        const dns_keys = KEYSET.dns_keys.map(dns_key => {
            return {
                old_value: dns_key,
                new_value: dns_key,
            }
        })

        wrapper = initWrapper(DnsKeysHistory, {
            propsData: {
                dns_keys: dns_keys,
            },
        })
    })

    test('showing short dns keys and matches snapshot', () => {
        KEYSET.dns_keys.forEach(dns_key => {
            expect(wrapper.html().includes(`${dns_key.key.substring(0, 10)}…`)).toBe(true)
        })

        expect(wrapper.element).toMatchSnapshot()
    })

    test('after click show full dns key and matches snapshot', done => {
        // find show more buttons and click on them
        wrapper.findAll('a').trigger('click')

        // XXX: Workaround for @vue/test-utils==1.0.0-beta.30
        // See https://github.com/vuejs/vue-test-utils/issues/1363
        wrapper.vm.$nextTick(() => {
            // check if contains keys
            KEYSET.dns_keys.forEach(dns_key => {
                expect(wrapper.html().includes(dns_key.key)).toBe(true)
            })

            // check snapshot after clicking on show more buttons
            expect(wrapper.element).toMatchSnapshot()
            done()
        })

    })
})
