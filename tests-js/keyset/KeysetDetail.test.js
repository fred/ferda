import { KEYSET } from 'ferda-test/TestObjects'
import { initWrapper } from 'ferda-test/TestUtils'
import KeysetDetail from 'ferda/keyset/KeysetDetail.vue'

describe('KeysetDetail', () => {
    let wrapper

    beforeEach(() => {
        wrapper = initWrapper(KeysetDetail, {
            propsData: {
                uuid: '00000000-0000-0000-0000-000000000000',
                history_id: '00000000-0000-0000-0000-000000000001',
                keyset: KEYSET,
            },
        })
    })

    test('matches snapshot', () => {
        expect(wrapper.element).toMatchSnapshot()
    })
})
