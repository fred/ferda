import { KEYSET } from 'ferda-test/TestObjects'
import { initWrapper } from 'ferda-test/TestUtils'
import DnsKeyInfoBox from 'ferda/keyset/DnsKeyInfoBox.vue'

describe('DnsKeyInfoBox', () => {
    let wrapper

    beforeEach(() => {
        wrapper = initWrapper(DnsKeyInfoBox, {
            propsData: {
                dns_key: KEYSET.dns_keys[0],
            },
        })
    })

    test('matches snapshot', () => {
        expect(wrapper.element).toMatchSnapshot()
    })
})
