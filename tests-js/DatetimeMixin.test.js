import { DATE_RANGE, DATE_TYPE, DatetimeMixin, SEARCH_HISTORY_PERIOD } from 'ferda/DatetimeMixin'

import { getClonedStoreOptions, initWrapper } from './TestUtils'

describe('DatetimeMixin', () => {
    let wrapper
    let Component
    let store

    beforeEach(() => {
        jest.useFakeTimers()
        jest.setSystemTime(new Date('2020-01-01T12:00:00+00:00'))

        Component = {
            render() {},
            mixins: [DatetimeMixin],
        }

        store = getClonedStoreOptions()
        wrapper = initWrapper(Component, { customStoreOptions: store })
    })

    afterEach(() => {
        jest.runOnlyPendingTimers()
        jest.useRealTimers()
    })

    test('iso_to_locale en', () => {
        store.modules.preferences.getters.selected_language = () => 'en'
        wrapper = initWrapper(Component, {customStoreOptions: store})
        expect(wrapper.vm.iso_to_locale('2019-01-04T12:00:05Z')).toBe('Jan 4, 2019 12:00:05 PM')
    })

    test('iso_to_locale cs', () => {
        store.modules.preferences.getters.selected_language = () => 'cs'
        wrapper = initWrapper(Component, {customStoreOptions: store})
        expect(wrapper.vm.iso_to_locale('2019-01-04T12:00:05Z')).toBe('4. led 2019 12:00:05')
    })

    test('time zone format is correct', () => {
        expect(wrapper.vm.get_time_zone_abbr()).toBe('UTC')
        expect(wrapper.vm.get_time_zone_offset()).toBe('+00:00')
        expect(wrapper.vm.iso_to_locale('2021-08-11T22:00:00+00:00', true)).toBe('Aug 11, 2021 10:00:00 PM UTC')
        expect(wrapper.vm.iso_to_locale('2021-08-11T22:00:00')).toBe('Aug 11, 2021 10:00:00 PM')
        expect(wrapper.vm.time_to_locale('22:00:00+00', true)).toBe('10:00:00 PM UTC')
        expect(wrapper.vm.time_to_locale('22:00')).toBe('10:00:00 PM')
    })

    test('history_search_period - last month', () => {
        expect(wrapper.vm.history_search_period(SEARCH_HISTORY_PERIOD.LAST_MONTH)).toBe('2019-12-01T12:00:00Z')
    })

    test('history_search_period - last year', () => {
        expect(wrapper.vm.history_search_period(SEARCH_HISTORY_PERIOD.LAST_YEAR)).toBe('2019-01-01T12:00:00Z')
    })

    test('history_search_period - last 5 years', () => {
        expect(wrapper.vm.history_search_period(SEARCH_HISTORY_PERIOD.LAST_5_YEARS)).toBe('2015-01-01T12:00:00Z')
    })

    test('history_search_period - full history', () => {
        expect(wrapper.vm.history_search_period(SEARCH_HISTORY_PERIOD.FULL_HISTORY)).toBe(undefined)
    })

    test('history_search_period - bad input', () => {
        expect(wrapper.vm.history_search_period('some_bad_input')).toBe(undefined)
    })

    test('date_range - full range', () => {
        expect(wrapper.vm.date_range('2021-01-01', '2021-02-01')).toBe('Jan 1, 2021 - Feb 1, 2021')
        expect(wrapper.vm.date_range('2021-01-01')).toBe('Jan 1, 2021 - now')
    })

    test('picker returns full date', () => {
        expect(wrapper.vm.get_iso_date('2021-01')).toBe('2021-01-01')
        expect(wrapper.vm.get_iso_date('2021-01-01')).toBe('2021-01-01')
    })

    test('method is_valid_time returns correct value', () => {
        expect(wrapper.vm.is_valid_time('12:00:00')).toStrictEqual(true)
        expect(wrapper.vm.is_valid_time('12:00:00+01:00')).toStrictEqual(false)
        expect(wrapper.vm.is_valid_time('some invalid value')).toStrictEqual(false)
    })

    test('method is_less_than returns correct value', () => {
        expect(wrapper.vm.is_less_than('2021-01-01T12:00:00+01:00', '2022-01-01T12:00:00+01:00')).toStrictEqual(true)
        expect(wrapper.vm.is_less_than('2022-01-01T12:00:00+01:00', '2021-01-01T12:00:00+01:00')).toStrictEqual(false)
    })

    test('method is_valid_iso returns correct values', () => {
        expect(wrapper.vm.is_valid_iso('2021-01-01T12:00:00')).toStrictEqual(true)
        expect(wrapper.vm.is_valid_iso('2021-01-01T12:00:00+02:00')).toStrictEqual(true)
        expect(wrapper.vm.is_valid_iso('2021-01-01')).toStrictEqual(true)
        expect(wrapper.vm.is_valid_iso('02:15:15')).toStrictEqual(false)
        expect(wrapper.vm.is_valid_iso('some_invalid_input')).toStrictEqual(false)
    })

    test('method validate_datetime_input returns correct results', () => {
        expect(wrapper.vm.validate_datetime_input('', '')).toStrictEqual(null)
        expect(wrapper.vm.validate_datetime_input('2021-01-01', '2021-01-01')).toStrictEqual(false)
        expect(wrapper.vm.validate_datetime_input('invalid_input', '2021-01-01'))
            .toStrictEqual(['common:iso_format_expected'])
        expect(wrapper.vm.validate_datetime_input('2021-01-01', '2022-01-01')).toBe('2021-01-01')
    })

    test('method get_default_range returns correct value', () => {
        expect(wrapper.vm.get_default_range(604800)).toStrictEqual('2020-01-08T12:00:00+00:00')
    })

    test('method is_valid_range returns correct value', () => {
        expect(wrapper.vm.is_valid_range('2020-01-05T12:00:00+00:00', '2020-01-13T12:00:00+00:00')).toBe(true)
        expect(wrapper.vm.is_valid_range('2020-01-05T12:00:00+00:00', '2020-02-13T12:00:00+00:00')).toBe(false)
    })

    test('method is_valid_future_date returns correct value', () => {
        expect(wrapper.vm.is_valid_future_date('2019-01-01T12:00:00+00:00'))
            .toStrictEqual(false)
        expect(wrapper.vm.is_valid_future_date('2020-01-21T12:00:00+00:00')).toStrictEqual(true)
    })

    test('method get_current_date_range returns correct values', () => {
        expect(wrapper.vm.get_current_date_range(DATE_TYPE.HOUR))
            .toStrictEqual({created_from: '2020-01-01T12:00:00+00:00', created_to: '2020-01-01T13:00:00+00:00'})
        expect(wrapper.vm.get_current_date_range(DATE_TYPE.DAY))
            .toStrictEqual({created_from: '2020-01-01T00:00:00+00:00', created_to: '2020-01-02T00:00:00+00:00'})
        expect(wrapper.vm.get_current_date_range(DATE_TYPE.MONTH))
            .toStrictEqual({created_from: '2020-01-01T00:00:00+00:00', created_to: '2020-02-01T00:00:00+00:00'})
    })

    test('method get_past_date_range returns correct values', () => {
        expect(wrapper.vm.get_past_date_range(DATE_TYPE.HOUR))
            .toStrictEqual({created_from: '2020-01-01T11:00:00+00:00', created_to: '2020-01-01T12:00:00+00:00'})
        expect(wrapper.vm.get_past_date_range(DATE_TYPE.DAY))
            .toStrictEqual({created_from: '2019-12-31T00:00:00+00:00', created_to: '2020-01-01T00:00:00+00:00'})
        expect(wrapper.vm.get_past_date_range(DATE_TYPE.MONTH))
            .toStrictEqual({created_from: '2019-12-01T00:00:00+00:00', created_to: '2020-01-01T00:00:00+00:00'})
    })

    test('method get_date_range works correctly', () => {
        expect(wrapper.vm.get_date_range(DATE_RANGE.CURRENT_HOUR))
            .toStrictEqual({created_from: '2020-01-01T12:00:00+00:00', created_to: '2020-01-01T13:00:00+00:00'})
        expect(wrapper.vm.get_date_range(DATE_RANGE.PAST_HOUR))
            .toStrictEqual({created_from: '2020-01-01T11:00:00+00:00', created_to: '2020-01-01T12:00:00+00:00'})
        expect(wrapper.vm.get_date_range(DATE_RANGE.CURRENT_DAY))
            .toStrictEqual({created_from: '2020-01-01T00:00:00+00:00', created_to: '2020-01-02T00:00:00+00:00'})
        expect(wrapper.vm.get_date_range(DATE_RANGE.PAST_DAY))
            .toStrictEqual({created_from: '2019-12-31T00:00:00+00:00', created_to: '2020-01-01T00:00:00+00:00'})
        expect(wrapper.vm.get_date_range(DATE_RANGE.PAST_MONTH))
            .toStrictEqual({created_from: '2019-12-01T00:00:00+00:00', created_to: '2020-01-01T00:00:00+00:00'})
    })
})

describe('NaiveLocalConversion', () => {
    let wrapper
    let Component
    const store = getClonedStoreOptions()

    beforeEach(() => {
        jest.useFakeTimers()
        jest.setSystemTime(new Date('2020-01-01T12:00:00+01:00'))
        store.modules.settings.getters.TIME_ZONE = () => 'Europe/Prague'

        Component = { render() {}, mixins: [DatetimeMixin] }
        wrapper = initWrapper(Component, { customStoreOptions: store })
    })

    afterEach(() => {
        jest.runOnlyPendingTimers()
        jest.useRealTimers()
    })

    test('method naive_local_to_naive_utc returns correct value', () => {
        expect(wrapper.vm.naive_local_to_naive_utc('2022-01-05T12:00:00')).toBe('2022-01-05T11:00:00')
    })

    test('method naive_utc_to_aware_local returns correct value', () => {
        expect(wrapper.vm.naive_utc_to_aware_local('2022-01-05T11:00:00')).toBe('2022-01-05T12:00:00+01:00')
    })

    test('method naive_local_time_to_naive_utc_time returns correct value', () => {
        expect(wrapper.vm.naive_local_time_to_naive_utc_time('12:00:00')).toBe('11:00:00')
    })

    test('method naive_utc_time_to_aware_local_time returns correct value', () => {
        expect(wrapper.vm.naive_utc_time_to_aware_local_time('11:00:00', false)).toBe('12:00:00')
        expect(wrapper.vm.naive_utc_time_to_aware_local_time('11:00:00', true)).toBe('12:00:00+01:00')
    })

    test('method create_next_day_midnight returns correct value', () => {
        expect(wrapper.vm.create_next_day_midnight('2021-01-01T12:24:23+01:00', 1)).toBe('2021-01-02T00:00:00+00:00')
        expect(wrapper.vm.create_next_day_midnight('2021-01-01', 0)).toBe('2021-01-01T00:00:00+00:00')
    })

    test('method get_locale_moment returns correct values', () => {
        expect(wrapper.vm.get_locale_moment('2021-01-01').format('ll')).toBe('Jan 1, 2021')
        expect(wrapper.vm.get_locale_moment('2021-01-01T12:00:00').format('ll')).toBe('Jan 1, 2021')
        expect(wrapper.vm.get_locale_moment('2021-01-01T12:00:00+00:00').format('ll')).toBe('Jan 1, 2021')
    })

    test('method get_current_delete_date returns correct results', () => {
        expect(wrapper.vm.get_current_delete_date(
            { events: { unregistered: { timestamp: '2021-01-01T12:00:00+00:00' } } }),
        ).toBe('Jan 1, 2021')

        expect(wrapper.vm.get_current_delete_date(
            { delete_candidate_at: '2022-01-01T12:00:00+00:00', events: { unregistered: null } }),
        ).toBe('Jan 1, 2022')

        expect(wrapper.vm.get_current_delete_date(
            { delete_candidate_scheduled_at: '2023-01-01T12:00:00+00:00', events: { unregistered: null } }),
        ).toBe('Jan 1, 2023')
    })
})
