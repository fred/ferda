import axios from 'axios'
import MockAdapter from 'axios-mock-adapter'

import { MESSAGE_REQUEST } from 'ferda-test/TestObjects'
import { MESSAGE_TYPE, MessagesMixin } from 'ferda/MessagesMixin'
import { URLS } from 'ferda/Urls'

import { initWrapper } from './TestUtils'

describe('MessagesMixin', () => {
    let wrapper
    let axios_mock
    const initial_console = console.log

    beforeEach(() => {
        axios_mock = new MockAdapter(axios)
        const Component = { render() {}, mixins: [MessagesMixin] }
        wrapper = initWrapper(Component, { mocks: { $notify: jest.fn() }})
    })

    afterEach(() => {
        console.log = initial_console
    })

    test('method get_message_subtypes fetches subtypes correctly', async() => {
        axios_mock.onGet(URLS.message_subtypes(MESSAGE_TYPE.EMAIL)).reply(200, ['subtype_1', 'subtype_2'])
        const message_types = await wrapper.vm.get_message_subtypes(MESSAGE_TYPE.EMAIL)
        expect(message_types).toStrictEqual(['subtype_1', 'subtype_2'])
    })

    test('method get_message_subtypes fails by server error', async() => {
        console.log = jest.fn()
        axios_mock.onGet(URLS.message_subtypes(MESSAGE_TYPE.EMAIL)).reply(500)
        const message_types = await wrapper.vm.get_message_subtypes(MESSAGE_TYPE.EMAIL)
        expect(message_types).toStrictEqual(null)
        expect(console.log).toHaveBeenCalledWith('error: notifications:something_went_wrong')
        expect(wrapper.vm.$notify).toHaveBeenCalledWith(wrapper.vm.ERROR_SOMETHING_WENT_WRONG)
    })

    test('method get_message_subtypes is aborted', async() => {
        console.log = jest.fn()
        wrapper.vm.is_http_cancel = jest.fn(() => true)
        axios_mock.onGet(URLS.message_subtypes(MESSAGE_TYPE.EMAIL)).reply(500, { __CANCEL__: true })
        const message_types = await wrapper.vm.get_message_subtypes(MESSAGE_TYPE.EMAIL)
        expect(message_types).toStrictEqual(null)
        expect(console.log).toHaveBeenCalledWith('info: notifications:search_aborted')
        expect(wrapper.vm.$notify).toHaveBeenCalledWith(wrapper.vm.INFO_REQUEST_ABORTED)
    })

    test('method get_message_list returns correct results', async() => {
        axios_mock.onGet(URLS.message_list(MESSAGE_TYPE.EMAIL)).reply(200, MESSAGE_REQUEST)
        const message_params = {
            message_type: 'email',
            time_begin_from: '2021-01-01T12:00:00+01:00',
            time_begin_to: '2022-01-01T12:00:00+01:00',
            recipients: ['recipient@example.com'],
            selected_subtypes: ['subtype_1', 'subtype_2'],
        }
        const abort_controller = new window.AbortController()
        const result = await wrapper.vm.get_message_list(message_params, 10, abort_controller, null)
        expect(result).toStrictEqual(MESSAGE_REQUEST)
    })

    test('method get_message_list fails by server error', async() => {
        console.log = jest.fn()
        axios_mock.onGet(URLS.message_list(MESSAGE_TYPE.EMAIL)).reply(500)
        const message_params = {
            time_begin_from: null,
            time_begin_to: null,
            recipients: ['recipient@example.com'],
            selected_subtypes: ['subtype_1', 'subtype_2'],
        }
        await wrapper.vm.get_message_list(message_params)

        expect(wrapper.vm.$notify).toHaveBeenCalledWith(wrapper.vm.ERROR_SOMETHING_WENT_WRONG)
        expect(console.log).toHaveBeenCalledWith('error: notifications:something_went_wrong')
    })

    test('method get_message_list is aborted', async() => {
        console.log = jest.fn()
        wrapper.vm.is_http_cancel = jest.fn(() => true)
        axios_mock.onGet(URLS.message_list(MESSAGE_TYPE.EMAIL)).reply(500, { __CANCEL__: true })
        const message_types = await wrapper.vm.get_message_list()
        expect(message_types).toStrictEqual(null)
        expect(console.log).toHaveBeenCalledWith('info: notifications:search_aborted')
        expect(wrapper.vm.$notify).toHaveBeenCalledWith(wrapper.vm.INFO_REQUEST_ABORTED)
    })

    test('method get_message_list_url_params returns correct values', async() => {
        const message_params = {
            time_begin_from: '2021-01-01T12:00:00+01:00',
            time_begin_to: '2022-01-01T12:00:00+01:00',
            recipients: ['recipient@example.com'],
            references: 'contact.00000000-0000-0000-0000-000000000001',
            selected_subtypes: ['subtype_1', 'subtype_2'],
        }
        const result_without_url = wrapper.vm.get_message_list_url_params(message_params, null)
        expect(result_without_url.get('time_begin_from')).toStrictEqual('2021-01-01T12:00:00+01:00')
        expect(result_without_url.get('time_begin_to')).toStrictEqual('2022-01-01T12:00:00+01:00')
        expect(result_without_url.getAll('recipients')).toEqual(['recipient@example.com'])
        expect(result_without_url.get('references')).toEqual('contact.00000000-0000-0000-0000-000000000001')
        expect(result_without_url.getAll('types')).toEqual(['subtype_1', 'subtype_2'])

        const result_with_url = wrapper.vm.get_message_list_url_params(null, 10, 'http://example.com/')
        expect(result_with_url.get('page_size')).toBe('10')
    })

    test('method get_recipient_caption returns correct value', () => {
        expect(wrapper.vm.get_recipient_caption(MESSAGE_TYPE.LETTER, { name: 'rec_name', organization: 'rec_org' }))
            .toBe('rec_org, rec_name')
        expect(wrapper.vm.get_recipient_caption(MESSAGE_TYPE.EMAIL, 'recipient@example.com'))
            .toBe('recipient@example.com')
    })

    test('method filter_subtypes returns correct value', () => {
        expect(wrapper.vm.filter_subtypes({ text: 'Test label', value: 'test_name'}, 'test')).toStrictEqual(true)
        expect(wrapper.vm.filter_subtypes({ text: 'Test label', value: 'test_name'}, 'ha')).toStrictEqual(false)
    })

    test('method get_matching_label return correct value', () => {
        const subtype_labels = [
            { label: 'Test label', name: 'test_name', tags: ['test']},
            { label: 'Test label 2', name: 'test_name_2', tags: ['test']},
        ]
        expect(wrapper.vm.get_matching_label('test_name_2', subtype_labels)).toStrictEqual('Test label 2')
        expect(wrapper.vm.get_matching_label(null, subtype_labels)).toStrictEqual(null)
        expect(wrapper.vm.get_matching_label('test_name_2', [])).toStrictEqual(null)
    })

    test('method highlight_subtypes works correctly', () => {
        expect(wrapper.vm.highlight_subtypes('some_text', null)).toBe('some_text')
        expect(wrapper.vm.highlight_subtypes('some_text', 'another_value')).toBe('some_text')
        expect(wrapper.vm.highlight_subtypes('some_text_value', 'text'))
            .toBe('some_<span class="v-list-item__mask">text</span>_value')
    })
})
