export const CONTACT_OBIWAN = {
    contact_id: 'UUID-OBIWAN',
    contact_handle: 'OBIWAN',
    organization: 'Jedi Order',
    name: 'Obiwan Kenobi',
    publish: {organization: true, name: true},
    events: {
        registered: {registrar_handle: 'REG-SW', timestamp: '2000-01-01T10:00:00Z'},
        transferred: {registrar_handle: 'REG-SW', timestamp: '2000-01-01T10:00:00Z'},
        updated: {registrar_handle: 'REG-CZNIC', timestamp: '2001-01-01T10:00:00Z'},
        unregistered: null,
    },
}

export const CONTACT_YODA = {
    contact_id: 'UUID-YODA',
    contact_handle: 'YODA',
    organization: 'Jedi Order',
    name: 'Yoda',
    publish: {organization: true, name: true},
    events: {
        registered: {registrar_handle: 'REG-SW', timestamp: '2000-01-01T10:00:00Z'},
        transferred: {registrar_handle: 'REG-SW', timestamp: '2000-01-01T10:00:00Z'},
        updated: {registrar_handle: 'REG-SW', timestamp: '2001-01-01T10:00:00Z'},
        unregistered: null,
    },
}

export const CONTACT_HARRY = {
    contact_id: 'UUID-HARRY',
    contact_handle: 'HARRY',
    organization: 'Gryffindor',
    name: 'Harry',
    telephone: '123456789',
    email: 'harry.potter@example.com',
    publish: {organization: true, name: true},
    events: {
        registered: {registrar_handle: 'REG-SW', timestamp: '2000-01-01T10:00:00Z'},
        transferred: {registrar_handle: 'REG-SW', timestamp: '2000-01-01T10:00:00Z'},
        updated: {registrar_handle: 'REG-SW', timestamp: '2001-01-01T10:00:00Z'},
        unregistered: {
            timestamp: null,
        },
    },
}

export const SEARCH_CONTACT = Object.freeze({
    contact_id: 'b6271c9d-4ad2-4118-b60c-358769d5a66b',
    contact_handle: 'TEST-HANDLE',
    contact_history_id: '14ebdb86-3774-5e76-b081-c3e8e0e23bcb',
    sponsoring_registrar: 'REG-TEST',
    name: 'Some name',
    organization: '',
    telephone: '+420.123456789',
    fax: '',
    emails: ['test@test.org'],
    notify_emails: ['notify@email.org'],
    vat_identification_number: '',
    additional_identifier: { type: 'birthdate', value: '2012-04-05' },
    mailing_address: null,
    billing_address: null,
    shipping_address1: null,
    shipping_address2: null,
    shipping_address3: null,
    events: {
        registered: { registrar_handle: 'REG-TEST', timestamp: '2020-01-01T12:00:00+00:00' },
        transferred: { registrar_handle: 'REG-TEST', timestamp: null },
        updated: { registrar_handle: 'REG-TEST', timestamp: null },
        unregistered: null,
    },
    place: {
        city: 'Test city',
        company: null,
        country_code: 'CZ',
        postal_code: '05855',
        state_or_province: '',
        street: ['test1', 'test2', 'test3'],
    },
})

export const SEARCH_DOMAIN = Object.freeze({
    domain_id: 'c6271c9d-4ad2-4118-b60c-358769d5a64b',
    fqdn: 'test.cz',
    domain_history_id: 'b6271c9d-4ad2-4218-b60c-358769d5a66c',
    registrant: {
        id: 'a6271a9d-4cd2-4118-c60c-358769d5a66b',
        handle: 'TEST-HANDLE',
        name: 'Registrant name',
        organization: '',
    },
    sponsoring_registrar: 'REG-HARRY',
    nsset: { id: 'b6271c9d-4ad2-4118-b10d-358769d5a66c', handle: 'TEST-NSSET' },
    keyset: null,
    administrative_contacts: [],
    events: {
        registered: { registrar_handle: 'REG-HARRY', timestamp: '2020-01-01T12:00:00+00:00' },
        transferred: { registrar_handle: 'REG-HARRY', timestamp: null },
        updated: { registrar_handle: 'REG-HARRY', timestamp: null },
        unregistered: null,
    },
    expires_at: '2023-07-16T22:00:00+00:00',
    outzone_at: null,
    delete_candidate_at: null,
    expiration_warning_scheduled_at: '2023-06-16T22:00:00+00:00',
    outzone_unguarded_warning_scheduled_at: '2023-08-10T22:00:00+00:00',
    outzone_scheduled_at: '2023-08-15T22:00:00+00:00',
    delete_warning_scheduled_at: '2023-09-10T22:00:00+00:00',
    delete_candidate_scheduled_at: '2023-09-15T22:00:00+00:00',
    validation_expires_at: null,
})

export const SEARCH_NSSET = Object.freeze({
    nsset_id: 'c6271c9d-4ad2-4111-c60b-358769d5a66b',
    nsset_handle: 'TEST-NSSET',
    nsset_history_id: '12ebdb86-3774-5e76-b081-c3e8e0e23ebc',
    sponsoring_registrar: 'REG-TEST',
    dns_hosts: [
        { fqdn: 'test.nsset.org', ip_addresses: ['127.0.0.1'] },
        { fqdn: 'testing.nsset.org', ip_addresses: ['127.0.0.1'] },
    ],
    technical_contacts: [
        {
            id: 'df14218a-523f-492c-96b9-3439697b4353',
            handle: 'HANDLE',
            name: 'Some name',
            organization: 'Some org l.t.d.',
        },
    ],
    technical_check_level: 3,
    events: {
        registered: { timestamp: '2000-01-01T10:00:00Z', registrar_handle: 'REG-TEST' },
        transferred: { timestamp: '2000-01-01T10:00:00Z', registrar_handle: 'REG-TEST' },
        updated: { timestamp: '2000-01-01T10:00:00Z', registrar_handle: 'REG-TEST' },
        unregistered: { timestamp: null },

    },
})

export const SEARCH_KEYSET = Object.freeze({
    keyset_id: 'c6271c9d-4ad2-4222-c60b-358769d5a66c',
    keyset_handle: 'TEST-KEYSET',
    keyset_history_id: 'c6271c9d-4bc3-4111-c60b-358769d5a66b',
    sponsoring_registrar: 'REG-TEST',
    dns_keys: [{ flags: 257, protocol: 3, alg: 10, key: 'xxx' }],
    technical_contacts: [
        {
            id: 'fc14218a-523f-492c-96b9-3439697b4353',
            handle: 'HANDLE',
            name: 'Some name',
            organization: 'Some org l.t.d.',
        },
    ],
    events: {
        registered: { timestamp: '2000-01-01T10:00:00Z', registrar_handle: 'REG-TEST' },
        transferred: { timestamp: '2000-01-01T10:00:00Z', registrar_handle: 'REG-TEST' },
        updated: { timestamp: '2000-01-01T10:00:00Z', registrar_handle: 'REG-TEST' },
        unregistered: { timestamp: null },

    },
})

export const CONTACT_REPRESENTATIVE = Object.freeze({
    id: '769a1795-4887-4aac-83bd-416b96fb8acc',
    history_id: 'ee9db93e-8141-5c1b-c022-a617ded9e4e2',
    contact_id: '4ff73dd6-ebe1-321d-9b7e-0c439d2e3936',
    name: 'Test name',
    organization: 'Test organization',
    place: {
        street: ['str1', 'str2', 'str3'],
        city: 'Test city',
        state_or_province: 'Test',
        postal_code: '12345',
        country_code: 'AO',
        company: null,
    },
    telephone: '+123.123456789',
    email: 'test@test.test',
})

export const CONTACT_RELATED_DOMAINS = Object.freeze([
    {
        domain: {
            administrative_contacts: [],
            auth_info: null,
            domain_history_id: 'a61a3251-842b-4a13-79b4-c38ed02d79cc',
            domain_id: '295a4d30-5438-4b31-c3db-ab5acb36b244',
            events: {
                registered: null,
                transfered: null,
                unregistered: null,
                updated: null,
            },
            expires_at: '2025-01-01',
            fqdn: 'test.org',
            keyset: null,
            nsset: null,
            registrant: '704c420e-df36-4276-9ab7-84c40cb52351',
            sponsoring_registrar: 'REG-TEST_A',
        },
        roles: ['holder'],
    },
])

export const CONTACT_RELATED_DOMAINS_RESPONSE = Object.freeze({
    results: CONTACT_RELATED_DOMAINS,
    pagination: {
        next_page_token: 'b9fc022e-2bd2-42bd-bcde-0f04b1c0aa931514',
        items_left: 15,
    },
})

export const DOMAIN = {
    domain_id: 'UUID-HOGWARTS',
    fqdn: 'example.com',
    expires_at: '2010-10-02T00:00:00Z',
    registrant: CONTACT_HARRY,
    events: {
        registered: {
            timestamp: '2006-04-11T20:11:16.150Z',
            registrar_handle: 'REG-MOJEID',
        },
        transferred: {
            timestamp: '2006-04-11T20:11:16.150Z',
            registrar_handle: 'REG-MOJEID',
        },
        updated: {
            timestamp: '2007-04-02T18:39:40.798Z',
            registrar_handle: 'REG-MOJEID',
        },
        unregistered: {
            timestamp: null,
        },
    },
}

export const STATE_FLAGS = Object.freeze({
    linked: true,
    serverBlocked: false,
    unknownFlag: true,
    serverInzoneManual: true,
})

export const KEYSET = {
    keyset_handle: 'KEYSET',
    keyset_id: 'UUID-KEYSET',
    dns_keys: [
        { flags: 257, protocol: 3, alg: 13,
            key: 'LM4zvjUgZi2XZKsYooDE0HFYGfWp242fKB+O8sLsuox8S6MJTowY8lBDjZD7JKbmaNot3+1H8zU9TrDzWmmHwQ==' },
        { flags: 257, protocol: 3, alg: 5,
            key: 'NGRmNmRmMWVjY2YwMTgwMWJmOTdkNGM0OGJjMmE0YzJhYTE1ZjU2M2M2YjgxNjBlY2M4NzRjMTNjZGEyYzdhMiAgLQo=' },
    ],
    technical_contacts: [{ id: CONTACT_YODA.contact_id }, { id: CONTACT_OBIWAN.contact_id }],
    events: {
        registered: { timestamp: '2000-01-01T10:00:00Z', registrar_handle: 'REG-SW' },
        transferred: { timestamp: '2000-01-01T10:00:00Z', registrar_handle: 'REG-SW' },
        updated: { timestamp: '2000-01-01T10:00:00Z', registrar_handle: 'REG-SW' },
        unregistered: {
            timestamp: null,
        },
    },
}

export const NSSET = {
    nsset_id: 'UUID-NSSET',
    nsset_handle: 'NSSET',
    dns_hosts: [
        {fqdn: 'dns.example.com', ip_addresses: ['127.0.0.1']},
        {fqdn: 'dns2.example.com', ip_addresses: ['127.0.0.2', '127.0.0.3']},
    ],
    technical_contacts: [{ id: CONTACT_OBIWAN.contact_id }, { id: CONTACT_YODA.contact_id }],
    events: {
        registered: {registrar_handle: 'REG-SW', timestamp: '2000-01-01T10:00:00Z'},
        transferred: {registrar_handle: 'REG-SW', timestamp: '2000-01-01T10:00:00Z'},
        updated: {registrar_handle: 'REG-CZNIC', timestamp: '2001-01-01T10:00:00Z'},
        unregistered: {
            timestamp: null,
        },
    },
}

export const NSSET2 = {
    nsset_id: 'UUID-NSSET2',
    nsset_handle: 'NSSET2',
    dns_hosts: [
        {fqdn: 'dns.example.com', ip_addresses: ['127.0.0.1']},
        {fqdn: 'dns3.example.com', ip_addresses: ['127.0.0.3', '127.0.0.4']},
    ],
    technical_contacts: [{ id: CONTACT_OBIWAN.contact_id }],
    events: {
        registered: {registrar_handle: 'REG-SW', timestamp: '2000-01-01T10:00:00Z'},
        transferred: {registrar_handle: 'REG-SW', timestamp: '2000-01-01T10:00:00Z'},
        updated: {registrar_handle: 'REG-CZNIC', timestamp: '2001-01-01T10:00:00Z'},
        unregistered: null,
    },
}

export const REGISTRAR_EMPIRE = {
    registrar_handle: 'REG-EMPIRE',
}

export const REGISTRAR_REBELS = {
    registrar_handle: 'REG-REBELS',
}

export const REGISTRAR_LIST = [
    {
        zone_access: {wizards: true, developers: false, testers: true},
        credit: {wizards: 'unlimited', developers: 0, testers: 42000},
        info: {
            company_registration_number: '',
            emails: ['ministry@example.org'],
            fax: '',
            is_system_registrar: false,
            is_vat_payer: true,
            name: '',
            organization: 'Ministry of magic',
            payment_memo_regex: '',
            place: {},
            registrar_handle: 'REG-HARRY',
            registrar_id: 'REG-HARRY',
            telephone: '',
            url: 'www.example.org',
            variable_symbol: '42',
            vat_identification_number: '',
        },
    },
    {
        zone_access: {},
        credit: {wizards: 'unlimited', developers: 0, testers: 42000},
        info: {
            company_registration_number: '',
            emails: ['dumbledore@example.org'],
            fax: '',
            is_system_registrar: false,
            is_vat_payer: true,
            name: '',
            organization: 'Hogwarts',
            payment_memo_regex: '',
            place: {},
            registrar_handle: 'REG-DUMBLEDORE',
            registrar_id: 'REG-DUMBLEDORE',
            telephone: '',
            url: 'www.example.org',
            variable_symbol: '423',
            vat_identification_number: '',
        },
    },
]

export const REGISTRAR_DETAIL = Object.freeze({
    zone_access: {cz: true, '0.1.2.e123.test': false },
    credit: {wizards: 'unlimited', developers: 0, testers: 42000},
    info: {
        company_registration_number: '',
        emails: ['council@example.org', 'jedi@example.org'],
        fax: '',
        is_system_registrar: false,
        is_vat_payer: true,
        name: 'Jedi Council',
        organization: 'Jedi Order',
        payment_memo_regex: '',
        place: {
            street: ['High council chamber'],
            city: 'Alderan',
            postal_code: '42000',
            state_or_province: null,
            country_code: 'AL',
        },
        registrar_handle: 'REG-EMPIRE',
        registrar_id: 'REG-EMPIRE',
        telephone: '',
        url: 'www.example.org',
        variable_symbol: '42',
        vat_identification_number: '',
    },
})

export const REGISTRAR_CERTIFICATE = Object.freeze([
    {
        credentials_id: '03aef79e-2059-4d21-9dea-54009c364f39',
        create_time: '2022-11-29T09:21:22.030Z',
        certificate: {
            fingerprint: '6c:c5:55:3f:bf:eb:d8:d8:4e:dc:bb:74:16:96:7e:af',
            cert_data_pem: 'content',
            subject: 'subject',
            issuer: 'issuer',
            not_valid_before: '2022-11-15T08:26:20Z',
            not_valid_after: '2023-11-15T08:26:20Z',
        },
    },
    {
        credentials_id: '02aef79e-2059-4d21-9dea-54009c364f39',
        create_time: '2022-11-29T09:21:22.030Z',
        certificate: {
            fingerprint: '6c:f5:55:3f:bf:eb:d8:d8:4e:dc:bb:74:16:96:7e:af',
            cert_data_pem: 'content',
            subject: 'subject',
            issuer: 'issuer',
            not_valid_before: '2022-11-15T08:26:20Z',
            not_valid_after: '2023-11-15T08:26:20Z',
        },
    },
])

export const REGISTRAR = Object.freeze({
    company_registration_number: '',
    emails: ['council@example.org'],
    fax: '',
    is_system_registrar: false,
    is_vat_payer: true,
    is_internal: false,
    name: 'Jedi Council',
    organization: 'Jedi Order',
    payment_memo_regex: '',
    place: {
        street: ['High council chamber'],
        city: 'Alderan',
        postal_code: '42000',
        state_or_province: '',
        country_code: 'AL',
    },
    registrar_handle: 'REG-EMPIRE',
    registrar_id: 'REG-EMPIRE',
    telephone: '',
    url: 'www.example.org',
    variable_symbol: '42',
    vat_identification_number: '',
})

export const ZONES = Object.freeze({
    test: [{
        zone: 'test',
        valid_from: '2020-08-21T10:00:00Z',
        valid_to: '2023-08-21T10:00:00Z',
    }],
    test2: [{
        zone: 'test2',
        valid_from: '2020-08-21T10:00:00Z',
        valid_to: null,
    }],
})

export const ZONE_ACCESS = Object.freeze({
    zone: 'test3',
    valid_from: '2020-08-21T10:00:00Z',
    valid_to: '2023-08-21T10:00:00Z',
})

export const EDIT_ZONE_ACCESS = Object.freeze({
    access: {
        zone: 'test',
        valid_from: '2020-08-21T10:00:00Z',
        valid_to: '2023-08-21T10:00:00Z',
    },
    valid_from: '2020-08-21T10:00:00Z',
    valid_to: '2024-08-21T10:00:00Z',
})

export const EMAIL_ENVELOPE = {
    uid: '527a6406-1f21-43fc-969c-e1bf1bdbe0ff.2021-01-01',
    create_datetime: '2020-08-21T10:00:00Z',
    send_datetime: '2020-09-22T12:30:00Z',
    delivery_datetime: null,
    attempts: 15,
    status: 1,
    message: {
        sender: null,
        recipient: 'recipient@example.com',
        extra_headers: {},
        attachments: [
            {
                'uuid': '00000000-0000-0000-0000-000000000001',
                'name': 'file1.txt',
                'mimetype': 'text/plain',
                'size': 4096,
            },
            {
                'uuid': '00000000-0000-0000-0000-000000000002',
                'name': 'file2.txt',
                'mimetype': 'text/plain',
                'size': 4096,
            },
        ],
        subject: 'SUBJECT',
        body: 'BODY',
        type: 'subtype_1',
    },
}

export const EMAIL_ENVELOPE_ATTACHMENTS_UNAVAILABLE = {
    uid: '2020-08-21T10:53:24.091432+00:00#527a6406-1f21-43fc-969c-e1bf1bdbe0ff',
    create_datetime: '2020-08-21T10:00:00Z',
    send_datetime: '2020-09-22T12:30:00Z',
    delivery_datetime: null,
    attempts: 15,
    status: 1,
    message: {
        sender: null,
        recipient: 'recipient@example.com',
        extra_headers: {},
        attachments: [
            {
                'uuid': '00000000-0000-0000-0000-000000000001',
                'mimetype': 'text/plain',
                'size': 4096,
            },
            {
                'uuid': '00000000-0000-0000-0000-000000000002',
                'mimetype': 'text/plain',
                'size': 4096,
            },
        ],
        subject: 'SUBJECT',
        body: 'BODY',
    },
}

export const SMS_ENVELOPE = {
    uid: '2020-11-19T12:09:04+00:00#541d3e5d-7729-4183-a652-c72ddbd3f026',
    archive: true,
    attempts: 0,
    create_datetime: '2020-11-19T12:09:04Z',
    delivery_datetime: null,
    message: {
        body: null,
        body_template: 'example.txt',
        body_template_uuid: null,
        context: {},
        recipient: '+420.124',
        type: null,
    },
    references: [],
    send_datetime: null,
    status: 0,
}

export const SEARCH_RESULTS = {
    object_type: 'contact',
    search_date: '2021-01-01',
}

export const CONTACT_SEARCH_RESULT_NO_HISTORY = Object.freeze({
    object_id: 1,
    matched_items: [
        'billing_address.street',
        'email',
        'notify_email',
        'billing_address.city',
        'additional_identifier.birthdate',
    ],
    contact: {
        additional_identifier: {
            type: 'birthdate',
            value: '1989-11-17',
        },
        billing_address: {
            street: ['Privet Drive 4', 'Diagon Alley'],
            city: 'Little Whinging',
            country_code: 'UK',
        },
        contact_handle: CONTACT_HARRY.contact_handle,
        contact_id: 1,
        emails: [CONTACT_HARRY.email],
        notify_emails: [CONTACT_HARRY.email],
        sponsoring_registrar: 'REG-HARRY',
        events: CONTACT_HARRY.events,
        organization: '',
        name: 'test name',
    },
})

export const CONTACT_SEARCH_RESULT_WITH_HISTORY = Object.freeze({
    object_id: 2,
    histories: [{
        valid_from: '2021-01-01T12:00:00+00:00',
        valid_to: '2022-01-01T12:00:00+00:00',
        matched_items: [
            'email',
            'notify_email',
        ],
        contact: {
            additional_identifier: {
                type: 'birthdate',
                value: '1989-11-17',
            },
            billing_address: {
                street: ['Privet Drive 4', 'Diagon Alley'],
                city: 'Little Whinging',
                country_code: 'UK',
            },
            contact_handle: CONTACT_HARRY.contact_handle,
            contact_id: 2,
            emails: [CONTACT_HARRY.email],
            notify_emails: [CONTACT_HARRY.email],
            sponsoring_registrar: 'REG-HARRY',
            events: CONTACT_HARRY.events,
            organization: '',
        },
    }],
    contact: {
        additional_identifier: {
            type: 'birthdate',
            value: '1989-11-17',
        },
        billing_address: {
            street: ['Privet Drive 4', 'Diagon Alley'],
            city: 'Little Whinging',
            country_code: 'UK',
        },
        contact_handle: CONTACT_HARRY.contact_handle,
        contact_id: 2,
        emails: [CONTACT_HARRY.email],
        notify_emails: [CONTACT_HARRY.email],
        sponsoring_registrar: 'REG-HARRY',
        events: CONTACT_HARRY.events,
        organization: '',
    },
})

export const NSSET_SEARCH_RESULT_WITH_HISTORY = Object.freeze({
    object_id: 3,
    histories: [{
        valid_from: '2021-01-01T12:00:00+00:00',
        valid_to: '2022-01-01T12:00:00+00:00',
        matched_items: ['dns_hosts.fqdn'],
        nsset: {
            nsset_id: 'c6271c9d-4ad2-4111-c60b-358769d5a66b',
            nsset_handle: 'TEST-NSSET',
            nsset_history_id: '12ebdb86-3774-5e76-b081-c3e8e0e23ebc',
            sponsoring_registrar: 'REG-TEST',
            dns_hosts: [
                { fqdn: 'test.nsset.org', ip_addresses: ['127.0.0.1'] },
                { fqdn: 'testing.nsset.org', ip_addresses: ['127.0.0.1'] },
            ],
            technical_contacts: [
                {
                    id: 'df14218a-523f-492c-96b9-3439697b4353',
                    handle: 'HANDLE',
                    name: 'Some name',
                    organization: 'Some org l.t.d.',
                },
            ],
            technical_check_level: 3,
            events: {
                registered: { timestamp: '2000-01-01T10:00:00Z', registrar_handle: 'REG-TEST' },
                transferred: { timestamp: '2000-01-01T10:00:00Z', registrar_handle: 'REG-TEST' },
                updated: { timestamp: '2000-01-01T10:00:00Z', registrar_handle: 'REG-TEST' },
                unregistered: { timestamp: null },
            },
        },
    }],
    nsset: {
        nsset_id: 'c6271c9d-4ad2-4111-c60b-358769d5a66b',
        nsset_handle: 'TEST-NSSET',
        nsset_history_id: '12ebdb86-3774-5e76-b081-c3e8e0e23ebc',
        sponsoring_registrar: 'REG-TEST',
        dns_hosts: [
            { fqdn: 'test.nsset.org', ip_addresses: ['127.0.0.1'] },
            { fqdn: 'testing.nsset.org', ip_addresses: ['127.0.0.1'] },
        ],
        technical_contacts: [
            {
                id: 'df14218a-523f-492c-96b9-3439697b4353',
                handle: 'HANDLE',
                name: 'Some name',
                organization: 'Some org l.t.d.',
            },
        ],
        technical_check_level: 3,
        events: {
            registered: { timestamp: '2000-01-01T10:00:00Z', registrar_handle: 'REG-TEST' },
            transferred: { timestamp: '2000-01-01T10:00:00Z', registrar_handle: 'REG-TEST' },
            updated: { timestamp: '2000-01-01T10:00:00Z', registrar_handle: 'REG-TEST' },
            unregistered: { timestamp: null },

        },
    },
})

export const KEYSET_SEARCH_RESULT_WITH_HISTORY = Object.freeze({
    object_id: 3,
    histories: [{
        valid_from: '2021-01-01T12:00:00+00:00',
        valid_to: '2022-01-01T12:00:00+00:00',
        matched_items: ['dns_keys.key'],
        keyset: {
            keyset_id: 'b6271c9d-4ad2-4111-c60b-358769d5a66b',
            keyset_handle: 'TEST-KEYSET',
            keyset_history_id: '12ecdc86-3774-5e76-b081-c3e8e0e23ebc',
            sponsoring_registrar: 'REG-TEST',
            dns_keys: [{ flags: 257, protocol: 3, alg: 10, key: 'xxx' }],
            technical_contacts: [
                {
                    id: 'fc14218a-523f-492c-96b9-3439697b4353',
                    handle: 'HANDLE',
                    name: 'Some name',
                    organization: 'Some org l.t.d.',
                },
            ],
            events: {
                registered: { timestamp: '2000-01-01T10:00:00Z', registrar_handle: 'REG-TEST' },
                transferred: { timestamp: '2000-01-01T10:00:00Z', registrar_handle: 'REG-TEST' },
                updated: { timestamp: '2000-01-01T10:00:00Z', registrar_handle: 'REG-TEST' },
                unregistered: { timestamp: null },
            },
        },
    }],
    keyset: {
        keyset_id: 'c6271c9d-4ad2-4222-c60b-358769d5a66c',
        keyset_handle: 'TEST-KEYSET',
        keyset_history_id: 'c6271c9d-4bc3-4111-c60b-358769d5a66b',
        sponsoring_registrar: 'REG-TEST',
        dns_keys: [{ flags: 257, protocol: 3, alg: 10, key: 'xxx' }],
        technical_contacts: [
            {
                id: 'fc14218a-523f-492c-96b9-3439697b4353',
                handle: 'HANDLE',
                name: 'Some name',
                organization: 'Some org l.t.d.',
            },
        ],
        events: {
            registered: { timestamp: '2000-01-01T10:00:00Z', registrar_handle: 'REG-TEST' },
            transferred: { timestamp: '2000-01-01T10:00:00Z', registrar_handle: 'REG-TEST' },
            updated: { timestamp: '2000-01-01T10:00:00Z', registrar_handle: 'REG-TEST' },
            unregistered: { timestamp: null },

        },
    },
})

export const LETTER_ENVELOPE = {
    uid: '2020-12-09T08:24:58+00:00#5f2d3f16-8625-4c81-9bee-f3e1a43e2b03',
    archive: true,
    attempts: 1,
    create_datetime: '2020-10-02T23:00:02.309Z',
    delivery_datetime: null,
    message: {
        body_template: null,
        body_template_uuid: null,
        context: {},
        file: {
            mimetype: 'application/pdf',
            name: 'request.pdf',
            size: 43119,
            uuid: '032c5b31-b2fc-3436-bb82-e030e7585a70',
        },
        recipient: {
            city: 'London',
            country: 'UK',
            name: 'Ludo Pytloun',
            organization: '',
            postal_code: '333 33',
            state_or_province: '',
            street: 'Straight',
        },
        type: 'test_subtype',
        references: [
            {
                type: 'contact',
                value: '352a1185-cbcb-3acb-be37-b6d44b5907c4',
            },
        ],
        send_datetime: '2020-10-03T03:45:07.325Z',
        status: 1,
    },
}

export const MESSAGE_REQUEST = Object.freeze({
    next: '/api/messages/email/?offset=66666666.666666&page_size=10',
    results:[
        {
            archive: true,
            attempts: 1,
            create_datetime: '2020-08-21T00:00:00Z',
            send_datetime: '2020-09-22T00:00:00Z',
            delivery_datetime: null,
            message_id: '<00000000.0000000000@test.cz>',
            status: 1,
            uid: '2020-12-09T08:24:58+00:00.5f2d3f16-8625-4c81-9bee-f3e1a43e2b03',
            references: [],
            message: {
                type: null,
                sender: null,
                recipient: 'recipient@example.com',
                extra_headers: {},
                attachments: [
                    {
                        'uuid': '00000000-0000-0000-0000-000000000001',
                        'name': 'file1.txt',
                        'mimetype': 'text/plain',
                        'size': 4096,
                    },
                    {
                        'uuid': '00000000-0000-0000-0000-000000000002',
                        'name': 'file2.txt',
                        'mimetype': 'text/plain',
                        'size': 4096,
                    },
                ],
                subject_template: 'SUBJECT',
                body_template: 'BODY',
                body_template_html: null,
                subject_template_uuid: null,
                body_template_uuid: '00000000-0000-0000-0000-000000000004',
                body_template_html_uuid: '00000000-0000-0000-0000-000000000005',
                context: {},
            },
        },
    ],
})

export const MESSAGE_PARAMS = Object.freeze({
    selected_subtypes: ['subtype_1', 'subtype_2'],
    message_subtypes: [
        { label: 'subtype 1', name: 'subtype_1', tags: ['email'] },
        { label: 'subtype 2', name: 'subtype_2', tags: ['email'] },
    ],
    message_type: 'email',
    time_begin_from: '2021-01-01T12:00:00+01:00',
    time_begin_to:  '2022-01-01T12:00:00+01:00',
})

export const REPORT = {
    service: 'test_service',
    name: 'test_report',
    label: 'Test Report',
    input_parameters: [
        { name: 'uuid', type: 'uuid', label: 'UUID' },
        { name: 'handle', type: 'character varying', label: 'Handle' },
        { name: 'exdate', type: 'date', label: 'Expiration date' },
        { name: 'is_checked', type: 'boolean', label: 'Is checked' },
    ],
    output_parameters: [
        { name: 'uuid', type: 'uuid', label: 'UUID' },
        { name: 'handle', type: 'character varying', 'label': 'Handle' },
        { name: 'exdate', type: 'date', label: 'Expiration date' },
    ],
}

export const REPORT_PROPERTIES = {
    uuid: '352a1185-cbcb-3acb-be37-b6d44b5907c4',
    handle: 'test.cz',
    exdate: '2021-01-01',
    is_checked: false,
}

export const REPORT_TABLE_DATA = `
    { 'uuid': 'e84b93c6-7bfe-11be-9439-0242ac130002', 'handle': 'example.cz', 'exdate': '2018-01-01' }\n
    { 'uuid': 'e84b965a-7fbe-11eb-9439-0242ac130002', 'handle': 'test.cz', 'exdate': '2018-01-02' }\n
    { 'uuid': 'e84b98c6-ebfe-11eb-9439-0242ac130002', 'handle': 'domain.cz', 'exdate': '2018-01-03' }\n
`

export const REPORT_TABLE_DATA_PARSED = [
    { uuid: 'e84b93c6-7bfe-11be-9439-0242ac130002', handle: 'example.cz', exdate: '2018-01-01' },
    { uuid: 'e84b965a-7fbe-11eb-9439-0242ac130002', handle: 'test.cz', exdate: '2018-01-02' },
    { uuid: 'e84b98c6-ebfe-11eb-9439-0242ac130002', handle: 'domain.cz', exdate: '2018-01-03' },
]

export const REPORT_TABLE_DATA_FILTERED = [
    { uuid: 'e84b93c6-7bfe-11be-9439-0242ac130002', handle: 'example.cz', exdate: '2018-01-01' },
]

export const REPORT_TABLE_HEADERS = [
    { text: 'UUID', value: 'uuid', divider: true, formatter: () => true, sort: null },
    { text: 'Handle', value: 'handle', divider: true, formatter: () => true, sort: null },
    { text: 'Expiration date', value: 'exdate', divider: true, formatter: () => true, sort: null },
]

export const REPORT_TABLE_EXPORT = Object.freeze([
    { parameter_1: 'Parameter 1', parameter_2: 'Parameter 2', parameter_3: 'Parameter 3' },
    { parameter_1: 'Parameter 1', parameter_2: 'Parameter 2', parameter_3: 'Parameter 3' },
    { parameter_1: 'Parameter 1', parameter_2: 'Parameter 2', parameter_3: 'Parameter 3' },
])

export const LOG_SERVICES = Object.freeze(['Example 1', 'Example 2', 'Example 3'])
export const LOG_ENTRY_TYPES = Object.freeze({ log_entry_types: ['Type 2', 'Type 1'] })

export const LOG_PARAMS = Object.freeze({
    time_begin_from: '2021-01-01T12:00:00+01:00',
    time_begin_to: '2022-01-01T12:00:00+01:00',
    log_services: {
        items: ['Example 1', 'Example 2', 'Example 3'],
        selected_value: 'Example 2',
    },
    log_entry_types: {
        items: ['Type 1', 'Type 2', 'Type 3'],
        selected_values: ['Type 1', 'Type 2'],
    },
    username: 'jdoe',
    object_reference_type: 'ref_type',
    object_reference_value: 'ref_value',
    property_name: 'soup',
    property_value: 'gazpacho',
    offset: '55555.55555',
    page_size: 10,
})

export const LOG_PARAMS_DETAIL = Object.freeze({
    log_entry_id: '1#20220101T150000.555555#Example1#0',
    log_entry_url: '/api/logger/log_entry/1#20220101T150000.555555#Example1#0',
})

export const VISIBLE_LOG_REQUEST_HEADERS = Object.freeze([
    { text: 'Log entry ID', value: 'log_entry_id' },
    { text: 'Timestamp from', value: 'time_begin' },
    { text: 'Timestamp to', value: 'time_end' },
    { text: 'Service', value: 'service' },
    { text: 'Source IP', value: 'source_ip' },
    { text: 'Log entry type', value: 'log_entry_type' },
    { text: 'Username', value: 'username' },
    { text: 'Result name', value: 'result_name' },
])

export const LOG_REQUEST_RESULT = Object.freeze({
    next: '/logger/api/log_entry/?offset=55555555.555555',
    results: [
        {
            url: '/logger/api/log_entry/',
            log_entry_id: '1#20220101T150000.555555#Example1#0',
            time_begin: '2021-01-01T12:00:00+01:00',
            time_end: '2022-01-01T12:00:00+01:00',
            source_ip: '127.0.0.1',
            service: 'Example 2',
            log_entry_type: 'Type 2',
            result_code: 0,
            result_name: 'Success',
            raw_request: null,
            raw_response: null,
            username: 'johndoe',
            user_id: 1,
            input_properties: {},
            output_properties: {},
            references: {},
        },
        {
            url: '/logger/api/log_entry/',
            log_entry_id: '1#20220101T150000.555555#Example1#0',
            time_begin: '2021-01-01T12:00:00+01:00',
            time_end: '2022-01-01T12:00:00+01:00',
            source_ip: '127.0.0.1',
            service: 'Example 2',
            log_entry_type: 'Type 2',
            result_code: 0,
            result_name: 'Success',
            raw_request: null,
            raw_response: null,
            username: 'johndoe',
            user_id: 1,
            input_properties: {},
            output_properties: {},
            references: {
                example: ['2021-01-01T12:00:00.555555+00:00#422c9445-050f-403a-8d12-0d648c3438f7'],
            },
        },
    ],
})

export const LOG_REQUEST_DETAIL = Object.freeze({
    input_properties: {
        time_begin_from: [{
            value: '2022-02-17T00:00:00+01:00',
            children: {},
        }],
    },
    log_entry_id: '1#20220101T150000.555555#Example1#0',
    log_entry_type: 'Type 2',
    output_properties: {},
    raw_request: null,
    raw_response: null,
    references: {
        example: ['422c9445-050f-403a-8d12-0d648c3438f7'],
    },
    result_code: 0,
    result_name: 'Success',
    service: 'Example 2',
    source_ip: '127.0.0.1',
    time_begin: '2021-01-01T12:00:00+01:00',
    time_end: '2022-01-01T12:00:00+01:00',
    user_id: 1,
    username: 'johndoe',
})

export const LOG_REQUEST_DETAIL_XML = Object.freeze({
    input_properties: {
        time_begin_from: [{
            value: '2022-02-17T00:00:00+01:00',
            children: {},
        }],
    },
    log_entry_id: '1#20220101T150000.555555#Example1#0',
    log_entry_type: 'Type 2',
    output_properties: {},
    raw_request: `<?xml version="1.0" encoding="UTF-8" standalone="no"?>
        <epp xmlns="urn:aaaa:test:xml:ns:epp-1.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"></epp>`,
    raw_response: `<?xml version="1.0" encoding="UTF-8" standalone="no"?>
        <epp xmlns="urn:aaaa:test:xml:ns:epp-1.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"></epp>`,
    references: {
        example: ['422c9445-050f-403a-8d12-0d648c3438f7'],
    },
    result_code: 0,
    result_name: 'Success',
    service: 'Example 2',
    source_ip: '127.0.0.1',
    time_begin: '2021-01-01T12:00:00+01:00',
    time_end: '2022-01-01T12:00:00+01:00',
    user_id: 1,
    username: 'johndoe',
})

export const ADDITIONAL_OUTZONE_CONTACTS_DATA = Object.freeze({
    results: [
        {
            domain_id: 'aad5292c-6419-4c75-cc14-7715968e64f3',
            fqdn: 'xaver.org',
            registrant_id: 'acf1277c-6919-4b74-cv14-7144968e64f3',
            registrant_handle: 'HANDLE3',
            registrant_name: 'Xaver Doe',
            registrant_organization: '',
            contacts: [{ type: 'email', value: 'xaver@org.com' }],
            additional_contacts: [{ type: 'email', value: 'xaver@email.org' }],
        },
        {
            domain_id: 'cbd5292c-6419-4c74-ee14-7715968e64f3',
            fqdn: 'test1.org',
            registrant_id: 'acc1292c-6919-4b74-ce14-7144968e64f3',
            registrant_handle: 'HANDLE1',
            registrant_name: 'John Doe',
            registrant_organization: '',
            contacts: [{ type: 'email', value: 'email@org.com' }],
            additional_contacts: [{ type: 'email', value: 'test@email.org' }],
        },
        {
            domain_id: 'ccd5292c-6419-4c75-ee14-7715968e64f3',
            fqdn: 'test2.org',
            registrant_id: 'aff1292c-6919-4b74-cv14-7144968e64f3',
            registrant_handle: 'HANDLE2',
            registrant_name: 'Jane Doe',
            registrant_organization: '',
            contacts: [{ type: 'email', value: 'jane@org.com' }],
            additional_contacts: [{ type: 'email', value: 'jane@email.org' }],
        },
    ],
    pagination: { next_page_token: 'some_token', items_left: 0},
})

export const ADDITIONAL_DELETE_CONTACTS_DATA = Object.freeze({
    results: [
        {
            domain_id: 'aad5292c-6419-4c75-cc14-7715968e64f3',
            fqdn: 'xaver.org',
            registrant_id: 'acf1277c-6919-4b74-cv14-7144968e64f3',
            registrant_handle: 'HANDLE3',
            registrant_name: 'Xaver Doe',
            registrant_organization: '',
            contacts: [{ type: 'telephone', value: '+420.123456789' }],
            additional_contacts: [{ type: 'telephone', value: '+420.123456789' }],
        },
        {
            domain_id: 'cbd5292c-6419-4c74-ee14-7715968e64f3',
            fqdn: 'test1.org',
            registrant_id: 'acc1292c-6919-4b74-ce14-7144968e64f3',
            registrant_handle: 'HANDLE1',
            registrant_name: 'John Doe',
            registrant_organization: '',
            contacts: [{ type: 'telephone', value: '+420.111222333' }],
            additional_contacts: [{ type: 'telephone', value: '+420.111222333' }],
        },
        {
            domain_id: 'ccd5292c-6419-4c75-ee14-7715968e64f3',
            fqdn: 'test2.org',
            registrant_id: 'aff1292c-6919-4b74-cv14-7144968e64f3',
            registrant_handle: 'HANDLE2',
            registrant_name: 'Jane Doe',
            registrant_organization: '',
            contacts: [{ type: 'telephone', value: '+420.555555555' }],
            additional_contacts: [{ type: 'telephone', value: '+420.555555555' }],
        },
        {
            domain_id: 'cscc5292c-6419-4c75-ee14-2355968e64f3',
            fqdn: 'test4.org',
            registrant_id: 'acf1277c-6919-4b74-cv14-7144968e64f3',
            registrant_handle: 'HANDLE4',
            registrant_name: 'Josh Doe',
            registrant_organization: '',
            contacts: [{ type: 'telephone', value: '+420.555555555' }],
            additional_contacts: [],
        },
    ],
    pagination: { next_page_token: 'some_token', items_left: 0},
})

export const GROUPED_CONTACT_DATA = [
    {
        items: [{
            additional_contacts: [{ type: 'email', value: 'test@email.org' }],
            contacts: [{ type: 'email', value: 'email@org.com' }],
            domain_id: 'cbd5292c-6419-4c74-ee14-7715968e64f3',
            fqdn: 'test1.org',
            registrant_handle: 'HANDLE1',
            registrant_id: 'acc1292c-6919-4b74-ce14-7144968e64f3',
            registrant_name: 'John Doe',
            registrant_organization: '',
        }],
        name: 'HANDLE1',
    },
    {
        items: [{
            additional_contacts: [{ type: 'email', value: 'jane@email.org' }],
            contacts: [{ type: 'email', value: 'jane@org.com' }],
            domain_id: 'ccd5292c-6419-4c75-ee14-7715968e64f3',
            fqdn: 'test2.org',
            registrant_handle: 'HANDLE2',
            registrant_id: 'aff1292c-6919-4b74-cv14-7144968e64f3',
            registrant_name: 'Jane Doe',
            registrant_organization: '',
        }],
        name: 'HANDLE2',
    },
    {
        items: [{
            additional_contacts: [{ type: 'email', value: 'xaver@email.org' }],
            contacts: [{ type: 'email', value: 'xaver@org.com' }],
            domain_id: 'aad5292c-6419-4c75-cc14-7715968e64f3',
            fqdn: 'xaver.org',
            registrant_handle: 'HANDLE3',
            registrant_id: 'acf1277c-6919-4b74-cv14-7144968e64f3',
            registrant_name: 'Xaver Doe',
            registrant_organization: '',
        }],
        name: 'HANDLE3',
    },
]
export const ADDITIONAL_OUTZONE_CONTACTS_FOR_UPDATE = Object.freeze({
    domains: {
        'ccd5292c-2119-4c75-ee14-7715968e64f4': [{ type: 'email', value: 'johndoe@email.com' }],
        'eed5292c-2229-1c74-ee24-7712568e64f4': [{ type: 'email', value: 'janedoe@email.com' }],
    },
})

export const ADDITIONAL_DELETE_CONTACTS_FOR_UPDATE = Object.freeze({
    domains: {
        'ccd5292c-2119-4c75-ee14-7715968e64f4': [{ type: 'telephone', value: '+420.123456789' }],
        'eed5292c-2229-1c74-ee24-7712568e64f4': [{ type: 'telephone', value: '+420.123456789' }],
    },
})

export const REGISTRAR_CERTIFICATIONS = Object.freeze([
    {
        certification_id: 'ccd5292c-2119-4c75-ee14-7715968effff',
        registrar_handle: 'REG-TEST',
        valid_from: '2023-04-25T08:20:58.109Z',
        valid_to: '2024-04-30T08:20:58.109Z',
        classification: 15,
        file_id: 'ccd52111-2119-4c75-ee14-7715968e64f4',
    },
    {
        certification_id: 'ddd5292c-2119-4c75-ee14-7715968effff',
        registrar_handle: 'REG-TEST',
        valid_from: '2023-04-30T08:20:58.109Z',
        valid_to: '2024-04-30T08:20:58.109Z',
        classification: 55,
        file_id: 'dd552123-2119-4c75-ee14-7715968e64f4',
    },
])
