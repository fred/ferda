import axios from 'axios'
import MockAdapter from 'axios-mock-adapter'
import flushPromises from 'flush-promises'

import { CONTACT_HARRY } from 'ferda-test/TestObjects'
import { initWrapper } from 'ferda-test/TestUtils'
import { HistoryMixin } from 'ferda/HistoryMixin'
import { OBJECT_TYPE } from 'ferda/RegistryMixin'
import { URLS } from 'ferda/Urls'
import ObjectHistory from 'ferda/common/ObjectHistory.vue'

describe('ObjectHistory', () => {
    let axios_mock
    let wrapper
    let info_diff

    const historyProps = {
        uuid: 'UUID-HARRY',
        object_handle: 'HARRY',
        from_history_id: 'V1',
        to_history_id: 'V2',
        object_history: {
            contact_id: 'UUID-HARRY',
            timeline: [{
                contact_history_id: 'V1',
                valid_from: '2000-01-01T03:00:00Z',
            }, {
                contact_history_id: 'V2',
                valid_from: '2001-01-01T03:00:00+02:00',
            }],
            valid_to: null,
        },
        object_type: OBJECT_TYPE.CONTACT,
        object_events: CONTACT_HARRY.events,
    }

    beforeEach(async() => {
        axios_mock = new MockAdapter(axios)
        axios_mock.onGet(URLS.object_info_history_ajax(OBJECT_TYPE.CONTACT, 'UUID-HARRY', 'V1'))
            .reply(200, CONTACT_HARRY)
        axios_mock.onGet(URLS.object_info_history_ajax(OBJECT_TYPE.CONTACT, 'UUID-HARRY', 'V2'))
            .reply(200, CONTACT_HARRY)

        info_diff = HistoryMixin.methods.contact_diff(CONTACT_HARRY, CONTACT_HARRY)

        wrapper = initWrapper(ObjectHistory, {
            propsData: {
                ...historyProps,
                info_diff,
            },
            slots: {
                'object-info-append': 'OBJECT_INFO_SLOT_APPEND',
                'object-info-prepend': 'OBJECT_INFO_SLOT_PREPEND',
            },
        })

        await flushPromises()
    })

    test('matches snapshot', () => {
        expect(info_diff).not.toBe(null)
        expect(wrapper.element).toMatchSnapshot()
    })

    test('emitting info-update', () => {
        const info = wrapper.emitted('info-update')

        expect(info && info[0] && info[0][0]).not.toBe(null)
        expect(info[0][0].old).not.toBe(null)
        expect(info[0][0].new).not.toBe(null)
    })
})
