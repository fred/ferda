import { initWrapper } from 'ferda-test/TestUtils'
import EventsInfoBox from 'ferda/common/EventsInfoBox.vue'

describe('EventsInfoBox', () => {
    let wrapper

    beforeEach(async() => {
        wrapper = initWrapper(EventsInfoBox, {
            propsData: {
                events: {
                    registered: {
                        registrar_handle: 'REG-HOGWARTS',
                        timestamp: '2019-01-01T09:00:00Z',
                    },
                    transferred: {
                        registrar_handle: 'REG-MINISTRY',
                        timestamp: '2019-02-02T11:11:11.111+01:00',
                    },
                    updated: null,
                    unregistered: null,
                },
            },
        })
    })

    test('matches snapshot', () => {
        expect(wrapper.element).toMatchSnapshot()
    })
})
