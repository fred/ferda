import { initWrapper } from 'ferda-test/TestUtils'
import FerdaIcon from 'ferda/common/FerdaIcon.vue'

describe('FerdaIcon with undefined icon', () => {
    let wrapper

    beforeEach(() => {
        wrapper = initWrapper(FerdaIcon)
    })

    test('matches snapshot', () => {
        expect(wrapper.element).toMatchSnapshot()
    })

    test('method is_mdi returns false', () => {
        expect(wrapper.vm.is_mdi).toBe(false)
    })

    test('adds the correct icon prefix', () => {
        expect(wrapper.text()).toBe('')
    })

    test('computes added_classes correctly', () => {
        expect(wrapper.vm.added_classes).toBe(undefined)
    })
})

describe('FerdaIcon for SVG icon', () => {
    let wrapper

    beforeEach(() => {
        wrapper = initWrapper(FerdaIcon, {
            propsData: {
                icon: 'test-icon',
            },
        })
    })

    test('matches snapshot', () => {
        expect(wrapper.element).toMatchSnapshot()
    })

    test('rmethod is_mdi returns true', () => {
        expect(wrapper.vm.is_mdi).toBe(true)
    })

    test('adds the correct icon prefix for an SVG icon', () => {
        expect(wrapper.text()).toBe('$vuetify.icons.test-icon')
    })

    test('computes added_classes correctly for an SVG icon', () => {
        expect(wrapper.vm.added_classes).toBe('svg-icon svg-test-icon')
    })
})

describe('FerdaIcon for FA icon', () => {
    let wrapper

    beforeEach(() => {
        wrapper = initWrapper(FerdaIcon, {
            propsData: {
                icon: 'fa-test-icon',
            },
        })
    })

    test('matches snapshot', () => {
        expect(wrapper.element).toMatchSnapshot()
    })

    test('method is_mdi returns false ', () => {
        expect(wrapper.vm.is_mdi).toBe(false)
    })

    test('adds the correct icon prefix for an FA icon', () => {
        expect(wrapper.text()).toBe('fa-test-icon')
    })

    test('computes added_classes to empty string for an FA icon', () => {
        expect(wrapper.vm.added_classes).toBe(undefined)
    })
})
