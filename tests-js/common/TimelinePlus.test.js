import { Timeline } from 'timeline-plus'

import { initWrapper } from 'ferda-test/TestUtils'
import TimelinePlus from 'ferda/common/TimelinePlus.vue'

jest.mock('timeline-plus')

describe('StateFlagsInfoBox', () => {
    let wrapper

    beforeEach(async() => {
        Timeline.mockClear()
        wrapper = initWrapper(TimelinePlus, {
            propsData: {
                items: [],
                groups: [],
            },
        })
    })

    test('matches snapshot', () => {
        expect(wrapper.element).toMatchSnapshot()
    })

    test('updates', done => {
        const mockTimeline = Timeline.mock.instances[0]
        expect(mockTimeline.constructor.mock.calls[0]).toEqual([null, [], [], wrapper.vm.$options.default_options])
        wrapper.setProps({
            items: ['item'],
            groups: ['group'],
            options: {zoomable: false},
        })

        // XXX: Workaround for @vue/test-utils==1.0.0-beta.30
        // See https://github.com/vuejs/vue-test-utils/issues/1363
        wrapper.vm.$nextTick(() => {
            expect(mockTimeline.setItems.mock.calls[0]).toEqual([['item']])
            expect(mockTimeline.setGroups.mock.calls[0]).toEqual([['group']])
            expect(mockTimeline.setOptions.mock.calls[0]).toEqual([{
                editable: false,
                selectable: false,
                verticalScroll: true,
                zoomable: false,
            }])
            done()
        })
    })
})
