import { initWrapper } from 'ferda-test/TestUtils'
import BadgeArea from 'ferda/common/BadgeArea.vue'

describe('BadgeArea', () => {
    let wrapper

    beforeEach(() => {
        wrapper = initWrapper(BadgeArea, { propsData: {
            badges: [{ name: 'test-badge', image: 'test.svg' }],
        }})
    })

    test('matches snapshot', () => {
        expect(wrapper.element).toMatchSnapshot()
    })
})
