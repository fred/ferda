import { initWrapper } from 'ferda-test/TestUtils'
import AddressInfoBox from 'ferda/common/AddressInfoBox.vue'

describe('AddressInfoBox defaults', () => {
    let wrapper

    beforeEach(() => {
        wrapper = initWrapper(AddressInfoBox, {
            propsData: {
                label: 'Personal data',
                public_access: false,
                address: {
                    street: ['Privet Drive 4', 'Diagon Alley'],
                    city: 'Little Whinging',
                },
            },
        }, false)
    })

    test('matches snapshot', () => {
        expect(wrapper.element).toMatchSnapshot()
    })

    test('defaults', () => {
        expect(wrapper.vm.show_items_access).toBe(false)
        expect(wrapper.vm.show_box_access).toBe(false)
    })

    test('has proper content', () => {
        expect(wrapper.findAll('label').at(0).text())
            .toMatch(/common:address.company:/)
        expect(wrapper.findAll('label').at(1).text())
            .toMatch(/common:address.street:/)
        expect(wrapper.findAll('label').at(2).text())
            .toMatch(/common:address.city:/)
        expect(wrapper.findAll('div.tref-value').at(1).findAll('div').at(1).text()).toBe('Privet Drive 4')
        expect(wrapper.findAll('div.tref-value').at(1).findAll('div').at(2).text()).toBe('Diagon Alley')
        expect(wrapper.findAll('div.tref-value').at(2).text()).toBe('Little Whinging')
    })
})

describe('AddressInfoBox with items access displayed', () => {
    let wrapper

    beforeEach(() => {
        wrapper = initWrapper(AddressInfoBox, {
            propsData: {
                label: 'Personal data',
                public_access: false,
                show_items_access: true,
                address: {
                    street: ['Privet Drive 4', 'Diagon Alley'],
                    city: 'Little Whinging',
                },
            },
        }, false)
    })

    test('has proper content', () => {
        expect(wrapper.findAll('.access').length).toBe(6)
    })
})
