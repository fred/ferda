import axios from 'axios'
import MockAdapter from 'axios-mock-adapter'
import flushPromises from 'flush-promises'
import moment from 'moment-timezone'

import { initWrapper } from 'ferda-test/TestUtils'
import { OBJECT_TYPE } from 'ferda/RegistryMixin'
import { URLS } from 'ferda/Urls'
import StateFlagsHistory from 'ferda/common/StateFlagsHistory.vue'

describe('StateFlagsHistory', () => {
    let axios_mock
    let wrapper

    beforeEach(async() => {
        moment.now = () => Date.parse('2019-02-02T12:00:00Z')

        axios_mock = new MockAdapter(axios)
        axios_mock.onGet(URLS.object_state_history_ajax(OBJECT_TYPE.CONTACT, 'HARRY')).reply(200, {
            timeline: [
                {
                    valid_from: '2019-01-01T12:00:00Z',
                    flags: {
                        linked: true,
                    },
                },
                {
                    valid_from: '2019-01-02T13:00:00Z',
                    flags: {
                        serverBlocked: true,
                    },
                },
                {
                    valid_from: '2019-01-02T15:00:00Z',
                    flags: {
                        serverBlocked: false,
                    },
                },
                {
                    valid_from: '2019-01-03T14:00:00Z',
                    flags: {
                        serverBlocked: true,
                    },
                },
                {
                    valid_from: '2019-01-01T12:00:00Z',
                    flags: {
                        deleteCandidate: true,
                    },
                },
            ],
        })
        axios_mock.onGet(URLS.object_state_history_ajax(OBJECT_TYPE.CONTACT, 'DUMBLEDORE')).reply(500)
        wrapper = initWrapper(StateFlagsHistory, {
            propsData: {
                uuid: 'HARRY',
                start: '2019-01-01T12:00:00Z',
                end: '2019-01-09T00:00:00Z',
            },
        })
        await flushPromises()
    })

    test('matches snapshot', () => {
        expect(wrapper.element).toMatchSnapshot()
    })

    test('state flags history loading fails', async() => {
        wrapper = initWrapper(StateFlagsHistory, {
            propsData: {
                uuid: 'DUMBLEDORE',
                start: '2019-01-01T12:00:00Z',
                end: '2019-01-09T00:00:00Z',
            },
        })
        wrapper.vm.$notify = jest.fn()
        console.log = jest.fn()

        await flushPromises()
        expect(wrapper.vm.$notify).toHaveBeenCalledWith(wrapper.vm.ERROR_SOMETHING_WENT_WRONG)
    })

    test('computed items', () => {
        const items = wrapper.vm.items.map(item => ({
            ...item,
            start: item.start.format(),
            end: item.end.format(),
        }))

        expect(items).toStrictEqual([
            {
                start: '2019-01-01T12:00:00Z',
                end: '2019-02-02T12:00:00Z',
                group: 1,
                title: 'deleteCandidate (common:since Jan 1, 2019 12:00:00 PM)',
            },
            {
                start: '2019-01-01T12:00:00Z',
                end: '2019-02-02T12:00:00Z',
                group: 2,
                title: 'linked (common:since Jan 1, 2019 12:00:00 PM)',
            },
            {
                start: '2019-01-02T13:00:00Z',
                end: '2019-01-02T15:00:00Z',
                group: 3,
                title: 'serverBlocked (Jan 2, 2019 1:00:00 PM – Jan 2, 2019 3:00:00 PM)',
            },
            {
                start: '2019-01-03T14:00:00Z',
                end: '2019-02-02T12:00:00Z',
                group: 3,
                title: 'serverBlocked (common:since Jan 3, 2019 2:00:00 PM)',
            },
        ])
    })

    test('computed groups', () => {
        expect(wrapper.vm.groups).toEqual([{
            id: 1,
            content: 'deleteCandidate',
        }, {
            id: 2,
            content: 'linked',
        }, {
            id: 3,
            content: 'serverBlocked',
        }])
    })

    test('computed options', () => {
        expect(wrapper.vm.options.min).toEqual(moment('2019-01-01T12:00:00Z').tz('UTC'))
        expect(wrapper.vm.options.max).toEqual(moment('2019-01-09T00:00:00Z').tz('UTC'))
        expect(wrapper.vm.options.start).toEqual(moment('2019-01-01T12:00:00Z').tz('UTC'))
        expect(wrapper.vm.options.end).toEqual(moment('2019-01-09T00:00:00Z').tz('UTC'))
        expect(wrapper.vm.options.groupOrder).toBe('content')
        expect(wrapper.vm.options.type).toBe('range')
    })
})
