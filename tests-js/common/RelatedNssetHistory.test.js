import { NSSET } from 'ferda-test/TestObjects'
import { initWrapper } from 'ferda-test/TestUtils'
import RelatedNssetHistory from 'ferda/common/RelatedNssetHistory.vue'

describe('RelatedNssetHistory', () => {

    test('matches snapshot', () => {
        const wrapper = initWrapper(RelatedNssetHistory, {
            propsData: {
                old_nsset: NSSET,
                new_nsset: NSSET,
            },
        })

        expect(wrapper.element).toMatchSnapshot()
    })

    test('matches snapshot when deleted', () => {
        const wrapper = initWrapper(RelatedNssetHistory, {
            propsData: {
                old_nsset: NSSET,
                new_contact: null,
            },
        })

        expect(wrapper.element).toMatchSnapshot()
    })
})
