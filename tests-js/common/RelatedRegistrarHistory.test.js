import { REGISTRAR_EMPIRE, REGISTRAR_REBELS } from 'ferda-test/TestObjects'
import { initWrapper } from 'ferda-test/TestUtils'
import RelatedRegistrarHistory from 'ferda/common/RelatedRegistrarHistory.vue'

describe('RelatedRegistrarHistory', () => {

    test('matches snapshot', () => {
        const wrapper = initWrapper(RelatedRegistrarHistory, {
            propsData: {
                label: 'Master',
                old_registrar: REGISTRAR_EMPIRE,
                new_registrar: REGISTRAR_REBELS,
            },
        })

        expect(wrapper.element).toMatchSnapshot()
    })

    test('matches snapshot when registrars are same', () => {
        const wrapper = initWrapper(RelatedRegistrarHistory, {
            propsData: {
                label: 'Same master',
                old_registrar: REGISTRAR_EMPIRE,
                new_registrar: REGISTRAR_EMPIRE,
            },
        })

        expect(wrapper.element).toMatchSnapshot()
    })
})
