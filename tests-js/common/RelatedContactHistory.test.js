import { CONTACT_OBIWAN, CONTACT_YODA } from 'ferda-test/TestObjects'
import { initWrapper } from 'ferda-test/TestUtils'
import RelatedContactHistory from 'ferda/common/RelatedContactHistory.vue'

describe('RelatedContactHistory', () => {
    let wrapper

    test('matches snapshot', () => {
        wrapper = initWrapper(RelatedContactHistory, {
            propsData: {
                label: 'Master',
                old_contact: CONTACT_OBIWAN,
                new_contact: CONTACT_YODA,
            },
        })

        expect(wrapper.element).toMatchSnapshot()
    })

    test('matches snapshot when deleted', () => {
        wrapper = initWrapper(RelatedContactHistory, {
            propsData: {
                label: 'Former master',
                old_contact: CONTACT_OBIWAN,
                new_contact: null,
            },
        })

        expect(wrapper.element).toMatchSnapshot()
    })

    test('contacts_differs is falsy', () => {
        wrapper = initWrapper(RelatedContactHistory, {
            propsData: {
                label: 'Master',
                old_contact: CONTACT_OBIWAN,
                new_contact: CONTACT_OBIWAN,
            },
        })

        expect(wrapper.vm.contacts_differ).toBeFalsy()
    })
})
