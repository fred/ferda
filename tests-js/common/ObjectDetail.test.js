import axios from 'axios'
import MockAdapter from 'axios-mock-adapter'
import flushPromises from 'flush-promises'

import { CONTACT_OBIWAN, STATE_FLAGS } from 'ferda-test/TestObjects'
import { getClonedStoreOptions, initWrapper } from 'ferda-test/TestUtils'
import { OBJECT_TYPE } from 'ferda/RegistryMixin'
import { URLS } from 'ferda/Urls'
import ObjectDetail from 'ferda/common/ObjectDetail.vue'

describe('ObjectDetail', () => {
    let axios_mock
    let wrapper
    const initial_log = console.log
    let store

    beforeEach(() => {
        axios_mock = new MockAdapter(axios)
        axios_mock.onGet(URLS.log_services()).reply(200, ['Service 1', 'Service 2'])

        jest.useFakeTimers()
        jest.setSystemTime(new Date('2022-06-01T00:00:00Z'))

        store = getClonedStoreOptions()
        store.modules.general.getters.permissions = () => ['ferda.can_set_manual_in_zone']

        wrapper = initWrapper(ObjectDetail, {
            propsData: {
                uuid: CONTACT_OBIWAN.contact_id,
                object_handle: CONTACT_OBIWAN.contact_handle,
                object_type: OBJECT_TYPE.CONTACT,
                object_events: CONTACT_OBIWAN.events,
                has_history: true,
                object_state_flags: null,
                loading: false,
            },
            slots: {
                'object-badges': 'OBJECT_BADGES_SLOT',
                'object-info': 'OBJECT_INFO_SLOT',
                'related-objects': 'RELATED_OBJECTS_SLOT',
                'objects': 'OBJECTS_SLOT',
            },
            mocks: {
                $notify: jest.fn(),
            },
            customStoreOptions: store,
        })
    })

    afterEach(() => {
        console.log = initial_log
    })

    test('matches snapshot', () => {
        expect(wrapper.element).toMatchSnapshot()
    })

    test('matches snapshot - object type DOMAIN with flags', () => {
        store.modules.general.getters.permissions = () => ['ferda.can_set_manual_in_zone']
        wrapper = initWrapper(ObjectDetail, {
            propsData: {
                uuid: CONTACT_OBIWAN.contact_id,
                object_handle: CONTACT_OBIWAN.contact_handle,
                object_type: OBJECT_TYPE.DOMAIN,
                object_events: CONTACT_OBIWAN.events,
                has_history: true,
                object_state_flags: STATE_FLAGS,
                loading: true,
            },
            slots: {
                'object-badges': 'OBJECT_BADGES_SLOT',
                'object-info': 'OBJECT_INFO_SLOT',
                'related-objects': 'RELATED_OBJECTS_SLOT',
                'objects': 'OBJECTS_SLOT',
            },
            mocks: {
                $notify: jest.fn(),
            },
            customStoreOptions: store,
        })
        expect(wrapper.element).toMatchSnapshot()
    })

    test('matches empty snapshot - object type DOMAIN', () => {
        wrapper = initWrapper(ObjectDetail, {
            propsData: {
                uuid: CONTACT_OBIWAN.contact_id,
                object_handle: CONTACT_OBIWAN.contact_handle,
                object_type: OBJECT_TYPE.DOMAIN,
                object_events: CONTACT_OBIWAN.events,
                has_history: true,
                object_state_flags: null,
                loading: false,
            },
            slots: {
                'object-badges': 'OBJECT_BADGES_SLOT',
                'object-info': 'OBJECT_INFO_SLOT',
                'related-objects': 'RELATED_OBJECTS_SLOT',
                'objects': 'OBJECTS_SLOT',
            },
            mocks: {
                $notify: jest.fn(),
            },
        })
        expect(wrapper.element).toMatchSnapshot()
    })

    test('method load_services fails on page load', async() => {
        console.log = jest.fn()
        axios_mock.onGet(URLS.log_services()).reply(500)
        wrapper = initWrapper(ObjectDetail, {
            propsData: {
                uuid: CONTACT_OBIWAN.contact_id,
                object_handle: CONTACT_OBIWAN.contact_handle,
                object_type: OBJECT_TYPE.CONTACT,
                object_events: CONTACT_OBIWAN.events,
                has_history: true,
                object_state_flags: null,
                loading: false,
            },
            slots: {
                'object-badges': 'OBJECT_BADGES_SLOT',
                'object-info': 'OBJECT_INFO_SLOT',
                'related-objects': 'RELATED_OBJECTS_SLOT',
                'objects': 'OBJECTS_SLOT',
            },
            mocks: {
                $notify: jest.fn(),
            },
            customStoreOptions: store,
        })
        await flushPromises()

        expect(wrapper.vm.logger_services).toStrictEqual([])
        expect(wrapper.vm.$notify).toHaveBeenCalledWith(wrapper.vm.ERROR_SOMETHING_WENT_WRONG)
        expect(console.log).toHaveBeenCalledWith('error: notifications:something_went_wrong')
    })

    test('method get_related_log_params returns correct params', () => {
        expect(wrapper.vm.get_related_log_params('id', 'type').toString()).toBe('reference=type.id')
        expect(wrapper.vm.get_related_log_params('id', 'type', 'service').toString())
            .toBe('reference=type.id&service=service')
    })

    test('property unregistered_date has correct values', async() => {
        expect(wrapper.vm.unregistered_date).toBeUndefined() // not specified for CONTACT_OBIWAN

        await wrapper.setProps({ object_events: {
            ...CONTACT_OBIWAN.events, unregistered: { timestamp: '2021-01-01T12:00:00+01:00' }},
        })
        expect(wrapper.vm.unregistered_date).toBe('2021-01-01T12:00:00+01:00')
    })
})
