import { initWrapper } from 'ferda-test/TestUtils'
import HistoryNavigationButtons from 'ferda/common/HistoryNavigationButtons.vue'

const mutations = {
    set_old_history_id: null,
    set_new_history_id: null,
}

describe('HistoryNavigationButtons old', () => {
    let wrapper

    beforeEach(() => {
        mutations.set_old_history_id = jest.fn()
        mutations.set_new_history_id = jest.fn()
        const getters = {
            version_list: () => ['V0', 'V1', 'V2', 'V3', 'V4', 'V5'],
            old_history_id: () => 'V3',
            new_history_id: () => 'V2',
        }
        wrapper = initWrapper(HistoryNavigationButtons, {
            customStoreOptions: {modules: {history: {namespaced: true, getters, mutations}}},
            propsData: {
                old: true,
            },
        })
    })

    test('matches snapshot', () => {
        expect(wrapper.element).toMatchSnapshot()
    })

    test('defaults', () => {
        expect(wrapper.vm.old_idx).toEqual(3)
        expect(wrapper.vm.new_idx).toEqual(2)
        expect(wrapper.vm.lower_limit_idx).toEqual(0)
        expect(wrapper.vm.upper_limit_idx).toEqual(4)
        expect(mutations.set_old_history_id.mock.calls.length).toBe(0)
        expect(mutations.set_new_history_id.mock.calls.length).toBe(0)
    })

    test('click previous', () => {
        const prev_button = wrapper.find('.btn-history-previous')
        prev_button.vm.$emit('click')
        expect(mutations.set_old_history_id.mock.calls.length).toBe(1)
        expect(mutations.set_old_history_id.mock.calls[0][1]).toEqual('V2')
        expect(mutations.set_new_history_id.mock.calls.length).toBe(1)
        expect(mutations.set_new_history_id.mock.calls[0][1]).toEqual('V3')
    })

    test('click next', () => {
        const next_button = wrapper.find('.btn-history-next')
        next_button.vm.$emit('click')
        expect(mutations.set_old_history_id.mock.calls.length).toBe(1)
        expect(mutations.set_old_history_id.mock.calls[0][1]).toEqual('V4')
        expect(mutations.set_new_history_id.mock.calls.length).toBe(1)
        expect(mutations.set_new_history_id.mock.calls[0][1]).toEqual('V5')
    })
})

describe('HistoryNavigationButtons new', () => {
    let wrapper

    beforeEach(() => {
        mutations.set_old_history_id = jest.fn()
        mutations.set_new_history_id = jest.fn()
        const getters = {
            version_list: () => ['V0', 'V1', 'V2', 'V3', 'V4', 'V5'],
            old_history_id: () => 'V3',
            new_history_id: () => 'V2',
        }
        wrapper = initWrapper(HistoryNavigationButtons, {
            customStoreOptions: {modules: {history: {namespaced: true, getters, mutations}}},
            propsData: {
                old: false,
            },
        })
    })

    test('defaults', () => {
        expect(wrapper.vm.old_idx).toEqual(3)
        expect(wrapper.vm.new_idx).toEqual(2)
        expect(wrapper.vm.lower_limit_idx).toEqual(1)
        expect(wrapper.vm.upper_limit_idx).toEqual(5)
        expect(mutations.set_old_history_id.mock.calls.length).toBe(0)
        expect(mutations.set_new_history_id.mock.calls.length).toBe(0)
    })

    test('click previous', () => {
        const prev_button = wrapper.find('.btn-history-previous')
        prev_button.vm.$emit('click')
        expect(mutations.set_new_history_id.mock.calls.length).toBe(1)
        expect(mutations.set_new_history_id.mock.calls[0][1]).toEqual('V1')
        expect(mutations.set_old_history_id.mock.calls.length).toBe(1)
        expect(mutations.set_old_history_id.mock.calls[0][1]).toEqual('V0')
    })

    test('click next', () => {
        const next_button = wrapper.find('.btn-history-next')
        next_button.vm.$emit('click')
        expect(mutations.set_new_history_id.mock.calls.length).toBe(1)
        expect(mutations.set_new_history_id.mock.calls[0][1]).toEqual('V3')
        expect(mutations.set_old_history_id.mock.calls.length).toBe(1)
        expect(mutations.set_old_history_id.mock.calls[0][1]).toEqual('V2')
    })
})
