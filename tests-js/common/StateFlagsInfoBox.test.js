import axios from 'axios'
import MockAdapter from 'axios-mock-adapter'
import flushPromises from 'flush-promises'

import { STATE_FLAGS } from 'ferda-test/TestObjects'
import { initWrapper } from 'ferda-test/TestUtils'
import { OBJECT_TYPE } from 'ferda/RegistryMixin'
import { URLS } from 'ferda/Urls'
import StateFlagsInfoBox from 'ferda/common/StateFlagsInfoBox.vue'

describe('StateFlagsInfoBox', () => {
    let axios_mock
    let wrapper
    const initial_log = console.log

    beforeEach(async() => {
        axios_mock = new MockAdapter(axios)
        axios_mock.onGet(URLS.object_state_history_ajax(OBJECT_TYPE.CONTACT, 'DUMBLEDORE')).reply(500)
        wrapper = initWrapper(StateFlagsInfoBox, {
            propsData: {
                object_type: OBJECT_TYPE.CONTACT,
                object_id: 'HARRY',
                object_state_flags: STATE_FLAGS,
                verbose: false,
            },
            stubs: {
                FontAwesomeIcon: true,
            },
            mocks: {
                $notify: jest.fn(),
            },
        }, false)
        await flushPromises()
    })

    afterEach(() => {
        console.log = initial_log
    })

    test('matches snapshot', () => {
        expect(wrapper.element).toMatchSnapshot()
    })

    test('matches snapshot without flags', async() => {
        await wrapper.setProps({ object_state_flags: null })
        expect(wrapper.element).toMatchSnapshot()
    })

    test('matches snapshot - change flags', async() => {
        await wrapper.setProps({ object_state_flags:  { serverInzoneManual: false }})
        expect(wrapper.element).toMatchSnapshot()
    })

    test('state flags groups - no remaining', () => {
        wrapper.setData({
            state_flags: [
                {
                    active: false,
                    description: 'Description 2',
                    name: 'serverBlocked',
                }, {
                    active: true,
                    description: 'Description 1',
                    name: 'linked',
                }, {
                    active: true,
                    description: 'Description 4',
                    name: 'serverInzoneManual',
                }, {
                    active: true,
                    description: 'Description 3',
                    name: 'unknownFlag',
                },
            ],
        })
        expect(wrapper.vm.state_flags_groups).toStrictEqual([
            [
                { active: true, description: 'common:state_flags_options.linked', name: 'linked' },
                { active: false, description: 'common:state_flags_options.serverBlocked', name: 'serverBlocked' },
            ],
            [
                {
                    active: true,
                    description: 'common:state_flags_options.serverInzoneManual',
                    name: 'serverInzoneManual',
                },
            ],
        ])
    })
})
