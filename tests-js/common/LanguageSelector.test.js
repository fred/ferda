import VueI18Next from '@panter/vue-i18next'
import { createLocalVue, shallowMount } from '@vue/test-utils'
import axios from 'axios'
import MockAdapter from 'axios-mock-adapter'
import flushPromises from 'flush-promises'
import i18next from 'i18next'
import Vuex, { Store } from 'vuex'

import { getClonedStoreOptions } from 'ferda-test/TestUtils'
import { URLS } from 'ferda/Urls'
import LanguageSelector from 'ferda/common/LanguageSelector.vue'

const localVue = createLocalVue()
localVue.use(Vuex)
localVue.use(VueI18Next)

describe('LanguageSelector', () => {
    let axios_mock
    let wrapper
    const { location } = window

    beforeAll(() => {
        delete window.location
        window.location = { reload: jest.fn() }
    })

    afterAll(() => {
        window.location = location
    })

    beforeEach(() => {
        axios_mock = new MockAdapter(axios)
        axios_mock.onPost(URLS.lang_setting(), 204)

        const StoreOptions = getClonedStoreOptions()
        i18next.init({
            lng: StoreOptions.modules.preferences.getters.selected_language(),
            fallbackLng: 'en',
            resources: {
                en: {
                    translation: {
                        'book_name': 'The Tales of Beedle the Bard',
                    },
                },
                cs: {
                    translation: {
                        'book_name': 'Bajky barda Beedleho',
                    },
                },
            },
        })

        wrapper = shallowMount(LanguageSelector, {
            localVue,
            i18n: new VueI18Next(i18next),
            store: new Store(StoreOptions),
        })

        wrapper.vm.$cookies = { get: () => 'This_is_CSRF_token' }
    })

    test('matches snapshot', () => {
        expect(wrapper.element).toMatchSnapshot()
    })

    test('matches snapshot after language change', () => {
        i18next.changeLanguage('cs')
        expect(wrapper.element).toMatchSnapshot()
    })

    test('translation is giving correct results', () => {
        expect(wrapper.vm.$t('book_name')).toBe('The Tales of Beedle the Bard')
        i18next.changeLanguage('cs')
        expect(wrapper.vm.$t('book_name')).toBe('Bajky barda Beedleho')
    })

    test('language change is working and matches snapshot', async() => {
        let request
        axios_mock.onPost(URLS.lang_setting()).reply((config) => {
            request = config
            return [204]
        })

        i18next.changeLanguage = jest.fn()
        wrapper.vm.$store.dispatch = jest.fn()

        wrapper.find('#switch-to-cs').vm.$emit('click')
        await flushPromises()

        // checks if request (changing lang on BE) contains all information and they are correct
        expect(request.data).toBe('language=cs&csrfmiddlewaretoken=This_is_CSRF_token')
        expect(request.headers['X-Requested-With']).toBe('XMLHttpRequest')

        // checks language change on FE
        expect(i18next.changeLanguage).toHaveBeenCalledTimes(1)
        expect(i18next.changeLanguage).toHaveBeenCalledWith('cs')
        expect(wrapper.vm.$store.dispatch).toHaveBeenCalledTimes(1)
        expect(wrapper.vm.$store.dispatch).toHaveBeenCalledWith('preferences/set_selected_language', 'cs')
        expect(window.location.reload).toHaveBeenCalledTimes(1)

        expect(wrapper.element).toMatchSnapshot()
    })

    test('switchLang fails', async() => {
        const default_lang = wrapper.vm.$store.getters['preferences/selected_language']

        axios_mock.onPost(URLS.lang_setting()).reply(500)
        i18next.changeLanguage = jest.fn()
        wrapper.vm.$store.dispatch = jest.fn()
        wrapper.vm.notify_message = jest.fn()
        wrapper.find('#switch-to-cs').vm.$emit('click')

        await flushPromises()
        const lang_after_failure = wrapper.vm.$store.getters['preferences/selected_language']

        expect(wrapper.vm.notify_message).toHaveBeenCalledWith(wrapper.vm.ERROR_SOMETHING_WENT_WRONG)
        expect(default_lang).toEqual(lang_after_failure)
    })
})
