import { REGISTRAR_EMPIRE } from 'ferda-test/TestObjects'
import { initWrapper } from 'ferda-test/TestUtils'
import RelatedRegistrar from 'ferda/common/RelatedRegistrar.vue'

describe('RelatedContact', () => {

    test('matches snapshot', () => {
        const wrapper = initWrapper(RelatedRegistrar, {
            propsData: {
                label: 'Master',
                registrar: REGISTRAR_EMPIRE,
            },
        })

        expect(wrapper.element).toMatchSnapshot()
    })
})
