import { initWrapper } from 'ferda-test/TestUtils'
import TimePickerMenu from 'ferda/common/TimePickerMenu.vue'

describe('TimePickerMenu', () => {
    let wrapper

    test('empty matches snapshot', () => {
        wrapper = initWrapper(TimePickerMenu)
        expect(wrapper.element).toMatchSnapshot()
    })

    test('value matches snapshot', () => {
        wrapper = initWrapper(TimePickerMenu, {
            propsData: {
                value: '01:33:33',
                rules: [],
                label: 'Label',
                name: 'name',
            },
        })
        expect(wrapper.element).toMatchSnapshot()
    })

    test('update_value emitts correctly', () => {
        wrapper = initWrapper(TimePickerMenu, {
            propsData: {
                value: '01:33:33',
                rules: [],
                label: 'Label',
                name: 'name',
                with_time_zone: true,
            },
        })

        wrapper.vm.update_value('02:35:35')

        expect(wrapper.emitted('change')[1][0]).toBe('02:35:35+00:00')
    })

    test('value is displayed correctly', async() => {
        await wrapper.setData({ time: null })
        expect(wrapper.vm.display_value).toBe('00:00:00 UTC')

        await wrapper.setProps({ with_time_zone: false })
        await wrapper.setData({ time: '10:00' })
        expect(wrapper.vm.display_value).toBe('10:00')

        await wrapper.setProps({ with_time_zone: true })
        await wrapper.setData({ time: '10:00' })
        expect(wrapper.vm.display_value).toBe('10:00 UTC')

        await wrapper.setData({ time: undefined })
        expect(wrapper.vm.display_value).toBe('')
    })

    test('value is displayed correctly - without time zone', async() => {
        wrapper = initWrapper(TimePickerMenu, {
            propsData: {
                value: '01:33:33',
                rules: [],
                label: 'Label',
                name: 'name',
            },
        })
        await wrapper.setData({ time: null })
        expect(wrapper.vm.display_value).toBe('00:00:00')
    })

    test('value is displayed correctly - with time zone', async() => {
        wrapper = initWrapper(TimePickerMenu, {
            propsData: {
                value: '01:33:33',
                rules: [],
                label: 'Label',
                name: 'name',
                with_time_zone: true,
            },
        })
        await wrapper.setData({ time: null })
        expect(wrapper.vm.display_value).toBe('00:00:00 UTC')
    })

    test('data is prefilled correctly - without time zone', async() => {
        history.pushState(null, null, '?name=01:33:33')
        wrapper = initWrapper(TimePickerMenu, {
            propsData: {
                value: '01:33:33',
                rules: [],
                label: 'Label',
                name: 'name',
            },
        })

        expect(wrapper.vm.time).toBe('01:33:33')
    })

    test('data is prefilled correctly - with time zone', () => {
        history.pushState(null, null, '?name=01%3A33%3A33%2B01%3A00')
        wrapper = initWrapper(TimePickerMenu, {
            propsData: {
                value: '01:33:33',
                rules: [],
                label: 'Label',
                name: 'name',
                with_time_zone: true,
            },
        })
        expect(wrapper.vm.time).toBe('01:33:33')
    })

    test('change emits correctly - without time zone', () => {
        wrapper = initWrapper(TimePickerMenu, {
            propsData: {
                value: '01:33:33',
                rules: [],
                label: 'Label',
                name: 'name',
            },
        })

        wrapper.vm.update_value('02:35:35')
        expect(wrapper.vm.time).toBe('02:35:35')
        expect(wrapper.emitted('change')[0][0]).toBe('02:35:35')
    })

    test('change emits correctly - with time zone', () => {
        wrapper = initWrapper(TimePickerMenu, {
            propsData: {
                value: '1:33',
                rules: [],
                label: 'Label',
                name: 'name',
                with_time_zone: true,
            },
        })

        wrapper.vm.update_value('2:35')
        expect(wrapper.emitted('change')[0][0]).toBe('2:35+00:00')
    })

    test('method change_time works correctly - input not changed', () => {
        wrapper.vm.update_value = jest.fn()
        wrapper.vm.change_time({ target: { value: '2:35 UTC' }})
        expect(wrapper.vm.update_value).not.toHaveBeenCalled()
    })

    test('method change_time works correctly - invalid input', () => {
        wrapper.vm.update_value = jest.fn()
        wrapper.vm.change_time({ target: { value: 'invalid input' }})
        expect(wrapper.vm.update_value).not.toHaveBeenCalled()
        expect(wrapper.vm.error_messages).toStrictEqual(['common:time_format_expected'])
    })

    test('method change_time works correctly - valid time input', () => {
        wrapper.vm.update_value = jest.fn()
        wrapper.vm.change_time({ target: { value: '15:15:15' }})
        expect(wrapper.vm.update_value).toHaveBeenCalledWith('15:15:15')
    })

    test('method reset_time works correctly', () => {
        expect(wrapper.vm.time).toBe('2:35')

        wrapper.vm.reset_time()

        expect(wrapper.vm.time).toBeUndefined()
        expect(wrapper.emitted('change')[1][0]).toBeNull()
    })
})
