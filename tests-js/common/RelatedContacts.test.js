import axios from 'axios'
import MockAdapter from 'axios-mock-adapter'
import flushPromises from 'flush-promises'

import { initWrapper } from 'ferda-test/TestUtils'
import { OBJECT_TYPE } from 'ferda/RegistryMixin'
import { URLS } from 'ferda/Urls'
import RelatedContacts from 'ferda/common/RelatedContacts.vue'

describe('RelatedContacts condensed', () => {
    let axios_mock
    let wrapper

    beforeEach(async() => {
        axios_mock = new MockAdapter(axios)
        axios_mock.onGet(URLS.object_info_ajax(OBJECT_TYPE.CONTACT, 'OBIWAN')).reply(200, {
            contact_handle: 'OBIWAN',
            name: 'Obiwan Kenobi',
        })
        axios_mock.onGet(URLS.object_info_ajax(OBJECT_TYPE.CONTACT, 'JARJAR')).reply(200, {
            contact_handle: 'JARJAR',
            name: 'Jar Jar Binks',
        })
        wrapper = initWrapper(RelatedContacts, {
            propsData: {
                contacts: [
                    'OBIWAN',
                    'JARJAR',
                ],
                label: 'Administrative contact',
                label_plural: 'Administrative contacts',
                verbose: false,
            },
        })
        await flushPromises()
    })

    test('matches snapshot', () => {
        expect(wrapper.element).toMatchSnapshot()
    })

    test('computed contact_info', () => {
        expect(wrapper.vm.contact_info).toEqual({
            OBIWAN: {
                contact_handle: 'OBIWAN',
                name: 'Obiwan Kenobi',
            },
            JARJAR: {
                contact_handle: 'JARJAR',
                name: 'Jar Jar Binks',
            },
        })
    })
})

describe('RelatedContacts verbose', () => {
    let axios_mock
    let wrapper

    beforeEach(async() => {
        axios_mock = new MockAdapter(axios)
        axios_mock.onGet(URLS.object_info_ajax(OBJECT_TYPE.CONTACT, 'OBIWAN')).reply(200, {
            contact_handle: 'OBIWAN',
            name: 'Obiwan Kenobi',
        })
        axios_mock.onGet(URLS.object_info_ajax(OBJECT_TYPE.CONTACT, 'JARJAR')).reply(200, {
            contact_handle: 'JARJAR',
            name: 'Jar Jar Binks',
        })
        wrapper = initWrapper(RelatedContacts, {
            propsData: {
                contacts: [
                    'OBIWAN',
                    'JARJAR',
                ],
                label: 'Administrative contact',
                label_plural: 'Administrative contacts',
                verbose: true,
            },
        })
        await flushPromises()
    })

    test('matches snapshot', () => {
        expect(wrapper.element).toMatchSnapshot()
    })
})
