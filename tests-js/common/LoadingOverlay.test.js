import { createLocalVue } from '@vue/test-utils'
import flushPromises from 'flush-promises'

import { initWrapper } from 'ferda-test/TestUtils'
import LoadingOverlay from 'ferda/common/LoadingOverlay.vue'
import { sleep } from 'ferda/utils'

const localVue = createLocalVue()

describe('LoadingOverlay', () => {
    let wrapper

    beforeEach(async() => {
        wrapper = initWrapper(LoadingOverlay, {
            localVue,
            propsData: {
                value: false,
            },
        })
        await flushPromises()
    })

    test('has aborted on escape keypress', async() => {
        expect(wrapper.emitted().abort).toBeFalsy()
        wrapper.setProps({ value: true })

        await sleep(10)
        const event = new KeyboardEvent('keyup', { key: 'Escape', keyCode: 27 })
        document.dispatchEvent(event)

        await wrapper.vm.$nextTick()
        expect(wrapper.emitted().abort).toBeTruthy()
    })

    test('has aborted on close button click', async() => {
        expect(wrapper.emitted().abort).toBeFalsy()
        wrapper.setProps({ value: true })
        await wrapper.findComponent({ name: 'v-btn' }).trigger('click')
        expect(wrapper.emitted().abort).toBeTruthy()
    })
})
