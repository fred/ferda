import { MESSAGE_PARAMS } from 'ferda-test/TestObjects'
import { initWrapper  } from 'ferda-test/TestUtils'
import { MESSAGE_TYPE } from 'ferda/MessagesMixin'
import RelatedMessageForm from 'ferda/common/RelatedMessageForm.vue'

describe('RelatedMessageForm', () => {
    let wrapper

    beforeEach(() => {
        wrapper = initWrapper(RelatedMessageForm, {
            propsData: {
                time_begin_from: MESSAGE_PARAMS.time_begin_from,
                time_begin_to: MESSAGE_PARAMS.time_begin_to,
                message_type: MESSAGE_PARAMS.message_type,
                message_subtypes: MESSAGE_PARAMS.message_subtypes,
                message_subtypes_selected: MESSAGE_PARAMS.selected_subtypes,
                message_subtypes_loaded: false,
            },
        })
        wrapper.vm.$refs.form.validate = jest.fn()
    })

    test('empty matches snapshot', () => {
        wrapper = initWrapper(RelatedMessageForm)
        expect(wrapper.element).toMatchSnapshot()
    })

    test('messages matches snapshot', () => {
        expect(wrapper.element).toMatchSnapshot()
    })

    test('method sorted_message_subtypes with data', async() => {
        const sorted_subtypes = [
            { text: 'subtype 2', value: 'subtype_2' },
            { text: 'subtype 3', value: 'subtype_3' },
            { text: 'test 1', value: 'subtype_1' },
        ]
        await wrapper.setProps({ message_subtypes: [
            { label: 'subtype 2', name: 'subtype_2', tags: ['email'] },
            { label: 'test 1', name: 'subtype_1', tags: ['email'] },
            { label: 'subtype 3', name: 'subtype_3', tags: ['email'] },
        ]})
        expect(wrapper.vm.sorted_message_subtypes).toStrictEqual(sorted_subtypes)
    })

    test('method submit_search emits correctly - form is valid', () => {
        wrapper.vm.submit_search(MESSAGE_PARAMS)

        expect(wrapper.emitted('submit-search')[0][0]).toStrictEqual({
            selected_subtypes: MESSAGE_PARAMS.selected_subtypes,
            message_type: MESSAGE_TYPE.EMAIL,
            time_begin_from: MESSAGE_PARAMS.time_begin_from,
            time_begin_to: MESSAGE_PARAMS.time_begin_to,
        })
    })

    test('method submit_search emits correctly - form is invalid', async() => {
        await wrapper.setData({ is_valid: false })
        wrapper.vm.submit_search()
        expect(wrapper.emitted('submit-search')).toBeFalsy()
    })

    test('method update_message_subtypes emits correctly', () => {
        wrapper.vm.update_message_subtypes(MESSAGE_TYPE.EMAIL)
        expect(wrapper.emitted('update:message_subtypes_selected')[0][0]).toStrictEqual([])
        expect(wrapper.emitted('update:message_type')[0][0]).toEqual(MESSAGE_TYPE.EMAIL)
    })

    test('method update_message_subtypes - type is null', () => {
        wrapper.vm.update_message_subtypes(null)
        expect(wrapper.emitted('update:message_type')).toBeFalsy()
    })

    test('validation is triggered - time_begin_from', async() => {
        await wrapper.setProps({ time_begin_from: '2021-01-31T12:00:00+01:00' })
        await wrapper.vm.$nextTick()
        expect(wrapper.vm.$refs.form.validate).toHaveBeenCalledTimes(1)
    })

    test('validation is triggered - time_begin_to', async() => {
        await wrapper.setProps({ time_begin_to: '2021-02-31T12:00:00+01:00' })
        await wrapper.vm.$nextTick()
        expect(wrapper.vm.$refs.form.validate).toHaveBeenCalledTimes(1)
    })
})
