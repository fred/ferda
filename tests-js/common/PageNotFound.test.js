import { initWrapper } from 'ferda-test/TestUtils'
import PageNotFound from 'ferda/common/PageNotFound.vue'

describe('PageNotFound', () => {
    let wrapper

    beforeEach(() => {
        wrapper = initWrapper(PageNotFound)
    })

    test('matches snapshot', () => {
        expect(wrapper.element).toMatchSnapshot()
    })
})
