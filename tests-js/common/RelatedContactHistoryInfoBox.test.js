import { CONTACT_OBIWAN } from 'ferda-test/TestObjects'
import { initWrapper } from 'ferda-test/TestUtils'
import RelatedContactHistoryInfoBox from 'ferda/common/RelatedContactHistoryInfoBox.vue'

describe('RelatedContactHistoryInfoBox', () => {
    let wrapper

    test('matches snapshot', () => {
        wrapper = initWrapper(RelatedContactHistoryInfoBox, {
            propsData: {
                label: 'Owner',
                contact: CONTACT_OBIWAN,
            },
        })

        expect(wrapper.element).toMatchSnapshot()
    })
})
