import { initWrapper } from 'ferda-test/TestUtils'
import RelatedContactsHistory from 'ferda/common/RelatedContactsHistory.vue'

const old_contact = {
    contact_handle: 'OBIWAN',
    organization: 'Jedi Order',
    name: 'Obiwan Kenobi',
    publish: {organization: true, name: true},
    events: {},
}

const new_contact = {
    contact_handle: 'OBIWAN',
    organization: 'Rebel alliance',
    name: 'Obiwan Kenobi',
    publish: {organization: true, name: false},
    events: {},
}

describe('RelatedContactsHistory', () => {
    test('matches snapshot', () => {
        const wrapper = initWrapper(RelatedContactsHistory, {
            propsData: {
                label: 'Master',
                contacts: [{old_value: old_contact, new_value: new_contact, differs: true}],
            },
        })

        expect(wrapper.element).toMatchSnapshot()
    })

    test('matches snapshot when deleted', () => {
        const wrapper = initWrapper(RelatedContactsHistory, {
            propsData: {
                label: 'Former master',
                contacts: [{old_value: old_contact, new_value: null, differs: true}],
            },
        })

        expect(wrapper.element).toMatchSnapshot()
    })
})
