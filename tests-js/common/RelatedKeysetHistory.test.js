import { KEYSET } from 'ferda-test/TestObjects'
import { initWrapper } from 'ferda-test/TestUtils'
import RelatedKeysetHistory from 'ferda/common/RelatedKeysetHistory.vue'

describe('RelatedKeysetHistory', () => {

    test('matches snapshot', () => {
        const wrapper = initWrapper(RelatedKeysetHistory, {
            propsData: {
                label: 'Master',
                old_keyset: KEYSET,
                new_keyset: KEYSET,
            },
        })

        expect(wrapper.element).toMatchSnapshot()
    })

    test('matches snapshot when deleted - new', () => {
        const wrapper = initWrapper(RelatedKeysetHistory, {
            propsData: {
                label: 'Former master',
                old_keyset: KEYSET,
                new_keyset: null,
            },
        })

        expect(wrapper.element).toMatchSnapshot()
    })

    test('matches snapshot when deleted - old', () => {
        const wrapper = initWrapper(RelatedKeysetHistory, {
            propsData: {
                label: 'Former master 2',
                old_keyset: null,
                new_keyset: KEYSET,
            },
        })

        expect(wrapper.element).toMatchSnapshot()
    })
})
