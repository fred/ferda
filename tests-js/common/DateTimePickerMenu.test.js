import flushPromises from 'flush-promises'

import { initWrapper } from 'ferda-test/TestUtils'
import DateTimePickerMenu from 'ferda/common/DateTimePickerMenu.vue'

describe('DateTimePickerMenu', () => {
    let wrapper
    const original_console = console.log

    beforeEach(() => {
        jest.useFakeTimers()
        jest.setSystemTime(new Date('2020-01-01T12:00:00+00:00'))
        wrapper = initWrapper(DateTimePickerMenu, {
            propsData: { value: '2021-01-01T12:33:33', label: 'Label', rules: [], name: 'name' },
        })
    })

    afterEach(() => {
        jest.runOnlyPendingTimers()
        jest.useRealTimers()
        console.log = original_console
    })

    test('empty matches snapshot', () => {
        wrapper = initWrapper(DateTimePickerMenu, { propsData: { value: null, label: null, rules: [], name: null }})
        expect(wrapper.element).toMatchSnapshot()
    })

    test('matches snapshot', () => {
        expect(wrapper.element).toMatchSnapshot()
    })

    test('save emits correctly', async() => {
        await wrapper.setData({ date: '2021-04-22', time: '20:50:54', date_time: '22. dub 2021 20:50:54' })
        wrapper.vm.save()
        expect(wrapper.emitted('change')[1][0]).toBe('2021-04-22T20:50:54')

        await wrapper.setProps({ with_time_zone: true })
        wrapper.vm.save()
        expect(wrapper.emitted('change')[2][0]).toBe('2021-04-22T20:50:54Z')
    })

    test('hours are selected by default', async() => {
        await wrapper.setData({ date: '2021-01-01', time: '11:11:11' })
        await wrapper.vm.open()

        /**
         * Possible values: 1, 2 or 3
         * 1 = Hours option is focused
         * 2 = Minutes option is focused
         * 3 = Seconds option is focused
         */
        expect(wrapper.vm.$refs.time_picker.selecting).toBe(1)
    })

    test('method open prefills default values correctly', async() => {
        await wrapper.setData({ date: null, time: null })
        await wrapper.vm.open()
        expect(wrapper.vm.date).toBe('2020-01-01')
        expect(wrapper.vm.time).toBe('00:00:00')
    })

    test('data is prefilled correctly', async() => {
        const url = '?timestamp=2021-01-01 11:55:49'
        history.pushState(null, null, url)

        wrapper = initWrapper(DateTimePickerMenu, {
            propsData: {
                label: 'Label',
                rules: [],
                name: 'timestamp',
                with_time_zone: true,
            },
        })
        await flushPromises()

        expect(wrapper.vm.date_time).toBe('1. Jan 2021 11:55:49')
    })

    test('value is null', () => {
        wrapper = initWrapper(DateTimePickerMenu, {
            propsData: {
                value: null,
            },
        })
        expect(wrapper.vm.display_value).toBe('')
    })

    test('value is not null', async() =>{
        await wrapper.setProps({ value: '2021-10-24T06:28:30+02:00' })
        expect(wrapper.vm.date).toStrictEqual('2021-10-24')
        expect(wrapper.vm.time).toStrictEqual('04:28:30')
    })

    test('value is displayed correctly without time zone', () => {
        wrapper = initWrapper(DateTimePickerMenu, {
            propsData: {
                value: '2021-10-24T06:28:30+02:00',
            },
        })
        expect(wrapper.vm.display_value).toBe('Oct 24, 2021 4:28:30 AM')
    })

    test('value is displayed correctly with time zone', () => {
        wrapper = initWrapper(DateTimePickerMenu, {
            propsData: {
                value: '2021-10-24T06:28:30+02:00',
                with_time_zone: true,
            },
        })
        expect(wrapper.vm.display_value).toBe('24. Oct 2021 04:28:30 UTC')
    })

    test('change emits correctly', async() => {
        await wrapper.setData({ date: '2021-01-01', time: '08:42:44' })
        await wrapper.vm.save()
        expect(wrapper.emitted('change')[1][0]).toBe('2021-01-01T08:42:44')
    })

    test('change emits correctly - with mount_date', () => {
        wrapper = initWrapper(DateTimePickerMenu, {
            propsData: {
                mount_date: '2022-03-24T06:28:30+02:00',
            },
        })
        expect(wrapper.emitted('change')[0][0]).toBe('2022-03-24T06:28:30+02:00')
    })

    test('datetime string is correct', () => {
        expect(wrapper.vm.get_datetime_string('2021-01-01', '15:15:15', true))
            .toBe('2021-01-01T15:15:15Z')
        expect(wrapper.vm.get_datetime_string('2021-01-01', '15:15:15', false))
            .toBe('2021-01-01T15:15:15')
    })

    test('method prefill_now_datetime prefills widget correctly', () => {
        wrapper = initWrapper(DateTimePickerMenu, {
            propsData: {
                value: null,
                label: null,
                rules: [],
                name: null,
            },
        })
        wrapper.vm.prefill_now_datetime()
        expect(wrapper.vm.date).toBe('2020-01-01')
        expect(wrapper.vm.time).toBe('12:00:00')
    })

    test('value watcher works correctly', async() => {
        await wrapper.setProps({ value: null })
        expect(wrapper.emitted('change')[1][0]).toStrictEqual(null)
        expect(wrapper.vm.date).toStrictEqual(null)
        expect(wrapper.vm.time).toStrictEqual(null)
        expect(wrapper.vm.date_time).toStrictEqual(null)
    })

    test('method update_value updates values correctly', () => {
        wrapper.vm.update_value('2022-01-01', 'date')
        expect(wrapper.vm.date).toBe('2022-01-01')

        wrapper.vm.update_value('15:15:15', 'time')
        expect(wrapper.vm.time).toBe('15:15:15')
    })

    test('method change_date works correctly - empty date', () => {
        wrapper.vm.validate_datetime_input = jest.fn(() => null)
        wrapper.vm.change_date({ target: { value: '2021-01-01T12:00:00' }})
        expect(wrapper.emitted('update:value')[0][0]).toStrictEqual(null)
        expect(wrapper.emitted('change')[0][0]).toStrictEqual('2020-01-01T00:00:00')
    })

    test('method change_date works correctly - input not changed', () => {
        wrapper.vm.validate_datetime_input = jest.fn(() => false)
        wrapper.vm.change_date({ target: { value: '2021-01-01' }})
        expect(wrapper.emitted('update:value')).toBeFalsy()
        expect(wrapper.emitted('change')[0][0]).toBe('2020-01-01T00:00:00') // original value
    })

    test('method change_date works correctly - invalid input', () => {
        const errors = ['test errror']
        wrapper.vm.validate_datetime_input = jest.fn(() => errors)
        wrapper.vm.change_date({ target: { value: 'invalid input' }})
        expect(wrapper.emitted('update:value')).toBeFalsy()
        expect(wrapper.emitted('change')[0][0]).toBe('2020-01-01T00:00:00') // original value
        expect(wrapper.vm.error_messages).toStrictEqual(errors)
    })

    test('method change_date works correctly - valid iso input', () => {
        wrapper.vm.validate_datetime_input = jest.fn(() => '2021-01-01T13:00:00')
        wrapper.vm.change_date({ target: { value: '2021-01-01T13:00:00' }})
        expect(wrapper.emitted('update:value')[0][0]).toBe('2021-01-01T13:00:00')
        expect(wrapper.emitted('change')[1][0]).toBe('2021-01-01T13:00:00')
    })

    test('method reset_date emits correctly', () => {
        wrapper.vm.reset_date()

        expect(wrapper.emitted('update:value')[0][0]).toBeNull()
        expect(wrapper.emitted('change')[1][0]).toBeNull()
    })
})
