import { KEYSET } from 'ferda-test/TestObjects'
import { initWrapper } from 'ferda-test/TestUtils'
import RelatedKeyset from 'ferda/common/RelatedKeyset.vue'

describe('RelatedKeyset', () => {

    test('matches snapshot', () => {
        const wrapper = initWrapper(RelatedKeyset, {
            propsData: {
                label: 'Label keyset',
                keyset: KEYSET,
            },
        })

        expect(wrapper.element).toMatchSnapshot()
    })
})
