import VueI18Next from '@panter/vue-i18next'
import { createLocalVue } from '@vue/test-utils'
import i18next from 'i18next'

import { getClonedStoreOptions, initWrapper } from 'ferda-test/TestUtils'
import { URLS } from 'ferda/Urls'
import AppHeader from 'ferda/common/AppHeader.vue'

const localVue = createLocalVue()
localVue.use(VueI18Next)

describe('AppHeader', () => {
    let wrapper

    beforeEach(() => {
        jest.useFakeTimers()
        jest.setSystemTime(new Date('2020-01-01T12:00:00+00:00'))
        wrapper = initWrapper(AppHeader, {
            stubs: {
                notifications: true,
            },
            mocks: {
                '$cookies': {
                    get: (name) => {
                        if (name === 'session_expires') return '"2020-01-01T12:35:11+00:00"'
                        if (name === 'server_time') return '"2020-01-01T12:05:00+00:00"'
                        return null
                    },
                },
            },
        })
    })

    afterEach(() => {
        jest.runOnlyPendingTimers()
        jest.useRealTimers()
    })

    test('matches snapshot', () => {
        expect(wrapper.element).toMatchSnapshot()
    })

    test('staff user matches snapshot', () => {
        const store = getClonedStoreOptions()
        store.modules.general.getters.user = () => ({
            username: 'rimmer',
            first_name: 'Arnold',
            last_name: 'Rimmer',
            is_staff: true,
        })
        wrapper = initWrapper(AppHeader, {
            stubs: {notifications: true},
            customStoreOptions: store,
        })
        expect(wrapper.element).toMatchSnapshot()
    })

    test('form actions are called', () => {
        wrapper.vm.$store.dispatch = jest.fn()

        wrapper.vm.show_search_form_switch()
        wrapper.vm.verbose_switch()
        wrapper.vm.set_show_drawer(true)

        expect(wrapper.vm.$store.dispatch).toHaveBeenNthCalledWith(
            1, 'preferences/set_show_search_form', !wrapper.vm.show_search_form,
        )
        expect(wrapper.vm.$store.dispatch).toHaveBeenNthCalledWith(
            2, 'preferences/set_verbose', !wrapper.vm.verbose,
        )
        expect(wrapper.vm.$store.dispatch).toHaveBeenNthCalledWith(
            3, 'preferences/set_show_drawer', true,
        )
    })

    test('get_server_time_difference() - cookie is invalid', () => {
        wrapper = initWrapper(AppHeader, {
            stubs: {
                notifications: true,
            },
            mocks: {
                '$cookies': null,
            },
        })

        expect(wrapper.vm.get_server_time_difference()).toEqual(null)
    })
})

describe('Appheader indentation and colorization', () => {
    let wrapper
    const { location } = window

    beforeEach(() => {
        wrapper = initWrapper(AppHeader, { stubs: { notifications: true }})
    })

    afterEach(() => {
        window.location = location
    })

    test('method is_active returns correct results - registrar list', () => {
        delete window.location
        window.location = { pathname: URLS.registrar_list() }
        wrapper = initWrapper(AppHeader, { stubs: { notifications: true }})

        expect(wrapper.vm.is_active('registrars')).toStrictEqual(true)
    })

    test('method is_active returns correct results - registry search', () => {
        delete window.location
        window.location = { pathname: URLS.registry_search() }
        wrapper = initWrapper(AppHeader, { stubs: { notifications: true }})

        expect(wrapper.vm.is_active('registry')).toStrictEqual(true)
    })

    test('method is_active returns correct results - other item', () => {
        wrapper = initWrapper(AppHeader, { stubs: { notifications: true }})

        expect(wrapper.vm.is_active('registry')).toStrictEqual(false)
    })
})

describe('AppHeader time_until_logout', () => {
    let wrapper

    beforeEach(() => {
        jest.useFakeTimers()
        jest.setSystemTime(new Date('2020-01-01T12:00:00+00:00'))
    })

    afterEach(() => {
        jest.runOnlyPendingTimers()
        jest.useRealTimers()
    })

    /**
     * Return properties with inserted time for wrapper.
     *
     * @param {string} fullTimeString - time in long string form that will be displayed.
     * @returns {Object}
     */
    const addPropertiesWithTime = (fullTimeString) => {
        const i18nextInstance = i18next.createInstance()
        i18nextInstance.init({
            compatibilityJSON: 'v3',
            lng: 'en',
            fallbackLng: 'cs',
            ns: ['common'],
            defaultNS: 'common',
            resources: {
                en: {
                    common: {
                        'days': {
                            key: '{{count}} day',
                            key_plural: '{{count}} days',
                        },
                    },
                },
                cs: {
                    common: {
                        'days': {
                            key_0: '{{count}} den',
                            key_1: '{{count}} dny',
                            key_2: '{{count}} dnů',
                        },
                    },
                },
            }})
        return {
            localVue,
            i18n: new VueI18Next(i18nextInstance),
            stubs: {
                notifications: true,
            },
            mocks: {
                '$cookies': {
                    get: (name) => {
                        if (name === 'session_expires') return fullTimeString
                        if (name === 'server_time') return '"2020-01-01T00:00:00+00:00"'
                        return null
                    },
                },
            },
        }
    }

    test('Display localized string of one day', () => {
        wrapper = initWrapper(AppHeader, addPropertiesWithTime('"2020-02-02T01:23:45+00:00"'))
        expect(wrapper.vm.time_until_logout).toBe('1 day 01:23:45')
    })

    test('Hide zero days', () => {
        wrapper = initWrapper(AppHeader, addPropertiesWithTime('"2020-01-01T02:01:23+00:00"'))
        expect(wrapper.vm.time_until_logout).toBe('2:01:23')
    })

    test('Show zero minutes and padded seconds', () => {
        wrapper = initWrapper(AppHeader, addPropertiesWithTime('"2020-01-01T00:00:09+00:00"'))
        expect(wrapper.vm.time_until_logout).toBe('0:09')
    })

    test('Display 8 days in cs', () => {
        wrapper = initWrapper(AppHeader, addPropertiesWithTime('"2020-02-09T01:08:09+00:00"'))
        wrapper.vm.$i18n.i18next.changeLanguage('cs')
        expect(wrapper.vm.time_until_logout).toBe('8 dnů 01:08:09')
    })

    test('matches snapshot', done => {
        wrapper = initWrapper(AppHeader, addPropertiesWithTime('"2020-02-03T23:58:59+00:00"'))
        wrapper.vm.$nextTick(() => {
            expect(wrapper.element).toMatchSnapshot()
            done()
        })
    })
})

describe('AppHeader automatic logout', () => {
    const { location } = window

    beforeEach(() => {
        delete window.location
        window.location = {
            replace: jest.fn(),
            pathname: '/some/path/',
            search: '?query=whatever',
        }
        initWrapper(AppHeader, {
            stubs: {
                notifications: true,
            },
            mocks: {
                '$cookies': {
                    get: () => '"2020-01-01T11:59:59+00:00"',
                },
            },
        })
    })

    afterEach(() => {
        window.location = location
    })

    test('automatic logout', () => {
        expect(window.location.replace).toHaveBeenCalledWith(`${URLS.logout_url()}?next=/some/path/%3Fquery%3Dwhatever`)
    })
})
