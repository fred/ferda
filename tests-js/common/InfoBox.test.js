import { initWrapper } from 'ferda-test/TestUtils'
import InfoBox from 'ferda/common/InfoBox.vue'

describe('InfoBox default', () => {
    let wrapper

    beforeEach(() => {
        wrapper = initWrapper(InfoBox, {
            propsData: {
                label: 'Personal data',
                public_access: true,
                enable_click: false,
            },
            stubs: {
                FontAwesomeIcon: true,
            },
        }, false)
    })

    test('matches snapshot', () => {
        expect(wrapper.element).toMatchSnapshot()
    })

    test('defaults', () => {
        expect(wrapper.vm.show_items_access).toBe(true)
        expect(wrapper.vm.show_box_access).toBe(false)
    })

    test('has proper header', () => {
        const header = wrapper.find('.col')
        expect(header.text()).toContain('Personal data')
        expect(header.findAll('span.access').length).toBe(0)
    })
})

describe('InfoBox with access displayed', () => {
    let wrapper

    beforeEach(() => {
        wrapper = initWrapper(InfoBox, {
            propsData: {
                label: 'Personal data',
                public_access: false,
                show_box_access: true,
                enable_click: false,
            },
            stubs: {
                FontAwesomeIcon: true,
            },
        }, false)
    })

    test('matches snapshot', () => {
        expect(wrapper.element).toMatchSnapshot()
    })

    test('has proper header', () => {
        const header = wrapper.find('.col')
        expect(header.text()).toContain('Personal data')
        expect(header.find('.access').classes()).toContain('private')
    })

    test('method toggle_box does not emit - enable_click is false', () => {
        wrapper.vm.toggle_box()
        expect(wrapper.emitted('toggle-box')).toBeFalsy()
    })

    test('method toggle_box emits - enable_click is true', () => {
        wrapper = initWrapper(InfoBox, {
            propsData: {
                label: 'Personal data',
                public_access: false,
                show_box_access: true,
                enable_click: true,
            },
            stubs: {
                FontAwesomeIcon: true,
            },
        }, false)
        wrapper.vm.toggle_box()
        expect(wrapper.emitted('toggle-box')).toBeTruthy()
    })
})
