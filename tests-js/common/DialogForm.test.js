import { initWrapper } from 'ferda-test/TestUtils'
import DialogForm from 'ferda/common/DialogForm.vue'

describe('DialogForm', () => {
    let wrapper

    beforeEach(() => {
        wrapper = initWrapper(DialogForm, {
            propsData: {
                width: 1000,
                dialog_opened: true,
                title: 'Test dialog',
            },
        })
    })

    test('matches snapshot', () => {
        expect(wrapper.element).toMatchSnapshot()
    })

    test('matches empty snapshot', () => {
        wrapper = initWrapper(DialogForm)
        expect(wrapper.element).toMatchSnapshot()
    })
})
