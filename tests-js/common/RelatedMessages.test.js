import axios from 'axios'
import MockAdapter from 'axios-mock-adapter'
import flushPromises from 'flush-promises'

import { MESSAGE_PARAMS, MESSAGE_REQUEST } from 'ferda-test/TestObjects'
import { initWrapper } from 'ferda-test/TestUtils'
import { MESSAGE_TYPE } from 'ferda/MessagesMixin'
import { OBJECT_TYPE } from 'ferda/RegistryMixin'
import { URLS } from 'ferda/Urls.js'
import RelatedMessages from 'ferda/common/RelatedMessages.vue'

describe('RelatedMessages', () => {
    let wrapper
    let axios_mock
    const initial_log = console.log
    const initial_search = window.location.search

    beforeEach(async() => {
        jest.useFakeTimers()
        jest.setSystemTime(new Date('2020-01-01T12:00:00+00:00'))
        axios_mock = new MockAdapter(axios)
        axios_mock.onGet(URLS.message_list(MESSAGE_TYPE.EMAIL)).reply(200, MESSAGE_REQUEST)
        axios_mock.onGet(URLS.message_subtypes(MESSAGE_TYPE.EMAIL)).reply(200, MESSAGE_PARAMS.message_subtypes)

        wrapper = initWrapper(RelatedMessages, {
            propsData: {
                uuid: '00000000-0000-0000-0000-000000000001',
                object_type: OBJECT_TYPE.CONTACT,
            },
            mocks: { $notify: jest.fn() },
        })
        await flushPromises()
    })

    afterEach(() => {
        delete window.location
        window.location = { search: initial_search }
        console.log = initial_log
        jest.runOnlyPendingTimers()
        jest.useRealTimers()
    })

    test('matches snapshot', () => {
        expect(wrapper.element).toMatchSnapshot()
    })

    test('messages section is loaded when particular URL param is present', async() => {
        delete window.location
        window.location = { search: '?emails=true' }

        wrapper = initWrapper(RelatedMessages, {
            propsData: { uuid: '00000000-0000-0000-0000-000000000001', object_type: OBJECT_TYPE.CONTACT },
            mocks: { $notify: jest.fn() },
        })
        await flushPromises()

        expect(wrapper.vm.messages_shown).toStrictEqual(true)
        expect(wrapper.vm.messages.length).not.toBe(0)
    })

    test('computed variable search_result_page works correctly', async() => {
        await wrapper.setData({ messages: [], current_page: 1, page_size: 10 })

        expect(wrapper.vm.search_results_page).toStrictEqual(wrapper.vm.messages)
    })

    test('method submit_search fetches data correctly', async() => {
        await wrapper.vm.submit_search(null, false, true)
        expect(wrapper.vm.message_params).toStrictEqual({
            message_type: MESSAGE_TYPE.EMAIL,
            references: `${OBJECT_TYPE.CONTACT}.00000000-0000-0000-0000-000000000001`,
            selected_subtypes: [],
            message_subtypes: MESSAGE_PARAMS.message_subtypes,
            time_begin_from: '2020-01-01T00:00:00+00:00',
            time_begin_to: '2020-02-01T00:00:00+00:00',
        })
    })

    test('method submit_search fetches data correctly with params', async() => {
        await wrapper.setData({ last_search_message_type: MESSAGE_TYPE.SMS, messages: [] })
        await wrapper.vm.submit_search({
            message_type: MESSAGE_TYPE.EMAIL,
            selected_subtypes: MESSAGE_PARAMS.selected_subtypes,
            time_begin_from: MESSAGE_PARAMS.time_begin_from,
            time_begin_to:  MESSAGE_PARAMS.time_begin_to,
        }, null, false, true)

        expect(wrapper.vm.message_params).toStrictEqual({
            message_type: MESSAGE_TYPE.EMAIL,
            selected_subtypes: MESSAGE_PARAMS.selected_subtypes,
            message_subtypes: MESSAGE_PARAMS.message_subtypes,
            references: `${OBJECT_TYPE.CONTACT}.00000000-0000-0000-0000-000000000001`,
            time_begin_from: MESSAGE_PARAMS.time_begin_from,
            time_begin_to:  MESSAGE_PARAMS.time_begin_to,
        })
    })

    test('method submit_search fails', async() => {
        console.log = jest.fn()
        delete window.history
        window.history = { replaceState: jest.fn() }
        axios_mock.onGet(URLS.message_list(MESSAGE_TYPE.EMAIL)).reply(500)
        await wrapper.vm.submit_search()

        expect(wrapper.vm.messages).toStrictEqual(null)
        expect(wrapper.vm.$notify).toHaveBeenCalledWith(wrapper.vm.ERROR_SOMETHING_WENT_WRONG)
        expect(console.log).toHaveBeenCalledWith('error: notifications:something_went_wrong')
    })

    test('method submit_search is aborted', async() => {
        console.log = jest.fn()
        wrapper.vm.submit_search()
        await wrapper.vm.stop_search()

        expect(wrapper.vm.messages).toStrictEqual(null)
        expect(wrapper.vm.$notify).toHaveBeenNthCalledWith(1, { clean: true })
        expect(wrapper.vm.$notify).toHaveBeenNthCalledWith(2, wrapper.vm.INFO_REQUEST_ABORTED)
        expect(console.log).toHaveBeenCalledWith('info: notifications:search_aborted')
    })

    test('method change_page_size changes values correctly', async() => {
        wrapper.vm.submit_search = jest.fn()
        await wrapper.setData({ current_page: 3 })
        expect(wrapper.vm.current_page).toBe(3)
        expect(wrapper.vm.page_size).toBe(wrapper.vm.$store.getters['settings/PAGINATION_PAGE_SIZE'])

        await wrapper.vm.change_page_size(5)
        expect(wrapper.vm.current_page).toBe(1)
        expect(wrapper.vm.page_size).toBe(5)
        expect(wrapper.vm.submit_search).toHaveBeenCalledWith(null, null, true)
    })

    test('method update_message_subtypes works correctly', async() => {
        expect(wrapper.vm.message_params.message_subtypes).toStrictEqual(MESSAGE_PARAMS.message_subtypes)
        axios_mock.onGet(URLS.message_subtypes(MESSAGE_TYPE.EMAIL)).reply(200, ['subtype_1', 'subtype_2', 'subtype_3'])
        await wrapper.vm.update_message_subtypes(MESSAGE_TYPE.EMAIL, true)
        expect(wrapper.vm.message_params.message_subtypes).toStrictEqual(['subtype_1', 'subtype_2', 'subtype_3'])
    })

    test('method update_message_subtypes fails', async() => {
        console.log = jest.fn()
        expect(wrapper.vm.message_params.message_subtypes).toStrictEqual(MESSAGE_PARAMS.message_subtypes)
        axios_mock.onGet(URLS.message_subtypes(MESSAGE_TYPE.EMAIL)).reply(500)
        await wrapper.vm.update_message_subtypes(MESSAGE_TYPE.EMAIL)
        expect(wrapper.vm.message_params.message_subtypes).toStrictEqual(MESSAGE_PARAMS.message_subtypes)
        expect(console.log).toHaveBeenCalledWith('error: notifications:something_went_wrong')
        expect(wrapper.vm.$notify).toHaveBeenCalledWith(wrapper.vm.ERROR_SOMETHING_WENT_WRONG)
    })

    test('method change current page changes data correctly', async() => {
        expect(wrapper.vm.current_page).toBe(1)
        wrapper.vm.change_current_page(2)
        expect(wrapper.vm.current_page).toBe(2)
    })

    test('method scroll_top works is correctly', async() => {
        wrapper.vm.scroll_top()
        expect(wrapper.vm.messages_shown).toStrictEqual(true)
        wrapper.vm.scroll_top()
        expect(wrapper.vm.messages_shown).toStrictEqual(false)
    })

    test('message_type is null', async() => {
        wrapper.vm.update_message_subtypes = jest.fn()
        await wrapper.setData({ message_params: { message_type: MESSAGE_TYPE.SMS }})
        expect(await wrapper.vm.update_message_subtypes).toHaveBeenCalledWith(MESSAGE_TYPE.SMS)
    })

    test('date range has been reset - time_begin_from', async() => {
        wrapper.vm.get_date_range = jest.fn(() => '2021-02-01T00:00:00+00:00')
        await wrapper.setData({ message_params: { time_begin_from: '2022-02-01T00:00:00+00:00' }})
        expect(wrapper.vm.active_date_range).toStrictEqual(null)
    })

    test('date range has been reset - time_begin_to', async() => {
        wrapper.vm.get_date_range = jest.fn(() => '2021-02-01T00:00:00+00:00')
        await wrapper.setData({ message_params: { time_begin_to: '2022-02-01T00:00:00+00:00' }})
        expect(wrapper.vm.active_date_range).toStrictEqual(null)
    })
})
