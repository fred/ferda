import { initWrapper } from 'ferda-test/TestUtils'
import { DatetimeMixin } from 'ferda/DatetimeMixin'
import DateRangePicker from 'ferda/common/DateRangePicker.vue'

describe('DateRangePicker', () => {
    let wrapper

    beforeEach(() => {
        wrapper = initWrapper(DateRangePicker, {
            propsData: {
                active_date_range: null,
            },
            mixins: [DatetimeMixin],
        })
    })

    test('matches snapshot - active date range', () => {
        wrapper.setProps({ active_date_range: wrapper.vm.DATE_RANGE_LIST[0].value })
        expect(wrapper.element).toMatchSnapshot()
    })

    test('matches snapshot without props', () => {
        wrapper = initWrapper(DateRangePicker)
        expect(wrapper.element).toMatchSnapshot()
    })
})
