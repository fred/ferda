import { initWrapper } from 'ferda-test/TestUtils'
import HistoryNavigation from 'ferda/common/HistoryNavigation.vue'

const mutations = {
    set_old_history_id: null,
    set_new_history_id: null,
}

describe('HistoryNavigation', () => {
    let wrapper

    beforeEach(() => {
        mutations.set_old_history_id = jest.fn()
        mutations.set_new_history_id = jest.fn()
        wrapper = initWrapper(HistoryNavigation, {
            propsData: {
                versions: {
                    'V1': {valid_from: '2000-01-01T13:00:00-02:00', valid_to: '2001-01-01T13:00:00Z'},
                    'V2': {valid_from: '2001-01-01T13:00:00Z', valid_to: null},
                },
            },
        })
    })

    test('matches snapshot', () => {
        expect(wrapper.element).toMatchSnapshot()
    })

    test('method version_idx', () => {
        expect(wrapper.vm.version_idx(null)).toBe(undefined)
        expect(wrapper.vm.version_idx('V1')).toBe(1)
        expect(wrapper.vm.version_idx('V2')).toBe(2)
    })

    test('method version_start', () => {
        expect(wrapper.vm.version_start(null)).toBe(undefined)
        expect(wrapper.vm.version_start('V1')).toEqual('Jan 1, 2000 3:00:00 PM')
        expect(wrapper.vm.version_start('V2')).toBe('Jan 1, 2001 1:00:00 PM')
    })

    test('method version_end', () => {
        expect(wrapper.vm.version_end(null)).toBe(undefined)
        expect(wrapper.vm.version_end('V1')).toEqual('Jan 1, 2001 1:00:00 PM')
        expect(wrapper.vm.version_end('V2')).toBe('common:now')
    })

    test('method set_history_idx returns correct string', () => {
        wrapper.vm.set_history_idx([2, 2])
        expect(location.search).toBe('?from_history_id=V1&to_history_id=V2')
    })
})
