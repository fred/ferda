import { initWrapper } from 'ferda-test/TestUtils'
import DataPaginator from 'ferda/common/DataPaginator.vue'
import { PAGINATION } from 'ferda/constants'

describe('DataPaginator', () => {
    let wrapper

    beforeEach(() => {
        wrapper = initWrapper(DataPaginator, { propsData: {
            data: [
                { field_1: 'field_1', field_2: 'field_2' }, { field_1: 'field_1', field_2: 'field_2' },
                { field_1: 'field_1', field_2: 'field_2' }, { field_1: 'field_1', field_2: 'field_2' },
                { field_1: 'field_1', field_2: 'field_2' }, { field_1: 'field_1', field_2: 'field_2' },
                { field_1: 'field_1', field_2: 'field_2' }, { field_1: 'field_1', field_2: 'field_2' },
                { field_1: 'field_1', field_2: 'field_2' }, { field_1: 'field_1', field_2: 'field_2' },
                { field_1: 'field_1', field_2: 'field_2' }, { field_1: 'field_1', field_2: 'field_2' },
            ],
            page_size: 5,
            page_numbers: [5, 10, 20, 50],
            current_page: 1,
        }})
    })

    test('matches snapshot', () => {
        expect(wrapper.element).toMatchSnapshot()
    })

    test('method results_count returns correct value', () => {
        expect(wrapper.vm.results_count).toBe(3)
    })

    test('method paginate_data emitts correctly - paging next without API call', () => {
        wrapper.vm.paginate_data(PAGINATION.NEXT)
        expect(wrapper.emitted('change-current-page')[0][0]).toBe(2)
    })

    test('method paginate_data emitts correctly - paging next with API call', () => {
        wrapper = initWrapper(DataPaginator, { propsData: {
            data: [
                { field_1: 'field_1', field_2: 'field_2' }, { field_1: 'field_1', field_2: 'field_2' },
                { field_1: 'field_1', field_2: 'field_2' }, { field_1: 'field_1', field_2: 'field_2' },
                { field_1: 'field_1', field_2: 'field_2' }, { field_1: 'field_1', field_2: 'field_2' },
                { field_1: 'field_1', field_2: 'field_2' }, { field_1: 'field_1', field_2: 'field_2' },
                { field_1: 'field_1', field_2: 'field_2' }, { field_1: 'field_1', field_2: 'field_2' },
                { field_1: 'field_1', field_2: 'field_2' }, { field_1: 'field_1', field_2: 'field_2' },
            ],
            page_size: 5,
            page_numbers: [5, 10, 20, 50],
            current_page: 4,
            next_page: 'http://example.com/',
        }})
        wrapper.vm.paginate_data(PAGINATION.PREVIOUS)
        wrapper.vm.paginate_data(PAGINATION.NEXT)
        expect(wrapper.emitted('load-next')[0][0]).toBe('http://example.com/')
    })

    test('method paginate_data emitts correctly - paging previous', () => {
        wrapper = initWrapper(DataPaginator, { propsData: {
            data: [
                { field_1: 'field_1', field_2: 'field_2' }, { field_1: 'field_1', field_2: 'field_2' },
                { field_1: 'field_1', field_2: 'field_2' }, { field_1: 'field_1', field_2: 'field_2' },
                { field_1: 'field_1', field_2: 'field_2' }, { field_1: 'field_1', field_2: 'field_2' },
                { field_1: 'field_1', field_2: 'field_2' }, { field_1: 'field_1', field_2: 'field_2' },
                { field_1: 'field_1', field_2: 'field_2' }, { field_1: 'field_1', field_2: 'field_2' },
                { field_1: 'field_1', field_2: 'field_2' }, { field_1: 'field_1', field_2: 'field_2' },
            ],
            page_size: 5,
            page_numbers: [5, 10, 20, 50],
            current_page: 4,
        }})
        wrapper.vm.paginate_data(PAGINATION.PREVIOUS)
        expect(wrapper.emitted('change-current-page')[0][0]).toBe(3)
    })

    test('method change_page_size emitts correctly', () => {
        wrapper.vm.change_page_size(20)
        expect(wrapper.emitted('change-page-size')[0][0]).toBe(20)
    })

    test('property next_page_disabled has correct value', async() => {
        await wrapper.setProps({ current_page: 3 })
        expect(wrapper.vm.next_page_disabled).toStrictEqual(true)

        await wrapper.setProps({ current_page: 4 })
        expect(wrapper.vm.next_page_disabled).toStrictEqual(false)

        await wrapper.setProps({ current_page: 3, is_loaded: false })
        expect(wrapper.vm.next_page_disabled).toStrictEqual(true)

        await wrapper.setProps({ data: [] })
        expect(wrapper.vm.next_page_disabled).toStrictEqual(true)
    })

    test('watcher for pagination emits correctly', async() => {
        const pagination = {
            itemsLength: 150,
            itemsPerPage: 150,
            page: 1,
            pageCount: 1,
            pageStart: 0,
            pageStop: 150,
        }
        const new_props = {
            itemsPerPageText: 'some_text',
            options: {
                groupBy: ['some_group'],
                groupDesc: [false],
                itemsPerPage: 150,
                multiSort: false,
                mustSort: true,
                page: 1,
                sortBy: ['some_sort'],
                sortDesc: [false],
            },
            pagination,
        }
        await wrapper.setProps({ table_props: new_props })
        expect(wrapper.emitted('change-props')[0][0]).toStrictEqual(pagination)
    })

    test('method next_page_disabled returns correct results - no next page', async() => {
        await wrapper.setProps({ current_page: wrapper.vm.results_count, next_page: null })
        expect(wrapper.vm.next_page_disabled).toStrictEqual(true)
    })

    test('method next_page_disabled returns correct results - no data', async() => {
        await wrapper.setProps({ data: [] })
        expect(wrapper.vm.next_page_disabled).toStrictEqual(true)
    })

    test('method next_page_disabled returns correct results - results not loaded', async() => {
        await wrapper.setProps({ is_loaded: false })
        expect(wrapper.vm.next_page_disabled).toStrictEqual(true)
    })
})
