import { NSSET } from 'ferda-test/TestObjects'
import { initWrapper } from 'ferda-test/TestUtils'
import RelatedNsset from 'ferda/common/RelatedNsset.vue'

describe('RelatedNsset', () => {
    let wrapper

    beforeEach(() => {
        wrapper = initWrapper(RelatedNsset, {
            propsData: {
                nsset: NSSET,
            },
        })
    })

    test('matches snapshot', () => {
        expect(wrapper.element).toMatchSnapshot()
    })
})
