import { MESSAGE_REQUEST } from 'ferda-test/TestObjects'
import { initWrapper } from 'ferda-test/TestUtils'
import RelatedMessageList from 'ferda/common/RelatedMessageList.vue'

describe('RelatedMessageList', () => {
    let wrapper

    beforeEach(() => {
        wrapper = initWrapper(RelatedMessageList, {
            propsData: {
                messages: MESSAGE_REQUEST.results,
                search_results_page: MESSAGE_REQUEST.results,
                last_search_message_type: 'email',
                next_page: 'http://example.com/',
                page_size: 10,
                current_page: 1,
            },
        })
    })

    test('matches snapshot', () => {
        expect(wrapper.element).toMatchSnapshot()
    })

    test('empty matches snapshot', () => {
        wrapper = initWrapper(RelatedMessageList, {
            propsData: {
                page_size: 10,
                current_page: 1,
            },
        })
        expect(wrapper.element).toMatchSnapshot()
    })

    test('method change_page_size emits event', () => {
        wrapper.vm.change_page_size(20)

        expect(wrapper.emitted('change-page-size')[0][0]).toBe(20)
    })

    test('method change_current_page emits event', () => {
        wrapper.vm.change_current_page(2)

        expect(wrapper.emitted('change-current-page')[0][0]).toBe(2)
    })

    test('method load_next emits event', () => {
        wrapper.vm.load_next('http://exampletwo.com/')

        expect(wrapper.emitted('load-next')[0][0]).toBe('http://exampletwo.com/')
    })
})
