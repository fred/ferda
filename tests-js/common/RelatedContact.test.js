import { initWrapper } from 'ferda-test/TestUtils'
import RelatedContact from 'ferda/common/RelatedContact.vue'

describe('RelatedContact', () => {
    let wrapper

    beforeEach(() => {
        wrapper = initWrapper(RelatedContact, {
            propsData: {
                contact: {
                    contact_handle: 'OBIWAN',
                    organization: 'Jedi Order',
                    name: 'Obiwan Kenobi',
                    publish: {
                        organization: true,
                        name: false,
                    },
                },
            },
        })
    })

    test('matches snapshot', () => {
        expect(wrapper.element).toMatchSnapshot()
    })
})
