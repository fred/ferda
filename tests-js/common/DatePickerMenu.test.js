import { initWrapper } from 'ferda-test/TestUtils'
import DatePickerMenu from 'ferda/common/DatePickerMenu.vue'

describe('DatePickerMenu', () => {
    let wrapper

    beforeEach(() => {
        jest.useFakeTimers()
        jest.setSystemTime(new Date('2022-02-02T12:00:00+00:00'))
        wrapper = initWrapper(DatePickerMenu, {
            propsData: {
                value: '2020-01-01',
                label: 'Label',
                api_error_messages: [],
            },
        })
    })

    afterEach(() => {
        jest.runOnlyPendingTimers()
        jest.useRealTimers()
    })

    test('empty matches snapshot', () => {
        wrapper = initWrapper(DatePickerMenu)
        expect(wrapper.element).toMatchSnapshot()
    })

    test('matches snapshot', () => {
        wrapper = initWrapper(DatePickerMenu, { propsData: { value: '2020-01-01', label: 'Label' }})
        expect(wrapper.element).toMatchSnapshot()
    })

    test('update_value emits correctly', async() => {
        const new_value = '2021-02-02'

        wrapper.vm.update_value(new_value)
        await wrapper.vm.$nextTick()

        expect(wrapper.emitted('change')[0][0]).toBe(new_value)
        expect(wrapper.emitted('update:value')[0][0]).toBe(new_value)
    })

    test('datepicker shows current date - Date inside is not selected', async() => {
        expect(wrapper.vm.picker_date).toStrictEqual(null)
        await wrapper.setProps({ value: null })
        await wrapper.setData({ show_menu: true })
        expect(wrapper.vm.picker_date).toBe('2022-02-02')
    })

    test('datepicker shows selected date - Date inside is selected', async() => {
        expect(wrapper.vm.picker_date).toStrictEqual(null)
        await wrapper.setProps({ value: '2021-04-01' })
        await wrapper.setData({ show_menu: true })
        expect(wrapper.vm.picker_date).toBe('2021-04-01')
    })

    test('method display_value shows correctly formatted results', async() => {
        expect(wrapper.vm.display_value).toBe('Jan 1, 2020')
        await wrapper.setProps({ value: null })
        expect(wrapper.vm.display_value).toBe('')
        await wrapper.setProps({ value: 'Invalid date' })
        expect(wrapper.vm.display_value).toBe('Invalid date')
    })

    test('method change_date works correctly - empty date', () => {
        wrapper.vm.validate_datetime_input = jest.fn(() => null)
        wrapper.vm.change_date({ target: { value: '2021-01-01' }})
        expect(wrapper.emitted('update:value')[0][0]).toStrictEqual(null)
        expect(wrapper.emitted('change')[0][0]).toStrictEqual(null)
    })

    test('method change_date works correctly - input not changed', () => {
        wrapper.vm.validate_datetime_input = jest.fn(() => false)
        wrapper.vm.change_date({ target: { value: '2021-01-01' }})
        expect(wrapper.emitted('update:value')).toBeFalsy()
        expect(wrapper.emitted('change')).toBeFalsy()
    })

    test('method change_date works correctly - invalid input', () => {
        const errors = ['test errror']
        wrapper.vm.validate_datetime_input = jest.fn(() => errors)
        wrapper.vm.change_date({ target: { value: 'invalid input' }})
        expect(wrapper.emitted('update:value')).toBeFalsy()
        expect(wrapper.emitted('change')).toBeFalsy()
        expect(wrapper.vm.error_messages).toStrictEqual(errors)
    })

    test('method change_date works correctly - invalid unrequired input', async() => {
        const errors = ['test errror']
        await wrapper.setProps({ required: false })
        wrapper.vm.validate_datetime_input = jest.fn(() => errors)
        wrapper.vm.change_date({ target: { value: 'invalid input' }})
        expect(wrapper.emitted('update:value')[0][0]).toBe('invalid input')
        expect(wrapper.vm.error_messages).toStrictEqual(errors)
    })

    test('method change_date works correctly - valid iso input', () => {
        wrapper.vm.validate_datetime_input = jest.fn(() => '2021-01-01')
        wrapper.vm.change_date({ target: { value: '2021-01-01' }})
        expect(wrapper.emitted('update:value')[0][0]).toBe('2021-01-01')
        expect(wrapper.emitted('change')[0][0]).toBe('2021-01-01')
    })

    test('method change_date works correctly - min value exceeded', async() => {
        await wrapper.setProps({ min: '2022-02-02' })
        wrapper.vm.change_date({ target: { value: '2021-01-01' }})

        expect(wrapper.emitted('update:value')[0][0]).toBe('2022-02-02')
        expect(wrapper.emitted('change')[0][0]).toBe('2022-02-02')
    })

    test('method reset_date emits correctly', () => {
        wrapper.vm.reset_date()

        expect(wrapper.emitted('update:value')[0][0]).toBeNull()
        expect(wrapper.emitted('change')[0][0]).toBeNull()
    })

    test('error_messages shows errors from API', async() => {
        const api_error = [ {detail: 'Invalid data', fields: ['name']} ]
        await wrapper.setProps({ api_error_messages: api_error})
        expect(wrapper.vm.error_messages).toStrictEqual(api_error)
    })
})
