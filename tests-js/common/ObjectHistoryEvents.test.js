import { initWrapper } from 'ferda-test/TestUtils'
import ObjectHistoryEvents from 'ferda/common/ObjectHistoryEvents.vue'

describe('ObjectHistoryEvents', () => {
    let wrapper

    beforeEach(() => {
        wrapper = initWrapper(ObjectHistoryEvents, {
            propsData: {
                events_diff: {
                    updated: {
                        old_value: {
                            timestamp: '2010-10-10T12:00:00Z',
                            registrar_handle: 'REG-MOJEID',
                        },
                        new_value: {
                            timestamp: '2010-10-10T12:00:00Z',
                            registrar_handle: 'REG-MOJEID',
                        },
                        differs: false,
                    },
                    transferred: {
                        old_value: {
                            timestamp: '2010-10-10T12:00:00Z',
                            registrar_handle: 'REG-MOJEID',
                        },
                        new_value: {
                            timestamp: '2011-11-11T12:00:00Z',
                            registrar_handle: 'REG-CZNIC',
                        },
                        differs: true,
                    },
                },
            },
        })
    })

    test('matches snapshot', () => {
        expect(wrapper.element).toMatchSnapshot()
    })

    test('matches snapshot with empty timestamps', () => {
        wrapper = initWrapper(ObjectHistoryEvents, {
            propsData: {
                events_diff: {
                    updated: {
                        old_value: {
                            timestamp: null,
                            registrar_handle: 'REG-MOJEID',
                        },
                        new_value: {
                            timestamp: null,
                            registrar_handle: 'REG-MOJEID',
                        },
                        differs: false,
                    },
                    transferred: {
                        old_value: {
                            timestamp: null,
                            registrar_handle: 'REG-MOJEID',
                        },
                        new_value: {
                            timestamp: null,
                            registrar_handle: 'REG-CZNIC',
                        },
                        differs: true,
                    },
                },
            },
        })

        expect(wrapper.element).toMatchSnapshot()
    })
})
