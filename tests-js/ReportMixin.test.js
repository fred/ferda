import { DatetimeMixin } from 'ferda/DatetimeMixin'
import { ReportMixin } from 'ferda/ReportMixin'

import { REPORT, REPORT_TABLE_DATA_PARSED, REPORT_TABLE_HEADERS } from './TestObjects'
import { getClonedStoreOptions, initWrapper } from './TestUtils'

describe('ReportMixin', () => {
    let Component
    let wrapper
    let store

    beforeEach(() => {
        jest.useFakeTimers()
        jest.setSystemTime(new Date('2020-01-01T12:00:00+01:00'))
        Component = {
            render() {},
            mixins: [ DatetimeMixin, ReportMixin ],
        }

        store = getClonedStoreOptions()
        wrapper = initWrapper(Component, {
            mocks: { '$cookies': { get: () => 'csrftoken' }, customStoreOptions: store },
        })
    })

    test('get_run_params returns correct parameters', () => {
        expect(
            wrapper.vm.get_run_params({ param_1: 'parameter_1', param_2: 'parameter_2' })
                .toString(),
        ).toBe('param_1=parameter_1&param_2=parameter_2')
    })

    afterEach(() => {
        jest.runOnlyPendingTimers()
        jest.useRealTimers()
    })

    test('time zone is added correctly', () => {
        expect(wrapper.vm.add_time_zone('05:25')).toBe('05:25+00:00')
        expect(wrapper.vm.add_time_zone('2021-06-07 20:30:30')).toBe('2021-06-07T20:30:30Z')
    })

    test('time zone is stripped and local time kept', () => {
        expect(wrapper.vm.strip_time_zone_from_time('05:25:00')).toBe('05:25:00')
        expect(wrapper.vm.strip_time_zone_from_time('05:25:00+02:00')).toBe('05:25:00')
    })

    test('method strip_time_zone_from_datetime returns correct value', () => {
        expect(wrapper.vm.strip_time_zone_from_datetime('2021-01-01T12:00:00+01:00')).toBe('Jan 1, 2021 12:00:00 PM')
    })

    test('date-time is parsed correctly', () => {
        expect(wrapper.vm.parse_datetime('2021-06-22T00:00:00+02:00')).toEqual(
            {'date': '2021-06-21', 'date_time': '21. Jun 2021 22:00:00', 'time': '22:00:00'},
        )
    })

    test('output parameter is formatted correctly', () => {
        expect(
            wrapper.vm.format_output_parameter('2021-08-02T22:00:00+00:00', 'timestamp with time zone'),
        ).toBe('Aug 2, 2021 10:00:00 PM UTC')
        expect(
            wrapper.vm.format_output_parameter('2021-08-02T22:00:00', 'timestamp without time zone'))
            .toBe('Aug 2, 2021 10:00:00 PM')
        expect(
            wrapper.vm.format_output_parameter('2021-08-02T22:00:00', 'timestamp'))
            .toBe('Aug 2, 2021 10:00:00 PM')
        expect(wrapper.vm.format_output_parameter('22:00:00+00', 'time with time zone')).toBe('10:00:00 PM UTC')
        expect(wrapper.vm.format_output_parameter('22:00', 'time without time zone')).toBe('10:00:00 PM')
        expect(wrapper.vm.format_output_parameter('22:00', 'time')).toBe('10:00:00 PM')
        expect(wrapper.vm.format_output_parameter('2021-01-01', 'date')).toBe('Jan 1, 2021')
        expect(wrapper.vm.format_output_parameter('test text', 'character varying')).toBe('test text')
        expect(wrapper.vm.format_output_parameter(null, 'timestamp')).toBe('')
    })

    test('method convert_naive_properties_to_aware_locals returns data in user zone', () => {
        store.modules.settings.getters.TIME_ZONE = () => 'Europe/Prague'
        wrapper = initWrapper(Component, { customStoreOptions: store, mocks: { '$cookies': { get: () => 'csrftoken' }}})

        expect(wrapper.vm.convert_naive_properties_to_aware_locals(
            {
                timepoint1: '2022-01-10T12:00:00',
                timepoint2: '2022-01-10T12:00:00',
                time1: '12:00:00',
                time2: '12:00:00',
                some_value: 'test',
            },
            [
                { name: 'timepoint1', type: 'timestamp without time zone' },
                { name: 'timepoint2', type: 'timestamp' },
                { name: 'time1', type: 'time' },
                { name: 'time2', type: 'time without time zone' },
                { name: 'some_value', type: 'number'},
            ],
            true,
        ),
        ).toStrictEqual(
            {
                some_value: 'test',
                time1: '13:00:00+01:00',
                time2: '13:00:00+01:00',
                timepoint1: '2022-01-10T13:00:00+01:00',
                timepoint2: '2022-01-10T13:00:00+01:00',
            },
        )
    })

    test('method get_params_for_zone_conversion returns correct params', () => {
        expect(wrapper.vm.get_params_for_zone_conversion([
            { name: 'ts', type: 'timestamp', label: 'Timestamp' },
            { name: 'tswotz', type: 'timestamp without time zone', label: 'Timestamp without time zone' },
            { name: 'tswtz', type: 'timestamp with time zone', label: 'Timestamp with time zone' },
            { name: 't', type: 'time', label: 'Time' },
            { name: 'twotz', type: 'time without time zone', label: 'Time without time zone' },
            { name: 'twtz', type: 'time with time zone', label: 'Time with time zone' },
            { name: 'some_string', type: 'character varying', label: 'Some string' },
        ])).toStrictEqual(['ts', 'tswotz', 't', 'twotz'])
    })

    test('method sort_decimals works correctly', () => {
        expect(wrapper.vm.sort_decimals('1,25', '25,1')).toStrictEqual(-1)
        expect(wrapper.vm.sort_decimals('25,1', '1,25')).toStrictEqual(1)
        expect(wrapper.vm.sort_decimals('25,1', '25,1')).toStrictEqual(0)
    })

    test('method get_widget_type works correctly', () => {
        expect(wrapper.vm.get_widget_type('integer')).toStrictEqual('number')
    })

    test('method get_report_headers works correctly', () => {
        expect(JSON.stringify(wrapper.vm.get_report_headers(REPORT)))
            .toStrictEqual(JSON.stringify(REPORT_TABLE_HEADERS))
    })
})

describe('ReportMixin - CSV export', () => {
    let Component
    let wrapper
    let store
    let blob_mock
    const initial_url = { ...global.URL }

    beforeEach(() => {
        Component = { render() {}, mixins: [DatetimeMixin, ReportMixin]}
        store = getClonedStoreOptions()
        global.URL.createObjectURL = jest.fn()
        global.URL.revokeObjectURL = jest.fn()

        blob_mock = jest.fn()
        jest.spyOn(global, 'Blob').mockImplementationOnce((...args) => blob_mock(...args))

        wrapper = initWrapper(Component,
            { mocks: { '$cookies': { get: () => 'csrftoken' }, customStoreOptions: store }},
        )
    })

    afterEach(() => {
        global.URL = initial_url
        jest.restoreAllMocks()
    })

    test('method export_table_csv works correctly', () => {
        wrapper.vm.export_table_csv(REPORT_TABLE_DATA_PARSED, REPORT)

        expect(blob_mock).toHaveBeenCalledWith([
            'UUID;Handle;Expiration date\n',
            'e84b93c6-7bfe-11be-9439-0242ac130002;example.cz;2018-01-01\n',
            'e84b965a-7fbe-11eb-9439-0242ac130002;test.cz;2018-01-02\n',
            'e84b98c6-ebfe-11eb-9439-0242ac130002;domain.cz;2018-01-03\n',
        ], { type: 'text/csv;charset=utf-8' })
        expect(URL.revokeObjectURL).toHaveBeenCalled()
    })
})
