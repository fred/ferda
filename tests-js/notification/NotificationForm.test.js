import { initWrapper } from 'ferda-test/TestUtils'
import { NOTIFICATION_TYPE } from 'ferda/RegistryNotificationMixin'
import NotificationForm from 'ferda/notification/NotificationForm.vue'

describe('NotificationForm', () => {
    let wrapper

    beforeEach(() => {
        jest.useFakeTimers()
        jest.setSystemTime(new Date('2022-01-05T00:00:00+02:00'))
        wrapper = initWrapper(NotificationForm, { propsData: { notification_type: NOTIFICATION_TYPE.OUTZONE }})
    })

    afterEach(() => {
        jest.runOnlyPendingTimers()
        jest.useRealTimers()
    })

    test('matches snapshot', () => {
        expect(wrapper.element).toMatchSnapshot()
    })

    test('method submit_search emits correctly - valid input', async() => {
        await wrapper.setData({ is_valid: true })
        wrapper.vm.submit_search()
        expect(wrapper.emitted('submit-search')).toStrictEqual([[]])
    })

    test('method submit_search emits correctly - invalid input', async() => {
        await wrapper.setData({ is_valid: false })
        wrapper.vm.submit_search()
        expect(wrapper.emitted('submit-search')).toBeUndefined()
    })

    test('method update_start emits correctly - valid input', () => {
        wrapper.vm.update_start('2021-01-01')
        expect(wrapper.emitted('update:start')[0][0]).toBe('2021-01-01T00:00:00+00:00')
    })

    test('method update_start emits correctly - invalid input', () => {
        wrapper.vm.update_start(null)
        expect(wrapper.emitted('update:start')[0][0]).toBeNull()
    })

    test('method change_notification_type emits correctly - valid input', () => {
        wrapper.vm.change_notification_type(NOTIFICATION_TYPE.OUTZONE)
        expect(wrapper.emitted('update:notification_type')[0][0]).toBe(NOTIFICATION_TYPE.OUTZONE)
    })

    test('method change_notification_type emits correctly - invalid input', () => {
        wrapper.vm.change_notification_type(null)
        expect(wrapper.emitted('update:notification_type')).toBeUndefined()
    })
})
