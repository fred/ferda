import {
    ADDITIONAL_DELETE_CONTACTS_DATA,
    ADDITIONAL_OUTZONE_CONTACTS_DATA,
    GROUPED_CONTACT_DATA,
} from 'ferda-test/TestObjects'
import { initWrapper } from 'ferda-test/TestUtils'
import { CONTACT_TYPE, NOTIFICATION_TYPE } from 'ferda/RegistryNotificationMixin'
import NotificationTable from 'ferda/notification/NotificationTable.vue'

describe('NotificationTable', () => {
    let wrapper
    const uuid = '422c9445-050f-403a-8d12-0d648c3438f7'
    const registrant_handle = 'HANDLE1'
    const value = 'johndoe@email.info'
    const telephone = '+420.123456789'

    beforeEach(async() => {
        jest.useFakeTimers()
        jest.setSystemTime(new Date('2022-05-05T12:00:00+01:00'))

        wrapper = initWrapper(NotificationTable, {
            propsData: {
                domains: ADDITIONAL_OUTZONE_CONTACTS_DATA.results,
                page_size: 100,
                current_page: 1,
                items_left: 0,
                is_loaded: true,
                last_search_value: '2022-05-05T12:00:00+01:00',
            },
        })
        await wrapper.setData({
            selected_groups_outzone: {'2022-05-01T12:00:00+01:00': []},
            selected_groups_delete: {'2022-05-01T12:00:00+01:00': []},
        })
    })

    afterEach(() => {
        jest.runOnlyPendingTimers()
        jest.useRealTimers()
    })

    test('matches snapshot', () => {
        expect(wrapper.element).toMatchSnapshot()
    })

    test('watcher for domains reacts', async() => {
        wrapper.vm.update_row = jest.fn()
        await wrapper.setProps({ domains: [] })

        expect(wrapper.vm.update_row).toHaveBeenCalled()
    })

    test('method row_visible returns correct values', async() => {
        await wrapper.setData({ visible_row_boxes: { [registrant_handle]: [uuid] } })
        expect(wrapper.vm.row_visible(registrant_handle, uuid)).toStrictEqual(true)

        await wrapper.setData({ visible_row_boxes: { [registrant_handle]: ['something else'] } })
        expect(wrapper.vm.row_visible(registrant_handle, uuid)).toStrictEqual(false)
    })

    test('method group_visible returns correct values', async() => {
        await wrapper.setData({ visible_group_boxes: { [registrant_handle]: [uuid] } })
        expect(wrapper.vm.group_visible(registrant_handle, uuid)).toStrictEqual(true)

        await wrapper.setData({ visible_group_boxes: { [registrant_handle]: ['something else'] } })
        expect(wrapper.vm.group_visible(registrant_handle, uuid)).toStrictEqual(false)
    })

    test('method show_row_box works correctly', async() => {
        wrapper.vm.$refs.row_form = { validate: jest.fn(() => true) }
        await wrapper.setData({ visible_row_boxes: {[registrant_handle]: []}})
        await wrapper.vm.show_row_box(registrant_handle, uuid)
        expect(wrapper.vm.visible_row_boxes[registrant_handle]).toStrictEqual([uuid])

        await wrapper.setData({ visible_row_boxes: {} })
        await wrapper.vm.show_row_box(registrant_handle, uuid)
        expect(wrapper.vm.visible_row_boxes[registrant_handle]).toStrictEqual([uuid])
    })

    test('method show_group_box works correctly', async() => {
        wrapper.vm.$refs.group_form = { validate: jest.fn(() => true) }
        await wrapper.setData({ visible_group_boxes: {[registrant_handle]: []}})
        await wrapper.vm.show_group_box(registrant_handle, uuid)
        expect(wrapper.vm.visible_group_boxes[registrant_handle]).toStrictEqual([uuid])

        await wrapper.setData({ visible_group_boxes: {} })
        await wrapper.vm.show_group_box(registrant_handle, uuid)
        expect(wrapper.vm.visible_group_boxes[registrant_handle]).toStrictEqual([uuid])
    })

    test('method group_items works correctly', async() => {
        expect(wrapper.vm.group_items(ADDITIONAL_OUTZONE_CONTACTS_DATA.results)).toStrictEqual(GROUPED_CONTACT_DATA)
        await wrapper.setData({ table_props: null, sort_desc: [true] })
        expect(wrapper.vm.group_items(ADDITIONAL_OUTZONE_CONTACTS_DATA.results)).toStrictEqual(GROUPED_CONTACT_DATA)
    })

    test('method get_existing_contacts returns correct values', async() => {
        const type = CONTACT_TYPE.EMAIL
        const existing_email = 'existing@email.org'
        const additional_contacts = [{ type, value }]
        await wrapper.setProps({ last_search_notification_type: NOTIFICATION_TYPE.OUTZONE })
        expect(wrapper.vm.get_existing_contacts(additional_contacts, registrant_handle, uuid)).toStrictEqual([value])

        await wrapper.setData({ outzone_rows: { [registrant_handle]: { [uuid]: [existing_email]}}})
        expect(wrapper.vm.get_existing_contacts(additional_contacts, registrant_handle, uuid))
            .toStrictEqual([existing_email])

        await wrapper.setData({ outzone_rows: {} })
        expect(wrapper.vm.get_existing_contacts([], registrant_handle, uuid)).toStrictEqual([])
    })

    test('method get_row_data works correctly - outzone', async() => {
        await wrapper.setProps({ last_search_notification_type: NOTIFICATION_TYPE.OUTZONE })
        await wrapper.setData({ outzone_rows: { [registrant_handle]: { [uuid]: [value]}}})
        expect(wrapper.vm.get_row_data(registrant_handle, uuid)).toStrictEqual([value])
    })

    test('method get_row_data works correctly - delete', async() => {
        await wrapper.setProps({ last_search_notification_type: NOTIFICATION_TYPE.DELETE })
        await wrapper.setData({ delete_rows: { [registrant_handle]: { [uuid]: [telephone]}}})
        expect(wrapper.vm.get_row_data(registrant_handle, uuid)).toStrictEqual([telephone])
    })

    test('method change_row_outzone works correctly - existing data', async() => {
        jest.spyOn(window.localStorage.__proto__, 'setItem')
        window.localStorage.__proto__.setItem = jest.fn()
        await wrapper.setProps({ last_search_notification_type: NOTIFICATION_TYPE.OUTZONE })
        await wrapper.setData({ outzone_rows: { [registrant_handle]: {} } })
        wrapper.vm.change_row_outzone([value], { domain_id: uuid, registrant_handle })
        const result = {
            'HANDLE1': { 'cbd5292c-6419-4c74-ee14-7715968e64f3': [] },
            [registrant_handle]: { '422c9445-050f-403a-8d12-0d648c3438f7': [value] },
        }

        expect(wrapper.vm.outzone_rows).toStrictEqual(result)
        expect(window.localStorage.setItem).toHaveBeenCalledWith('notification.outzone_rows', JSON.stringify(result))
    })

    test('method change_row_outzone works correctly - no data', async() => {
        jest.spyOn(window.localStorage.__proto__, 'setItem')
        window.localStorage.__proto__.setItem = jest.fn()
        await wrapper.setData({ outzone_rows: {} })
        wrapper.vm.change_row_outzone([value], { domain_id: uuid, registrant_handle })
        const result = { [registrant_handle]: { '422c9445-050f-403a-8d12-0d648c3438f7': [value] }}

        expect(wrapper.vm.outzone_rows).toStrictEqual(result)
        expect(window.localStorage.setItem).toHaveBeenCalledWith('notification.outzone_rows', JSON.stringify(result))
    })

    test('method change_row_delete works correctly - existing data', async() => {
        jest.spyOn(window.localStorage.__proto__, 'setItem')
        window.localStorage.__proto__.setItem = jest.fn()
        await wrapper.setData({ delete_rows: { [registrant_handle]: {} } })
        await wrapper.setProps({
            domains: ADDITIONAL_DELETE_CONTACTS_DATA.results,
            last_search_notification_type: NOTIFICATION_TYPE.DELETE,
        })
        wrapper.vm.change_row_delete([telephone], { domain_id: uuid, registrant_handle })

        expect(wrapper.vm.delete_rows.HANDLE2).toStrictEqual({ 'ccd5292c-6419-4c75-ee14-7715968e64f3': [] })
        expect(wrapper.vm.delete_rows.HANDLE3).toStrictEqual({ 'aad5292c-6419-4c75-cc14-7715968e64f3': [] })
        expect(wrapper.vm.delete_rows.HANDLE1).toStrictEqual({
            '422c9445-050f-403a-8d12-0d648c3438f7': ['+420.123456789'],
            'cbd5292c-6419-4c74-ee14-7715968e64f3': ['+420.111222333'],
        })
    })

    test('method change_row_delete works correctly - no data', async() => {
        await wrapper.setData({ delete_rows: {} })
        wrapper.vm.change_row_delete([telephone], { domain_id: uuid, registrant_handle })

        expect(wrapper.vm.delete_rows).toStrictEqual({
            HANDLE1: { '422c9445-050f-403a-8d12-0d648c3438f7': [telephone] },
        })
    })

    test('method change_group_outzone works correctly', () => {
        jest.spyOn(window.localStorage.__proto__, 'setItem')
        window.localStorage.__proto__.setItem = jest.fn()
        wrapper.vm.change_group_outzone([value], { domain_id: uuid, registrant_handle })
        const result_rows = { 'HANDLE1': { 'cbd5292c-6419-4c74-ee14-7715968e64f3': [value] }}
        const result_group = { [registrant_handle]: [value] }

        expect(wrapper.vm.outzone_rows).toStrictEqual(result_rows)
        expect(window.localStorage.setItem).toHaveBeenNthCalledWith(
            1, 'notification.outzone_groups', JSON.stringify(result_group),
        )
        expect(window.localStorage.setItem).toHaveBeenNthCalledWith(
            2, 'notification.outzone_rows', JSON.stringify(result_rows),
        )
    })

    test('method change_group_delete works correctly', () => {
        jest.spyOn(window.localStorage.__proto__, 'setItem')
        window.localStorage.__proto__.setItem = jest.fn()
        wrapper.vm.change_group_delete([telephone], { domain_id: uuid, registrant_handle })
        const result_rows = {
            'HANDLE3': { 'aad5292c-6419-4c75-cc14-7715968e64f3': [] },
            'HANDLE1': { 'cbd5292c-6419-4c74-ee14-7715968e64f3': [telephone] },
            'HANDLE2': { 'ccd5292c-6419-4c75-ee14-7715968e64f3': [] },
        }
        const result_group = { 'HANDLE3': [], 'HANDLE1': [telephone], 'HANDLE2': [] }

        expect(wrapper.vm.delete_rows).toStrictEqual(result_rows)
        expect(window.localStorage.setItem).toHaveBeenNthCalledWith(
            1, 'notification.delete_groups', JSON.stringify(result_group),
        )
        expect(window.localStorage.setItem).toHaveBeenNthCalledWith(
            2, 'notification.delete_rows', JSON.stringify(result_rows),
        )
    })

    test('method submit_values emits correctly - outzone', async() => {
        await wrapper.setData({ outzone_rows: ADDITIONAL_OUTZONE_CONTACTS_DATA.results.map })
        await wrapper.setData({ outzone_rows: {
            'HANDLE1': { 'cbd5292c-6419-4c74-ee14-7715968e64f3': ['email@org.com'] },
            'HANDLE2': { 'ccd5292c-6419-4c75-ee14-7715968e64f3': ['email@org.com'] },
            'HANDLE3': { 'aad5292c-6419-4c75-cc14-7715968e64f3': ['email@org.com'] },
        }})
        await wrapper.setProps({ last_search_notification_type: NOTIFICATION_TYPE.OUTZONE })
        await wrapper.vm.submit_values()

        expect(wrapper.emitted('submit')[0][0]).toStrictEqual({
            domains: {
                'cbd5292c-6419-4c74-ee14-7715968e64f3': [{ type: 'email', value: 'email@org.com' }],
                'ccd5292c-6419-4c75-ee14-7715968e64f3': [{ type: 'email', value: 'email@org.com' }],
                'aad5292c-6419-4c75-cc14-7715968e64f3': [{ type: 'email', value: 'email@org.com' }],
            },
        })
    })

    test('method submit_values emits correctly - delete', async() => {
        await wrapper.setData({ delete_rows: {
            'HANDLE1': { 'cbd5292c-6419-4c74-ee14-7715968e64f3': ['+420.123456789'] },
            'HANDLE2': { 'ccd5292c-6419-4c75-ee14-7715968e64f3': ['+420.123456789'] },
            'HANDLE3': { 'aad5292c-6419-4c75-cc14-7715968e64f3': ['+420.123456789'] },
        }})
        await wrapper.setProps({ last_search_notification_type: NOTIFICATION_TYPE.DELETE })
        await wrapper.vm.submit_values()

        expect(wrapper.emitted('submit')[0][0]).toStrictEqual({
            domains: {
                'cbd5292c-6419-4c74-ee14-7715968e64f3': [{ type: CONTACT_TYPE.TELEPHONE, value: '+420.123456789' }],
                'ccd5292c-6419-4c75-ee14-7715968e64f3': [{ type: CONTACT_TYPE.TELEPHONE, value: '+420.123456789' }],
                'aad5292c-6419-4c75-cc14-7715968e64f3': [{ type: CONTACT_TYPE.TELEPHONE, value: '+420.123456789' }],
            },
        })
    })

    test('method submit_values works correctly - form is not valid', async() => {
        await wrapper.setData({ form_valid: false })
        await wrapper.vm.submit_values()

        expect(wrapper.emitted('submit')).toBeFalsy()
    })

    test('method get_group_description works correctly', () => {
        expect(wrapper.vm.get_group_description('registrant_name', 'registrant_organization')).toBe(
            'registrant_organization - registrant_name',
        )
        expect(wrapper.vm.get_group_description('registrant_name', '')).toBe('registrant_name')
    })

    test('method update_row works correctly - outzone', async() => {
        await wrapper.setProps({ last_search_notification_type: NOTIFICATION_TYPE.OUTZONE })
        await wrapper.setData({ outzone_rows: {} })
        wrapper.vm.update_row()

        expect(wrapper.vm.outzone_rows).toStrictEqual({
            HANDLE1: { 'cbd5292c-6419-4c74-ee14-7715968e64f3': ['test@email.org'] },
            HANDLE2: { 'ccd5292c-6419-4c75-ee14-7715968e64f3': ['jane@email.org'] },
            HANDLE3: { 'aad5292c-6419-4c75-cc14-7715968e64f3': ['xaver@email.org'] },
        })
    })

    test('method update_row works correctly - delete', async() => {
        await wrapper.setProps({
            domains: ADDITIONAL_DELETE_CONTACTS_DATA.results,
            last_search_notification_type: NOTIFICATION_TYPE.DELETE,
        })
        await wrapper.setData({ delete_rows: {} })
        wrapper.vm.update_row()

        expect(wrapper.vm.delete_rows).toStrictEqual({
            HANDLE1: { 'cbd5292c-6419-4c74-ee14-7715968e64f3': ['+420.111222333'] },
            HANDLE2: { 'ccd5292c-6419-4c75-ee14-7715968e64f3': ['+420.555555555'] },
            HANDLE3: { 'aad5292c-6419-4c75-cc14-7715968e64f3': ['+420.123456789'] },
            // Tests the case where only registry phone is defined
            HANDLE4: { 'cscc5292c-6419-4c75-ee14-2355968e64f3': ['+420.555555555'] },
        })
    })

    test('method format_phone works correctly', () => {
        expect(wrapper.vm.format_phone(['123456789'])).toStrictEqual(['+420.123456789'])
        expect(wrapper.vm.format_phone(['123 456 789'])).toStrictEqual(['+420.123456789'])
        expect(wrapper.vm.format_phone(['+420.123456789'])).toStrictEqual(['+420.123456789'])
    })

    test('group_data_origin returns correct results - outzone', async() => {
        const groups = { [registrant_handle]: [value] }
        await wrapper.setProps({ last_search_notification_type: NOTIFICATION_TYPE.OUTZONE })
        await wrapper.setData({ outzone_groups: groups })

        expect(wrapper.vm.group_data_origin).toStrictEqual(groups)
    })

    test('group_data_origin returns correct results - delete', async() => {
        const groups = { [registrant_handle]: [telephone], HANDLE2: [], HANDLE3: [] }
        await wrapper.setProps({ last_search_notification_type: NOTIFICATION_TYPE.DELETE })
        await wrapper.setData({ delete_groups: groups })

        expect(wrapper.vm.group_data_origin).toStrictEqual(groups)
    })

    test('value_placeholder returns correct results', async() => {
        await wrapper.setProps({ last_search_notification_type: NOTIFICATION_TYPE.OUTZONE })
        expect(wrapper.vm.value_placeholder).toStrictEqual(['valuePlaceholder'])

        await wrapper.setProps({ last_search_notification_type: NOTIFICATION_TYPE.DELETE })
        expect(wrapper.vm.value_placeholder).toStrictEqual(['valuePlaceholder', 'valuePlaceholder--phone'])
    })

    test('method get_progress_value returns correct values', () => {
        expect(wrapper.vm.get_progress_value('HANDLE', ['HANDLE', 'TEST'])).toStrictEqual(['TEST'])
        expect(wrapper.vm.get_progress_value('HANDLE', ['TEST'])).toStrictEqual(['TEST', 'HANDLE'])
    })

    test('method delete_old_progress_cache works correctly', async() => {
        jest.spyOn(window.localStorage.__proto__, 'setItem')
        window.localStorage.__proto__.setItem = jest.fn()
        wrapper = initWrapper(NotificationTable, {
            propsData: {
                domains: ADDITIONAL_OUTZONE_CONTACTS_DATA.results,
                page_size: 100,
                current_page: 1,
                items_left: 0,
                is_loaded: true,
                last_search_value: '2022-05-05T12:00:00+01:00',
            },
            data() {
                return {
                    selected_groups_outzone: {
                        '2021-01-01T12:00:00+01:00': [],
                        '2021-12-01T12:00:00+01:00': [],
                        '2022-05-01T12:00:00+01:00': [],
                        '2022-07-01T12:00:00+01:00': [],
                        '2022-05-05T12:00:00+01:00': [],
                        '2022-05-04T12:00:00+01:00': [],
                    },
                    selected_groups_delete: {
                        '2021-01-01T12:00:00+01:00': [],
                        '2021-12-01T12:00:00+01:00': [],
                        '2022-05-01T12:00:00+01:00': [],
                        '2022-07-01T12:00:00+01:00': [],
                        '2022-05-05T12:00:00+01:00': [],
                        '2022-05-04T12:00:00+01:00': [],
                    },
                }
            },
        })

        // Only dates after (or equal) 2022-05-05 (current date) should remain
        expect(wrapper.vm.selected_groups_outzone).toStrictEqual({
            '2022-05-05T12:00:00+01:00': [],
            '2022-07-01T12:00:00+01:00': [],
        })
        expect(window.localStorage.setItem).toHaveBeenNthCalledWith(
            1,
            'notification.selected_groups_outzone',
            JSON.stringify({ '2022-07-01T12:00:00+01:00': [], '2022-05-05T12:00:00+01:00': []}),
        )
        expect(window.localStorage.setItem).toHaveBeenNthCalledWith(
            2,
            'notification.selected_groups_delete',
            JSON.stringify({ '2022-07-01T12:00:00+01:00': [], '2022-05-05T12:00:00+01:00': []}),
        )
    })

    test('property selected_progress_origin returns correct data', async() => {
        await wrapper.setData({
            selected_groups_outzone: {'2022-05-05T12:00:00+01:00': ['TEST1']},
            selected_groups_delete: {'2022-05-05T12:00:00+01:00': ['TEST2']},
        })
        await wrapper.setProps({ last_search_notification_type: NOTIFICATION_TYPE.OUTZONE })
        expect(wrapper.vm.selected_progress_origin).toStrictEqual(['TEST1'])

        await wrapper.setProps({ last_search_notification_type: NOTIFICATION_TYPE.DELETE })
        expect(wrapper.vm.selected_progress_origin).toStrictEqual(['TEST2'])
    })

    test('last_search_value watcher works correctly', async() => {
        const last_search_value = '2025-01-01T12:00:00+01:00'
        await wrapper.setProps({ last_search_value })

        expect(wrapper.vm.selected_groups_outzone[last_search_value]).toStrictEqual([])
        expect(wrapper.vm.selected_groups_delete[last_search_value]).toStrictEqual([])
    })

    test('method manage_progress_checkbox works correctly - check only with box checked', async() => {
        jest.spyOn(window.localStorage.__proto__, 'setItem')
        window.localStorage.__proto__.setItem = jest.fn()
        const last_search_value = '2025-01-01T12:00:00+01:00'
        await wrapper.setProps({ last_search_notification_type: NOTIFICATION_TYPE.OUTZONE, last_search_value })
        await wrapper.setData({ selected_groups_outzone: {[last_search_value]: ['HANDLE']} })
        wrapper.vm.manage_progress_checkbox('HANDLE')

        expect(wrapper.vm.selected_groups_outzone[last_search_value]).toStrictEqual(['HANDLE'])
        expect(window.localStorage.setItem).not.toHaveBeenCalled()
    })

    test('method manage_progress_checkbox works correctly - toggle', async() => {
        jest.spyOn(window.localStorage.__proto__, 'setItem')
        window.localStorage.__proto__.setItem = jest.fn()
        await wrapper.setProps({ last_search_notification_type: NOTIFICATION_TYPE.OUTZONE })
        wrapper.vm.manage_progress_checkbox('HANDLE')

        expect(wrapper.vm.selected_groups_outzone[wrapper.vm.last_search_value]).toStrictEqual(['HANDLE'])
        expect(window.localStorage.setItem).toHaveBeenCalledWith(
            'notification.selected_groups_outzone',
            JSON.stringify({'2022-05-05T12:00:00+01:00': ['HANDLE'], '2022-05-01T12:00:00+01:00': []}),
        )
    })
})
