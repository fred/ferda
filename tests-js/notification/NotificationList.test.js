import axios from 'axios'
import MockAdapter from 'axios-mock-adapter'
import flushPromises from 'flush-promises'

import {
    ADDITIONAL_DELETE_CONTACTS_DATA,
    ADDITIONAL_DELETE_CONTACTS_FOR_UPDATE,
    ADDITIONAL_OUTZONE_CONTACTS_DATA,
    ADDITIONAL_OUTZONE_CONTACTS_FOR_UPDATE,
} from 'ferda-test/TestObjects'
import { initWrapper } from 'ferda-test/TestUtils'
import { NOTIFICATION_PARAM, NOTIFICATION_TYPE } from 'ferda/RegistryNotificationMixin'
import { URLS } from 'ferda/Urls'
import NotificationList from 'ferda/notification/NotificationList.vue'

describe('NotificationList', () => {
    let wrapper
    let axios_mock
    const initial_history = { ...window.history }
    const initial_location = { ...window.location }
    const initial_console = console.log

    beforeEach(async() => {
        jest.useFakeTimers()
        jest.setSystemTime(new Date('2022-01-05T00:00:00+02:00'))

        delete window.history
        window.history = { replaceState: jest.fn() }

        axios_mock = new MockAdapter(axios)
        axios_mock.onGet(URLS.additional_outzone_contacts_ajax()).reply(200, ADDITIONAL_OUTZONE_CONTACTS_DATA)
        axios_mock.onPost(URLS.additional_outzone_contacts_result_ajax()).reply(200)
        axios_mock.onGet(URLS.additional_delete_contacts_ajax()).reply(200, ADDITIONAL_DELETE_CONTACTS_DATA)
        axios_mock.onPost(URLS.additional_delete_contacts_result_ajax()).reply(200)
        wrapper = initWrapper(NotificationList, { mocks: { $notify: jest.fn(), $cookies: { get: jest.fn() }}})
        await wrapper.setData({ last_search_value: '2021-01-01T12:00:00+01:00' })
        await flushPromises()
    })

    afterEach(() => {
        delete window.history
        delete window.location
        window.history = initial_history
        window.location = initial_location
        console.log = initial_console
        jest.runOnlyPendingTimers()
        jest.useRealTimers()
    })

    test('matches snapshot', () => {
        expect(wrapper.element).toMatchSnapshot()
    })

    test('component works correctly with prefilled URL params', async() => {
        const start = '2021-01-01T12:00:00+01:00'
        const page_size = 200

        const mount_params = new URLSearchParams()
        mount_params.append(NOTIFICATION_PARAM.START, start)
        mount_params.append(NOTIFICATION_PARAM.PAGE_SIZE, page_size)
        mount_params.append(NOTIFICATION_PARAM.NOTIFICATION_TYPE, NOTIFICATION_TYPE.OUTZONE)

        delete window.location
        window.location = { search: `?${mount_params.toString()}` }

        wrapper = initWrapper(NotificationList, { mocks: { $notify: jest.fn(), $cookies: { get: jest.fn() }}})
        await flushPromises()

        expect(wrapper.vm.start).toBe(start)
        expect(wrapper.vm.notification_type).toBe(NOTIFICATION_TYPE.OUTZONE)
        // Make sure it loads data
        expect(wrapper.vm.domains).toStrictEqual(ADDITIONAL_OUTZONE_CONTACTS_DATA.results)
    })

    test('method submit_search works correctly - outzone', async() => {
        const start = '2021-01-01T00:00:00+02:00'
        await wrapper.setData({ start, notification_type: NOTIFICATION_TYPE.OUTZONE, domains: [] })
        await wrapper.vm.submit_search(true, false)

        expect(wrapper.vm.domains).toStrictEqual(ADDITIONAL_OUTZONE_CONTACTS_DATA.results)
        expect(wrapper.vm.next_page).toBe('some_token')
        expect(wrapper.vm.items_left).toBe(0)

        const params = new URLSearchParams()
        params.append(NOTIFICATION_PARAM.START, start)
        params.append(NOTIFICATION_PARAM.PAGE_SIZE, wrapper.vm.$store.getters['settings/PAGINATION_PAGE_SIZE'])
        params.append(NOTIFICATION_PARAM.NOTIFICATION_TYPE, NOTIFICATION_TYPE.OUTZONE)

        expect(history.replaceState).toHaveBeenCalledWith(null, null, `?${params.toString()}`)
    })

    test('method submit_search works correctly - delete', async() => {
        const start = '2021-01-01T00:00:00+02:00'
        await wrapper.setData({ start, notification_type: NOTIFICATION_TYPE.DELETE, domains: [] })
        await wrapper.vm.submit_search(true, false)

        expect(wrapper.vm.domains).toStrictEqual(ADDITIONAL_DELETE_CONTACTS_DATA.results)
        expect(wrapper.vm.next_page).toBe('some_token')
        expect(wrapper.vm.items_left).toBe(0)

        const params = new URLSearchParams()
        params.append(NOTIFICATION_PARAM.START, start)
        params.append(NOTIFICATION_PARAM.PAGE_SIZE, wrapper.vm.$store.getters['settings/PAGINATION_PAGE_SIZE'])
        params.append(NOTIFICATION_PARAM.NOTIFICATION_TYPE, NOTIFICATION_TYPE.DELETE)

        expect(history.replaceState).toHaveBeenCalledWith(null, null, `?${params.toString()}`)
    })

    test('method submit_search fails by server_error - outzone', async() => {
        console.log = jest.fn()
        axios_mock.onGet(URLS.additional_outzone_contacts_ajax()).reply(500)
        await wrapper.setData({ notification_type: NOTIFICATION_TYPE.OUTZONE })
        await wrapper.vm.submit_search()

        expect(wrapper.vm.domains).toStrictEqual(null)
        expect(wrapper.vm.$notify).toHaveBeenNthCalledWith(1, { clean: true })
        expect(wrapper.vm.$notify).toHaveBeenNthCalledWith(2, wrapper.vm.ERROR_SOMETHING_WENT_WRONG)
        expect(console.log).toHaveBeenCalledWith('error: notifications:something_went_wrong')
    })

    test('method submit_search fails by server_error - delete', async() => {
        console.log = jest.fn()
        axios_mock.onGet(URLS.additional_delete_contacts_ajax()).reply(500)
        await wrapper.setData({ notification_type: NOTIFICATION_TYPE.DELETE })
        await wrapper.vm.submit_search()

        expect(wrapper.vm.domains).toStrictEqual(null)
        expect(wrapper.vm.$notify).toHaveBeenNthCalledWith(1, { clean: true })
        expect(wrapper.vm.$notify).toHaveBeenNthCalledWith(2, wrapper.vm.ERROR_SOMETHING_WENT_WRONG)
        expect(console.log).toHaveBeenCalledWith('error: notifications:something_went_wrong')
    })

    test('method get_additional_contacts returns correct results', async() => {
        console.log = jest.fn()
        expect(await wrapper.vm.get_additional_contacts(false)).toStrictEqual(ADDITIONAL_OUTZONE_CONTACTS_DATA)
        expect(wrapper.vm.$notify).not.toHaveBeenCalled()
        expect(console.log).not.toHaveBeenCalled()
    })

    test('method get_additional_contacts fails by server error', async() => {
        console.log = jest.fn()
        axios_mock.onGet(URLS.additional_outzone_contacts_ajax()).reply(500)
        expect(await wrapper.vm.get_additional_contacts(false)).toBeNull()
        expect(wrapper.vm.$notify).toHaveBeenCalledWith(wrapper.vm.ERROR_SOMETHING_WENT_WRONG)
        expect(console.log).toHaveBeenCalledWith('error: notifications:something_went_wrong')
    })

    test('method get_additional_contacts is aborted', async() => {
        console.log = jest.fn()
        wrapper.vm.get_additional_contacts()
        await wrapper.vm.stop_search()

        expect(wrapper.vm.$notify).toHaveBeenCalledWith(wrapper.vm.INFO_REQUEST_ABORTED)
        expect(console.log).toHaveBeenCalledWith('info: notifications:search_aborted')
    })

    test('method get_additional_contacts_params returns correct params - for url', async() => {
        const last_search_start = '2021-01-01T12:00:00+01:00'
        const page_size = 200
        const next_page = 'some_token'

        await wrapper.setData({ last_search_start, page_size, next_page })

        const expected_params = new URLSearchParams()
        expected_params.append(NOTIFICATION_PARAM.START, last_search_start)
        expected_params.append(NOTIFICATION_PARAM.PAGE_SIZE, page_size)
        expected_params.append(NOTIFICATION_PARAM.NOTIFICATION_TYPE, NOTIFICATION_TYPE.OUTZONE)
        expected_params.append(NOTIFICATION_PARAM.PAGE_TOKEN, next_page)

        expect(wrapper.vm.get_additional_contacts_params(false, true).toString()).toBe(expected_params.toString())
    })

    test('method submit_result works correctly - outzone', async() => {
        jest.spyOn(window.localStorage.__proto__, 'removeItem')
        window.localStorage.__proto__.removeItem = jest.fn()
        console.log = jest.fn()
        await wrapper.setData({ last_search_notification_type: NOTIFICATION_TYPE.OUTZONE })
        await wrapper.vm.submit_result(ADDITIONAL_OUTZONE_CONTACTS_FOR_UPDATE)

        expect(wrapper.vm.$notify).toHaveBeenCalledWith(wrapper.vm.SUCCESS_CHANGES_SAVED)
        expect(console.log).toHaveBeenCalledWith('success: notifications:changes_saved')
        expect(wrapper.vm.domains).toStrictEqual(null)
        expect(window.localStorage.removeItem).toHaveBeenNthCalledWith(1, 'notification.outzone_rows')
        expect(window.localStorage.removeItem).toHaveBeenNthCalledWith(2, 'notification.outzone_groups')
    })

    test('method submit_result works correctly - delete', async() => {
        jest.spyOn(window.localStorage.__proto__, 'removeItem')
        window.localStorage.__proto__.removeItem = jest.fn()
        console.log = jest.fn()
        await wrapper.setData({ last_search_notification_type: NOTIFICATION_TYPE.DELETE })
        await wrapper.vm.submit_result(ADDITIONAL_DELETE_CONTACTS_FOR_UPDATE)

        expect(wrapper.vm.$notify).toHaveBeenCalledWith(wrapper.vm.SUCCESS_CHANGES_SAVED)
        expect(console.log).toHaveBeenCalledWith('success: notifications:changes_saved')
        expect(wrapper.vm.domains).toStrictEqual(null)
        expect(window.localStorage.removeItem).toHaveBeenNthCalledWith(1, 'notification.delete_rows')
        expect(window.localStorage.removeItem).toHaveBeenNthCalledWith(2, 'notification.delete_groups')
    })

    test('method get_additional_contacts_params returns correct params - for http request', async() => {
        const last_search_start = '2021-01-01T12:00:00+01:00'
        const end = wrapper.vm.create_next_day_midnight(last_search_start, 1)
        const page_size = 200
        const next_page = 'some_token'

        await wrapper.setData({ last_search_start, page_size, next_page })

        const expected_params = new URLSearchParams()
        expected_params.append(NOTIFICATION_PARAM.START, last_search_start)
        expected_params.append(NOTIFICATION_PARAM.PAGE_SIZE, page_size)
        expected_params.append(NOTIFICATION_PARAM.END, end)
        expected_params.append(NOTIFICATION_PARAM.PAGE_TOKEN, next_page)

        expect(wrapper.vm.get_additional_contacts_params(false).toString()).toBe(expected_params.toString())
    })

    test('method submit_result fails by server error - unspecified 500 error', async() => {
        const previous_data = wrapper.vm.domains
        jest.spyOn(window.localStorage.__proto__, 'removeItem')
        window.localStorage.__proto__.removeItem = jest.fn()
        await wrapper.setData({ last_search_notification_type: NOTIFICATION_TYPE.OUTZONE })
        console.log = jest.fn()
        axios_mock.onPost(URLS.additional_outzone_contacts_result_ajax()).reply(500)
        await wrapper.vm.submit_result(ADDITIONAL_OUTZONE_CONTACTS_FOR_UPDATE)

        expect(wrapper.vm.$notify).toHaveBeenCalledWith(wrapper.vm.ERROR_SOMETHING_WENT_WRONG)
        expect(console.log).toHaveBeenCalledWith('error: notifications:something_went_wrong')
        expect(wrapper.vm.domains).toStrictEqual(previous_data)
        expect(window.localStorage.removeItem).not.toHaveBeenCalled()
    })

    test('method submit_result fails by server error - 404 because of data inconsistency', async() => {
        const previous_data = wrapper.vm.domains
        jest.spyOn(window.localStorage.__proto__, 'removeItem')
        window.localStorage.__proto__.removeItem = jest.fn()
        await wrapper.setData({ last_search_notification_type: NOTIFICATION_TYPE.OUTZONE })
        console.log = jest.fn()
        axios_mock.onPost(URLS.additional_outzone_contacts_result_ajax()).reply(404)
        await wrapper.vm.submit_result(ADDITIONAL_OUTZONE_CONTACTS_FOR_UPDATE)

        expect(wrapper.vm.$notify).toHaveBeenCalledWith({
            title: 'domain:data_inconsistency_title',
            text: 'domain:data_inconsistency_text',
            type: 'info',
            duration: -1,
        })
        expect(console.log).toHaveBeenCalledWith('info: domain:data_inconsistency_text')
        expect(wrapper.vm.domains).toStrictEqual(previous_data)
        expect(window.localStorage.removeItem).not.toHaveBeenCalled()
    })

    test('method submit_result is aborted', async() => {
        const previous_data = wrapper.vm.domains
        jest.spyOn(window.localStorage.__proto__, 'removeItem')
        window.localStorage.__proto__.removeItem = jest.fn()
        console.log = jest.fn()
        wrapper.vm.submit_result(ADDITIONAL_OUTZONE_CONTACTS_FOR_UPDATE)
        await wrapper.vm.stop_search()

        expect(wrapper.vm.$notify).toHaveBeenCalledWith(wrapper.vm.INFO_REQUEST_ABORTED)
        expect(console.log).toHaveBeenCalledWith('info: notifications:search_aborted')
        expect(wrapper.vm.domains).toStrictEqual(previous_data)
        expect(window.localStorage.removeItem).not.toHaveBeenCalled()
    })

    test('method change_page_size works correctly', async() => {
        const new_page_size = 300
        wrapper.vm.submit_search = jest.fn()
        await wrapper.vm.change_page_size(new_page_size)

        expect(wrapper.vm.page_size).toBe(new_page_size)
        expect(wrapper.vm.current_page).toBe(1)
        expect(wrapper.vm.submit_search).toHaveBeenCalledWith(false, true)
    })
})
