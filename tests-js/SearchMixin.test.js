import { OBJECT_TYPE } from 'ferda/RegistryMixin'
import { SearchMixin } from 'ferda/SearchMixin'

import { SEARCH_CONTACT } from './TestObjects'
import { initWrapper } from './TestUtils'

describe('SearchMixin', () => {
    let wrapper

    beforeEach(() => {
        const Component = {
            render() {},
            mixins: [SearchMixin],
        }
        wrapper = initWrapper(Component)
    })

    test('get_match_keys - current (empty results)', () => {
        expect(wrapper.vm.get_match_keys({})).toStrictEqual([])
    })

    test('get_match_keys - current', () => {
        expect(wrapper.vm.get_match_keys({ matched_items: ['telephone'] })).toStrictEqual(['telephone'])
    })

    test('get_match_keys - last known (still existing object)', () => {
        const histories_results = {
            histories: [
                {
                    matched_items: ['telephone'],
                    valid_from: '2017-04-02T18:39:40.798Z',
                    valid_to: null,
                },
            ],
            last_history: { events: {} },
        }

        expect(wrapper.vm.get_match_keys(histories_results)).toStrictEqual(['telephone'])
    })

    test('get_match_keys - last known (unregistered object)', () => {
        const histories_results = {
            histories: [{
                matched_items: ['email'],
                valid_from: '2017-04-02T18:39:40.798Z',
                valid_to: '2018-04-02T18:39:40.798Z',
            }],
            last_history: {events: {unregistered: {timestamp: '2018-04-02T18:39:40.798Z'}}},
        }

        expect(wrapper.vm.get_match_keys(histories_results)).toStrictEqual(['email'])
    })

    test('method match_item_label returns correct values', () => {
        expect(wrapper.vm.match_item_label('test', OBJECT_TYPE.CONTACT)).toEqual(`${OBJECT_TYPE.CONTACT}:test`)
        expect(wrapper.vm.match_item_label('test', OBJECT_TYPE.DOMAIN)).toEqual(`${OBJECT_TYPE.DOMAIN}:test`)
        expect(wrapper.vm.match_item_label('test', OBJECT_TYPE.NSSET)).toEqual(`${OBJECT_TYPE.NSSET}:test`)
        expect(wrapper.vm.match_item_label('test', OBJECT_TYPE.KEYSET)).toEqual(`${OBJECT_TYPE.KEYSET}:test`)
        expect(wrapper.vm.match_item_label('additional_identifier.test', OBJECT_TYPE.CONTACT))
            .toEqual(`contact:additional_identifier (contact:identifier_type.test)`)
    })

    test('method match_item_value returns correct values', () => {
        expect(wrapper.vm.match_item_value({ contact: SEARCH_CONTACT }, 'additional_identifier')).toStrictEqual({
            type: 'birthdate',
            value: '2012-04-05',
        })

        expect(wrapper.vm.match_item_value({ contact: SEARCH_CONTACT }, 'notify_email')).toBe('notify@email.org')
        expect(wrapper.vm.match_item_value({ contact: SEARCH_CONTACT }, 'name')).toBe('Some name')
    })

    test('method matched_addresses returns correct values', () => {
        expect(wrapper.vm.matched_addresses({
            contact: SEARCH_CONTACT,
            matched_items: ['place.street', 'place.city']})).toStrictEqual([
            {
                address: [
                    { is_matching: true, value: 'test1, test2, test3' },
                    { is_matching: true, value: 'Test city' },
                    { is_matching: false, value: '05855' },
                    { is_matching: false, value: '' },
                    { is_matching: false, value: 'CZ' },
                ],
                label: 'contact:place',
                type: 'place',
            },
        ])
        expect(wrapper.vm.matched_addresses({ contact: SEARCH_CONTACT, matched_items: ['email'] })).toStrictEqual([])
    })

    test('method address_items returns correct values', () => {
        expect(wrapper.vm.address_items({ contact: SEARCH_CONTACT, matched_items: ['place.street'] }, 'place'))
            .toStrictEqual([
                { is_matching: true, value: 'test1, test2, test3' },
                { is_matching: false, value: 'Test city' },
                { is_matching: false, value: '05855' },
                { is_matching: false, value: '' },
                { is_matching: false, value: 'CZ' },
            ])
        expect(wrapper.vm.address_items({ contact: {}, matched_items: ['place.street'] }, 'place')).toStrictEqual(null)
    })

    test('method parse_address_item returns correct values', () => {
        expect(wrapper.vm.parse_address_item(
            { contact: SEARCH_CONTACT, matched_items: ['place.city'] }, 'place', 'city'),
        ).toStrictEqual({ is_matching: true, value: 'Test city' })

        expect(wrapper.vm.parse_address_item(
            { contact: SEARCH_CONTACT, matched_items: ['place.street'] }, 'place', 'street', 'test1, test2, test3'),
        ).toStrictEqual({ is_matching: true, value: 'test1, test2, test3' })
    })

    test('method other_matches returns correct results', () => {
        expect(wrapper.vm.other_matches(null)).toStrictEqual([])
        expect(wrapper.vm.other_matches(['fax', 'place.street'])).toStrictEqual(['fax'])
    })
})
