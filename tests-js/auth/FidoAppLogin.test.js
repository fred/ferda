import { DJANGO_FIDO_FORM_ID, createTranslations, startFido2 } from '@cz-nic/django-fido/django_fido/js/fido2'

import { initWrapper } from 'ferda-test/TestUtils'
import FidoAppLogin from 'ferda/auth/FidoAppLogin.vue'

jest.mock('@cz-nic/django-fido/django_fido/js/fido2')

describe('FidoAppLogin', () => {
    let wrapper

    beforeEach(() => {
        wrapper = initWrapper(FidoAppLogin, {
            propsData: {
                form_errors: {},
            },
            mocks: {
                '$cookies': {
                    get: () => jest.fn(),
                },
                gettext: (x) => x,
            },
        })
    })

    test('matches snapshot', () => {
        expect(wrapper.element).toMatchSnapshot()
    })

    test('fido is initialized', () => {
        expect(createTranslations).toBeCalled()
        expect(startFido2).toBeCalled()
    })

    test('method login works correctly', () => {
        const form_mock = jest.fn(() => ({ value: wrapper.vm.$cookies.get() }))
        document.querySelector = jest.fn(() => ({ querySelector: form_mock }))
        wrapper.vm.login()

        expect(document.querySelector).toHaveBeenCalledWith(`#${DJANGO_FIDO_FORM_ID}`)
        expect(form_mock).toHaveBeenCalledWith('[name="csrfmiddlewaretoken"]')
    })
})
