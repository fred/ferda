import { initWrapper } from 'ferda-test/TestUtils'
import AppLogin from 'ferda/auth/AppLogin.vue'

describe('AppLogin', () => {
    let wrapper
    const initial_selector = document.querySelector

    beforeEach(() => {
        wrapper = initWrapper(AppLogin, {
            propsData: {
                form_errors: {},
            },
            mocks: {
                '$cookies': {
                    get: () => jest.fn(),
                },
            },
        })
    })

    afterEach(() => {
        document.querySelector = initial_selector
    })

    test('matches snapshot', () => {
        expect(wrapper.element).toMatchSnapshot()
    })

    test('method login works correctly', () => {
        const form_mock = jest.fn(() => ({ value: wrapper.vm.$cookies.get() }))
        document.querySelector = jest.fn(() => ({ querySelector: form_mock }))
        wrapper.vm.login()

        expect(document.querySelector).toHaveBeenCalledWith('#login-form')
        expect(form_mock).toHaveBeenCalledWith('[name="csrfmiddlewaretoken"]')
    })
})
