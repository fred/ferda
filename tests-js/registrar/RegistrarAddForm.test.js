import axios from 'axios'
import MockAdapter from 'axios-mock-adapter'
import flushPromises from 'flush-promises'

import { REGISTRAR, REGISTRAR_LIST } from 'ferda-test/TestObjects'
import { getClonedStoreOptions, initWrapper } from 'ferda-test/TestUtils'
import { URLS } from 'ferda/Urls'
import RegistrarAddForm from 'ferda/registrar/RegistrarAddForm.vue'

describe('RegistrarAddForm', () => {
    let axios_mock
    let wrapper
    let store
    const initial_console = console.log

    beforeEach(async() => {
        axios_mock = new MockAdapter(axios)
        axios_mock.onGet(URLS.countries_list()).reply(200, {
            'CZ': 'Czechia',
            'AL': 'Albania',
            'US': 'United States of America',
        })
        store = getClonedStoreOptions()
        store.modules.general.getters.permissions = () => ['ferda.can_edit_registrar']

        wrapper = initWrapper(RegistrarAddForm, {
            propsData: {
                loading: false,
                registrars: REGISTRAR_LIST,
                registrar_errors: { detail: 'Nevalidní data', fields: ['name'] },
            },
            data() {
                return {
                    company_registration_number: '',
                    emails: ['council@example.org'],
                    fax: '',
                    is_system_registrar: false,
                    is_vat_payer: true,
                    is_internal: false,
                    name: 'Jedi Council',
                    organization: 'Jedi Order',
                    payment_memo_regex: '',
                    street1: 'High council chamber',
                    street2:'',
                    street3: '',
                    city: 'Alderan',
                    postal_code: '42000',
                    state_or_province: '',
                    country_code: 'AL',
                    registrar_handle: 'REG-EMPIRE',
                    telephone: '',
                    url: 'www.example.org',
                    variable_symbol: '42',
                    vat_identification_number: '',
                }
            },
            mocks: {
                $notify: jest.fn(),
            },
        })
        wrapper.vm.$refs.form.validate = jest.fn()
        wrapper.vm.$refs.emails.blur = jest.fn()
        await flushPromises()
    })

    afterEach(() => {
        console.log = initial_console
    })

    test('matches snapshot', () => {
        expect(wrapper.element).toMatchSnapshot()
    })

    test('matches empty snapshot', () => {
        wrapper = initWrapper(RegistrarAddForm)
        expect(wrapper.element).toMatchSnapshot()
    })

    test('method set_countries fails by server error', async() => {
        console.log = jest.fn()
        axios_mock.onGet(URLS.countries_list()).reply(500)
        wrapper = initWrapper(RegistrarAddForm)
        wrapper.vm.$notify = jest.fn()
        await wrapper.vm.set_countries()
        expect(wrapper.vm.countries).toStrictEqual([])
        expect(wrapper.vm.$notify).toHaveBeenCalledWith(wrapper.vm.ERROR_SOMETHING_WENT_WRONG)
        expect(console.log).toHaveBeenCalledWith('error: notifications:something_went_wrong')
    })

    test('method submit_dialog works correctly - is valid', async() => {
        wrapper.vm.$refs = {
            form: { validate: jest.fn(() => true) },
            emails: { blur: jest.fn() },
        }
        await wrapper.vm.submit_dialog()

        const expected_result = { ...REGISTRAR, place: { ...REGISTRAR.place, street: [REGISTRAR.place.street[0]] }}
        delete expected_result.registrar_id
        expect(wrapper.emitted('submit-dialog')[0][0]).toStrictEqual(expected_result)
    })

    test('method submit_dialog works correctly - is not valid', async() => {
        await wrapper.setData({ registrar_handle: '  '})
        wrapper.vm.$refs = {
            form: { validate: jest.fn(() => false) },
            emails: { blur: jest.fn() },
        }
        await wrapper.vm.submit_dialog()
        expect(wrapper.emitted('submit-dialog')).toBeFalsy()
    })

    test('prop registrar_errors is empty - reset form', async() => {
        await wrapper.setProps({ registrar_errors: null })
        wrapper.vm.submit_dialog()
        expect(wrapper.vm.registrar_handle).toStrictEqual('')
    })

    test('form is cleared after new registrar is added', async() => {
        wrapper.vm.reset_form = jest.fn()
        wrapper.vm.$refs.form.resetValidation = jest.fn()
        await wrapper.setProps({ registrars: [] })

        expect(wrapper.vm.reset_form).toHaveBeenCalled()
        expect(wrapper.vm.$refs.form.resetValidation).toHaveBeenCalled()
    })
})
