import axios from 'axios'
import MockAdapter from 'axios-mock-adapter'
import flushPromises from 'flush-promises'

import { REGISTRAR, REGISTRAR_DETAIL, REGISTRAR_EMPIRE } from 'ferda-test/TestObjects'
import { getClonedStoreOptions, initWrapper } from 'ferda-test/TestUtils'
import { URLS } from 'ferda/Urls.js'
import RegistrarDetail from 'ferda/registrar/RegistrarDetail.vue'

describe('RegistrarDetail', () => {
    let wrapper
    let axios_mock
    const initial_log = console.log
    const store = getClonedStoreOptions()
    store.modules.general.getters.permissions = () => ['ferda.can_view_registry', 'ferda.can_view_registrar_credit']

    beforeEach(async() => {
        axios_mock = new MockAdapter(axios)
        axios_mock.onGet(URLS.registrar_detail_ajax(REGISTRAR_EMPIRE.registrar_handle)).reply(200, REGISTRAR_DETAIL)

        wrapper = initWrapper(RegistrarDetail, {
            mocks: {
                $notify: jest.fn(),
                $route: { params: { registrar_handle: REGISTRAR_EMPIRE.registrar_handle }},
                $cookies: { get: jest.fn() },
                $router: { replace: jest.fn() },
            },
            customStoreOptions: store,
        })
        await flushPromises()
    })

    afterEach(() => {
        console.log = initial_log
    })

    test('matches snapshot without data', () => {
        expect(wrapper.element).toMatchSnapshot()
    })

    test('matches snapshot with registrar data', async() => {
        await wrapper.setData({ registrar_data: REGISTRAR_DETAIL })
        expect(wrapper.element).toMatchSnapshot()
    })

    test('method get_registrar_detail_data returns correct results', async() => {
        console.log = jest.fn()
        const registrar_detail_url = URLS.registrar_detail_ajax(REGISTRAR_EMPIRE.registrar_handle)

        expect(await wrapper.vm.get_registrar_detail_data(registrar_detail_url))
            .toStrictEqual(REGISTRAR_DETAIL)
        expect(wrapper.vm.$notify).not.toHaveBeenCalled()
        expect(console.log).not.toHaveBeenCalled()
    })

    test('method get_registrar_detail_data fails - server error', async() => {
        console.log = jest.fn()
        axios_mock.onGet(URLS.registrar_detail_ajax(REGISTRAR_EMPIRE.registrar_handle)).reply(500)

        expect(
            await wrapper.vm.get_registrar_detail_data(URLS.registrar_detail_ajax(REGISTRAR_EMPIRE.registrar_handle)),
        ).toBeNull()
        expect(wrapper.vm.$notify).toHaveBeenCalledWith(wrapper.vm.ERROR_SOMETHING_WENT_WRONG)
        expect(console.log).toHaveBeenCalledWith('error: notifications:something_went_wrong')
    })

    test('method get_registrar_detail_data fails - registrar not found', async() => {
        console.log = jest.fn()
        axios_mock.onGet(URLS.registrar_detail_ajax(REGISTRAR_EMPIRE.registrar_handle)).reply(404, {
            detail: 'Registrar does not exist.',
        })
        const result = await wrapper.vm.get_registrar_detail_data(
            URLS.registrar_detail_ajax(REGISTRAR_EMPIRE.registrar_handle),
        )

        expect(result).toStrictEqual({ not_found_message: 'Registrar does not exist.' })
        expect(console.log).not.toHaveBeenCalled()
        expect(wrapper.vm.$notify).not.toHaveBeenCalled()
    })

    test('method get_registrar_detail fails - registrar not found', async() => {
        const original_data = wrapper.vm.registrar_data
        wrapper.vm.get_registrar_detail_data = jest.fn(() => ({ not_found_message: 'Registrar does not exist.' }))
        const result = await wrapper.vm.get_registrar_detail(REGISTRAR_EMPIRE.registrar_handle)

        expect(result).toStrictEqual({ not_found_message: 'Registrar does not exist.' })
        expect(wrapper.vm.results_loading).toStrictEqual(false)
        expect(wrapper.vm.registrar_data).toStrictEqual(original_data)
    })

    test('method get_registrar_detail_data is aborted', async() => {
        console.log = jest.fn()
        wrapper.vm.get_registrar_detail_data(URLS.registrar_detail_ajax(REGISTRAR_EMPIRE.registrar_handle))
        wrapper.vm.stop_search()
        await flushPromises()

        expect(wrapper.vm.$notify).toHaveBeenCalledWith(wrapper.vm.INFO_REQUEST_ABORTED)
        expect(console.log).toHaveBeenCalledWith('info: notifications:search_aborted')
    })

    test('method get_registrar_detail works correctly', async() => {
        await wrapper.setData({ registrar_data: REGISTRAR_DETAIL })
        await wrapper.vm.get_registrar_detail(REGISTRAR_EMPIRE.registrar_handle)

        expect(wrapper.vm.registrar_data).toStrictEqual(REGISTRAR_DETAIL)
        expect(wrapper.vm.results_loading).toBeFalsy()
    })

    test('method get_registrar_detail fails', async() => {
        console.log = jest.fn()
        axios_mock.onGet(URLS.registrar_detail_ajax(REGISTRAR_EMPIRE.registrar_handle)).reply(500)
        const initial_registrar_data = wrapper.vm.registrar_data
        await wrapper.vm.get_registrar_detail(REGISTRAR_EMPIRE.registrar_handle)

        expect(wrapper.vm.registrar_data).toStrictEqual(initial_registrar_data)
        expect(wrapper.vm.results_loading).toBeFalsy()
        expect(wrapper.vm.$notify).toHaveBeenCalledWith(wrapper.vm.ERROR_SOMETHING_WENT_WRONG)
        expect(console.log).toHaveBeenCalledWith('error: notifications:something_went_wrong')
    })

    test('method update_registrar fails by server error - update failed', async() => {
        axios_mock.onPut(URLS.registrar_update_ajax(REGISTRAR_EMPIRE.registrar_handle)).reply(500)
        console.log = jest.fn()

        await wrapper.vm.update_registrar({ registrar: REGISTRAR })

        expect(wrapper.vm.registrar_data_loading).toStrictEqual(false)
    })

    test('method update_registrar fails by server error - server validation failed', async() => {
        const error_data = { data: 'test_data' }
        axios_mock.onPut(URLS.registrar_update_ajax(REGISTRAR_EMPIRE.registrar_handle)).reply(400, error_data)
        console.log = jest.fn()

        await wrapper.vm.update_registrar({ registrar: REGISTRAR })

        expect(wrapper.vm.update_errors).toStrictEqual(error_data)
        expect(wrapper.vm.registrar_data_loading).toStrictEqual(false)
    })

    test('method update_registrar fails by server error - refresh of registrar failed', async() => {
        axios_mock.onPut(URLS.registrar_update_ajax(REGISTRAR_EMPIRE.registrar_handle)).reply(200)
        axios_mock.onGet(URLS.registrar_detail_ajax(REGISTRAR_EMPIRE.registrar_handle)).reply(500)
        console.log = jest.fn()

        await wrapper.vm.update_registrar({ registrar: REGISTRAR })

        expect(wrapper.vm.registrar_data_loading).toStrictEqual(false)
        expect(wrapper.vm.update_errors).toStrictEqual(null)
    })

    test('method update_registrar works correctly - update without handle change', async() => {
        axios_mock.onPut(URLS.registrar_update_ajax(REGISTRAR_EMPIRE.registrar_handle)).reply(200)
        console.log = jest.fn()

        await wrapper.vm.update_registrar({ registrar: REGISTRAR })

        expect(wrapper.vm.registrar_data_loading).toStrictEqual(false)
        expect(wrapper.vm.$notify).toHaveBeenCalledWith(wrapper.vm.SUCCESS_CHANGES_SAVED)
        expect(console.log).toHaveBeenCalledWith('success: notifications:changes_saved')
    })

    test('method update_registrar works correctly - handle is changed', async() => {
        axios_mock.onPut(URLS.registrar_update_ajax(REGISTRAR_EMPIRE.registrar_handle)).reply(200)
        axios_mock.onGet(URLS.registrar_detail_ajax('CHANGED-HANDLE')).reply(200, REGISTRAR_DETAIL)
        console.log = jest.fn()

        await wrapper.vm.update_registrar({ registrar: { ...REGISTRAR, registrar_handle: 'CHANGED-HANDLE' } })
        expect(wrapper.vm.$router.replace).toHaveBeenCalledWith({ path: '/registry/registrar/CHANGED-HANDLE/' })
        expect(wrapper.vm.registrar_data_loading).toStrictEqual(false)
        expect(wrapper.vm.$notify).toHaveBeenCalledWith(wrapper.vm.SUCCESS_CHANGES_SAVED)
        expect(console.log).toHaveBeenCalledWith('success: notifications:changes_saved')
    })

    test('current_handle has correct value', () => {
        expect(wrapper.vm.current_handle).toBe(REGISTRAR.registrar_handle)
    })
})

describe('RegistrarDetail mounted errors', () => {
    let wrapper
    let axios_mock
    const initial_log = console.log

    beforeEach(() => {
        console.log = jest.fn()
        axios_mock = new MockAdapter(axios)
        axios_mock.onGet(URLS.registrar_detail_ajax(REGISTRAR_EMPIRE.registrar_handle)).reply(200, REGISTRAR_DETAIL)
    })

    afterEach(() => {
        console.log = initial_log
    })

    test('fetching data failed during component mounting - server error', async() => {
        axios_mock.onGet(URLS.registrar_detail_ajax(REGISTRAR_EMPIRE.registrar_handle)).reply(500)
        wrapper = initWrapper(RegistrarDetail, {
            mocks: { $notify: jest.fn(), $route: { params: { registrar_handle: REGISTRAR_EMPIRE.registrar_handle }} },
        })
        await flushPromises()

        expect(wrapper.vm.$notify).toHaveBeenCalledWith(wrapper.vm.ERROR_SOMETHING_WENT_WRONG)
        expect(console.log).toHaveBeenCalledWith('error: notifications:something_went_wrong')
        expect(wrapper.vm.registrar_not_found_message).toBeNull()
    })

    test('fetching data failed during component mounting - not found error', async() => {
        axios_mock.onGet(URLS.registrar_detail_ajax(REGISTRAR_EMPIRE.registrar_handle)).reply(404, {
            detail: 'Registrar does not exist.',
        })
        wrapper = initWrapper(RegistrarDetail, {
            mocks: { $notify: jest.fn(), $route: { params: { registrar_handle: REGISTRAR_EMPIRE.registrar_handle }} },
        })
        await flushPromises()

        expect(wrapper.element).toMatchSnapshot()
        expect(wrapper.vm.$notify).not.toHaveBeenCalled()
        expect(console.log).not.toHaveBeenCalled()
        expect(wrapper.vm.registrar_not_found_message).toBe('Registrar does not exist.')
    })
})
