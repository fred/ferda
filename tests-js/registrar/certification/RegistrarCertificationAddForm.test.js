import { initWrapper } from 'ferda-test/TestUtils'
import RegistrarCertificationAddForm from 'ferda/registrar/certification/RegistrarCertificationAddForm.vue'

describe('RegistrarCertificationAddForm', () => {
    let wrapper
    const initial_console = console.log

    beforeEach(() => {
        jest.useFakeTimers()
        jest.setSystemTime(new Date('2023-04-19T12:01:00+00:00'))
        wrapper = initWrapper(RegistrarCertificationAddForm, {
            propsData: {
                is_loading: false,
                certification_errors: { fields: ['test'], detail: 'error'},
            },
            data() {
                return {
                    classification: 55,
                    file: new File(['test'], 'test.pdf', { type: 'application/pdf '}),
                    valid_from: '2023-04-20T00:00:00Z',
                    valid_to: '2023-08-21T10:00:00Z',
                }
            },
        })

        wrapper.vm.$refs.form.validate = jest.fn()
        wrapper.vm.$refs.form.reset = jest.fn()
    })

    afterEach(() => {
        console.log = initial_console
        jest.runOnlyPendingTimers()
        jest.useRealTimers()
    })

    test('matches snapshot', () => {
        expect(wrapper.element).toMatchSnapshot()
    })

    test('matches empty snapshot', () => {
        wrapper = initWrapper(RegistrarCertificationAddForm)
        expect(wrapper.element).toMatchSnapshot()
    })

    test('method submit_dialog works correctly - is valid', async() => {
        wrapper.vm.$refs = {
            form: { validate: jest.fn(() => true) },
        }

        await wrapper.vm.submit_dialog()

        expect(wrapper.emitted('submit-dialog')[0][0]).toStrictEqual({
            classification: 55,
            file: new File(['test'], 'test.pdf', { type: 'application/pdf '}),
            valid_from: '2023-04-19T00:00:00+00:00',
            valid_to: '2023-08-21T10:00:00Z',
        })
    })

    test('method submit_dialog works correctly - is valid and valid_to is null', async() => {
        wrapper.vm.$refs = {
            form: { validate: jest.fn(() => true) },
        }
        await wrapper.setData({ valid_to: null })

        await wrapper.vm.submit_dialog()

        expect(wrapper.emitted('submit-dialog')[0][0]).toStrictEqual({
            classification: 55,
            file: new File(['test'], 'test.pdf', { type: 'application/pdf '}),
            valid_from: '2023-04-19T00:00:00+00:00',
            valid_to: null,
        })
    })

    test('method submit_dialog works correctly - is not valid', async() => {
        await wrapper.setData({ classification: null })
        wrapper.vm.$refs = {
            form: { validate: jest.fn(() => false) },
        }
        await wrapper.vm.submit_dialog()
        expect(wrapper.emitted('submit-dialog')).toBeFalsy()
    })

    test('prop certification_errors is empty - reset form', async() => {
        wrapper.vm.$refs.form.reset = jest.fn()
        await wrapper.setProps({ certification_errors: null })
        expect(wrapper.vm.$refs.form.reset).toHaveBeenCalled()
    })

    test('Sets todays date - after open dialog', async() => {
        await wrapper.setProps({ dialog_opened: true })
        expect(wrapper.vm.valid_from).toStrictEqual('2023-04-19T00:00:00+00:00')
    })
})
