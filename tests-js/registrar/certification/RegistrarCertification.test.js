import axios from 'axios'
import MockAdapter from 'axios-mock-adapter'
import flushPromises from 'flush-promises'

import { REGISTRAR_CERTIFICATIONS } from 'ferda-test/TestObjects'
import { getClonedStoreOptions, initWrapper } from 'ferda-test/TestUtils'
import { URLS } from 'ferda/Urls.js'
import RegistrarCertification from 'ferda/registrar/certification/RegistrarCertification.vue'

describe('RegistrarCertification', () => {
    let wrapper
    let axios_mock
    const initial_console = console.log
    const store = getClonedStoreOptions()
    store.modules.general.getters.permissions = () => ['ferda.can_edit_registrar']

    beforeEach(async() => {
        axios_mock = new MockAdapter(axios)
        jest.useFakeTimers()
        jest.setSystemTime(new Date('2022-01-01T12:01:00+00:00'))

        axios_mock.onGet(URLS.registrar_certification_ajax(REGISTRAR_CERTIFICATIONS[0].registrar_handle))
            .reply(200, REGISTRAR_CERTIFICATIONS)
        axios_mock.onPost(URLS.registrar_update_certification_ajax(
            REGISTRAR_CERTIFICATIONS[0].registrar_handle, REGISTRAR_CERTIFICATIONS[0].certification_id))
            .reply(200, REGISTRAR_CERTIFICATIONS[0])
        axios_mock.onDelete(URLS.registrar_update_certification_ajax(
            REGISTRAR_CERTIFICATIONS[0].registrar_handle, REGISTRAR_CERTIFICATIONS[0].certification_id))
            .reply(200, REGISTRAR_CERTIFICATIONS[0])
        wrapper = initWrapper(RegistrarCertification, {
            propsData: {
                registrar_handle: REGISTRAR_CERTIFICATIONS[0].registrar_handle,
            },
            mocks: {
                $notify: jest.fn(),
                $cookies: { get: jest.fn() },
            },
            customStoreOptions: store,
        })
        await flushPromises()
    })

    afterEach(() => {
        console.log = initial_console
        jest.runOnlyPendingTimers()
        jest.useRealTimers()
        jest.clearAllMocks()
    })

    test('matches snapshot', () => {
        expect(wrapper.element).toMatchSnapshot()
    })

    test('matches empty snapshot', async() => {
        await wrapper.setData({ certifications: [] })
        expect(wrapper.element).toMatchSnapshot()
    })

    test('method get_certification fails', async() => {
        console.log = jest.fn()

        axios_mock.onGet(URLS.registrar_certification_ajax(REGISTRAR_CERTIFICATIONS[0].registrar_handle)).reply(500)
        const initial_certifications = wrapper.vm.certifications
        await wrapper.vm.get_certifications()

        expect(wrapper.vm.certifications).toStrictEqual(initial_certifications)
        expect(wrapper.vm.certification_loading).toStrictEqual(false)
        expect(wrapper.vm.$notify).toHaveBeenCalledWith(wrapper.vm.ERROR_SOMETHING_WENT_WRONG)
        expect(console.log).toHaveBeenCalledWith('error: notifications:something_went_wrong')
    })

    test('method get_certifications works correctly', async() => {
        console.log = jest.fn()
        await wrapper.vm.get_certifications()

        expect(wrapper.vm.certification_loading).toStrictEqual(false)
        expect(wrapper.vm.$notify).not.toHaveBeenCalled()
        expect(console.log).not.toHaveBeenCalled()
    })

    test('method add_certification fails', async() => {
        console.log = jest.fn()
        axios_mock.onGet(URLS.registrar_certification_ajax(REGISTRAR_CERTIFICATIONS[0].registrar_handle)).reply(500)
        const initial_certifications = wrapper.vm.certifications

        await wrapper.vm.add_certification(new FormData())

        expect(wrapper.vm.certifications).toStrictEqual(initial_certifications)
        expect(wrapper.vm.certification_loading).toStrictEqual(false)
        expect(wrapper.vm.$notify).toHaveBeenCalledWith(wrapper.vm.ERROR_SOMETHING_WENT_WRONG)
        expect(console.log).toHaveBeenCalledWith('error: notifications:something_went_wrong')
    })

    test('method add_certification works correctly', async() => {
        console.log = jest.fn()
        jest.spyOn(wrapper.vm, 'get_certifications').mockReturnValue(REGISTRAR_CERTIFICATIONS)

        axios_mock.onPost(URLS.registrar_certification_ajax(REGISTRAR_CERTIFICATIONS[0].registrar_handle))
            .reply(200, REGISTRAR_CERTIFICATIONS[0])
        await wrapper.vm.add_certification(new FormData())

        expect(wrapper.vm.certification_loading).toStrictEqual(false)
        expect(wrapper.vm.$notify).toHaveBeenCalledWith(wrapper.vm.SUCCESS_CHANGES_SAVED)
        expect(console.log).toHaveBeenCalledWith('success: notifications:changes_saved')
    })

    test('method add_certification works correctly - certifications is null', async() => {
        console.log = jest.fn()
        jest.spyOn(wrapper.vm, 'get_certifications').mockReturnValue(null)
        axios_mock.onPost(URLS.registrar_certification_ajax(REGISTRAR_CERTIFICATIONS[0].registrar_handle)).reply(200)
        await wrapper.vm.add_certification(new FormData())

        expect(wrapper.vm.$notify).not.toHaveBeenCalledWith(wrapper.vm.SUCCESS_CHANGES_SAVED)
    })

    test('method add_certification fails by server error - 400', async() => {
        console.log = jest.fn()
        const api_error = {detail: 'Invalid data', fields: ['name']}
        axios_mock.onPost(URLS.registrar_certification_ajax(REGISTRAR_CERTIFICATIONS[0].registrar_handle))
            .reply(400, api_error)
        await wrapper.vm.add_certification(REGISTRAR_CERTIFICATIONS[0])

        expect(wrapper.vm.$notify).not.toHaveBeenCalled()
        expect(console.log).not.toHaveBeenCalled()
        expect(wrapper.vm.certification_errors).toStrictEqual(api_error)
    })

    test('method get_classification_stars works correctly', () => {
        expect(wrapper.vm.get_classification_stars(REGISTRAR_CERTIFICATIONS[0].classification)).toStrictEqual(0)
        expect(wrapper.vm.get_classification_stars(REGISTRAR_CERTIFICATIONS[1].classification)).toStrictEqual(1)
        expect(wrapper.vm.get_classification_stars(65)).toStrictEqual(2)
        expect(wrapper.vm.get_classification_stars(75)).toStrictEqual(3)
        expect(wrapper.vm.get_classification_stars(87)).toStrictEqual(4)
        expect(wrapper.vm.get_classification_stars(95)).toStrictEqual(5)
    })

    test('method edit_certification fails', async() => {
        console.log = jest.fn()
        axios_mock.onPost(URLS.registrar_update_certification_ajax(
            REGISTRAR_CERTIFICATIONS[0].registrar_handle, REGISTRAR_CERTIFICATIONS[0].certification_id)).reply(500)
        const initial_certifications = wrapper.vm.certifications

        await wrapper.vm.edit_certification(new FormData())

        expect(wrapper.vm.certifications).toStrictEqual(initial_certifications)
        expect(wrapper.vm.certification_loading).toStrictEqual(false)
        expect(wrapper.vm.$notify).toHaveBeenCalledWith(wrapper.vm.ERROR_SOMETHING_WENT_WRONG)
        expect(console.log).toHaveBeenCalledWith('error: notifications:something_went_wrong')
    })

    test('method edit_certification works correctly', async() => {
        console.log = jest.fn()
        jest.spyOn(wrapper.vm, 'get_certifications').mockReturnValue(REGISTRAR_CERTIFICATIONS)
        await wrapper.setData({ certification_data: REGISTRAR_CERTIFICATIONS[0]})
        await wrapper.vm.edit_certification(new FormData())

        expect(wrapper.vm.certification_loading).toStrictEqual(false)
        expect(wrapper.vm.$notify).toHaveBeenCalledWith(wrapper.vm.SUCCESS_CHANGES_SAVED)
        expect(console.log).toHaveBeenCalledWith('success: notifications:changes_saved')
    })

    test('method edit_certification works correctly - certifications is null', async() => {
        console.log = jest.fn()
        jest.spyOn(wrapper.vm, 'get_certifications').mockReturnValue(null)
        await wrapper.setData({ certification_data: REGISTRAR_CERTIFICATIONS[0]})
        axios_mock.onPost(URLS.registrar_update_certification_ajax(
            REGISTRAR_CERTIFICATIONS[0].registrar_handle, REGISTRAR_CERTIFICATIONS[0].certification_id)).reply(200)
        await wrapper.vm.edit_certification(new FormData())

        expect(wrapper.vm.$notify).not.toHaveBeenCalledWith(wrapper.vm.SUCCESS_CHANGES_SAVED)
    })

    test('method edit_certification fails by server error - 400', async() => {
        console.log = jest.fn()
        jest.spyOn(wrapper.vm, 'get_certifications').mockReturnValue(null)
        await wrapper.setData({ certification_data: REGISTRAR_CERTIFICATIONS[0]})
        const api_error = {detail: 'Invalid data', fields: ['classification']}
        axios_mock.onPost(URLS.registrar_update_certification_ajax(
            REGISTRAR_CERTIFICATIONS[0].registrar_handle, REGISTRAR_CERTIFICATIONS[0].certification_id))
            .reply(400, api_error)
        await wrapper.vm.edit_certification(new FormData())

        expect(wrapper.vm.$notify).not.toHaveBeenCalled()
        expect(console.log).not.toHaveBeenCalled()
        expect(wrapper.vm.certification_errors).toStrictEqual(api_error)
    })

    test('method delete_certification fails', async() => {
        console.log = jest.fn()
        axios_mock.onDelete(URLS.registrar_update_certification_ajax(
            REGISTRAR_CERTIFICATIONS[0].registrar_handle, REGISTRAR_CERTIFICATIONS[0].certification_id)).reply(500)
        const initial_certifications = wrapper.vm.certifications
        await wrapper.vm.delete_certification()

        expect(wrapper.vm.certifications).toStrictEqual(initial_certifications)
        expect(wrapper.vm.certification_loading).toStrictEqual(false)
        expect(wrapper.vm.$notify).toHaveBeenCalledWith(wrapper.vm.ERROR_SOMETHING_WENT_WRONG)
        expect(console.log).toHaveBeenCalledWith('error: notifications:something_went_wrong')
    })

    test('method delete_certification works correctly', async() => {
        console.log = jest.fn()
        jest.spyOn(wrapper.vm, 'get_certifications').mockReturnValue(REGISTRAR_CERTIFICATIONS)
        await wrapper.setData({ certification_data: REGISTRAR_CERTIFICATIONS[0]})
        await wrapper.vm.delete_certification()

        expect(wrapper.vm.certification_loading).toStrictEqual(false)
        expect(wrapper.vm.$notify).toHaveBeenCalledWith(wrapper.vm.SUCCESS_CHANGES_SAVED)
        expect(console.log).toHaveBeenCalledWith('success: notifications:changes_saved')
    })

    test('method delete_certification works correctly - certifications is null', async() => {
        console.log = jest.fn()
        jest.spyOn(wrapper.vm, 'get_certifications').mockReturnValue(null)
        await wrapper.setData({ certification_data: REGISTRAR_CERTIFICATIONS[0]})
        axios_mock.onDelete(URLS.registrar_update_certification_ajax(
            REGISTRAR_CERTIFICATIONS[0].registrar_handle, REGISTRAR_CERTIFICATIONS[0].certification_id)).reply(200)
        await wrapper.vm.delete_certification()

        expect(wrapper.vm.$notify).not.toHaveBeenCalledWith(wrapper.vm.SUCCESS_CHANGES_SAVED)
    })

    test('certification_title has correct values', async() => {
        await wrapper.setData({ last_action_type: wrapper.vm.CERTIFICATION_ACTION.ADD })
        expect(wrapper.vm.certification_title).toBe('registrar:add_certification')

        await wrapper.setData({ last_action_type: wrapper.vm.CERTIFICATION_ACTION.EDIT })
        expect(wrapper.vm.certification_title).toBe('registrar:edit_certification')

        await wrapper.setData({ last_action_type: wrapper.vm.CERTIFICATION_ACTION.DELETE })
        expect(wrapper.vm.certification_title).toBe('registrar:delete_certification.title')

        await wrapper.setData({ last_action_type: 'something else' })
        expect(wrapper.vm.certification_title).toBe('')
    })

    test('method show_certification_form works correctly - with certification_data', () => {
        wrapper.vm.show_certification_form(wrapper.vm.CERTIFICATION_ACTION.ADD, REGISTRAR_CERTIFICATIONS[0])

        expect(wrapper.vm.certification_data).toBe(REGISTRAR_CERTIFICATIONS[0])
        expect(wrapper.vm.last_action_type).toBe(wrapper.vm.CERTIFICATION_ACTION.ADD)
        expect(wrapper.vm.dialog_opened).toStrictEqual(true)
    })

    test('method show_certification_form works correctly - without certification_data', () => {
        wrapper.vm.show_certification_form(wrapper.vm.CERTIFICATION_ACTION.ADD)

        expect(wrapper.vm.certification_data).toStrictEqual(null)
        expect(wrapper.vm.last_action_type).toBe(wrapper.vm.CERTIFICATION_ACTION.ADD)
        expect(wrapper.vm.dialog_opened).toStrictEqual(true)
    })

    test('method can_delete_certification works correctly', () => {
        expect(wrapper.vm.can_delete_certification({ valid_from: '2023-01-01T12:00:00+00:00'})).toStrictEqual(true)
        expect(wrapper.vm.can_delete_certification({ valid_from: '2022-01-01T12:00:00+00:00'})).toStrictEqual(false)
    })

    test('method can_edit_certification works correctly', () => {
        expect(wrapper.vm.can_edit_certification({ valid_to: null })).toStrictEqual(true)
        expect(wrapper.vm.can_edit_certification({ valid_to: '2023-01-01T12:00:00+00:00' })).toStrictEqual(true)
        expect(wrapper.vm.can_edit_certification({ valid_to: '2022-01-01T12:00:00+00:00' })).toStrictEqual(false)
    })
})
