import { REGISTRAR_CERTIFICATIONS } from 'ferda-test/TestObjects'
import { initWrapper } from 'ferda-test/TestUtils'
import RegistrarCertificationDeleteForm from 'ferda/registrar/certification/RegistrarCertificationDeleteForm.vue'

describe('RegistrarCertificationDeleteForm', () => {
    let wrapper
    const initial_console = console.log

    beforeEach(() => {
        wrapper = initWrapper(RegistrarCertificationDeleteForm, {
            propsData: {
                is_loading: false,
                certification: REGISTRAR_CERTIFICATIONS[0],
            },
        })
    })

    afterEach(() => {
        console.log = initial_console
    })

    test('matches snapshot', () => {
        expect(wrapper.element).toMatchSnapshot()
    })

    test('matches empty snapshot', () => {
        wrapper = initWrapper(RegistrarCertificationDeleteForm, {
            propsData: {
                certification: {},
            },
        })
        expect(wrapper.element).toMatchSnapshot()
    })
})
