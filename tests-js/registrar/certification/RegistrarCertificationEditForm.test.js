import { REGISTRAR_CERTIFICATIONS } from 'ferda-test/TestObjects'
import { initWrapper } from 'ferda-test/TestUtils'
import RegistrarCertificationEditForm from 'ferda/registrar/certification/RegistrarCertificationEditForm.vue'

describe('RegistrarCertificationEditForm', () => {
    let wrapper
    const initial_console = console.log

    beforeEach(() => {
        jest.useFakeTimers()
        jest.setSystemTime(new Date('2023-04-19T12:01:00+00:00'))
        wrapper = initWrapper(RegistrarCertificationEditForm, {
            propsData: {
                is_loading: false,
                certification: REGISTRAR_CERTIFICATIONS[0],
                dialog_opened: false,
                certification_errors: { fields: ['test'], detail: 'error'},
            },
        })
        wrapper.vm.$refs.form.validate = jest.fn()
        wrapper.vm.$refs.form.reset = jest.fn()
    })

    afterEach(() => {
        console.log = initial_console
        jest.runOnlyPendingTimers()
        jest.useRealTimers()
    })

    test('matches snapshot', () => {
        expect(wrapper.element).toMatchSnapshot()
    })

    test('matches empty snapshot', () => {
        wrapper = initWrapper(RegistrarCertificationEditForm, {
            propsData: {
                certification: {},
            },
        })
        expect(wrapper.element).toMatchSnapshot()
    })

    test('method submit_dialog works correctly - is valid', async() => {
        wrapper.vm.$refs = {
            form: { validate: jest.fn(() => true) },
        }
        await wrapper.setData({
            classification: 55,
            file: new File(['test'], 'test.pdf', { type: 'application/pdf '}),
            valid_to: '2023-08-21T10:00:00Z',
        })
        await wrapper.vm.submit_dialog()

        expect(wrapper.emitted('submit-dialog')[0][0]).toStrictEqual({
            classification: 55,
            file: new File(['test'], 'test.pdf', { type: 'application/pdf '}),
            valid_to: '2023-08-21T10:00:00Z',
        })
    })

    test('method submit_dialog works correctly - is not valid', async() => {
        await wrapper.setData({ valid_to: '2020-08-21' })
        wrapper.vm.$refs = {
            form: { validate: jest.fn(() => false) },
        }
        await wrapper.vm.submit_dialog()
        expect(wrapper.emitted('submit-dialog')).toBeFalsy()
    })

    test('prop certification_errors is empty - reset form', async() => {
        wrapper.vm.$refs.form.reset = jest.fn()
        await wrapper.setProps({ certification_errors: null })
        expect(wrapper.vm.$refs.form.reset).toHaveBeenCalled()
    })

    test('valid_to is updated accordingly to certification change', async() => {
        await wrapper.setProps({
            dialog_opened: true,
            certification: { ...REGISTRAR_CERTIFICATIONS[0], valid_to: '2025-08-21T10:00:00Z' },
        })
        expect(wrapper.vm.valid_to).toStrictEqual('2025-08-20')
    })

    test('field valid_to is not required', async() => {
        await wrapper.setProps({
            dialog_opened: true,
            certification: { ...REGISTRAR_CERTIFICATIONS[0], valid_to: null },
        })
        await wrapper.setData({ valid_to: '2025-08-21T10:00:00Z' })

        expect(wrapper.vm.required).toStrictEqual(false)
    })

    test('field valid_to is required', async() => {
        await wrapper.setProps({
            dialog_opened: true,
            certification: { ...REGISTRAR_CERTIFICATIONS[0], valid_to: '2025-08-21T10:00:00Z' },
        })
        expect(wrapper.vm.required).toStrictEqual(true)
    })
})
