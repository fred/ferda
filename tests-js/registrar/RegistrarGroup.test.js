import axios from 'axios'
import MockAdapter from 'axios-mock-adapter'
import flushPromises from 'flush-promises'

import { initWrapper } from 'ferda-test/TestUtils'
import { URLS } from 'ferda/Urls'
import RegistrarGroup from 'ferda/registrar/RegistrarGroup.vue'

describe('RegistrarGroup', () => {
    let axios_mock
    let wrapper
    const initial_console = console.log

    beforeEach(async() => {
        console.log = jest.fn()
        axios_mock = new MockAdapter(axios)
        axios_mock.onGet(URLS.registrar_available_group_ajax()).reply(200, ['group1', 'group2', 'group3', 'group4'])
        axios_mock.onGet(URLS.registrar_membership_group_ajax('TEST-REGISTRAR')).reply(200, ['group1', 'group2'])
        wrapper = initWrapper(RegistrarGroup, {
            propsData: { registrar_id: 'TEST-REGISTRAR' },
            mocks: { $notify: jest.fn(), $cookies: { get: () => jest.fn(() => 'csrftoken') }},
        })
        await flushPromises()
    })

    afterEach(() => {
        console.log = initial_console
    })

    test('matches snapshot', () => {
        expect(wrapper.element).toMatchSnapshot()
    })

    test('method get_available_groups fails by server error', async() => {
        await wrapper.setData({ available_groups: [] })
        axios_mock.onGet(URLS.registrar_available_group_ajax()).reply(500)
        await wrapper.vm.get_available_groups()

        expect(wrapper.vm.available_groups).toStrictEqual([])
        expect(wrapper.vm.$notify).toHaveBeenCalledWith(wrapper.vm.ERROR_SOMETHING_WENT_WRONG)
        expect(console.log).toHaveBeenCalledWith('error: notifications:something_went_wrong')
    })

    test('method get_available_groups works correctly', async() => {
        await wrapper.setData({ available_groups: [] })
        await wrapper.vm.get_available_groups()

        expect(wrapper.vm.available_groups).toStrictEqual(['group1', 'group2', 'group3', 'group4'])
        expect(wrapper.vm.$notify).not.toHaveBeenCalled()
        expect(console.log).not.toHaveBeenCalled()
    })

    test('method get_registrar_groups fails by server error', async() => {
        await wrapper.setData({ registrar_groups: [] })
        axios_mock.onGet(URLS.registrar_membership_group_ajax('TEST-REGISTRAR')).reply(500)
        await wrapper.vm.get_registrar_groups()

        expect(wrapper.vm.registrar_groups).toStrictEqual([])
        expect(wrapper.vm.$notify).toHaveBeenCalledWith(wrapper.vm.ERROR_SOMETHING_WENT_WRONG)
        expect(console.log).toHaveBeenCalledWith('error: notifications:something_went_wrong')
    })

    test('method get_registrar_groups works correctly', async() => {
        await wrapper.setData({ registrar_groups: [] })
        await wrapper.vm.get_registrar_groups()

        expect(wrapper.vm.registrar_groups).toStrictEqual(['group1', 'group2'])
        expect(wrapper.vm.$notify).not.toHaveBeenCalled()
        expect(console.log).not.toHaveBeenCalled()
    })

    test('method save_groups fails by server error', async() => {
        axios_mock.onPost(URLS.registrar_manage_group_ajax('TEST-REGISTRAR', 'group3')).reply(500)
        axios_mock.onDelete(URLS.registrar_manage_group_ajax('TEST-REGISTRAR', 'group1')).reply(500)
        axios_mock.onGet(URLS.registrar_membership_group_ajax('TEST-REGISTRAR')).reply(500)

        await wrapper.vm.save_groups({ to_be_added: ['group3'], to_be_deleted: ['group1'] })

        expect(wrapper.vm.manage_groups_loading).toStrictEqual(false)
        expect(wrapper.vm.$notify).toHaveBeenCalledWith(wrapper.vm.ERROR_SOMETHING_WENT_WRONG)
        expect(console.log).toHaveBeenCalledWith('error: notifications:something_went_wrong')
    })

    test('method save_groups works correctly', async() => {
        axios_mock.onPost(URLS.registrar_manage_group_ajax('TEST-REGISTRAR', 'group3')).reply(200)
        axios_mock.onDelete(URLS.registrar_manage_group_ajax('TEST-REGISTRAR', 'group1')).reply(200)

        await wrapper.vm.save_groups({ to_be_added: ['group3'], to_be_deleted: ['group1'] })

        expect(wrapper.vm.manage_groups_loading).toStrictEqual(false)
        expect(wrapper.vm.$notify).toHaveBeenCalledWith(wrapper.vm.SUCCESS_CHANGES_SAVED)
        expect(console.log).toHaveBeenCalledWith('success: notifications:changes_saved')
    })
})
