import axios from 'axios'
import MockAdapter from 'axios-mock-adapter'
import flushPromises from 'flush-promises'

import { getClonedStoreOptions, initWrapper } from 'ferda-test/TestUtils'
import { URLS } from 'ferda/Urls.js'
import RegistrarZoneAccess from 'ferda/registrar/RegistrarZoneAccess.vue'

import { EDIT_ZONE_ACCESS, REGISTRAR_EMPIRE, ZONES, ZONE_ACCESS } from '../TestObjects'

describe('RegistrarZoneAccess', () => {
    let wrapper
    let axios_mock
    const initial_console = console.log
    const store = getClonedStoreOptions()
    store.modules.general.getters.permissions = () => ['ferda.can_edit_registrar']

    beforeEach(async() => {
        axios_mock = new MockAdapter(axios)
        jest.useFakeTimers()
        jest.setSystemTime(new Date('2022-01-01T12:01:00+00:00'))

        axios_mock.onGet(URLS.registrar_zone_access(REGISTRAR_EMPIRE.registrar_handle)).reply(200, ZONES)
        axios_mock.onPost(URLS.registrar_zone_access(REGISTRAR_EMPIRE.registrar_handle)).reply(200, ZONE_ACCESS)

        wrapper = initWrapper(RegistrarZoneAccess, {
            propsData: {
                zones: { test: 0, test2: 0 },
                registrar_handle: REGISTRAR_EMPIRE.registrar_handle,
            },
            data() {
                return { dialog_opened: true }
            },
            mocks: {
                $notify: jest.fn(),
                $cookies: { get: jest.fn() },
            },
            customStoreOptions: store,
        })
        await flushPromises()
    })

    afterEach(() => {
        console.log = initial_console
        jest.runOnlyPendingTimers()
        jest.useRealTimers()
    })

    test('matches snapshot', () => {
        expect(wrapper.element).toMatchSnapshot()
    })

    test('method get_zone_access works correctly', () => {
        wrapper.vm.$notify = jest.fn()
        wrapper.vm.get_zone_access()

        expect(wrapper.vm.is_loading).toBe(false)
        expect(wrapper.vm.$notify).not.toHaveBeenCalled()
    })

    test('method get_zone_access fails', async() => {
        console.log = jest.fn()
        wrapper.vm.$notify = jest.fn()

        axios_mock.onGet(URLS.registrar_zone_access(REGISTRAR_EMPIRE.registrar_handle)).reply(500)
        const initial_zone_access = wrapper.vm.zone_access
        await wrapper.vm.get_zone_access()

        expect(wrapper.vm.zone_access).toStrictEqual(initial_zone_access)
        expect(wrapper.vm.is_loading).toBeFalsy()
        expect(wrapper.vm.$notify).toHaveBeenCalledWith(wrapper.vm.ERROR_SOMETHING_WENT_WRONG)
        expect(console.log).toHaveBeenCalledWith('error: notifications:something_went_wrong')
    })

    test('method can_delete_zone works correctly', () => {
        expect(wrapper.vm.can_delete_zone({ valid_from: '2023-01-01T12:00:00+00:00'})).toStrictEqual(true)
        expect(wrapper.vm.can_delete_zone({ valid_from: '2022-01-01T12:00:00+00:00'})).toStrictEqual(false)
    })

    test('method can_edit_zone works correctly', () => {
        expect(wrapper.vm.can_edit_zone({ valid_to: null })).toStrictEqual(true)
        expect(wrapper.vm.can_edit_zone({ valid_to: '2023-01-01T12:00:00+00:00' })).toStrictEqual(true)
        expect(wrapper.vm.can_edit_zone({ valid_to: '2022-01-01T12:00:00+00:00' })).toStrictEqual(false)
    })

    test('dialog is closed when zone is changed', async() => {
        await wrapper.setData({ zone_access: {
            test: [{
                zone: 'test',
                valid_from: '2021-08-21T10:00:00Z',
                valid_to: '2023-08-21T10:00:00Z',
            }],
        }})
        expect(wrapper.vm.dialog_opened).toStrictEqual(false)
    })

    test('zone_access_title has correct values', async() => {
        await wrapper.setData({ last_action_type: wrapper.vm.ZONE_ACCESS_ACTION.ADD })
        expect(wrapper.vm.zone_access_title).toBe('registrar:add_access')

        await wrapper.setData({ last_action_type: wrapper.vm.ZONE_ACCESS_ACTION.EDIT })
        expect(wrapper.vm.zone_access_title).toBe('registrar:edit_access')

        await wrapper.setData({ last_action_type: wrapper.vm.ZONE_ACCESS_ACTION.DELETE })
        expect(wrapper.vm.zone_access_title).toBe('registrar:delete_access.title')

        await wrapper.setData({ last_action_type: 'something else' })
        expect(wrapper.vm.zone_access_title).toBe('')
    })

    test('method show_zone_access_form works correctly - with form_data', () => {
        wrapper.vm.show_zone_access_form(wrapper.vm.ZONE_ACCESS_ACTION.EDIT, ZONE_ACCESS)

        expect(wrapper.vm.form_data).toBe(ZONE_ACCESS)
        expect(wrapper.vm.last_action_type).toBe(wrapper.vm.ZONE_ACCESS_ACTION.EDIT)
        expect(wrapper.vm.dialog_opened).toStrictEqual(true)
    })

    test('method show_zone_access_form works correctly - without form_data', () => {
        wrapper.vm.show_zone_access_form(wrapper.vm.ZONE_ACCESS_ACTION.ADD)

        expect(wrapper.vm.form_data).toStrictEqual(null)
        expect(wrapper.vm.last_action_type).toBe(wrapper.vm.ZONE_ACCESS_ACTION.ADD)
        expect(wrapper.vm.dialog_opened).toStrictEqual(true)
    })

    test('method add_zone_access works correctly', async() => {
        console.log = jest.fn()
        await wrapper.vm.add_zone_access(ZONE_ACCESS)

        expect(wrapper.vm.$notify).toHaveBeenCalledWith(wrapper.vm.SUCCESS_CHANGES_SAVED)
        expect(console.log).toHaveBeenCalledWith('success: notifications:changes_saved')
    })

    test('method add_zone_access works correctly - zone access is null', async() => {
        console.log = jest.fn()
        wrapper.vm.get_zone_access = jest.fn(() => null)
        await wrapper.vm.add_zone_access(ZONE_ACCESS)

        expect(wrapper.vm.$notify).not.toHaveBeenCalledWith(wrapper.vm.SUCCESS_CHANGES_SAVED)
    })

    test('method add_zone_access fails by server error', async() => {
        wrapper.vm.$notify = jest.fn()
        console.log = jest.fn()

        axios_mock.onPost(URLS.registrar_zone_access(REGISTRAR_EMPIRE.registrar_handle)).reply(500)
        await wrapper.vm.add_zone_access(ZONE_ACCESS)

        expect(wrapper.vm.$notify).toHaveBeenCalledWith(wrapper.vm.ERROR_SOMETHING_WENT_WRONG)
        expect(console.log).toHaveBeenCalledWith('error: notifications:something_went_wrong')
    })

    test('method add_zone_access fails by server error - 400', async() => {
        const api_error = {detail: 'Invalid data', fields: ['name']}
        console.log = jest.fn()
        axios_mock.onPost(URLS.registrar_zone_access(REGISTRAR_EMPIRE.registrar_handle)).reply(400, api_error)
        await wrapper.vm.add_zone_access(ZONE_ACCESS)

        expect(wrapper.vm.$notify).not.toHaveBeenCalled()
        expect(console.log).not.toHaveBeenCalled()
        expect(wrapper.vm.zone_access_errors).toStrictEqual(api_error)
    })

    test('method edit_zone_access works correctly', async() => {
        console.log = jest.fn()
        axios_mock.onPut(URLS.registrar_zone_access(REGISTRAR_EMPIRE.registrar_handle)).reply(200)
        await wrapper.vm.edit_zone_access(EDIT_ZONE_ACCESS)

        expect(wrapper.vm.$notify).toHaveBeenCalledWith(wrapper.vm.SUCCESS_CHANGES_SAVED)
        expect(console.log).toHaveBeenCalledWith('success: notifications:changes_saved')
    })

    test('method edit_zone_access works correctly - zone access is null', async() => {
        wrapper.vm.get_zone_access = jest.fn(() => null)
        axios_mock.onPut(URLS.registrar_zone_access(REGISTRAR_EMPIRE.registrar_handle)).reply(200)
        await wrapper.vm.edit_zone_access(ZONE_ACCESS)

        expect(wrapper.vm.$notify).not.toHaveBeenCalledWith(wrapper.vm.SUCCESS_CHANGES_SAVED)
    })

    test('method edit_zone_access fails by server error', async() => {
        console.log = jest.fn()
        wrapper.setData({ zone_access: ZONES })
        axios_mock.onPut(URLS.registrar_zone_access(REGISTRAR_EMPIRE.registrar_handle)).reply(500)
        await wrapper.vm.edit_zone_access(EDIT_ZONE_ACCESS)

        expect(wrapper.vm.$notify).toHaveBeenCalledWith(wrapper.vm.ERROR_SOMETHING_WENT_WRONG)
        expect(console.log).toHaveBeenCalledWith('error: notifications:something_went_wrong')
    })

    test('method edit_zone_access fails by server error - 400', async() => {
        const api_error = {detail: 'Invalid data', fields: ['name']}
        axios_mock.onPut(URLS.registrar_zone_access(REGISTRAR_EMPIRE.registrar_handle)).reply(400, api_error)
        await wrapper.vm.edit_zone_access(EDIT_ZONE_ACCESS)

        expect(wrapper.vm.zone_access_errors).toStrictEqual(api_error)
    })

    test('method delete_zone_access works correctly', async() => {
        console.log = jest.fn()
        axios_mock.onDelete(URLS.registrar_zone_access(REGISTRAR_EMPIRE.registrar_handle)).reply(200)
        await wrapper.vm.delete_zone_access(ZONE_ACCESS)

        expect(wrapper.vm.$notify).toHaveBeenCalledWith(wrapper.vm.SUCCESS_CHANGES_SAVED)
        expect(console.log).toHaveBeenCalledWith('success: notifications:changes_saved')
    })

    test('method delete_zone_access works correctly - zone access is null', async() => {
        wrapper.vm.get_zone_access = jest.fn(() => null)
        axios_mock.onDelete(URLS.registrar_zone_access(REGISTRAR_EMPIRE.registrar_handle)).reply(200)
        await wrapper.vm.delete_zone_access(ZONE_ACCESS)

        expect(wrapper.vm.$notify).not.toHaveBeenCalledWith(wrapper.vm.SUCCESS_CHANGES_SAVED)
    })

    test('method delete_zone_access fails by server error', async() => {
        console.log = jest.fn()
        axios_mock.onDelete(URLS.registrar_zone_access(REGISTRAR_EMPIRE.registrar_handle)).reply(500)
        await wrapper.vm.delete_zone_access(ZONE_ACCESS)

        expect(wrapper.vm.$notify).toHaveBeenCalledWith(wrapper.vm.ERROR_SOMETHING_WENT_WRONG)
        expect(console.log).toHaveBeenCalledWith('error: notifications:something_went_wrong')
    })
})
