import { getClonedStoreOptions, initWrapper } from 'ferda-test/TestUtils'
import { MESSAGE_FORM_FIELDS, MESSAGE_TYPE } from 'ferda/MessagesMixin'
import RegistrarDetailInfo from 'ferda/registrar/RegistrarDetailInfo.vue'

describe('RegistrarDetailInfo', () => {
    let wrapper
    let store

    beforeEach(() => {
        store = getClonedStoreOptions()
        store.modules.preferences.getters.verbose = () => false

        wrapper = initWrapper(RegistrarDetailInfo, {
            propsData: {
                registrar_data_loading: false,
                registrar: {
                    organization: 'Jedi Order',
                    name: 'Jedi Council',
                    emails: ['council@example.org', 'jedi@example.org'],
                    url: 'https://jedi.example.org/',
                    is_vat_payer: false,
                    is_system_registrar: true,
                    variable_symbol: '42',
                    payment_memo_regex: '',
                    telephone: '',
                    company_registration_number: '',
                    place: {
                        street: ['High council chamber'],
                        city: 'Alderan',
                        postal_code: '42000',
                        state_or_province: null,
                        country_code: 'AL',
                    },
                },
            },
            stubs: {
                FontAwesomeIcon: true,
            },
            customStoreOptions: store,
        })
    })

    test('matches snapshot', () => {
        expect(wrapper.element).toMatchSnapshot()
    })

    test('matches snapshot with verbose', () => {
        store.modules.preferences.getters.verbose = () => true
        wrapper = initWrapper(RegistrarDetailInfo, {
            propsData: {
                registrar_data_loading: false,
                registrar: {
                    organization: 'Jedi Order',
                    name: 'Jedi Council',
                    emails: ['council@example.org', 'jedi@example.org'],
                    url: 'https://jedi.example.org/',
                    is_vat_payer: false,
                    is_system_registrar: true,
                    variable_symbol: '42',
                    place: {
                        street: ['High council chamber'],
                        city: 'Alderan',
                        postal_code: '42000',
                        state_or_province: null,
                        country_code: 'AL',
                    },
                },
            },
            stubs: {
                FontAwesomeIcon: true,
            },
            customStoreOptions: store,
        })
        expect(wrapper.element).toMatchSnapshot()
    })

    test('registrar watcher works correctly', async() => {
        await wrapper.setProps({
            registrar: {
                ...wrapper.vm.registrar,
                organization: 'Test organization',
            },
        })
        await wrapper.vm.$nextTick()

        expect(wrapper.vm.registrar.organization).toBe('Test organization')
    })

    test('property messages_by_emails_params returns correct values', () => {
        const params = wrapper.vm.messages_by_emails_params

        expect(JSON.stringify(params.getAll(MESSAGE_FORM_FIELDS.RECIPIENTS)))
            .toStrictEqual(JSON.stringify(['council@example.org', 'jedi@example.org']))
        expect(params.get(MESSAGE_FORM_FIELDS.MESSAGE_TYPE)).toBe(MESSAGE_TYPE.EMAIL)
    })
})
