import { initWrapper } from 'ferda-test/TestUtils'
import RegistrarZoneEditForm from 'ferda/registrar/RegistrarZoneEditForm.vue'

describe('RegistrarZoneEditForm', () => {
    let wrapper
    const initial_console = console.log

    beforeEach(() => {
        jest.useFakeTimers()
        jest.setSystemTime(new Date('2022-01-01T12:01:00+00:00'))
        wrapper = initWrapper(RegistrarZoneEditForm, {
            propsData: {
                is_loading: false,
                zone_access: {
                    zone: 'test',
                    valid_from: '2020-08-21T10:00:00Z',
                    valid_to: '2023-08-21T10:00:00Z',
                },
                zone_access_errors: { fields: ['test'], detail: 'error'},
                dialog_opened: false,
            },
        })
        wrapper.vm.$refs.form.validate = jest.fn()
        wrapper.vm.$refs.form.reset = jest.fn()
    })

    afterEach(() => {
        console.log = initial_console
        jest.runOnlyPendingTimers()
        jest.useRealTimers()
    })

    test('matches snapshot', () => {
        expect(wrapper.element).toMatchSnapshot()
    })

    test('matches empty snapshot', () => {
        wrapper = initWrapper(RegistrarZoneEditForm, {
            propsData: {
                is_loading: false,
                zone_access: { zone: 'test' },
            },
        })
        expect(wrapper.element).toMatchSnapshot()
    })

    test('method submit_dialog works correctly - is valid', async() => {
        wrapper.vm.$refs = {
            form: { validate: jest.fn(() => true) },
        }
        await wrapper.setData({ valid_to: '2024-08-21' })
        await wrapper.vm.submit_dialog()

        expect(wrapper.emitted('submit-dialog')[0][0]).toStrictEqual({
            access: {
                zone: 'test',
                valid_from: '2020-08-21T10:00:00Z',
                valid_to: '2023-08-21T10:00:00Z',
            },
            valid_from: '2020-08-21T00:00:00Z',
            valid_to: '2024-08-22T00:00:00Z',
        })
    })

    test('method submit_dialog works correctly - is not valid', async() => {
        await wrapper.setData({ valid_to: '2020-08-21' })
        wrapper.vm.$refs = {
            form: { validate: jest.fn(() => false) },
        }
        await wrapper.vm.submit_dialog()
        expect(wrapper.emitted('submit-dialog')).toBeFalsy()
    })

    test('prop zone_access_errors is empty - reset form', async() => {
        wrapper.vm.$refs.form.reset = jest.fn()
        await wrapper.setProps({ zone_access_errors: null })
        expect(wrapper.vm.$refs.form.reset).toHaveBeenCalled()
    })

    test('valid_to is updated accordingly to zone_access change', async() => {
        await wrapper.setProps({ zone_access: {
            zone: 'test',
            valid_from: '2020-08-21T10:00:00Z',
            valid_to: '2025-08-21T10:00:00Z',
        }})
        expect(wrapper.vm.valid_to).toStrictEqual('2025-08-20')
    })

    test('field valid_to is not required', async() => {
        await wrapper.setProps({
            dialog_opened: true,
            zone_access: { zone: 'test', valid_from: '2022-08-21T10:00:00Z', valid_to: null },
        })
        expect(wrapper.vm.required).toStrictEqual(false)
    })

    test('field valid_to is required', async() => {
        await wrapper.setProps({
            dialog_opened: true,
            zone_access: { zone: 'test', valid_from: '2020-08-21T10:00:00Z', valid_to: '2023-08-21T10:00:00Z' },
        })
        expect(wrapper.vm.required).toStrictEqual(true)
    })
})
