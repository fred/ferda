import axios from 'axios'
import MockAdapter from 'axios-mock-adapter'
import flushPromises from 'flush-promises'

import { REGISTRAR, REGISTRAR_LIST } from 'ferda-test/TestObjects'
import { initWrapper } from 'ferda-test/TestUtils'
import { REGISTRAR_ALL_ITEMS } from 'ferda/RegistryMixin'
import { URLS } from 'ferda/Urls'
import RegistrarEditForm from 'ferda/registrar/RegistrarEditForm.vue'

describe('RegistrarEditForm', () => {
    let axios_mock
    let wrapper
    const initial_console = console.log

    beforeEach(async() => {
        axios_mock = new MockAdapter(axios)
        axios_mock.onGet(URLS.countries_list()).reply(200, { AS: 'American Samoa' })
        axios_mock.onGet(URLS.registrar_list_ajax()).reply(200, REGISTRAR_LIST)

        wrapper = initWrapper(RegistrarEditForm, {
            propsData: {
                registrar: REGISTRAR,
                dialog_opened: true,
                dialog_loading: false,
            },
            mocks: { $notify: jest.fn() },
        })
        await flushPromises()
    })

    afterEach(() => {
        console.log = initial_console
    })

    test('matches snapshot', () => {
        expect(wrapper.element).toMatchSnapshot()
    })

    test('property registrar_data has correct value', () => {
        const expected_result = { ...REGISTRAR, place: { ...REGISTRAR.place, street: [REGISTRAR.place.street[0]] }}
        delete expected_result.registrar_id
        expect(wrapper.vm.registrar_data.registrar).toStrictEqual(expected_result)
    })

    test('method get_initial_value returns correct values', () => {
        for (const key of Object.keys(REGISTRAR_ALL_ITEMS)) {
            expect(wrapper.vm.get_initial_value(key)).toBe(REGISTRAR[key])
        }
    })

    test('method submit works correctly - invalid form', async() => {
        wrapper.vm.$refs.emails.blur = jest.fn()
        wrapper.vm.$refs.form = { validate: jest.fn(() => false) }
        await wrapper.vm.submit()

        expect(wrapper.emitted('submit')).toBeFalsy()
    })

    test('method submit works correctly - valid form', async() => {
        wrapper.vm.$refs.emails.blur = jest.fn()
        wrapper.vm.$refs.form = { validate: jest.fn(() => true) }
        await wrapper.vm.submit()

        const expected_result = { ...REGISTRAR, place: { ...REGISTRAR.place, street: [REGISTRAR.place.street[0]] }}
        delete expected_result.registrar_id

        expect(wrapper.emitted('submit')[0][0].registrar).toStrictEqual(expected_result)
    })

    test('method load_countries fails by server error', async() => {
        axios_mock.onGet(URLS.countries_list()).reply(500)
        console.log = jest.fn()
        await wrapper.vm.load_countries()

        expect(wrapper.vm.countries_loading).toStrictEqual(false)
        expect(wrapper.vm.countries).toStrictEqual([{ text: 'American Samoa', value: 'AS' }])
        expect(wrapper.vm.$notify).toHaveBeenCalledWith(wrapper.vm.ERROR_SOMETHING_WENT_WRONG)
        expect(console.log).toHaveBeenCalledWith('error: notifications:something_went_wrong')
    })

    test('method load_countries works correctly', async() => {
        axios_mock.onGet(URLS.countries_list()).reply(200, { AL: 'Albania' })
        console.log = jest.fn()
        await wrapper.vm.load_countries()

        expect(wrapper.vm.countries_loading).toStrictEqual(false)
        expect(wrapper.vm.countries).toStrictEqual([{ text: 'Albania', value: 'AL' }])
        expect(wrapper.vm.$notify).not.toHaveBeenCalled()
        expect(console.log).not.toHaveBeenCalled()
    })

    test('method load_registrar_data fails by server error', async() => {
        axios_mock.onGet(URLS.registrar_list_ajax()).reply(500)
        console.log = jest.fn()
        await wrapper.vm.load_registrar_data()

        expect(wrapper.vm.registrar_loading).toStrictEqual(false)
        expect(wrapper.vm.handles).toStrictEqual(['REG-DUMBLEDORE'])
        expect(wrapper.vm.$notify).toHaveBeenCalledWith(wrapper.vm.ERROR_SOMETHING_WENT_WRONG)
        expect(console.log).toHaveBeenCalledWith('error: notifications:something_went_wrong')
    })

    test('method load_registrar_data works correctly', async() => {
        console.log = jest.fn()
        await wrapper.vm.load_registrar_data()

        expect(wrapper.vm.registrar_loading).toStrictEqual(false)

        expect(wrapper.vm.registrar_loading).toStrictEqual(false)
        expect(wrapper.vm.handles).toStrictEqual(['REG-DUMBLEDORE'])
        expect(wrapper.vm.$notify).not.toHaveBeenCalled()
        expect(console.log).not.toHaveBeenCalled()
    })

    test('dialog_opened watcher works correctly', async() => {
        await wrapper.setData({ handle_editable: true })
        await wrapper.setProps({ dialog_opened: false })

        expect(wrapper.vm.handle_editable).toStrictEqual(false)
    })
})
