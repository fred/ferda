import { REGISTRAR_CERTIFICATE } from 'ferda-test/TestObjects'
import { initWrapper } from 'ferda-test/TestUtils'
import RegistrarCertificateAddForm from 'ferda/registrar/RegistrarCertificateAddForm.vue'

describe('RegistrarCertificateAddForm', () => {
    let wrapper

    beforeEach(() => {
        wrapper = initWrapper(RegistrarCertificateAddForm, {
            propsData: {
                certificates: REGISTRAR_CERTIFICATE,
                is_opened: true,
                is_loading: false,
            },
        })
    })

    test('matches snapshot', () => {
        expect(wrapper.element).toMatchSnapshot()
    })

    test('certificates watcher works correctly', async() => {
        wrapper.vm.$refs.form.resetValidation = jest.fn()
        await wrapper.setProps({ certificates: [] })

        expect(wrapper.vm.use_certificate_file).toStrictEqual(true)
        expect(wrapper.vm.cert_data_pem).toStrictEqual(null)
        expect(wrapper.vm.fingerprint).toBe('')
        expect(wrapper.vm.password).toBe('')
        expect(wrapper.vm.$refs.form.resetValidation).toHaveBeenCalled()
    })

    test('method add_certificate works correctly - invalid form', async() => {
        wrapper.vm.$refs.form.validate = jest.fn(() => false)
        await wrapper.vm.add_certificate()

        expect(wrapper.emitted('add-certificate')).toBeFalsy()
    })

    test('method add_certificate works correctly - valid form with file', async() => {
        wrapper.vm.$refs.form.validate = jest.fn(() => true)
        await wrapper.setData({
            password: 'password', cert_data_pem: new File(['test'], 'test.txt', { type: 'text/plain' }),
        })
        wrapper.vm.cert_data_pem.text = jest.fn(() => Promise.resolve('content'))
        await wrapper.vm.add_certificate()

        expect(wrapper.emitted('add-certificate')[0][0]).toStrictEqual(
            { password: 'password', cert_data_pem: 'content'  },
        )
    })

    test('method add_certificate works correctly - valid form with fingerprint', async() => {
        wrapper.vm.$refs.form.validate = jest.fn(() => true)
        await wrapper.setData({ use_certificate_file: false, password: 'password', fingerprint: 'aa:aa' })
        await wrapper.vm.add_certificate()

        expect(wrapper.emitted('add-certificate')[0][0]).toStrictEqual({ password: 'password', fingerprint: 'aa:aa'  })
    })
})
