import { initWrapper } from 'ferda-test/TestUtils'
import RegistrarCertificateDeleteForm from 'ferda/registrar/RegistrarCertificateDeleteForm.vue'

describe('RegistrarCertificateDeleteForm', () => {
    let wrapper

    beforeEach(() => {
        wrapper = initWrapper(RegistrarCertificateDeleteForm, {
            propsData: {
                is_opened: true,
                is_loading: false,
            },
        })
    })

    test('matches snapshot', () => {
        expect(wrapper.element).toMatchSnapshot()
    })
})
