import { initWrapper } from 'ferda-test/TestUtils'
import RegistrarBaseView from 'ferda/registrar/RegistrarBaseView.vue'

describe('RegistrarBaseView', () => {
    let wrapper

    beforeEach(() => {
        wrapper = initWrapper(RegistrarBaseView, { stubs: ['RouterView'] })
    })

    test('matches snapshot', () => {
        expect(wrapper.element).toMatchSnapshot()
    })
})
