import axios from 'axios'
import MockAdapter from 'axios-mock-adapter'
import flushPromises from 'flush-promises'

import { REGISTRAR_CERTIFICATE, REGISTRAR_EMPIRE } from 'ferda-test/TestObjects'
import { getClonedStoreOptions, initWrapper } from 'ferda-test/TestUtils'
import { URLS } from 'ferda/Urls'
import RegistrarCertificate from 'ferda/registrar/RegistrarCertificate.vue'

describe('RegistrarCertificate', () => {
    let wrapper
    let store
    let axios_mock
    const initial_log = { ...console.log }

    beforeEach(async() => {
        console.log = jest.fn()
        axios_mock = new MockAdapter(axios)
        axios_mock.onGet(URLS.registrar_certificate_get_create_ajax(REGISTRAR_EMPIRE.registrar_handle))
            .reply(200, REGISTRAR_CERTIFICATE)

        store = getClonedStoreOptions()
        store.modules.general.getters.permissions = () => [
            'ferda.can_view_registrar_credit', 'ferda.can_edit_registrar',
        ]

        wrapper = initWrapper(RegistrarCertificate, {
            propsData: { registrar_handle: REGISTRAR_EMPIRE.registrar_handle },
            customStoreOptions: store,
            mocks: { $notify: jest.fn(), $cookies: { get: jest.fn(() => 'csrftoken') }},
        })
        await flushPromises()
    })

    afterEach(() => {
        console.log = initial_log
    })

    test('matches snapshot', () => {
        expect(wrapper.element).toMatchSnapshot()
    })

    test('certificate_form_title has correct values', async() => {
        await wrapper.setData({ last_action_type: wrapper.vm.CERTIFICATE_ACTION.ADD })
        expect(wrapper.vm.certificate_form_title).toBe('registrar:add_new_certificate')

        await wrapper.setData({ last_action_type: wrapper.vm.CERTIFICATE_ACTION.COPY })
        expect(wrapper.vm.certificate_form_title).toBe('registrar:copy_certificate')

        await wrapper.setData({ last_action_type: wrapper.vm.CERTIFICATE_ACTION.DELETE })
        expect(wrapper.vm.certificate_form_title).toBe('registrar:delete_certificate')

        await wrapper.setData({ last_action_type: wrapper.vm.CERTIFICATE_ACTION.EDIT })
        expect(wrapper.vm.certificate_form_title).toBe('registrar:change_epp_access_password')

        await wrapper.setData({ last_action_type: 'something else' })
        expect(wrapper.vm.certificate_form_title).toBe('')
    })

    test('certificates watcher works correctly', async() => {
        await wrapper.setData({ form_opened: true, certificates: [] })

        expect(wrapper.vm.form_opened).toStrictEqual(false)
    })

    test('method show_certificate_form works correctly - with certificate_id', () => {
        wrapper.vm.show_certificate_form(wrapper.vm.CERTIFICATE_ACTION.EDIT, 'some_id')

        expect(wrapper.vm.certificate_id).toBe('some_id')
        expect(wrapper.vm.last_action_type).toBe(wrapper.vm.CERTIFICATE_ACTION.EDIT)
        expect(wrapper.vm.form_opened).toStrictEqual(true)
    })

    test('method show_certificate_form works correctly - without certificate_id', () => {
        wrapper.vm.show_certificate_form(wrapper.vm.CERTIFICATE_ACTION.EDIT)

        expect(wrapper.vm.certificate_id).toStrictEqual(null)
        expect(wrapper.vm.last_action_type).toBe(wrapper.vm.CERTIFICATE_ACTION.EDIT)
        expect(wrapper.vm.form_opened).toStrictEqual(true)
    })

    test('method add_certificate works correctly', async() => {
        axios_mock.onPost(URLS.registrar_certificate_get_create_ajax(REGISTRAR_EMPIRE.registrar_handle)).reply(200)
        wrapper.vm.refresh_certificates = jest.fn()
        await wrapper.vm.add_certificate({ fingerprint: 'aa:aa', password: 'password' })

        expect(wrapper.vm.$notify).not.toHaveBeenCalled()
        expect(console.log).not.toHaveBeenCalled()
        expect(wrapper.vm.refresh_certificates).toHaveBeenCalled()
        expect(wrapper.vm.certificate_errors).toStrictEqual(null)
    })

    test('method add_certificate fails by server error - backend error', async() => {
        axios_mock.onPost(URLS.registrar_certificate_get_create_ajax(REGISTRAR_EMPIRE.registrar_handle)).reply(500)
        wrapper.vm.refresh_certificates = jest.fn()
        await wrapper.vm.add_certificate({ fingerprint: 'aa:aa', password: 'password' })

        expect(wrapper.vm.$notify).toHaveBeenCalledWith(wrapper.vm.ERROR_SOMETHING_WENT_WRONG)
        expect(console.log).toHaveBeenCalledWith('error: notifications:something_went_wrong')
        expect(wrapper.vm.refresh_certificates).not.toHaveBeenCalled()
        expect(wrapper.vm.certificate_errors).toStrictEqual(null)
    })

    test('method add_certificate fails by server error - invalid data', async() => {
        axios_mock.onPost(URLS.registrar_certificate_get_create_ajax(REGISTRAR_EMPIRE.registrar_handle)).reply(400, {
            detail: 'Invalid data',
            fields: ['certificate.fingerprint'],
        })
        wrapper.vm.refresh_certificates = jest.fn()
        await wrapper.vm.add_certificate({ fingerprint: 'xxx', password: 'password' })

        expect(wrapper.vm.$notify).not.toHaveBeenCalled()
        expect(console.log).not.toHaveBeenCalled()
        expect(wrapper.vm.refresh_certificates).not.toHaveBeenCalled()
        expect(wrapper.vm.certificate_errors).toStrictEqual(
            { detail: 'Invalid data', fields: ['certificate.fingerprint'] },
        )
    })

    test('method edit_certificate works correctly', async() => {
        axios_mock.onPut(URLS.registrar_certificate_update_ajax(REGISTRAR_EMPIRE.registrar_handle, 'some_id'))
            .reply(200)
        wrapper.vm.refresh_certificates = jest.fn()
        await wrapper.setProps({ registrar_handle: REGISTRAR_EMPIRE.registrar_handle })
        await wrapper.setData({ certificate_id: 'some_id' })
        await wrapper.vm.edit_certificate({ password: 'password' })

        expect(wrapper.vm.$notify).not.toHaveBeenCalled()
        expect(console.log).not.toHaveBeenCalled()
        expect(wrapper.vm.refresh_certificates).toHaveBeenCalled()
    })

    test('method edit_certificate fails by server error', async() => {
        axios_mock.onPut(URLS.registrar_certificate_update_ajax(REGISTRAR_EMPIRE.registrar_handle, 'some_id'))
            .reply(500)
        wrapper.vm.refresh_certificates = jest.fn()
        await wrapper.setProps({ registrar_handle: REGISTRAR_EMPIRE.registrar_handle })
        await wrapper.setData({ certificate_id: 'some_id' })
        await wrapper.vm.edit_certificate({ password: 'password' })

        expect(wrapper.vm.$notify).toHaveBeenCalledWith(wrapper.vm.ERROR_SOMETHING_WENT_WRONG)
        expect(console.log).toHaveBeenCalledWith('error: notifications:something_went_wrong')
        expect(wrapper.vm.refresh_certificates).not.toHaveBeenCalled()
    })

    test('method delete_certificate works correctly', async() => {
        axios_mock.onDelete(URLS.registrar_certificate_update_ajax(REGISTRAR_EMPIRE.registrar_handle, 'some_id'))
            .reply(200)
        wrapper.vm.refresh_certificates = jest.fn()
        await wrapper.setData({ certificate_id: 'some_id' })
        await wrapper.vm.delete_certificate()

        expect(wrapper.vm.$notify).not.toHaveBeenCalled()
        expect(console.log).not.toHaveBeenCalled()
        expect(wrapper.vm.refresh_certificates).toHaveBeenCalled()
    })

    test('method delete_certificate fails by server error', async() => {
        axios_mock.onDelete(URLS.registrar_certificate_update_ajax(REGISTRAR_EMPIRE.registrar_handle, 'some_id'))
            .reply(500)
        wrapper.vm.refresh_certificates = jest.fn()
        await wrapper.setData({ certificate_id: 'some_id' })
        await wrapper.vm.delete_certificate()

        expect(wrapper.vm.$notify).toHaveBeenCalledWith(wrapper.vm.ERROR_SOMETHING_WENT_WRONG)
        expect(console.log).toHaveBeenCalledWith('error: notifications:something_went_wrong')
        expect(wrapper.vm.refresh_certificates).not.toHaveBeenCalled()
    })

    test('method refresh_certificates works correctly', async() => {
        await wrapper.vm.refresh_certificates()

        expect(wrapper.vm.certificates).toStrictEqual(REGISTRAR_CERTIFICATE)
        expect(wrapper.vm.$notify).toHaveBeenCalledWith(wrapper.vm.SUCCESS_CHANGES_SAVED)
        expect(console.log).toHaveBeenCalledWith('success: notifications:changes_saved')
    })

    test('method refresh_certificates fails by server error', async() => {
        const original_data = wrapper.vm.certificates
        axios_mock.onGet(URLS.registrar_certificate_get_create_ajax(REGISTRAR_EMPIRE.registrar_handle)).reply(500)
        await wrapper.vm.refresh_certificates()

        expect(wrapper.vm.certificates).toStrictEqual(original_data)
        expect(wrapper.vm.$notify).toHaveBeenCalledWith(wrapper.vm.ERROR_SOMETHING_WENT_WRONG)
        expect(console.log).toHaveBeenCalledWith('error: notifications:something_went_wrong')
    })
})
