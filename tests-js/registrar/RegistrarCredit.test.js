import { initWrapper } from 'ferda-test/TestUtils'
import RegistrarCredit from 'ferda/registrar/RegistrarCredit.vue'

describe('RegistrarCredit', () => {
    let wrapper

    beforeEach(() => {
        wrapper = initWrapper(RegistrarCredit, {
            propsData: {
                credit: {
                    cz: '42000.00',
                    '0.2.4.e164.arpa': '0.00',
                },
                zone_access: {
                    cz: true,
                    '0.2.4.e164.arpa': false,
                },
            },
            stubs: {
                FontAwesomeIcon: true,
            },
        })
    })

    test('matches snapshot with registrar credit', () => {
        expect(wrapper.element).toMatchSnapshot()
    })

    test('matches snapshot without registrar credit', () => {
        wrapper = initWrapper(RegistrarCredit, {
            propsData: {
                credit: {
                    cz: null,
                    '0.2.4.e164.arpa': null,
                },
                zone_access: {
                    cz: true,
                    '0.2.4.e164.arpa': false,
                },
            },
            stubs: {
                FontAwesomeIcon: true,
            },
        })

        expect(wrapper.element).toMatchSnapshot()
    })

    test('zone_credit property returns correct value', () => {
        expect(wrapper.vm.zone_credit).toStrictEqual([
            { credit: '0.00', zone: '0.2.4.e164.arpa' }, { credit: '42000.00', zone: 'cz' },
        ])
    })
})
