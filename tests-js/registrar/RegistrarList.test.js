import axios from 'axios'
import MockAdapter from 'axios-mock-adapter'
import flushPromises from 'flush-promises'

import { REGISTRAR, REGISTRAR_LIST } from 'ferda-test/TestObjects'
import { getClonedStoreOptions, initWrapper } from 'ferda-test/TestUtils'
import { URLS } from 'ferda/Urls'
import RegistrarList from 'ferda/registrar/RegistrarList.vue'

describe('RegistrarList', () => {
    let wrapper
    let axios_mock
    const StoreOptions = getClonedStoreOptions()
    const initial_console = console.log
    const default_history = window.history
    const default_location = window.location

    beforeEach(async() => {
        axios_mock = new MockAdapter(axios)
        axios_mock.onPost(URLS.registrar_create()).reply(200, REGISTRAR)
        axios_mock.onGet(URLS.registrar_list_ajax()).reply(200, REGISTRAR_LIST)
        StoreOptions.modules.general.getters.permissions = () => [
            'ferda.can_view_registrar_credit', 'ferda.can_edit_registrar',
        ]
        wrapper = initWrapper(RegistrarList, {
            customStoreOptions: StoreOptions,
            mocks: { $notify: jest.fn(), $cookies: { get: () => jest.fn(() => 'csrftoken')}},
        })
        await flushPromises()
    })

    afterEach(() => {
        console.log = initial_console
        delete window.history
        delete window.location
        window.history = default_history
        window.location = default_location

        jest.clearAllMocks()
    })

    test('matches snapshot', () => {
        expect(wrapper.vm.registrars_per_zone).toContainEqual(
            { handle:'REG-HARRY', organization:'Ministry of magic', zone:'wizards', credit: 'unlimited' },
        )
        expect(wrapper.vm.registrars_per_zone).toContainEqual(
            {handle:'REG-HARRY', organization:'Ministry of magic', zone:'testers', credit: 42000},
        )

        expect(wrapper.element).toMatchSnapshot()
    })

    test('matches snapshot without credit view permission', async() => {
        const store = getClonedStoreOptions()
        store.modules.general.getters.permissions = () => []
        wrapper = initWrapper(RegistrarList, { customStoreOptions: store })
        await flushPromises()

        expect(wrapper.element).toMatchSnapshot()
    })

    test('matches empty snapshot', async() => {
        axios_mock.onGet(URLS.registrar_list_ajax()).reply(200, [])
        wrapper = initWrapper(RegistrarList)
        await flushPromises()

        expect(wrapper.element).toMatchSnapshot()
    })

    test('filter_by_zone returns correct results', async() => {
        await wrapper.setData({ displayed_zone: null })
        expect(wrapper.vm.filter_by_zone('registrar:all_registrars')).toBe(true)

        await wrapper.setData({ displayed_zone: 'registrar:inactive_registrars' })
        expect(wrapper.vm.filter_by_zone('')).toBe(true)

        await wrapper.setData({ displayed_zone: 'cz' })
        expect(wrapper.vm.filter_by_zone('cz')).toBe(true)
    })

    test('Registrar list is prefilled with non-existent zone', async() => {
        history.pushState(null, null, '?zone=bad_zone')
        wrapper = initWrapper(RegistrarList)
        await flushPromises()

        expect(wrapper.vm.displayed_zone).toBe('bad_zone')
    })

    test('Registrar list is prefilled with all registrars', async() => {
        history.pushState(null, null, '?zone=All+registrars&query=query')
        wrapper = initWrapper(RegistrarList)
        await flushPromises()

        expect(wrapper.vm.displayed_zone).toBe('All registrars')
    })

    test('Registrar list is prefilled with inactive registrars', async() => {
        history.pushState(null, null, '?zone=Inactive+registrars')
        wrapper = initWrapper(RegistrarList, { customStoreOptions: StoreOptions })
        await flushPromises()

        expect(wrapper.vm.displayed_zone).toBe('Inactive registrars')
    })

    test('method add_registrar works correctly', async() => {
        const spy = jest.spyOn(wrapper.vm, 'load_registrars')
        await wrapper.vm.add_registrar(REGISTRAR)

        expect(wrapper.vm.add_registrar_processing).toStrictEqual(false)
        expect(wrapper.vm.dialog_opened).toStrictEqual(false)
        expect(spy).toHaveBeenCalled()
    })

    test('method add_registrar fails by server error', async() => {
        console.log = jest.fn()
        axios_mock.onPost(URLS.registrar_create()).reply(500)
        await wrapper.vm.add_registrar(REGISTRAR)

        expect(wrapper.vm.$notify).toHaveBeenCalledWith(wrapper.vm.ERROR_SOMETHING_WENT_WRONG)
        expect(console.log).toHaveBeenCalledWith('error: notifications:something_went_wrong')
        expect(wrapper.emitted('update-registrars')).toBeFalsy()
    })

    test('method add_registrar fails by server error - 400', async() => {
        const api_error = { detail: 'Invalid data', fields: ['name'] }
        console.log = jest.fn()
        axios_mock.onPost(URLS.registrar_create()).reply(400, api_error)
        await wrapper.vm.add_registrar(REGISTRAR)

        expect(wrapper.vm.registrar_errors).toStrictEqual(api_error)
    })

    test('method set_url_params works correctly', async() => {
        await wrapper.setData({ filter_query: 'test_query' })
        const spy = jest.spyOn(history, 'replaceState')
        wrapper.vm.set_url_params()

        const call_params = new URLSearchParams()
        call_params.append('zone', encodeURIComponent('Inactive registrars'))
        call_params.append('query', 'test_query')

        expect(spy).toHaveBeenCalledWith(null, null, `?${call_params.toString()}`)
    })

    test('method load_registrars fails by server error', async() => {
        await wrapper.setData({ registrars: [] })
        jest.spyOn(wrapper.vm, 'get_registrar_list_data').mockReturnValue(null)
        await wrapper.vm.load_registrars()

        expect(wrapper.vm.registrars).toStrictEqual([])
    })

    test('method load_registrars works correctly', async() => {
        await wrapper.setData({ registrars: [] })
        await wrapper.vm.load_registrars()

        expect(wrapper.vm.registrars).toStrictEqual(REGISTRAR_LIST)
    })
})
