import { REGISTRAR_CERTIFICATE } from 'ferda-test/TestObjects'
import { initWrapper } from 'ferda-test/TestUtils'
import RegistrarCertificateEditForm from 'ferda/registrar/RegistrarCertificateEditForm.vue'

describe('RegistrarCertificateEditForm', () => {
    let wrapper

    beforeEach(() => {
        wrapper = initWrapper(RegistrarCertificateEditForm, {
            propsData: {
                certificates: REGISTRAR_CERTIFICATE,
                is_opened: true,
                is_loading: false,
            },
        })
    })

    test('matches snapshot', () => {
        expect(wrapper.element).toMatchSnapshot()
    })

    test('certificates watcher works correctly', async() => {
        wrapper.vm.$refs.form.resetValidation = jest.fn()
        await wrapper.setProps({ certificates: [] })

        expect(wrapper.vm.password).toBe('')
        expect(wrapper.vm.$refs.form.resetValidation).toHaveBeenCalled()
    })

    test('method edit_certificate works correctly - invalid form', () => {
        wrapper.vm.$refs.form.validate = jest.fn(() => false)
        wrapper.vm.edit_certificate()

        expect(wrapper.emitted('edit-certificate')).toBeFalsy()
    })

    test('method edit_certificate works correctly - valid form', async() => {
        wrapper.vm.$refs.form.validate = jest.fn(() => true)
        await wrapper.setData({ password: 'password' })
        await wrapper.vm.edit_certificate()

        expect(wrapper.emitted('edit-certificate')[0][0]).toStrictEqual({ password: 'password' })
    })
})
