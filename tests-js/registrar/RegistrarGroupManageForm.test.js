import { initWrapper } from 'ferda-test/TestUtils'
import RegistrarGroupManageForm from 'ferda/registrar/RegistrarGroupManageForm.vue'

describe('RegistrarGroupManageForm', () => {
    let wrapper

    beforeEach(() => {
        wrapper = initWrapper(RegistrarGroupManageForm, {
            propsData: {
                available_groups: ['group1', 'group2', 'group3', 'group4'],
                registrar_groups: ['group1', 'group2'],
                manage_groups_loading: false,
                available_groups_loading: false,
            },
        })
    })

    test('matches snapshot', () => {
        expect(wrapper.element).toMatchSnapshot()
    })

    test('property submit_disabled returns correct values', async() => {
        await wrapper.setData({ selected_groups: ['group1'] })
        await wrapper.setProps({ registrar_groups: ['group1'] })

        expect(wrapper.vm.submit_disabled).toStrictEqual(true)

        await wrapper.setData({ selected_groups: ['group1'] })
        await wrapper.setProps({ registrar_groups: ['group2'] })

        expect(wrapper.vm.submit_disabled).toStrictEqual(false)
    })

    test('property to_be_added returns correct values', async() => {
        await wrapper.setData({ selected_groups: ['group1', 'group2', 'group3'] })
        await wrapper.setProps({ registrar_groups: ['group2'] })

        expect(wrapper.vm.to_be_added).toStrictEqual(['group1', 'group3'])
    })

    test('property to_be_deleted returns correct values', async() => {
        await wrapper.setData({ selected_groups: ['group2'] })
        await wrapper.setProps({ registrar_groups: ['group1', 'group2', 'group3'] })

        expect(wrapper.vm.to_be_deleted).toStrictEqual(['group1', 'group3'])
    })
})
