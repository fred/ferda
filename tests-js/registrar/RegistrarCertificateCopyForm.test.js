import { REGISTRAR_CERTIFICATE } from 'ferda-test/TestObjects'
import { initWrapper } from 'ferda-test/TestUtils'
import RegistrarCertificateCopyForm from 'ferda/registrar/RegistrarCertificateCopyForm.vue'

describe('RegistrarCertificateCopyForm', () => {
    let wrapper

    beforeEach(() => {
        wrapper = initWrapper(RegistrarCertificateCopyForm, {
            propsData: {
                certificates: REGISTRAR_CERTIFICATE,
                certificate_id: REGISTRAR_CERTIFICATE[0].credentials_id,
                is_opened: true,
                is_loading: false,
            },
        })
    })

    test('matches snapshot', () => {
        expect(wrapper.element).toMatchSnapshot()
    })

    test('certificates watcher works correctly', async() => {
        wrapper.vm.$refs.form.resetValidation = jest.fn()
        await wrapper.setProps({ certificates: [] })

        expect(wrapper.vm.use_certificate_file).toStrictEqual(true)
        expect(wrapper.vm.cert_data_pem).toStrictEqual(null)
        expect(wrapper.vm.fingerprint).toBe('')
        expect(wrapper.vm.$refs.form.resetValidation).toHaveBeenCalled()
    })

    test('method copy_certificate works correctly - invalid form', async() => {
        wrapper.vm.$refs.form.validate = jest.fn(() => false)
        await wrapper.vm.copy_certificate()

        expect(wrapper.emitted('add-certificate')).toBeFalsy()
    })

    test('method copy_certificate works correctly - valid form with file', async() => {
        wrapper.vm.$refs.form.validate = jest.fn(() => true)
        await wrapper.setData({ cert_data_pem: new File(['test'], 'test.txt', { type: 'text/plain' }) })
        wrapper.vm.cert_data_pem.text = jest.fn(() => Promise.resolve('content'))
        await wrapper.vm.copy_certificate()

        expect(wrapper.emitted('add-certificate')[0][0]).toStrictEqual(
            { template_credentials_id: wrapper.vm.certificate_id, cert_data_pem: 'content'  },
        )
    })

    test('method copy_certificate works correctly - valid form with fingerprint', async() => {
        wrapper.vm.$refs.form.validate = jest.fn(() => true)
        await wrapper.setData({ use_certificate_file: false, fingerprint: 'aa:aa' })
        await wrapper.vm.copy_certificate()

        expect(wrapper.emitted('add-certificate')[0][0]).toStrictEqual(
            { template_credentials_id: wrapper.vm.certificate_id, fingerprint: 'aa:aa'  },
        )
    })
})
