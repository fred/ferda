import { initWrapper } from 'ferda-test/TestUtils'
import RegistrarZoneDeleteForm from 'ferda/registrar/RegistrarZoneDeleteForm.vue'

describe('RegistrarZoneDeleteForm', () => {
    let wrapper
    const initial_console = console.log

    beforeEach(() => {
        wrapper = initWrapper(RegistrarZoneDeleteForm, {
            propsData: {
                is_loading: false,
                zone_access: {
                    zone: 'test',
                    valid_from: '2020-08-21T10:00:00Z',
                    valid_to: '2023-08-21T10:00:00Z',
                },
            },
        })
    })

    afterEach(() => {
        console.log = initial_console
    })

    test('matches snapshot', () => {
        expect(wrapper.element).toMatchSnapshot()
    })

    test('matches empty snapshot', () => {
        wrapper = initWrapper(RegistrarZoneDeleteForm, {
            propsData: {
                zone_access: { zone: 'test' },
            },
        })
        expect(wrapper.element).toMatchSnapshot()
    })

    test('method submit_dialog works correctly', async() => {
        await wrapper.vm.submit_dialog()
        expect(wrapper.emitted('submit-dialog')[0][0]).toStrictEqual({
            zone: 'test',
            valid_from: '2020-08-21T10:00:00Z',
            valid_to: '2023-08-21T10:00:00Z',
        })
    })
})
