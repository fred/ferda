import { initWrapper } from 'ferda-test/TestUtils'
import RegistrarZoneAddForm from 'ferda/registrar/RegistrarZoneAddForm.vue'

describe('RegistrarZoneAddForm', () => {
    let wrapper
    const initial_console = console.log

    beforeEach(() => {
        jest.useFakeTimers()
        jest.setSystemTime(new Date('2022-01-01T12:01:00+00:00'))
        wrapper = initWrapper(RegistrarZoneAddForm, {
            propsData: {
                is_loading: false,
                zones: { test: 0, test2: 0 },
                zone_access_errors: { fields: ['test'], detail: 'error'},
            },
            data() {
                return {
                    zone: 'test',
                    valid_from: '2022-08-21T10:00:00Z',
                    valid_to: '2023-08-21T10:00:00Z',
                }
            },
        })
        wrapper.vm.$refs.form.validate = jest.fn()
        wrapper.vm.$refs.form.reset = jest.fn()
    })

    afterEach(() => {
        console.log = initial_console
        jest.runOnlyPendingTimers()
        jest.useRealTimers()
    })

    test('matches snapshot', () => {
        expect(wrapper.element).toMatchSnapshot()
    })

    test('matches empty snapshot', () => {
        wrapper = initWrapper(RegistrarZoneAddForm, {
            propsData: {
                zones: {},
            },
        })
        expect(wrapper.element).toMatchSnapshot()
    })

    test('method submit_dialog works correctly - is valid', async() => {
        wrapper.vm.$refs = {
            form: { validate: jest.fn(() => true) },
        }
        await wrapper.vm.submit_dialog()

        expect(wrapper.emitted('submit-dialog')[0][0]).toStrictEqual({
            zone: 'test',
            valid_from: '2022-01-01T00:00:00Z',
            valid_to: '2023-08-22T10:00:00Z',
        })
    })

    test('method submit_dialog works correctly - is valid and valid_to is null', async() => {
        wrapper.vm.$refs = {
            form: { validate: jest.fn(() => true) },
        }
        wrapper.setData({ valid_to: null })
        await wrapper.vm.submit_dialog()

        expect(wrapper.emitted('submit-dialog')[0][0]).toStrictEqual({
            zone: 'test',
            valid_from: '2022-01-01T00:00:00Z',
            valid_to: null,
        })
    })

    test('method submit_dialog works correctly - is not valid', async() => {
        await wrapper.setData({ zone: null})
        wrapper.vm.$refs = {
            form: { validate: jest.fn(() => false) },
            emails: { blur: jest.fn() },
        }
        await wrapper.vm.submit_dialog()
        expect(wrapper.emitted('submit-dialog')).toBeFalsy()
    })

    test('prop zone_access_errors is empty - reset form', async() => {
        wrapper.vm.$refs.form.reset = jest.fn()
        await wrapper.setProps({ zone_access_errors: null })
        expect(wrapper.vm.$refs.form.reset).toHaveBeenCalled()
    })

    test('Sets todays date - after open dialog', async() => {
        await wrapper.setProps({ dialog_opened: true })
        expect(wrapper.vm.valid_from).toStrictEqual('2022-01-01T00:00:00+00:00')
    })
})
