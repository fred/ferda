import cloneDeep from 'lodash.clonedeep'

import { Store } from 'ferda/../store'

import { initWrapper } from './TestUtils'

describe('Store', () => {
    const createNewStore = () => {
        const Component = {
            render() {},
        }
        const wrapper = initWrapper(Component, {
            customStoreOptions: cloneDeep(Store),
        })
        return wrapper.vm.$store
    }

    const checkStoreModuleContent = (store, module_name, expected_state) => {
        for (const item_name in expected_state) {
            expect(store.state[module_name][item_name]).toStrictEqual(expected_state[item_name])
            expect(store.getters[`${module_name}/${item_name}`]).toStrictEqual(expected_state[item_name])
        }
    }

    const checkPreferencesStoreModuleContent = (store, expected_state) => {
        for (const item_name in expected_state) {
            expect(store.state.preferences[item_name]).toStrictEqual(expected_state[item_name].state)
            expect(store.getters[`preferences/${item_name}`]).toStrictEqual(expected_state[item_name].getter)
        }
    }

    const testSetItem = (module_name, item_name, new_value, expected_state) => {
        const store = createNewStore()
        // set value
        store.commit(`${module_name}/set_${item_name.toLowerCase()}`, new_value)

        // testing store content
        const cloned_expected_state = cloneDeep(expected_state)
        cloned_expected_state[item_name] = new_value
        checkStoreModuleContent(store, module_name, cloned_expected_state)
    }

    const testSetPreferenceItem = (user_id, item_name, storage_item_name, value, expected_state) => {
        const store = createNewStore()
        // Currently (Jan '19) localStorage can not be mocked or spied on by jest as you usually would
        // https://github.com/facebook/jest/issues/6798
        jest.spyOn(window.localStorage.__proto__, 'setItem')
        window.localStorage.__proto__.setItem = jest.fn()

        // set values
        store.commit('general/set_user', {username: user_id})
        store.dispatch(`preferences/set_${item_name.toLowerCase()}`, value)

        // testing store content
        const cloned_expected_state = cloneDeep(expected_state)
        cloned_expected_state[item_name].state = value.toString()
        cloned_expected_state[item_name].getter = value
        checkPreferencesStoreModuleContent(store, cloned_expected_state)

        // testing local storage
        expect(localStorage.setItem).toHaveBeenCalled()
        expect(localStorage.setItem).toHaveBeenCalledWith(
            `preferences.${user_id}.${storage_item_name}`, value.toString(),
        )
    }

    test('search module', () => {
        const expected_store_state = {
            last_query_values: [],
        }
        const module_name = 'search'

        // testing default values
        checkStoreModuleContent(module_name, expected_store_state)

        testSetItem(module_name, 'last_query_values', ['Harry', 'Potter'], expected_store_state)
    })

    test('history module', () => {
        const expected_store_state = {
            version_list: [],
            old_history_id: null,
            new_history_id: null,
        }
        const module_name = 'history'

        // testing default values
        checkStoreModuleContent(module_name, expected_store_state)

        testSetItem(module_name, 'version_list', ['test_version'], expected_store_state)
        testSetItem(module_name, 'old_history_id', 'thousands_years_ago', expected_store_state)
        testSetItem(module_name, 'new_history_id', 'thousands_years_after', expected_store_state)
    })

    test('settings module', () => {
        const expected_store_state = {
            TIME_ZONE: 'UTC',
            LANGUAGE_CODE: 'en',
            LANGUAGES: ['en', 'cs'],
            FIDO2_AUTHENTICATION: false,
            PAGINATION_PAGE_SIZE: 50,
            OBJECT_DETAIL_LOGGER_SERVICES: [],
        }
        const module_name = 'settings'

        // testing default values
        checkStoreModuleContent(module_name, expected_store_state)

        testSetItem(module_name, 'TIME_ZONE', 'UTC-1', expected_store_state)
        testSetItem(module_name, 'LANGUAGE_CODE', 'pa', expected_store_state)
        testSetItem(module_name, 'LANGUAGES', [['pa', 'Parseltongue']], expected_store_state)
        testSetItem(module_name, 'FIDO2_AUTHENTICATION', false, expected_store_state)
        testSetItem(module_name, 'PAGINATION_PAGE_SIZE', 50, expected_store_state)
        testSetItem(module_name, 'OBJECT_DETAIL_LOGGER_SERVICES', [], expected_store_state)
    })

    test('general module', () => {
        const expected_store_state = {
            user: {},
            permissions: [],
        }
        const module_name = 'general'

        // testing default values
        checkStoreModuleContent(module_name, expected_store_state)

        testSetItem(module_name, 'user', {name: 'Harry Potter', address: 'Number 4, Privet Drive, Little Whinging'},
            expected_store_state)
        testSetItem(module_name, 'permissions', ['spelling'], expected_store_state)

    })

    test('preferences module', () => {
        const expected_store_state = {
            verbose: { state:  null, getter: false },
            show_search_form: { state: null, getter: true },
            show_drawer: { state: null, getter: true },
            selected_language: { state: null, getter: 'en' },
        }

        // testing default values
        checkPreferencesStoreModuleContent(expected_store_state)

        testSetPreferenceItem('hpotter', 'verbose', 'verbose_view', true, expected_store_state)
        testSetPreferenceItem('hpotter', 'verbose', 'verbose_view', false, expected_store_state)

        testSetPreferenceItem('hpotter', 'show_search_form', 'show_search_form', true, expected_store_state)
        testSetPreferenceItem('hpotter', 'show_search_form', 'show_search_form', false, expected_store_state)

        testSetPreferenceItem('hpotter', 'show_drawer', 'show_drawer', true, expected_store_state)
        testSetPreferenceItem('hpotter', 'show_drawer', 'show_drawer', false, expected_store_state)

        testSetPreferenceItem('hpotter', 'selected_language', 'selected_language', 'pa', expected_store_state)
    })
})
