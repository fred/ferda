import axios from 'axios'
import MockAdapter from 'axios-mock-adapter'
import flushPromises from 'flush-promises'

import { OBJECT_TYPE, RegistryMixin } from 'ferda/RegistryMixin'
import { URLS } from 'ferda/Urls'

import { CONTACT_OBIWAN, KEYSET, NSSET, REGISTRAR_LIST, STATE_FLAGS } from './TestObjects'
import { initWrapper } from './TestUtils'

describe('RegistryMixin', () => {
    let wrapper
    let axios_mock
    const initial_console = console.log

    beforeEach(async() => {
        axios_mock = new MockAdapter(axios)
        const Component = {
            render() {},
            mixins: [RegistryMixin],
        }
        wrapper = initWrapper(Component)
        axios_mock.onGet(URLS.countries_list()).reply(500)
        axios_mock.onGet(URLS.object_info_ajax(OBJECT_TYPE.CONTACT, CONTACT_OBIWAN.contact_id)).reply(404, null)
        axios_mock.onGet(URLS.object_info_ajax(OBJECT_TYPE.NSSET, 'UUID-NSSET')).reply(200, NSSET)
        axios_mock.onGet(URLS.object_info_ajax(OBJECT_TYPE.KEYSET, 'UUID-KEYSET')).reply(200, KEYSET)
        axios_mock.onGet(URLS.object_state_ajax(OBJECT_TYPE.CONTACT, 'UUID-OBIWAN')).reply(200, STATE_FLAGS)
        await flushPromises()
    })

    afterEach(() => {
        console.log = initial_console
    })

    test('dnskey_alg_label', () => {
        expect(wrapper.vm.get_dnskey_alg_label(5)).toStrictEqual(['keyset:dns_key_alg.alg5', 'keyset:reserved'])
        expect(wrapper.vm.get_dnskey_alg_label(23)).toBe('keyset:unassigned')
        expect(wrapper.vm.get_dnskey_alg_label(230)).toStrictEqual(['keyset:dns_key_alg.alg230', 'keyset:reserved'])
        expect(wrapper.vm.get_dnskey_alg_label(2300)).toBe('2300 keyset:is_out_of_range')
    })

    test('dnskey_protocol_label', () => {
        expect(wrapper.vm.get_dnskey_protocol_label(3)).toBe('keyset:dnssec')
        expect(wrapper.vm.get_dnskey_protocol_label(230)).toBe('keyset:unassigned')
        expect(wrapper.vm.get_dnskey_protocol_label(1)).toBe('keyset:reserved')
        expect(wrapper.vm.get_dnskey_protocol_label(2300)).toBe('2300 keyset:is_out_of_range')
    })

    test('dnskey_flag_label', () => {
        expect(wrapper.vm.get_dnskey_flag_labels(1)).toBe('keyset:dns_key_flag.sep')
        expect(wrapper.vm.get_dnskey_flag_labels(384)).toContain('keyset:dns_key_flag.revoke')
        expect(wrapper.vm.get_dnskey_flag_labels(384)).toContain('keyset:dns_key_flag.zone')
        expect(wrapper.vm.get_dnskey_flag_labels(384)
            .replace('keyset:dns_key_flag.revoke', '')
            .replace('keyset:dns_key_flag.zone', '')
            .replace(',', '').trim()).toBe('')
        expect(wrapper.vm.get_dnskey_flag_labels(2)).toBe('')
        expect(wrapper.vm.get_dnskey_flag_labels(230000)).toBe('230000 keyset:is_out_of_range')
    })

    test('get_object_info success', async() => {
        const url = URLS.object_info_ajax(OBJECT_TYPE.CONTACT, CONTACT_OBIWAN.contact_id)

        axios_mock.onGet(url).reply(200, CONTACT_OBIWAN)
        wrapper.vm.notify_message = jest.fn()

        const object_info = await wrapper.vm.get_object_info(OBJECT_TYPE.CONTACT, CONTACT_OBIWAN.contact_id)

        expect(object_info).toEqual(CONTACT_OBIWAN)
        expect(wrapper.vm.notify_message).toBeCalledTimes(0)
    })

    test('get_object_info error', async() => {
        wrapper.vm.notify_message = jest.fn()
        const object_info = await wrapper.vm.get_object_info(OBJECT_TYPE.CONTACT, CONTACT_OBIWAN.contact_id)
        expect(object_info).toBe(null)
        expect(wrapper.vm.notify_message).toBeCalledTimes(1)
        expect(wrapper.vm.notify_message).toBeCalledWith(wrapper.vm.ERROR_SOMETHING_WENT_WRONG)
    })

    test('fqdn_sort 1', () => {
        const test_data = [
            'intranet.hogwarts.com',
            'slytherin.hogwarts.com',
            'hufflepuff.hogwarts.com',
            'ravenclaw.hogwarts.com',
            'ravenclaw.hogwarts.com',
            'hogwarts.com',
            'intranet.ravenclaw.hogwarts.com',
            'gryffindor.hogwarts.com',
            'teachers.hogwarts.com',
            'hogwarts.online',
        ]

        const sorted = wrapper.vm.fqdn_sort(test_data)

        expect(sorted).toMatchObject([
            'hogwarts.com',
            'gryffindor.hogwarts.com',
            'hufflepuff.hogwarts.com',
            'intranet.hogwarts.com',
            'ravenclaw.hogwarts.com',
            'ravenclaw.hogwarts.com',
            'intranet.ravenclaw.hogwarts.com',
            'slytherin.hogwarts.com',
            'teachers.hogwarts.com',
            'hogwarts.online',
        ])
    })

    test('fqdn_sort 2', () => {
        const test_data = [ 'b.aaaaaa', 'b.a', 'c.a-aa' ]

        const sorted = wrapper.vm.fqdn_sort(test_data)

        expect(sorted).toMatchObject([ 'b.a', 'c.a-aa', 'b.aaaaaa' ])
    })

    test('get_countries fails', async() => {
        wrapper.vm.notify_message = jest.fn()

        const countries = await wrapper.vm.get_countries()

        expect(wrapper.vm.notify_message).toHaveBeenCalledWith(wrapper.vm.ERROR_SOMETHING_WENT_WRONG)
        expect(countries).toEqual(null)
    })

    test('get_search_params returns correct params', () => {
        const match_str = [
            'search_items=search_item_1',
            '&search_items=search_item_2',
            '&query=query1',
            '&query=query2',
            '&history=2021-01-01',
            '&valid_from=2020-01-01',
            '&type=test_type',
        ]

        expect(wrapper.vm.get_search_params(
            ['query1', 'query2'],
            'test_type',
            ['search_item_1', 'search_item_2'],
            '2021-01-01',
            '2020-01-01',
        ).toString()).toBe(match_str.join(''))
    })

    test('method get_search_form_params returns correct values', () => {
        expect(wrapper.vm.get_search_form_params(['address']).toString())
            .toBe([
                'search_items=place.street',
                '&search_items=place.city',
                '&search_items=place.state_or_province',
                '&search_items=place.postal_code',
            ].join(''))
        expect(wrapper.vm.get_search_form_params(['identifier']).toString()).toBe('search_items=additional_identifier')
        expect(wrapper.vm.get_search_form_params(['something']).toString()).toBe('search_items=something')
    })

    test('get_nsset_info returns correct info', async() => {
        const nsset_info = await wrapper.vm.get_nsset_info(NSSET.nsset_id)
        expect(nsset_info).toEqual(NSSET)
    })

    test('get_keyset_info returns correct info', async() => {
        const keyset_info = await wrapper.vm.get_keyset_info(KEYSET.keyset_id)
        expect(keyset_info).toEqual(KEYSET)
    })

    test('method get_state_flags_data works correctly', async() => {
        axios_mock.onGet(URLS.object_state_ajax(OBJECT_TYPE.DOMAIN, 'UUID-OBIWAN')).reply(200, STATE_FLAGS)
        const response = await wrapper.vm.get_state_flags_data(OBJECT_TYPE.DOMAIN, 'UUID-OBIWAN')
        expect(response).toStrictEqual(STATE_FLAGS)
    })

    test('method get_state_flags_data fails by server error', async() => {
        axios_mock.onGet(URLS.object_state_ajax(OBJECT_TYPE.DOMAIN, 'UUID-OBIWAN')).reply(500, null)
        wrapper.vm.$notify = jest.fn()
        console.log = jest.fn()
        await wrapper.vm.get_state_flags_data(OBJECT_TYPE.DOMAIN, 'UUID-OBIWAN')
        expect(wrapper.vm.$notify).toHaveBeenCalledWith(wrapper.vm.ERROR_SOMETHING_WENT_WRONG)
        expect(console.log).toHaveBeenCalledWith('error: notifications:something_went_wrong')
    })

    test('method get_registrar_list_data fails by server error', async() => {
        axios_mock.onGet(URLS.registrar_list_ajax()).reply(500)
        wrapper.vm.$notify = jest.fn()
        console.log = jest.fn()

        const registrar_list = await wrapper.vm.get_registrar_list_data()

        expect(registrar_list).toStrictEqual(null)
        expect(wrapper.vm.$notify).toHaveBeenCalledWith(wrapper.vm.ERROR_SOMETHING_WENT_WRONG)
        expect(console.log).toHaveBeenCalledWith('error: notifications:something_went_wrong')
    })

    test('method get_registrar_list_data is aborted', async() => {
        wrapper.vm.$notify = jest.fn()
        console.log = jest.fn()

        const abort_controller = new AbortController()
        wrapper.vm.get_registrar_list_data(abort_controller.signal)
        abort_controller.abort()
        await flushPromises()

        expect(wrapper.vm.$notify).toHaveBeenCalledWith(wrapper.vm.INFO_REQUEST_ABORTED)
        expect(console.log).toHaveBeenCalledWith('info: notifications:search_aborted')
    })

    test('method get_registrar_list works correctly', async() => {
        axios_mock.onGet(URLS.registrar_list_ajax()).reply(200, REGISTRAR_LIST)
        wrapper.vm.$notify = jest.fn()
        console.log = jest.fn()

        const registrar_list = await wrapper.vm.get_registrar_list_data()

        expect(registrar_list).toStrictEqual(REGISTRAR_LIST)
        expect(wrapper.vm.$notify).not.toHaveBeenCalled()
        expect(console.log).not.toHaveBeenCalled()
    })

    test('method get_registrar_certificate_error_messages works correctly', () => {
        expect(wrapper.vm.get_registrar_certificate_error_messages(
            { fields: ['fingerprint'], detail: 'Invalid data'}, true),
        ).toStrictEqual(['Invalid data'])
        expect(wrapper.vm.get_registrar_certificate_error_messages(
            { fields: ['something'], detail: 'Something'}, false),
        ).toStrictEqual(['Something'])
    })

    test('method set_countries fails by server error', async() => {
        console.log = jest.fn()
        axios_mock.onGet(URLS.countries_list()).reply(500)
        const Component = {
            render() {},
            mixins: [RegistryMixin],
        }
        wrapper = initWrapper(Component)
        wrapper.vm.$notify = jest.fn()
        await wrapper.vm.set_countries()

        expect(wrapper.vm.$notify).toHaveBeenCalledWith(wrapper.vm.ERROR_SOMETHING_WENT_WRONG)
        expect(console.log).toHaveBeenCalledWith('error: notifications:something_went_wrong')
    })

    test('method get_registrar_url returns correct values', () => {
        expect(wrapper.vm.get_registrar_url('test.org')).toBe('https://test.org')
        expect(wrapper.vm.get_registrar_url('http://test.org')).toBe('http://test.org')
        expect(wrapper.vm.get_registrar_url('https://test.org')).toBe('https://test.org')
        expect(wrapper.vm.get_registrar_url(null)).toBe('')
        expect(wrapper.vm.get_registrar_url('')).toBe('')
    })

    test('method get_delete_date returns correct values', () => {
        expect(wrapper.vm.get_delete_date({ contact: { events: { unregistered: null } } }, OBJECT_TYPE.CONTACT))
            .toStrictEqual(null)
        expect(wrapper.vm.get_delete_date(
            { contact: { events: { unregistered: { timestamp: '2021-01-01T12:00:00' } } } }, OBJECT_TYPE.CONTACT),
        ).toStrictEqual('2021-01-01T12:00:00')
    })

    test('method dns_keys_algorithms returns correct values', () => {
        expect(wrapper.vm.dns_keys_algorithms([{ alg: 10 }])).toStrictEqual([
            'keyset:dns_key_alg.alg10',
            'keyset:reserved',
        ])
    })
})
