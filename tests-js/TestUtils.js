import { createLocalVue, mount, shallowMount } from '@vue/test-utils'
import AsyncComputed from 'vue-async-computed'
import Vuetify from 'vuetify'
import Vuex, { Store } from 'vuex'

import { SearchModule } from 'ferda/../store'

export const getClonedStoreOptions = () => {
    return {
        modules: {
            preferences: {
                namespaced: true,
                getters: {
                    verbose: () => true,
                    show_search_form: () => false,
                    show_drawer: () => false,
                    selected_language: () => 'en',
                },
            },
            settings: {
                namespaced: true,
                getters: {
                    LANGUAGE_CODE: () => 'en',
                    TIME_ZONE: () => 'UTC',
                    LANGUAGES: () => ['en', 'cs'],
                    FIDO2_AUTHENTICATION: () => true,
                    MANUAL_IN_ZONE_DEFAULT_DURATION: () => 604800,
                    PAGINATION_PAGE_SIZE: () => 50,
                    OBJECT_DETAIL_LOGGER_SERVICES: () => [],
                    MAX_FILE_UPLOAD_SIZE: () => 10000000,
                },
            },
            general: {
                namespaced: true,
                getters: {
                    user: () => ({
                        username: 'harry',
                        first_name: 'Harry',
                        last_name: 'Potter',
                    }),
                    permissions: () => [
                        'ferda.can_view_registry',
                        'ferda.can_view_registrar_credit',
                    ],
                },
            },
            history: {
                namespaced: true,
                getters: {
                    version_list: () => ['V1', 'V2'],
                    old_history_id: () => 'V1',
                    new_history_id: () => 'V2',
                },
                mutations: {
                    set_version_list: jest.fn(),
                    set_old_history_id: jest.fn(),
                    set_new_history_id: jest.fn(),
                },
            },
            search: {
                namespaced: true,
                state: SearchModule.state,
                getters: SearchModule.getters,
                mutations: {
                    set_query_values: jest.fn((state, value) => {state.query_values = value}),
                    set_last_query_values: jest.fn((state, value) => {state.set_last_query_values = value}),
                    set_search_date: jest.fn((state, value) => {state.search_date = value}),
                    set_object_type: jest.fn((state, value) => {state.object_type = value}),
                    set_search_items: jest.fn((state, value) => {state.search_items = value}),
                },
            },
        },
    }
}

export const createDefaultStore = () => {
    return new Store(getClonedStoreOptions())
}

export const initWrapper = (component, properties, shallow = true) => {
    if (!properties) properties = {}

    if (!properties.localVue) properties.localVue = createLocalVue()

    properties.localVue.use(Vuex)
    properties.localVue.use(AsyncComputed)

    if (!properties.customStoreOptions) {
        properties.store = createDefaultStore()
    } else {
        properties.store = new Store(properties.customStoreOptions)
    }

    if (!properties.vuetify) properties.vuetify = new Vuetify()
    if (!properties.mocks) properties.mocks = {}
    if (!properties.i18n) properties.mocks.$t = key => key

    return shallow ? shallowMount(component, properties) : mount(component, properties)
}
