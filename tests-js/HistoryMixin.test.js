import { HistoryMixin } from 'ferda/HistoryMixin'

import { initWrapper } from './TestUtils'

describe('HistoryMixin', () => {
    let wrapper

    beforeEach(() => {
        const Component = {
            render() {},
            mixins: [HistoryMixin],
        }

        wrapper = initWrapper(Component)
    })

    test('deep_diff', () => {
        expect(wrapper.vm.deep_diff(
            {a: 1, b: 'b', c: true, d: {x: null, y: 1}, e: [0, 1, 2]},
            {a: 1, b: 'B', c: true, d: {x: 0, y: 1}, e: [2, 3, 4]},
        )).toEqual({
            a: {old_value: 1, new_value: 1, differs: false},
            b: {old_value: 'b', new_value: 'B', differs: true},
            c: {old_value: true, new_value: true, differs: false},
            d: {
                x: {old_value: null, new_value: 0, differs: true},
                y: {old_value: 1, new_value: 1, differs: false},
                differs: true,
            },
            e: {
                added: [3, 4],
                deleted: [0, 1],
                stayed: [2],
                old_value: [0, 1, 2],
                new_value: [2, 3, 4],
                differs: true,
            },
            differs: true,
        })
    })

    test('contact_diff', () => {
        expect(wrapper.vm.contact_diff(
            {
                name: 'Harry Potter',
                additional_identifier: {type: 'birthdate', value: '2000-01-01'},
                publish: {name: true, additional_identifier: false},
                events: {
                    updated: {timestamp: '2019-01-01T13:00:00Z', registrar_handle: 'REG-X'},
                    transferred: {timestamp: '2019-01-01T13:00:00Z', registrar_handle: 'REG-X'},
                },
                warning_letter: 'not_specified',
            },
            {
                name: 'Harry Potter',
                additional_identifier: {type: 'birthdate', value: '2000-07-07'},
                publish: {name: false, additional_identifier: false},
                events: {
                    updated: {timestamp: '2019-01-01T13:00:00Z', registrar_handle: 'REG-Y'},
                    transferred: {timestamp: '2020-01-01T13:00:00Z', registrar_handle: 'REG-X'},
                },
                warning_letter: 'not_specified',
            },
        )).toEqual({
            name: {
                old_value: 'Harry Potter',
                new_value: 'Harry Potter',
                differs: false,
            },
            additional_identifier: {
                old_value: '2000-01-01 (contact:identifier_type.birthdate)',
                new_value: '2000-07-07 (contact:identifier_type.birthdate)',
                differs: true,
            },
            publish: {
                name: {
                    old_value: true,
                    new_value: false,
                    differs: true,
                },
                additional_identifier: {
                    old_value: false,
                    new_value: false,
                    differs: false,
                },
                differs: true,
            },
            events: {
                transferred: {
                    new_value: {registrar_handle: 'REG-X', timestamp: '2020-01-01T13:00:00Z'},
                    old_value: {registrar_handle: 'REG-X', timestamp: '2019-01-01T13:00:00Z'},
                    differs: false,
                },
                updated: {
                    new_value: {registrar_handle: 'REG-Y', timestamp: '2019-01-01T13:00:00Z'},
                    old_value: {registrar_handle: 'REG-X', timestamp: '2019-01-01T13:00:00Z'},
                    differs: true,
                },
                differs: true,
            },
            differs: true,
            warning_letter: {
                differs: false,
                new_value: 'contact:warning_letter_preference.not_specified',
                old_value: 'contact:warning_letter_preference.not_specified',
            },
        })
    })

    test('format_additional_identifier', () => {
        expect(wrapper.vm.format_additional_identifier(null)).toBe(null)
        expect(wrapper.vm.format_additional_identifier({type: 'birthdate', value: '2000-01-01'}))
            .toBe('2000-01-01 (contact:identifier_type.birthdate)')
    })

    test('events_differs', () => {
        expect(wrapper.vm.events_differs(null, null)).toBe(false)
        expect(wrapper.vm.events_differs(null, {})).toBe(true)
        expect(wrapper.vm.events_differs({}, null)).toBe(true)
        expect(wrapper.vm.events_differs({registrar_handle: 'REG-CZNIC'}, {registrar_handle: 'REG-CZNIC'})).toBe(false)
        expect(wrapper.vm.events_differs({registrar_handle: 'REG-MOJEID'}, {registrar_handle: 'REG-CZNIC'})).toBe(true)
    })

    test('_managed_object_diff is null', () => {
        expect(wrapper.vm._managed_object_diff(null, null)).toEqual(null)
    })

    test('contact_diff is null', () => {
        expect(wrapper.vm.contact_diff(null, null)).toEqual(null)
    })

    test('get_object_info_cached is null', async() => {
        expect(await wrapper.vm.get_object_info_cached('type', null, 'history_spec')).toEqual(null)
    })
})
