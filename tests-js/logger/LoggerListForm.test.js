import axios from 'axios'
import MockAdapter from 'axios-mock-adapter'
import flushPromises from 'flush-promises'

import { LOG_ENTRY_TYPES, LOG_PARAMS, LOG_SERVICES } from 'ferda-test/TestObjects'
import { initWrapper } from 'ferda-test/TestUtils'
import { LOG_FORM_FIELDS } from 'ferda/LoggerMixin'
import { URLS } from 'ferda/Urls'
import LoggerListForm from 'ferda/logger/LoggerListForm.vue'

describe('LoggerListForm', () => {
    let wrapper
    let axios_mock
    const initial_console = console.log
    const { location } = window

    beforeEach(() => {
        jest.useFakeTimers()
        jest.setSystemTime(new Date('2022-02-17T00:00:00+00:00'))
        axios_mock = new MockAdapter(axios)
        axios_mock.onGet(URLS.log_services()).reply(200, LOG_SERVICES)
        axios_mock.onGet(URLS.log_entry_types(LOG_PARAMS.log_services.selected_value)).reply(200, LOG_ENTRY_TYPES)
        wrapper = initWrapper(LoggerListForm, { mocks: { validate: jest.fn(), $notify: jest.fn() } })
    })

    afterEach(() => {
        jest.runOnlyPendingTimers()
        jest.useRealTimers()
        delete window.location
        window.location = location
        console.log = initial_console
    })

    test('matches snapshot', () => {
        expect(wrapper.element).toMatchSnapshot()
    })

    test('method submit_log_request emitts correctly', () => {
        wrapper.vm.$refs.form.validate = jest.fn(() => false)
        wrapper.vm.submit_log_request()
        expect(wrapper.emitted('submit-log-request')).toBeFalsy()

        wrapper.vm.$refs.form.validate = jest.fn(() => true)
        wrapper.vm.submit_log_request()
        expect(wrapper.emitted('submit-log-request')).toBeTruthy()
    })

    test('form validation is called', () => {
        wrapper.vm.validate_form()
        expect(wrapper.vm.$refs.form.validate).toHaveBeenCalled()
    })

    test('component emits correctly with prefilled URL', async() => {
        const params = new URLSearchParams()
        params.append(LOG_FORM_FIELDS.TIME_BEGIN_FROM, LOG_PARAMS.time_begin_from)
        params.append(LOG_FORM_FIELDS.TIME_BEGIN_TO, LOG_PARAMS.time_begin_to)
        params.append(LOG_FORM_FIELDS.SERVICE, LOG_PARAMS.log_services.selected_value)
        params.append(LOG_FORM_FIELDS.USERNAME, LOG_PARAMS.username)
        params.append(
            LOG_FORM_FIELDS.REFERENCE, `${LOG_PARAMS.object_reference_type}.${LOG_PARAMS.object_reference_value}`,
        )
        params.append(LOG_FORM_FIELDS.PROPERTIES, `${LOG_PARAMS.property_name}.${LOG_PARAMS.property_value}`)
        for (const log_entry_type of LOG_PARAMS.log_entry_types.selected_values)
            params.append(LOG_FORM_FIELDS.LOG_ENTRY_TYPES, log_entry_type)

        delete window.location
        window.location = { search: `?${params.toString()}` }
        wrapper = initWrapper(LoggerListForm, { mocks: { validate: jest.fn(), $notify: jest.fn() }})
        await flushPromises()

        expect(wrapper.emitted('update:time_begin_from')[0][0]).toBe(LOG_PARAMS.time_begin_from)
        expect(wrapper.emitted('update:time_begin_to')[0][0]).toBe(LOG_PARAMS.time_begin_to)
        expect(wrapper.emitted('update:selected_log_service')[0][0]).toBe(LOG_PARAMS.log_services.selected_value)
        expect(wrapper.emitted('update:selected_log_entry_types')[0][0])
            .toEqual(LOG_PARAMS.log_entry_types.selected_values)
        expect(wrapper.emitted('update:selected_reference')[0][0]).toBe(LOG_PARAMS.object_reference_type)
        expect(wrapper.emitted('update:reference_value')[0][0]).toBe(LOG_PARAMS.object_reference_value)
        expect(wrapper.emitted('update:property_name')[0][0]).toBe(LOG_PARAMS.property_name)
        expect(wrapper.emitted('update:property_value')[0][0]).toBe(LOG_PARAMS.property_value)
        expect(wrapper.emitted('submit-log-request')).toBeTruthy()
    })

    test('method submit_log_request emits correctly', async() => {
        wrapper.vm.$refs.form.validate = jest.fn(() => false)
        wrapper.vm.submit_log_request()
        expect(wrapper.emitted('submit-log-request')).toBeFalsy()

        wrapper.vm.$refs.form.validate = jest.fn(() => true)
        wrapper.vm.submit_log_request()
        expect(wrapper.emitted('submit-log-request')).toBeTruthy()
    })

    test('method load_services loads data correctly', async() => {
        console.log = jest.fn()
        const loaded_services = await wrapper.vm.load_services()
        expect(loaded_services).toStrictEqual(LOG_PARAMS.log_services.items)
    })

    test('method load_services fails by server error', async() => {
        console.log = jest.fn()
        axios_mock.onGet(URLS.log_services()).reply(500)
        await wrapper.vm.load_services()

        expect(wrapper.vm.$notify).toHaveBeenCalledWith(wrapper.vm.ERROR_SOMETHING_WENT_WRONG)
        expect(console.log).toHaveBeenCalledWith('error: notifications:something_went_wrong')
    })

    test('method load_services fails after component is mounted', async() => {
        console.log = jest.fn()
        axios_mock.onGet(URLS.log_services()).reply(500)
        wrapper = initWrapper(LoggerListForm, { mocks: { validate: jest.fn(), $notify: jest.fn() } })
        await flushPromises()
        expect(wrapper.vm.services_loaded).toStrictEqual(true)
        expect(wrapper.vm.form_params.all_log_services).toStrictEqual([])
        expect(wrapper.vm.$notify).toHaveBeenCalledWith(wrapper.vm.ERROR_SOMETHING_WENT_WRONG)
        expect(console.log).toHaveBeenCalledWith('error: notifications:something_went_wrong')
    })

    test('reference value is updated by URL', async() => {
        const params = new URLSearchParams()
        params.append(LOG_FORM_FIELDS.REFERENCE, '.some_reference_value')

        delete window.location
        window.location = { search: `?${params.toString()}` }
        wrapper = initWrapper(LoggerListForm, { mocks: { validate: jest.fn() }})
        await flushPromises()

        expect(wrapper.emitted('update:reference_value')[0][0]).toBe('some_reference_value')
        expect(wrapper.vm.services_loaded).toStrictEqual(true)
        expect(wrapper.vm.service_bound_fields_loaded).toStrictEqual(true)
    })

    test('method update_service_bound_fields updates log_entry_types correctly', async() => {
        console.log = jest.fn()
        await wrapper.vm.update_service_bound_fields(LOG_PARAMS.log_services.selected_value, true)
        expect(wrapper.vm.form_params.all_log_entry_types).toStrictEqual(LOG_PARAMS.log_entry_types.selected_values)
    })

    test('method update_service_bound_fields does not update fields when input is deleted', async() => {
        wrapper.vm.get_service_bound_fields = jest.fn()
        await wrapper.vm.update_service_bound_fields(null)
        expect(wrapper.vm.get_service_bound_fields).not.toHaveBeenCalled()
    })

    test('method update_service_bound_fields fails by server error', async() => {
        console.log = jest.fn()
        axios_mock.onGet(URLS.log_entry_types(LOG_PARAMS.log_services.selected_value)).reply(500)
        await wrapper.vm.update_service_bound_fields(LOG_PARAMS.log_services.selected_value, true)

        expect(wrapper.vm.$notify).toHaveBeenCalledWith(wrapper.vm.ERROR_SOMETHING_WENT_WRONG)
        expect(console.log).toHaveBeenCalledWith('error: notifications:something_went_wrong')
    })

    test('selected_log_service watcher works correctly', async() => {
        const new_service = 'new service'
        wrapper.vm.update_service_bound_fields = jest.fn()
        await wrapper.setProps({ selected_log_service: new_service })
        expect(wrapper.vm.update_service_bound_fields).toHaveBeenCalledWith(new_service, false)
    })

    test('method change_reference works correctly', () => {
        const new_reference = 'new reference'
        wrapper.vm.validate_form = jest.fn()
        wrapper.vm.change_reference(new_reference)
        expect(wrapper.vm.validate_form).toHaveBeenCalled()
        expect(wrapper.emitted('update:reference_value')[0][0]).toBe(new_reference)
    })

    test('method change_property_name works correctly', () => {
        const new_name = 'soup'
        wrapper.vm.validate_form = jest.fn()
        wrapper.vm.change_property_name(new_name)
        expect(wrapper.vm.validate_form).toHaveBeenCalled()
        expect(wrapper.emitted('update:property_name')[0][0]).toBe(new_name)
    })

    test('method change_property_value works correctly', () => {
        const new_value = 'gazpacho'
        wrapper.vm.validate_form = jest.fn()
        wrapper.vm.change_property_value(new_value)
        expect(wrapper.vm.validate_form).toHaveBeenCalled()
        expect(wrapper.emitted('update:property_value')[0][0]).toBe(new_value)
    })
})
