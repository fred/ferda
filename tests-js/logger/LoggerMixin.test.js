import axios from 'axios'
import MockAdapter from 'axios-mock-adapter'

import { initWrapper } from 'ferda-test/TestUtils'
import { LoggerMixin } from 'ferda/LoggerMixin'
import { URLS } from 'ferda/Urls'

describe('LoggerMixin', () => {
    let axios_mock
    let wrapper
    let Component

    beforeEach(() => {
        axios_mock = new MockAdapter(axios)
        Component = {
            render() {},
            mixins: [LoggerMixin],
        }

        wrapper = initWrapper(Component)
    })

    test('method get_services works', async() => {
        const expected_services = ['Service 1', 'Service 2']
        axios_mock.onGet(URLS.log_services()).reply(200, expected_services)
        const services = await wrapper.vm.get_services()

        expect(services).toStrictEqual(expected_services)
    })

    test('method get_services fails', async() => {
        axios_mock.onGet(URLS.log_services()).reply(500)

        expect(async() => await wrapper.vm.get_services()).rejects.toThrow('Error: Request failed with status code 500')
    })
})
