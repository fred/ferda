import axios from 'axios'
import MockAdapter from 'axios-mock-adapter'
import flushPromises from 'flush-promises'
import Vuetify from 'vuetify'

import { LOG_PARAMS_DETAIL, LOG_REQUEST_DETAIL, LOG_REQUEST_DETAIL_XML } from 'ferda-test/TestObjects'
import { initWrapper } from 'ferda-test/TestUtils'
import { URLS } from 'ferda/Urls.js'
import LoggerDetail from 'ferda/logger/LoggerDetail.vue'

describe('LoggerDetail', () => {
    let wrapper
    let axios_mock
    const initial_log = console.log

    beforeEach(async() => {
        axios_mock = new MockAdapter(axios)
        axios_mock.onGet(URLS.log_detail_ajax(LOG_PARAMS_DETAIL.log_entry_id)).reply(200, LOG_REQUEST_DETAIL)
        wrapper = initWrapper(LoggerDetail, {
            mocks: {
                $route: {
                    params: { log_id: LOG_PARAMS_DETAIL.log_entry_id },
                },
                $notify: jest.fn(),
            },
        })
        await flushPromises()
    })

    afterEach(() => {
        console.log = initial_log
    })

    test('matches snapshot', () => {
        expect(wrapper.element).toMatchSnapshot()
    })

    test('matches snapshot with xml', () => {
        axios_mock.onGet(URLS.log_detail_ajax(LOG_PARAMS_DETAIL.log_entry_id)).reply(200, LOG_REQUEST_DETAIL_XML)
        wrapper = initWrapper(LoggerDetail, {
            mocks: {
                $route: {
                    params: { log_id: LOG_PARAMS_DETAIL.log_entry_id },
                },
                $notify: jest.fn(),
            },
        })

        expect(wrapper.element).toMatchSnapshot()
    })

    test('method get_log_detail loads data correctly', async() => {
        await wrapper.setData({
            log_detail: LOG_REQUEST_DETAIL,
        })
        await wrapper.vm.get_log_detail(LOG_PARAMS_DETAIL.log_entry_id)

        expect(wrapper.vm.log_detail).toStrictEqual(LOG_REQUEST_DETAIL)
        expect(wrapper.vm.is_loaded).toBeTruthy()
    })

    test('method get_log_detail fails', async() => {
        console.log = jest.fn()
        axios_mock.onGet(URLS.log_detail_ajax(LOG_PARAMS_DETAIL.log_entry_id)).reply(500)
        const initial_log_id = wrapper.vm.log_id
        await wrapper.vm.get_log_detail({
            log_id: LOG_PARAMS_DETAIL.log_entry_id,
        })

        expect(wrapper.vm.log_id).toStrictEqual(initial_log_id)
        expect(wrapper.vm.is_loaded).toBeTruthy()
        expect(wrapper.vm.$notify).toHaveBeenCalledWith(wrapper.vm.ERROR_SOMETHING_WENT_WRONG)
        expect(console.log).toHaveBeenCalledWith('error: notifications:something_went_wrong')
    })

    test('method get_log_detail_data returns correct results', async() => {
        await wrapper.vm.get_log_detail_data(URLS.log_detail_ajax(LOG_PARAMS_DETAIL.log_entry_id))

        expect(wrapper.vm.log_etries_url).toStrictEqual(LOG_PARAMS_DETAIL.log_entries_url)
    })

    test('method get_log_detail_data fails by server error', async() => {
        console.log = jest.fn()
        axios_mock.onGet(URLS.log_detail_ajax(LOG_PARAMS_DETAIL.log_entry_id)).reply(500)
        wrapper = initWrapper(LoggerDetail, {
            mocks: {
                $route: {
                    params: { log_id: LOG_PARAMS_DETAIL.log_entry_id },
                },
                $notify: jest.fn(),
            },
        })
        await flushPromises()

        expect(wrapper.vm.log_detail).toBeNull()
        expect(wrapper.vm.$notify).toHaveBeenCalledWith(wrapper.vm.ERROR_SOMETHING_WENT_WRONG)
        expect(console.log).toHaveBeenCalledWith('error: notifications:something_went_wrong')
    })

    test('method get_log_detail_data is aborted', async() => {
        console.log = jest.fn()
        const initial_log_id = wrapper.vm.log_id
        wrapper.vm.get_log_detail_data(LOG_PARAMS_DETAIL.log_entries_url)
        wrapper.vm.stop_search()
        await flushPromises()

        expect(wrapper.vm.log_id).toStrictEqual(initial_log_id)
        expect(wrapper.vm.$notify).toHaveBeenCalledWith(wrapper.vm.INFO_REQUEST_ABORTED)
        expect(console.log).toHaveBeenCalledWith('info: notifications:search_aborted')
    })

    test('method is_reference_link works correctly', () => {
        expect(wrapper.vm.is_reference_link('contact', '12345abc-0000-44aa-88bb-abcdef112233')).toStrictEqual(true)
        expect(wrapper.vm.is_reference_link('contact', 'abcdefyz')).toStrictEqual(false)
        expect(wrapper.vm.is_reference_link('registrar', 'TEST-REG')).toStrictEqual(true)
    })

    test('method is_valid_xml returns true', () => {
        expect(wrapper.vm.is_valid_xml(LOG_REQUEST_DETAIL_XML.raw_request)).toBeTruthy()
    })

    test('method is_valid_xml returns false', () => {
        const non_valid_xml = `<?xml version="1.0" encoding="UTF-8" standalone="no"?>
       `
        expect(wrapper.vm.is_valid_xml(non_valid_xml)).toBeFalsy()
    })

    test('method is_valid_xml returns null', () => {
        expect(wrapper.vm.is_valid_xml(null)).toBe(false)
    })

    test('method format_xml returns format xml', () => {
        const format_xml =
`<?xml version="1.0" encoding="UTF-8" standalone="no"?>
    <epp xmlns="urn:aaaa:test:xml:ns:epp-1.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
    </epp>`

        expect(wrapper.vm.format_xml(LOG_REQUEST_DETAIL_XML.raw_request)).toStrictEqual(format_xml)
    })

    test('box with references is opened', () => {
        const breakpoint = {
            init: jest.fn(),
            framework: {},
            xl: true,
        }
        const vuetify = new Vuetify()
        vuetify.framework.breakpoint = breakpoint

        wrapper = initWrapper(LoggerDetail, {
            vuetify,
            mocks: {
                $route: {
                    params: { log_id: LOG_PARAMS_DETAIL.log_entry_id },
                },
                $notify: jest.fn(),
            },
        })
        expect(wrapper.vm.$vuetify.breakpoint.xl).toBe(true)
    })
})
