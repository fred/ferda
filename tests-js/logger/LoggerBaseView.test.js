import { initWrapper } from 'ferda-test/TestUtils'
import LoggerBaseView from 'ferda/logger/LoggerBaseView.vue'

describe('LoggerBaseView', () => {
    let wrapper

    beforeEach(() => {
        wrapper = initWrapper(LoggerBaseView, {
            stubs: ['RouterView'],
        })
    })

    test('matches snapshot', () => {
        expect(wrapper.element).toMatchSnapshot()
    })
})
