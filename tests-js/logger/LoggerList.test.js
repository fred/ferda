import axios from 'axios'
import MockAdapter from 'axios-mock-adapter'
import flushPromises from 'flush-promises'

import { LOG_PARAMS, LOG_REQUEST_RESULT } from 'ferda-test/TestObjects'
import { initWrapper } from 'ferda-test/TestUtils'
import { DATE_RANGE } from 'ferda/DatetimeMixin.js'
import { URLS } from 'ferda/Urls'
import LoggerList from 'ferda/logger/LoggerList.vue'

describe('LoggerList', () => {
    let wrapper
    let axios_mock
    const initial_log = console.log
    const default_history = window.history
    const default_location = window.location
    const initial_create = URL.createObjectURL
    const initial_revoke = URL.revokeObjectURL

    beforeEach(async() => {
        jest.useFakeTimers()
        jest.setSystemTime(new Date('2020-01-01T12:01:00+00:00'))
        axios_mock = new MockAdapter(axios)
        axios_mock.onGet(URLS.log_entries()).reply(200, LOG_REQUEST_RESULT)
        wrapper = initWrapper(LoggerList, { mocks: { $notify: jest.fn() }})
        await flushPromises()
    })

    afterEach(() => {
        jest.runOnlyPendingTimers()
        jest.useRealTimers()
        delete window.history
        delete window.location
        window.history = default_history
        window.location = default_location
        console.log = initial_log
        URL.createObjectURL = initial_create
        URL.revokeObjectURL = initial_revoke
    })

    test('matches snapshot', () => {
        expect(wrapper.element).toMatchSnapshot()
    })

    test('component is mounted with page_size prefilled', async() => {
        delete window.location
        window.location = { search: '?page_size=20' }
        wrapper = initWrapper(LoggerList, { mocks: { $notify: jest.fn() }})
        await flushPromises()

        expect(wrapper.vm.page_size).toBe(20)
    })

    test('method submit_log_request loads data correctly', async() => {
        await wrapper.setData({ active_date_range: DATE_RANGE.CURRENT_HOUR})
        await wrapper.vm.submit_log_request(null, true, true)
        expect(wrapper.vm.log_entries).toStrictEqual(LOG_REQUEST_RESULT.results)
    })

    test('method submit_log_request fails', async() => {
        console.log = jest.fn()
        axios_mock.onGet(URLS.log_entries()).reply(500)
        const initial_log_entries = wrapper.vm.log_entries
        await wrapper.vm.submit_log_request()

        expect(wrapper.vm.log_entries).toStrictEqual(initial_log_entries)
        expect(wrapper.vm.$notify).toHaveBeenCalledWith(wrapper.vm.ERROR_SOMETHING_WENT_WRONG)
        expect(console.log).toHaveBeenCalledWith('error: notifications:something_went_wrong')
    })

    test('method get_log_entries returns correct results', async() => {
        const log_entries = await wrapper.vm.get_log_entries({
            time_begin_from: LOG_PARAMS.time_begin_from,
            time_begin_to: LOG_PARAMS.time_begin_to,
            service: LOG_PARAMS.log_services.selected_value,
            log_entry_types: LOG_PARAMS.log_entry_types.selected_values,
        })

        expect(log_entries).toStrictEqual(LOG_REQUEST_RESULT)
        expect(wrapper.vm.$notify).not.toHaveBeenCalled()
    })

    test('method get_log_entries fails by server error', async() => {
        console.log = jest.fn()
        axios_mock.onGet(URLS.log_entries()).reply(500)
        const log_entries = await wrapper.vm.get_log_entries({
            time_begin_from: LOG_PARAMS.time_begin_from,
            time_begin_to: LOG_PARAMS.time_begin_to,
            service: LOG_PARAMS.log_services.selected_value,
            log_entry_types: LOG_PARAMS.log_entry_types.selected_values,
            page_size: LOG_PARAMS.page_size,
        })

        expect(log_entries).toStrictEqual(null)
        expect(wrapper.vm.$notify).toHaveBeenCalledWith(wrapper.vm.ERROR_SOMETHING_WENT_WRONG)
        expect(console.log).toHaveBeenCalledWith('error: notifications:something_went_wrong')
    })

    test('searching of log_entries is aborted', async() => {
        console.log = jest.fn()
        const initial_log_entries = wrapper.vm.log_entries
        wrapper.vm.submit_log_request()
        wrapper.vm.stop_search()
        await flushPromises()

        expect(wrapper.vm.log_entries).toStrictEqual(initial_log_entries)
        expect(console.log).toHaveBeenCalledWith('info: notifications:search_aborted')
        expect(wrapper.vm.$notify).toHaveBeenCalledWith(wrapper.vm.INFO_REQUEST_ABORTED)
    })

    test('method get_log_url_params returns correct params - url not specified', () => {
        const expected_params = [
            'time_begin_from=2021-01-01T12%3A00%3A00%2B01%3A00&', 'time_begin_to=2022-01-01T12%3A00%3A00%2B01%3A00&',
            'service=Example+2&', 'log_entry_types=Type+1&log_entry_types=Type+2&', 'username=jdoe&',
            'reference=ref_type.ref_value&', 'properties=soup.gazpacho&', 'page_size=10',
        ]

        expect(wrapper.vm.get_log_url_params({
            time_begin_from: LOG_PARAMS.time_begin_from,
            time_begin_to: LOG_PARAMS.time_begin_to,
            selected_log_service: LOG_PARAMS.log_services.selected_value,
            selected_log_entry_types: LOG_PARAMS.log_entry_types.selected_values,
            username: LOG_PARAMS.username,
            selected_reference: LOG_PARAMS.object_reference_type,
            reference_value: LOG_PARAMS.object_reference_value,
            property_name: LOG_PARAMS.property_name,
            property_value: LOG_PARAMS.property_value,
        }, LOG_PARAMS.page_size, null).toString()).toBe(expected_params.join(''))
    })

    test('method get_log_url_params returns correct params - url spceified', () => {
        expect(wrapper.vm.get_log_url_params({}, 20, 'http://someurl.com/?test_param=value').toString())
            .toBe('test_param=value&page_size=20')
    })

    test('method stop_search aborts log_entries request', async() => {
        wrapper.vm.abort_request = jest.fn()
        await wrapper.setData({ abort_controller_log_entries: new window.AbortController() })
        wrapper.vm.stop_search()
        expect(wrapper.vm.abort_request).toHaveBeenCalledTimes(1)
    })

    test('method change_page_size works correctly', async() => {
        wrapper.vm.submit_log_request = jest.fn()
        await wrapper.vm.change_page_size(50)
        expect(wrapper.vm.page_size).toStrictEqual(50)
        expect(wrapper.vm.submit_log_request).toHaveBeenCalledWith(null, true)
    })

    test('date range has been reset - time_begin_from', async() => {
        wrapper.vm.get_date_range = jest.fn(() => '2021-02-01T00:00:00+00:00')
        await wrapper.setData({ log_request_params: { time_begin_from: '2022-02-01T00:00:00+00:00' }})
        expect(wrapper.vm.active_date_range).toStrictEqual(null)
    })

    test('date range has been reset - time_begin_to', async() => {
        wrapper.vm.get_date_range = jest.fn(() => '2021-02-01T00:00:00+00:00')
        await wrapper.setData({ log_request_params: { time_begin_to: '2022-02-01T00:00:00+00:00' }})
        expect(wrapper.vm.active_date_range).toStrictEqual(null)
    })
})
