import { LOG_PARAMS, LOG_REQUEST_RESULT } from 'ferda-test/TestObjects'
import { initWrapper } from 'ferda-test/TestUtils'
import LoggerListTable from 'ferda/logger/LoggerListTable.vue'

describe('LoggerListTable', () => {
    let wrapper

    beforeEach(() => {
        wrapper = initWrapper(LoggerListTable, {
            propsData: {
                log_entries: LOG_REQUEST_RESULT.results,
                page_size: LOG_PARAMS.page_size,
                current_page: 1,
            },
            mocks: { reset: jest.fn(), blur: jest.fn() },
        })
    })

    test('matches snapshot', () => {
        expect(wrapper.element).toMatchSnapshot()
    })

    test('method change_page_size emits correctly', () => {
        wrapper.vm.change_page_size(50)
        expect(wrapper.emitted('update:page_size')[0][0]).toBe(50)
        expect(wrapper.emitted('change-page-size')[0][0]).toBe(50)
        expect(wrapper.emitted('update:current_page')[0][0]).toBe(1)
    })
})
