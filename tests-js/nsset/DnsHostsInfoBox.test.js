import { NSSET } from 'ferda-test/TestObjects'
import { initWrapper } from 'ferda-test/TestUtils'
import DnsHostsInfoBox from 'ferda/nsset/DnsHostsInfoBox.vue'

describe('DnsHostsInfoBox', () => {
    let wrapper

    beforeEach(() => {
        wrapper = initWrapper(DnsHostsInfoBox, {
            propsData: {
                dns_hosts: NSSET.dns_hosts,
            },
        })
    })

    test('matches snapshot', () => {
        expect(wrapper.element).toMatchSnapshot()
    })
})
