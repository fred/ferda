import { NSSET } from 'ferda-test/TestObjects'
import { initWrapper } from 'ferda-test/TestUtils'
import NssetDetail from 'ferda/nsset/NssetDetail.vue'

describe('NssetDetail', () => {
    let wrapper

    beforeEach(() => {
        wrapper = initWrapper(NssetDetail, {
            propsData: {
                uuid: '00000000-0000-0000-0000-000000000000',
                history_id: '00000000-0000-0000-0000-000000000001',
                nsset: NSSET,
            },
        })
    })

    test('matches snapshot', () => {
        expect(wrapper.element).toMatchSnapshot()
    })
})
