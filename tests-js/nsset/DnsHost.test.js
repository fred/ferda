import { NSSET, SEARCH_NSSET } from 'ferda-test/TestObjects'
import { getClonedStoreOptions, initWrapper } from 'ferda-test/TestUtils'
import DnsHost from 'ferda/nsset/DnsHost.vue'

describe('DnsHost', () => {
    let wrapper
    let store

    const createElementWrapper = (dns_host, mark) => {
        wrapper = initWrapper(DnsHost, {
            propsData: {
                dns_host,
                mark,
            },
        })
        return wrapper.element
    }

    test('matches snapshot 1 marked', () => {
        expect(createElementWrapper(NSSET.dns_hosts[0], true)).toMatchSnapshot()
    })

    test('matches snapshot 1 unmarked', () => {
        expect(createElementWrapper(NSSET.dns_hosts[0], false)).toMatchSnapshot()
    })

    test('matches snapshot 2 marked', () => {
        expect(createElementWrapper(NSSET.dns_hosts[1], true)).toMatchSnapshot()
    })

    test('matches snapshot 2 unmarked', () => {
        expect(createElementWrapper(NSSET.dns_hosts[1], false)).toMatchSnapshot()
    })

    test('method ip_address_matches_exactly returns correct results - no history', () => {
        store = getClonedStoreOptions()
        store.modules.search.getters.last_query_values = () => ['127.0.0.1']

        wrapper = initWrapper(DnsHost, {
            propsData: {
                dns_host: SEARCH_NSSET.dns_hosts[0],
                mark: true,
                info: { ...SEARCH_NSSET, matched_items: ['dns_hosts.ip_addresses'] },
            },
            customStoreOptions: store,
        })

        expect(wrapper.vm.ip_address_matches_exactly('127.0.0.1')).toStrictEqual(true)
    })

    test('method ip_address_matches_exactly returns correct results - history', () => {
        store = getClonedStoreOptions()
        store.modules.search.getters.last_query_values = () => ['127.0.0.1']

        wrapper = initWrapper(DnsHost, {
            propsData: {
                dns_host: SEARCH_NSSET.dns_hosts[0],
                mark: true,
                info: { item_name: 'dns_hosts.ip_addresses' },
                is_registry_search: true,
            },
            customStoreOptions: store,
        })

        expect(wrapper.vm.ip_address_matches_exactly('127.0.0.1')).toStrictEqual(true)
    })
})
