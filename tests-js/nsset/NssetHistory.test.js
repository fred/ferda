import { createLocalVue } from '@vue/test-utils'
import axios from 'axios'
import MockAdapter from 'axios-mock-adapter'
import flushPromises from 'flush-promises'

import { CONTACT_OBIWAN, CONTACT_YODA, NSSET, NSSET2 } from 'ferda-test/TestObjects'
import { initWrapper } from 'ferda-test/TestUtils'
import { OBJECT_TYPE } from 'ferda/RegistryMixin'
import { URLS } from 'ferda/Urls'
import ObjectHistory from 'ferda/common/ObjectHistory.vue'
import NssetHistory from 'ferda/nsset/NssetHistory.vue'

const localVue = createLocalVue()
localVue.component('ObjectHistory', ObjectHistory)

describe('NssetHistory', () => {
    let axios_mock
    let wrapper

    beforeEach(async() => {
        axios_mock = new MockAdapter(axios)
        axios_mock.onGet(URLS.object_info_history_ajax(OBJECT_TYPE.NSSET, NSSET.nsset_id, 'V1')).reply(200, NSSET)
        axios_mock.onGet(URLS.object_info_history_ajax(OBJECT_TYPE.NSSET, NSSET.nsset_id, 'V2')).reply(200, NSSET2)
        axios_mock.onGet(URLS.object_info_ajax(OBJECT_TYPE.CONTACT, CONTACT_OBIWAN.contact_id))
            .reply(200, CONTACT_OBIWAN)
        axios_mock.onGet(
            URLS.object_info_history_ajax_datetime(
                OBJECT_TYPE.CONTACT, CONTACT_OBIWAN.contact_id, '2019-01-01T13:00:00Z',
            ),
        ).reply(200, CONTACT_OBIWAN)
        axios_mock.onGet(
            URLS.object_info_history_ajax_datetime(
                OBJECT_TYPE.CONTACT, CONTACT_OBIWAN.contact_id, '2020-01-01T13:00:00Z',
            ),
        ).reply(200, CONTACT_OBIWAN)
        axios_mock
            .onGet(URLS.object_info_ajax(OBJECT_TYPE.CONTACT, CONTACT_YODA.contact_id))
            .reply(200, CONTACT_YODA)
        axios_mock.onGet(
            URLS.object_info_history_ajax_datetime(
                OBJECT_TYPE.CONTACT, CONTACT_YODA.contact_id, '2019-01-01T13:00:00Z',
            ),
        ).reply(200, CONTACT_YODA)

        wrapper = initWrapper(NssetHistory, {
            localVue,
            propsData: {
                uuid: NSSET.nsset_id,
                object_handle: NSSET.nsset_handle,
                from_history_id: 'V1',
                to_history_id: 'V2',
                object_history: {
                    timeline: [
                        {nsset_history_id: 'V1', valid_from: '2019-01-01T13:00:00Z'},
                        {nsset_history_id: 'V2', valid_from: '2020-01-01T13:00:00Z'},
                    ],
                    valid_to: null,
                },
            },
            stubs: {
                /* ObjectHistory specific */
                AppHeader: true,
                EmbeddedSearchBar: true,
                HistoryNavigation: true,
                RelatedRegistrarHistory: true,
                ObjectHistoryEvents: true,
                StateFlagsHistory: true,
                /* NssetHistory specific */
                NssetHistoryInfo: true,
                DnsHostsHistory: true,
                RelatedContactsHistory: true,
            },
        }, false)
        await flushPromises()
    })

    test('matches snapshot', () => {
        expect(wrapper.element).toMatchSnapshot()
    })
})
