import { NSSET } from 'ferda-test/TestObjects'
import { initWrapper } from 'ferda-test/TestUtils'
import NssetDetailInfo from 'ferda/nsset/NssetDetailInfo.vue'

describe('NssetDetailInfo', () => {
    let wrapper

    beforeEach(() => {
        wrapper = initWrapper(NssetDetailInfo, {
            propsData: {
                nsset: NSSET,
            },
        })
    })

    test('matches snapshot', () => {
        expect(wrapper.element).toMatchSnapshot()
    })
})
