import { createLocalVue } from '@vue/test-utils'
import axios from 'axios'
import MockAdapter from 'axios-mock-adapter'
import flushPromises from 'flush-promises'

import { CONTACT_OBIWAN, CONTACT_YODA, KEYSET, NSSET } from 'ferda-test/TestObjects'
import { initWrapper } from 'ferda-test/TestUtils'
import { OBJECT_TYPE } from 'ferda/RegistryMixin'
import { URLS } from 'ferda/Urls'
import ObjectHistory from 'ferda/common/ObjectHistory.vue'
import DomainHistory from 'ferda/domain/DomainHistory.vue'

const localVue = createLocalVue()
localVue.component('ObjectHistory', ObjectHistory)

describe('DomainHistory', () => {
    let axios_mock
    let wrapper

    beforeEach(async() => {
        axios_mock = new MockAdapter(axios)
        axios_mock.onGet(URLS.object_info_history_ajax(OBJECT_TYPE.DOMAIN, 'UUID-JEDI', 'V1')).reply(200, {
            fqdn: 'jedi-order.cz',
            expires_at: '2020-01-01T13:00:00Z',
            registrant: { id: CONTACT_YODA.contact_id },
            administrative_contacts: [
                { id: CONTACT_OBIWAN.contact_id },
            ],
            nsset: { id: NSSET.nsset_id },
            keyset: { id: KEYSET.keyset_id },
            events: {},
        })
        axios_mock.onGet(URLS.object_info_history_ajax(OBJECT_TYPE.DOMAIN, 'UUID-JEDI', 'V2')).reply(200, {
            fqdn: 'jedi-order.cz',
            expires_at: '2021-01-01T13:00:00Z',
            registrant: { id: CONTACT_OBIWAN.contact_id },
            administrative_contacts: [],
            nsset: { id: NSSET.nsset_id },
            keyset: { id: KEYSET.keyset_id },
            events: {},
        })
        axios_mock.onGet(URLS.object_info_ajax(OBJECT_TYPE.CONTACT, CONTACT_OBIWAN.contact_id))
            .reply(200, CONTACT_OBIWAN)
        axios_mock.onGet(
            URLS.object_info_history_ajax_datetime(
                OBJECT_TYPE.CONTACT, CONTACT_OBIWAN.contact_id, '2019-01-01T13:00:00Z',
            ),
        ).reply(200, CONTACT_OBIWAN)
        axios_mock.onGet(
            URLS.object_info_history_ajax_datetime(
                OBJECT_TYPE.CONTACT, CONTACT_OBIWAN.contact_id, '2020-01-01T13:00:00Z',
            ),
        ).reply(200, CONTACT_OBIWAN)
        axios_mock.onGet(URLS.object_info_ajax(
            OBJECT_TYPE.CONTACT, CONTACT_YODA.contact_id),
        ).reply(200, CONTACT_YODA)
        axios_mock.onGet(
            URLS.object_info_history_ajax_datetime(
                OBJECT_TYPE.CONTACT, CONTACT_YODA.contact_id, '2019-01-01T13:00:00Z',
            ),
        ).reply(200, CONTACT_YODA)
        axios_mock.onGet(
            URLS.object_info_history_ajax_datetime(
                OBJECT_TYPE.CONTACT, CONTACT_YODA.contact_id, '2020-01-01T13:00:00Z',
            ),
        ).reply(200, CONTACT_YODA)
        axios_mock.onGet(URLS.object_info_ajax(OBJECT_TYPE.NSSET, NSSET.nsset_id)).reply(200, NSSET)
        axios_mock.onGet(
            URLS.object_info_history_ajax_datetime(OBJECT_TYPE.NSSET, NSSET.nsset_id, '2019-01-01T13:00:00Z'),
        ).reply(200, NSSET)
        axios_mock.onGet(
            URLS.object_info_history_ajax_datetime(OBJECT_TYPE.NSSET, NSSET.nsset_id, '2020-01-01T13:00:00Z'),
        ).reply(200, NSSET)
        axios_mock.onGet(
            URLS.object_info_history_ajax_datetime(OBJECT_TYPE.KEYSET, KEYSET.keyset_id, '2019-01-01T13:00:00Z'),
        ).reply(200, KEYSET)
        axios_mock.onGet(URLS.object_info_ajax(OBJECT_TYPE.KEYSET, KEYSET.keyset_id)).reply(200, KEYSET)
        axios_mock.onGet(
            URLS.object_info_history_ajax_datetime(OBJECT_TYPE.KEYSET, KEYSET.keyset_id, '2020-01-01T13:00:00Z'),
        ).reply(200, KEYSET)

        wrapper = initWrapper(DomainHistory, {
            localVue,
            propsData: {
                uuid: 'UUID-JEDI',
                object_handle: 'jedi-order.cz',
                from_history_id: 'V1',
                to_history_id: 'V2',
                object_history: {
                    timeline: [
                        {domain_history_id: 'V1', valid_from: '2019-01-01T13:00:00Z'},
                        {domain_history_id: 'V2', valid_from: '2020-01-01T13:00:00Z'},
                    ],
                    valid_to: null,
                },
            },
            stubs: {
                /* ObjectHistory specific */
                AppHeader: true,
                EmbeddedSearchBar: true,
                HistoryNavigation: true,
                RelatedRegistrarHistory: true,
                ObjectHistoryEvents: true,
                StateFlagsHistory: true,
                /* DomainHistory specific */
                DomainHistoryInfo: true,
                RelatedContactHistory: true,
                RelatedContactsHistory: true,
                RelatedNssetHistory: true,
                RelatedKeysetHistory: true,
            },
        }, false)
        await flushPromises()
    })

    test('matches snapshot', () => {
        expect(wrapper.element).toMatchSnapshot()
    })
})
