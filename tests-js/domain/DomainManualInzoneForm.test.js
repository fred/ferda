import { initWrapper } from 'ferda-test/TestUtils'
import DomainManualInzoneForm from 'ferda/domain/DomainManualInzoneForm.vue'

describe('DomainManualInzoneForm', () => {
    let wrapper
    const initial_console = console.log

    beforeEach(() => {
        jest.useFakeTimers()
        jest.setSystemTime(new Date('2022-01-01T12:00:00+00:00'))
        wrapper = initWrapper(DomainManualInzoneForm, {
            propsData: {
                is_loading: false,
                dialog_opened: false,
            },
        })
        wrapper.vm.$refs.form.validate = jest.fn()
    })

    afterEach(() => {
        console.log = initial_console
        jest.runOnlyPendingTimers()
        jest.useRealTimers()
    })

    test('matches snapshot', () => {
        expect(wrapper.element).toMatchSnapshot()
    })

    test('method submit_dialog works correctly - is valid', async() => {
        wrapper.vm.$refs.form = { validate: jest.fn(() => true) }

        await wrapper.vm.submit_dialog()

        expect(wrapper.emitted('submit-dialog')[0][0]).toStrictEqual({
            valid_from: '2022-01-01T12:00:00+00:00',
            valid_to: '2022-01-08T12:00:00+00:00',
        })
    })

    test('method submit_dialog works correctly - is not valid', async() => {
        wrapper.vm.$refs.form = { validate: jest.fn(() => false) }
        await wrapper.vm.submit_dialog()
        expect(wrapper.emitted('submit-dialog')).toBeFalsy()
    })

    test('Sets correct date - after open dialog', async() => {
        await wrapper.setProps({ dialog_opened: true })
        expect(wrapper.vm.inzone_valid_from).toStrictEqual('2022-01-01T12:00:00+00:00')
        expect(wrapper.vm.inzone_valid_to).toStrictEqual('2022-01-08T12:00:00+00:00')
    })
})
