import { DOMAIN } from 'ferda-test/TestObjects'
import { initWrapper } from 'ferda-test/TestUtils'
import DomainDetailInfo from 'ferda/domain/DomainDetailInfo.vue'

describe('DomainDetailInfo', () => {
    let wrapper

    beforeEach(() => {
        jest.useFakeTimers()
        jest.setSystemTime(new Date('2023-01-24T00:00:00+00:00'))
        wrapper = initWrapper(DomainDetailInfo, {
            propsData: {
                domain: DOMAIN,
            },
        })
    })

    afterEach(() => {
        jest.runOnlyPendingTimers()
        jest.useRealTimers()
    })

    test('matches snapshot', () => {
        expect(wrapper.element).toMatchSnapshot()
    })

    test('property current_delete_date returns correct values', async() => {
        await wrapper.setProps(
            { domain: { events: { unregistered: null }, delete_candidate_at: '2021-01-01T12:00:00+01:00'  }},
        )
        expect(wrapper.vm.current_delete_date).toBe('2021-01-01T12:00:00+01:00')

        await wrapper.setProps(
            { domain: { events: { unregistered: { timestamp: '2022-01-01T12:00:00+01:00' }}}},
        )
        expect(wrapper.vm.current_delete_date).toBe('2022-01-01T12:00:00+01:00')

        await wrapper.setProps(
            { domain: { events: { unregistered: null }, delete_candidate_scheduled_at: '2023-01-01T12:00:00+01:00'  }},
        )
        expect(wrapper.vm.current_delete_date).toBe('2023-01-01T12:00:00+01:00')
    })
})
