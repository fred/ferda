import axios from 'axios'
import MockAdapter from 'axios-mock-adapter'
import flushPromises from 'flush-promises'

import { STATE_FLAGS } from 'ferda-test/TestObjects'
import { getClonedStoreOptions, initWrapper } from 'ferda-test/TestUtils'
import { OBJECT_TYPE } from 'ferda/RegistryMixin'
import { URLS } from 'ferda/Urls.js'
import DomainDetail from 'ferda/domain/DomainDetail.vue'

describe('DomainDetail', () => {
    let axios_mock
    let wrapper
    let store
    const initial_log = console.log

    beforeEach(async() => {
        jest.useFakeTimers()
        jest.setSystemTime(new Date('2020-01-01T12:00:00+00:00'))

        store = getClonedStoreOptions()
        store.modules.general.getters.permissions = () => ['ferda.can_set_manual_in_zone']

        axios_mock = new MockAdapter(axios)
        axios_mock.onGet(URLS.object_info_ajax(OBJECT_TYPE.NSSET, '473f6dd9-af61-4723-f4b7-52caf97803e2'))
            .reply(200, {})
        axios_mock.onGet(URLS.object_info_ajax(OBJECT_TYPE.KEYSET, '743d82ad-0122-54ce-b3ed-b1ec07e16f42'))
            .reply(200, {})
        axios_mock.onGet(URLS.object_state_ajax(OBJECT_TYPE.DOMAIN, '00000000-0000-0000-0000-000000000000'))
            .reply(200, STATE_FLAGS)
        axios_mock.onPost(URLS.manual_in_zone('00000000-0000-0000-0000-000000000000')).reply(200)

        wrapper = initWrapper(DomainDetail, {
            propsData: {
                uuid: '00000000-0000-0000-0000-000000000000',
                history_id: '00000000-0000-0000-0000-000000000001',
                domain: {
                    fqdn: 'example.com',
                    registrant: '11111111-1111-1111-1111-111111111111',
                    administrative_contacts: [
                        '22222222-2222-2222-2222-222222222222',
                        '33333333-3333-3333-3333-333333333333',
                    ],
                    events: {},
                    nsset: '473f6dd9-af61-4723-f4b7-52caf97803e2',
                    keyset: '743d82ad-0122-54ce-b3ed-b1ec07e16f42',
                },
            },
            mocks: {
                $notify: jest.fn(),
                '$cookies': {
                    get: () => jest.fn(),
                },
            },
            customStoreOptions: store,
        })
        await flushPromises()
    })
    afterEach(() => {
        jest.runOnlyPendingTimers()
        jest.useRealTimers()
        console.log = initial_log
    })

    test('matches snapshot', () => {
        expect(wrapper.element).toMatchSnapshot()
    })

    test('computed data is changed', async() => {
        await wrapper.setProps({
            uuid: '00000000-0000-0000-0000-000000000000',
            history_id: '00000000-0000-0000-0000-000000000001',
            domain: {
                fqdn: 'example.com',
                registrant: '11111111-1111-1111-1111-111111111111',
                administrative_contacts: [
                    '22222222-2222-2222-2222-222222222222',
                    '33333333-3333-3333-3333-333333333333',
                ],
                events: {},
                nsset: null,
                keyset: null,
            }})
        expect(wrapper.vm.related_nsset).toBe(null)
        expect(wrapper.vm.related_keyset).toBe(null)
    })

    test('method set_domain_inzone_disabled works correctly', () => {
        wrapper.vm.set_domain_inzone_disabled()
        expect(wrapper.vm.in_zone_disabled).toStrictEqual(true)

        wrapper.setData({ object_state_flags: { serverInzoneManual: false }})
        wrapper.vm.set_domain_inzone_disabled()
        expect(wrapper.vm.in_zone_disabled).toStrictEqual(false)
    })

    test('method submit_manual_in_zone works correctly', async() => {
        wrapper.vm.set_manual_in_zone = jest.fn()
        const inzone_data = {
            start: '2022-01-08TT00:00:00Z',
            end: '2022-01-25TT00:00:00Z',
        }
        await wrapper.setData({ in_zone_disabled: false})
        await wrapper.vm.submit_manual_in_zone(inzone_data)
        expect(wrapper.vm.set_manual_in_zone).toHaveBeenCalled()
    })

    test('method get_url_params works correctly', () => {
        const expected_params = 'start=2022-01-01TT00%3A00%3A00Z&end=2022-01-08TT00%3A00%3A00Z'
        const DATE_IN_ZONE = {
            inzone_valid_from: '2022-01-01TT00:00:00Z',
            inzone_valid_to:'2022-01-08TT00:00:00Z',
        }
        expect(wrapper.vm.get_url_params(DATE_IN_ZONE.inzone_valid_from, DATE_IN_ZONE.inzone_valid_to).toString())
            .toBe(expected_params)
        expect(wrapper.vm.get_url_params(null, null)).toBeUndefined()
    })

    test('method set_manual_in_zone works correctly', async() => {
        console.log = jest.fn()
        const DATE_IN_ZONE = {
            inzone_valid_from: '2022-01-01TT00:00:00Z',
            inzone_valid_to:'2022-01-08TT00:00:00Z',
        }
        await wrapper.vm.set_manual_in_zone(DATE_IN_ZONE.inzone_valid_to, DATE_IN_ZONE.inzone_valid_from)
        expect(wrapper.vm.$notify).toHaveBeenCalledWith(wrapper.vm.SUCCESS_CHANGES_SAVED)
        expect(wrapper.vm.inzone_loading).toStrictEqual(false)
        expect(wrapper.vm.inzone_menu_opened).toStrictEqual(false)
    })

    test('method set_manual_in_zone fails by error', async() => {
        axios_mock.onPost(URLS.manual_in_zone('00000000-0000-0000-0000-000000000000')).reply(500)
        console.log = jest.fn()
        wrapper.vm.$notify = jest.fn()
        await wrapper.vm.set_manual_in_zone(false)
        expect(wrapper.vm.$notify).toHaveBeenCalledWith(wrapper.vm.ERROR_SOMETHING_WENT_WRONG)
        expect(console.log).toHaveBeenCalledWith('error: notifications:something_went_wrong')
    })
})
