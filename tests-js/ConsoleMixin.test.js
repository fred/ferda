import { ConsoleMixin } from 'ferda/ConsoleMixin'

import { initWrapper } from './TestUtils'

describe('ConsoleMixin', () => {
    let Component
    let wrapper
    const old_node_env = process.env.NODE_ENV
    const error = { name: 'test_error' }

    beforeEach(() => {
        Component = {
            render() {},
            mixins: [ConsoleMixin],
        }
        wrapper = initWrapper(Component)
    })

    test('Error is logged', () => {
        console.error = jest.fn()
        process.env.NODE_ENV = 'development'
        wrapper.vm.log_error(error)

        expect(console.error).toHaveBeenCalledWith(error)
    })

    test('Error is not logged', () => {
        console.error = jest.fn()
        process.env.NODE_ENV = 'production'
        wrapper.vm.log_error(error)

        expect(console.error).toHaveBeenCalledTimes(0)
    })

    afterEach(() => {
        process.env.NODE_ENV = old_node_env
    })
})
