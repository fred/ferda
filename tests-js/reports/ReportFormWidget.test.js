import { initWrapper } from 'ferda-test/TestUtils'
import ReportFormWidget from 'ferda/reports/ReportFormWidget.vue'

describe('ReportFormWidget', () => {
    let wrapper

    test('matches snapshot for character varying', () => {
        const options = {
            propsData: {
                pg_type: 'character varying',
                label: 'Test 1',
                pg_parameter_name: 'test1',
                value: 'test1',
            },
        }

        wrapper = initWrapper(ReportFormWidget, options)
        expect(wrapper.element).toMatchSnapshot()
    })

    test('matches snapshot for text', () => {
        const options = {
            propsData: {
                pg_type: 'text',
                label: 'Test 2',
                pg_parameter_name: 'test2',
                value: 'test2',
            },
        }

        wrapper = initWrapper(ReportFormWidget, options)
        expect(wrapper.element).toMatchSnapshot()
    })

    test('matches snapshot for number', () => {
        const options = {
            propsData: {
                pg_type: 'integer',
                label: 'Test 3',
                pg_parameter_name: 'test3',
                value: 'test3',
            },
        }

        wrapper = initWrapper(ReportFormWidget, options)
        expect(wrapper.element).toMatchSnapshot()
    })

    test('matches snapshot for checkbox', () => {
        const options = {
            propsData: {
                pg_type: 'boolean',
                label: 'Test 4',
                pg_parameter_name: 'test4',
                value: true,
            },
        }

        wrapper = initWrapper(ReportFormWidget, options)
        expect(wrapper.element).toMatchSnapshot()
    })

    test('matches snapshot for date', () => {
        const options = {
            propsData: {
                pg_type: 'date',
                label: 'Test 5',
                pg_parameter_name: 'test5',
                value: '2021-01-01',
            },
        }

        wrapper = initWrapper(ReportFormWidget, options)
        expect(wrapper.element).toMatchSnapshot()
    })

    test('matches snapshot for time', () => {
        const options = {
            propsData: {
                pg_type: 'time',
                label: 'Test 6',
                pg_parameter_name: 'test6',
                value: '1:33',
            },
        }

        wrapper = initWrapper(ReportFormWidget, options)
        expect(wrapper.element).toMatchSnapshot()
    })

    test('matches snapshot for timestamp', () => {
        const options = {
            propsData: {
                pg_type: 'timestamp',
                label: 'Test 7',
                pg_parameter_name: 'test7',
                value: '2021-01-01 1:33',
            },
        }

        wrapper = initWrapper(ReportFormWidget, options)
        expect(wrapper.element).toMatchSnapshot()
    })

    test('widget emitts correctly', async() => {
        const options = {
            propsData: {
                pg_type: 'character varying',
                label: 'Test 1',
                pg_parameter_name: 'test1',
                value: 'test1',
            },
            stubs: {
                'VTextField': {
                    template: '<input type="text" @input="$listeners.input" />',
                },
            },
        }

        wrapper = initWrapper(ReportFormWidget, options)

        const input = wrapper.find('input')
        await input.setValue('new value')
        await input.trigger('input')

        expect(wrapper.emitted('update:value')).toBeTruthy()
    })
})
