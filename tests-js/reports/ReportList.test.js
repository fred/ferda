import { REPORT } from 'ferda-test/TestObjects'
import { initWrapper } from 'ferda-test/TestUtils'
import ReportList from 'ferda/reports/ReportList.vue'

describe('ReportList', () => {
    let wrapper

    beforeEach(() => {
        wrapper = initWrapper(ReportList, { propsData: { reports: [REPORT] } })
    })

    test('matches snapshot', () => {
        expect(wrapper.element).toMatchSnapshot()
    })

    test('empty matches snapshot', () => {
        wrapper = initWrapper(ReportList)

        expect(wrapper.element).toMatchSnapshot()
    })

    test('property filtered_reports returns correct results', async() => {
        await wrapper.setData({ report_list_query: 'test', selected_services: [REPORT.service] })
        expect(wrapper.vm.filtered_reports).toStrictEqual([REPORT])

        await wrapper.setData({ report_list_query: '', selected_services: [] })
        expect(wrapper.vm.filtered_reports).toStrictEqual([REPORT])

        await wrapper.setData({ report_list_query: 'test', selected_services: [] })
        expect(wrapper.vm.filtered_reports).toStrictEqual([REPORT])

        await wrapper.setData({ report_list_query: '', selected_services: [REPORT.service] })
        expect(wrapper.vm.filtered_reports).toStrictEqual([REPORT])

        await wrapper.setData({ report_list_query: 'test', selected_services: ['something'] })
        expect(wrapper.vm.filtered_reports).toStrictEqual([])

        await wrapper.setData({ report_list_query: 'something', selected_services: [REPORT.service] })
        expect(wrapper.vm.filtered_reports).toStrictEqual([])
    })
})
