import { REPORT_TABLE_DATA_FILTERED, REPORT_TABLE_DATA_PARSED, REPORT_TABLE_HEADERS } from 'ferda-test/TestObjects'
import { initWrapper } from 'ferda-test/TestUtils'
import { EXPORT_TYPE } from 'ferda/constants'
import ReportTable from 'ferda/reports/ReportTable.vue'

describe('ReportTable', () => {
    let wrapper

    beforeEach(() => {
        wrapper = initWrapper(ReportTable, {
            propsData: {
                data: REPORT_TABLE_DATA_PARSED,
                headers: REPORT_TABLE_HEADERS,
                is_loading: true,
            },
        })
    })

    test('matches snapshot', () => {
        expect(wrapper.element).toMatchSnapshot()
    })

    test('empty matches snapshot', () => {
        wrapper = initWrapper(ReportTable, { propsData: { is_loading: true }})

        expect(wrapper.element).toMatchSnapshot()
    })

    test('export emits correctly', async() => {
        wrapper.vm.$refs.export_options_list = {
            reset: () => true,
            blur: () => true,
        }
        await wrapper.setProps({ data: REPORT_TABLE_DATA_FILTERED })

        wrapper.vm.export_data(EXPORT_TYPE.CSV)
        expect(wrapper.emitted('export')[0][0].data).toBe(REPORT_TABLE_DATA_FILTERED)
    })
})
