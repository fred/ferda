import { REPORT, REPORT_PROPERTIES } from 'ferda-test/TestObjects'
import { initWrapper } from 'ferda-test/TestUtils'
import ReportForm from 'ferda/reports/ReportForm.vue'

describe('ReportForm', () => {
    let wrapper

    const options = { propsData: { report: REPORT } }

    beforeEach(() => {
        wrapper = initWrapper(ReportForm, options)
        wrapper.vm.$refs.form.validate = jest.fn(() => true)
    })

    test('matches snapshot', () => {
        expect(wrapper.element).toMatchSnapshot()
    })

    test('form submit emits correct data', async() => {
        await wrapper.setData({ properties: REPORT_PROPERTIES })
        wrapper.vm.run_report()
        expect(wrapper.emitted('run-report')[0]).toEqual([REPORT_PROPERTIES])
        expect(wrapper.vm.$refs.form.validate).toHaveBeenCalled()
    })

    test('form validation error', () => {
        wrapper.vm.$refs.form.validate = jest.fn(() => false)

        wrapper.vm.run_report()
        expect(wrapper.emitted('run-report')).toBeFalsy()
        expect(wrapper.vm.$refs.form.validate).toHaveBeenCalled()
    })

    test('url params contains boolean', () => {
        const url = '?exdate=2021-01-01&handle=handle.cz&is_checked=true'

        history.pushState(null, null, url)

        wrapper = initWrapper(ReportForm, options)
        wrapper.vm.$refs.form.validate = jest.fn(() => true)

        expect(wrapper.vm.properties).toEqual({
            'exdate': '2021-01-01',
            'handle': 'handle.cz',
            'is_checked': true,
            'uuid': null,
        })
    })

    afterEach(() => {
        history.pushState(null, null, location.pathname)
    })
})
