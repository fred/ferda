import flushPromises from 'flush-promises'
import { TextDecoder, TextEncoder } from 'util'

import { REPORT, REPORT_PROPERTIES, REPORT_TABLE_HEADERS } from 'ferda-test/TestObjects'
import { getClonedStoreOptions, initWrapper } from 'ferda-test/TestUtils'
import { EXPORT_TYPE } from 'ferda/constants'
import ReportDetail from 'ferda/reports/ReportDetail.vue'
import { sleep } from 'ferda/utils'

describe('ReportDetail', () => {
    let wrapper
    const initial_decoder = global.TextDecoder
    const initial_encoder = global.TextEncoder
    const initial_fetch = global.fetch
    const expected_clear_notification = { clean: true }

    beforeEach(() => {
        wrapper = initWrapper(ReportDetail, {
            propsData: { report: REPORT },
            customStoreOptions: getClonedStoreOptions(),
            mocks: { '$cookies': { get: () => 'csrftoken' } },
        })

        wrapper.vm.$notify = jest.fn()
    })

    afterEach(() => {
        global.TextDecoder = initial_decoder
        global.TextEncoder = initial_encoder
        global.fetch = initial_fetch
    })

    test('matches snapshot', () => {
        expect(wrapper.element).toMatchSnapshot()
    })

    test('method run_report generates rows correctly', async() => {
        const encoded_data = new Uint8Array([
            123, 34, 116, 101, 115, 116, 95, 107, 101, 121, 34, 58, 32, 34,
            116, 101, 115, 116, 95, 118, 97, 108, 117, 101, 34, 125, 10,
        ])
        // We have to polyfill TextDecoder globally as this is not included in jsdom
        global.TextDecoder = TextDecoder
        global.TextEncoder = TextEncoder
        global.fetch = jest.fn(() =>
            Promise.resolve({ ok: true, body: { getReader: () =>
                ({ read: () => ({ done: true, value: encoded_data })})}}),
        )
        await wrapper.vm.run_report(REPORT_PROPERTIES)
        await sleep(30)
        await flushPromises()
        expect(wrapper.vm.data).toStrictEqual([{ test_key: 'test_value' }])
        expect(wrapper.vm.data_streaming).toStrictEqual(false)
    })

    test('method run_report is aborted', async() => {
        console.log = jest.fn()
        // '__CANCEL__' is the key by which axios recognizes whether the error is abort error or not
        global.fetch = jest.fn(() => Promise.reject({ __CANCEL__: true }))

        wrapper.vm.run_report(REPORT_PROPERTIES)
        wrapper.vm.stop_search()

        await flushPromises()

        expect(wrapper.vm.data).toStrictEqual(null)
        expect(wrapper.vm.$notify).toHaveBeenCalledTimes(2)
        expect(wrapper.vm.$notify).toHaveBeenNthCalledWith(1, expected_clear_notification)
        expect(wrapper.vm.$notify).toHaveBeenNthCalledWith(2, {
            title: 'notifications:report_run_abort',
            text: 'notifications:report_run_aborted',
            type: 'info',
            duration: 7000,
        })
        expect(console.log).toHaveBeenCalledWith('info: notifications:report_run_aborted')
    })

    test('method run_report fails - server error', async() => {
        console.log = jest.fn()
        global.fetch = jest.fn(() => Promise.reject({ name: 'Connection error' }))

        wrapper.vm.run_report(REPORT_PROPERTIES)

        await flushPromises()
        expect(wrapper.vm.data).toStrictEqual(null)
        expect(wrapper.vm.$notify).toHaveBeenCalledTimes(2)
        expect(wrapper.vm.$notify).toHaveBeenNthCalledWith(1, expected_clear_notification)
        expect(wrapper.vm.$notify).toHaveBeenNthCalledWith(2, {
            title: 'notifications:operation_failed',
            text: 'notifications:something_went_wrong',
            type: 'error',
            duration: -1,
        })
        expect(console.log).toHaveBeenCalledWith('error: notifications:something_went_wrong')
    })

    test('method run_report fails - connection failure', async() => {
        console.log = jest.fn()
        global.fetch = jest.fn(() => Promise.resolve({ ok: false }))

        wrapper.vm.run_report(REPORT_PROPERTIES)

        await flushPromises()
        expect(wrapper.vm.data).toStrictEqual(null)
        expect(wrapper.vm.$notify).toHaveBeenCalledWith(wrapper.vm.ERROR_SOMETHING_WENT_WRONG)
        expect(console.log).toHaveBeenCalledWith('error: notifications:something_went_wrong')
    })

    test('report headers are correct', () => {
        expect(JSON.stringify(wrapper.vm.get_report_headers(REPORT))).toEqual(JSON.stringify(REPORT_TABLE_HEADERS))
    })

    test('csv export is called', () => {
        wrapper.vm.export_table_csv = jest.fn()

        const table_data = [
            {uuid: '0001', handle: 'RIMMER', name: 'Arnold Rimmer'},
            {uuid: '0002', handle: 'LISTER', name: 'Dave Lister'},
        ]
        wrapper.vm.create_export({ type: EXPORT_TYPE.CSV, data: table_data })
        expect(wrapper.vm.export_table_csv).toHaveBeenCalledWith(
            table_data,
            {
                'service': 'test_service',
                'name': 'test_report',
                'label': 'Test Report',
                'input_parameters': [
                    {'label': 'UUID', 'name': 'uuid', 'type': 'uuid'},
                    {'label': 'Handle', 'name': 'handle', 'type': 'character varying'},
                    {'label': 'Expiration date', 'name': 'exdate', 'type': 'date'},
                    {'label': 'Is checked', 'name': 'is_checked', 'type': 'boolean'},
                ],
                'output_parameters': [
                    {'label': 'UUID', 'name': 'uuid', 'type': 'uuid'},
                    {'label': 'Handle', 'name': 'handle', 'type': 'character varying'},
                    {'label': 'Expiration date', 'name': 'exdate', 'type': 'date'},
                ],
            },
        )
    })
})
