import { REPORT } from 'ferda-test/TestObjects'
import { initWrapper } from 'ferda-test/TestUtils'
import ReportListFilter from 'ferda/reports/ReportListFilter.vue'

describe('ReportListFilter', () => {
    let wrapper

    beforeEach(() => {
        wrapper = initWrapper(ReportListFilter, {
            propsData: { reports: [REPORT], selected_services: [], report_list_query: '' },
        })
    })

    test('matches snapshot', () => {
        expect(wrapper.element).toMatchSnapshot()
    })
})
