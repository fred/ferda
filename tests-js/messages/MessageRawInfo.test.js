import { EMAIL_ENVELOPE } from 'ferda-test/TestObjects'
import { initWrapper } from 'ferda-test/TestUtils'
import MessageRawInfo from 'ferda/messages/MessageRawInfo.vue'

describe('MessageRawInfo', () => {
    let wrapper

    beforeEach(() => {
        wrapper = initWrapper(MessageRawInfo, { propsData: { envelope: EMAIL_ENVELOPE }})
    })

    test('matches snapshot', () => {
        expect(wrapper.element).toMatchSnapshot()
    })
})
