import { EMAIL_ENVELOPE, EMAIL_ENVELOPE_ATTACHMENTS_UNAVAILABLE } from 'ferda-test/TestObjects'
import { initWrapper } from 'ferda-test/TestUtils'
import EmailMessage from 'ferda/messages/EmailMessage.vue'

describe('EmailMessage', () => {
    let wrapper

    beforeEach(() => {
        wrapper = initWrapper(EmailMessage, {
            propsData: {
                envelope: EMAIL_ENVELOPE,
                matching_label: 'subtype label 1',
            },
        })
    })

    test('matches snapshot with attachments available', () => {
        expect(wrapper.element).toMatchSnapshot()
    })

    test('matches snapshot with attachments unavailable', () => {
        wrapper = initWrapper(EmailMessage, {propsData: {envelope: EMAIL_ENVELOPE_ATTACHMENTS_UNAVAILABLE}})
        expect(wrapper.element).toMatchSnapshot()
    })

    test('matches snapshot with verbose prop', () => {
        wrapper = initWrapper(EmailMessage, { propsData: { envelope: EMAIL_ENVELOPE, is_verbose: true }})
        expect(wrapper.element).toMatchSnapshot()
    })

    test('attachments to download exist', () => {
        const attachments = wrapper.find('a.tref-attachment-link')
        expect(attachments.exists()).toBe(true)
    })

    test('message attachments_not_available exists', () => {
        wrapper = initWrapper(EmailMessage, {propsData: {envelope: EMAIL_ENVELOPE_ATTACHMENTS_UNAVAILABLE}})
        const attachments = wrapper.find('p.tref-attachment-not-available')
        expect(attachments.exists()).toBe(true)
    })

    test('links point to correct attachments', () => {
        const attachmentLinks = wrapper.findAllComponents({ ref: 'attachment_link' })

        for (let i = 0; i < attachmentLinks.length; i++) {
            const containsLink = attachmentLinks.at(i).attributes('href').includes(
                EMAIL_ENVELOPE.message.attachments[i].uuid,
            )

            expect(containsLink).toBe(true)
        }
    })
})
