import axios from 'axios'
import MockAdapter from 'axios-mock-adapter'
import flushPromises from 'flush-promises'

import { EMAIL_ENVELOPE, MESSAGE_PARAMS} from 'ferda-test/TestObjects'
import { initWrapper } from 'ferda-test/TestUtils'
import { MESSAGE_TYPE } from 'ferda/MessagesMixin'
import { URLS } from 'ferda/Urls'
import MessageList from 'ferda/messages/MessageList.vue'

describe('MessageList', () => {
    let axios_mock
    let wrapper
    const initial_console = console.log

    beforeEach(async() => {
        jest.useFakeTimers()
        jest.setSystemTime(new Date('2020-01-01T12:01:00+00:00'))
        axios_mock = new MockAdapter(axios)
        axios_mock.onGet(URLS.message_list(MESSAGE_TYPE.EMAIL)).reply(200, { next: null, results: [EMAIL_ENVELOPE] })
        axios_mock.onGet(URLS.message_subtypes(MESSAGE_TYPE.EMAIL)).reply(200, MESSAGE_PARAMS.message_subtypes)
        wrapper = initWrapper(MessageList, { mocks: { $notify: jest.fn() }})
        wrapper.vm.$refs.recipients.blur = jest.fn()
        wrapper.vm.$refs.form.validate = jest.fn()

        await flushPromises()
    })

    afterEach(() => {
        delete window.location
        window.location = { search: '' }
        console.log = initial_console
        jest.runOnlyPendingTimers()
        jest.useRealTimers()
    })

    test('messages matches snapshot', () => {
        expect(wrapper.element).toMatchSnapshot()
    })

    test('method submit_search fails by server error', async() => {
        console.log = jest.fn()
        axios_mock.onGet(URLS.message_list(MESSAGE_TYPE.EMAIL)).reply(500)
        await wrapper.vm.submit_search()
        expect(wrapper.vm.messages).toStrictEqual(null)
        expect(console.log).toHaveBeenCalledWith('error: notifications:something_went_wrong')
        expect(wrapper.vm.$notify).toHaveBeenCalledWith(wrapper.vm.ERROR_SOMETHING_WENT_WRONG)
    })

    test('method submit_search is aborted', async() => {
        console.log = jest.fn()
        wrapper.vm.submit_search()
        await wrapper.vm.$nextTick()
        await wrapper.vm.stop_search()

        expect(wrapper.vm.messages).toStrictEqual(null)
        expect(wrapper.vm.$notify).toHaveBeenNthCalledWith(1, { clean: true })
        expect(wrapper.vm.$notify).toHaveBeenNthCalledWith(2, wrapper.vm.INFO_REQUEST_ABORTED)
        expect(console.log).toHaveBeenCalledWith('info: notifications:search_aborted')
    })

    test('method submit_search fetches data correctly', async() => {
        await wrapper.setData({ messages: [], active_date_range: 'current_month' })
        await wrapper.vm.submit_search(null, false, true)
        expect(wrapper.vm.message_params).toStrictEqual({
            message_type: MESSAGE_TYPE.EMAIL,
            recipients: [],
            selected_message_type: MESSAGE_TYPE.EMAIL,
            selected_subtypes: [],
            message_subtypes: MESSAGE_PARAMS.message_subtypes,
            time_begin_from: '2020-01-01T00:00:00+00:00',
            time_begin_to: '2020-02-01T00:00:00+00:00',
        })
    })

    test('method submit_search is called with prefilled URL correctly', async() => {
        console.log = jest.fn()
        delete window.location
        window.location = {
            search: '?page_size=5&recipients=rec@example.com&types=subtype_1&types=subtype_2&date_range=current_month' }
        wrapper = initWrapper(MessageList, { mocks: { $notify: jest.fn() } })
        wrapper.vm.$refs.recipients.blur = jest.fn()
        wrapper.vm.$refs.form.validate = jest.fn()
        await flushPromises()
        expect(wrapper.vm.message_params).toStrictEqual({
            message_type: MESSAGE_TYPE.EMAIL,
            recipients: ['rec@example.com'],
            selected_message_type: MESSAGE_TYPE.EMAIL,
            selected_subtypes: MESSAGE_PARAMS.selected_subtypes,
            message_subtypes: MESSAGE_PARAMS.message_subtypes,
            time_begin_from: '2020-01-01T00:00:00+00:00',
            time_begin_to: '2020-02-01T00:00:00+00:00',
        })
        expect(wrapper.vm.active_date_range).toStrictEqual('current_month')
    })

    test('method submit_search does not submit when form is in error state', async() => {
        const original_messages = wrapper.vm.messages
        wrapper.vm.get_message_list = jest.fn()
        await wrapper.setData({ is_valid: false })
        await wrapper.vm.submit_search()
        expect(wrapper.vm.get_message_list).not.toHaveBeenCalled()
        expect(wrapper.vm.messages).toStrictEqual(original_messages)
    })

    test('method get_recipient_caption returns correct value', () => {
        expect(wrapper.vm.get_recipient_caption(MESSAGE_TYPE.LETTER, { name: 'rec_name', organization: 'rec_org' }))
            .toBe('rec_org, rec_name')
        expect(wrapper.vm.get_recipient_caption(MESSAGE_TYPE.EMAIL, 'recipient@example.com'))
            .toBe('recipient@example.com')
    })

    test('method change_page_size changes values correctly', async() => {
        wrapper.vm.submit_search = jest.fn()
        await wrapper.setData({ current_page: 3 })
        expect(wrapper.vm.current_page).toBe(3)
        expect(wrapper.vm.page_size).toBe(wrapper.vm.$store.getters['settings/PAGINATION_PAGE_SIZE'])

        await wrapper.vm.change_page_size(5)
        expect(wrapper.vm.current_page).toBe(1)
        expect(wrapper.vm.page_size).toBe(5)
        expect(wrapper.vm.submit_search).toHaveBeenCalledWith(null, true)
    })

    test('method update_message_subtypes works correctly', async() => {
        const message_subtypes = [
            { label: 'subtype 1', name: 'subtype_1', tags: ['email'] },
            { label: 'subtype 2', name: 'subtype_2', tags: ['email'] },
            { label: 'subtype 3', name: 'subtype_3', tags: ['email'] },
        ]
        expect(wrapper.vm.message_params.message_subtypes).toStrictEqual(MESSAGE_PARAMS.message_subtypes)
        axios_mock.onGet(URLS.message_subtypes(MESSAGE_TYPE.EMAIL)).reply(200, message_subtypes)
        await wrapper.vm.update_message_subtypes(MESSAGE_TYPE.EMAIL, true)
        expect(wrapper.vm.message_params.message_subtypes).toStrictEqual(message_subtypes)
    })

    test('method update_message_subtypes does not update subtypes when user deletes input', async() => {
        wrapper.vm.get_message_subtypes = jest.fn()
        await wrapper.vm.update_message_subtypes(null)
        expect(wrapper.vm.get_message_subtypes).not.toHaveBeenCalled()
    })

    test('method update_message_subtypes fails', async() => {
        console.log = jest.fn()
        expect(wrapper.vm.message_params.message_subtypes).toStrictEqual(MESSAGE_PARAMS.message_subtypes)
        axios_mock.onGet(URLS.message_subtypes(MESSAGE_TYPE.EMAIL)).reply(500)
        await wrapper.vm.update_message_subtypes(MESSAGE_TYPE.EMAIL)
        expect(wrapper.vm.message_params.message_subtypes).toStrictEqual(MESSAGE_PARAMS.message_subtypes)
        expect(console.log).toHaveBeenCalledWith('error: notifications:something_went_wrong')
        expect(wrapper.vm.$notify).toHaveBeenCalledWith(wrapper.vm.ERROR_SOMETHING_WENT_WRONG)
    })

    test('method change current page changes data correctly', async() => {
        expect(wrapper.vm.current_page).toBe(1)
        wrapper.vm.change_current_page(2)
        expect(wrapper.vm.current_page).toBe(2)
    })

    test('validation is triggered - time_begin_from', async() => {
        await wrapper.setData({ message_params: { time_begin_from: '2021-01-31T12:00:00+01:00' }})
        await wrapper.vm.$nextTick()
        expect(wrapper.vm.$refs.form.validate).toHaveBeenCalledTimes(3)
    })

    test('validation is triggered - time_begin_to', async() => {
        await wrapper.setData({ message_params: { time_begin_to: '2021-02-31T12:00:00+01:00' }})
        await wrapper.vm.$nextTick()
        expect(wrapper.vm.$refs.form.validate).toHaveBeenCalledTimes(3)
    })
})
