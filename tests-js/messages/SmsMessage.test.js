import flushPromises from 'flush-promises'

import { SMS_ENVELOPE } from 'ferda-test/TestObjects'
import { initWrapper } from 'ferda-test/TestUtils'
import SmsMessage from 'ferda/messages/SmsMessage.vue'

describe('SmsMessage', () => {
    let wrapper

    beforeEach(async() => {
        wrapper = initWrapper(SmsMessage, {
            propsData: {
                envelope: SMS_ENVELOPE,
                matching_label: 'subtype label 1',
            },
        })
        await flushPromises()
    })

    test('Matches snapshot', () => {
        expect(wrapper.element).toMatchSnapshot()
    })

    test('matches snapshot with verbose prop', () => {
        wrapper = initWrapper(SmsMessage, { propsData: { envelope: SMS_ENVELOPE, is_verbose: true }})
        expect(wrapper.element).toMatchSnapshot()
    })
})
