import flushPromises from 'flush-promises'

import { LETTER_ENVELOPE } from 'ferda-test/TestObjects'
import { initWrapper } from 'ferda-test/TestUtils'
import LetterMessage from 'ferda/messages/LetterMessage.vue'

describe('LetterMessage', () => {
    let wrapper

    beforeEach(async() => {
        wrapper = initWrapper(LetterMessage, {
            propsData: {
                envelope: LETTER_ENVELOPE,
                matching_label: 'subtype label 1',
            },
        })
        await flushPromises()
    })

    test('Matches snapshot', () => {
        expect(wrapper.element).toMatchSnapshot()
    })

    test('matches snapshot with verbose prop', () => {
        wrapper = initWrapper(LetterMessage, { propsData: { envelope: LETTER_ENVELOPE, is_verbose: true }})
        expect(wrapper.element).toMatchSnapshot()
    })
})
