import axios from 'axios'
import MockAdapter from 'axios-mock-adapter'
import flushPromises from 'flush-promises'

import { LETTER_ENVELOPE } from 'ferda-test/TestObjects'
import { initWrapper } from 'ferda-test/TestUtils'
import { MESSAGE_TYPE } from 'ferda/MessagesMixin'
import { URLS } from 'ferda/Urls'
import MessageDetail from 'ferda/messages/MessageDetail.vue'

describe('MessageDetail', () => {
    let axios_mock
    let wrapper
    const initial_log = console.log

    beforeEach(async() => {
        axios_mock = new MockAdapter(axios)
        axios_mock.onGet(URLS.message_detail_ajax(encodeURIComponent(LETTER_ENVELOPE.uid), MESSAGE_TYPE.EMAIL))
            .reply(200, LETTER_ENVELOPE)
        axios_mock.onGet(URLS.message_subtypes(MESSAGE_TYPE.EMAIL))
            .reply(200, [{ name: 'test_subtype', label: 'Test label', tags: ['test']}])

        wrapper = initWrapper(MessageDetail, {
            propsData: { message_id: LETTER_ENVELOPE.uid, message_type: MESSAGE_TYPE.EMAIL },
            mocks: { $notify: jest.fn() },
        })
        await flushPromises()
    })

    afterEach(() => {
        console.log = initial_log
    })

    test('matches snapshot', () => {
        expect(wrapper.element).toMatchSnapshot()
    })

    test('component loads data correctly after mount', () => {
        expect(wrapper.vm.message_detail).toStrictEqual(LETTER_ENVELOPE)
    })

    test('method get_message_detail fails by server error', async() => {
        console.log = jest.fn()
        axios_mock.onGet(URLS.message_detail_ajax(encodeURIComponent(LETTER_ENVELOPE.uid), MESSAGE_TYPE.EMAIL))
            .reply(500)
        wrapper = initWrapper(MessageDetail, {
            propsData: { message_id: LETTER_ENVELOPE.uid, message_type: MESSAGE_TYPE.EMAIL },
            mocks: { $notify: jest.fn() },
        })
        await flushPromises()
        expect(wrapper.vm.message_detail).toStrictEqual(null)
        expect(wrapper.vm.$notify).toHaveBeenCalledWith(wrapper.vm.ERROR_SOMETHING_WENT_WRONG)
        expect(console.log).toHaveBeenCalledWith('error: notifications:something_went_wrong')
    })

    test('method get_message_detail is aborted by user', async() => {
        console.log = jest.fn()
        axios_mock.onGet(URLS.message_detail_ajax(encodeURIComponent(LETTER_ENVELOPE.uid), MESSAGE_TYPE.EMAIL))
            .reply(200, LETTER_ENVELOPE)
        wrapper = initWrapper(MessageDetail, {
            propsData: { message_id: LETTER_ENVELOPE.uid, message_type: MESSAGE_TYPE.EMAIL },
            mocks: { $notify: jest.fn() },
        })
        wrapper.vm.stop_loading()
        await flushPromises()
        expect(wrapper.vm.message_detail).toStrictEqual(null)
        expect(wrapper.vm.$notify).toHaveBeenCalledWith(wrapper.vm.INFO_REQUEST_ABORTED)
        expect(console.log).toHaveBeenCalledWith('info: notifications:search_aborted')
    })

    test('property has_extra_content returns correct value - subject_template', async() => {
        await wrapper.setData({ message_detail: {
            ...LETTER_ENVELOPE,
            message: { ...LETTER_ENVELOPE.message, subject_template: 'some_template.txt' },
        }})

        expect(wrapper.vm.has_extra_content).toStrictEqual(true)
    })

    test('property has_extra_content returns correct value - body_template', async() => {
        await wrapper.setData({ message_detail: {
            ...LETTER_ENVELOPE,
            message: { ...LETTER_ENVELOPE.message, body_template: 'some_template.txt' },
        }})

        expect(wrapper.vm.has_extra_content).toStrictEqual(true)
    })

    test('property has_extra_content returns correct value - body_template_html', async() => {
        await wrapper.setData({ message_detail: {
            ...LETTER_ENVELOPE,
            message: { ...LETTER_ENVELOPE.message, body_template_html: 'some_template.txt' },
        }})

        expect(wrapper.vm.has_extra_content).toStrictEqual(true)
    })

    test('property has_extra_content returns correct value - context', async() => {
        await wrapper.setData({ message_detail: {
            ...LETTER_ENVELOPE,
            message: { ...LETTER_ENVELOPE.message, context: { test: 'test context' }},
        }})

        expect(wrapper.vm.has_extra_content).toStrictEqual(true)
    })

    test('property has_extra_content returns correct value - extra_headers', async() => {
        await wrapper.setData({ message_detail: {
            ...LETTER_ENVELOPE,
            message: { ...LETTER_ENVELOPE.message, extra_headers: { header: 'test header' }},
        }})

        expect(wrapper.vm.has_extra_content).toStrictEqual(true)
    })
})
