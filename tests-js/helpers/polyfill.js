import 'blob-polyfill'

import arrayAtInserter from 'array.prototype.at'
import stringAtInserter from 'string.prototype.at'
import replaceAllInserter from 'string.prototype.replaceall'

replaceAllInserter.shim()
stringAtInserter.shim()
arrayAtInserter.shim()
