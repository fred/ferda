const { TestEnvironment } = require('jest-environment-jsdom')
module.exports = class TimezoneAwareJSDOMEnvironment extends TestEnvironment {
    constructor(config, context) {
        process.env.TZ = 'UTC'
        super(config, context)
    }
}
