import { MESSAGE_TYPE } from 'ferda/MessagesMixin'
import { RuleMixin } from 'ferda/RuleMixin'

import { LOG_PARAMS } from './TestObjects'
import { initWrapper } from './TestUtils'

describe('RuleMixin', () => {
    let wrapper

    beforeEach(() => {
        jest.useFakeTimers()
        jest.setSystemTime(new Date('2020-01-01T12:00:00+00:00'))
        const Component = { render() {}, mixins: [RuleMixin] }
        wrapper = initWrapper(Component, { mocks: { $notify: jest.fn() }})
    })

    afterEach(() => {
        jest.runOnlyPendingTimers()
        jest.useRealTimers()
    })

    test('method required_rule validates correctly', () => {
        expect(wrapper.vm.required_rule()[0](MESSAGE_TYPE.EMAIL)).toStrictEqual(true)
        expect(wrapper.vm.required_rule()[0]()).toBe('rule:field_required')
        expect(wrapper.vm.required_rule()[0]([])).toBe('rule:field_required')
    })

    test('computed date less_than_rule works correctly', () => {
        expect(wrapper.vm.less_than_rule('2022-05-01T00:00:00Z', null)).toStrictEqual([true])
        expect(wrapper.vm.less_than_rule(null, '2022-05-09T00:00:00Z')).toStrictEqual([true])
        expect(wrapper.vm.less_than_rule('2022-05-01T00:00:00Z', '2022-05-09T00:00:00Z')[0](
            '2022-05-01T00:00:00Z', '2022-05-09T00:00:00Z')).toStrictEqual(true)
        expect(wrapper.vm.less_than_rule()[0]('2022-05-01T00:00:00Z', '2022-04-01T00:00:00Z'))
            .toStrictEqual('rule:invalid_range')
    })

    test('method has_proper_range_rule validates correctly', () => {
        expect(wrapper.vm.has_proper_range_rule(LOG_PARAMS.time_begin_to, LOG_PARAMS.time_begin_from)[0](
            LOG_PARAMS.time_begin_to, LOG_PARAMS.time_begin_from)).toBe('rule:invalid_range')
        expect(wrapper.vm.has_proper_range_rule('2022-05-01T00:00:00Z', '2022-05-09T00:00:00Z')[0](
            '2022-05-01T00:00:00Z', '2022-05-09T00:00:00Z')).toStrictEqual(true)
    })

    test('method both_values_required_rule validates correctly', () => {
        expect(wrapper.vm.both_values_required_rule(null, null)).toStrictEqual([true])
        expect(wrapper.vm.both_values_required_rule(
            LOG_PARAMS.object_reference_type, LOG_PARAMS.object_reference_value)).toStrictEqual([true])
        expect(wrapper.vm.both_values_required_rule(null, LOG_PARAMS.object_reference_value)[0]('some_val'))
            .toStrictEqual(true)
        expect(wrapper.vm.both_values_required_rule(LOG_PARAMS.object_reference_type, null)[0]())
            .toStrictEqual('rule:field_required')
    })

    test('method email_rule validates correctly', () => {
        expect(wrapper.vm.email_rule(['some invalid input'])).toStrictEqual(['rule:invalid_email'])
        expect(wrapper.vm.email_rule(['johndoe@email.com', 'some invalid input'])).toStrictEqual(['rule:invalid_email'])
        expect(wrapper.vm.email_rule(['johndoe@email.com'])).toStrictEqual([true])
        expect(wrapper.vm.email_rule(['johndoe@email.com', 'janedoe@email.com'])).toStrictEqual([true])
        expect(wrapper.vm.email_rule('something invalid', false)).toStrictEqual([false])
        expect(wrapper.vm.email_rule('johndoe@email.com', false)).toStrictEqual([true])
        expect(wrapper.vm.email_rule('johndoe@email.info', false)).toStrictEqual([true])
        expect(wrapper.vm.email_rule('johndoe@email.dental', false)).toStrictEqual([true])
        expect(wrapper.vm.email_rule('john.doe@some-number.6.9.2.3.0.6.0.2.4.e164.arpa', false)).toStrictEqual([true])
        expect(wrapper.vm.email_rule('johndoe@email.d-en-tal', false)).toStrictEqual([true])
        expect(wrapper.vm.email_rule('johndoe@email.x-y', false)).toStrictEqual([true])
        expect(wrapper.vm.email_rule('johndoe@email.x', false)).toStrictEqual([false])
        expect(wrapper.vm.email_rule('johndoe@email.-x', false)).toStrictEqual([false])
        expect(wrapper.vm.email_rule('johndoe@email.x-', false)).toStrictEqual([false])
        expect(wrapper.vm.email_rule('john++doe@email.com', false)).toStrictEqual([true])
        expect(wrapper.vm.email_rule('+johndoe+@email.com', false)).toStrictEqual([true])
        expect(wrapper.vm.email_rule('+@email.com', false)).toStrictEqual([true])
        expect(wrapper.vm.email_rule('johndoe@e+mail.com', false)).toStrictEqual([false])
    })

    test('method phone_rule validates correctly', () => {
        expect(wrapper.vm.phone_rule(['some invalid input'])).toStrictEqual(['rule:invalid_phone'])
        expect(wrapper.vm.phone_rule(['+123.111111111', 'some invalid input'])).toStrictEqual(['rule:invalid_phone'])
        expect(wrapper.vm.phone_rule(['+12.123456749'])).toStrictEqual([true])
        expect(wrapper.vm.phone_rule(['+531.123456789', '+55.231564987'])).toStrictEqual([true])
        expect(wrapper.vm.phone_rule('something invalid', false)).toStrictEqual([false])
        expect(wrapper.vm.phone_rule('+420.123456789', false)).toStrictEqual([true])
    })

    test('method range_rule validates correctly', () => {
        expect(wrapper.vm.range_rule('2022-05-02T00:00:00Z', '2022-05-09T00:00:00Z')[0](
            '2022-05-02T00:00:00Z', '2022-05-09T00:00:00Z')).toStrictEqual(true)
        expect(wrapper.vm.range_rule('2022-05-02T00:00:00Z', '2023-05-01T00:00:00Z')[0](
            '2022-05-02T00:00:00Z', '2023-05-01T00:00:00Z')).toStrictEqual('rule:range_inzone')
    })

    test('method future_date_rule works correctly', () => {
        expect(wrapper.vm.future_date_rule('2019-05-02T00:00:00Z')[0]('2019-05-02T00:00:00Z'))
            .toStrictEqual('rule:future_date')
        expect(wrapper.vm.future_date_rule('2022-06-08T00:00:00Z')[0]('2022-06-08T00:00:00Z')).toStrictEqual(true)
    })

    test('method duplicite_rule works correctly', () => {
        expect(wrapper.vm.duplicite_rule(['TEST-REG'], 'TEST-REG')).toStrictEqual(['rule:duplicite_value'])
        expect(wrapper.vm.duplicite_rule(['TEST-REG'], 'new_val')).toStrictEqual([true])
    })

    test('method max_length_rule works correctly', () => {
        expect(wrapper.vm.max_length_rule('test', 3)).toStrictEqual(['rule:too_long'])
        expect(wrapper.vm.max_length_rule('test', 4)).toStrictEqual([true])
        expect(wrapper.vm.max_length_rule('test', 5)).toStrictEqual([true])
    })

    test('method min_length_rule works correctly', () => {
        expect(wrapper.vm.min_length_rule('test', 5)).toStrictEqual(['rule:too_short'])
        expect(wrapper.vm.min_length_rule('test', 4)).toStrictEqual([true])
        expect(wrapper.vm.min_length_rule('test', 2)).toStrictEqual([true])
    })

    test('method regex_pattern_rule works correctly', () => {
        // Tests that whole input is capitalized
        expect(wrapper.vm.regex_pattern_rule('value', '^[A-Z]*$')).toStrictEqual(['rule:invalid_pattern'])
        expect(wrapper.vm.regex_pattern_rule('Value', '^[A-Z]*$')).toStrictEqual(['rule:invalid_pattern'])
        expect(wrapper.vm.regex_pattern_rule('VALUE', '^[A-Z]*$')).toStrictEqual([true])
    })

    test('method url_rule works correctly', () => {
        expect(wrapper.vm.url_rule('www.test.org')).toStrictEqual(['rule:invalid_url'])
        expect(wrapper.vm.url_rule('test.org')).toStrictEqual(['rule:invalid_url'])
        expect(wrapper.vm.url_rule('http://www.test.org')).toStrictEqual([true])
        expect(wrapper.vm.url_rule('http://test.org')).toStrictEqual([true])
        expect(wrapper.vm.url_rule('https://www.test.org')).toStrictEqual([true])
        expect(wrapper.vm.url_rule('https://test.org')).toStrictEqual([true])
    })

    test('method valid_iso_rule returns correct values', () => {
        expect(wrapper.vm.valid_iso_rule('something invalid')).toStrictEqual(['common:iso_format_expected'])
        expect(wrapper.vm.valid_iso_rule('2021-01-01')).toStrictEqual([true])
        expect(wrapper.vm.valid_iso_rule('2021-01-01T12:00:00')).toStrictEqual([true])
        expect(wrapper.vm.valid_iso_rule('2021-01-01T12:00:00+01:00')).toStrictEqual([true])
    })

    test('method number_range_rule returns correct values', () => {
        expect(wrapper.vm.number_range_rule('-59', 0, 100)).toStrictEqual(['rule:scope_value'])
        expect(wrapper.vm.number_range_rule('150', 0, 100)).toStrictEqual(['rule:scope_value'])
        expect(wrapper.vm.number_range_rule('59', 0, 100)).toStrictEqual([true])
    })

    test('method min_contact_search_length_rule works correctly', () => {
        expect(wrapper.vm.min_contact_search_length_rule([], ['name'])).toStrictEqual([true])
        expect(wrapper.vm.min_contact_search_length_rule(['Jo'], ['contact_handle'])).toStrictEqual([true])
        expect(wrapper.vm.min_contact_search_length_rule(['John Doe'], ['name'])).toStrictEqual([true])
        expect(wrapper.vm.min_contact_search_length_rule(['Jo'], ['name'])).toStrictEqual(['rule:min_search_length'])
        expect(wrapper.vm.min_contact_search_length_rule(['Jo', 'John Doe'], ['name']))
            .toStrictEqual(['rule:min_search_length'])
    })

    test('method max_file_size_rule works correctly', () => {
        expect(wrapper.vm.max_file_size_rule(2000)[0]({ size: 10 })).toStrictEqual(true)
        expect(wrapper.vm.max_file_size_rule(2000)[0]({ size: 2000 })).toStrictEqual(true)
        expect(wrapper.vm.max_file_size_rule(2000)[0]({ size: 2001 })).toStrictEqual('rule:file_too_large')
    })
})
