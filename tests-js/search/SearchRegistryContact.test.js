import { CONTACT_SEARCH_RESULT_WITH_HISTORY, REGISTRAR_LIST } from 'ferda-test/TestObjects'
import { initWrapper } from 'ferda-test/TestUtils'
import SearchRegistryContact from 'ferda/search/SearchRegistryContact.vue'

describe('SearchRegistryContact', () => {
    let wrapper

    beforeEach(() => {
        wrapper = initWrapper(SearchRegistryContact, {
            propsData: {
                query_values:  [],
                search_results: [],
                registrars: REGISTRAR_LIST,
            },
        }, false)
        wrapper.vm.$style.matchingItem = 'matchingItem'
    })

    test('matches snapshot', () => {
        expect(wrapper.element).toMatchSnapshot()
    })

    test('empty matches snapshot', () => {
        wrapper = initWrapper(SearchRegistryContact, {
            propsData: {
                search_results: [],
                registrars: REGISTRAR_LIST,
            },
        }, false)
        expect(wrapper.element).toMatchSnapshot()
    })

    test('method contact_history_matches returns correct results', () => {
        expect(wrapper.vm.contact_history_matches(CONTACT_SEARCH_RESULT_WITH_HISTORY)).toStrictEqual([
            {
                item_name: 'email',
                valid_from: CONTACT_SEARCH_RESULT_WITH_HISTORY.histories[0].valid_from,
                valid_to: CONTACT_SEARCH_RESULT_WITH_HISTORY.histories[0].valid_to,
                value: CONTACT_SEARCH_RESULT_WITH_HISTORY.histories[0].contact.emails.join(', '),
            },
            {
                item_name: 'notify_email',
                valid_from: CONTACT_SEARCH_RESULT_WITH_HISTORY.histories[0].valid_from,
                valid_to: CONTACT_SEARCH_RESULT_WITH_HISTORY.histories[0].valid_to,
                value: CONTACT_SEARCH_RESULT_WITH_HISTORY.histories[0].contact.notify_emails.join(', '),
            },
        ])
    })

    test('method get_match works correctly', () => {
        expect(wrapper.vm.get_match('place', CONTACT_SEARCH_RESULT_WITH_HISTORY.histories[0], 'place.street'))
            .toStrictEqual({
                item_name: 'place',
                valid_from: CONTACT_SEARCH_RESULT_WITH_HISTORY.histories[0].valid_from,
                valid_to: CONTACT_SEARCH_RESULT_WITH_HISTORY.histories[0].valid_to,
                value: [],
            })

        expect(wrapper.vm.get_match(
            'additional_identifier',
            CONTACT_SEARCH_RESULT_WITH_HISTORY.histories[0], 'additional_identifier.birthdate'),
        ).toStrictEqual({
            item_name: 'additional_identifier.birthdate',
            valid_from: CONTACT_SEARCH_RESULT_WITH_HISTORY.histories[0].valid_from,
            valid_to: CONTACT_SEARCH_RESULT_WITH_HISTORY.histories[0].valid_to,
            value: CONTACT_SEARCH_RESULT_WITH_HISTORY.histories[0].contact.additional_identifier.value,
        })

        expect(wrapper.vm.get_match(
            'notify_email', CONTACT_SEARCH_RESULT_WITH_HISTORY.histories[0], 'notify_email',
        )).toStrictEqual({
            item_name: 'notify_email',
            valid_from: CONTACT_SEARCH_RESULT_WITH_HISTORY.histories[0].valid_from,
            valid_to: CONTACT_SEARCH_RESULT_WITH_HISTORY.histories[0].valid_to,
            value: CONTACT_SEARCH_RESULT_WITH_HISTORY.histories[0].contact.notify_emails.join(', '),
        })

        expect(wrapper.vm.get_match(
            'email', CONTACT_SEARCH_RESULT_WITH_HISTORY.histories[0], 'email',
        )).toStrictEqual({
            item_name: 'email',
            valid_from: CONTACT_SEARCH_RESULT_WITH_HISTORY.histories[0].valid_from,
            valid_to: CONTACT_SEARCH_RESULT_WITH_HISTORY.histories[0].valid_to,
            value: CONTACT_SEARCH_RESULT_WITH_HISTORY.histories[0].contact.emails.join(', '),
        })

        expect(wrapper.vm.get_match(
            'name', CONTACT_SEARCH_RESULT_WITH_HISTORY.histories[0], 'name',
        )).toStrictEqual({
            item_name: 'name',
            valid_from: CONTACT_SEARCH_RESULT_WITH_HISTORY.histories[0].valid_from,
            valid_to: CONTACT_SEARCH_RESULT_WITH_HISTORY.histories[0].valid_to,
            value: CONTACT_SEARCH_RESULT_WITH_HISTORY.histories[0].contact.name,
        })
    })
})
