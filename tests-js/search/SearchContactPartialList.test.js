import { initWrapper } from 'ferda-test/TestUtils'
import SearchContactPartialList from 'ferda/search/SearchContactPartialList.vue'

describe('SearchContactPartialList', () => {
    let wrapper

    beforeEach(() => {
        wrapper = initWrapper(SearchContactPartialList, {
            propsData: {
                history_matches_getter: () => [],
            },
        }, false)
    })

    test('matches snapshot', () => {
        expect(wrapper.element).toMatchSnapshot()
    })

    test('method get_history_value works correctly', () => {
        expect(wrapper.vm.get_history_value({ value: [{ is_matching: true, value: 'test' }] })).toBe('test')
        expect(wrapper.vm.get_history_value({ value: 'test' })).toBe('test')
    })
})
