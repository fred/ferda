import { NSSET_SEARCH_RESULT_WITH_HISTORY, REGISTRAR_LIST } from 'ferda-test/TestObjects'
import { initWrapper } from 'ferda-test/TestUtils'
import SearchRegistryNsset from 'ferda/search/SearchRegistryNsset.vue'

describe('SearchRegistryNsset', () => {
    let wrapper

    beforeEach(() => {
        wrapper = initWrapper(SearchRegistryNsset, {
            propsData: {
                query_values:  [],
                search_results: [],
                registrars: REGISTRAR_LIST,
            },
        }, false)
    })

    test('empty matches snapshot', () => {
        wrapper = initWrapper(SearchRegistryNsset, {
            propsData: {
                search_results: [],
                registrars: REGISTRAR_LIST,
            },
        }, false)

        expect(wrapper.element).toMatchSnapshot()
    })

    test('matches snapshot', () => {
        expect(wrapper.element).toMatchSnapshot()
    })

    test('method nsset_history_matches returns correct results', () => {
        expect(wrapper.vm.nsset_history_matches(NSSET_SEARCH_RESULT_WITH_HISTORY)).toStrictEqual([
            {
                item_name: 'dns_hosts.fqdn',
                valid_from: NSSET_SEARCH_RESULT_WITH_HISTORY.histories[0].valid_from,
                valid_to: NSSET_SEARCH_RESULT_WITH_HISTORY.histories[0].valid_to,
                value: NSSET_SEARCH_RESULT_WITH_HISTORY.histories[0].nsset.dns_hosts[0],
            },
            {
                item_name: 'dns_hosts.fqdn',
                valid_from: NSSET_SEARCH_RESULT_WITH_HISTORY.histories[0].valid_from,
                valid_to: NSSET_SEARCH_RESULT_WITH_HISTORY.histories[0].valid_to,
                value: NSSET_SEARCH_RESULT_WITH_HISTORY.histories[0].nsset.dns_hosts[1],
            },
        ])
    })
})
