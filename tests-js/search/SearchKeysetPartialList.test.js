import { initWrapper } from 'ferda-test/TestUtils'
import SearchKeysetPartialList from 'ferda/search/SearchKeysetPartialList.vue'

describe('SearchKeysetPartialList', () => {
    let wrapper

    beforeEach(() => {
        wrapper = initWrapper(SearchKeysetPartialList, {
            propsData: {
                history_matches_getter: () => [],
            },
        }, false)
    })

    test('matches snapshot', () => {
        expect(wrapper.element).toMatchSnapshot()
    })
})
