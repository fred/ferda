import { REGISTRAR_LIST } from 'ferda-test/TestObjects'
import { initWrapper } from 'ferda-test/TestUtils'
import SearchRegistryDomain from 'ferda/search/SearchRegistryDomain.vue'

describe('SearchRegistryDomain', () => {
    let wrapper

    beforeEach(() => {
        wrapper = initWrapper(SearchRegistryDomain, {
            propsData: {
                query_values: [],
                search_results: [],
                registrars: REGISTRAR_LIST,
            },
        }, false)
    })

    test('matches snapshot', () => {
        expect(wrapper.element).toMatchSnapshot()
    })

    test('empty matches snapshot', () => {
        wrapper = initWrapper(SearchRegistryDomain, {
            propsData: {
                search_results: [],
                registrars: REGISTRAR_LIST,
            },
        }, false)

        expect(wrapper.element).toMatchSnapshot()
    })
})
