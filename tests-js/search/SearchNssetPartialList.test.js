import { initWrapper } from 'ferda-test/TestUtils'
import SearchNssetPartialList from 'ferda/search/SearchNssetPartialList.vue'

describe('SearchNssetPartialList', () => {
    let wrapper

    beforeEach(() => {
        wrapper = initWrapper(SearchNssetPartialList, {
            propsData: {
                history_matches_getter: () => [],
            },
        }, false)
    })

    test('matches snapshot', () => {
        expect(wrapper.element).toMatchSnapshot()
    })

    test('method get_history_value works correctly', () => {
        expect(wrapper.vm.get_history_value({ item_name: 'dns_hosts.fqdn', value: { fqdn: 'fqdn' } })).toBe('fqdn')
        expect(wrapper.vm.get_history_value({ item_name: 'test', value: 'test' })).toBe('test')
    })
})
