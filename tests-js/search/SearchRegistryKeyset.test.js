import { KEYSET_SEARCH_RESULT_WITH_HISTORY, REGISTRAR_LIST } from 'ferda-test/TestObjects'
import { initWrapper } from 'ferda-test/TestUtils'
import SearchRegistryKeyset from 'ferda/search/SearchRegistryKeyset.vue'

describe('SearchRegistryKeyset', () => {
    let wrapper

    beforeEach(() => {
        wrapper = initWrapper(SearchRegistryKeyset, {
            propsData: {
                query_values:  [],
                search_results: [],
                registrars: REGISTRAR_LIST,
            },
        }, false)
    })

    test('matches snapshot', () => {
        expect(wrapper.element).toMatchSnapshot()
    })

    test('method keyset_history_matches returns correct results', () => {
        expect(wrapper.vm.keyset_history_matches(KEYSET_SEARCH_RESULT_WITH_HISTORY)).toStrictEqual([])
    })
})
