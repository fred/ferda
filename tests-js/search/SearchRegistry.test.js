import axios from 'axios'
import MockAdapter from 'axios-mock-adapter'
import flushPromises from 'flush-promises'

import { CONTACT_HARRY, REGISTRAR_LIST } from 'ferda-test/TestObjects'
import { getClonedStoreOptions, initWrapper } from 'ferda-test/TestUtils'
import { SEARCH_HISTORY_PERIOD } from 'ferda/DatetimeMixin'
import { OBJECT_TYPE } from 'ferda/RegistryMixin'
import { URLS } from 'ferda/Urls'
import SearchRegistry from 'ferda/search/SearchRegistry.vue'

describe('SearchRegistry', () => {
    let axios_mock
    let wrapper
    const StoreOptions = getClonedStoreOptions()
    const initial_console = console.log
    const initial_location = window.location

    beforeEach(async() => {
        jest.useFakeTimers()
        jest.setSystemTime(new Date('2020-01-01T12:00:00+00:00'))

        axios_mock = new MockAdapter(axios)
        axios_mock.onGet(
            URLS.registry_search_ajax(
                OBJECT_TYPE.CONTACT, new URLSearchParams('?search_items=name&query=harry&type=contact'),
            ),
        ).reply(200, { results: [] })
        axios_mock.onGet(URLS.registrar_list_ajax()).reply(200, REGISTRAR_LIST)

        wrapper = initWrapper(SearchRegistry, { mocks: { $notify: jest.fn() }, customStoreOptions: StoreOptions })
        await flushPromises()
    })

    afterEach(() => {
        jest.runOnlyPendingTimers()
        jest.useRealTimers()
        jest.clearAllMocks()
        delete window.location
        console.log = initial_console
        window.location = initial_location
    })

    test('matches snapshot', () => {
        expect(wrapper.element).toMatchSnapshot()
    })

    test('method get_search_results works correctly - standard search', async() => {
        console.log = jest.fn()
        axios_mock.onGet(
            URLS.registry_search_ajax(
                OBJECT_TYPE.CONTACT, new URLSearchParams('?search_items=name&query=harry&type=contact'),
            ),
        ).reply(200, {
            results: [{
                contact: {
                    object_id: 1,
                    contact_handle: CONTACT_HARRY.contact_handle,
                    email: CONTACT_HARRY.email,
                    notify_email: CONTACT_HARRY.email,
                },
            }],
        })

        expect(await wrapper.vm.get_search_results(
            ['harry'], SEARCH_HISTORY_PERIOD.NOW, OBJECT_TYPE.CONTACT, ['name']),
        ).toStrictEqual([{
            contact: {
                object_id: 1,
                contact_handle: CONTACT_HARRY.contact_handle,
                email: CONTACT_HARRY.email,
                notify_email: CONTACT_HARRY.email,
            },
        }])
        expect(wrapper.vm.$notify).not.toHaveBeenCalled()
        expect(console.log).not.toHaveBeenCalled()
    })

    test('method get_search_results works correctly - history search', async() => {
        console.log = jest.fn()
        axios_mock.onGet(
            URLS.history_search_ajax(
                OBJECT_TYPE.CONTACT, new URLSearchParams('?search_items=name&query=harry&type=contact'),
            ),
        ).reply(200, { results: [{
            object_id: 1,
            last_history: {
                contact_handle: CONTACT_HARRY.contact_handle,
                email: CONTACT_HARRY.email,
                notify_email: CONTACT_HARRY.email,
            },
        }]})

        expect(await wrapper.vm.get_search_results(
            ['harry'], SEARCH_HISTORY_PERIOD.FULL_HISTORY, OBJECT_TYPE.CONTACT, ['name']),
        ).toStrictEqual([{
            object_id: 1,
            contact: {
                contact_handle: 'HARRY',
                email: 'harry.potter@example.com',
                notify_email: 'harry.potter@example.com',
            },
            last_history: {
                contact_handle: 'HARRY',
                email: 'harry.potter@example.com',
                notify_email: 'harry.potter@example.com',
            },
        }])
        expect(wrapper.vm.$notify).not.toHaveBeenCalled()
        expect(console.log).not.toHaveBeenCalled()
    })

    test('method get_search_results fails - registrar list fail', async() => {
        console.log = jest.fn()
        axios_mock.onGet(URLS.registrar_list_ajax()).reply(500)

        expect(await wrapper.vm.get_search_results(['potter'], SEARCH_HISTORY_PERIOD.NOW,
            OBJECT_TYPE.CONTACT, ['name'])).toStrictEqual(null)
        expect(wrapper.vm.$notify).toHaveBeenCalledWith(wrapper.vm.ERROR_SOMETHING_WENT_WRONG)
        expect(console.log).toHaveBeenCalledWith('error: notifications:something_went_wrong')
    })

    test('method get_search_results fails - query fail', async() => {
        console.error = jest.fn()
        console.log = jest.fn()
        const params = new URLSearchParams('?query=potter&type=contact&search_items=name')
        axios_mock.onGet(
            URLS.registry_search_ajax(OBJECT_TYPE.CONTACT, params),
        ).reply(500)
        wrapper.vm.$notify = jest.fn()

        expect(await wrapper.vm.get_search_results(['potter'], SEARCH_HISTORY_PERIOD.NOW,
            OBJECT_TYPE.CONTACT, ['name'])).toStrictEqual(null)
        expect(wrapper.vm.$notify).toHaveBeenCalledTimes(1)
        expect(console.log).toHaveBeenCalledTimes(1)
        expect(console.log.mock.calls[0][0]).toBe('error: notifications:something_went_wrong')
    })

    test('method get_search_results fails - stop_search called', async() => {
        const params = new URLSearchParams('?query=potter&type=contact&search_items=name')
        wrapper.vm.$notify = jest.fn()
        console.log = jest.fn()
        axios_mock.onGet(URLS.registry_search_ajax(OBJECT_TYPE.CONTACT, params)).reply(500, { __CANCEL__: true })
        await wrapper.setData({ registrar_list: REGISTRAR_LIST })

        wrapper.vm.get_search_results(['potter'], SEARCH_HISTORY_PERIOD.NOW, OBJECT_TYPE.CONTACT, ['name'])
        wrapper.vm.stop_search()
        await flushPromises()

        expect(wrapper.vm.$notify).toHaveBeenCalledTimes(1)
        expect(wrapper.vm.$notify).toHaveBeenCalledWith(wrapper.vm.INFO_REQUEST_ABORTED)
        expect(wrapper.vm.search_results).toStrictEqual([])
    })

    test('notification disappears on start of new search', async() => {
        console.log = jest.fn()
        wrapper.vm.$notify = jest.fn()

        wrapper.vm.$notify({
            title: 'Message',
            text: 'Backend error',
            type: 'error',
            duration: -1,
        })

        await wrapper.vm.$nextTick()
        await wrapper.vm.submit_search({
            object_type: OBJECT_TYPE.CONTACT,
            query_values: ['test'],
            search_date: SEARCH_HISTORY_PERIOD.NOW,
            search_items: [],
        })

        expect(wrapper.find('.notification-wrapper').exists()).toBe(false)
    })

    test('method submit_search fails by server error', async() => {
        jest.spyOn(wrapper.vm, 'get_search_results').mockReturnValue(null)
        jest.spyOn(wrapper.vm.$store, 'commit')
        await wrapper.vm.submit_search({
            object_type: OBJECT_TYPE.CONTACT,
            query_values: ['test'],
            search_date: SEARCH_HISTORY_PERIOD.NOW,
            search_items: [],
        })

        expect(wrapper.vm.search_results).toStrictEqual([])
        expect(wrapper.vm.last_search_object_type).toStrictEqual(null)
        expect(wrapper.vm.$store.commit).not.toHaveBeenCalled()
    })

    test('method submit_search works correctly', async() => {
        jest.spyOn(wrapper.vm, 'get_search_results').mockReturnValue({
            results: [{
                contact: {
                    object_id: 1,
                    contact_handle: CONTACT_HARRY.contact_handle,
                    email: CONTACT_HARRY.email,
                    notify_email: CONTACT_HARRY.email,
                },
            }],
        })
        jest.spyOn(wrapper.vm.$store, 'commit')

        await wrapper.vm.submit_search({
            object_type: OBJECT_TYPE.CONTACT,
            query_values: ['test'],
            search_date: SEARCH_HISTORY_PERIOD.NOW,
            search_items: [],
        })

        expect(wrapper.vm.search_results).toStrictEqual({
            results: [{
                contact: {
                    object_id: 1,
                    contact_handle: CONTACT_HARRY.contact_handle,
                    email: CONTACT_HARRY.email,
                    notify_email: CONTACT_HARRY.email,
                },
            }],
        })
        expect(wrapper.vm.last_search_object_type).toStrictEqual(OBJECT_TYPE.CONTACT)
        expect(wrapper.vm.$store.commit).toHaveBeenCalledWith('search/set_last_query_values', ['test'])
    })

    test('method get_search_url returns correct URLs - now', () => {
        const params = new URLSearchParams(`?query=query&type=${OBJECT_TYPE.DOMAIN}`)
        expect(wrapper.vm.get_search_url(true, ['query'], OBJECT_TYPE.DOMAIN, [], 'now'))
            .toBe(URLS.registry_search_ajax(OBJECT_TYPE.DOMAIN, params))
    })

    test('method get_search_url returns correct URLs - in the past', () => {
        const params = new URLSearchParams()
        params.append('query', 'query')
        params.append('valid_from', '2019-12-01T12:00:00Z')
        params.append('type', OBJECT_TYPE.DOMAIN)

        expect(wrapper.vm.get_search_url(false, ['query'], OBJECT_TYPE.DOMAIN, [], 'last_month'))
            .toBe(URLS.history_search_ajax(OBJECT_TYPE.DOMAIN, params))
    })
})
