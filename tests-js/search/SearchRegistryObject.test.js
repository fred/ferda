import axios from 'axios'
import MockAdapter from 'axios-mock-adapter'
import flushPromises from 'flush-promises'

import {
    CONTACT_HARRY,
    CONTACT_REPRESENTATIVE,
    CONTACT_SEARCH_RESULT_NO_HISTORY,
    REGISTRAR_LIST,
    SEARCH_CONTACT,
    SEARCH_DOMAIN,
    SEARCH_KEYSET,
    SEARCH_NSSET,
} from 'ferda-test/TestObjects'
import { getClonedStoreOptions, initWrapper } from 'ferda-test/TestUtils'
import { OBJECT_TYPE } from 'ferda/RegistryMixin'
import { URLS } from 'ferda/Urls'
import SearchRegistryObject from 'ferda/search/SearchRegistryObject.vue'

describe('SearchRegistryObject - empty', () => {
    let wrapper

    beforeEach(() => {
        wrapper = initWrapper(SearchRegistryObject, {
            propsData: {
                object_type: OBJECT_TYPE.CONTACT,
                search_results: [],
                registrars: REGISTRAR_LIST,
                history_matches_getter: () => [],
            },
            slots: {
                'overview-item': 'OVERVIEW-ITEM',
                'other-items': 'OTHER-ITEMS',
                'history-match': 'HISTORY-MATCH',
            },
        })
    })

    test('empty matches snapshot', () => {
        wrapper = initWrapper(SearchRegistryObject, {
            propsData: {
                search_results: [],
                registrars: REGISTRAR_LIST,
                history_matches_getter: () => [],
            },
        })

        expect(wrapper.element).toMatchSnapshot()
    })

    test('matches snapshot', () => {
        expect(wrapper.element).toMatchSnapshot()
    })
})

describe('SearchRegistryObject - not empty (domain)', () => {
    let wrapper
    let store
    let axios_mock
    const initial_console = console.log

    beforeEach(async() => {
        axios_mock = new MockAdapter(axios)

        store = getClonedStoreOptions()
        store.modules.search.getters.last_query_values = () => ['test.cz']

        axios_mock.onGet(URLS.object_info_ajax(OBJECT_TYPE.CONTACT, SEARCH_DOMAIN.registrant.id))
            .reply(200, CONTACT_HARRY)

        wrapper = initWrapper(SearchRegistryObject, {
            propsData: {
                registrars: REGISTRAR_LIST,
                object_type: OBJECT_TYPE.DOMAIN,
                search_results: [
                    { object_id: SEARCH_DOMAIN.domain_id, matched_items: ['fqdn'], domain: SEARCH_DOMAIN },
                ],
                grouped_search_results: {
                    exact_matches: [
                        { object_id: SEARCH_DOMAIN.domain_id, matched_items: ['fqdn'], domain: SEARCH_DOMAIN },
                    ],
                    partial_matches: [],
                },
                history_matches_getter: () => [],
            },
            slots: {
                'overview-item': 'OVERVIEW-ITEM',
                'other-items': 'OTHER-ITEMS',
                'other-info': 'OTHER-INFO',
                'history-match': 'HISTORY-MATCH',
                'partial-list': 'PARTIAL-LIST',
            },
            customStoreOptions: store,
        })
        wrapper.vm.$notify = jest.fn()
        console.log = jest.fn()
        await flushPromises()
    })

    afterEach(() => {
        console.log = initial_console
    })

    test('method load_domain_owner fails by server error - 500', async() => {
        axios_mock.onGet(URLS.object_info_ajax(OBJECT_TYPE.CONTACT, SEARCH_DOMAIN.registrant.id)).reply(500)

        // Need to reinit wrapper because of watcher trigger on mounted
        wrapper = initWrapper(SearchRegistryObject, {
            propsData: {
                registrars: REGISTRAR_LIST,
                object_type: OBJECT_TYPE.DOMAIN,
                search_results: [
                    { object_id: SEARCH_DOMAIN.domain_id, matched_items: ['fqdn'], domain: SEARCH_DOMAIN },
                ],
                history_matches_getter: () => [],
            },
            slots: {
                'overview-item': 'OVERVIEW-ITEM',
                'other-items': 'OTHER-ITEMS',
                'other-info': 'OTHER-INFO',
                'history-match': 'HISTORY-MATCH',
                'partial-list': 'PARTIAL-LIST',
            },
            customStoreOptions: store,
        })
        wrapper.vm.$notify = jest.fn()
        await flushPromises()
        await wrapper.vm.load_domain_owner()

        expect(wrapper.vm.domain_registrant).toStrictEqual(null)
        expect(wrapper.vm.$notify).toHaveBeenCalledWith(wrapper.vm.ERROR_SOMETHING_WENT_WRONG)
        expect(console.log).toHaveBeenCalledWith('error: notifications:something_went_wrong')
    })

    test('method load_domain_owner fails by server error - 404', async() => {
        axios_mock.onGet(URLS.object_info_ajax(OBJECT_TYPE.CONTACT, SEARCH_DOMAIN.registrant.id)).reply(404)
        await wrapper.vm.load_domain_owner()

        expect(wrapper.vm.domain_registrant).toStrictEqual(null)
        expect(wrapper.vm.domain_registrant_loading).toStrictEqual(false)
        expect(wrapper.vm.$notify).not.toHaveBeenCalled()
        expect(console.log).not.toHaveBeenCalled()
    })

    test('method load_domain_owner works correctly', async() => {
        await wrapper.vm.load_domain_owner()

        expect(wrapper.vm.domain_registrant).toStrictEqual(CONTACT_HARRY)
        expect(wrapper.vm.$notify).not.toHaveBeenCalled()
        expect(console.log).not.toHaveBeenCalled()
    })
})

describe('SearchRegistryObject - not empty (contact)', () => {
    let wrapper
    let store
    let axios_mock
    const initial_console = console.log

    beforeEach(async() => {
        axios_mock = new MockAdapter(axios)

        store = getClonedStoreOptions()
        store.modules.search.getters.last_query_values = () => ['harry.potter@example.com']

        wrapper = initWrapper(SearchRegistryObject, {
            propsData: {
                registrars: REGISTRAR_LIST,
                object_type: OBJECT_TYPE.CONTACT,
                search_results: [],
                history_matches_getter: () => [{item_name: 'test match', valid_from: '2020-03-05T13:00:00Z'}],
            },
            slots: {
                'overview-item': 'OVERVIEW-ITEM',
                'other-items': 'OTHER-ITEMS',
                'other-info': 'OTHER-INFO',
                'history-match': 'HISTORY-MATCH',
                'partial-list': 'PARTIAL-LIST',
            },
            customStoreOptions: store,
        })
        wrapper.vm.$notify = jest.fn()
        console.log = jest.fn()

        await wrapper.setProps({ search_results: [CONTACT_SEARCH_RESULT_NO_HISTORY] })
    })

    afterEach(() => {
        console.log = initial_console
    })

    test('matches snapshot', () => {
        expect(wrapper.element).toMatchSnapshot()
    })

    test('method get_normalized_string returns correct values', () => {
        expect(wrapper.vm.get_normalized_string('Crème Brulée')).toBe('creme brulee')
        expect(wrapper.vm.get_normalized_string('HáčkyČárky')).toBe('hackycarky')
    })

    test('method get_compared_property returns correct values', () => {
        expect(wrapper.vm.get_compared_property('test', { contact: { test: 'test value' } })).toBe('test value')
        expect(wrapper.vm.get_compared_property('additional_identifier.test', {
            contact: { additional_identifier: { type: 'test', value: 'test value' }},
        })).toBe('test value')
    })

    test('method object_matches_exactly returns correct values', () => {
        expect(wrapper.vm.object_matches_exactly(CONTACT_SEARCH_RESULT_NO_HISTORY)).toStrictEqual(true)
    })

    test('property expanded_matches returns correct values', async() => {
        const exact_standard_match = CONTACT_SEARCH_RESULT_NO_HISTORY
        const partial_standard_match = {
            ...CONTACT_SEARCH_RESULT_NO_HISTORY,
            contact: {
                ...CONTACT_SEARCH_RESULT_NO_HISTORY.contact,
                emails: ['change@email.org'],
                notify_emails: ['change@email.org'],
            },
        }

        await wrapper.setProps({ search_results: [exact_standard_match] })
        expect(wrapper.vm.expanded_matches).toStrictEqual([exact_standard_match])

        await wrapper.setProps({ search_results: [partial_standard_match] })
        expect(wrapper.vm.expanded_matches).toStrictEqual([partial_standard_match])
    })

    test('method toggle_show_more works correctly', async() => {
        await wrapper.setData({ page_size: wrapper.vm.grouped_search_results.exact_matches.length })
        wrapper.vm.toggle_show_more()

        expect(wrapper.vm.page_size).toBe(3)

        await wrapper.setData({ page_size: 5 })
        wrapper.vm.toggle_show_more()

        expect(wrapper.vm.page_size).toBe(wrapper.vm.grouped_search_results.exact_matches.length)
    })

    test('property toggle_more_text returns correct values', async() => {
        expect(wrapper.vm.toggle_more_text).toBe('common:show_less')

        await wrapper.setData({
            grouped_search_results: {
                exact_matches: [
                    CONTACT_SEARCH_RESULT_NO_HISTORY,
                    {
                        ...CONTACT_SEARCH_RESULT_NO_HISTORY,
                        object_id: 2,
                        contact: { ...CONTACT_SEARCH_RESULT_NO_HISTORY.contact, contact_id: 2 },
                    },
                ],
                partial_matches: [],
            },
            page_size: 1,
        })

        expect(wrapper.vm.toggle_more_text).toBe('common:show_more')
    })

    test('method load_representatives works correctly - representative does not exist', async() => {
        axios_mock.onGet(URLS.contact_representative_by_contact_id_ajax(CONTACT_SEARCH_RESULT_NO_HISTORY.object_id))
            .reply(404)
        await wrapper.vm.load_representatives()

        expect(wrapper.vm.$notify).not.toHaveBeenCalled()
        expect(console.log).not.toHaveBeenCalled()
        expect(wrapper.vm.representatives[CONTACT_SEARCH_RESULT_NO_HISTORY.object_id]).toStrictEqual(null)
    })

    test('method load_representatives works correctly - server error', async() => {
        axios_mock.onGet(URLS.contact_representative_by_contact_id_ajax(CONTACT_SEARCH_RESULT_NO_HISTORY.object_id))
            .reply(500)
        await wrapper.vm.load_representatives()

        expect(wrapper.vm.$notify).toHaveBeenCalledWith(wrapper.vm.ERROR_SOMETHING_WENT_WRONG)
        expect(console.log).toHaveBeenCalledWith('error: notifications:something_went_wrong')
        expect(wrapper.vm.representatives[CONTACT_SEARCH_RESULT_NO_HISTORY.object_id]).toStrictEqual(null)
    })

    test('method load_representatives works correctly - loaded representative', async() => {
        axios_mock.onGet(URLS.contact_representative_by_contact_id_ajax(CONTACT_SEARCH_RESULT_NO_HISTORY.object_id))
            .reply(200, CONTACT_REPRESENTATIVE)
        await wrapper.vm.load_representatives()

        expect(wrapper.vm.$notify).not.toHaveBeenCalled()
        expect(console.log).not.toHaveBeenCalled()
        expect(wrapper.vm.representatives[CONTACT_SEARCH_RESULT_NO_HISTORY.object_id])
            .toBe(CONTACT_REPRESENTATIVE.organization)
    })

    test('method prop_exact_match returns correct results', () => {
        expect(wrapper.vm.prop_exact_match(['harry.potter@example.com'])).toStrictEqual(true)
        expect(wrapper.vm.prop_exact_match('test')).toStrictEqual(false)
    })

    test('method get_property_by_path returns correct values', () => {
        expect(wrapper.vm.get_property_by_path('something.test', { something: [{ test: 'test1' }, {test: 'test2'}] }))
            .toStrictEqual(['test1', 'test2'])
        expect(wrapper.vm.get_property_by_path('something.test', { something: { test: 'test value' } }))
            .toBe('test value')
    })
})

describe('SearchRegistryObject - changes related to object type', () => {
    let wrapper
    let store

    beforeEach(() => {
        store = getClonedStoreOptions()
        store.modules.search.getters.last_query_values = () => ['harry.potter@example.com']

        wrapper = initWrapper(SearchRegistryObject, {
            propsData: {
                registrars: REGISTRAR_LIST,
                object_type: OBJECT_TYPE.CONTACT,
                search_results: [],
                history_matches_getter: () => [{item_name: 'test match', valid_from: '2020-03-05T13:00:00Z'}],
            },
            slots: {
                'overview-item': 'OVERVIEW-ITEM',
                'other-items': 'OTHER-ITEMS',
                'other-info': 'OTHER-INFO',
                'history-match': 'HISTORY-MATCH',
                'partial-list': 'PARTIAL-LIST',
            },
            customStoreOptions: store,
        })
    })

    test('property object_section_title returns correct values', async() => {
        await wrapper.setProps({ object_type: OBJECT_TYPE.CONTACT })
        expect(wrapper.vm.object_section_title).toBe('common:information')

        await wrapper.setProps({ object_type: OBJECT_TYPE.DOMAIN })
        expect(wrapper.vm.object_section_title).toBe('domain:domain_owner')

        await wrapper.setProps({ object_type: OBJECT_TYPE.NSSET })
        expect(wrapper.vm.object_section_title).toBe('nsset:dns_hosts')

        await wrapper.setProps({ object_type: OBJECT_TYPE.KEYSET })
        expect(wrapper.vm.object_section_title).toBe('keyset:dns_keys')
    })

    test('property object_title_item returns correct values', async() => {
        await wrapper.setProps({ object_type: OBJECT_TYPE.CONTACT })
        expect(wrapper.vm.object_title_item).toBe('contact_handle')

        await wrapper.setProps({ object_type: OBJECT_TYPE.DOMAIN })
        expect(wrapper.vm.object_title_item).toBe('fqdn')

        await wrapper.setProps({ object_type: OBJECT_TYPE.NSSET })
        expect(wrapper.vm.object_title_item).toBe('nsset_handle')

        await wrapper.setProps({ object_type: OBJECT_TYPE.KEYSET })
        expect(wrapper.vm.object_title_item).toBe('keyset_handle')
    })

    test('method overview_items returns correct results', async() => {
        await wrapper.setProps({ object_type: OBJECT_TYPE.CONTACT })
        expect(wrapper.vm.overview_items({ contact: SEARCH_CONTACT })).toStrictEqual([
            { name: 'name', value: SEARCH_CONTACT.name },
            { name: 'place', value: SEARCH_CONTACT.place },
            { name: 'organization', value: SEARCH_CONTACT.organization },
            { name: 'telephone', value: SEARCH_CONTACT.telephone },
            { name: 'email', value: SEARCH_CONTACT.emails.join(', ') },
        ])

        await wrapper.setProps({ object_type: OBJECT_TYPE.NSSET })
        expect(wrapper.vm.overview_items({ nsset: SEARCH_NSSET })).toStrictEqual([
            { name: 'dns_hosts', value: SEARCH_NSSET.dns_hosts },
        ])

        await wrapper.setProps({ object_type: OBJECT_TYPE.KEYSET })
        expect(wrapper.vm.overview_items({ keyset: SEARCH_KEYSET })).toStrictEqual([
            { name: 'dns_keys', value: SEARCH_KEYSET.dns_keys },
        ])

        await wrapper.setData({ domain_registrant: { email: 'test@email.org', telephone: '+33.123456789'} })
        await wrapper.setProps({ object_type: OBJECT_TYPE.DOMAIN })
        expect(wrapper.vm.overview_items({ domain: SEARCH_DOMAIN })).toStrictEqual([
            { name: 'registrant.handle', value: SEARCH_DOMAIN.registrant.handle },
            { name: 'registrant.organization', value: SEARCH_DOMAIN.registrant.organization },
            { name: 'registrant.name', value: SEARCH_DOMAIN.registrant.name },
            { name: 'registrant.telephone', value: '+33.123456789' },
            { name: 'registrant.email', value: 'test@email.org' },
        ])
    })

    test('method get_related_registrar works correctly', async() => {
        expect(wrapper.vm.get_related_registrar({ [OBJECT_TYPE.CONTACT]: { sponsoring_registrar: 'NON-EXISTENT' } }))
            .toStrictEqual(null)
        expect(wrapper.vm.get_related_registrar({ [OBJECT_TYPE.CONTACT]: { sponsoring_registrar: 'REG-HARRY' } }))
            .toStrictEqual({ handle: 'REG-HARRY', organization: 'Ministry of magic', url: 'www.example.org' })
    })
})

describe('SearchRegistryObject - CSV export', () => {
    let wrapper
    let blob_mock
    const initial_url = { ...URL }

    beforeEach(() => {
        global.URL.createObjectURL = jest.fn()
        global.URL.revokeObjectURL = jest.fn()

        blob_mock = jest.fn()
        jest.spyOn(global, 'Blob').mockImplementationOnce((...args) => blob_mock(...args))

        wrapper = initWrapper(SearchRegistryObject, {
            propsData: {
                registrars: REGISTRAR_LIST,
                object_type: OBJECT_TYPE.CONTACT,
                search_results: [],
                history_matches_getter: () => [{item_name: 'test match', valid_from: '2020-03-05T13:00:00Z'}],
            },
            slots: {
                'overview-item': 'OVERVIEW-ITEM',
                'other-items': 'OTHER-ITEMS',
                'other-info': 'OTHER-INFO',
                'history-match': 'HISTORY-MATCH',
                'partial-list': 'PARTIAL-LIST',
            },
        })
    })

    afterEach(() => {
        global.URL = initial_url
        jest.restoreAllMocks()
    })

    test('method export_exact_matches works correctly', async() => {
        await wrapper.setData({
            grouped_search_results: {
                exact_matches: [CONTACT_SEARCH_RESULT_NO_HISTORY],
                partial_matches: [],
            },
        })
        wrapper.vm.export_exact_matches()

        expect(blob_mock).toHaveBeenCalledWith([
            'handle;organization;name;telephone;email;representative;registrar\n',
            'HARRY;null;test name;null;harry.potter@example.com;null;REG-HARRY, Ministry of magic\n',
        ], { type: 'text/csv;charset=utf-8' })
        expect(URL.revokeObjectURL).toHaveBeenCalled()
    })
})
