import flushPromises from 'flush-promises'

import { initWrapper } from 'ferda-test/TestUtils'
import { SEARCH_HISTORY_PERIOD } from 'ferda/DatetimeMixin'
import { OBJECT_TYPE } from 'ferda/RegistryMixin'
import { URLS } from 'ferda/Urls'
import SearchBar from 'ferda/search/SearchBar.vue'

describe('SearchBar', () => {
    let wrapper

    beforeEach(async() => {
        wrapper = initWrapper(SearchBar, {}, false)
        await flushPromises()
    })

    afterEach(() => {
        history.pushState(null, null, location.pathname)
    })

    test('matches snapshot', () => {
        expect(wrapper.element).toMatchSnapshot()
    })

    test('method submit_search', async() => {
        await wrapper.setData({
            query_values: ['harry'],
            search_date: SEARCH_HISTORY_PERIOD.NOW,
            object_type: OBJECT_TYPE.CONTACT,
            object_search_items: {
                [OBJECT_TYPE.CONTACT]: ['name', 'email'],
                [OBJECT_TYPE.DOMAIN]: [],
                [OBJECT_TYPE.NSSET]: [],
                [OBJECT_TYPE.KEYSET]: [],
            },
        })
        await wrapper.vm.submit_search()

        expect(wrapper.emitted('submit-search')[0][0]).toStrictEqual({
            object_type: OBJECT_TYPE.CONTACT,
            query_values: ['harry'],
            search_date: SEARCH_HISTORY_PERIOD.NOW,
            search_items: ['name', 'email'],
        })
    })

    test('method submit_search does not emit when form is invalid', async() => {
        await wrapper.setData({ is_valid: false })
        await wrapper.vm.submit_search()
        expect(wrapper.emitted('search-submit')).toBeFalsy()
    })

    test('checkboxes', async() => {
        await wrapper.setData({ object_type: OBJECT_TYPE.CONTACT })
        wrapper.vm.handle_check_all_change(false)
        expect(wrapper.vm.is_all_checked).toBe(false)
        expect(wrapper.vm.is_indeterminate).toBe(false)
        expect(wrapper.vm.selected_search_items).toEqual([])

        wrapper.vm.object_search_items[OBJECT_TYPE.CONTACT] = ['name']
        expect(wrapper.vm.is_all_checked).toBe(false)
        expect(wrapper.vm.is_indeterminate).toBe(true)
        expect(wrapper.vm.selected_search_items).toEqual(['name'])

        wrapper.vm.handle_check_all_change(true)
        expect(wrapper.vm.is_all_checked).toBe(true)
        expect(wrapper.vm.is_indeterminate).toBe(false)
        expect(wrapper.vm.selected_search_items).toEqual(wrapper.vm.search_items_list)
    })

    test('location is changed with embedded', async() => {
        delete window.location
        window.location = { replace: jest.fn() }
        window.localStorage.__proto__.getItem = jest.fn(() => OBJECT_TYPE.DOMAIN)

        wrapper = initWrapper(SearchBar, { propsData: { embedded: true }}, false)
        await flushPromises()
        await wrapper.setData({ query_values: ['test_query']})

        history.pushState(null, null, '?query=test_query&type=domain')
        wrapper.vm.save_search()

        const desired_params = new URLSearchParams()
        desired_params.append('query', 'test_query')
        desired_params.append('type', OBJECT_TYPE.DOMAIN)

        expect(window.location.replace).toHaveBeenLastCalledWith(
            `${URLS.registry_search()}?${desired_params.toString()}`,
        )
    })

    test('method manage_form works correctly', async() => {
        delete window.location
        const mounted_params = new URLSearchParams()
        mounted_params.append('type', OBJECT_TYPE.CONTACT)
        mounted_params.append('query', 'test1')
        mounted_params.append('query', 'test2')
        mounted_params.append('history', 'full')
        mounted_params.append('search_items', 'place.street')
        mounted_params.append('search_items', 'place.city')
        mounted_params.append('search_items', 'place.state_or_province')
        mounted_params.append('search_items', 'place.postal_code')
        mounted_params.append('search_items', 'additional_identifier')
        mounted_params.append('search_items', 'organization')

        window.location = { search: `?${mounted_params.toString()}` }

        wrapper = initWrapper(SearchBar, {}, false)
        await flushPromises()

        expect(wrapper.vm.object_type).toBe(OBJECT_TYPE.CONTACT)
        expect(wrapper.vm.query_values).toStrictEqual(['test1', 'test2'])
        expect(wrapper.vm.search_date).toStrictEqual('full')
        expect(wrapper.vm.object_search_items).toStrictEqual({
            [OBJECT_TYPE.CONTACT]: ['address', 'identifier', 'organization'],
            [OBJECT_TYPE.DOMAIN]: [],
            [OBJECT_TYPE.KEYSET]: [],
            [OBJECT_TYPE.NSSET]: [],
        })
    })

    test('method trim_query_values tims whitespaces', async() => {
        await wrapper.setData({
            query_values: ['  test    case '],
            search_date: SEARCH_HISTORY_PERIOD.NOW,
            object_type: OBJECT_TYPE.CONTACT,
            object_search_items: {
                [OBJECT_TYPE.CONTACT]: ['name', 'email'],
                [OBJECT_TYPE.DOMAIN]: [],
                [OBJECT_TYPE.NSSET]: [],
                [OBJECT_TYPE.KEYSET]: [],
            },
        })
        await wrapper.vm.trim_query_values()

        expect(wrapper.vm.query_values).toStrictEqual(['test case'])
    })
})
