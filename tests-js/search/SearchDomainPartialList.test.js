import { initWrapper } from 'ferda-test/TestUtils'
import SearchDomainPartialList from 'ferda/search/SearchDomainPartialList.vue'

describe('SearchDomainPartialList', () => {
    let wrapper

    beforeEach(() => {
        wrapper = initWrapper(SearchDomainPartialList, {
            propsData: {
                history_matches_getter: () => [],
            },
        }, false)
    })

    test('matches snapshot', () => {
        expect(wrapper.element).toMatchSnapshot()
    })
})
