ChangeLog
=========

.. contents:: Releases
   :backlinks: none
   :local:


Unreleased
----------

4.3.0 (2024-02-27)
------------------

New features

* Change warning info message at registry search for 2-letter identifiers (#421)

Bug fixes

* Allow unlimited number of items in DataTables without pagination (#408, #410)

Maintenance

* Replace obsolete ajax API endpoints (#409, #423)
* Add report configuration in YAML (#414)
* Add report tags (#417)

4.2.3 (2024-02-18)
------------------

* Fix login password toggle (#416)
* Use only MDI font for circled check/cross icons (#418)

4.2.2 (2024-01-28)
------------------

4.2.1 (2024-01-28)
------------------

Bug fixes

* Add missing class locators to SVG icons (#412)

4.2.0 (2024-01-20)
------------------

* Add missing class locators to SVG icons (#412)

4.1.0 (2024-12-12)
------------------

* Correct size of medium-sized SVG icons (#88)

4.0.0 (2024-12-12)
------------------

Breaking changes

* Upgrade to pydantic 2 (#401)
* Remove context processors (#361)
* Remove previous link from paginated response (#102)
* Remove deprecated search ajax views (#274)
* Remove deprecated plugin system (#359)
* Remove deprecated context processor ferda_modules (#336)

Bug fixes

* Allow ``+`` sign in email addresses (#217)

3.26.5 (2024-10-16)
-------------------

* Fix invalid datetime value in report output (#402)

Bug fixes

* Use custom SVG icons instead of MDI font (#88)

3.26.4 (2024-10-03)
-------------------

* Enforce API authentication (#400)

3.26.3 (2024-09-25)
-------------------

* Upgrade to fred-filed ~= 0.4.0 (#399)
* Upgrade to fred-hermes ~= 5.3 (#399)

3.26.2 (2024-09-24)
-------------------

* Upgrade to ``fred-api-registry==5.6.0`` (#398)

3.26.1 (2024-09-18)
-------------------

Maintenance

* Replace ``frgal`` with ``fred-frgal``

3.26.0 (2024-09-11)
-------------------

New features

* Remember last searched object type (#393)
* Change generic error message (#394)

Bug fixes

* Handle certification protocol upload being too large (#381)
* Fix gettext error at login page (#391)
* Enforce at least 2 characters for e-mail TLDs (#387)
* Don't try to download user settings at fido login view (#396)

3.25.1 (2024-07-15)
-------------------

Maintenance

* Move some javascript imports to ``ferda/base.html`` template (#392)

3.25.0 (2024-07-09)
-------------------

New features

* Load user info and permissions from API (#368)
* Load django settings from API (#369)

Bug fixes

* Sort log entry types in logger module alphabetically (#388)

3.24.0 (2024-03-18)
-------------------

New features

* Check logger permissions on per-service basis (#384)
* Add max file upload size setting (#381)
  * not used by frontend yet

Bug fixes

* Fix email validation rules (#386)

3.23.0 (2024-02-06)
-------------------

New features

* Add logger list filter by properties (#378)
* Strip registry search query whitespaces (#371)

Bug fixes

* Fix loading state in zone access form (#255)
* Fix loading state in manual in zone form (#256)
* Fix after login redirect (#340)
* Fix translations (#379)

Maintenance

* Replace method trim_recipients in MessageList (#208)
* Upgrade to hermes 5 (#373)
* Update demo deploy configuration

3.22.2 (2023-09-07)
-------------------

* Upgrade to Django 4.2 only (#374)

3.22.1 (2023-08-21)
-------------------

* Add tests-js to published NPM package

3.22.0 (2023-08-21)
-------------------

New features

* Add registrar group membership UI (#268)
* Add search by registrar e-mail icon to registrar detail (#308)
* Display raw message data in message detail (#304)
* Add certification table to registrar detail (#313)
* Add actions to certification table to registrar detail (#325)
* Enable frontend report list filtering (#342)
* Add settings API (#310)
  * replace `django-settings` and `ferda-version` context processors in the future
* Add url prefixes to settings API (#334)
* Add user API (#362)
  * replace `user-profile` and `user-permissions` context processors in the future
* Add pluggy plugin framework (#357)
* Add create permissions plugin hook (#358)
* Add endpoint to download certification file (#324)
* Add regal-based API to replace `ferda.backend.client`
  (#314, #315, #316, #317, #318, #319, #320, #321, #323, #326, #330, #331, #332)

Bug fixes

* Make record statement PDF respect browser settings (download / view) (#291)
* Fix fingerprint file validation (error is not persistent anymore) (#264)
* Add missing __init__ modules

Maintenance

* Redesign search registry tab bar (#286)
  * remove unused tabs and move registrar list to separated menu item
* Update texts with respect to termdict (#279)
* Encapsulate logic in registrar detail (EPP certificates) (#248)
* Use babel polyfill to support stage 3 methods (#341)
* Unify test locators for dialog submit and close buttons (#343)
* Postpone registrar list load after the first registry search is performed (#345)
* Improve CSV export tests so that it is possible to check CSV content (#266)
* Handle left navigation menu in frontend only (#335)
* Update URL namespaces (#366)
* Import object id types directly from fred-types (#364)
* Deprecate old ferda plugins (#360)
* Reformat with black (#356)
* Add Django 4.2 support (#346)
* Refactor record statement tests (#333)

3.21.1 (2023-08-23)
-------------------

* Limit minimal length of contact registry search queries (#370).

3.21.0 (2023-08-07)
-------------------

* Add endpoints for representative (#347, #352).
* Update contact representative frontend (#350).
* Add ``registrant_ref`` to domain info results (#349).
* Register logger object reference types (#353).
* Fix the order of contact representative items (#354).

3.20.0 (2023-05-23)
-------------------

New features

* Add registrar groups and registrar certifications API
  (#292, #293, #294, #295, #296, #297, #298, #299, #300)
* Reverse plugin app order (#309)
* Add missing object state descriptions (#327)

Bug fixes

* Fix using TypedDict in regal 1.1.3
* Fix failing JavaScript tests (#312)

Docs

* Add demo deploy docs (#301)

3.19.0 (2023-03-27)
-------------------

New features

* Add search by notify-email to contact detail (#284)
* Add registry search CSV export for exact matches (#281)
* Add action buttons to registry search results (#282)

Enhancements

* Update registry search design (#189)
* Update log request detail design (#271)
* Unify Company field name in contact representative forms (#254)
* Remove opening links in new tabs (#290)

Bug fixes

* Fix lowercased translations (#306)
* Fix display of status icons in messages (#276)
* Remove company field in registrar forms (#302)

Maintenance

* Refactor contact representative form (#254)
* Encapsulate logic in registrar zone access form (#249)
* Fix mypy

3.18.2 (2023-02-24)
-------------------

New features

* Add ``output_properties`` to ``ferda.tests.utils.LoggerMixin.assertLog`` (#283)

Maintenance

* Change generic annotations from Schema to BaseModel (#278)

3.18.1 (2023-02-17)
-------------------

Breaking changes

* Changed PluginConfig from dataclass to pydantic model
  * The interface stayed the same, but types and values are now strictly checked

New features

* Add plugin permissions

3.18.0 (2023-02-15)
-------------------

New features

* Add new universal REST API for registry search

Enhancements

* Repaint colorized icons and tooltips to unified variants in registry module

Maintenance

* Unify form dialogs - contact representative form
* Unify form dialogs - manual inzone form

3.17.3 (2023-03-02)
-------------------

Bug fixes

* Fix record statements of domains, keysets and nssets (#287)

3.17.2 (2023-02-17)
-------------------

Bug fixes

* Fix display of status icons in messages

3.17.1 (2023-02-10)
-------------------

Enhancements

* Change required fields in registrar edit forms
* Add URL format hint to registrar edit forms

3.17.0 (2023-02-07)
-------------------

New features

* Add deleted domains to related related domains list in contact detail (#252)

Enhancements

* Don't crop certificate fingerprint in registrar detail (#241)
* Update columns in contact related domains table (#250)
* Update columns in contact related domains CSV export (#230, #251)
* Improve 404 registrar detail page (#237)
* Add title for information boxes in contact, domain and nsset detail (#258)
* Use real and scheduled domain milestones (#265)

Maintenance

* Upgrade to nodejs 18.x (#243)
* Remove deprecated auth info views and buttons (#211, #212)
* Add custom test locators (#179)
* Upgrade to django-fido 1.2.0
* Update setup and CI

Bug fixes

* Add link to registrar to log entry detail (#238)
* Fix validation in manual inzone form (#246)
* Don't require valid_to at zone access edit form (#245)
* Fix invalid outzone e-mail icon appearance (#247)
* Fix certificate validation so that it recovers from backend errors (#257)
* Fix default search period for message search link from contact detail (#260)
* Move searched references to log properties of ``LoggerList`` (#262)
* Hide related domains section in contact detail in case contact is deleted (#259)

3.16.0 (2022-12-12)
-------------------

New features

* Add registrar edit
* Add registrar zone access view and edit
* Add registrar epp credentials view and edit
* Show is_internal information at registrar detail
* Show expected outzone and delete dates at domain detail

Enhancements

* Shorten labels of object detail buttons
* Add a link from object detail to logger module
* Show name and label in message subtypes
* Trim white space from the beginning and end of recipients at messaging module
* Catch and propagate gRPC errors during registrar manipulation
* Change default search registry object type from contact to domain
* Add explaining note for registry search boxes
* Add message subtype labels to message detail
* Show exclamation icon at additional notification outzone emails when they are invalid

Maintenance

* Drop obsolete registrar methods from backend client
* Move all JS component rules to one place
* Refactor JS date-range picker
* Create custom html locators (classes in .tref-xxx format) for automated testing
* Update major npm package versions and fix npm audit problems
* Change message subtypes API endpoint to accept message types as query arguments

Bug fixes

* Fix swagger API docs
* Fix registrar website link at registrar detail
* Fix registry query box to detect the input without defocus and immediate search icon click
* Fix registrar detail condensed view
* Fix sorting of table records in notification module

3.15.0 (2022-09-07)
-------------------

New features

* Split two-factor authentication to two steps
* Refresh CSRF token before sending the login form
* Add message subtype labels
* Add filter of exact substring matches to registry search
* Add prefilled date ranges to messages and logger
* Add prerequisites for plugins
* Add progress info to notifications

Enhancements

* Add tooltip for message icons at contact detail
* Add "all" checkbox to related domains table
* Set 50 as default page size across all data tables
* Make message body box in messages list resizable
* Unify empty search results

Bug fixes

* Fix login form position
* Move all URLs to one place
* Fix skip to page from URL in registry search
* Fix URL with message list pagination
* Replace expiration datetime with date in related domains

Maintenance

* Use new registrar django-ninja REST API
* Remove obsolete registrar API
* Prevent unnecessary JS test logging to the console

3.14.0 (2022-08-02)
-------------------

New features

* Add record statement to object detail
  * Add setting for custom record statement timeout
* Add domains delete additional contacts edit
* Add prefilled value text to additional contacts edit
* Add status icon to message detail
* Add registrar REST API

Bug fixes

* Fix sorting in the column with decimals in reports
* Fix text overflow in messages
* Fix the display of list icons in messages
* Fix opening collapsed related messages in contact detail

Refactoring

* Merge boxes with basic information and permanent address in registrar detail
* Open message detail in new browser tab
* Limit date selection in messages

Maintenance

* Update browserslist storage
* Add domains delete additional contacts edit
* Correction of text display in message expansion panel
* Fix sorting in the column with decimals on reports
* Fix the display of message list icons
* Limit date selection at messages
* Display prefilled values next to the button at notifications
* Open the box with related messages and related domains on click

3.13.2 (2022-07-01)
-------------------

Bug fixes

* Fix table with related domains in contact detail

3.13.1 (2022-06-27)
-------------------

Bug fixes

* Fix domains outzone additional contacts edit

3.13.0 (2022-06-22)
-------------------

New features

* Add domains outzone additional contacts edit
* Add manual in zone button to domain detail
* Display related messages in object detail
* Display related domains in contact detail

3.12.1 (2022-05-12)
-------------------

Bug fixes

* Add accidentally removed links from contact detail to message list

3.12.0 (2022-05-11)
-------------------

New features

* Add message detail
* Add links from contact detail to message list
* Add messages box to contact detail
* Add search feature to some select boxes
* Add the ability to jump to the first and last page at report list
* Add notification close icon

Refactoring

* Use new paginated messages API at message list
* Use streamed response at report list

Bug fixes

* Fix position of icon at contact representative box
* Fix change detection in recipients box at message list
* Fix incorrect state for active domains
* Add messages box to object detail
* Enable writing to date-time picker menu directly
* Cache pagination results at list of log requests
* Move clipboard interaction to one mixin
* Add related domains to contact detail

3.11.3 (2022-04-20)
-------------------

* Upgrade fred-grill to 2.0 - fixes some decoding issues

3.11.2 (2022-04-11)
-------------------

Bug fixes

* Fix redundant API calls caused by selectbox component

3.11.1 (2022-04-06)
-------------------

New features

* Add logger search by username
* Add logger search by references

Bug fixes

* Add child properties to log entry detail

3.11.0 (2022-03-08)
-------------------

New features

* BREAKING Enable configuration of multiple reporting backends
* Add Logger module
* Create log entry types on startup
* Add translations for new contact states (identity-related)
* Add an option to paste date-time to widget from clipboard
* Convert naive date-time and time between utc and user's zone
* Add status icons to message list
* Add favicon

Refactoring
* Integrate browserslist
* Integrate postcss-loader
* Migrate request abortion to AbortController
* Minor changes in messaging and reporting UI
* Update supported Python versions

3.10.1 (2021-10-26)
-------------------

Bug fixes

* Drop translation for hidden registrar credit (hide column instead)
* Move admin app from left sidebar to user menu

3.10.0 (2021-10-11)
-------------------

New features

* Add better clear functionality to report widgets
* Enable manual prefill of date-time widget

Refactoring

* Change of translations
* Upgrade grpcio and protobuf dependencies
* Separate Jest config from package.json

Bug fixes

* Don't close DatePickerMenu dialog on month or year click
* Translate hidden registrar credit
* Add canceled status to translations

3.9.7 (2021-10-21)
------------------

Bug fixes

* Fix fred-api-logger version (cherry-pick from release/3.10)

3.9.6 (2021-08-11)
------------------

Bug fixes

* CSV export of report results now contains all the data

3.9.5 (2021-07-01)
------------------

Bug fixes

* Minor fixes in DatetimePickerMenu widget

3.9.4 (2021-06-28)
------------------

New features

* Add localized formatting to report run result

Bug fixes

* Fix behaviour of datetime widget

3.9.3 (2021-06-17)
------------------

New features

* Update report permissions
* Prefill date-time widget with current date in report form
* Add time zone abbreviation to corresponding widget types
* Localize numbers in report result

3.9.2 (2021-05-07)
------------------

Bug fixes

* Fix report update in case of parameter with the same name and different type

3.9.1 (2021-04-22)
------------------

Bug fixes

* Add missing dbreport settings
* Fix template links
* Some minor design fixes from testing

3.9.0 (2021-04-07)
------------------

New features

* Add DB report admin
* Add report parameters ordering to admin
* Add report labels to report views
* Use report labels in frontend
* Add object level permissions for reports

Refactoring

* Separate registry.js
* Add make_credentials backend helper function and simplify gRPC backends
* Frontend code revision - javascript
* Update npm packages to new major versions
* Update vuetify version and corresponding UI elements
* Migrate from fetch to axios

Bug fixes

* Add URL changes reflecting registrar list search
* Fix SameSite cookies

3.8.3 (2021-03-10)
------------------

Bug fixes

* Fix "Is VAT payer" label

3.8.2 (2021-03-10)
------------------

Bug fixes

* Fix InfoBox with access icons alignment

3.8.1 (2021-03-04)
------------------

Bug fixes

* Fix gettext translations

3.8.0 (2021-02-24)
------------------

New features

* Add messaging module – Email, Sms and Letters
* Add e-mail attachments
* Save registry object search parameters to SessionStorage
* Add selected langugage highlight
* Add possibility to cancel running search
* Use cznic PyPI for development and tests
* Use history_datetime property instead of history_id in logger messages

Refactoring

* Use webfonts for icons
* Move mixins content
* Sort JS imports

Bug fixes

* Display days in the logout counter
* Remove time from domain expire date

3.7.0 (2020-06-17)
------------------

Refactoring

* Migrate Vuetify framework from version 1.5 to version 2.3
  * Users should only see minor cosmetic changes in the UI

3.6.0 (2020-06-01)
------------------

New features

* Display contact representative in contact view
* Add user interface for contact representative edit and delete actions

Refactoring

* Define all log entry types in one Enum in ferda.constants
* Upgrade settings to be compatible with django-app-settings>=0.7.1

3.5.0 (2020-05-22)
------------------

New features

* Add contact representative model

Bug fixes

* Rename registrars log reference to registrar

3.4.2 (2020-04-24)
------------------

Bug fixes

* Fix login form with FIDO disabled in Google Chrome

3.4.1 (2020-03-31)
------------------

Bug fixes

* Fix loading of related contacts in keyset history

3.4.0 (2020-03-25)
------------------

New features

* Add history domain search
* Add history keyset search
* Add history nsset search
* Add technical contacts to nsset and keyset search results
* Add Django 3.0 support
* Drop Django 2.1 support
* Add FIDO 2 authentication
* Add automatic logout and logout counter

Refactoring

* Refactor SearchRegistryObject component

Bug fixes

* Fix detail of domain with deleted owner or administrative contact

3.3.1 (2020-01-24)
------------------

Bug fixes

* Fix gRPC reconnect issue

3.3.0 (2020-01-20)
------------------

New features

* Add history contact search

Bug fixes

* Fix auth info loading for deleted objects
* Fix JS tests broken by new @vue/test-utils

3.2.0 (2019-12-17)
------------------

New features

* Add parallel non-blocking backend calls in registrar list view
* Add search contact history views (not yet visible to users)

Bug fixes

* Fix registrar list loading overlay
* Fix JS tests broken by new @vue/test-utils

3.1.0 (2019-11-14)
------------------

New features

* Add localization expiration
* Update registrar list according to client needs
* Replace some icons with better alternatives

Refactoring

* Update GitLab CI settings
* Update JS tests

Bug fixes

* Fix zones in registrar list
* Fix typing errors caused by new mypy version
* Fix translation bugs and typos

3.0.0 (2019-09-18)
------------------

New features

* Add keyset detail and history
* Add keyset to domain detail
* Add nsset and keyset search
* Add registrar list
* Add localization
* Add Czech translation
* Add monitoring view
* Add copy to clipboard button for auth info
* Add link for showing all available addresses in contact detail condensed mode
* Replace different errors and info messages with unified notifications
* Use chips for multi-expression queries in search bar
* Show registrant name in domain history

Refactoring

* Add generic ObjectDetail and ObjectHistory components
* Disallow eslint warnings
* Update tests with new wrapper and test store

Bug fixes

* Fix fetch errors in contact and domain history
* Fix highlighting of multi-word expressions for exact match
* Fix order of registrar zones
* Hide empty administrative contacts

Docs

* Rewrite changelog

2.1.0 (2019-07-22)
------------------

New features

* Add sponsoring registrar handle to managed object pages
* Add google convention for pydocstyle
* Save preferences per user instead of per browser

Bug fixes

* Fix asynchronous data loading
* Fix additional identifier in search results
* Fix bug with duplicate address showed in search results
* Fix search by VAT
* Fix eslint and test warnings

Docs

* Add logging docs

2.0.0 (2019-07-02)
------------------

Dependencies

* Upgrade to ``protobuf>=3.7.0`` because of the `bug in FromDatetime`__

__ https://github.com/protocolbuffers/protobuf/issues/5003

New features

* Add logging
* Add domain detail and history
* Add nsset detail and history
* Add domain search
* Add registrar detail
* Add search bar on the top of all pages
* Add detail of deleted object showing last recorded data
* Add ``can_view_authinfo`` permission
* Add ``view_registrar_credit`` permission
* Add taxpayer identification number to contact pages
* Add warning letters preference to contact pages
* Add hide button for auth info
* Add data-ref-id and data-ref-type attributes for automated testing
* Allow creating user without password (authentication is still possible through LDAP or similar)
* Show additional identifier type (birthday, passport number, etc.)
* Save drawer and verbosity preferences
* Send selected django settings to JavaScript
* Switch object detail columns
* Change checkbox color in search form
* Hide events infobox in condensed view

Bug fixes

* Fix broken links from object history pointing to deleted objects
* Fix exact match highlighting in search results
* Show company name in shipping address

1.0.0 (2019-04-09)
------------------

Initial version.

New features

* Add grpc backend
* Add Vue JS framework with Vuetify component library
* Add Vuex state management system
* Add eslint, jest and tox to CI
* Add Roboto fonts
* Add contact detail
* Add contact history
* Add contact search
* Add badge area (including new ``FERDA_BADGERS`` setting)
* Add authentication
* Add ``can_view_registry`` permission
* Add logging of auth info disclosure

Docs

* Add ``README``
* Add settings docs
* Add supported browsers declaration
