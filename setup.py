#!/usr/bin/python3
"""Setup script for ferda."""

from distutils.command.build import build

from setuptools import setup
from setuptools.command.sdist import sdist
from setuptools_npm import npm_not_skipped


class custom_build(build):
    """Override build command."""

    sub_commands = [
        ("compile_catalog", None),
        ("npm_install", npm_not_skipped),
        ("npm_run", npm_not_skipped),
    ] + build.sub_commands


class custom_sdist(sdist):
    """Override sdist command."""

    def run(self):
        """Add compile gettext catalog step."""
        self.run_command("compile_catalog")
        super().run()


setup(
    cmdclass={"build": custom_build, "sdist": custom_sdist},
)
