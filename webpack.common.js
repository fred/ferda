const path = require('path')
const { VueLoaderPlugin } = require('vue-loader')
const MiniCssExtractPlugin = require('mini-css-extract-plugin')
const VuetifyLoaderPlugin = require('vuetify-loader/lib/plugin')

module.exports = {
    entry: {
        'js/main': [
            path.resolve(__dirname, 'assets/js/app.base.js'),
            path.resolve(__dirname, 'assets/js/app.registry.js'),
            path.resolve(__dirname, 'assets/js/app.messaging.js'),
            path.resolve(__dirname, 'assets/js/app.reporting.js'),
            path.resolve(__dirname, 'assets/js/app.logger.js'),
        ],
        'css/main': path.resolve(__dirname, 'assets/scss/main.scss'),
        'css/admin': path.resolve(__dirname, 'assets/scss/admin.scss'),
    },
    output: {
        filename: '[name].js',
        path: path.resolve(__dirname, 'ferda/static/ferda'),
    },
    module: {
        rules: [{
            test: /\.js$/,
            include: path.join(__dirname, 'assets/js'),
            exclude: /node_modules/,
            use: ['babel-loader'],
        }, {
            test: /\.(s?c|sa)ss$/,
            oneOf: [{
                resourceQuery: /module/,
                use: [
                    MiniCssExtractPlugin.loader,
                    {
                        loader: 'css-loader',
                        options: {
                            modules: {
                                localIdentName: '[name]__[local]--[hash:base64:8]',
                                exportLocalsConvention: 'camelCase',
                            },
                        },
                    },
                    'postcss-loader',
                    'sass-loader',
                ],
            }, {
                use: [
                    MiniCssExtractPlugin.loader,
                    'css-loader',
                    'postcss-loader',
                    'sass-loader',
                ],
            }],
        }, {
            test: /\.styl$/,
            use: [
                MiniCssExtractPlugin.loader,
                'css-loader',
                'stylus-loader',
            ],
        }, {
            test: /\.vue$/,
            use: 'vue-loader',
        }, {
            test: /\.js$/,
            use: 'babel-loader',
            exclude: /node_modules/
        }, {
            test: /\.(woff|woff2|ttf|eot)(\?[\s\S]+)?$/,
            type: 'asset/resource',
            generator: {
                filename: 'webfonts/[name][ext]',
            },
        }, {
            test: /\.svg$/,
            type: 'asset/resource',
            generator: {
                filename: 'svg/[name].svg',
            },
        }, {
            test: /\.m?js/,
            resolve: {
                fullySpecified: false,
            },
        }],
    },
    plugins: [
        new VueLoaderPlugin(),
        new MiniCssExtractPlugin({
            filename: '[name].css',
            chunkFilename: '[id].css',
        }),
        new VuetifyLoaderPlugin(),
    ],
    resolve: {
        alias: {
            FerdaCSS: path.resolve(__dirname, 'assets/scss'),
            FerdaJS: path.resolve(__dirname, 'assets/js'),
            ferda: path.resolve(__dirname, 'assets/js/components'),
            'ferda-test': path.resolve(__dirname, 'tests-js'),
            svg: path.resolve(__dirname, 'assets/svg'),
        },
    },
}
