"""Ferda urls."""

from django.contrib import admin
from django.urls import include, path, reverse_lazy
from django.views.generic.base import RedirectView

urlpatterns = [
    path("", include("ferda.urls")),
    path("admin/logout/", RedirectView.as_view(url=reverse_lazy("ferda:logout"))),
    path("admin/login/", RedirectView.as_view(url=reverse_lazy("ferda:login"))),
    path("admin/", admin.site.urls),
    path("", RedirectView.as_view(url=reverse_lazy("ferda:registry:search:search"))),
    path("django_fido/", include("django_fido.urls")),
]
