"""Django settings for ferda app."""

import socket

import environ
from django.core.exceptions import ImproperlyConfigured

ENV = environ.Env()
APPNAME = "ferda"

######################
# Essential settings #
######################

SECRET_KEY = ENV.str("SECRET_KEY")
DEBUG = ENV.bool("DEBUG", default=False)
ALLOWED_HOSTS = ENV.list("ALLOWED_HOSTS", default=["*"] if DEBUG else ENV.NOTSET)
USE_X_FORWARDED_HOST = USE_X_FORWARDED_PORT = ENV.bool("USE_X_FORWARDED_HOST", default=False)
CSRF_TRUSTED_ORIGINS = ENV.list("CSRF_TRUSTED_ORIGINS", default=["127.0.0.1"] if DEBUG else [])
INTERNAL_IPS = ["127.0.0.1"]
ADMINS = [x.split(":", 1) for x in ENV.list("ADMINS", default=[])]
CSRF_COOKIE_SECURE = SESSION_COOKIE_SECURE = not DEBUG
X_FRAME_OPTIONS = "DENY"

XFF_TRUSTED_PROXY_DEPTH = 1
if ENV.bool("TRUSTED_PROXY", default=False):
    SECURE_PROXY_SSL_HEADER = ("HTTP_X_FORWARDED_PROTO", "https")
    XFF_TRUSTED_PROXY_DEPTH = 2

################
# Url settings #
################

ROOT_URLCONF = "ferda_cfg.urls"
LOGIN_REDIRECT_URL = "/registry/search/"
SECURE_SSL_REDIRECT = True

###########################
# Ferda specific settings #
###########################

# FERDA_REGISTRY_NETLOC has to be set to address and port of fred-backend-registry service.
FERDA_GRPC_NETLOC = ENV.str("FERDA_REGISTRY_NETLOC")

# FERDA_LOGGER_NETLOC has to be set to address and port of fred-backend-logger service.
# It can also be set to address of gloss, our simple logging server.
FERDA_LOGGER_NETLOC = ENV.str("FERDA_LOGGER_NETLOC", default="gloss:50051")

# FERDA_MESSENGER_NETLOC has to be set to address and port of fred-backend-messenger service.
FERDA_MESSENGER_NETLOC = ENV.str("FERDA_MESSENGER_NETLOC")

# FERDA_MESSENGER_NETLOC has to be set to address and port of fred-backend-fileman service.
FERDA_FILEMAN_NETLOC = ENV.str("FERDA_FILEMAN_NETLOC")

# FERDA_SECRETARY_URL has to be set to url of django-secretary service (including the path to the API).
FERDA_SECRETARY_URL = ENV.str("FERDA_SECRETARY_URL")

# A timeout for connection to django-secretary service.
FERDA_SECRETARY_TIMEOUT = ENV.str("FERDA_SECRETARY_TIMEOUT", default=3.05)

# Token for authentication at django-secretary service.
FERDA_SECRETARY_TOKEN = ENV.str("FERDA_SECRETARY_TOKEN", default=None)

# FERDA_DBREPORT values have to be set to address and port of fred-backend-dbreport services.
FERDA_REPORTS = {
    "registry": {
        "NETLOC": ENV.str("FERDA_DBREPORT_REGISTRY_NETLOC"),
    },
}

########################################
# Applications and middleware settings #
########################################

INSTALLED_APPS = [
    "django.contrib.auth",
    "django.contrib.contenttypes",
    "django.contrib.sessions",
    "django.contrib.messages",
    "django.contrib.staticfiles",
    "ferda.apps.FerdaConfig",
    "adminsortable2",
    "django.contrib.admin",
    "django_countries",
    "django_fido",
    "guardian",
]

AUTH_USER_MODEL = "ferda.User"

MIDDLEWARE = [
    "xff.middleware.XForwardedForMiddleware",
    "django.middleware.security.SecurityMiddleware",
    "django.contrib.sessions.middleware.SessionMiddleware",
    "django.middleware.locale.LocaleMiddleware",
    "django.middleware.common.CommonMiddleware",
    "django.middleware.csrf.CsrfViewMiddleware",
    "django.contrib.auth.middleware.AuthenticationMiddleware",
    "django.contrib.messages.middleware.MessageMiddleware",
    "django.middleware.clickjacking.XFrameOptionsMiddleware",
    "ferda.middleware.SessionExpiresMiddleware",
]

#####################
# Template settings #
#####################

TEMPLATES = [
    {
        "BACKEND": "django.template.backends.django.DjangoTemplates",
        "DIRS": [],
        "APP_DIRS": True,
        "OPTIONS": {
            "context_processors": [
                "django.template.context_processors.request",
                "django.template.context_processors.i18n",
                "django.contrib.auth.context_processors.auth",
                "django.contrib.messages.context_processors.messages",
                "ferda.context_processors.ferda_modules",
            ],
        },
    },
]

if DEBUG:
    TEMPLATES[0]["OPTIONS"]["context_processors"].insert(0, "django.template.context_processors.debug")

#####################
# Database settings #
#####################

if ENV.str("DATABASE_PASSWORD", default=None):
    DATABASES = {
        "default": {
            "ENGINE": "django.db.backends.postgresql_psycopg2",
            "HOST": ENV.str("DATABASE_HOST", default="localhost"),
            "PORT": ENV.str("DATABASE_PORT", default=""),
            "NAME": ENV.str("DATABASE_NAME", default=APPNAME),
            "USER": ENV.str("DATABASE_USER", default=APPNAME),
            "PASSWORD": ENV.str("DATABASE_PASSWORD", default=""),
        }
    }
else:
    # These settings specify simple file-based squlite database.
    # You'll probably want to use some other database, such as PostgreSQL.
    #
    # WARNING: These database settings place the sqlite file inside the docker container
    #          and it will be lost when you delete the container! This is not suitable
    #          for production use.
    DATABASES = {
        "default": ENV.db(
            "DATABASE_URL",
            default="sqlite:////app/db.sqlite3",
        ),
    }

###################
# E-mail settings #
###################

EMAIL_HOST = ENV.str("EMAIL_HOST", default="localhost")
EMAIL_PORT = ENV.int("EMAIL_PORT", default=25)
EMAIL_HOST_USER = ENV.str("EMAIL_HOST_USER", default="")
EMAIL_HOST_PASSWORD = ENV.str("EMAIL_PASSWORD", default="")
SERVER_EMAIL = ENV.str("SERVER_EMAIL", default="root@localhost")
DEFAULT_FROM_EMAIL = ENV.str("DEFAULT_FROM_EMAIL", default="webmaster@localhost")
EMAIL_SUBJECT_PREFIX = ENV.str("EMAIL_SUBJECT_PREFIX", default="[Ferda %s]: " % socket.gethostname())

#########################
# Static files settings #
#########################

STATIC_ROOT = "/app/static/"
STATIC_URL = "/static/"
STATICFILES_STORAGE = "django.contrib.staticfiles.storage.ManifestStaticFilesStorage"

#########################
# Media files settings #
#########################

MEDIA_ROOT = "/app/media/"
MEDIA_URL = "/media/"

#################################
# Language and locales settings #
#################################

# WARNING: Setting USE_TZ to False might have unexpected consequences.
USE_TZ = True
USE_I18N = True
USE_L10N = True

TIME_ZONE = ENV.str("TIME_ZONE", default="UTC")
LANGUAGE_CODE = ENV.str("LANGUAGE_CODE", default="en")
LANGUAGES = [x.split(":") for x in ENV.list("LANGUAGES", default=["en:English", "cs:Česky"])]

####################
# Logging settings #
####################

# We are logging to console. Docker collects this log and forwards it to syslog.
LOGGING = {
    "version": 1,
    "disable_existing_loggers": False,
    "formatters": {
        "verbose": {"format": "%(asctime)s %(levelname)-8s %(module)s:%(funcName)s:%(lineno)s %(message)s"},
    },
    "handlers": {
        "mail_admins": {
            "level": "ERROR",
            "class": "django.utils.log.AdminEmailHandler",
            "include_html": True,
        },
        "console": {
            "level": "DEBUG" if DEBUG else "INFO",
            "class": "logging.StreamHandler",
            "formatter": "verbose",
        },
    },
    "loggers": {
        "": {
            "handlers": ["console"],
            "level": "DEBUG" if DEBUG else "INFO",
            "propagate": False,
        },
        # Override django logger to only propagate logs to root logger.
        "django": {
            "propagate": True,
        },
        # Handle logs to django.request separately.
        "django.request": {
            "handlers": ["mail_admins", "console"],
            "level": "ERROR",
            "propagate": False,
        },
    },
}

###########################
# Authentication settings #
###########################

AUTHENTICATION_BACKENDS = [
    "django.contrib.auth.backends.ModelBackend",
]

# LDAP authentication - see https://github.com/etianen/django-python3-ldap
LDAP_AUTH_URL = ENV.str("LDAP_AUTH_URL", default=None)
LDAP_AUTH_USE_TLS = ENV.bool("LDAP_AUTH_USE_TLS", default=True)
LDAP_AUTH_SEARCH_BASE = ENV.str("LDAP_AUTH_SEARCH_BASE", default="")

if LDAP_AUTH_URL:
    try:
        import django_python3_ldap  # noqa: F401
    except ImportError as err:
        raise ImproperlyConfigured(
            "You try to set up LDAP without LDAP library installed. "
            "Enable LDAP support while building your docker image."
        ) from err
    else:
        AUTHENTICATION_BACKENDS.insert(0, "django_python3_ldap.auth.LDAPBackend")
        INSTALLED_APPS.append("django_python3_ldap")

# Fido two-factor authentication
if ENV.bool("FIDO_ENABLED", default=False):
    AUTHENTICATION_BACKENDS.append("django_fido.backends.Fido2AuthenticationBackend")
    DJANGO_FIDO_TWO_STEP_AUTH = True

# Object level permissions with django-guardian
AUTHENTICATION_BACKENDS.append("guardian.backends.ObjectPermissionBackend")
ANONYMOUS_USER_NAME = None

# We use 20 minutes from last activity until automatic logout.
SESSION_COOKIE_AGE = ENV.int("SESSION_COOKIE_AGE", default=20 * 60)
SESSION_SAVE_EVERY_REQUEST = ENV.bool("SESSION_SAVE_EVERY_REQUEST", default=True)
LANGUAGE_COOKIE_SAMESITE = "Lax"
LANGUAGE_COOKIE_SECURE = True
