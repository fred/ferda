=======
 FERDA
=======

Web admin interface for FRED_.

.. _FRED: http://fred.nic.cz

What's the meaning of “FERDA”?
==============================

**FERDA** is abbreviation for **Ferda is Enhanced Redesigned DAphne**.

* Daphne is a previous incarnation of FRED_ web administration tool

Also, Ferda is name of famous Czech comics character `Ferda Mravenec`_ (Ferdinand the Ant)
who is famous for being all around craftsman.

.. _Ferda Mravenec: https://en.wikipedia.org/wiki/Ferda_Mravenec


Supported browsers
==================

Ferda is tested with last two versions of Mozilla Firefox and Google Chrome.
However, it will probably work in any modern browser without problems.


Settings
========

``FERDA_GRPC_NETLOC``
---------------------

Network location, i.e. host and port, of the GRPC server.
This setting is required.

``FERDA_GRPC_SSL_CERT``
-----------------------

Path to file with SSL root certificate.
Default value is ``None``, which disables the SSL encryption.

``FERDA_LOGGER_NETLOC``
-----------------------

Network location, i.e. host and port, of the GRPC server for logging.
This setting is required.

``FERDA_LOGGER_SSL_CERT``
-------------------------

Path to file with SSL root certificate.
Default value is ``None``, which disables the SSL encryption.

``FERDA_MESSENGER_NETLOC``
--------------------------

Network location, i.e. host and port, of the GRPC server with messenger service.
This setting is required.

``FERDA_MESSENGER_SSL_CERT``
----------------------------

Path to file with SSL root certificate.
Default value is ``None``, which disables the SSL encryption.

``FERDA_FILEMAN_NETLOC``
------------------------

Network location, i.e. host and port, of the GRPC server with fileman service.
This setting is required.

``FERDA_FILEMAN_SSL_CERT``
--------------------------

Path to file with SSL root certificate.
Default value is ``None``, which disables the SSL encryption.

``FERDA_SECRETARY_TIMEOUT``
-----------------------------------

A timeout for connection to django-secretary service.
For possible values see documentation of ``requests`` library.
Default value is ``3.05``.

``FERDA_SECRETARY_TOKEN``
---------------------------------

Token for authentication at django-secretary service.
Default value is ``None``, i.e. no authentication.

``FERDA_SECRETARY_URL``
-----------------------

URL of django-secretary service (including the path to the API).
This setting is required.

``FERDA_RECORD_STATEMENT_TIMEOUT``
----------------------------------

A timeout for connection and read of record statement requests
to django-secretary service.
For possible values see documentation of ``requests`` library.
Default value is ``(3.05, 30)``.

``FERDA_REPORTS``
-----------------

Dictionary with dbreport service configurations. Key should be service name slug.
You should not change or remove the key once you set it unless you already
deleted all reports from this service in the Ferda admin interface.

Values are dictionaries with dbreport service configuration:

* ``NETLOC`` – Nework location, i.e. host and port of the gRPC server
  with dbreport service. This key is required.
* ``SSL_CERT`` – Path to file with SSL root certificate.
  Default value is ``None``, which disables the SSL encryption.
* No other keys are allowed.

``FERDA_STATE_FLAGS_DESCRIPTIONS``
----------------------------------

Dictionary setting.
Keys are state flags names and values are state flags descriptions.
In case that value is ``None``, the appropriate flag is hidden from the web interface.
This setting is merged with default flags dictionary from ``ferda.settings`` module.
Default value is ``{}`` (empty dictionary).

``FERDA_STATE_FLAGS_GROUPS``
----------------------------

List of state flags groups.
Each group should be list of state flags names.
State flags groups are displayed separately in contact detail.
State flags that are not included in any group are displayed as separate "others" group.
Default value is ``[['linked', 'serverBlocked', 'deleteCandidate', 'serverDeleteProhibited', 'serverTransferProhibited', 'serverUpdateProhibited']]``.

``FERDA_BADGERS``
-----------------

List of dotted paths to badge providers classes.
All classes in the list must subclass ``ferda.registry.plugins.badges.AbstractBadger``.
Default value is ``[]`` (empty list).

``FERDA_CONCURRENT_GRPC_CALLS_LIMIT``
-------------------------------------

Number of maximum concurrent grpc calls in single parallel call execution.
There may be multiple parallel call executions running at the same time (e.g. in different workers).
Default value is 10.

``FERDA_MANUAL_IN_ZONE_DEFAULT_DURATION``
-----------------------------------------

Default number of seconds of domain being manually generated in zone.
Default value is 604800 (7 days).

``FERDA_MAX_FILE_UPLOAD_SIZE``
------------------------------

Maximum size of file upload in bytes.
Default value is 10 000 000 (10 MB).


Environment variables
=====================

``FERDA_DEPLOY_ID``
-------------------

Unique identifier of Ferda deployment. This value is appended to Ferda version
to create unique version identifier.


Changes
=======

See changelog_.

.. _changelog: CHANGELOG.rst
